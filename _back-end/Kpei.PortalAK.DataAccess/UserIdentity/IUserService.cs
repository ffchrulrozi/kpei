﻿using Kpei.PortalAK.DataAccess.ModPincode.Forms;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;

namespace Kpei.PortalAK.DataAccess.UserIdentity {
    public interface IUserService {
        AppUser LoadUser(IIdentity identity);
        AppUser LoadUser(ClaimsIdentity identity);
        IEnumerable<AppUser> LoadUsers(IEnumerable<string> ids);
        IEnumerable<AppUser> LoadUsers(string kodeAk, IEnumerable<string> roleNames = null);
        AppUserRight GetUserRight(AppUser user, bool useCache = true);
        AppUserRight GetUserRight(string userId, string[] userRoleNames, string userCompanyCode, bool useCache = true);

        void SendEmailToManyAknp(IEnumerable<string> codes, string header, string body);

        #region Aknp

        void SendNotifNewRegistrasi(string destination, string url);
        void SendNotifRejectGuestRegistrasi(string destination, string url, string reason);

        void SendNotifNewAknp(string kodeAk, string url);
        void SendNotifUpdateAknp(string kodeAk, string url);
        void SendNotifApproveAknp(string kodeAk, string url);
        void SendNotifDoneAknp(string kodeAk, string url);
        void SendNotifRejectAknp(string kodeAk, string url, string reason);

        #endregion

        #region Pincode

        void SendNotifNewPincode(string kodeAk, string url);
        void SendNotifActionPincode(string kodeAk, string url);
        void SendNotifRejectPincode(string kodeAk, string url, string reason);
        void SendNewPincodeEmail(SendPincodeForm form, string kodeAk, string url, string newUsername, string newPassword, string urlTerkait);

        #endregion

        #region Laporan Keuangan

        void SendNotifNewLapKeu(string kodeAk, string url);
        void SendNotifUpdateLapKeu(string kodeAk, string url);
        void SendNotifActionLapKeu(string kodeAk, string url);
        void SendNotifRejectLapKeu(string kodeAk, string url, string reason);

        #endregion

        #region Survei

        void SendNotifNewSurvei(string url);
        void SendNotifUpdateSurvei(string url);

        #endregion

        #region m-CLEARS

        void SendNotifNewMclears(string kodeAk, string url);
        void SendNotifUpdateMclears(string kodeAk, string url);
        void SendNotifActionMclears(string kodeAk, string url);

        #endregion

        #region Administrasi Keanggotaan

        void SendNotifNewAdministrasiKeanggotaan(string kodeAk, string url);
        // void SendNotifRejectGuestRegistrasi(string destination, string url, string reason);
        void SendNotifUpdateAdministrasiKeanggotaan(string kodeAk, string url);
        void SendNotifApproveAdministrasiKeanggotaan(string kodeAk, string url);
        void SendNotifRejectAdministrasiKeanggotaan(string kodeAk, string url, string reason);

        #endregion

        #region Sosialisasi dan Pelatihan (Event)

        void SendNotifNewSospel(string url);
        void SendNotifUpdateSospel(List<string> kodeAk, string url);
        void SendNotifNewRegistrationSospel(string kodeAk, string url);
        void SendNotifUpdateRegistrationSospel(string kodeAk, string url);

        #endregion

    }
}