﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.ModPincode.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Drivers;
using Kpei.PortalAK.DataAccess.ModDataAknp.Services;
using Kpei.PortalAK.DataAccess.DbModels;
using System.Diagnostics;

namespace Kpei.PortalAK.DataAccess.UserIdentity {
    public class UserService : IUserService {
        private readonly AppDbContext _dbctx;
        private readonly MemberDbContext _memctx;
        private readonly ISettingRepository _stgrepo;
        private readonly IEmailService _emailsvc;
        private readonly IKpeiCompanyService _compsvc;
        private readonly IDataAknpDriverStore _dataAknpDrvs;
        private readonly bool debugMode;

        private readonly IEnumerable<EmailSetting> aknpEmail;
        private readonly IEnumerable<EmailSetting> pincodeEmail;
        private readonly IEnumerable<EmailSetting> mclearsEmail;
        private readonly IEnumerable<EmailSetting> administrasiKeanggotaanEmail;
        private readonly IEnumerable<EmailSetting> sospelEmail;
        private readonly IEnumerable<EmailSetting> keuanganEmail;
        private readonly IEnumerable<EmailSetting> surveiEmail;

        public UserService(AppDbContext dbctx, MemberDbContext memctx, ISettingRepository stgrepo, IEmailService emailsvc, IKpeiCompanyService compsvc, IDataAknpDriverStore dataAknpDrvs) {
            _dbctx = dbctx;
            _memctx = memctx;
            _stgrepo = stgrepo;
            _emailsvc = emailsvc;
            _compsvc = compsvc;
            _dataAknpDrvs = dataAknpDrvs;

            debugMode = _stgrepo.GlobalSetting().EmailDebugMode;

            aknpEmail = _stgrepo.GetAknpEmail();
            pincodeEmail = _stgrepo.GetPincodeEmail();
            mclearsEmail = _stgrepo.GetMclearsEmail();
            administrasiKeanggotaanEmail = _stgrepo.GetAdministrasiKeanggotaanEmail();
            sospelEmail = _stgrepo.GetSospelEmail();
            keuanganEmail = _stgrepo.GetKeuanganEmail();
            surveiEmail = _stgrepo.GetSurveiEmail();

        }

        /// <inheritdoc />
        public AppUser LoadUser(IIdentity identity) {
            return LoadUser(identity as ClaimsIdentity);
        }

        /// <inheritdoc />
        public AppUser LoadUser(ClaimsIdentity identity) {
            var swThis = Stopwatch.StartNew();
            if (identity == null) return null;
            var usr = new AppUser();
            foreach (var cla in identity.Claims) {
                if (cla.Type == ClaimTypes.NameIdentifier) {
                    usr.Id = cla.Value;
                } else if (cla.Type == ClaimTypes.Name) {
                    usr.UserName = cla.Value;
                } else if (cla.Type == "FullName") {
                    usr.FullName = cla.Value;
                } else if (cla.Type == "Email") {
                    usr.Email = cla.Value;
                } else if (cla.Type == "UserCreatedUtc") {
                    usr.UserCreatedUtc = string.IsNullOrWhiteSpace(cla.Value)
                        ? (DateTime?)null
                        : DateTime.Parse(cla.Value);
                } else if (cla.Type == "UserUpdatedUtc") {
                    usr.UserUpdatedUtc = string.IsNullOrWhiteSpace(cla.Value)
                        ? (DateTime?)null
                        : DateTime.Parse(cla.Value);
                } else if (cla.Type == "KpeiCompanyName") {
                    usr.KpeiCompanyName = cla.Value;
                } else if (cla.Type == "KpeiCompanyCode") {
                    usr.KpeiCompanyCode = cla.Value;
                } else if (cla.Type == "KpeiCompanyIsMI") {
                    usr.KpeiCompanyIsMI = bool.Parse(cla.Value);
                } else if (cla.Type == "KpeiCompanyIsNonAB") {
                    usr.KpeiCompanyIsNonAB = bool.Parse(cla.Value);
                } else if (cla.Type == "KpeiCompanyIsPEE") {
                    usr.KpeiCompanyIsPEE = bool.Parse(cla.Value);
                } else if (cla.Type == "KpeiCompanyIsPPE") {
                    usr.KpeiCompanyIsPPE = bool.Parse(cla.Value);
                } else if (cla.Type == "KpeiCompanyManagerName") {
                    usr.KpeiCompanyManagerName = cla.Value;
                } else if (cla.Type == "KpeiCompanyMkbdGroup") {
                    usr.KpeiCompanyMkbdGroup = cla.Value;
                } else if (cla.Type == "KpeiCompanyCreatedUtc") {
                    usr.KpeiCompanyCreatedUtc = string.IsNullOrWhiteSpace(cla.Value)
                        ? (DateTime?)null
                        : DateTime.Parse(cla.Value);
                } else if (cla.Type == "KpeiCompanyUpdatedUtc") {
                    usr.KpeiCompanyUpdatedUtc = string.IsNullOrWhiteSpace(cla.Value)
                        ? (DateTime?)null
                        : DateTime.Parse(cla.Value);
                } else if (cla.Type == "KpeiMemberRole") {
                    usr.KpeiMemberRole = cla.Value;
                }
            }

            Debug.WriteLine("LoadUser: whole process took " + swThis.Elapsed.ToString());
            return usr;
        }

        /// <inheritdoc />
        public IEnumerable<AppUser> LoadUsers(IEnumerable<string> ids) {
            var swThis = Stopwatch.StartNew();
            if (ids == null) return new AppUser[0];
            var userIds = ids.ToArray();
            
            var selectQuery = $@"
select
    usr.Id as {nameof(AppUser.Id)},
    usr.UserName as {nameof(AppUser.UserName)},
    usr.FullName as {nameof(AppUser.FullName)},
    usr.Email as {nameof(AppUser.Email)},
    usr.CreatedUtc as {nameof(AppUser.UserCreatedUtc)},
    usr.UpdatedUtc as {nameof(AppUser.UserUpdatedUtc)},
    comp.Code as {nameof(AppUser.KpeiCompanyCode)},
    comp.Name as {nameof(AppUser.KpeiCompanyName)},
    comp.IsNonAB as {nameof(AppUser.KpeiCompanyIsNonAB)},
    comp.IsMI as {nameof(AppUser.KpeiCompanyIsMI)},
    comp.IsPEE as {nameof(AppUser.KpeiCompanyIsPEE)},
    comp.IsPPE as {nameof(AppUser.KpeiCompanyIsPPE)},
    comp.ManagerName as {nameof(AppUser.KpeiCompanyManagerName)},
    comp.MkbdGroupName as {nameof(AppUser.KpeiCompanyMkbdGroup)},
    comp.CreatedUtc as {nameof(AppUser.KpeiCompanyCreatedUtc)},
    comp.UpdatedUtc as {nameof(AppUser.KpeiCompanyUpdatedUtc)},
    rol.Name as {nameof(AppUser.KpeiMemberRole)}
from
    dbo.Users as usr
    join dbo.Companies as comp on usr.CompanyId = comp.Id
    join UsersRoles as usro on usr.Id = usro.UserId
    join Roles as rol on usro.RoleId = rol.Id
where
    usr.Status = 1 and
    usr.Id in @userIds

".Trim().Replace("@userIds", $"('{string.Join("','", userIds)}')");

            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.DataSlug == DataAlamatAknpDriver.SLUG);

            var sw = Stopwatch.StartNew();

            var comps = _compsvc.AllKpeiCompanies();

            Debug.WriteLine("LoadUsers: Get Kpei Company elapsed " + sw.Elapsed.ToString() + " @" + swThis.Elapsed.ToString());
            sw.Reset();
            var allLatest = comps.Select(x => drv.GetLatestActiveData(x.Code)).Where(x => x != null)
                .ToArray().AsQueryable();
            Debug.WriteLine("LoadUsers: Get Latest Active Data elapsed " + sw.Elapsed.ToString() + " @" + swThis.Elapsed.ToString());
            sw.Reset();
            var results = _memctx.Database.SqlQuery<AppUser>(selectQuery)
                .ToList();
            Debug.WriteLine("LoadUsers: Get AppUser elapsed " + sw.Elapsed.ToString() + " @" + swThis.Elapsed.ToString());
            for (int i = 0; i < results.Count(); i++) {
                var dbData = allLatest.FirstOrDefault(e => e.KodeAk == results[i].KpeiCompanyCode) as DataAlamat;
                if (dbData?.Email != null) {
                    results[i].Email = dbData.Email;
                }
            }
            Debug.WriteLine("finished LoadUsers elapsed " + swThis.Elapsed.ToString());
            return results;
        }

        private IEnumerable<AppUser> LoadEmail(string kodeAk){
            List<AppUser> email = new List<AppUser>();
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.DataSlug == DataAlamatAknpDriver.SLUG);
            var company = _compsvc.AllKpeiCompanies().FirstOrDefault(x => x.Code == kodeAk);
            var alamat = drv.GetLatestActiveData(kodeAk) as DataAlamat;
            if (!string.IsNullOrWhiteSpace(alamat?.Email))
            {
                email.Add(new AppUser
                {
                    KpeiCompanyName = company.Name,
                    Email = alamat.Email,
                    KpeiCompanyCode = company.Code,
                    UserName = company.Name
                });
            }

            return email;
        }

        /// <inheritdoc />
        public IEnumerable<AppUser> LoadUsers(string kodeAk, IEnumerable<string> roleNames = null) {
            var selectQuery = $@"

select
    usr.Id as {nameof(AppUser.Id)},
    usr.UserName as {nameof(AppUser.UserName)},
    usr.FullName as {nameof(AppUser.FullName)},
    usr.Email as {nameof(AppUser.Email)},
    usr.CreatedUtc as {nameof(AppUser.UserCreatedUtc)},
    usr.UpdatedUtc as {nameof(AppUser.UserUpdatedUtc)},
    comp.Code as {nameof(AppUser.KpeiCompanyCode)},
    comp.Name as {nameof(AppUser.KpeiCompanyName)},
    comp.IsNonAB as {nameof(AppUser.KpeiCompanyIsNonAB)},
    comp.IsMI as {nameof(AppUser.KpeiCompanyIsMI)},
    comp.IsPEE as {nameof(AppUser.KpeiCompanyIsPEE)},
    comp.IsPPE as {nameof(AppUser.KpeiCompanyIsPPE)},
    comp.ManagerName as {nameof(AppUser.KpeiCompanyManagerName)},
    comp.MkbdGroupName as {nameof(AppUser.KpeiCompanyMkbdGroup)},
    comp.CreatedUtc as {nameof(AppUser.KpeiCompanyCreatedUtc)},
    comp.UpdatedUtc as {nameof(AppUser.KpeiCompanyUpdatedUtc)},
    rol.Name as {nameof(AppUser.KpeiMemberRole)}
from
    dbo.Users as usr
    join dbo.Companies as comp on usr.CompanyId = comp.Id
    join UsersRoles as usro on usr.Id = usro.UserId
    join Roles as rol on usro.RoleId = rol.Id
where
    usr.Status = 1 and
    comp.Code = @kodeAk

".Trim();
            var rns = roleNames?.ToArray() ?? new string[0];
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.DataSlug == DataAlamatAknpDriver.SLUG);
            var comps = _compsvc.AllKpeiCompanies();
            var allLatest = comps.Select(x => drv.GetLatestActiveData(x.Code)).Where(x => x != null)
                .ToArray().AsQueryable();

            if (rns.Length == 0) {
                var results = _memctx.Database.SqlQuery<AppUser>(selectQuery, new SqlParameter("@kodeAk", kodeAk))
                    .ToList();
                for (int i = 0; i < results.Count(); i++) {
                    var dbData = allLatest.FirstOrDefault(e => e.KodeAk == results[i].KpeiCompanyCode) as DataAlamat;
                    if (dbData?.Email != null) {
                        results[i].Email = dbData.Email;
                    }
                }
                return results;
            } else {
                selectQuery += " and rol.Name in @roleNames".Replace("@roleNames", $"('{string.Join("','", rns)}')");

                var results = _memctx.Database.SqlQuery<AppUser>(selectQuery, new SqlParameter("@kodeAk", kodeAk))
                    .ToList();
                for (int i = 0; i < results.Count(); i++) {
                    var dbData = allLatest.FirstOrDefault(e => e.KodeAk == results[i].KpeiCompanyCode) as DataAlamat;
                    if (dbData?.Email != null) {
                        results[i].Email = dbData.Email;
                    }
                }
                return results;
            }
        }

        /// <inheritdoc />
        public AppUserRight GetUserRight(AppUser user, bool useCache = true) {
            return GetUserRight(user.Id, new[] { user.KpeiMemberRole }, user.KpeiCompanyCode, useCache);
        }

        /// <inheritdoc />
        public AppUserRight GetUserRight(string userId, string[] userRoleNames, string userCompanyCode,
            bool useCache = true) {
            AppUserRight result;
            if (useCache) {
                var cacheKey = $"AppUserRight({userId})";
                result = HttpContext.Current.Items[cacheKey] as AppUserRight;
                if (result != null) {
                    return result;
                }
            }

            var rightsData = _dbctx.UserRights.Where(x => x.MemberUserId == userId).AsNoTracking().ToArray();
            var guestAknpCode = _stgrepo.GlobalSetting().GuestAknpCode;
            result = new AppUserRight();
            foreach (var mod in CoreModules.IdAndNameTuples) {
                var modRight = new AppUserRightPerModule();

                if (userCompanyCode == guestAknpCode) {
                    result.ForModule[mod.Item1] = modRight;
                    continue;
                }

                var modRightData = rightsData.FirstOrDefault(x => x.ModuleId == mod.Item1);
                if (modRightData != null) {
                    if (userRoleNames.Contains("SuperAdmin") || userRoleNames.Contains("CompAdmin")) {
                        modRight.KpeiAdmin = true;
                        modRight.KpeiApprover = true;
                        modRight.KpeiModifier = true;
                        modRight.KpeiViewer = true;
                    } else {
                        modRight.KpeiAdmin = modRightData.KpeiAdmin;
                        modRight.KpeiApprover = modRightData.KpeiApprover;
                        modRight.KpeiModifier = modRightData.KpeiModifier;
                        modRight.KpeiViewer = modRightData.KpeiViewer;
                        modRight.NonKpeiModifier = modRightData.NonKpeiModifier;
                        modRight.NonKpeiViewer = modRightData.NonKpeiViewer;
                        modRight.NonKpeiViewerAsKpei = modRightData.NonKpeiViewerAsKpei;
                    }

                    if (userCompanyCode == "KPEI" || userCompanyCode == "ZZ") {
                        modRight.NonKpeiModifier = false;
                        modRight.NonKpeiViewer = false;
                        modRight.NonKpeiViewerAsKpei = false;
                    } else {
                        modRight.KpeiAdmin = false;
                        modRight.KpeiApprover = false;
                        modRight.KpeiModifier = false;
                        modRight.KpeiViewer = false;
                    }
                } else {
                    if (userCompanyCode == "KPEI" || userCompanyCode == "ZZ") {
                        if (userRoleNames.Contains("SuperAdmin") || userRoleNames.Contains("CompAdmin")) {
                            modRight.KpeiAdmin = true;
                            modRight.KpeiApprover = true;
                            modRight.KpeiModifier = true;
                            modRight.KpeiViewer = true;
                        } else {
                            modRight.KpeiViewer = true;
                        }
                    } else {
                        modRight.NonKpeiViewer = true;
                        modRight.NonKpeiModifier = true;
                    }
                }

                result.ForModule[mod.Item1] = modRight;
            }

            return result;
        }

        public void SendNotifNewRegistrasi(string destination, string url) {

            var templateKpei = aknpEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_NEW_KPEI);
            var templateGuest = aknpEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_NEW_GUEST);

            SendEmailToKpeiAndAknp(
                null,
                url,
                templateKpei
            );
            if (templateGuest?.Header?.Header != null && templateGuest?.Template?.Value != null) {
                templateGuest.Template.Value = EmailReplaceKey.ReplaceUser(templateGuest.Template.Value, "Calon AK / Partisipan", "Perusahaan Calon AK / Partisipan");
                _emailsvc.SendAsync(
                    destination,
                    templateGuest.Header.Header,
                    templateGuest.Template.Value
                    );
            }
        }


        public void SendNotifRejectGuestRegistrasi(string destination, string url, string reason) {

            var templateKpei = aknpEmail?.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_REJECT_KPEI);
            var templateGuest = aknpEmail?.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_REJECT_GUEST);

            if (templateKpei?.Template?.Value != null) {
                templateKpei.Template.Value = EmailReplaceKey.ReplaceReason(templateKpei.Template.Value, reason);
            }
            SendEmailToKpeiAndAknp(
                null,
                url,
                templateKpei
            );

            if (templateGuest?.Template?.Value != null) {
                templateGuest.Template.Value = EmailReplaceKey.ReplaceReason(templateGuest.Template.Value, reason);
            }
            _emailsvc.SendAsync(
                destination,
                templateGuest.Header?.Header,
                templateGuest.Template?.Value
                );
        }

        private void SendEmailToKpeiAndAknp(string kodeAk, string url, EmailSetting templateKpei, EmailSetting templateAknp = null) {
            if (templateKpei?.Template?.Value != null && url != null) {
                templateKpei.Template.Value = EmailReplaceKey.ReplaceLink(templateKpei.Template.Value, url);
            }
            var globalSetting = _stgrepo.GlobalSetting();
            if (templateKpei?.Header?.Header != null && templateKpei?.Template?.Value != null) {
                foreach (var email in globalSetting.KkeGroupEmails) {
                    _emailsvc.SendAsync(
                        email,
                        templateKpei.Header.Header,
                        templateKpei.Template.Value);
                }
            }

            if (kodeAk != null && templateAknp != null) {
                if (url != null && templateAknp.Template != null) {
                    templateAknp.Template.Value = EmailReplaceKey.ReplaceLink(templateAknp.Template.Value, url);
                }
                var grupEmailAknp = LoadEmail(kodeAk).Select(x => new { x.Email, x.UserName, x.KpeiCompanyName }).ToList();
                foreach (var email in grupEmailAknp) {
                    var editedTemplate = EmailReplaceKey.ReplaceUser(templateAknp.Template.Value, email.UserName, email.KpeiCompanyName);
                    if (templateAknp?.Header?.Header != null && templateAknp?.Template?.Value != null) {
                        _emailsvc.SendAsync(
                        email.Email,
                        templateAknp.Header.Header,
                        editedTemplate);
                    }
                }
            }
        }

        #region Aknp
        
        public void SendNotifNewAknp(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                aknpEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_NEW_KPEI),
                aknpEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_NEW_AKNP)
                );
        }

        public void SendNotifUpdateAknp(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                aknpEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_UPDATE_KPEI),
                aknpEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_UPDATE_AKNP)
                );
        }

        public void SendNotifApproveAknp(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                aknpEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_ACTION_KPEI),
                aknpEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_ACTION_AKNP)
                );
        }

        public void SendNotifDoneAknp(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                aknpEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_ACTION_KPEI),
                aknpEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_ACTION_AKNP)
                );
        }

        public void SendNotifRejectAknp(string kodeAk, string url, string reason) {
            var templateKpei = aknpEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_REJECT_KPEI);
            var templateAknp = aknpEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_DATA_REJECT_AKNP);
            if (templateAknp?.Template?.Value != null && reason != null) {
                templateAknp.Template.Value = EmailReplaceKey.ReplaceReason(templateAknp.Template.Value, reason);
            }
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                templateKpei,
                templateAknp
                );
        }

        #endregion

        #region Pincode

        public void SendNotifNewPincode(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                pincodeEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_PINCODE_NEW_KPEI),
                pincodeEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_PINCODE_NEW_AKNP)
                );
        }
        
        public void SendNotifActionPincode(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                pincodeEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_PINCODE_ACTION_KPEI),
                pincodeEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_PINCODE_ACTION_AKNP)
                );
        }

        public void SendNotifRejectPincode(string kodeAk, string url, string reason) {
            var templateKpei = pincodeEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_PINCODE_REJECT_KPEI);
            var templateAknp = pincodeEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_PINCODE_REJECT_AKNP);
            if (templateAknp?.Template?.Value != null && reason != null) {
                templateAknp.Template.Value = EmailReplaceKey.ReplaceReason(templateAknp.Template.Value, reason);
            }
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                templateKpei,
                templateAknp
                );
        }

        public void SendNewPincodeEmail(SendPincodeForm form, string kodeAK, string url, string newUsername, string newPassword, string urlTerkait) {
            var templateAknp = pincodeEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_PINCODE_SEND_AKNP);
            var templateDireksi = pincodeEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_PINCODE_SEND_DIREKSI);
            if (templateDireksi != null) {
                templateDireksi.Template.Value = EmailReplaceKey.ReplacePincode(templateDireksi.Template.Value, newUsername, newPassword, urlTerkait);
                if (url != null) {
                    templateDireksi.Template.Value = EmailReplaceKey.ReplaceLink(templateDireksi.Template.Value, url);
                }
            }
            
            SendEmailToKpeiAndAknp(
                kodeAK,
                url,
                null,
                templateAknp
                );

            if (form.EmailDireksi?.Count > 0 && templateDireksi?.Header?.Header != null && templateDireksi?.Template?.Value != null) {
                foreach (var email in form.EmailDireksi) {
                    _emailsvc.SendAsync(email, templateDireksi.Header.Header, templateDireksi.Template.Value);
                }
            }

            if (form.EmailEkstra?.Count > 0 && templateDireksi?.Header?.Header != null && templateDireksi?.Template?.Value != null) {
                foreach (var email in form.EmailEkstra) {
                    _emailsvc.SendAsync(email, templateDireksi.Header.Header, templateDireksi.Template.Value);
                }
            }
        }

        #endregion

        #region Laporan Keuangan

        public void SendNotifNewLapKeu(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                keuanganEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_KEUANGAN_NEW_KPEI),
                keuanganEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_KEUANGAN_NEW_AKNP)
                );
        }

        public void SendNotifUpdateLapKeu(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                keuanganEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_KEUANGAN_UPDATE_KPEI),
                keuanganEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_KEUANGAN_UPDATE_AKNP)
                );
        }

        public void SendNotifActionLapKeu(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                keuanganEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_KEUANGAN_ACTION_KPEI),
                keuanganEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_KEUANGAN_ACTION_AKNP)
                );
        }

        public void SendNotifRejectLapKeu(string kodeAk, string url, string reason) {
            var templateKpei = keuanganEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_KEUANGAN_REJECT_KPEI);
            var templateAknp = keuanganEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_KEUANGAN_REJECT_AKNP);
            if (templateAknp?.Template?.Value != null && reason != null) {
                templateAknp.Template.Value = EmailReplaceKey.ReplaceReason(templateAknp.Template.Value, reason);
            }
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                templateKpei,
                templateAknp
                );
        }

        #endregion

        #region m-CLEARS

        public void SendNotifNewMclears(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                mclearsEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_MCLEARS_NEW_KPEI),
                mclearsEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_MCLEARS_NEW_AKNP)
                );
        }

        public void SendNotifUpdateMclears(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                mclearsEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_MCLEARS_UPDATE_KPEI),
                mclearsEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_MCLEARS_UPDATE_AKNP)
                );
        }

        public void SendNotifActionMclears(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                mclearsEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_MCLEARS_ACTION_KPEI),
                mclearsEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_MCLEARS_ACTION_AKNP)
                );
        }

        public void SendNotifNewAdministrasiKeanggotaan(string kodeAk, string url)
        {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                administrasiKeanggotaanEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_ADMINISTRASI_KEANGGOTAAN_NEW_KPEI),
                administrasiKeanggotaanEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_ADMINISTRASI_KEANGGOTAAN_NEW_AKNP)
            );
        }

        public void SendNotifUpdateAdministrasiKeanggotaan(string kodeAk, string url)
        {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                administrasiKeanggotaanEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_ADMINISTRASI_KEANGGOTAAN_UPDATE_KPEI),
                administrasiKeanggotaanEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_ADMINISTRASI_KEANGGOTAAN_UPDATE_AKNP)
            );
        }

        public void SendNotifApproveAdministrasiKeanggotaan(string kodeAk, string url)
        {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                null,
                administrasiKeanggotaanEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_ADMINISTRASI_KEANGGOTAAN_ACTION_AKNP)
            );
        }

        public void SendNotifRejectAdministrasiKeanggotaan(string kodeAk, string url, string reason)
        {
            var templateAknp = administrasiKeanggotaanEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_ADMINISTRASI_KEANGGOTAAN_REJECT_AKNP);
            if (templateAknp?.Template?.Value != null && reason != null) {
                templateAknp.Template.Value = EmailReplaceKey.ReplaceReason(templateAknp.Template.Value, reason);
            }
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                null,
                templateAknp
            );
        }

        #endregion

        #region Sosialisasi dan Pelatihan (Event)

        public void SendNotifNewSospel(string url) {
            SendEmailToKpeiAndAknp(
                null,
                url,
                sospelEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_EVENT_NEW_KPEI)
                );
        }

        public void SendNotifUpdateSospel(List<string> kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                null,
                url,
                sospelEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_EVENT_UPDATE_KPEI)
                );

            if (kodeAk?.Count > 0) {
                var templateAknp = sospelEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_EVENT_UPDATE_AKNP);
                if (url != null) {
                    templateAknp.Template.Value = EmailReplaceKey.ReplaceLink(templateAknp.Template.Value, url);
                }

                if (templateAknp?.Header?.Header != null && templateAknp?.Template?.Value != null) { 
                    foreach (var code in kodeAk) {
                        var grupEmailAknp = LoadEmail(code).Select(x => x.Email).ToList();
                        foreach (var email in grupEmailAknp) {
                            _emailsvc.SendAsync(
                                email,
                                templateAknp.Header.Header,
                                templateAknp.Template.Value);
                        }
                    }
                }
            }

        }

        public void SendNotifNewRegistrationSospel(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                sospelEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_EVENT_REGISTER_KPEI),
                sospelEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_EVENT_REGISTER_AKNP)
                );
        }

        public void SendNotifUpdateRegistrationSospel(string kodeAk, string url) {
            SendEmailToKpeiAndAknp(
                kodeAk,
                url,
                sospelEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_EVENT_UPDATEREG_KPEI),
                sospelEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_EVENT_UPDATEREG_AKNP)
                );
        }

        #endregion

        #region Survei

        //Send to KPEI only (for Survei)
        public void SendNotifNewSurvei(string url) {
            SendEmailToKpeiAndAknp(
                null,
                url,
                surveiEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_SURVEI_NEW_KPEI)
                );
        }

        //Send to KPEI only (for Survei)
        public void SendNotifUpdateSurvei(string url) {
            SendEmailToKpeiAndAknp(
                null,
                url,
                surveiEmail.FirstOrDefault(e => e.Template.Field == SettingFields.EMAIL_SURVEI_UPDATE_KPEI)
                );
        }

        #endregion

        #region Reminder

        //public void SendReminderKeuangan(string destination, string usr = null, string comp = null) {
        //    var email = _stgrepo.GetEmailByField(SettingFields.HEADER_KEUANGAN_REMIND_AKNP);
        //    if (email.Template.Value != null && usr != null && comp != null) {
        //        email.Template.Value = EmailReplaceKey.ReplaceUser(email.Template.Value, usr, comp);
        //    }
        //    _emailsvc.SendAsync(
        //        destination,
        //        email.Header.Header,
        //        email.Template.Value);
        //}

        //public void SendReminderEvent(string destination, string usr = null, string comp = null) {
        //    var email = _stgrepo.GetEmailByField(SettingFields.HEADER_EVENT_REMIND_AKNP);
        //    if (email.Template.Value != null && usr != null && comp != null) {
        //        email.Template.Value = EmailReplaceKey.ReplaceUser(email.Template.Value, usr, comp);
        //    }
        //    _emailsvc.SendAsync(
        //        destination, 
        //        email.Header.Header,
        //        email.Template.Value);
        //}

        public void SendEmailToManyAknp(IEnumerable<string> codes, string header, string body) {
            var debugAddress = _stgrepo.GlobalSetting().EmailDebugAddress;
            foreach (var code in codes) {
                var grupEmailAknp = LoadEmail(code).Select(x => new { x.Email, x.UserName, x.KpeiCompanyName }).ToList();
                if (debugMode && !string.IsNullOrWhiteSpace(debugAddress)) {
                    foreach (var target in grupEmailAknp) {
                        if (!string.IsNullOrWhiteSpace(body)) {
                            body = EmailReplaceKey.ReplaceUser(body, target.UserName, target.KpeiCompanyName);
                        }
                        _emailsvc.SendAsync(
                            debugAddress,
                            header,
                            body
                        );
                    }
                }
                if (!debugMode) {
                    foreach (var target in grupEmailAknp) {
                        if (!string.IsNullOrWhiteSpace(body)) {
                            body = EmailReplaceKey.ReplaceUser(body, target.UserName, target.KpeiCompanyName);
                        }
                        _emailsvc.SendAsync(
                            target.Email,
                            header,
                            body
                        );
                    }
                }
            }

        }

        #endregion
    }
}