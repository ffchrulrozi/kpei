﻿using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.UserIdentity {
    public class AppUserRight {
        private IDictionary<string, AppUserRightPerModule> _forModule;
        public IDictionary<string, AppUserRightPerModule> ForModule {
            get { return _forModule ?? (_forModule = new Dictionary<string, AppUserRightPerModule>()); }
            set { _forModule = value; }
        }
    }

    public class AppUserRightPerModule {
        public bool KpeiApprover { get; set; }
        public bool KpeiModifier { get; set; }
        public bool KpeiViewer { get; set; }
        public bool KpeiAdmin { get; set; }
        public bool NonKpeiModifier { get; set; }
        public bool NonKpeiViewer { get; set; }
        public bool NonKpeiViewerAsKpei { get; set; }
    }
}