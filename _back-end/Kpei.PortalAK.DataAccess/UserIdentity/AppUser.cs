﻿using System;
using System.Security.Principal;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using System.Diagnostics;

namespace Kpei.PortalAK.DataAccess.UserIdentity {
    public class AppUser {
        private DateTime? _kpeiCompanyCreatedUtc;
        private DateTime? _kpeiCompanyUpdatedUtc;
        private DateTime? _userCreatedUtc;
        private DateTime? _userUpdatedUtc;

        public string Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime? UserCreatedUtc {
            get { return _userCreatedUtc; }
            set {
                _userCreatedUtc =
                    value.HasValue ? DateTime.SpecifyKind(value.Value, DateTimeKind.Utc) : (DateTime?)null;
            }
        }
        public DateTime? UserUpdatedUtc {
            get { return _userUpdatedUtc; }
            set {
                _userUpdatedUtc =
                    value.HasValue ? DateTime.SpecifyKind(value.Value, DateTimeKind.Utc) : (DateTime?)null;
            }
        }
        public string KpeiCompanyName { get; set; }
        public string KpeiCompanyCode { get; set; }
        public bool KpeiCompanyIsNonAB { get; set; }
        public bool KpeiCompanyIsPPE { get; set; }
        public bool KpeiCompanyIsPEE { get; set; }
        public bool KpeiCompanyIsMI { get; set; }
        public string KpeiCompanyManagerName { get; set; }
        public string KpeiCompanyMkbdGroup { get; set; }
        public DateTime? KpeiCompanyCreatedUtc {
            get { return _kpeiCompanyCreatedUtc; }
            set {
                _kpeiCompanyCreatedUtc =
                    value.HasValue ? DateTime.SpecifyKind(value.Value, DateTimeKind.Utc) : (DateTime?)null;
            }
        }
        public DateTime? KpeiCompanyUpdatedUtc {
            get { return _kpeiCompanyUpdatedUtc; }
            set {
                _kpeiCompanyUpdatedUtc =
                    value.HasValue ? DateTime.SpecifyKind(value.Value, DateTimeKind.Utc) : (DateTime?)null;
            }
        }
        public string KpeiMemberRole { get; set; }

        public bool IsKpei {
            get {
                var sw = Stopwatch.StartNew();
                var result = KpeiCompanyCode == "KPEI" || KpeiCompanyCode == "ZZ";
                Debug.WriteLine("IsKpei took " + sw.Elapsed.ToString());
                return result;
                }
        }

        public bool IsAknp => !IsKpei && !IsGuestAknp;

        public bool IsSuperAdmin => KpeiMemberRole.Contains("SuperAdmin") || KpeiMemberRole.Contains("CompAdmin");

        public bool IsGuestAknp {
            get {
                var stgsvc = DepResolver.Instance.GetService<ISettingRepository>();
                var guestCode = stgsvc.GlobalSetting().GuestAknpCode;
                return KpeiCompanyCode == guestCode;
            }
        }

        public AppUserRight GetRight(bool useCache = true) {
            var usvc = DepResolver.Instance.GetService<IUserService>();
            return usvc.GetUserRight(this, useCache);
        }

        public bool KpeiAdminRightFor(string moduleId, bool useCache = true) {
            var right = GetRight(useCache);
            return right?.ForModule[moduleId].KpeiAdmin == true;
        }

        public bool KpeiViewerRightFor(string moduleId, bool useCache = true) {
            var right = GetRight(useCache);
            return right?.ForModule[moduleId].KpeiViewer == true ||
                   right?.ForModule[moduleId].NonKpeiViewerAsKpei == true;
        }

        public bool KpeiModifierRightFor(string moduleId, bool useCache = true) {
            var right = GetRight(useCache);
            return right?.ForModule[moduleId].KpeiModifier == true;
        }

        public bool KpeiApproverRightFor(string moduleId, bool useCache = true) {
            var right = GetRight(useCache);
            return right?.ForModule[moduleId].KpeiApprover == true;
        }

        public bool NonKpeiViewerRightFor(string moduleId, bool useCache = true) {
            var right = GetRight(useCache);
            return right?.ForModule[moduleId].NonKpeiViewer == true;
        }

        public bool NonKpeiModifierRightFor(string moduleId, bool useCache = true) {
            var right = GetRight(useCache);
            return right?.ForModule[moduleId].NonKpeiModifier == true;
        }

        public bool NonKpeiViewerAsKpei(string moduleId, bool useCache = true) {
            var right = GetRight(useCache);
            return right?.ForModule[moduleId].NonKpeiViewerAsKpei == true;
        }

        public static AppUser CreateFromIdentity(IIdentity identity) {
            var usvc = DepResolver.Instance.GetService<IUserService>();
            return usvc.LoadUser(identity);
        }
    }

    public static class UserIdentityExtensions {
        public static AppUser GetAppUser(this IIdentity ident) {
            return AppUser.CreateFromIdentity(ident);
        }
    }
}