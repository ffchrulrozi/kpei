﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Attributes;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.Validators;

namespace Kpei.PortalAK.DataAccess.ModTradingMember.Forms
{
    public class TradingMemberForm
    {
        public string NamaPerusahaan { get; set; }
        public string KodePerusahaan { get; set; }

        public string TipePerusahaan { get; set; }

    }
}
