﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;

namespace Kpei.PortalAK.DataAccess.ModTradingMember.Forms
{
    public class ChangeRequestDttRequestForm : DttRequestForm
    {
        public string KodePerusahaan { get; set; }
        public string TipePerusahaan { get; set; }
    }
}
