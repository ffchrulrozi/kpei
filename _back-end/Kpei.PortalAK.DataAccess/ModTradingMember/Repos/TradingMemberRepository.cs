﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModTradingMember.Forms;

namespace Kpei.PortalAK.DataAccess.ModTradingMember.Repos
{
    public class TradingMemberRepository : ITradingMemberRepository
    {
        private readonly AppDbContext _context;
        public TradingMemberRepository(AppDbContext _context)
        {
            this._context = _context;
        }

        public DataTM Get(string kodePerusahaan) => _context.DataTM.FirstOrDefault(x => x.KodePerusahaan == kodePerusahaan);

        public DataTM Create(TradingMemberForm form)
        {
            DataTM data = new DataTM
            {
                KodePerusahaan = form.KodePerusahaan,
                NamaPerusahaan = form.NamaPerusahaan,
                TipePerusahaan = form.TipePerusahaan,
            };

            _context.DataTM.Add(data);
            _context.SaveChanges();
            return data;
        }

        public bool Delete(string id)
        {
            DataTM data = _context.DataTM.FirstOrDefault(x => x.Id == id);
            if (data != null)
            {
                _context.Entry(data).State = EntityState.Deleted;
                _context.SaveChanges();
                return true;
            }

            return false;
        }

        public PaginatedResult<DataTM> ListDtt(ChangeRequestDttRequestForm req)
        {
            var result = new PaginatedResult<DataTM>();

            var allQ = _context.DataTM.AsQueryable();
            var filQ = _context.DataTM.AsQueryable();

            if (!string.IsNullOrWhiteSpace(req.TipePerusahaan)) {
                filQ = filQ.Where(x => x.TipePerusahaan == req.TipePerusahaan);
            }

            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                var kws = Regex.Split(req.search.value, @"\s+");
                if (kws.Length > 0) {
                    filQ = filQ.Where(x =>
                        kws.All(kw => x.KodePerusahaan.Contains(kw))
                    );
                }
            }

            if (req.firstOrderColumn?.name == nameof(ChangeRequestDttRequestForm.KodePerusahaan)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.KodePerusahaan)
                    : filQ.OrderBy(x => x.KodePerusahaan);
            }else if (req.firstOrderColumn?.name == nameof(ChangeRequestDttRequestForm.TipePerusahaan)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.TipePerusahaan)
                    : filQ.OrderBy(x => x.TipePerusahaan);
            }

            result.Initialize(allQ, filQ, req.start, req.length);

            return result;
        }
   }
}
