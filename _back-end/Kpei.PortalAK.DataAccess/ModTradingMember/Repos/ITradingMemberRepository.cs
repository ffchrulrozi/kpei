﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModTradingMember.Forms;

namespace Kpei.PortalAK.DataAccess.ModTradingMember.Repos
{
    public interface ITradingMemberRepository
    {
        DataTM Get(string kodePerusahaan);
        DataTM Create(TradingMemberForm form);
        bool Delete(string id);

        PaginatedResult<DataTM> ListDtt(ChangeRequestDttRequestForm req);
    }
}
