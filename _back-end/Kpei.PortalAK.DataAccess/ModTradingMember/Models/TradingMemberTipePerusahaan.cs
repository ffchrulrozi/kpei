﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Models
{
    public class TradingMemberTipePerusahaan
    {
        public string Name { get; }
        public string Label { get; }
        private TradingMemberTipePerusahaan(string name, string label)
        {
            Name = name;
            Label = label;
        }

        public static IEnumerable<TradingMemberTipePerusahaan> All() {
            yield return AK;
            yield return NonAK;
        }

        public static readonly TradingMemberTipePerusahaan AK = new TradingMemberTipePerusahaan("AK&P", "AK&P");
        public static readonly TradingMemberTipePerusahaan NonAK = new TradingMemberTipePerusahaan("Non AK&P", "Non AK&P");
    }
}
