﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Forms;
using Kpei.PortalAK.DataAccess.Core;

namespace Kpei.PortalAK.DataAccess.ModSosialisasi.Repos {
    public interface IEventRepository {
        IEnumerable<Event> Event { get; }
        IEnumerable<EventPembicara> EventPembicara { get; }
        IEnumerable<EventPeserta> EventPeserta { get; }

        Event Get(string id);
        Event Create(CreateEventForm form);
        Event Update(UpdateEventForm form);
        void Delete(string id);

        EventPembicara GetPembicara(string id);
        List<EventPembicara> GetPembicaraFromEvent(string id);
        EventPembicara CreatePembicara(CreateEventPembicaraForm form);
        EventPembicara UpdatePembicara(UpdateEventPembicaraForm form);

        EventPeserta GetPeserta(string id);
        IEnumerable<EventPeserta> GetAllPeserta();
        IEnumerable<EventPeserta> GetPesertaFromEvent(string id);
        IEnumerable<EventPeserta> GetPesertaFromKodeAk(string kodeAk);
        EventPeserta CreatePeserta(CreateEventPesertaForm form);
        EventPeserta UpdatePeserta(UpdateEventPesertaForm form);
        bool ChangeHadir(string id, bool hadir);
        
        PaginatedResult<Event> List(EventDttRequestForm req);
        PaginatedResult<EventPeserta> ListPeserta(PesertaDttRequestForm req);
    }
}
