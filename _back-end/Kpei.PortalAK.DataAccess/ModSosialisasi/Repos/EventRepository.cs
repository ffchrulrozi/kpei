﻿using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Forms;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Kpei.PortalAK.DataAccess.ModSosialisasi.Repos {
    public class EventRepository : IEventRepository {
        private readonly AppDbContext _context;

        public EventRepository(AppDbContext context) {
            _context = context;
        }

        public IEnumerable<Event> Event => _context.Event;

        public IEnumerable<EventPembicara> EventPembicara => _context.EventPembicara;

        public IEnumerable<EventPeserta> EventPeserta => _context.EventPeserta;

        #region EventHandler
        public Event Create(CreateEventForm form) {
            var newEvent = new Event {
                Judul = form.Judul,
                Highlight = form.Highlight,
                Tempat = form.Tempat,
                TglPelaksanaanStartUtc = form.TglPelaksanaanStartUtc,
                TglPelaksanaanEndUtc = form.TglPelaksanaanEndUtc,
                TglDaftarBukaUtc = form.TglDaftarBukaUtc,
                TglDaftarTutupUtc = form.TglDaftarTutupUtc,
                JenisPelatihan = form.JenisPelatihan,
                KapasitasPerAK = form.KapasitasPerAK,
                KapasitasTotal = form.KapasitasTotal,
                BrosurFileUrl = form.BrosurFileUrl,
                MateriFileUrl = form.MateriFileUrl,
                StatusEvent = form.StatusEvent
            };
            _context.Event.Add(newEvent);
            _context.SaveChanges();
            return newEvent;
        }

        public Event Get(string id) {
            return _context.Event.FirstOrDefault(x => x.Id == id);
        }

        public Event Update(UpdateEventForm form) {
            var dataEvent = Get(form.Id);
            dataEvent.Judul = form.Judul;
            dataEvent.Highlight = form.Highlight;
            dataEvent.Tempat = form.Tempat;
            dataEvent.TglPelaksanaanStartUtc = form.TglPelaksanaanStartUtc;
            dataEvent.TglPelaksanaanEndUtc = form.TglPelaksanaanEndUtc;
            dataEvent.TglDaftarBukaUtc = form.TglDaftarBukaUtc;
            dataEvent.TglDaftarTutupUtc = form.TglDaftarTutupUtc;
            dataEvent.JenisPelatihan = form.JenisPelatihan;
            dataEvent.KapasitasPerAK = form.KapasitasPerAK;
            dataEvent.KapasitasTotal = form.KapasitasTotal;
            dataEvent.BrosurFileUrl = form.BrosurFileUrl;
            dataEvent.MateriFileUrl = form.MateriFileUrl;
            dataEvent.StatusEvent = form.StatusEvent;
            _context.SaveChanges();
            return dataEvent;
        }

        public void Delete(string id) {
            List<EventPembicara> listPembicara = GetPembicaraFromEvent(id);
            for (int i = 0; i < listPembicara.Count; i++) {
                _context.EventPembicara.Remove(GetPembicara(listPembicara[i].Id));
            }
            _context.Event.Remove(Get(id));
            _context.SaveChanges();
        } 
        #endregion

        #region EventPembicaraHandler
        public EventPembicara GetPembicara(string id) {
            return _context.EventPembicara.FirstOrDefault(e => e.Id == id);
        }

        public List<EventPembicara> GetPembicaraFromEvent(string id) {
            return _context.EventPembicara.Where(e => e.EventId == id).ToList();
        }

        public EventPembicara CreatePembicara(CreateEventPembicaraForm form) {
            var newPembicara = new EventPembicara {
                EventId = form.EventId,
                Nama = form.Nama,
                Jabatan = form.Jabatan,
                Email = form.Email,
                Telp = form.Telp
            };
            _context.EventPembicara.Add(newPembicara);
            _context.SaveChanges();
            return newPembicara;
        }

        public EventPembicara UpdatePembicara(UpdateEventPembicaraForm form) {
            var emptyData = false;
            if (form.Nama == null && form.Jabatan == null && form.Email == null && form.Telp == null) {
                emptyData = true;
            }
            var pembicara = GetPembicara(form.Id);
            if (pembicara != null) {
                if (emptyData) {
                    DeletePembicara(form.Id);
                } else {
                    pembicara.EventId = form.EventId;
                    pembicara.Nama = form.Nama;
                    pembicara.Jabatan = form.Jabatan;
                    pembicara.Email = form.Email;
                    pembicara.Telp = form.Telp;
                }
            } else {
                if (!emptyData) {
                    CreatePembicara(new CreateEventPembicaraForm {
                        EventId = form.EventId,
                        Nama = form.Nama,
                        Jabatan = form.Jabatan,
                        Email = form.Email,
                        Telp = form.Telp
                    });
                }
            }
            _context.SaveChanges();
            return pembicara;
        }

        private void DeletePembicara(string id) {
            _context.EventPembicara.Remove(GetPembicara(id));
        } 
        #endregion

        #region EventPesertaHandler
        public EventPeserta GetPeserta(string id) {
            return _context.EventPeserta.FirstOrDefault(e => e.Id == id);
        }

        public IEnumerable<EventPeserta> GetAllPeserta() {
            return _context.EventPeserta.ToList();
        }

        public IEnumerable<EventPeserta> GetPesertaFromEvent(string id) {
            return _context.EventPeserta.Where(e => e.EventId == id).ToList();
        }

        public IEnumerable<EventPeserta> GetPesertaFromKodeAk(string kodeAk) {
            return _context.EventPeserta.Where(e => e.KodeAK == kodeAk).ToList();
        }

        public EventPeserta CreatePeserta(CreateEventPesertaForm form) {
            var newPeserta = new EventPeserta {
                EventId = form.EventId,
                KodeAK = form.KodeAK,
                Nama = form.Nama,
                Jabatan = form.Jabatan,
                Email = form.Email,
                Telp = form.Telp,
                Hadir = form.Hadir
            };
            _context.EventPeserta.Add(newPeserta);
            _context.SaveChanges();
            return newPeserta;
        }

        public EventPeserta UpdatePeserta(UpdateEventPesertaForm form) {
            bool emptyData = false;
            if (form.Nama == null && form.Jabatan == null && form.Email == null && form.Telp == null) {
                emptyData = true;
            }
            var oldPeserta = GetPeserta(form.Id);
            if (oldPeserta == null) {
                if (!emptyData) {
                    CreatePeserta(new CreateEventPesertaForm {
                        EventId = form.EventId,
                        KodeAK = form.KodeAK,
                        Nama = form.Nama,
                        Jabatan = form.Jabatan,
                        Email = form.Email,
                        Telp = form.Telp,
                        Hadir = form.Hadir
                    });
                }
            } else {
                if (!emptyData) {
                    oldPeserta.EventId = form.EventId;
                    oldPeserta.KodeAK = form.KodeAK;
                    oldPeserta.Nama = form.Nama;
                    oldPeserta.Jabatan = form.Jabatan;
                    oldPeserta.Email = form.Email;
                    oldPeserta.Telp = form.Telp;
                    oldPeserta.Hadir = form.Hadir;
                    _context.SaveChanges();
                } else {
                    DeletePeserta(form.Id);
                }
            }
            return oldPeserta;
        }

        private void DeletePeserta(string id) {
            _context.EventPeserta.Remove(GetPeserta(id));
            _context.SaveChanges();
        }

        public bool ChangeHadir(string id, bool hadir) {
            var peserta = GetPeserta(id);
            peserta.Hadir = hadir;
            _context.SaveChanges();
            return peserta.Hadir;
        } 
        #endregion


        #region DttPaginatedResult
        public PaginatedResult<Event> List(EventDttRequestForm req) {
            var result = new PaginatedResult<Event>();

            var allQ = _context.Event.AsQueryable();
            var filQ = _context.Event.AsQueryable();

            if (!string.IsNullOrWhiteSpace(req.KodeAK)) {
                var registeredAcara = GetPesertaFromKodeAk(req.KodeAK).Select(e => e.EventId).Distinct();
                filQ = filQ.Where(e => registeredAcara.Contains(e.Id));
            }

            if (!string.IsNullOrWhiteSpace(req.StatusExpiration)) {
                var utcNow = DateTime.UtcNow;
                if (req.StatusExpiration == EventExpirationStatus.DRAFT_STATUS.Name) {
                    filQ = filQ.Where(e => e.StatusEvent == EventStatus.DRAFT_STATUS.Name);
                } else {
                    filQ = filQ.Where(e => e.StatusEvent == EventStatus.PUBLISHED_STATUS.Name);
                }

                if (req.StatusExpiration == EventExpirationStatus.AWAIT_OPEN_STATUS.Name) {
                    filQ = filQ.Where(e => utcNow < e.TglDaftarBukaUtc);
                } else if (req.StatusExpiration == EventExpirationStatus.REGIS_OPEN_STATUS.Name) {
                    filQ = filQ.Where(e => utcNow < e.TglDaftarTutupUtc && utcNow >= e.TglDaftarBukaUtc);
                } else if (req.StatusExpiration == EventExpirationStatus.REGIS_CLOSED_STATUS.Name) {
                    filQ = filQ.Where(e => utcNow < e.TglPelaksanaanStartUtc && utcNow > e.TglDaftarTutupUtc);
                } else if (req.StatusExpiration == EventExpirationStatus.EVENT_RUNNING_STATUS.Name) {
                    filQ = filQ.Where(e => utcNow < e.TglPelaksanaanEndUtc && utcNow >= e.TglPelaksanaanStartUtc);
                } else if (req.StatusExpiration == EventExpirationStatus.DONE_STATUS.Name) {
                    filQ = filQ.Where(e => utcNow >= e.TglPelaksanaanStartUtc);
                }
            }

            if (req.FromDateTime != null) {
                var fromUtc = req.FromDateTime.Value.Date.ToUniversalTime();
                filQ = filQ.Where(e => e.TglPelaksanaanStartUtc >= fromUtc);
            } 
            if (req.ToDateTime != null) {
                var toUtc = req.ToDateTime.Value.Date.AddDays(1).ToUniversalTime();
                filQ = filQ.Where(e => e.TglPelaksanaanStartUtc <= toUtc);
            }

            if (!string.IsNullOrWhiteSpace(req.JenisPelatihan)) {
                filQ = filQ.Where(e => e.JenisPelatihan == req.JenisPelatihan);
            }
            
            if (!string.IsNullOrWhiteSpace(req.Status)) {
                filQ = filQ.Where(e => e.StatusEvent == req.Status);
            }

            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                var kws = Regex.Split(req.search.value, @"\s+");
                if (kws.Length > 0) {
                    filQ = filQ.Where(x =>
                            kws.All(kw => x.StatusEvent.Contains(kw))
                            || kws.All(kw => x.Judul.Contains(kw))
                            || kws.All(kw => x.JenisPelatihan.Contains(kw))
                        );
                }
            }

            if (req.firstOrderColumn?.name == nameof(DbModels.Event.Judul)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.Judul)
                    : filQ.OrderBy(x => x.Judul);
            } else if (req.firstOrderColumn?.name == nameof(DbModels.Event.JenisPelatihan)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.JenisPelatihan)
                    : filQ.OrderBy(x => x.JenisPelatihan);
            } else if (req.firstOrderColumn?.name == nameof(DbModels.Event.KapasitasTotal)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.KapasitasTotal)
                    : filQ.OrderBy(x => x.KapasitasTotal);
            } else if (req.firstOrderColumn?.name == nameof(DbModels.Event.StatusEvent)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.StatusEvent)
                    : filQ.OrderBy(x => x.StatusEvent);
            } else {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.CreatedUtc)
                    : filQ.OrderBy(x => x.CreatedUtc);
            }
            result.Initialize(allQ, filQ, req.start, req.length);
            return result;
        }

        public PaginatedResult<EventPeserta> ListPeserta(PesertaDttRequestForm req) {
            var result = new PaginatedResult<EventPeserta>();

            var allQ = _context.EventPeserta.AsQueryable();
            var filQ = _context.EventPeserta.AsQueryable();

            if (!string.IsNullOrWhiteSpace(req.KodeAK)) {
                filQ = filQ.Where(e => e.KodeAK == req.KodeAK);
            }

            if (!string.IsNullOrWhiteSpace(req.EventId)) {
                filQ = filQ.Where(e => e.EventId == req.EventId);
            }

            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                var kws = Regex.Split(req.search.value, @"\s+");
                if (kws.Length > 0) {
                    filQ = filQ.Where(x =>
                            kws.All(kw => x.KodeAK.Contains(kw))
                            || kws.All(kw => x.Nama.Contains(kw))
                        );
                }
            }

            filQ = req.order.FirstOrDefault()?.IsDesc == true
                ? filQ.OrderByDescending(x => x.CreatedUtc)
                : filQ.OrderByDescending(x => x.CreatedUtc);

            result.Initialize(allQ, filQ, req.start, req.length);
            return result;
        } 
        #endregion
    }
}