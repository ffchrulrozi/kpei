﻿using Kpei.PortalAK.DataAccess.ModSosialisasi.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSosialisasi.Models {
    public class EventExpirationStatus {
        private IEventRepository _repo;
        private EventExpirationStatus(string name, string label, IEventRepository repo = null) {
            Name = name;
            Label = label;
            _repo = repo;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<EventExpirationStatus> All() {
            yield return DRAFT_STATUS;
            yield return AWAIT_OPEN_STATUS;
            yield return REGIS_OPEN_STATUS;
            yield return REGIS_CLOSED_STATUS;
            yield return FULL_STATUS;
            yield return EVENT_RUNNING_STATUS;
            yield return DONE_STATUS;
        }

        public static readonly EventExpirationStatus AWAIT_OPEN_STATUS = new EventExpirationStatus("AWAIT_OPEN", "Pre-registration");
        public static readonly EventExpirationStatus DRAFT_STATUS = new EventExpirationStatus("DRAFT", "Draft");
        public static readonly EventExpirationStatus REGIS_OPEN_STATUS = new EventExpirationStatus("REGIS_OPEN", "Pendaftaran Dibuka");
        public static readonly EventExpirationStatus REGIS_CLOSED_STATUS = new EventExpirationStatus("REGIS_CLOSED", "Pendaftaran Ditutup");
        public static readonly EventExpirationStatus FULL_STATUS = new EventExpirationStatus("FULL", "Kuota sudah penuh");
        public static readonly EventExpirationStatus EVENT_RUNNING_STATUS = new EventExpirationStatus("EVENT_RUNNING", "Acara berlangsung");
        public static readonly EventExpirationStatus DONE_STATUS = new EventExpirationStatus("DONE", "Acara telah selesai");
        
    }
}
