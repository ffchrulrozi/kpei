﻿using Kpei.PortalAK.DataAccess.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSosialisasi.Models {
    public class KehadiranPeserta {
        public string PesertaId { get; set; }
        public string KodeAK { get; set; }
        public string NamaAK { get; set; }
        public string Nama { get; set; }
        public string Jabatan { get; set; }
        public string Email { get; set; }
        public string Telp { get; set; }
        public bool Hadir { get; set; }

        //List Untuk NonKpei
        public KehadiranPeserta(EventPeserta dat) {
            Nama = dat.Nama;
            Jabatan = dat.Jabatan;
            Email = dat.Email;
            Telp = dat.Telp;
        }

        //List Untuk Kpei
        public KehadiranPeserta(EventPeserta dat, string NamaAK) {
            PesertaId = dat.Id;
            KodeAK = dat.KodeAK;
            this.NamaAK = NamaAK;
            Nama = dat.Nama;
            Jabatan = dat.Jabatan;
            Hadir = dat.Hadir;
        }
    }
}
