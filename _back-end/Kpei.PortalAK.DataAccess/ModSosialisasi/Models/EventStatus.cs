﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSosialisasi.Models {
    public class EventStatus {
        private EventStatus(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<EventStatus> All() {
            yield return PUBLISHED_STATUS;
            yield return DRAFT_STATUS;
        }

        public static readonly EventStatus PUBLISHED_STATUS = new EventStatus("PUBLISHED", "PUBLISHED");
        public static readonly EventStatus DRAFT_STATUS = new EventStatus("DRAFT", "DRAFT");
    }
}
