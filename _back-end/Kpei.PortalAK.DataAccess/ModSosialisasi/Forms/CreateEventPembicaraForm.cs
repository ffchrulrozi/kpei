﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSosialisasi.Forms {
    public class CreateEventPembicaraForm {
        [MaxLength(128)]
        public string EventId { get; set; }

        [Display(Name = "Nama")]
        [MaxLength(128)]
        public string Nama { get; set; }
        [Display(Name = "Jabatan")]
        [MaxLength(128)]
        public string Jabatan { get; set; }
        [Display(Name = "E-mail")]
        [MaxLength(128)]
        public string Email { get; set; }
        [MaxLength(128)]
        public string Telp { get; set; }
    }
}
