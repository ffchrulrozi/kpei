﻿using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSosialisasi.Forms {
    public class PesertaDttRequestForm : DttRequestForm{
        public string KodeAK { get; set; }
        public string EventId { get; set; }
    }
}
