﻿using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSosialisasi.Forms {
    public class EventDttRequestForm : DttRequestForm {
        public string KodeAK { get; set; }
        public string Status { get; set; }
        public string StatusExpiration { get; set; }
        public string JenisPelatihan { get; set; }
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }
    }
}
