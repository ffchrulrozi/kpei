﻿using Kpei.PortalAK.DataAccess.DbModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSosialisasi.Forms {
    public class UpdateEventForm {
        
        [MaxLength(128)]
        public string Id { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Judul")]
        [MaxLength(1024)]
        public string Judul { get; set; }
        [MaxLength(256)]
        public string Highlight { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Tempat")]
        [MaxLength(256)]
        public string Tempat { get; set; }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Tanggal Mulai Pelaksanaan")]
        [CustomValidation(typeof(CreateEventForm), nameof(StartDateValidation))]
        public DateTime? TglPelaksanaanStartUtc { get; set; }

        public static ValidationResult StartDateValidation(object val, ValidationContext valctx) {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as CreateEventForm;
            if (form == null) return success;
            if (form.TglPelaksanaanStartUtc < form.TglDaftarBukaUtc ||
                form.TglPelaksanaanStartUtc < form.TglDaftarTutupUtc ||
                form.TglPelaksanaanStartUtc > form.TglPelaksanaanEndUtc) {
                return new ValidationResult("Tanggal yang anda masukkan tidak mungkin benar. Silahkan dicek kembali", new[] { valctx.MemberName });
            }
            return success;
        }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Tanggal Akhir Pelaksanaan")]
        [CustomValidation(typeof(CreateEventForm), nameof(EndDateValidation))]
        public DateTime? TglPelaksanaanEndUtc { get; set; }

        public static ValidationResult EndDateValidation(object val, ValidationContext valctx) {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as CreateEventForm;
            if (form == null) return success;
            if (form.TglPelaksanaanEndUtc < form.TglPelaksanaanStartUtc ||
                form.TglPelaksanaanStartUtc < form.TglDaftarTutupUtc ||
                form.TglPelaksanaanEndUtc < form.TglDaftarBukaUtc) {
                return new ValidationResult("Tanggal yang anda masukkan tidak mungkin benar. Silahkan dicek kembali", new[] { valctx.MemberName });
            }
            return success;
        }


        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Tanggal Buka Pendaftaran")]
        [CustomValidation(typeof(CreateEventForm), nameof(StartRegValidation))]
        public DateTime? TglDaftarBukaUtc { get; set; }

        public static ValidationResult StartRegValidation(object val, ValidationContext valctx) {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as CreateEventForm;
            if (form == null) return success;
            if (form.TglDaftarBukaUtc > form.TglDaftarTutupUtc ||
                form.TglDaftarBukaUtc > form.TglPelaksanaanStartUtc ||
                form.TglDaftarBukaUtc > form.TglPelaksanaanEndUtc) {
                return new ValidationResult("Tanggal yang anda masukkan tidak mungkin benar. Silahkan dicek kembali", new[] { valctx.MemberName });
            }
            return success;
        }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Tanggal Tutup Pendaftaran")]
        [CustomValidation(typeof(CreateEventForm), nameof(EndRegValidation))]
        public DateTime? TglDaftarTutupUtc { get; set; }

        public static ValidationResult EndRegValidation(object val, ValidationContext valctx) {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as CreateEventForm;
            if (form == null) return success;
            if (form.TglDaftarTutupUtc < form.TglDaftarBukaUtc ||
                form.TglDaftarTutupUtc > form.TglPelaksanaanStartUtc ||
                form.TglDaftarTutupUtc > form.TglPelaksanaanEndUtc) {
                return new ValidationResult("Tanggal yang anda masukkan tidak mungkin benar. Silahkan dicek kembali", new[] { valctx.MemberName });
            }
            return success;
        }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Jenis Pelatihan")]
        [MaxLength(128)]
        public string JenisPelatihan { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Kapasitas per AK")]
        public int KapasitasPerAK { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Kapasitas Total")]
        public int KapasitasTotal { get; set; }

        [MaxLength(128)]
        public string StatusEvent { get; set; }
        [MaxLength(128)]
        public string BrosurFileUrl { get; set; }
        [MaxLength(128)]
        public string MateriFileUrl { get; set; }

        public void Init(Event dat) {
            Id = dat.Id;
            Judul = dat.Judul;
            Highlight = dat.Highlight;
            Tempat = dat.Tempat;
            TglPelaksanaanStartUtc = dat.TglPelaksanaanStartUtc;
            TglPelaksanaanEndUtc = dat.TglPelaksanaanEndUtc;
            TglDaftarBukaUtc = dat.TglDaftarBukaUtc;
            TglDaftarTutupUtc = dat.TglDaftarTutupUtc;
            JenisPelatihan = dat.JenisPelatihan;
            KapasitasPerAK = dat.KapasitasPerAK;
            KapasitasTotal = dat.KapasitasTotal;
            StatusEvent = dat.StatusEvent;
            BrosurFileUrl = dat.BrosurFileUrl;
            MateriFileUrl = dat.MateriFileUrl;
        }

        public List<UpdateEventPembicaraForm> ListPembicara { get; set; }
    }
}
