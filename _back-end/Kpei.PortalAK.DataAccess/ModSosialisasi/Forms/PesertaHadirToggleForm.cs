﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSosialisasi.Forms {
    public class PesertaHadirToggleForm {
        public string PesertaId { get; set; }
        public bool Hadir { get; set; }
    }
}
