﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSosialisasi.Forms {
    public class UpdateEventPesertaForm {
        [Required]
        [MaxLength(128)]
        public string Id { get; set; }

        [Required]
        [MaxLength(128)]
        public string EventId { get; set; }
        
        [MaxLength(32)]
        public string KodeAK { get; set; }
        [MaxLength(128)]
        public string Nama { get; set; }
        [MaxLength(128)]
        public string Jabatan { get; set; }
        [MaxLength(128)]
        public string Email { get; set; }
        [MaxLength(128)]
        public string Telp { get; set; }
        public Boolean Hadir { get; set; }

    }
}
