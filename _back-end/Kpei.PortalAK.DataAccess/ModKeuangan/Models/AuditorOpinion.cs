﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModKeuangan.Models {
    public class AuditorOpinion {
        public string Name { get; }
        public string Label { get; }

        public AuditorOpinion(string label, string name) {
            Name = name;
            Label = label;
        }

        public static IEnumerable<AuditorOpinion> All() {
            yield return UNQUALIFIED_OPINION;
            yield return MODIFIED_UNQUALIFIED_OPINION;
            yield return QUALIFIED_OPINION;
            yield return ADVERSE_OPINION;
            yield return DISCLAIMER_OPINION;
            yield return UNAUDITED_OPINION;
        }

        public static readonly AuditorOpinion UNQUALIFIED_OPINION = new AuditorOpinion("Wajar tanpa pengecualian (Unqualified Opinion)", "UNQUALIFIED");
        public static readonly AuditorOpinion MODIFIED_UNQUALIFIED_OPINION = new AuditorOpinion("Wajar Tanpa Pengecualian dengan Paragraf Penjelasan (Modified Unqualified Opinion)", "MODIFIED_UNQUALIFIED");
        public static readonly AuditorOpinion QUALIFIED_OPINION = new AuditorOpinion("Wajar dengan pengecualian (Qualified opinion)", "QUALIFIED");
        public static readonly AuditorOpinion ADVERSE_OPINION = new AuditorOpinion("Pendapat tidak wajar (Adverse Opinion)", "ADVERSE");
        public static readonly AuditorOpinion DISCLAIMER_OPINION = new AuditorOpinion("Pernyataan tidak memberikan pendapat (Disclaimer of Opinion)", "DISCLAIMER");
        public static readonly AuditorOpinion UNAUDITED_OPINION = new AuditorOpinion("Tidak diaudit (Unaudited)", "UNAUDITED");



    }
}
