﻿using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModKeuangan.Models {
    public class PeriodeLaporanKeuangan {

        public static readonly PeriodeLaporanKeuangan AKHIR_TAHUN =
            new PeriodeLaporanKeuangan(nameof(AKHIR_TAHUN), "Laporan Akhir Tahun");

        public static readonly PeriodeLaporanKeuangan TENGAH_TAHUN =
            new PeriodeLaporanKeuangan(nameof(TENGAH_TAHUN), "Laporan Tengah Tahun");

        private PeriodeLaporanKeuangan(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<PeriodeLaporanKeuangan> All() {
            yield return AKHIR_TAHUN;
            yield return TENGAH_TAHUN;
        }
    }
}