﻿using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModKeuangan.Models {
    public class LaporanKeuanganStatus {

        public static readonly LaporanKeuanganStatus NEW_STATUS = new LaporanKeuanganStatus("NEW", "NEW");

        public static readonly LaporanKeuanganStatus
            APPROVED_STATUS = new LaporanKeuanganStatus("APPROVED", "APPROVED");

        public static readonly LaporanKeuanganStatus
            ONGOING_STATUS = new LaporanKeuanganStatus("ONGOING", "ON GOING");

        public static readonly LaporanKeuanganStatus
            REJECTED_STATUS = new LaporanKeuanganStatus("REJECTED", "REJECTED");

        private LaporanKeuanganStatus(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<LaporanKeuanganStatus> All() {
            yield return NEW_STATUS;
            yield return ONGOING_STATUS;
            yield return APPROVED_STATUS;
            yield return REJECTED_STATUS;
        }
    }
}