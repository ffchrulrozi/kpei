﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Kpei.PortalAK.DataAccess.ModKeuangan.Models;

namespace Kpei.PortalAK.DataAccess.ModKeuangan.Forms {
    public class CreateLaporanKeuanganForm {
        [Required(ErrorMessage = "{0} harus diisi")]
        [MaxLength(32)]
        public string KodeAK { get; set; }

        [Display(Name = "Tahun")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public int Tahun { get; set; }

        /// <summary>
        ///     Gets or sets the periode.<br />
        ///     See <see cref="PeriodeLaporanKeuangan" />
        /// </summary>
        /// <value>
        ///     The periode.
        /// </value>
        [Display(Name = "Periode")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [MaxLength(32)]
        public string Periode { get; set; }

        [DisplayName("File Laporan Keuangan")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [MaxLength(256)]
        public string LaporanKeuanganFileUrl { get; set; }

        [DisplayName("Opini Auditor")]
        [Required]
        [MaxLength(256)]
        public string OpiniAuditor { get; set; }

        [Display(Name = "Total Aset Lancar")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal AsetLancar { get; set; }

        [Display(Name = "Total Utang Lancar (Jangka Pendek)")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal UtangLancar { get; set; }

        [Display(Name = "Total Aset (Total Aktiva)")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal TotalAset { get; set; }

        [Display(Name = "Total Utang")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal TotalUtang { get; set; }

        [Display(Name = "Pendapatan Usaha")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal Pendapatan { get; set; }

        [Display(Name = "Laba Bersih setelah Pajak")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal LabaBersih { get; set; }

        [Display(Name = "Total Ekuitas")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal Ekuitas { get; set; }
    }
}