﻿using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using System;

namespace Kpei.PortalAK.DataAccess.ModKeuangan.Forms {
    public class LaporanKeuanganDttRequestForm : DttRequestForm {
        public string Periode { get; set; }
        public int Tahun { get; set; }
        public string Status { get; set; }
        public string KodeAk { get; set; }
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }
    }
}