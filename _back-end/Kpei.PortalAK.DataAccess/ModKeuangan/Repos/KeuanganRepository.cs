﻿using System.Linq;
using System.Text.RegularExpressions;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModKeuangan.Forms;
using Kpei.PortalAK.DataAccess.ModKeuangan.Models;
using System.Collections;
using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModKeuangan.Repos {
    public class KeuanganRepository : IKeuanganRepository {
        private readonly AppDbContext _context;

        public KeuanganRepository(AppDbContext context) {
            _context = context;
        }

        public IEnumerable<KeuanganLaporan> Laporan => _context.KeuanganLaporan;

        /// <inheritdoc />
        public KeuanganLaporan Get(string id) {
            return _context.KeuanganLaporan.FirstOrDefault(x => x.Id == id);
        }

        /// <inheritdoc />
        public KeuanganLaporan Create(CreateLaporanKeuanganForm form) {
            var lap = new KeuanganLaporan {
                KodeAK = form.KodeAK,
                Periode = form.Periode,
                Tahun = form.Tahun,
                LaporanKeuanganFileUrl = form.LaporanKeuanganFileUrl,
                OpiniAuditor = form.OpiniAuditor,
                AsetLancar = form.AsetLancar,
                UtangLancar = form.UtangLancar,
                TotalAset = form.TotalAset,
                TotalUtang = form.TotalUtang,
                Pendapatan = form.Pendapatan,
                LabaBersih = form.LabaBersih,
                Ekuitas = form.Ekuitas,
                StatusRequest = LaporanKeuanganStatus.NEW_STATUS.Name
            };

            _context.KeuanganLaporan.Add(lap);
            _context.SaveChanges();

            return lap;
        }

        /// <inheritdoc />
        public KeuanganLaporan Update(UpdateLaporanKeuanganForm form) {
            var lap = Get(form.Id);
        
            lap.KodeAK = form.KodeAK;
            lap.Periode = form.Periode;
            lap.Tahun = form.Tahun;
            lap.LaporanKeuanganFileUrl = form.LaporanKeuanganFileUrl;
            lap.OpiniAuditor = form.OpiniAuditor;
            lap.AsetLancar = form.AsetLancar;
            lap.UtangLancar = form.UtangLancar;
            lap.TotalAset = form.TotalAset;
            lap.TotalUtang = form.TotalUtang;
            lap.Pendapatan = form.Pendapatan;
            lap.LabaBersih = form.LabaBersih;
            lap.Ekuitas = form.Ekuitas;
            
            _context.SaveChanges();

            return lap;
        }

        /// <inheritdoc />
        public KeuanganLaporan Approve(string id, string approverId) {
            var lap = Get(id);
            if (lap == null) {
                return null;
            }
            if (lap.StatusRequest == LaporanKeuanganStatus.NEW_STATUS.Name) {
                lap.Approver1UserId = approverId;
                lap.StatusRequest = LaporanKeuanganStatus.ONGOING_STATUS.Name;
            } else if (lap.StatusRequest == LaporanKeuanganStatus.ONGOING_STATUS.Name && lap.Approver1UserId != approverId) {
                lap.Approver2UserId = approverId;
                lap.StatusRequest = LaporanKeuanganStatus.APPROVED_STATUS.Name;
            } else {
                return null;
            }
            _context.SaveChanges();
            return lap;
        }

        /// <inheritdoc />
        public KeuanganLaporan Reject(string id, string rejectReason) {
            var lap = Get(id);
            lap.StatusRequest = LaporanKeuanganStatus.REJECTED_STATUS.Name;
            lap.RejectReason = rejectReason;
            _context.SaveChanges();
            return lap;
        }

        /// <inheritdoc />
        public PaginatedResult<KeuanganLaporan> List(LaporanKeuanganDttRequestForm req) {
            var result = new PaginatedResult<KeuanganLaporan>();

            var allQ = _context.KeuanganLaporan.AsQueryable();
            var filQ = _context.KeuanganLaporan.AsQueryable();

            if (!string.IsNullOrWhiteSpace(req.Periode)) {
                filQ = filQ.Where(x => x.Periode == req.Periode);
            }

            if (!string.IsNullOrWhiteSpace(req.KodeAk)) {
                filQ = filQ.Where(x => x.KodeAK == req.KodeAk);
            }

            if (req.Tahun > 1970) {
                filQ = filQ.Where(x => x.Tahun == req.Tahun);
            }

            if (!string.IsNullOrWhiteSpace(req.Status)) {
                filQ = filQ.Where(x => x.StatusRequest == req.Status);
            }

            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                var kws = Regex.Split(req.search.value, @"\s+");
                if (kws.Length > 0) {
                    filQ = filQ.Where(x =>
                            kws.All(kw => x.KodeAK.Contains(kw))
                            || kws.All(kw => x.Periode.Contains(kw))
                            || kws.All(kw => x.StatusRequest.Contains(kw))
                        );
                }
            }

            if (req.FromDateTime != null) {
                var fromUtc = req.FromDateTime.Value.Date.ToUniversalTime();
                filQ = filQ.Where(x => x.CreatedUtc >= fromUtc);
            }
            if (req.ToDateTime != null) {
                var toUtc = req.ToDateTime.Value.Date.AddDays(1).ToUniversalTime();
                filQ = filQ.Where(x => x.CreatedUtc <= toUtc);
            }

            if (req.firstOrderColumn?.name == nameof(KeuanganLaporan.KodeAK)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.KodeAK)
                    : filQ.OrderBy(x => x.KodeAK);
            } else if (req.firstOrderColumn?.name == nameof(KeuanganLaporan.Periode)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.Periode)
                    : filQ.OrderBy(x => x.Periode);
            } else if (req.firstOrderColumn?.name == nameof(KeuanganLaporan.StatusRequest)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.StatusRequest)
                    : filQ.OrderBy(x => x.StatusRequest);
            } else {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.CreatedUtc)
                    : filQ.OrderBy(x => x.CreatedUtc);
            }

            result.Initialize(allQ, filQ, req.start, req.length);

            return result;
        }
    }
}