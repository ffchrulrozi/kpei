﻿using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModKeuangan.Forms;
using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModKeuangan.Repos {
    public interface IKeuanganRepository {
        IEnumerable<KeuanganLaporan> Laporan { get; }

        KeuanganLaporan Get(string id);
        KeuanganLaporan Create(CreateLaporanKeuanganForm form);
        KeuanganLaporan Update(UpdateLaporanKeuanganForm form);
        KeuanganLaporan Approve(string id, string approverId);
        KeuanganLaporan Reject(string id, string rejectReason);
        PaginatedResult<KeuanganLaporan> List(LaporanKeuanganDttRequestForm req);
    }
}