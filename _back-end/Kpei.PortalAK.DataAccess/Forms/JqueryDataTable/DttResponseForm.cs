﻿using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.Forms.JqueryDataTable {
    public class DttResponseForm {
        private List<object> _data;

        public int draw { get; set; }
        public long recordsTotal { get; set; }
        public long recordsFiltered { get; set; }

        public List<object> data {
            get { return _data ?? (_data = new List<object>()); }
            set { _data = value; }
        }
    }

    public class DttResponseErrorForm : DttResponseForm {
        public DttResponseErrorForm() {}

        public DttResponseErrorForm(DttResponseForm fm) {
            Initialize(fm);
        }

        public string error { get; set; }

        public void Initialize(DttResponseForm fm) {
            draw = fm.draw;
            recordsTotal = fm.recordsTotal;
            recordsFiltered = fm.recordsFiltered;
            data = fm.data;
        }
    }
}