﻿using Kpei.PortalAK.DataAccess.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLayananBaru.Forms
{
    public class LayananBaruForm
    {
        private string _tipeMemberAK;
        private List<LayananBaruField> _layananBaruField;
        public LayananBaruForm()
        {
            _layananBaruField = new List<LayananBaruField>();
        }

        [Display(Name = "Tipe Member")]
        public string TipeMemberAK
        {
            get { return _tipeMemberAK?.Trim(); }
            set { _tipeMemberAK = value; }
        }

        [Display(Name = "Field Tambahan")]
        [Required]
        public List<LayananBaruField> LayananBaruField
        {
            get { return _layananBaruField; }
            set { _layananBaruField = value; }
        }
    }

    public class LayananBaruField
    {
        private string _tipeMemberPartisipan;
        private string _label;
        private string _formType;
        private string _formRequired;

        public string Label
        {
            get { return _label?.Trim(); }
            set { _label = value; }
        }

        public string FormType
        {
            get { return _formType?.Trim(); }
            set { _formType = value; }
        }

        public string FormRequired
        {
            get { return _formRequired?.Trim(); }
            set { _formRequired = value; }
        }

        public string TipeMemberPartisipan
        {
            get { return _tipeMemberPartisipan?.Trim(); }
            set { _tipeMemberPartisipan = value; }
        }
    }
}
