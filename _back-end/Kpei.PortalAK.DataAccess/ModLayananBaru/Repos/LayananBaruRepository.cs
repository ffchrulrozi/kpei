﻿using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModLayananBaru.Forms;
using Kpei.PortalAK.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Kpei.PortalAK.DataAccess.ModLayananBaru.Repos
{
    public class LayananBaruRepository : ILayananBaruRepository
    {
        private readonly AppDbContext _context;
        private readonly IUserActionLogService _ulog;

        public LayananBaruRepository(AppDbContext context, IUserActionLogService ulog)
        {
            _context = context;
            _ulog = ulog;
        }

        public IEnumerable<LayananBaru> GetAllLayananBaru()
        {
            return _context.LayananBaru.ToList();
        }

        public IEnumerable<object> GetTipeMemberPartisipan()
        {
            return _context.LayananBaru
                .Where(x => x.TipeMemberAK == "PARTISIPAN")
                .GroupBy(x => x.TipeMemberPartisipan)
                .Select(x => new
                {
                    Key = x.Key,
                    Value = x.Key
                })
                .ToArray();
        }

        public List<LayananBaru> GetLayananBaruByTipeMember(string tipe)
        {
            return _context.LayananBaru
                .Where(x => x.TipeMemberAK == tipe)
                .OrderBy(x => x.Order)
                .ToList();
        }

        public Dictionary<string, List<LayananBaru>> GetLayananBaruByTipeMemberPartisipan(string tipe)
        {
           var layananBaru = _context.LayananBaru
                .Where(x => x.TipeMemberAK == tipe)
                .OrderBy(x => x.Order)
                .GroupBy(x => x.TipeMemberPartisipan)
                .ToList();

            Dictionary<string, List<LayananBaru>> dicLayananBaru = new Dictionary<string, List<LayananBaru>>();
            foreach (var item in layananBaru.ToList())
            {
                var layanan = _context.LayananBaru 
                    .Where(x => x.TipeMemberPartisipan == item.Key)
                    .OrderBy(x => x.Order)
                    .ToList();

                dicLayananBaru.Add(item.Key, layanan);
            }

            return dicLayananBaru;
        }

        public void UpdateLayananBaru(LayananBaruForm model)
        {
            var dbstg = _context.LayananBaru
                .Where(x => x.TipeMemberAK == model.TipeMemberAK)
                .ToList();

            if (dbstg.Count > 0)
            {
                foreach (var item in dbstg.ToList())
                {
                    _context.Entry(item).State = EntityState.Deleted;
                    _context.LayananBaru.Remove(item);

                    _context.SaveChanges();
                }
            }

            int i = 0;
            foreach (var p in model.LayananBaruField)
            {
                if (p == null ||
                    string.IsNullOrWhiteSpace(p.Label) ||
                    string.IsNullOrWhiteSpace(p.FormType)) continue;

                _context.LayananBaru.Add(new LayananBaru
                {
                    TipeMemberAK = model.TipeMemberAK,
                    TipeMemberPartisipan = p.TipeMemberPartisipan,
                    FieldName = p.Label,
                    FieldType = p.FormType,
                    FieldRequired = p.FormRequired == "true" ? true : false,
                    Order = (i + 1),
                });

                i++;
                _context.SaveChanges();
            }
        }
    }
}
