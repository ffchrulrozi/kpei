﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModLayananBaru.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLayananBaru.Repos
{
    public interface ILayananBaruRepository
    {
        List<LayananBaru> GetLayananBaruByTipeMember(string tipe);
        IEnumerable<object> GetTipeMemberPartisipan();
        Dictionary<string, List<LayananBaru>> GetLayananBaruByTipeMemberPartisipan(string tipe);

        IEnumerable<LayananBaru> GetAllLayananBaru();

        void UpdateLayananBaru(LayananBaruForm form);
    }
}
