﻿using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModDataAknp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.Services {
    public class KpeiArmsService : IKpeiArmsService{
        private readonly ArmsDbContext _armsDb;
        private readonly IDataAknpDriverStore _store;

        public KpeiArmsService(ArmsDbContext armsDb,IDataAknpDriverStore store) {
            _armsDb = armsDb;
            _store = store;
        }

        public bool SyncData(string dataSlug) {
            var drv = _store.LoadDrivers().FirstOrDefault(x => x.DataSlug == dataSlug);

            if (drv == null) return false;
            if (drv.IsRegistrationData) return false;




            return true;
        }
    }
}
