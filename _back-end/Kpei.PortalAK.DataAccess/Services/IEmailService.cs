﻿using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.Services {
    public interface IEmailService {
        Task SendAsync(string fromEmail, string toEmail, string subject, string body);
        Task SendAsync(string toEmail, string subject, string body);
    }
}