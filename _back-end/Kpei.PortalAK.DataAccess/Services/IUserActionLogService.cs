﻿using System.Threading.Tasks;
using System.Web;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using System;
using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.Services {
    public interface IUserActionLogService {
        Task LogUserActionAsync(HttpContextBase ctx, string moduleId, string message, string relatedEntityName = null,
            string relatedEntityId = null,
            object data = null);
        Task LogUserActionAsync(HttpContext ctx, string moduleId, string message, string relatedEntityName = null, string relatedEntityId = null,
            object data = null);

        Task<PaginatedResult<UserActionLog>> ListLogsAsync(UserActionLogDttRequestForm req);
    }

    public class UserActionLogDttRequestForm : DttRequestForm {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string KodeAk { get; set; }
        public string RelatedEntityName { get; set; }
        public string RelatedEntityId { get; set; }
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }
    }
}