﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Newtonsoft.Json;
using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.ModLog.Models;

namespace Kpei.PortalAK.DataAccess.Services {
    /// <summary>
    ///     Log user's action.<br />
    ///     In here we use our own <see cref="AppDbContext" /> so that it can run in new thread.
    /// </summary>
    /// <seealso cref="Kpei.PortalAK.DataAccess.Services.IUserActionLogService" />
    public class UserActionLogService : IUserActionLogService {
        /// <inheritdoc />
        public Task LogUserActionAsync(HttpContextBase hctx, string moduleId, string message, string relatedEntityName = null,
            string relatedEntityId = null, object data = null) {

            return Task.Run(() => {
                if (string.IsNullOrWhiteSpace(message)) {
                    throw new ArgumentException("Action message is required", nameof(message));
                }

                if (string.IsNullOrWhiteSpace(moduleId)) {
                    throw new ArgumentException("Module id is required", nameof(moduleId));
                }

                using (var db = new AppDbContext()) {
                    var newEntry = new UserActionLog {
                        ModuleId = moduleId
                    };
                    
                    if (hctx != null) {
                        newEntry.RequestMethod = hctx.Request?.HttpMethod;
                        newEntry.RequestUrl = hctx.Request?.Url?.AbsoluteUri;
                        if (hctx?.Request?.IsAuthenticated == true) {
                            var appUser = hctx?.User?.Identity?.GetAppUser();
                            newEntry.UserId = appUser?.Id;
                            newEntry.UserName = appUser?.UserName;
                            newEntry.UserAknpCode = appUser?.KpeiCompanyCode;
                        }
                    }

                    newEntry.Message = message;
                    newEntry.RelatedEntityName =
                        !string.IsNullOrWhiteSpace(relatedEntityName) ? relatedEntityName : null;
                    newEntry.RelatedEntityId = !string.IsNullOrWhiteSpace(relatedEntityId) ? relatedEntityId : null;
                    newEntry.DataJson = data != null ? JsonConvert.SerializeObject(data) : null;

                    db.UserActionLogs.Add(newEntry);
                    db.SaveChanges();
                }
            });
        }

        /// <inheritdoc />
        public Task LogUserActionAsync(HttpContext ctx, string moduleId, string message,
            string relatedEntityName = null, string relatedEntityId = null, object data = null) {
            return LogUserActionAsync(ctx != null ? new HttpContextWrapper(ctx) : null, moduleId, message,
                relatedEntityName,
                relatedEntityId, data);
        }

        /// <inheritdoc />
        public Task<PaginatedResult<UserActionLog>> ListLogsAsync(UserActionLogDttRequestForm req) {
            return Task.Run(() => {
                var result = new PaginatedResult<UserActionLog>();

                using (var db = new AppDbContext()) {
                    var allQ = db.UserActionLogs.AsQueryable();
                    var filQ = db.UserActionLogs.AsQueryable();

                    if (!string.IsNullOrWhiteSpace(req.UserId)) {
                        filQ = filQ.Where(x => x.UserId == req.UserId);
                    }

                    if (!string.IsNullOrWhiteSpace(req.UserName)) {
                        filQ = filQ.Where(x => x.UserName == req.UserName);
                    }

                    if (!string.IsNullOrWhiteSpace(req.KodeAk)) {
                        filQ = filQ.Where(x => x.UserAknpCode == req.KodeAk);
                    }

                    if (!string.IsNullOrWhiteSpace(req.RelatedEntityName)) {
                        if (req.RelatedEntityName == RelatedEntityName.AKNP_ENTITY.Name) {
                            filQ = filQ.Where(x => x.RelatedEntityName.Contains(req.RelatedEntityName));
                        } else {
                            filQ = filQ.Where(x => x.RelatedEntityName == req.RelatedEntityName);
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(req.RelatedEntityId)) {
                        filQ = filQ.Where(x => x.RelatedEntityId == req.RelatedEntityId);
                    }

                    if (!string.IsNullOrWhiteSpace(req.search.value)) {
                        var kws = Regex.Split(req.search.value, @"\s+");
                        if (kws.Length > 0) {
                            filQ = filQ.Where(x => kws.All(kw => x.UserId.Contains(kw))
                                                   || kws.All(kw => x.UserName.Contains(kw))
                                                   || kws.All(kw => x.UserAknpCode.Contains(kw))
                                                   || kws.All(kw => x.Message.Contains(kw))
                                                   || kws.All(kw => x.RequestUrl.Contains(kw))
                                                   || kws.All(kw => x.DataJson.Contains(kw))
                            );
                        }
                    }

                    if (req.FromDateTime != null && req.ToDateTime != null) {
                        filQ = filQ.Where(e => e.CreatedUtc >= req.FromDateTime && e.CreatedUtc <= req.ToDateTime);
                    } else if (req.FromDateTime != null) {
                        filQ = filQ.Where(e => e.CreatedUtc >= req.FromDateTime);
                    } else if (req.ToDateTime != null) {
                        filQ = filQ.Where(e => e.CreatedUtc <= req.ToDateTime);
                    }

                    if (req.firstOrderColumn?.name == nameof(UserActionLog.UpdatedUtc)) {
                        filQ = req.order.FirstOrDefault()?.IsDesc == true
                            ? filQ.OrderByDescending(x => x.UpdatedUtc)
                            : filQ.OrderBy(x => x.UpdatedUtc);
                    } else if (req.firstOrderColumn?.name == nameof(UserActionLog.UserName)) {
                        filQ = req.order.FirstOrDefault()?.IsDesc == true
                            ? filQ.OrderByDescending(x => x.UserName)
                            : filQ.OrderBy(x => x.UserName);
                    } else if (req.firstOrderColumn?.name == nameof(UserActionLog.UserAknpCode)) {
                        filQ = req.order.FirstOrDefault()?.IsDesc == true
                            ? filQ.OrderByDescending(x => x.UserAknpCode)
                            : filQ.OrderBy(x => x.UserAknpCode);
                    } else if (req.firstOrderColumn?.name == nameof(UserActionLog.Message)) {
                        filQ = req.order.FirstOrDefault()?.IsDesc == true
                            ? filQ.OrderByDescending(x => x.Message)
                            : filQ.OrderBy(x => x.Message);
                    } else {
                        filQ = req.order.FirstOrDefault()?.IsDesc == true
                            ? filQ.OrderByDescending(x => x.CreatedUtc)
                            : filQ.OrderBy(x => x.CreatedUtc);
                    }

                    result.Initialize(allQ, filQ, req.start, req.length);
                }

                return result;
            });
        }
    }
}