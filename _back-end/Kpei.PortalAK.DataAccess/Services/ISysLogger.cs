﻿using System;

namespace Kpei.PortalAK.DataAccess.Services {
    public interface ISysLogger {
        void LogError(Exception ex);
    }
}