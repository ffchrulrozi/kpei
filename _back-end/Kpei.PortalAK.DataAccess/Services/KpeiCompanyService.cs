﻿using System.Linq;
using System.Web;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModDataAknp.Services;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Drivers;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Repos;
using System.Diagnostics;
using System;

namespace Kpei.PortalAK.DataAccess.Services {
    public class KpeiCompanyService : IKpeiCompanyService {
        private readonly MemberDbContext _memdb;
        private readonly IDataAknpDriverStore _store;
        private readonly AppDbContext _context;

        public KpeiCompanyService(MemberDbContext memdb, IDataAknpDriverStore store, AppDbContext context) {
            _memdb = memdb;
            _store = store;
            _context = context;
        }

        public KpeiCompany[] LoadKpeiLogin(bool useCache = true) {
            var driver = _store.LoadDrivers().First(x => x.DataSlug == DataAkteAknpDriver.SLUG);
            var cacheKey = "AllKpeiCompanies";
            KpeiCompany[] result = null;
            if (useCache) {
                result = HttpContext.Current.Items[cacheKey] as KpeiCompany[];
                if (result != null) return result;
            }

            var comps = _memdb.Database.SqlQuery<KpeiCompany>($@"
select
    Code,
    Name
from dbo.Companies
order by Code asc

".Trim()).ToList();

            foreach (var data in comps) {
                var dbData = driver.GetLatestActiveData(data.Code) as DataAkte;
                if (dbData != null && !string.IsNullOrWhiteSpace(dbData?.NamaPerusahaan)) {
                    data.Name = dbData.NamaPerusahaan;
                    data.HistoryDate = dbData.TglNamaBerlaku;
                }
            }

            result = comps.OrderBy(e => e.Code).ToArray();

            HttpContext.Current.Items[cacheKey] = result;
            return result;
        }

        /// <inheritdoc />
        public KpeiCompany[] AllKpeiCompanies(bool useCache = true) {
            var swThis = Stopwatch.StartNew();
            //var driver = _store.LoadDrivers().First(x => x.DataSlug == DataAkteAknpDriver.SLUG);
            var cacheKey = "AllKpeiCompanies";
            KpeiCompany[] result = null;
            if (useCache) {
                result = HttpContext.Current.Items[cacheKey] as KpeiCompany[];
                if (result != null) return result;
            }

            var sw = Stopwatch.StartNew();
            var comps = _memdb.Database.SqlQuery<KpeiCompany>($@"
select
    Code,
    Name,
    Name as MemberName
from dbo.Companies
order by Code asc

".Trim()).ToList();
            var timer1 = "Calling SSO DB elapsed " + sw.Elapsed.ToString() + " @" + swThis.Elapsed.ToString();
            sw.Reset();
            var all = _context.DataAkte.Where(e => e.Draft == false).ToArray();
            var call = all.Where(e => !comps.Any(f => f.Code == e.KodeAk)).Select(e => e.KodeAk).Distinct().ToList();
            var timer2 = "Calling DataAkte DB elapsed @" + sw.Elapsed.ToString() + " @" + swThis.Elapsed.ToString();

            foreach (var data in comps) {
                //var dbData = driver.GetLatestActiveData(data.Code) as DataAkte;
                var dbData = all
                    .Where(e => e.KodeAk == data.Code
                        && e.Status == DataAknpRequestStatus.APPROVED_STATUS.Name
                        && e.ActiveUtc != null
                    )
                    .Where(e => e.ActiveUtc <= DateTime.UtcNow)
                    .OrderByDescending(e => e.TglNamaBerlaku)
                    .FirstOrDefault() as DataAkte;
                if (dbData != null && !string.IsNullOrWhiteSpace(dbData?.NamaPerusahaan)) {
                    data.Name = dbData.NamaPerusahaan;
                    data.HistoryDate = dbData.TglNamaBerlaku;
                }
            }

            //add data that not exists in SSO
            foreach (var param in call) {
                //var record = driver.GetLatestActiveData(param) as DataAkte;
                var record = all
                    .Where(e => e.KodeAk == param
                        && e.Status == DataAknpRequestStatus.APPROVED_STATUS.Name
                        && e.ActiveUtc != null
                    )
                    .Where(e => e.ActiveUtc <= DateTime.UtcNow)
                    .OrderByDescending(e => e.TglNamaBerlaku)
                    .FirstOrDefault();
                if (record != null && !string.IsNullOrWhiteSpace(record?.NamaPerusahaan)) {
                    comps.Add(new KpeiCompany { Code = record.KodeAk, Name = record.NamaPerusahaan, HistoryDate = record.ActiveUtc });
                }
            }

            result = comps.OrderBy(e => e.Code).ToArray();

            HttpContext.Current.Items[cacheKey] = result;
            Debug.WriteLine("Finished getting Kpei Company Data {0} with {1}, {2}", swThis.Elapsed.ToString(), timer1, timer2);
            return result;
        }
    }
}