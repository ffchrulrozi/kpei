﻿using System;

namespace Kpei.PortalAK.DataAccess.Services {
    public interface IKpeiCompanyService {
        KpeiCompany[] AllKpeiCompanies(bool useCache = true);
        KpeiCompany[] LoadKpeiLogin(bool useCache = true);
    }

    public class KpeiCompany {
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime? HistoryDate { get; set; }
        public string KategoriMember { get; set; }
        public string MemberName { get; set; }
    }
}