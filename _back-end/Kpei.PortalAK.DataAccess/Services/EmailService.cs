﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Simplify.Mail;
using System;
using System.Linq;

namespace Kpei.PortalAK.DataAccess.Services {
    public class EmailService : IEmailService {
        private readonly ISettingRepository _stgrepo;
        private bool debugMode;
        private string debugEmailAddress;

        public EmailService(ISettingRepository stgrepo) {
            _stgrepo = stgrepo;
            debugMode = _stgrepo.GlobalSetting().EmailDebugMode;
            debugEmailAddress = _stgrepo.GlobalSetting().EmailDebugAddress;
        }

        /// <inheritdoc />
        public Task SendAsync(string fromEmail, string toEmail, string subject, string body) {
            var mailer = CreateMailer();

            debugMode = _stgrepo.GlobalSetting().EmailDebugMode;
            debugEmailAddress = _stgrepo.GlobalSetting().EmailDebugAddress;

            return mailer.SendAsync(
                fromEmail, 
                debugMode == true ? new List<String> { debugEmailAddress } : new List<string> {toEmail}, 
                subject, body);
        }

        /// <inheritdoc />
        public Task SendAsync(string toEmail, string subject, string body) {
            var stg = _stgrepo.SmtpEmailSetting();
            var mailer = CreateMailer();
            var fromEmail = stg.FromEmailAddress;
            var utcNow = DateTime.UtcNow;

            debugMode = _stgrepo.GlobalSetting().EmailDebugMode;
            debugEmailAddress = _stgrepo.GlobalSetting().EmailDebugAddress;

            subject += " - " + TimeZoneInfo.ConvertTimeFromUtc(
                DateTime.SpecifyKind(utcNow, DateTimeKind.Utc), 
                TimeZoneInfo.GetSystemTimeZones().First(x => x.Id == "SE Asia Standard Time"))
                .ToString("dd/MM/yyyy HH:mm");
            return mailer.SendAsync(
                fromEmail, 
                debugMode == true ? new List<string> { debugEmailAddress } : new List<string> {toEmail}, 
                subject, body);
        }

        private MailSender CreateMailer() {
            var stg = _stgrepo.SmtpEmailSetting();
            var mailer = new MailSender(
                stg.SmtpServerAddress, stg.SmtpServerPort,
                stg.SmtpUserName, stg.SmtpUserPassword, stg.SmtpEnableSsl
            );
            return mailer;
        }
    }
}