﻿using System;

namespace Kpei.PortalAK.DataAccess.Services {
    public class DefaultSysLogger : ISysLogger {

        public static Action<Exception> LogErrorFunc { get; set; }

        /// <inheritdoc />
        public void LogError(Exception ex) {
            LogErrorFunc?.Invoke(ex);
        }
    }
}