﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLog.Models {
    public class RelatedEntityName {

        private RelatedEntityName(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<RelatedEntityName> All() {
            yield return AKNP_ENTITY;
            yield return PINCODE_ENTITY;
            yield return MCLEARS_ENTITY;
            yield return EVENT_ENTITY;
            yield return KEUANGAN_ENTITY;
            yield return SURVEI_ENTITY;
            yield return CORE_ENTITY;
        }

        public static readonly RelatedEntityName AKNP_ENTITY = new RelatedEntityName("DataAknp", "Data Anggota Kliring & Partisipan");
        public static readonly RelatedEntityName PINCODE_ENTITY = new RelatedEntityName("ResetPincodeRequest", "Reset Pincode");
        public static readonly RelatedEntityName MCLEARS_ENTITY = new RelatedEntityName("LayananMclears", "Layanan m-CLEARS");
        public static readonly RelatedEntityName EVENT_ENTITY = new RelatedEntityName("SosialisasiPelatihan", "Sosialisasi & Pelatihan");
        public static readonly RelatedEntityName KEUANGAN_ENTITY = new RelatedEntityName("LaporanKeuangan", "Laporan Keuangan");
        public static readonly RelatedEntityName SURVEI_ENTITY = new RelatedEntityName("SurveiKeanggotaan", "Survei Kepuasan");
        public static readonly RelatedEntityName CORE_ENTITY = new RelatedEntityName("Core", "Setting");
    }
}
