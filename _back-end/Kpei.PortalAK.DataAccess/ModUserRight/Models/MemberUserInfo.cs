﻿using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.DataAccess.ModUserRight.Models {
    public class MemberUserInfo {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string CompanyCode { get; set; }
        private string[] _roles;
        public string[] Roles {
            get { return _roles ?? new string[0]; }
            set { _roles = value; }
        }
        public AppUserRight Right { get; set; }
    }

    public class MemberUserInfoPerModule {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string CompanyCode { get; set; }
        private string[] _roles;
        public string[] Roles {
            get { return _roles ?? new string[0]; }
            set { _roles = value; }
        }
        public AppUserRightPerModule Right { get; set; }

        public MemberUserInfoPerModule(MemberUserInfo inf, string moduleId) {
            Id = inf.Id;
            UserName = inf.UserName;
            FullName = inf.FullName;
            Email = inf.Email;
            CompanyCode = inf.CompanyCode;
            Roles = inf.Roles;
            Right = inf.Right.ForModule[moduleId];
        }
    }
}