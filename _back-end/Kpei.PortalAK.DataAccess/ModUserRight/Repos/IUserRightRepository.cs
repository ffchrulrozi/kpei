﻿using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.ModUserRight.Forms;
using Kpei.PortalAK.DataAccess.ModUserRight.Models;

namespace Kpei.PortalAK.DataAccess.ModUserRight.Repos {
    public interface IUserRightRepository {

        MemberUserInfo GetSingleMemberRight(string userId);

        PaginatedResult<MemberUserInfo> GetMemberRights(UserRightDttRequestForm req);

        bool ChangeRight(string userId, string moduleId, string rightName, bool hasRight);
    }
}