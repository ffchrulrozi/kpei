﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModUserRight.Forms;
using Kpei.PortalAK.DataAccess.ModUserRight.Models;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.DataAccess.ModUserRight.Repos {
    public class UserRightRepository : IUserRightRepository {
        private readonly AppDbContext _db;
        private readonly MemberDbContext _memDb;
        private readonly IUserService _usersvc;

        public UserRightRepository(AppDbContext db, MemberDbContext memDb, IUserService usersvc) {
            _db = db;
            _memDb = memDb;
            _usersvc = usersvc;
        }

        /// <inheritdoc />
        public MemberUserInfo GetSingleMemberRight(string userId) {
            var selectQuery = $@"

select
    usr.Id as {nameof(MemberUserInfo.Id)},
    usr.UserName as {nameof(MemberUserInfo.UserName)},
    usr.FullName as {nameof(MemberUserInfo.FullName)},
    usr.Email as {nameof(MemberUserInfo.Email)},
    comp.Code as {nameof(MemberUserInfo.CompanyCode)}
from
    dbo.Users as usr
    join dbo.Companies as comp on usr.CompanyId = comp.Id
where
    usr.Status = 1 and
    usr.Id = @userId

".Trim();

            var result = _memDb.Database.SqlQuery<MemberUserInfo>(selectQuery, new SqlParameter("@userId", userId))
                .Single();
            LoadMemberRoles(result);
            LoadRights(result);

            return result;
        }

        /// <inheritdoc />
        public PaginatedResult<MemberUserInfo> GetMemberRights(UserRightDttRequestForm req) {

            var result = new PaginatedResult<MemberUserInfo> {
                Offset = req.start,
                Limit = req.length
            };

            FindRecordsTotal(req.IsKpei, result);
            FindRecordsFilteredAndLoadData(req.IsKpei, req.firstOrderColumn?.name ?? "UserName",
                req.order.FirstOrDefault()?.IsDesc == true, req.search.value, result);

            foreach (var dat in result.Data) {
                LoadMemberRoles(dat);
                LoadRights(dat);
            }

            return result;
        }

        /// <inheritdoc />
        public bool ChangeRight(string userId, string moduleId, string rightName, bool hasRight) {
            if (string.IsNullOrWhiteSpace(userId)) {
                throw new ArgumentNullException(nameof(userId), "Can't change user right of null user id");
            }

            if (CoreModules.IdAndNameTuples.All(x => x.Item1 != moduleId)) {
                throw new ArgumentException(nameof(moduleId), $"Module id not found in {typeof(CoreModules)}");
            }

            var hasProperRightName = rightName == nameof(UserRight.KpeiApprover)
                                     || rightName == nameof(UserRight.KpeiViewer)
                                     || rightName == nameof(UserRight.KpeiModifier)
                                     || rightName == nameof(UserRight.KpeiAdmin)
                                     || rightName == nameof(UserRight.NonKpeiViewer)
                                     || rightName == nameof(UserRight.NonKpeiModifier)
                                     || rightName == nameof(UserRight.NonKpeiViewerAsKpei);
            if (!hasProperRightName) {
                throw new ArgumentException(nameof(rightName), $"Right name is not recognized");
            }

            var rightsData = _db.UserRights.FirstOrDefault(x => x.MemberUserId == userId && x.ModuleId == moduleId);
            if (rightsData == null) {
                rightsData = new UserRight {
                    MemberUserId = userId,
                    ModuleId = moduleId
                };
                if (rightName.StartsWith("NonKpei")) {
                    rightsData.NonKpeiViewer = true;
                } else {
                    rightsData.KpeiViewer = true;
                }
                _db.UserRights.Add(rightsData);
            } else {
                _db.Entry(rightsData).State = EntityState.Modified;
            }

            typeof(UserRight).GetProperty(rightName).SetValue(rightsData, hasRight);
            _db.SaveChanges();

            var dat = GetSingleMemberRight(userId);
            var result = (bool) typeof(AppUserRightPerModule).GetProperty(rightName)
                .GetValue(dat.Right.ForModule[moduleId]);
            return result;
        }

        private void LoadRights(MemberUserInfo dat) {
            dat.Right = _usersvc.GetUserRight(dat.Id, dat.Roles, dat.CompanyCode, false);
        }

        private void LoadMemberRoles(MemberUserInfo dat) {
            var roles = _memDb.Database.SqlQuery<string>($@"

select
    ro.Name
from
    dbo.Roles as ro
    join dbo.UsersRoles as usro on ro.Id = usro.RoleId
    join dbo.Users as us on usro.UserId = us.Id
where
    us.Status = 1 and
    us.Id = @userId

".Trim(), new SqlParameter("@userId", dat.Id)).ToList();

            dat.Roles = roles.ToArray();
        }

        private void FindRecordsFilteredAndLoadData(bool isKpei, string orderCol, bool orderDesc, string search,
            PaginatedResult<MemberUserInfo> result) {

            var searchCols = new[] {
                "usr.Id", "usr.UserName", "comp.Code"
            };
            var nextCondOp = " and ";

            var searchWhere = CreateSearchWhereQuery(search, nextCondOp, searchCols);
            var searchCond = searchWhere.Item1;
            var searchPars = searchWhere.Item2.Cast<object>().ToArray();

            var filteredMembersQuery = isKpei
                ? $@"

select
    usr.Id as {nameof(MemberUserInfo.Id)},
    usr.UserName as {nameof(MemberUserInfo.UserName)},
    usr.FullName as {nameof(MemberUserInfo.FullName)},
    usr.Email as {nameof(MemberUserInfo.Email)},
    comp.Code as {nameof(MemberUserInfo.CompanyCode)}
from
    dbo.Users as usr
    join dbo.Companies as comp on usr.CompanyId = comp.Id
where
    usr.Status = 1 and
    {searchCond}
    (comp.Code like 'ZZ' or comp.Code like 'KPEI')

".Trim()
                : $@"

select
    usr.Id as {nameof(MemberUserInfo.Id)},
    usr.UserName as {nameof(MemberUserInfo.UserName)},
    usr.FullName as {nameof(MemberUserInfo.FullName)},
    usr.Email as {nameof(MemberUserInfo.Email)},
    comp.Code as {nameof(MemberUserInfo.CompanyCode)}
from
    dbo.Users as usr
    join dbo.Companies as comp on usr.CompanyId = comp.Id
where
    usr.Status = 1 and
    {searchCond}
    (comp.Code not like 'ZZ' and comp.Code not like 'KPEI')

".Trim();

            FindRecordsFiltered(result, filteredMembersQuery, searchPars);

            // can't use searchPars multiple times, so must reinit.
            searchWhere = CreateSearchWhereQuery(search, nextCondOp, searchCols);
            searchPars = searchWhere.Item2.Cast<object>().ToArray();
            LoadData(orderCol, orderDesc, result, filteredMembersQuery, searchPars);
        }

        private void FindRecordsFiltered(PaginatedResult<MemberUserInfo> result, string filteredMembersQuery,
            object[] searchPars) {
            result.RecordsFiltered = _memDb.Database.SqlQuery<int>($@"

select count(*) as cnt
from ({filteredMembersQuery}) as tbl

".Trim(), searchPars).Single();
        }

        private void LoadData(string orderCol, bool orderDesc, PaginatedResult<MemberUserInfo> result,
            string filteredMembersQuery,
            object[] searchPars) {
            var pagingBetweenLeft = result.Offset;
            var pagingBetweenRight = result.Offset + result.Limit;

            result.Data = _memDb.Database.SqlQuery<MemberUserInfo>($@"

select * from (
    select *, row_number() over (order by {orderCol} {(orderDesc ? "desc" : "asc")}) as __row_num__
    from ({filteredMembersQuery}) as tbl
) as subquery
where __row_num__ between {pagingBetweenLeft} and {pagingBetweenRight}


".Trim(), searchPars).ToList();
        }

        private void FindRecordsTotal(bool isKpei, PaginatedResult<MemberUserInfo> result) {
            var allMembersCountQuery = isKpei
                ? $@"

select
    count(usr.Id) as cnt
from
    dbo.Users as usr
    join dbo.Companies as comp on usr.CompanyId = comp.Id
where
    usr.Status = 1 and
    (comp.Code like 'ZZ' or comp.Code like 'KPEI')

".Trim()
                : $@"

select
    count(usr.Id) as cnt
from
    dbo.Users as usr
    join dbo.Companies as comp on usr.CompanyId = comp.Id
where
    usr.Status = 1 and
    comp.Code not like 'ZZ' and comp.Code not like 'KPEI'

".Trim();

            result.RecordsTotal = _memDb.Database.SqlQuery<int>(allMembersCountQuery).Single();
        }

        private Tuple<string, SqlParameter[]> CreateSearchWhereQuery(string search, string suffix,
            params string[] columns) {
            if (string.IsNullOrWhiteSpace(search)) return Tuple.Create("", new SqlParameter[0]);
            if (columns == null || columns.Length == 0) return Tuple.Create("", new SqlParameter[0]);
            suffix = suffix ?? "";

            var kws = Regex.Split(search, @"\s+");
            var kwsParam = new List<SqlParameter>();

            var sb = new StringBuilder();
            sb.Append("(");
            var idx = 0;
            foreach (var col in columns) {
                if (idx > 0) {
                    sb.Append(" or ");
                }

                var idx2 = 0;
                foreach (var kw in kws) {
                    if (idx2 > 0) {
                        sb.Append(" and ");
                    }

                    var parName = $"@searchKeyword_{idx}_{idx2}";
                    sb.Append($"{col} like {parName}");
                    kwsParam.Add(new SqlParameter($"{parName}", $"%{kw}%"));
                    idx2++;
                }

                idx++;
            }

            sb.Append(")");
            sb.Append(suffix);
            return Tuple.Create(sb.ToString(), kwsParam.ToArray());
        }
    }
}