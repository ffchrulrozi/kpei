﻿using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;

namespace Kpei.PortalAK.DataAccess.ModUserRight.Forms {
    public class UserRightDttRequestForm : DttRequestForm {
        public bool IsKpei { get; set; }
        public string ModuleId { get; set; }
    }
}