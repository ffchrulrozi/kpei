﻿namespace Kpei.PortalAK.DataAccess.ModUserRight.Forms {
    public class UserRightToggleForm {
        public string UserId { get; set; }
        public string ModuleId { get; set; }
        public string RightName { get; set; }
        public bool Check { get; set; }
    }
}