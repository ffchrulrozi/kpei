﻿using System;
using System.Linq;

namespace Kpei.PortalAK.DataAccess.Tools {
    public static class DateTimezoneConverter {
        public static readonly TimeZoneInfo WIB_TIME_ZONE = TimeZoneInfo.GetSystemTimeZones()
            .First(x => x.Id == "SE Asia Standard Time");

        public static DateTime ConvertFromUtcToWib(this DateTime dt) {
            var utc = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            return TimeZoneInfo.ConvertTimeFromUtc(utc, WIB_TIME_ZONE);
        }

        public static DateTime ConvertFromWibToUtc(this DateTime dt) {
            var wib = DateTime.SpecifyKind(dt, DateTimeKind.Unspecified);
            var conv = TimeZoneInfo.ConvertTimeToUtc(wib, WIB_TIME_ZONE);
            var utc = DateTime.SpecifyKind(conv, DateTimeKind.Utc);
            return utc;
        }
    }
}