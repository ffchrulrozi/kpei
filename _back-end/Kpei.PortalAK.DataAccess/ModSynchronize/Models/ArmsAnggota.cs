﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSynchronize.Models {
    public class ArmsAnggota {
        public Int64 AnggotaId { get; set; }
        public string Kode { get; set; }
        public string LongKode { get; set; }
        public string Nama { get; set; }
        public Int64? LastAkteNamaId { get; set; }
        public Int64? LastAkteModalId { get; set; }
        public Int64? LastAktePemegangSahamId { get; set; }
        public Int64? LastAkteKomisarisId { get; set; }
        public Int64? LastAkteDireksiId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
