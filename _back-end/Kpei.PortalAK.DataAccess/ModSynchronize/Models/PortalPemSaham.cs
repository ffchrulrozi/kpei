﻿using Kpei.PortalAK.DataAccess.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSynchronize.Models {
    public class PortalPemSaham : BaseAknpData {
        public string Nama { get; set; }
        
        public decimal LembarSaham { get; set; }
        
        public decimal NominalSahamRupiah { get; set; }
        
        public string WargaNegara { get; set; }

        public string AkteId { get; set; }

        public int LastVersion { get; set; }
    }
}
