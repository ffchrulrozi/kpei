﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSynchronize.Models {
    public class SyncAction {
        private SyncAction(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<SyncAction> All() {
            yield return INSERT_ACTION;
            yield return UPDATE_ACTION;
        }

        public static readonly SyncAction INSERT_ACTION = new SyncAction("Insert", "Insert");
        public static readonly SyncAction UPDATE_ACTION = new SyncAction("Update", "Update");
    }
}
