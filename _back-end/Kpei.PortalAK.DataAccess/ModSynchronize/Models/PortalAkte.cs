﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.DbModels;
using System.ComponentModel.DataAnnotations;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;

namespace Kpei.PortalAK.DataAccess.ModSynchronize.Models {
    public class PortalAkte : BaseAknpData {
        
        #region Nama

        [Display(Name = "Nama Perusahaan", GroupName = "Nama", Order = 9)]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        public string NamaPerusahaan { get; set; }

        [Display(Name = "Tgl. Berlaku Nama", GroupName = "Nama", Order = 10)]
        [AknpDisplay(null, false, false)]
        [Required]
        public DateTime TglNamaBerlaku { get; set; }

        #endregion

        #region Akte Details

        [Display(Name = "No. Akte", GroupName = "Informasi", Order = 1)]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        public string NoAkte { get; set; }

        [Display(Name = "Jenis Akte", GroupName = "Informasi", Order = 2)]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        public string JenisAkte { get; set; }

        [Display(Name = "Tgl. Akte", GroupName = "Informasi", Order = 3)]
        [AknpDisplay(null, true, true)]
        [Required]
        public DateTime? TglAkte { get; set; }

        [Display(GroupName = "Informasi", Order = 4)]
        [AknpDisplay(null, false, false)]
        [Required]
        [MaxLength(128)]
        public string Notaris { get; set; }

        [Display(GroupName = "Informasi", Order = 5)]
        [AknpDisplay(null, false, false)]
        [Required]
        [MaxLength(1024)]
        public string Ringkasan { get; set; }

        #endregion

        #region Modal

        [Display(Name = "Modal Dasar (Rp)", GroupName = "Modal", Order = 11)]
        [AknpDisplay(null, true, false)]
        [Required]
        public decimal ModalDasarRupiah { get; set; }

        [Display(Name = "Modal Disetor (Rp)", GroupName = "Modal", Order = 12)]
        [AknpDisplay(null, false, false)]
        [Required]
        public decimal ModalDisetorRupiah { get; set; }
        
        #endregion

        public int LastVersion { get; set; }

    }
}
