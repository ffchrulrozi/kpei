﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSynchronize.Models {
    public class SyncFlow {
        private SyncFlow(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<SyncFlow> All() {
            yield return ARMS_PORTAL_FLOW;
            yield return PORTAL_ARMS_FLOW;
        }

        public static readonly SyncFlow ARMS_PORTAL_FLOW = new SyncFlow("ARMS -> Portal", "ARMS -> Portal");
        public static readonly SyncFlow PORTAL_ARMS_FLOW = new SyncFlow("Portal -> ARMS", "Portal -> ARMS");
    }
}
