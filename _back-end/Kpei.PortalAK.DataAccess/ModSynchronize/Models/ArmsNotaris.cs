﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSynchronize.Models {
    public class ArmsNotaris {
        public Int64 NotarisId { get; set; }
        public string Nama { get; set; }
        public string Wilayah { get; set; }
        public string Alamat { get; set; }
        public string Telpon { get; set; }
        public string Fax { get; set; }
        public string NoSttd { get; set; }
        public DateTime? TglSttd { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedUtc { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
