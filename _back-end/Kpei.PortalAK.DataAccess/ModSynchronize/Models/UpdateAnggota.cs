﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSynchronize.Models {
    public class UpdateAnggota {
        public long AnggotaId { get; set; }
        public string Kode { get; set; }
        public long? Nama { get; set; }
        public long? Direksi { get; set; }
        public long? Komisaris { get; set; }
        public long? Pemsaham { get; set; }
        public long? Modal { get; set; }
    }
}
