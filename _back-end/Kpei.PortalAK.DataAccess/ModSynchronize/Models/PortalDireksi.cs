﻿using Kpei.PortalAK.DataAccess.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSynchronize.Models {
    public class PortalDireksi : BaseAknpData {
        public string Nama { get; set; }
        
        public string Jabatan { get; set; }
        
        public string Email { get; set; }
        
        public string Telp { get; set; }
        
        public string HP { get; set; }
        
        public string AkteId { get; set; }

        public int LastVersion { get; set; }
    }
}
