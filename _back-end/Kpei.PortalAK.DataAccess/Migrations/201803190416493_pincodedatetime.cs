namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pincodedatetime : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PincodeRequests", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.PincodeRequests", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.PincodeRequests", "TglAktifUtc", c => c.DateTime());
            AlterColumn("dbo.PincodeRequestLogs", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.PincodeRequestLogs", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PincodeRequestLogs", "UpdatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PincodeRequestLogs", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PincodeRequests", "TglAktifUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PincodeRequests", "UpdatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PincodeRequests", "CreatedUtc", c => c.DateTime(nullable: false));
        }
    }
}
