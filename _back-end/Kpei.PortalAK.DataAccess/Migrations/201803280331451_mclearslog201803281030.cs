namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mclearslog201803281030 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MclearsRequestLogs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RequestId = c.String(maxLength: 128),
                        Author = c.String(),
                        Aktivitas = c.String(),
                        CreatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MclearsRequests", t => t.RequestId)
                .Index(t => t.RequestId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MclearsRequestLogs", "RequestId", "dbo.MclearsRequests");
            DropIndex("dbo.MclearsRequestLogs", new[] { "RequestId" });
            DropTable("dbo.MclearsRequestLogs");
        }
    }
}
