namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addistrashedevent201804061141 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataStatusKPEIs", "isTrashed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataStatusKPEIs", "isTrashed");
        }
    }
}
