namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMissingJenisPerusahaanInDataInformasiLain : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataInformasiLains", "JenisPerusahaan", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataInformasiLains", "JenisPerusahaan");
        }
    }
}
