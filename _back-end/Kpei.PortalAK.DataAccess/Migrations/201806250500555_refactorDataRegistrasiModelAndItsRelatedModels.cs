namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class refactorDataRegistrasiModelAndItsRelatedModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataContactPersonRegistrasis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RegistrasiId = c.String(nullable: false, maxLength: 128),
                        Nama = c.String(nullable: false, maxLength: 128),
                        Jabatan = c.String(nullable: false, maxLength: 128),
                        Email = c.String(nullable: false, maxLength: 128),
                        Telp = c.String(nullable: false, maxLength: 128),
                        HP = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataRegistrasis", t => t.RegistrasiId, cascadeDelete: true)
                .Index(t => t.RegistrasiId);
            
            CreateTable(
                "dbo.DataDireksiRegistrasis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        TtdFileUrl = c.String(maxLength: 256),
                        KtpFileUrl = c.String(maxLength: 256),
                        Nama = c.String(nullable: false, maxLength: 128),
                        Jabatan = c.String(nullable: false, maxLength: 128),
                        Email = c.String(nullable: false, maxLength: 128),
                        Telp = c.String(nullable: false, maxLength: 128),
                        HP = c.String(nullable: false, maxLength: 128),
                        RegistrasiId = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataRegistrasis", t => t.RegistrasiId, cascadeDelete: true)
                .Index(t => t.RegistrasiId);
            
            CreateTable(
                "dbo.DataKomisarisRegistrasis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        TtdFileUrl = c.String(maxLength: 256),
                        KtpFileUrl = c.String(maxLength: 256),
                        Nama = c.String(nullable: false, maxLength: 128),
                        Jabatan = c.String(nullable: false, maxLength: 128),
                        Email = c.String(nullable: false, maxLength: 128),
                        Telp = c.String(nullable: false, maxLength: 128),
                        HP = c.String(nullable: false, maxLength: 128),
                        RegistrasiId = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataRegistrasis", t => t.RegistrasiId, cascadeDelete: true)
                .Index(t => t.RegistrasiId);
            
            CreateTable(
                "dbo.DataPejabatBerwenangRegistrasis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RegistrasiId = c.String(nullable: false, maxLength: 128),
                        TtdFileUrl = c.String(maxLength: 256),
                        KtpFileUrl = c.String(maxLength: 256),
                        Nama = c.String(nullable: false, maxLength: 128),
                        Jabatan = c.String(nullable: false, maxLength: 128),
                        Email = c.String(nullable: false, maxLength: 128),
                        Telp = c.String(nullable: false, maxLength: 128),
                        HP = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataRegistrasis", t => t.RegistrasiId, cascadeDelete: true)
                .Index(t => t.RegistrasiId);
            
            CreateTable(
                "dbo.DataPemegangSahamRegistrasis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Nama = c.String(nullable: false, maxLength: 128),
                        LembarSaham = c.Int(nullable: false),
                        NominalSahamRupiah = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WargaNegara = c.String(nullable: false, maxLength: 128),
                        RegistrasiId = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataRegistrasis", t => t.RegistrasiId, cascadeDelete: true)
                .Index(t => t.RegistrasiId);
            
            CreateTable(
                "dbo.DataSupportDocumentRegistrasis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        DocFileUrl = c.String(),
                        RegistrasiId = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataRegistrasis", t => t.RegistrasiId, cascadeDelete: true)
                .Index(t => t.RegistrasiId);
            
            AddColumn("dbo.DataDireksis", "KtpFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataKomisaris", "KtpFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataInformasiLains", "RegistrasiBaruId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPejabatBerwenangItems", "KtpFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "NamaPerusahaan", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataRegistrasis", "Website", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "ModalDisetorRupiah", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DataRegistrasis", "ModalDasarRupiah", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DataRegistrasis", "NoAkte", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataRegistrasis", "TglAkte", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataRegistrasis", "AkteFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "FormulirDaftarAkFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "SPDKFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "SKPEFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "PerjanjianLayananKliringPenjaminanTransaksiFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "PemberitahuanPenggunaanBankPembayaranFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "PersetujuanPemberianWewenangFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "SuratKuasaPindahBukuDanaFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "SuratKuasaFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "ChecklistAkFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "PerjanjianTitipEfekUntukPinjamFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "SuratKuasaPMEFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "AppPemberiPinjamanLenderPMEFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "AppPenerimaPinjamanBorrowerPMEFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "PerjanjianPMEAsBorrowerFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "PerjanjianPMEAsLenderFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataRegistrasis", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataRegistrasis", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataRegistrasis", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataRegistrasis", "Approver2UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "KodeAk", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.DataRegistrasis", "Gedung", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "Jalan", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.DataRegistrasis", "Kota", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "KodePos", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.DataRegistrasis", "Telp", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "Fax", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "PPE", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "PEE", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "MI", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "BK", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "BU", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "Notaris", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "NoPerjanjian", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "KategoriMember", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "TipeMemberAK", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "TipeMemberPartisipan", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "StatusPerusahaan", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "NPWP", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "AnggotaBursa", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "AnggaranDasarFileUrl", c => c.String(maxLength: 256));
            AlterColumn("dbo.DataRegistrasis", "AktePendirianFileUrl", c => c.String(maxLength: 256));
            AlterColumn("dbo.DataRegistrasis", "AktePerubahanFileUrl", c => c.String(maxLength: 256));
            AlterColumn("dbo.DataRegistrasis", "LaporanKeuanganFileUrl", c => c.String(maxLength: 256));
            AlterColumn("dbo.DataRegistrasis", "IjinUsahaFileUrl", c => c.String(maxLength: 256));
            AlterColumn("dbo.DataRegistrasis", "SPABFileUrl", c => c.String(maxLength: 256));
            AlterColumn("dbo.DataRegistrasis", "SPAKFileUrl", c => c.String(maxLength: 256));
            AlterColumn("dbo.DataRegistrasis", "NPWPFileUrl", c => c.String(maxLength: 256));
            CreateIndex("dbo.DataInformasiLains", "RegistrasiBaruId");
            AddForeignKey("dbo.DataInformasiLains", "RegistrasiBaruId", "dbo.DataRegistrasis", "Id");
            DropColumn("dbo.DataInformasiLains", "KTPFileUrl");
            DropColumn("dbo.DataRegistrasis", "Nama");
            DropColumn("dbo.DataRegistrasis", "ModalDasar");
            DropColumn("dbo.DataRegistrasis", "ModalDisetor");
            DropColumn("dbo.DataRegistrasis", "Email");
            DropColumn("dbo.DataRegistrasis", "BankPembayaran");
            DropColumn("dbo.DataRegistrasis", "StatusKPEI");
            DropColumn("dbo.DataRegistrasis", "StatusBursa");
            DropColumn("dbo.DataRegistrasis", "NoSPAK");
            DropColumn("dbo.DataRegistrasis", "TglSPAKUtc");
            DropColumn("dbo.DataRegistrasis", "NoSPAB");
            DropColumn("dbo.DataRegistrasis", "TglSPABUtc");
            DropColumn("dbo.DataRegistrasis", "KTPFileUrl");
            DropColumn("dbo.DataRegistrasis", "TglAktifUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataRegistrasis", "TglAktifUtc", c => c.DateTime());
            AddColumn("dbo.DataRegistrasis", "KTPFileUrl", c => c.String());
            AddColumn("dbo.DataRegistrasis", "TglSPABUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataRegistrasis", "NoSPAB", c => c.String(nullable: false));
            AddColumn("dbo.DataRegistrasis", "TglSPAKUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataRegistrasis", "NoSPAK", c => c.String(nullable: false));
            AddColumn("dbo.DataRegistrasis", "StatusBursa", c => c.String(nullable: false));
            AddColumn("dbo.DataRegistrasis", "StatusKPEI", c => c.String(nullable: false));
            AddColumn("dbo.DataRegistrasis", "BankPembayaran", c => c.String(nullable: false));
            AddColumn("dbo.DataRegistrasis", "Email", c => c.String(nullable: false));
            AddColumn("dbo.DataRegistrasis", "ModalDisetor", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DataRegistrasis", "ModalDasar", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DataRegistrasis", "Nama", c => c.String(nullable: false));
            AddColumn("dbo.DataInformasiLains", "KTPFileUrl", c => c.String(maxLength: 256));
            DropForeignKey("dbo.DataInformasiLains", "RegistrasiBaruId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataSupportDocumentRegistrasis", "RegistrasiId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataPemegangSahamRegistrasis", "RegistrasiId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataPejabatBerwenangRegistrasis", "RegistrasiId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataKomisarisRegistrasis", "RegistrasiId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataDireksiRegistrasis", "RegistrasiId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataContactPersonRegistrasis", "RegistrasiId", "dbo.DataRegistrasis");
            DropIndex("dbo.DataInformasiLains", new[] { "RegistrasiBaruId" });
            DropIndex("dbo.DataSupportDocumentRegistrasis", new[] { "RegistrasiId" });
            DropIndex("dbo.DataPemegangSahamRegistrasis", new[] { "RegistrasiId" });
            DropIndex("dbo.DataPejabatBerwenangRegistrasis", new[] { "RegistrasiId" });
            DropIndex("dbo.DataKomisarisRegistrasis", new[] { "RegistrasiId" });
            DropIndex("dbo.DataDireksiRegistrasis", new[] { "RegistrasiId" });
            DropIndex("dbo.DataContactPersonRegistrasis", new[] { "RegistrasiId" });
            AlterColumn("dbo.DataRegistrasis", "NPWPFileUrl", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "SPAKFileUrl", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "SPABFileUrl", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "IjinUsahaFileUrl", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "LaporanKeuanganFileUrl", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "AktePerubahanFileUrl", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "AktePendirianFileUrl", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "AnggaranDasarFileUrl", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "AnggotaBursa", c => c.String(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "NPWP", c => c.String(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "StatusPerusahaan", c => c.String(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "TipeMemberPartisipan", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "TipeMemberAK", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "KategoriMember", c => c.String(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "NoPerjanjian", c => c.String(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "Notaris", c => c.String(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "BU", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "BK", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "MI", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "PEE", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "PPE", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "Fax", c => c.String(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "Telp", c => c.String(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "KodePos", c => c.String(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "Kota", c => c.String(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "Jalan", c => c.String(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "Gedung", c => c.String());
            AlterColumn("dbo.DataRegistrasis", "KodeAk", c => c.String(maxLength: 32));
            DropColumn("dbo.DataRegistrasis", "Approver2UserId");
            DropColumn("dbo.DataRegistrasis", "Approver1UserId");
            DropColumn("dbo.DataRegistrasis", "RejectReason");
            DropColumn("dbo.DataRegistrasis", "ActiveUtc");
            DropColumn("dbo.DataRegistrasis", "Status");
            DropColumn("dbo.DataRegistrasis", "PerjanjianPMEAsLenderFileUrl");
            DropColumn("dbo.DataRegistrasis", "PerjanjianPMEAsBorrowerFileUrl");
            DropColumn("dbo.DataRegistrasis", "AppPenerimaPinjamanBorrowerPMEFileUrl");
            DropColumn("dbo.DataRegistrasis", "AppPemberiPinjamanLenderPMEFileUrl");
            DropColumn("dbo.DataRegistrasis", "SuratKuasaPMEFileUrl");
            DropColumn("dbo.DataRegistrasis", "PerjanjianTitipEfekUntukPinjamFileUrl");
            DropColumn("dbo.DataRegistrasis", "ChecklistAkFileUrl");
            DropColumn("dbo.DataRegistrasis", "SuratKuasaFileUrl");
            DropColumn("dbo.DataRegistrasis", "SuratKuasaPindahBukuDanaFileUrl");
            DropColumn("dbo.DataRegistrasis", "PersetujuanPemberianWewenangFileUrl");
            DropColumn("dbo.DataRegistrasis", "PemberitahuanPenggunaanBankPembayaranFileUrl");
            DropColumn("dbo.DataRegistrasis", "PerjanjianLayananKliringPenjaminanTransaksiFileUrl");
            DropColumn("dbo.DataRegistrasis", "SKPEFileUrl");
            DropColumn("dbo.DataRegistrasis", "SPDKFileUrl");
            DropColumn("dbo.DataRegistrasis", "FormulirDaftarAkFileUrl");
            DropColumn("dbo.DataRegistrasis", "AkteFileUrl");
            DropColumn("dbo.DataRegistrasis", "TglAkte");
            DropColumn("dbo.DataRegistrasis", "NoAkte");
            DropColumn("dbo.DataRegistrasis", "ModalDasarRupiah");
            DropColumn("dbo.DataRegistrasis", "ModalDisetorRupiah");
            DropColumn("dbo.DataRegistrasis", "Website");
            DropColumn("dbo.DataRegistrasis", "NamaPerusahaan");
            DropColumn("dbo.DataPejabatBerwenangItems", "KtpFileUrl");
            DropColumn("dbo.DataInformasiLains", "RegistrasiBaruId");
            DropColumn("dbo.DataKomisaris", "KtpFileUrl");
            DropColumn("dbo.DataDireksis", "KtpFileUrl");
            DropTable("dbo.DataSupportDocumentRegistrasis");
            DropTable("dbo.DataPemegangSahamRegistrasis");
            DropTable("dbo.DataPejabatBerwenangRegistrasis");
            DropTable("dbo.DataKomisarisRegistrasis");
            DropTable("dbo.DataDireksiRegistrasis");
            DropTable("dbo.DataContactPersonRegistrasis");
        }
    }
}
