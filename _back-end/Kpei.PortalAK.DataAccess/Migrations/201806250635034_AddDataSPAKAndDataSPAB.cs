namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDataSPAKAndDataSPAB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataSPABs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        NoSPAB = c.String(nullable: false, maxLength: 128),
                        TanggalSPAB = c.DateTime(nullable: false),
                        SuratKeteranganFileUrl = c.String(maxLength: 256),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataSPAKs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        NoSPAK = c.String(nullable: false, maxLength: 128),
                        TanggalSPAK = c.DateTime(nullable: false),
                        SuratKeteranganFileUrl = c.String(maxLength: 256),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.DataPMEs", "SuratKeteranganFileUrl", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataPMEs", "SuratKeteranganFileUrl");
            DropTable("dbo.DataSPAKs");
            DropTable("dbo.DataSPABs");
        }
    }
}
