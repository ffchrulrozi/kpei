namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeKodePerusahaan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataTMs", "KodePerusahaan", c => c.String(nullable: false, maxLength: 32));
            DropColumn("dbo.DataTMs", "KodeAk");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataTMs", "KodeAk", c => c.String(nullable: false, maxLength: 32));
            DropColumn("dbo.DataTMs", "KodePerusahaan");
        }
    }
}
