namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSpakAndSpabToRegistrasi : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataRegistrasis", "NoSPAK", c => c.String(maxLength: 128));
            AddColumn("dbo.DataRegistrasis", "TanggalSPAKUtc", c => c.DateTime());
            AddColumn("dbo.DataRegistrasis", "NoSPAB", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataRegistrasis", "TanggalSPABUtc", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataRegistrasis", "TanggalSPABUtc");
            DropColumn("dbo.DataRegistrasis", "NoSPAB");
            DropColumn("dbo.DataRegistrasis", "TanggalSPAKUtc");
            DropColumn("dbo.DataRegistrasis", "NoSPAK");
        }
    }
}
