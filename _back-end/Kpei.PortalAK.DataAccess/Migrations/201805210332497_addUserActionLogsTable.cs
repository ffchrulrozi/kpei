namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUserActionLogsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserActionLogs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(maxLength: 128),
                        UserName = c.String(maxLength: 128),
                        Message = c.String(nullable: false, maxLength: 512),
                        DataJson = c.String(maxLength: 2048),
                        RelatedEntityName = c.String(maxLength: 128),
                        RelatedEntityId = c.String(maxLength: 128),
                        RequestMethod = c.String(nullable: false, maxLength: 10),
                        RequestUrl = c.String(nullable: false, maxLength: 256),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserActionLogs");
        }
    }
}
