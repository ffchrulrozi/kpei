namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mergeWithUpdateJenisUsaha : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataJenisUsahas", "SuratKeteranganFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataJenisUsahas", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataJenisUsahas", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataJenisUsahas", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataJenisUsahas", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataJenisUsahas", "Approver2UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataBankPembayarans", "BankPembayaran", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "NoSimO", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "NoPerjanjian", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "JenisPerjanjian", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "StatusPerjanjian", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.DataJenisUsahas", "TglAktifUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataJenisUsahas", "TglAktifUtc", c => c.DateTime());
            AlterColumn("dbo.DataPerjanjianEBUs", "StatusPerjanjian", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "JenisPerjanjian", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "NoPerjanjian", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "NoSimO", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataBankPembayarans", "BankPembayaran", c => c.String(maxLength: 128));
            DropColumn("dbo.DataJenisUsahas", "Approver2UserId");
            DropColumn("dbo.DataJenisUsahas", "Approver1UserId");
            DropColumn("dbo.DataJenisUsahas", "RejectReason");
            DropColumn("dbo.DataJenisUsahas", "ActiveUtc");
            DropColumn("dbo.DataJenisUsahas", "Status");
            DropColumn("dbo.DataJenisUsahas", "SuratKeteranganFileUrl");
        }
    }
}
