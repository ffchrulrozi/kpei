namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeTahunColumnToIntegerForKeuanganLaporanTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.KeuanganLaporans", "Tahun", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.KeuanganLaporans", "Tahun", c => c.String(nullable: false, maxLength: 10));
        }
    }
}
