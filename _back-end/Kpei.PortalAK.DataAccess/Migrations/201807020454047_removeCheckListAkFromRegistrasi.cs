namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeCheckListAkFromRegistrasi : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.DataRegistrasis", "ChecklistAkFileUrl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataRegistrasis", "ChecklistAkFileUrl", c => c.String(maxLength: 256));
        }
    }
}
