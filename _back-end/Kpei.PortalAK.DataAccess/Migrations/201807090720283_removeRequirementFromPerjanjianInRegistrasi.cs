namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeRequirementFromPerjanjianInRegistrasi : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataRegistrasis", "NoPerjanjian", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "TglPerjanjianUtc", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataRegistrasis", "TglPerjanjianUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "NoPerjanjian", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
