namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTipeMemberDanHirarkiAndPerjanjian : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataPerjanjians",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        NoPerjanjian = c.String(nullable: false, maxLength: 128),
                        JenisPerjanjian = c.String(nullable: false, maxLength: 128),
                        TglPerjanjian = c.DateTime(nullable: false),
                        SuratKeteranganFileUrl = c.String(maxLength: 256),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataTipeMemberDanHirarkis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        JenisKeanggotaan = c.String(nullable: false),
                        Parent = c.String(nullable: false),
                        SuratKeteranganFileUrl = c.String(maxLength: 256),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DataTipeMemberDanHirarkis");
            DropTable("dbo.DataPerjanjians");
        }
    }
}
