namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNilaiSurveiToSurvei : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Surveis", "Nilai", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Surveis", "Nilai");
        }
    }
}
