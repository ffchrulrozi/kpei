namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeDoubleApproverAndAddFileSuratPermohonanInPincode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PincodeRequests", "SuratPermohonanFileUrl", c => c.String(maxLength: 256));
            DropColumn("dbo.PincodeRequests", "Approver1UserId");
            DropColumn("dbo.PincodeRequests", "Approver2UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PincodeRequests", "Approver2UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.PincodeRequests", "Approver1UserId", c => c.String(maxLength: 128));
            DropColumn("dbo.PincodeRequests", "SuratPermohonanFileUrl");
        }
    }
}
