namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeAdminUserFromPerjanjianKBOS : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.DataPerjanjianKBOS", "AdminUser");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataPerjanjianKBOS", "AdminUser", c => c.String(maxLength: 128));
        }
    }
}
