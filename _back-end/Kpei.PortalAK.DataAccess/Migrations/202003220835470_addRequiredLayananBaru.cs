namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRequiredLayananBaru : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LayananBarus", "FieldRequired", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LayananBarus", "FieldRequired");
        }
    }
}
