namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDokumenPerjanjianFileToDataRegistrasi : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataRegistrasis", "PerjanjianAkuDenganKPEIFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "PerjanjianAkiDenganKPEIFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "PerjanjianPEDDenganKPEIFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "PerjanjianPartisipanDenganKPEIFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "PerjanjianAkuDenganTradingMemberFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "PerjanjianPEDDenganABSponsorFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "PerjanjianPortabilityFileUrl", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataRegistrasis", "PerjanjianPortabilityFileUrl");
            DropColumn("dbo.DataRegistrasis", "PerjanjianPEDDenganABSponsorFileUrl");
            DropColumn("dbo.DataRegistrasis", "PerjanjianAkuDenganTradingMemberFileUrl");
            DropColumn("dbo.DataRegistrasis", "PerjanjianPartisipanDenganKPEIFileUrl");
            DropColumn("dbo.DataRegistrasis", "PerjanjianPEDDenganKPEIFileUrl");
            DropColumn("dbo.DataRegistrasis", "PerjanjianAkiDenganKPEIFileUrl");
            DropColumn("dbo.DataRegistrasis", "PerjanjianAkuDenganKPEIFileUrl");
        }
    }
}
