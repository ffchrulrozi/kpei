namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeRequiredFromPejabatBerwenang : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataPejabatBerwenangs", "Nama", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataPejabatBerwenangs", "Jabatan", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataPejabatBerwenangs", "Email", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataPejabatBerwenangs", "Telp", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataPejabatBerwenangs", "HP", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataPejabatBerwenangs", "HP", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataPejabatBerwenangs", "Telp", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataPejabatBerwenangs", "Email", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataPejabatBerwenangs", "Jabatan", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataPejabatBerwenangs", "Nama", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
