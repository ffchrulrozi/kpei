namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renamesetting201803221128 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.SettingPincodes", newName: "Settings");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Settings", newName: "SettingPincodes");
        }
    }
}
