namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeRequirementInInformasiLainAndJenisUsaha : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataRegistrasis", "NPWP", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "AnggotaBursa", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataInformasiLains", "NPWP", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataInformasiLains", "AnggotaBursa", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataInformasiLains", "AnggotaBursa", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataInformasiLains", "NPWP", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "AnggotaBursa", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "NPWP", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
