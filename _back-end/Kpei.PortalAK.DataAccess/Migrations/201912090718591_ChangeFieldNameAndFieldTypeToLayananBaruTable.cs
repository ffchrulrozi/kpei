namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeFieldNameAndFieldTypeToLayananBaruTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LayananBarus", "FieldName", c => c.String(maxLength: 256));
            AddColumn("dbo.LayananBarus", "FieldType", c => c.String(maxLength: 256));
            DropColumn("dbo.LayananBarus", "Field");
            DropColumn("dbo.LayananBarus", "Value");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LayananBarus", "Value", c => c.String());
            AddColumn("dbo.LayananBarus", "Field", c => c.String(maxLength: 256));
            DropColumn("dbo.LayananBarus", "FieldType");
            DropColumn("dbo.LayananBarus", "FieldName");
        }
    }
}
