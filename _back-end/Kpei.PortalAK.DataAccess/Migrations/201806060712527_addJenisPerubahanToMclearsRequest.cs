namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addJenisPerubahanToMclearsRequest : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MclearsRequests", "JenisPerubahan", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.MclearsPesertas", "Nama", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.MclearsPesertas", "Jabatan", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.MclearsPesertas", "Email", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.MclearsPesertas", "HP", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MclearsPesertas", "HP", c => c.String());
            AlterColumn("dbo.MclearsPesertas", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "Jabatan", c => c.String(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "Nama", c => c.String(nullable: false));
            DropColumn("dbo.MclearsRequests", "JenisPerubahan");
        }
    }
}
