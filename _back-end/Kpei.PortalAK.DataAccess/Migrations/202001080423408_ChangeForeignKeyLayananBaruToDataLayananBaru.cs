namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeForeignKeyLayananBaruToDataLayananBaru : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DataLayananBarus", "LayananBaruId", "dbo.LayananBarus");
            DropIndex("dbo.DataLayananBarus", new[] { "LayananBaruId" });
            AlterColumn("dbo.DataLayananBarus", "LayananBaruId", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataLayananBarus", "LayananBaruId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.DataLayananBarus", "LayananBaruId");
            AddForeignKey("dbo.DataLayananBarus", "LayananBaruId", "dbo.LayananBarus", "Id", cascadeDelete: true);
        }
    }
}
