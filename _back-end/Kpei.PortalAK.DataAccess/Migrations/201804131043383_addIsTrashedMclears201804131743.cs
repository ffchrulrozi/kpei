namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addIsTrashedMclears201804131743 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MclearsRequests", "isTrashed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MclearsRequests", "isTrashed");
        }
    }
}
