namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeDataProfilTableAddMoreSupportDocumentFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataSupportDocumentRegistrasis", "NamaDoc", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.DataSupportDocumentRegistrasis", "DocFileUrl", c => c.String(nullable: false, maxLength: 256));
            DropTable("dbo.DataProfils");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DataProfils",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        Nama = c.String(nullable: false, maxLength: 128),
                        ModalDasar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ModalDisetor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Alamat = c.String(nullable: false, maxLength: 128),
                        BankPembayaran = c.String(nullable: false, maxLength: 128),
                        StatusKPEI = c.String(nullable: false, maxLength: 128),
                        StatusBursa = c.String(nullable: false, maxLength: 128),
                        PPE = c.String(maxLength: 128),
                        PEE = c.String(maxLength: 128),
                        MI = c.String(maxLength: 128),
                        BK = c.String(maxLength: 128),
                        BU = c.String(maxLength: 128),
                        NoPerjanjian = c.String(nullable: false, maxLength: 128),
                        TglPerjanjianUtc = c.DateTime(nullable: false),
                        KategoriMember = c.String(nullable: false, maxLength: 128),
                        TipeMemberAK = c.String(nullable: false, maxLength: 128),
                        TipeMemberPartisipan = c.String(nullable: false, maxLength: 128),
                        StatusPerusahaan = c.String(nullable: false, maxLength: 128),
                        NPWP = c.String(nullable: false, maxLength: 128),
                        AnggotaBursa = c.String(nullable: false, maxLength: 128),
                        NoSPAK = c.String(nullable: false, maxLength: 128),
                        TglSPAKUtc = c.DateTime(nullable: false),
                        NoSPAB = c.String(nullable: false, maxLength: 128),
                        TglSPABUtc = c.DateTime(nullable: false),
                        AnggaranDasarFileUrl = c.String(maxLength: 256),
                        AktePendirianFileUrl = c.String(maxLength: 256),
                        AktePerubahanFileUrl = c.String(maxLength: 256),
                        LaporanKeuanganFileUrl = c.String(maxLength: 256),
                        IjinUsahaFileUrl = c.String(maxLength: 256),
                        SPABFileUrl = c.String(maxLength: 256),
                        SPAKFileUrl = c.String(maxLength: 256),
                        NPWPFileUrl = c.String(maxLength: 256),
                        KTPFileUrl = c.String(maxLength: 256),
                        CR = c.Single(nullable: false),
                        ROA = c.Single(nullable: false),
                        ROE = c.Single(nullable: false),
                        NetProfit = c.Single(nullable: false),
                        DAR = c.Single(nullable: false),
                        DER = c.Single(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.DataSupportDocumentRegistrasis", "DocFileUrl", c => c.String());
            DropColumn("dbo.DataSupportDocumentRegistrasis", "NamaDoc");
        }
    }
}
