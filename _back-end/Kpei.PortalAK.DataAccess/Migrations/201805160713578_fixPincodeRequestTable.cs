namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixPincodeRequestTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PincodeRequests", "Alasan", c => c.String(nullable: false, maxLength: 1024));
            AlterColumn("dbo.PincodeRequests", "StatusRequest", c => c.String(nullable: false, maxLength: 32));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PincodeRequests", "StatusRequest", c => c.String(nullable: false));
            AlterColumn("dbo.PincodeRequests", "Alasan", c => c.String(nullable: false));
        }
    }
}
