namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editNullableBooleanInMclearsPeserta : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MclearsPesertas", "AlertPC", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertPS", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertHTS", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertHTR", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertBACS", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertACS", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertTLP", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertTLS", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertTL5", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertMKBDA", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertFCOLWA", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertCLA", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertOCA", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertMCCA", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertDCDA", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertDCRA", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertMCA", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertCPA", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertL1", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertL3", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertL7", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertHPHFA", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "AlertBroadcast", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestPC", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestPS", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestHTS", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestHTR", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestPPR", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestMKBDR", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestOCR", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestHaircut", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestDCDR", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestDCRR", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestMCR", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestCPR", c => c.Boolean());
            AlterColumn("dbo.MclearsPesertas", "OnRequestHPHFR", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MclearsPesertas", "OnRequestHPHFR", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "OnRequestCPR", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "OnRequestMCR", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "OnRequestDCRR", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "OnRequestDCDR", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "OnRequestHaircut", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "OnRequestOCR", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "OnRequestMKBDR", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "OnRequestPPR", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "OnRequestHTR", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "OnRequestHTS", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "OnRequestPS", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "OnRequestPC", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertBroadcast", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertHPHFA", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertL7", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertL3", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertL1", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertCPA", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertMCA", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertDCRA", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertDCDA", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertMCCA", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertOCA", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertCLA", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertFCOLWA", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertMKBDA", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertTL5", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertTLS", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertTLP", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertACS", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertBACS", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertHTR", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertHTS", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertPS", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "AlertPC", c => c.Boolean(nullable: false));
        }
    }
}
