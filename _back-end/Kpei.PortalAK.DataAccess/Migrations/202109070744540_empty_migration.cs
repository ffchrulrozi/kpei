namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class empty_migration : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.DataRegistrasis", "SKKemenkumhamFileUrl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataRegistrasis", "SKKemenkumhamFileUrl", c => c.String(maxLength: 256));
        }
    }
}
