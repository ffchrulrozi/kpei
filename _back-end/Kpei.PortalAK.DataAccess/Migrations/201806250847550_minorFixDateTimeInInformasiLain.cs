namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class minorFixDateTimeInInformasiLain : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataInformasiLains", "TglSerahDokumenUtc", c => c.DateTime());
            AlterColumn("dbo.DataInformasiLains", "TglSerahDanaMinCashUtc", c => c.DateTime());
            AlterColumn("dbo.DataInformasiLains", "TglAktaPengikatanUtc", c => c.DateTime());
            AlterColumn("dbo.DataInformasiLains", "TglRekomendasiUtc", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataInformasiLains", "TglRekomendasiUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataInformasiLains", "TglAktaPengikatanUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataInformasiLains", "TglSerahDanaMinCashUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataInformasiLains", "TglSerahDokumenUtc", c => c.DateTime(nullable: false));
        }
    }
}
