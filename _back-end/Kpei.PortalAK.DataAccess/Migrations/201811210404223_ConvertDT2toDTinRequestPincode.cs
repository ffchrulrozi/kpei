namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConvertDT2toDTinRequestPincode : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PincodeRequests", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PincodeRequests", "UpdatedUtc", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PincodeRequests", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.PincodeRequests", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
    }
}
