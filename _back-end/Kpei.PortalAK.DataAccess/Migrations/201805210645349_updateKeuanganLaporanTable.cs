namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateKeuanganLaporanTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.KeuanganLaporans", "ArusKasFileId", "dbo.Files");
            DropForeignKey("dbo.KeuanganLaporans", "LabaRugiFileId", "dbo.Files");
            DropForeignKey("dbo.KeuanganLaporans", "NeracaFileId", "dbo.Files");
            DropForeignKey("dbo.KeuanganLaporans", "NewRegId", "dbo.DataRegistrasis");
            DropIndex("dbo.KeuanganLaporans", new[] { "NeracaFileId" });
            DropIndex("dbo.KeuanganLaporans", new[] { "LabaRugiFileId" });
            DropIndex("dbo.KeuanganLaporans", new[] { "ArusKasFileId" });
            DropIndex("dbo.KeuanganLaporans", new[] { "NewRegId" });
            AddColumn("dbo.KeuanganLaporans", "Periode", c => c.String(nullable: false, maxLength: 32));
            AddColumn("dbo.KeuanganLaporans", "StatusRequest", c => c.String(nullable: false, maxLength: 32));
            AddColumn("dbo.KeuanganLaporans", "NeracaFileUrl", c => c.String(nullable: false, maxLength: 256));
            AddColumn("dbo.KeuanganLaporans", "LabaRugiFileUrl", c => c.String(nullable: false, maxLength: 256));
            AddColumn("dbo.KeuanganLaporans", "ArusKasFileUrl", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.KeuanganLaporans", "Tahun", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.KeuanganLaporans", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.KeuanganLaporans", "UpdatedUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.KeuanganLaporans", "Jenis");
            DropColumn("dbo.KeuanganLaporans", "NeracaFileId");
            DropColumn("dbo.KeuanganLaporans", "LabaRugiFileId");
            DropColumn("dbo.KeuanganLaporans", "ArusKasFileId");
            DropColumn("dbo.KeuanganLaporans", "CR");
            DropColumn("dbo.KeuanganLaporans", "ROA");
            DropColumn("dbo.KeuanganLaporans", "ROE");
            DropColumn("dbo.KeuanganLaporans", "NetProfit");
            DropColumn("dbo.KeuanganLaporans", "DAR");
            DropColumn("dbo.KeuanganLaporans", "DER");
            DropColumn("dbo.KeuanganLaporans", "isTrashed");
            DropColumn("dbo.KeuanganLaporans", "NewRegId");
            DropColumn("dbo.KeuanganLaporans", "TglAktifUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.KeuanganLaporans", "TglAktifUtc", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.KeuanganLaporans", "NewRegId", c => c.String(maxLength: 128));
            AddColumn("dbo.KeuanganLaporans", "isTrashed", c => c.Boolean(nullable: false));
            AddColumn("dbo.KeuanganLaporans", "DER", c => c.Single(nullable: false));
            AddColumn("dbo.KeuanganLaporans", "DAR", c => c.Single(nullable: false));
            AddColumn("dbo.KeuanganLaporans", "NetProfit", c => c.Single(nullable: false));
            AddColumn("dbo.KeuanganLaporans", "ROE", c => c.Single(nullable: false));
            AddColumn("dbo.KeuanganLaporans", "ROA", c => c.Single(nullable: false));
            AddColumn("dbo.KeuanganLaporans", "CR", c => c.Single(nullable: false));
            AddColumn("dbo.KeuanganLaporans", "ArusKasFileId", c => c.String(maxLength: 128));
            AddColumn("dbo.KeuanganLaporans", "LabaRugiFileId", c => c.String(maxLength: 128));
            AddColumn("dbo.KeuanganLaporans", "NeracaFileId", c => c.String(maxLength: 128));
            AddColumn("dbo.KeuanganLaporans", "Jenis", c => c.String(nullable: false));
            AlterColumn("dbo.KeuanganLaporans", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.KeuanganLaporans", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.KeuanganLaporans", "Tahun", c => c.String(nullable: false));
            DropColumn("dbo.KeuanganLaporans", "ArusKasFileUrl");
            DropColumn("dbo.KeuanganLaporans", "LabaRugiFileUrl");
            DropColumn("dbo.KeuanganLaporans", "NeracaFileUrl");
            DropColumn("dbo.KeuanganLaporans", "StatusRequest");
            DropColumn("dbo.KeuanganLaporans", "Periode");
            CreateIndex("dbo.KeuanganLaporans", "NewRegId");
            CreateIndex("dbo.KeuanganLaporans", "ArusKasFileId");
            CreateIndex("dbo.KeuanganLaporans", "LabaRugiFileId");
            CreateIndex("dbo.KeuanganLaporans", "NeracaFileId");
            AddForeignKey("dbo.KeuanganLaporans", "NewRegId", "dbo.DataRegistrasis", "Id");
            AddForeignKey("dbo.KeuanganLaporans", "NeracaFileId", "dbo.Files", "Id");
            AddForeignKey("dbo.KeuanganLaporans", "LabaRugiFileId", "dbo.Files", "Id");
            AddForeignKey("dbo.KeuanganLaporans", "ArusKasFileId", "dbo.Files", "Id");
        }
    }
}
