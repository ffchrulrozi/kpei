namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeChangeRequestIdForeignKeyFromBaseAknpDataModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DataAlamats", "ChangeRequestId", "dbo.DataChangeRequests");
            DropIndex("dbo.DataAlamats", new[] { "ChangeRequestId" });
            DropColumn("dbo.DataAlamats", "ChangeRequestId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataAlamats", "ChangeRequestId", c => c.String(maxLength: 128));
            CreateIndex("dbo.DataAlamats", "ChangeRequestId");
            AddForeignKey("dbo.DataAlamats", "ChangeRequestId", "dbo.DataChangeRequests", "Id");
        }
    }
}
