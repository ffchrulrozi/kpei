namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNamaAK201804101456 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventPesertas", "NamaAK", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventPesertas", "NamaAK");
        }
    }
}
