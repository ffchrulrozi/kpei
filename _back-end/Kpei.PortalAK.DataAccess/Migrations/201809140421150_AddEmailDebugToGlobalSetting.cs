namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailDebugToGlobalSetting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GlobalSettings", "EmailDebugMode", c => c.Boolean(nullable: false));
            AddColumn("dbo.GlobalSettings", "EmailDebugAddress", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.GlobalSettings", "EmailDebugAddress");
            DropColumn("dbo.GlobalSettings", "EmailDebugMode");
        }
    }
}
