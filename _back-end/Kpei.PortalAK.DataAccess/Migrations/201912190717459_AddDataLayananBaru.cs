namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDataLayananBaru : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataLayananBarus",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        TipeMemberAK = c.String(maxLength: 128),
                        TipeMemberPartisipan = c.String(maxLength: 256),
                        FieldName = c.String(maxLength: 256),
                        FieldType = c.String(maxLength: 256),
                        FieldValue = c.String(maxLength: 256),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DataLayananBarus");
        }
    }
}
