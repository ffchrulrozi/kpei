namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addistrashedevent2_201804061343 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "isTrashed", c => c.Boolean(nullable: false));
            DropColumn("dbo.DataStatusKPEIs", "isTrashed");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataStatusKPEIs", "isTrashed", c => c.Boolean(nullable: false));
            DropColumn("dbo.Events", "isTrashed");
        }
    }
}
