namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adjustKeuangan201804171057 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.KeuanganLaporans", "NeracaFileId", "dbo.Files");
            DropIndex("dbo.KeuanganLaporans", new[] { "NeracaFileId" });
            AlterColumn("dbo.KeuanganLaporans", "NeracaFileId", c => c.String(maxLength: 128));
            CreateIndex("dbo.KeuanganLaporans", "NeracaFileId");
            AddForeignKey("dbo.KeuanganLaporans", "NeracaFileId", "dbo.Files", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KeuanganLaporans", "NeracaFileId", "dbo.Files");
            DropIndex("dbo.KeuanganLaporans", new[] { "NeracaFileId" });
            AlterColumn("dbo.KeuanganLaporans", "NeracaFileId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.KeuanganLaporans", "NeracaFileId");
            AddForeignKey("dbo.KeuanganLaporans", "NeracaFileId", "dbo.Files", "Id", cascadeDelete: true);
        }
    }
}
