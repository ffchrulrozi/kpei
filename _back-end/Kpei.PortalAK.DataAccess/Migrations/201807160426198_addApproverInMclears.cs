namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addApproverInMclears : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MclearsRequests", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.MclearsRequests", "Approver2UserId", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MclearsRequests", "Approver2UserId");
            DropColumn("dbo.MclearsRequests", "Approver1UserId");
        }
    }
}
