namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class edit201803271719 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MclearsRequests", "JlhPeserta", c => c.Int(nullable: false));
            AddColumn("dbo.MclearsRequests", "JlhLayanan", c => c.Int(nullable: false));
            DropColumn("dbo.MclearsRequests", "JlhLayananAlert");
            DropColumn("dbo.MclearsRequests", "JlhLayananRequest");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MclearsRequests", "JlhLayananRequest", c => c.Int(nullable: false));
            AddColumn("dbo.MclearsRequests", "JlhLayananAlert", c => c.Int(nullable: false));
            DropColumn("dbo.MclearsRequests", "JlhLayanan");
            DropColumn("dbo.MclearsRequests", "JlhPeserta");
        }
    }
}
