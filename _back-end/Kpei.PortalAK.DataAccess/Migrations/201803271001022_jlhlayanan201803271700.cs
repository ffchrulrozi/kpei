namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class jlhlayanan201803271700 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MclearsRequests", "JlhLayananAlert", c => c.Int(nullable: false));
            AddColumn("dbo.MclearsRequests", "JlhLayananRequest", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MclearsRequests", "JlhLayananRequest");
            DropColumn("dbo.MclearsRequests", "JlhLayananAlert");
        }
    }
}
