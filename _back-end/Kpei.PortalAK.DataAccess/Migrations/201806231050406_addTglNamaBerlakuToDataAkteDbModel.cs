namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTglNamaBerlakuToDataAkteDbModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataAktes", "TglNamaBerlaku", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataAktes", "TglNamaBerlaku");
        }
    }
}
