namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDataTradingMember : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataTradingMembers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        TipeMemberAK = c.String(maxLength: 128),
                        RegistrasiId = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataRegistrasis", t => t.RegistrasiId, cascadeDelete: true)
                .Index(t => t.RegistrasiId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DataTradingMembers", "RegistrasiId", "dbo.DataRegistrasis");
            DropIndex("dbo.DataTradingMembers", new[] { "RegistrasiId" });
            DropTable("dbo.DataTradingMembers");
        }
    }
}
