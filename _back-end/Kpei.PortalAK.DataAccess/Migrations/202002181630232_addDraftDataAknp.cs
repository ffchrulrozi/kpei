namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDraftDataAknp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataAktes", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataAktes", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataAlamats", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataAlamats", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataBankPembayarans", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataBankPembayarans", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataContactPersons", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataContactPersons", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataRegistrasis", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataRegistrasis", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataInformasiLains", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataInformasiLains", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataJenisUsahas", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataJenisUsahas", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPejabatBerwenangs", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataPejabatBerwenangs", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPerjanjians", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataPerjanjians", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPerjanjianEBUs", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataPerjanjianEBUs", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPerjanjianKBOS", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataPerjanjianKBOS", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPMEs", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataPMEs", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataSPABs", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataSPABs", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataSPAKs", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataSPAKs", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataStatusBursas", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataStatusBursas", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataStatusKPEIs", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "UserDraft", c => c.String(maxLength: 128));
            AddColumn("dbo.DataTipeMembers", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.DataTipeMembers", "UserDraft", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataTipeMembers", "UserDraft");
            DropColumn("dbo.DataTipeMembers", "Draft");
            DropColumn("dbo.DataStatusKPEIs", "UserDraft");
            DropColumn("dbo.DataStatusKPEIs", "Draft");
            DropColumn("dbo.DataStatusBursas", "UserDraft");
            DropColumn("dbo.DataStatusBursas", "Draft");
            DropColumn("dbo.DataSPAKs", "UserDraft");
            DropColumn("dbo.DataSPAKs", "Draft");
            DropColumn("dbo.DataSPABs", "UserDraft");
            DropColumn("dbo.DataSPABs", "Draft");
            DropColumn("dbo.DataPMEs", "UserDraft");
            DropColumn("dbo.DataPMEs", "Draft");
            DropColumn("dbo.DataPerjanjianKBOS", "UserDraft");
            DropColumn("dbo.DataPerjanjianKBOS", "Draft");
            DropColumn("dbo.DataPerjanjianEBUs", "UserDraft");
            DropColumn("dbo.DataPerjanjianEBUs", "Draft");
            DropColumn("dbo.DataPerjanjians", "UserDraft");
            DropColumn("dbo.DataPerjanjians", "Draft");
            DropColumn("dbo.DataPejabatBerwenangs", "UserDraft");
            DropColumn("dbo.DataPejabatBerwenangs", "Draft");
            DropColumn("dbo.DataJenisUsahas", "UserDraft");
            DropColumn("dbo.DataJenisUsahas", "Draft");
            DropColumn("dbo.DataInformasiLains", "UserDraft");
            DropColumn("dbo.DataInformasiLains", "Draft");
            DropColumn("dbo.DataRegistrasis", "UserDraft");
            DropColumn("dbo.DataRegistrasis", "Draft");
            DropColumn("dbo.DataContactPersons", "UserDraft");
            DropColumn("dbo.DataContactPersons", "Draft");
            DropColumn("dbo.DataBankPembayarans", "UserDraft");
            DropColumn("dbo.DataBankPembayarans", "Draft");
            DropColumn("dbo.DataAlamats", "UserDraft");
            DropColumn("dbo.DataAlamats", "Draft");
            DropColumn("dbo.DataAktes", "UserDraft");
            DropColumn("dbo.DataAktes", "Draft");
        }
    }
}
