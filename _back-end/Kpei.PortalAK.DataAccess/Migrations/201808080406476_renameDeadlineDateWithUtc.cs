namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renameDeadlineDateWithUtc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Deadlines", "KeuanganAwalPeriodeUtc", c => c.DateTime());
            AddColumn("dbo.Deadlines", "KeuanganAkhirPeriodeUtc", c => c.DateTime());
            DropColumn("dbo.Deadlines", "KeuanganAwalPeriode");
            DropColumn("dbo.Deadlines", "KeuanganAkhirPeriode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deadlines", "KeuanganAkhirPeriode", c => c.DateTime());
            AddColumn("dbo.Deadlines", "KeuanganAwalPeriode", c => c.DateTime());
            DropColumn("dbo.Deadlines", "KeuanganAkhirPeriodeUtc");
            DropColumn("dbo.Deadlines", "KeuanganAwalPeriodeUtc");
        }
    }
}
