namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRingkasanStringFieldToDataAkteDbModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataAktes", "Ringkasan", c => c.String(nullable: false, maxLength: 1024));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataAktes", "Ringkasan");
        }
    }
}
