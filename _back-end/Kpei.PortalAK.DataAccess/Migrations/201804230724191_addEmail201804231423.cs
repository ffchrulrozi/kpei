namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addEmail201804231423 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataRegistrasis", "Email", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataRegistrasis", "Email");
        }
    }
}
