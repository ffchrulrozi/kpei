namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editmclearsrequest201803261435 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MclearsRequests", "DetailId", "dbo.MclearsPesertas");
            DropIndex("dbo.MclearsRequests", new[] { "DetailId" });
            AddColumn("dbo.MclearsPesertas", "RequestId", c => c.String(maxLength: 128));
            CreateIndex("dbo.MclearsPesertas", "RequestId");
            AddForeignKey("dbo.MclearsPesertas", "RequestId", "dbo.MclearsRequests", "Id");
            DropColumn("dbo.MclearsRequests", "DetailId");
            DropTable("dbo.MclearsRequestToPesertas");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MclearsRequestToPesertas",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RequestId = c.String(),
                        DetailId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.MclearsRequests", "DetailId", c => c.String(nullable: false, maxLength: 128));
            DropForeignKey("dbo.MclearsPesertas", "RequestId", "dbo.MclearsRequests");
            DropIndex("dbo.MclearsPesertas", new[] { "RequestId" });
            DropColumn("dbo.MclearsPesertas", "RequestId");
            CreateIndex("dbo.MclearsRequests", "DetailId");
            AddForeignKey("dbo.MclearsRequests", "DetailId", "dbo.MclearsPesertas", "Id", cascadeDelete: true);
        }
    }
}
