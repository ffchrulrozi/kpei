namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateDataPmeKbosInformasiLain : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataInformasiLains", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataInformasiLains", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataInformasiLains", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataInformasiLains", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataInformasiLains", "Approver2UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPerjanjianEBUs", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataPerjanjianEBUs", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataPerjanjianEBUs", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataPerjanjianEBUs", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPerjanjianEBUs", "Approver2UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPerjanjianKBOS", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataPerjanjianKBOS", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataPerjanjianKBOS", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataPerjanjianKBOS", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPerjanjianKBOS", "Approver2UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPMEs", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataPMEs", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataPMEs", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataPMEs", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPMEs", "Approver2UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "NoSimO", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "NoPerjanjian", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "JenisPerjanjian", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "StatusPerjanjian", c => c.String(maxLength: 128));
            DropColumn("dbo.DataInformasiLains", "TglAktifUtc");
            DropColumn("dbo.DataPerjanjianEBUs", "TglAktifUtc");
            DropColumn("dbo.DataPerjanjianKBOS", "TglAktifUtc");
            DropColumn("dbo.DataPMEs", "TglAktifUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataPMEs", "TglAktifUtc", c => c.DateTime());
            AddColumn("dbo.DataPerjanjianKBOS", "TglAktifUtc", c => c.DateTime());
            AddColumn("dbo.DataPerjanjianEBUs", "TglAktifUtc", c => c.DateTime());
            AddColumn("dbo.DataInformasiLains", "TglAktifUtc", c => c.DateTime());
            AlterColumn("dbo.DataPerjanjianEBUs", "StatusPerjanjian", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "JenisPerjanjian", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "NoPerjanjian", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "NoSimO", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.DataPMEs", "Approver2UserId");
            DropColumn("dbo.DataPMEs", "Approver1UserId");
            DropColumn("dbo.DataPMEs", "RejectReason");
            DropColumn("dbo.DataPMEs", "ActiveUtc");
            DropColumn("dbo.DataPMEs", "Status");
            DropColumn("dbo.DataPerjanjianKBOS", "Approver2UserId");
            DropColumn("dbo.DataPerjanjianKBOS", "Approver1UserId");
            DropColumn("dbo.DataPerjanjianKBOS", "RejectReason");
            DropColumn("dbo.DataPerjanjianKBOS", "ActiveUtc");
            DropColumn("dbo.DataPerjanjianKBOS", "Status");
            DropColumn("dbo.DataPerjanjianEBUs", "Approver2UserId");
            DropColumn("dbo.DataPerjanjianEBUs", "Approver1UserId");
            DropColumn("dbo.DataPerjanjianEBUs", "RejectReason");
            DropColumn("dbo.DataPerjanjianEBUs", "ActiveUtc");
            DropColumn("dbo.DataPerjanjianEBUs", "Status");
            DropColumn("dbo.DataInformasiLains", "Approver2UserId");
            DropColumn("dbo.DataInformasiLains", "Approver1UserId");
            DropColumn("dbo.DataInformasiLains", "RejectReason");
            DropColumn("dbo.DataInformasiLains", "ActiveUtc");
            DropColumn("dbo.DataInformasiLains", "Status");
        }
    }
}
