namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class settingpincode201803201245 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SettingPincodes",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Field = c.String(),
                        Value = c.String(),
                        isTrashed = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SettingPincodes");
        }
    }
}
