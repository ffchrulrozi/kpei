namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMaxLengthToEventPembicara : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EventPembicaras", "EventId", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EventPembicaras", "EventId", c => c.String(nullable: false));
        }
    }
}
