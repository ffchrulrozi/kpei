namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeDoubleApprovalInMclears : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.MclearsRequests", "Approver1UserId");
            DropColumn("dbo.MclearsRequests", "Approver2UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MclearsRequests", "Approver2UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.MclearsRequests", "Approver1UserId", c => c.String(maxLength: 128));
        }
    }
}
