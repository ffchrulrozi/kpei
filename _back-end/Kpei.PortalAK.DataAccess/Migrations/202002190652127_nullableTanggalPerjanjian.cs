namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullableTanggalPerjanjian : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AdministrasiKeanggotaanTradingMembers", "TanggalPerjanjian", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AdministrasiKeanggotaanTradingMembers", "TanggalPerjanjian", c => c.DateTime(nullable: false));
        }
    }
}
