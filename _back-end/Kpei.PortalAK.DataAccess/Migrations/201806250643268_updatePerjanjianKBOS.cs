namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatePerjanjianKBOS : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataPerjanjianKBOS", "TanggalAktif", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataPerjanjianKBOS", "Keterangan", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPerjanjianKBOS", "AdminUser", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPerjanjianKBOS", "LPKB", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataPerjanjianKBOS", "LPOS", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.DataPerjanjianKBOS", "StatusPerjanjian");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataPerjanjianKBOS", "StatusPerjanjian", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.DataPerjanjianKBOS", "LPOS");
            DropColumn("dbo.DataPerjanjianKBOS", "LPKB");
            DropColumn("dbo.DataPerjanjianKBOS", "AdminUser");
            DropColumn("dbo.DataPerjanjianKBOS", "Keterangan");
            DropColumn("dbo.DataPerjanjianKBOS", "TanggalAktif");
        }
    }
}
