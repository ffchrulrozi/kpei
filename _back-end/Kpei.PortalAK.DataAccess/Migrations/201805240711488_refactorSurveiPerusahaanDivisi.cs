namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class refactorSurveiPerusahaanDivisi : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SurveiDivisis", "HasilFileId", "dbo.Files");
            DropForeignKey("dbo.SurveiDivisis", "SurveiId", "dbo.Surveis");
            DropForeignKey("dbo.SurveiDivisis", "TindakLanjutFileId", "dbo.Files");
            DropForeignKey("dbo.SurveiPerusahaans", "HasilFileId", "dbo.Files");
            DropForeignKey("dbo.SurveiPerusahaans", "SurveiId", "dbo.Surveis");
            DropForeignKey("dbo.SurveiPerusahaans", "TindakLanjutFileId", "dbo.Files");
            DropIndex("dbo.SurveiDivisis", new[] { "SurveiId" });
            DropIndex("dbo.SurveiDivisis", new[] { "HasilFileId" });
            DropIndex("dbo.SurveiDivisis", new[] { "TindakLanjutFileId" });
            DropIndex("dbo.SurveiPerusahaans", new[] { "SurveiId" });
            DropIndex("dbo.SurveiPerusahaans", new[] { "HasilFileId" });
            DropIndex("dbo.SurveiPerusahaans", new[] { "TindakLanjutFileId" });
            AddColumn("dbo.SurveiDivisis", "HasilFileUrl", c => c.String());
            AddColumn("dbo.SurveiDivisis", "TindakLanjutFileUrl", c => c.String());
            AddColumn("dbo.SurveiPerusahaans", "HasilFileUrl", c => c.String());
            AddColumn("dbo.SurveiPerusahaans", "TindakLanjutFileUrl", c => c.String());
            AlterColumn("dbo.Surveis", "Nama", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.Surveis", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Surveis", "UpdatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SurveiDivisis", "Kategori", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.SurveiDivisis", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SurveiDivisis", "UpdatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SurveiPerusahaans", "Kategori", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.SurveiPerusahaans", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SurveiPerusahaans", "UpdatedUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.Surveis", "isTrashed");
            DropColumn("dbo.Surveis", "TglAktifUtc");
            DropColumn("dbo.SurveiDivisis", "HasilFileId");
            DropColumn("dbo.SurveiDivisis", "TindakLanjutFileId");
            DropColumn("dbo.SurveiDivisis", "isTrashed");
            DropColumn("dbo.SurveiDivisis", "TglAktifUtc");
            DropColumn("dbo.SurveiPerusahaans", "HasilFileId");
            DropColumn("dbo.SurveiPerusahaans", "TindakLanjutFileId");
            DropColumn("dbo.SurveiPerusahaans", "isTrashed");
            DropColumn("dbo.SurveiPerusahaans", "TglAktifUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SurveiPerusahaans", "TglAktifUtc", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.SurveiPerusahaans", "isTrashed", c => c.Boolean(nullable: false));
            AddColumn("dbo.SurveiPerusahaans", "TindakLanjutFileId", c => c.String(maxLength: 128));
            AddColumn("dbo.SurveiPerusahaans", "HasilFileId", c => c.String(maxLength: 128));
            AddColumn("dbo.SurveiDivisis", "TglAktifUtc", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.SurveiDivisis", "isTrashed", c => c.Boolean(nullable: false));
            AddColumn("dbo.SurveiDivisis", "TindakLanjutFileId", c => c.String(maxLength: 128));
            AddColumn("dbo.SurveiDivisis", "HasilFileId", c => c.String(maxLength: 128));
            AddColumn("dbo.Surveis", "TglAktifUtc", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Surveis", "isTrashed", c => c.Boolean(nullable: false));
            AlterColumn("dbo.SurveiPerusahaans", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.SurveiPerusahaans", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.SurveiPerusahaans", "Kategori", c => c.String(nullable: false));
            AlterColumn("dbo.SurveiDivisis", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.SurveiDivisis", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.SurveiDivisis", "Kategori", c => c.String(nullable: false));
            AlterColumn("dbo.Surveis", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Surveis", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Surveis", "Nama", c => c.String(nullable: false));
            DropColumn("dbo.SurveiPerusahaans", "TindakLanjutFileUrl");
            DropColumn("dbo.SurveiPerusahaans", "HasilFileUrl");
            DropColumn("dbo.SurveiDivisis", "TindakLanjutFileUrl");
            DropColumn("dbo.SurveiDivisis", "HasilFileUrl");
            CreateIndex("dbo.SurveiPerusahaans", "TindakLanjutFileId");
            CreateIndex("dbo.SurveiPerusahaans", "HasilFileId");
            CreateIndex("dbo.SurveiPerusahaans", "SurveiId");
            CreateIndex("dbo.SurveiDivisis", "TindakLanjutFileId");
            CreateIndex("dbo.SurveiDivisis", "HasilFileId");
            CreateIndex("dbo.SurveiDivisis", "SurveiId");
            AddForeignKey("dbo.SurveiPerusahaans", "TindakLanjutFileId", "dbo.Files", "Id");
            AddForeignKey("dbo.SurveiPerusahaans", "SurveiId", "dbo.Surveis", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SurveiPerusahaans", "HasilFileId", "dbo.Files", "Id");
            AddForeignKey("dbo.SurveiDivisis", "TindakLanjutFileId", "dbo.Files", "Id");
            AddForeignKey("dbo.SurveiDivisis", "SurveiId", "dbo.Surveis", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SurveiDivisis", "HasilFileId", "dbo.Files", "Id");
        }
    }
}
