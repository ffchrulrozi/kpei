namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addModuleIdColumnToUserActionLogsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserActionLogs", "ModuleId", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserActionLogs", "ModuleId");
        }
    }
}
