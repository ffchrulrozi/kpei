namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeIsTrashedFromSetting : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Settings", "Field", c => c.String(maxLength: 256));
            AlterColumn("dbo.Settings", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Settings", "UpdatedUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.Settings", "isTrashed");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Settings", "isTrashed", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Settings", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Settings", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Settings", "Field", c => c.String());
        }
    }
}
