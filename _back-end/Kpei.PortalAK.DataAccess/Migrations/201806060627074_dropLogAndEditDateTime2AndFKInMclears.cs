namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dropLogAndEditDateTime2AndFKInMclears : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MclearsPesertas", "RequestId", "dbo.MclearsRequests");
            DropForeignKey("dbo.MclearsRequestLogs", "RequestId", "dbo.MclearsRequests");
            DropIndex("dbo.MclearsPesertas", new[] { "RequestId" });
            DropIndex("dbo.MclearsRequestLogs", new[] { "RequestId" });
            AlterColumn("dbo.MclearsPesertas", "RequestId", c => c.String());
            AlterColumn("dbo.MclearsPesertas", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.MclearsPesertas", "UpdatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.MclearsRequests", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.MclearsRequests", "UpdatedUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.MclearsPesertas", "TglAktifUtc");
            DropColumn("dbo.MclearsRequests", "JlhPeserta");
            DropColumn("dbo.MclearsRequests", "JlhLayanan");
            DropColumn("dbo.MclearsRequests", "isTrashed");
            DropColumn("dbo.MclearsRequests", "TglAktifUtc");
            DropTable("dbo.MclearsRequestLogs");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MclearsRequestLogs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RequestId = c.String(maxLength: 128),
                        Author = c.String(),
                        Aktivitas = c.String(),
                        CreatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.MclearsRequests", "TglAktifUtc", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.MclearsRequests", "isTrashed", c => c.Boolean(nullable: false));
            AddColumn("dbo.MclearsRequests", "JlhLayanan", c => c.Int(nullable: false));
            AddColumn("dbo.MclearsRequests", "JlhPeserta", c => c.Int(nullable: false));
            AddColumn("dbo.MclearsPesertas", "TglAktifUtc", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.MclearsRequests", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.MclearsRequests", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.MclearsPesertas", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.MclearsPesertas", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.MclearsPesertas", "RequestId", c => c.String(maxLength: 128));
            CreateIndex("dbo.MclearsRequestLogs", "RequestId");
            CreateIndex("dbo.MclearsPesertas", "RequestId");
            AddForeignKey("dbo.MclearsRequestLogs", "RequestId", "dbo.MclearsRequests", "Id");
            AddForeignKey("dbo.MclearsPesertas", "RequestId", "dbo.MclearsRequests", "Id");
        }
    }
}
