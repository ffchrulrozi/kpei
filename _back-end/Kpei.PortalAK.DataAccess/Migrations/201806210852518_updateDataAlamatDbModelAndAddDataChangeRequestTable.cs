namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateDataAlamatDbModelAndAddDataChangeRequestTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataChangeRequests",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        Status = c.String(nullable: false, maxLength: 128),
                        DataDriverSlug = c.String(nullable: false, maxLength: 128),
                        DataId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.DataDriverSlug, t.DataId }, unique: true, name: "ix_drv_slug_id");
            
            AddColumn("dbo.DataAlamats", "Website", c => c.String(maxLength: 256));
            AddColumn("dbo.DataAlamats", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataAlamats", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataAlamats", "ChangeRequestId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataAlamats", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataAlamats", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataAlamats", "Approver2UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataAlamats", "Jalan", c => c.String(nullable: false, maxLength: 256));
            CreateIndex("dbo.DataAlamats", "ChangeRequestId");
            AddForeignKey("dbo.DataAlamats", "ChangeRequestId", "dbo.DataChangeRequests", "Id");
            DropColumn("dbo.DataAlamats", "TglAktifUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataAlamats", "TglAktifUtc", c => c.DateTime());
            DropForeignKey("dbo.DataAlamats", "ChangeRequestId", "dbo.DataChangeRequests");
            DropIndex("dbo.DataChangeRequests", "ix_drv_slug_id");
            DropIndex("dbo.DataAlamats", new[] { "ChangeRequestId" });
            AlterColumn("dbo.DataAlamats", "Jalan", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.DataAlamats", "Approver2UserId");
            DropColumn("dbo.DataAlamats", "Approver1UserId");
            DropColumn("dbo.DataAlamats", "RejectReason");
            DropColumn("dbo.DataAlamats", "ChangeRequestId");
            DropColumn("dbo.DataAlamats", "ActiveUtc");
            DropColumn("dbo.DataAlamats", "Status");
            DropColumn("dbo.DataAlamats", "Website");
            DropTable("dbo.DataChangeRequests");
        }
    }
}
