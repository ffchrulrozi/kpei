namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renameTglWithUtc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "TglPelaksanaanStartUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "TglPelaksanaanEndUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "TglDaftarBukaUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "TglDaftarTutupUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.Events", "TglPelaksanaanStart");
            DropColumn("dbo.Events", "TglPelaksanaanEnd");
            DropColumn("dbo.Events", "TglDaftarBuka");
            DropColumn("dbo.Events", "TglDaftarTutup");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Events", "TglDaftarTutup", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "TglDaftarBuka", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "TglPelaksanaanEnd", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "TglPelaksanaanStart", c => c.DateTime(nullable: false));
            DropColumn("dbo.Events", "TglDaftarTutupUtc");
            DropColumn("dbo.Events", "TglDaftarBukaUtc");
            DropColumn("dbo.Events", "TglPelaksanaanEndUtc");
            DropColumn("dbo.Events", "TglPelaksanaanStartUtc");
        }
    }
}
