namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addStatusNRejectReasonLayananJasa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LayananJasas", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.LayananJasas", "RejectReason", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LayananJasas", "RejectReason");
            DropColumn("dbo.LayananJasas", "Status");
        }
    }
}
