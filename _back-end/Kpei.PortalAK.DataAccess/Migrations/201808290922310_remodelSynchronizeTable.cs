namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remodelSynchronizeTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SynchronizeTables", "Flow", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.SynchronizeTables", "Action", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SynchronizeTables", "Action");
            DropColumn("dbo.SynchronizeTables", "Flow");
        }
    }
}
