namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldOrderToLayananBaru : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LayananBarus", "Order", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LayananBarus", "Order");
        }
    }
}
