namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateStatusBursaAndKpei : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataStatusBursas", "NoPengumuman", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataStatusBursas", "TanggalAktif", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusBursas", "TanggalSuspendAwal", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusBursas", "TanggalSuspendAkhir", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusBursas", "TanggalCabut", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusBursas", "Alasan", c => c.String(maxLength: 128));
            AddColumn("dbo.DataStatusBursas", "Keterangan", c => c.String(maxLength: 128));
            AddColumn("dbo.DataStatusKPEIs", "NoDokumen", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataStatusKPEIs", "SubStatusAnggotaKPEI", c => c.String(maxLength: 128));
            AddColumn("dbo.DataStatusKPEIs", "TanggalAwalStatus", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "TanggalAkhirStatus", c => c.DateTime(nullable: false));
            DropColumn("dbo.DataStatusKPEIs", "NoPengumuman");
            DropColumn("dbo.DataStatusKPEIs", "TanggalSuspendAwal");
            DropColumn("dbo.DataStatusKPEIs", "TanggalSuspendAkhir");
            DropColumn("dbo.DataStatusKPEIs", "TanggalCabut");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataStatusKPEIs", "TanggalCabut", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "TanggalSuspendAkhir", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "TanggalSuspendAwal", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "NoPengumuman", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.DataStatusKPEIs", "TanggalAkhirStatus");
            DropColumn("dbo.DataStatusKPEIs", "TanggalAwalStatus");
            DropColumn("dbo.DataStatusKPEIs", "SubStatusAnggotaKPEI");
            DropColumn("dbo.DataStatusKPEIs", "NoDokumen");
            DropColumn("dbo.DataStatusBursas", "Keterangan");
            DropColumn("dbo.DataStatusBursas", "Alasan");
            DropColumn("dbo.DataStatusBursas", "TanggalCabut");
            DropColumn("dbo.DataStatusBursas", "TanggalSuspendAkhir");
            DropColumn("dbo.DataStatusBursas", "TanggalSuspendAwal");
            DropColumn("dbo.DataStatusBursas", "TanggalAktif");
            DropColumn("dbo.DataStatusBursas", "NoPengumuman");
        }
    }
}
