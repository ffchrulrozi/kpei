namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addEmailTemplatesToPincodeSettingTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PincodeSettings", "EmailTemplateForKpei", c => c.String(maxLength: 2048));
            AddColumn("dbo.PincodeSettings", "EmailTemplateForAknp", c => c.String(maxLength: 2048));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PincodeSettings", "EmailTemplateForAknp");
            DropColumn("dbo.PincodeSettings", "EmailTemplateForKpei");
        }
    }
}
