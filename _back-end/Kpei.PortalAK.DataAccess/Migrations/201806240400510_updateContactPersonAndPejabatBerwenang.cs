namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateContactPersonAndPejabatBerwenang : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataContactPersons", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataContactPersons", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataContactPersons", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataContactPersons", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataContactPersons", "Approver2UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPejabatBerwenangs", "SuratKeteranganFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataPejabatBerwenangs", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataPejabatBerwenangs", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataPejabatBerwenangs", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataPejabatBerwenangs", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPejabatBerwenangs", "Approver2UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataContactPersons", "Nama", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataContactPersons", "Jabatan", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataContactPersons", "Email", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataContactPersons", "Telp", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataContactPersons", "HP", c => c.String(maxLength: 32));
            DropColumn("dbo.DataContactPersons", "NewRegId");
            DropColumn("dbo.DataContactPersons", "TglAktifUtc");
            DropColumn("dbo.DataPejabatBerwenangs", "AkteId");
            DropColumn("dbo.DataPejabatBerwenangs", "NewRegId");
            DropColumn("dbo.DataPejabatBerwenangs", "TglAktifUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataPejabatBerwenangs", "TglAktifUtc", c => c.DateTime());
            AddColumn("dbo.DataPejabatBerwenangs", "NewRegId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPejabatBerwenangs", "AkteId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataContactPersons", "TglAktifUtc", c => c.DateTime());
            AddColumn("dbo.DataContactPersons", "NewRegId", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataContactPersons", "HP", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.DataContactPersons", "Telp", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataContactPersons", "Email", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataContactPersons", "Jabatan", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataContactPersons", "Nama", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.DataPejabatBerwenangs", "Approver2UserId");
            DropColumn("dbo.DataPejabatBerwenangs", "Approver1UserId");
            DropColumn("dbo.DataPejabatBerwenangs", "RejectReason");
            DropColumn("dbo.DataPejabatBerwenangs", "ActiveUtc");
            DropColumn("dbo.DataPejabatBerwenangs", "Status");
            DropColumn("dbo.DataPejabatBerwenangs", "SuratKeteranganFileUrl");
            DropColumn("dbo.DataContactPersons", "Approver2UserId");
            DropColumn("dbo.DataContactPersons", "Approver1UserId");
            DropColumn("dbo.DataContactPersons", "RejectReason");
            DropColumn("dbo.DataContactPersons", "ActiveUtc");
            DropColumn("dbo.DataContactPersons", "Status");
        }
    }
}
