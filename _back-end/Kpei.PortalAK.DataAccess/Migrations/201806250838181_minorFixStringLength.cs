namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class minorFixStringLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataPerjanjianEBUs", "Keterangan", c => c.String(maxLength: 256));
            AlterColumn("dbo.DataPMEs", "JenisKeanggotaan", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataTipeMemberDanHirarkis", "JenisKeanggotaan", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataTipeMemberDanHirarkis", "Parent", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataTipeMemberDanHirarkis", "Parent", c => c.String(nullable: false));
            AlterColumn("dbo.DataTipeMemberDanHirarkis", "JenisKeanggotaan", c => c.String(nullable: false));
            AlterColumn("dbo.DataPMEs", "JenisKeanggotaan", c => c.String(nullable: false));
            AlterColumn("dbo.DataPerjanjianEBUs", "Keterangan", c => c.String());
        }
    }
}
