namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addEmailInDataAlamatAndDataRegistrasi : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataAlamats", "Email", c => c.String(maxLength: 256));
            AddColumn("dbo.DataRegistrasis", "Email", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataRegistrasis", "Email");
            DropColumn("dbo.DataAlamats", "Email");
        }
    }
}
