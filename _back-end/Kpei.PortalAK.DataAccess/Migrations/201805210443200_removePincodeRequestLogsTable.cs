namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removePincodeRequestLogsTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PincodeRequestLogs", "RequestId", "dbo.PincodeRequests");
            DropIndex("dbo.PincodeRequestLogs", new[] { "RequestId" });
            DropTable("dbo.PincodeRequestLogs");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PincodeRequestLogs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RequestId = c.String(maxLength: 128),
                        Author = c.String(),
                        Aktivitas = c.String(),
                        CreatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.PincodeRequestLogs", "RequestId");
            AddForeignKey("dbo.PincodeRequestLogs", "RequestId", "dbo.PincodeRequests", "Id");
        }
    }
}
