// <auto-generated />
namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class refactorDataAknpWithAkteDbModels : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(refactorDataAknpWithAkteDbModels));
        
        string IMigrationMetadata.Id
        {
            get { return "201806230927401_refactorDataAknpWithAkteDbModels"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
