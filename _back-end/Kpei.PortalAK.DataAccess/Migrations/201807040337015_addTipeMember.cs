namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTipeMember : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataTipeMembers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KategoriMember = c.String(nullable: false, maxLength: 128),
                        TipeMemberAK = c.String(maxLength: 128),
                        TipeMemberPartisipan = c.String(maxLength: 128),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.DataInformasiLains", "KategoriMember");
            DropColumn("dbo.DataInformasiLains", "TipeMemberAK");
            DropColumn("dbo.DataInformasiLains", "TipeMemberPartisipan");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataInformasiLains", "TipeMemberPartisipan", c => c.String(maxLength: 128));
            AddColumn("dbo.DataInformasiLains", "TipeMemberAK", c => c.String(maxLength: 128));
            AddColumn("dbo.DataInformasiLains", "KategoriMember", c => c.String(nullable: false, maxLength: 128));
            DropTable("dbo.DataTipeMembers");
        }
    }
}
