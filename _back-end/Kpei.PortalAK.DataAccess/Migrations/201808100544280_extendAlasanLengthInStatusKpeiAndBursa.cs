namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class extendAlasanLengthInStatusKpeiAndBursa : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataStatusBursas", "Alasan", c => c.String(maxLength: 512));
            AlterColumn("dbo.DataStatusKPEIs", "Alasan", c => c.String(maxLength: 512));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataStatusKPEIs", "Alasan", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataStatusBursas", "Alasan", c => c.String(maxLength: 128));
        }
    }
}
