namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeTglAktifFromEvent : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Events", "TglAktifUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Events", "TglAktifUtc", c => c.DateTime());
        }
    }
}
