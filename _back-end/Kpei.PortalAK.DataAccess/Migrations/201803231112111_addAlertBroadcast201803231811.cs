namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAlertBroadcast201803231811 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MclearsPesertas", "AlertBroadcast", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MclearsPesertas", "AlertBroadcast");
        }
    }
}
