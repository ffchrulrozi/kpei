namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialization : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataAktes",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        NoAkte = c.String(nullable: false),
                        JenisAkte = c.String(nullable: false),
                        TglSejakUtc = c.DateTime(nullable: false),
                        TglSampaiUtc = c.DateTime(nullable: false),
                        AkteFileId = c.String(maxLength: 128),
                        NewRegId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.AkteFileId)
                .ForeignKey("dbo.DataRegistrasis", t => t.NewRegId)
                .Index(t => t.AkteFileId)
                .Index(t => t.NewRegId);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        NewRegId = c.String(maxLength: 128),
                        FilePath = c.String(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataRegistrasis", t => t.NewRegId)
                .Index(t => t.NewRegId);
            
            CreateTable(
                "dbo.DataRegistrasis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(maxLength: 32),
                        Nama = c.String(nullable: false),
                        ModalDasar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ModalDisetor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Gedung = c.String(),
                        Jalan = c.String(nullable: false),
                        Kota = c.String(nullable: false),
                        KodePos = c.String(nullable: false),
                        Telp = c.String(nullable: false),
                        Fax = c.String(nullable: false),
                        BankPembayaran = c.String(nullable: false),
                        StatusKPEI = c.String(nullable: false),
                        StatusBursa = c.String(nullable: false),
                        PPE = c.String(),
                        PEE = c.String(),
                        MI = c.String(),
                        BK = c.String(),
                        BU = c.String(),
                        NoPerjanjian = c.String(nullable: false),
                        TglPerjanjianUtc = c.DateTime(nullable: false),
                        KategoriMember = c.String(nullable: false),
                        TipeMemberAK = c.String(),
                        TipeMemberPartisipan = c.String(),
                        StatusPerusahaan = c.String(nullable: false),
                        NPWP = c.String(nullable: false),
                        AnggotaBursa = c.String(nullable: false),
                        NoSPAK = c.String(nullable: false),
                        TglSPAKUtc = c.DateTime(nullable: false),
                        NoSPAB = c.String(nullable: false),
                        TglSPABUtc = c.DateTime(nullable: false),
                        AnggaranDasarFileId = c.String(maxLength: 128),
                        AktePendirianFileId = c.String(maxLength: 128),
                        AktePerubahanFileId = c.String(maxLength: 128),
                        LaporanKeuanganFileId = c.String(maxLength: 128),
                        IjinUsahaFileId = c.String(maxLength: 128),
                        SPABFileId = c.String(maxLength: 128),
                        SPAKFileId = c.String(maxLength: 128),
                        NPWPFileId = c.String(maxLength: 128),
                        KTPFileId = c.String(maxLength: 128),
                        CR = c.Single(nullable: false),
                        ROA = c.Single(nullable: false),
                        ROE = c.Single(nullable: false),
                        NetProfit = c.Single(nullable: false),
                        DAR = c.Single(nullable: false),
                        DER = c.Single(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.AktePendirianFileId)
                .ForeignKey("dbo.Files", t => t.AktePerubahanFileId)
                .ForeignKey("dbo.Files", t => t.AnggaranDasarFileId)
                .ForeignKey("dbo.Files", t => t.IjinUsahaFileId)
                .ForeignKey("dbo.Files", t => t.KTPFileId)
                .ForeignKey("dbo.Files", t => t.LaporanKeuanganFileId)
                .ForeignKey("dbo.Files", t => t.NPWPFileId)
                .ForeignKey("dbo.Files", t => t.SPABFileId)
                .ForeignKey("dbo.Files", t => t.SPAKFileId)
                .Index(t => t.AnggaranDasarFileId)
                .Index(t => t.AktePendirianFileId)
                .Index(t => t.AktePerubahanFileId)
                .Index(t => t.LaporanKeuanganFileId)
                .Index(t => t.IjinUsahaFileId)
                .Index(t => t.SPABFileId)
                .Index(t => t.SPAKFileId)
                .Index(t => t.NPWPFileId)
                .Index(t => t.KTPFileId);
            
            CreateTable(
                "dbo.DataAlamats",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        Gedung = c.String(),
                        Jalan = c.String(nullable: false),
                        Kota = c.String(nullable: false),
                        KodePos = c.String(nullable: false),
                        Telp = c.String(nullable: false),
                        Fax = c.String(),
                        SuratKeteranganFileId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.SuratKeteranganFileId)
                .Index(t => t.SuratKeteranganFileId);
            
            CreateTable(
                "dbo.DataBankPembayarans",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        BankPembayaran = c.String(nullable: false),
                        SuratKeteranganFileId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.SuratKeteranganFileId)
                .Index(t => t.SuratKeteranganFileId);
            
            CreateTable(
                "dbo.DataContactPersons",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        Nama = c.String(nullable: false),
                        Jabatan = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Telp = c.String(nullable: false),
                        HP = c.String(nullable: false),
                        SuratKeteranganFileId = c.String(maxLength: 128),
                        NewRegId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataRegistrasis", t => t.NewRegId)
                .ForeignKey("dbo.Files", t => t.SuratKeteranganFileId)
                .Index(t => t.SuratKeteranganFileId)
                .Index(t => t.NewRegId);
            
            CreateTable(
                "dbo.DataDireksis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        Nama = c.String(nullable: false),
                        Jabatan = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Telp = c.String(nullable: false),
                        HP = c.String(nullable: false),
                        AkteId = c.String(maxLength: 128),
                        NewRegId = c.String(maxLength: 128),
                        TtdFileId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataAktes", t => t.AkteId)
                .ForeignKey("dbo.DataRegistrasis", t => t.NewRegId)
                .ForeignKey("dbo.Files", t => t.TtdFileId)
                .Index(t => t.AkteId)
                .Index(t => t.NewRegId)
                .Index(t => t.TtdFileId);
            
            CreateTable(
                "dbo.DataInformasiLains",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        NoPerjanjian = c.String(nullable: false),
                        TglPerjanjianUtc = c.DateTime(nullable: false),
                        KategoriMember = c.String(nullable: false),
                        TipeMemberAK = c.String(),
                        TipeMemberPartisipan = c.String(),
                        StatusPerusahaan = c.String(nullable: false),
                        NPWP = c.String(nullable: false),
                        AnggotaBursa = c.String(nullable: false),
                        NoSPAK = c.String(nullable: false),
                        TglSPAKUtc = c.DateTime(nullable: false),
                        NoSPAB = c.String(nullable: false),
                        TglSPABUtc = c.DateTime(nullable: false),
                        AnggaranDasarFileId = c.String(maxLength: 128),
                        AktePendirianFileId = c.String(maxLength: 128),
                        AktePerubahanFileId = c.String(maxLength: 128),
                        LaporanKeuanganFileId = c.String(maxLength: 128),
                        IjinUsahaFileId = c.String(maxLength: 128),
                        SPABFileId = c.String(maxLength: 128),
                        SPAKFileId = c.String(maxLength: 128),
                        NPWPFileId = c.String(maxLength: 128),
                        KTPFileId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.AktePendirianFileId)
                .ForeignKey("dbo.Files", t => t.AktePerubahanFileId)
                .ForeignKey("dbo.Files", t => t.AnggaranDasarFileId)
                .ForeignKey("dbo.Files", t => t.IjinUsahaFileId)
                .ForeignKey("dbo.Files", t => t.KTPFileId)
                .ForeignKey("dbo.Files", t => t.LaporanKeuanganFileId)
                .ForeignKey("dbo.Files", t => t.NPWPFileId)
                .ForeignKey("dbo.Files", t => t.SPABFileId)
                .ForeignKey("dbo.Files", t => t.SPAKFileId)
                .Index(t => t.AnggaranDasarFileId)
                .Index(t => t.AktePendirianFileId)
                .Index(t => t.AktePerubahanFileId)
                .Index(t => t.LaporanKeuanganFileId)
                .Index(t => t.IjinUsahaFileId)
                .Index(t => t.SPABFileId)
                .Index(t => t.SPAKFileId)
                .Index(t => t.NPWPFileId)
                .Index(t => t.KTPFileId);
            
            CreateTable(
                "dbo.DataJenisUsahas",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        PPE = c.String(),
                        PEE = c.String(),
                        MI = c.String(),
                        BK = c.String(),
                        BU = c.String(),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataKomisaris",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        Nama = c.String(nullable: false),
                        Jabatan = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Telp = c.String(nullable: false),
                        HP = c.String(nullable: false),
                        AkteId = c.String(maxLength: 128),
                        NewRegId = c.String(maxLength: 128),
                        TtdFileId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataAktes", t => t.AkteId)
                .ForeignKey("dbo.DataRegistrasis", t => t.NewRegId)
                .ForeignKey("dbo.Files", t => t.TtdFileId)
                .Index(t => t.AkteId)
                .Index(t => t.NewRegId)
                .Index(t => t.TtdFileId);
            
            CreateTable(
                "dbo.DataModalDasars",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        NominalModal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AkteId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataAktes", t => t.AkteId)
                .Index(t => t.AkteId);
            
            CreateTable(
                "dbo.DataModalDisetors",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        NominalModal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AkteId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataAktes", t => t.AkteId)
                .Index(t => t.AkteId);
            
            CreateTable(
                "dbo.DataNamas",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        Nama = c.String(nullable: false),
                        AkteId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataAktes", t => t.AkteId)
                .Index(t => t.AkteId);
            
            CreateTable(
                "dbo.DataPejabatBerwenangs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        Nama = c.String(nullable: false),
                        Jabatan = c.String(nullable: false),
                        AkteId = c.String(maxLength: 128),
                        NewRegId = c.String(maxLength: 128),
                        TtdFileId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataAktes", t => t.AkteId)
                .ForeignKey("dbo.DataRegistrasis", t => t.NewRegId)
                .ForeignKey("dbo.Files", t => t.TtdFileId)
                .Index(t => t.AkteId)
                .Index(t => t.NewRegId)
                .Index(t => t.TtdFileId);
            
            CreateTable(
                "dbo.DataPemegangSahams",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        Nama = c.String(nullable: false),
                        LembarSaham = c.Int(nullable: false),
                        NominalSaham = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WargaNegara = c.String(nullable: false),
                        AkteId = c.String(maxLength: 128),
                        NewRegId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataAktes", t => t.AkteId)
                .ForeignKey("dbo.DataRegistrasis", t => t.NewRegId)
                .Index(t => t.AkteId)
                .Index(t => t.NewRegId);
            
            CreateTable(
                "dbo.DataPerjanjianEBUs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        NoSimO = c.String(nullable: false),
                        TglSimOUtc = c.DateTime(nullable: false),
                        NoPerjanjian = c.String(nullable: false),
                        JenisPerjanjian = c.String(nullable: false),
                        TglSejakUtc = c.DateTime(nullable: false),
                        TglSampaiUtc = c.DateTime(nullable: false),
                        StatusPerjanjian = c.String(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAKtifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataPerjanjianKBOS",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        NoSPM = c.String(nullable: false),
                        NoPerjanjian = c.String(nullable: false),
                        JenisPerjanjian = c.String(nullable: false),
                        TglSPMUtc = c.DateTime(nullable: false),
                        StatusPerjanjian = c.String(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataPMEs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        JenisKeanggotaan = c.String(nullable: false),
                        LenderTglAwalUtc = c.DateTime(nullable: false),
                        LenderTglAkhirUtc = c.DateTime(nullable: false),
                        LenderStatus = c.String(),
                        LenderPerjanjian = c.String(),
                        BorrowerTglAwalUtc = c.DateTime(nullable: false),
                        BorrowerTglAkhirUtc = c.DateTime(nullable: false),
                        BorrowerStatus = c.String(),
                        BorrowerPerjanjian = c.String(),
                        AKNasabahFileId = c.String(maxLength: 128),
                        SuratKuasaSubRekFileId = c.String(maxLength: 128),
                        AplikasiLenderFileId = c.String(maxLength: 128),
                        AplikasiBorrowerFileId = c.String(maxLength: 128),
                        KPEIAsLenderFileId = c.String(maxLength: 128),
                        KPEIAsBorrowerFileId = c.String(maxLength: 128),
                        SuratKuasaBaruFileId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.AKNasabahFileId)
                .ForeignKey("dbo.Files", t => t.AplikasiBorrowerFileId)
                .ForeignKey("dbo.Files", t => t.AplikasiLenderFileId)
                .ForeignKey("dbo.Files", t => t.KPEIAsBorrowerFileId)
                .ForeignKey("dbo.Files", t => t.KPEIAsLenderFileId)
                .ForeignKey("dbo.Files", t => t.SuratKuasaBaruFileId)
                .ForeignKey("dbo.Files", t => t.SuratKuasaSubRekFileId)
                .Index(t => t.AKNasabahFileId)
                .Index(t => t.SuratKuasaSubRekFileId)
                .Index(t => t.AplikasiLenderFileId)
                .Index(t => t.AplikasiBorrowerFileId)
                .Index(t => t.KPEIAsLenderFileId)
                .Index(t => t.KPEIAsBorrowerFileId)
                .Index(t => t.SuratKuasaBaruFileId);
            
            CreateTable(
                "dbo.DataProfils",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        Nama = c.String(nullable: false),
                        ModalDasar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ModalDisetor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Alamat = c.String(nullable: false),
                        BankPembayaran = c.String(nullable: false),
                        StatusKPEI = c.String(nullable: false),
                        StatusBursa = c.String(nullable: false),
                        PPE = c.String(),
                        PEE = c.String(),
                        MI = c.String(),
                        BK = c.String(),
                        BU = c.String(),
                        NoPerjanjian = c.String(nullable: false),
                        TglPerjanjianUtc = c.DateTime(nullable: false),
                        KategoriMember = c.String(nullable: false),
                        TipeMemberAK = c.String(nullable: false),
                        TipeMemberPartisipan = c.String(nullable: false),
                        StatusPerusahaan = c.String(nullable: false),
                        NPWP = c.String(nullable: false),
                        AnggotaBursa = c.String(nullable: false),
                        NoSPAK = c.String(nullable: false),
                        TglSPAKUtc = c.DateTime(nullable: false),
                        NoSPAB = c.String(nullable: false),
                        TglSPABUtc = c.DateTime(nullable: false),
                        AnggaranDasarFileId = c.String(maxLength: 128),
                        AktePendirianFileId = c.String(maxLength: 128),
                        AktePerubahanFileId = c.String(maxLength: 128),
                        LaporanKeuanganFileId = c.String(maxLength: 128),
                        IjinUsahaFileId = c.String(maxLength: 128),
                        SPABFileId = c.String(maxLength: 128),
                        SPAKFileId = c.String(maxLength: 128),
                        NPWPFileId = c.String(maxLength: 128),
                        KTPFileId = c.String(maxLength: 128),
                        CR = c.Single(nullable: false),
                        ROA = c.Single(nullable: false),
                        ROE = c.Single(nullable: false),
                        NetProfit = c.Single(nullable: false),
                        DAR = c.Single(nullable: false),
                        DER = c.Single(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.AktePendirianFileId)
                .ForeignKey("dbo.Files", t => t.AktePerubahanFileId)
                .ForeignKey("dbo.Files", t => t.AnggaranDasarFileId)
                .ForeignKey("dbo.Files", t => t.IjinUsahaFileId)
                .ForeignKey("dbo.Files", t => t.KTPFileId)
                .ForeignKey("dbo.Files", t => t.LaporanKeuanganFileId)
                .ForeignKey("dbo.Files", t => t.NPWPFileId)
                .ForeignKey("dbo.Files", t => t.SPABFileId)
                .ForeignKey("dbo.Files", t => t.SPAKFileId)
                .Index(t => t.AnggaranDasarFileId)
                .Index(t => t.AktePendirianFileId)
                .Index(t => t.AktePerubahanFileId)
                .Index(t => t.LaporanKeuanganFileId)
                .Index(t => t.IjinUsahaFileId)
                .Index(t => t.SPABFileId)
                .Index(t => t.SPAKFileId)
                .Index(t => t.NPWPFileId)
                .Index(t => t.KTPFileId);
            
            CreateTable(
                "dbo.DataRequests",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        DetailId = c.String(nullable: false),
                        DetailTableName = c.String(nullable: false),
                        StatusRequest = c.String(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataStatusBursas",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        StatusBursa = c.String(nullable: false),
                        SuratKeteranganFileId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.SuratKeteranganFileId)
                .Index(t => t.SuratKeteranganFileId);
            
            CreateTable(
                "dbo.DataStatusKPEIs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        StatusKPEI = c.String(nullable: false),
                        SuratKeteranganFileId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.SuratKeteranganFileId)
                .Index(t => t.SuratKeteranganFileId);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Judul = c.String(nullable: false),
                        Highlight = c.String(),
                        Tempat = c.String(nullable: false),
                        WaktuPelaksanaan = c.DateTime(nullable: false),
                        Penutupan = c.Int(nullable: false),
                        JenisPelatihan = c.String(nullable: false),
                        KapasitasPerAK = c.Int(nullable: false),
                        KapasitasTotal = c.Int(nullable: false),
                        BrosurFileId = c.String(maxLength: 128),
                        MateriFileId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.BrosurFileId)
                .ForeignKey("dbo.Files", t => t.MateriFileId)
                .Index(t => t.BrosurFileId)
                .Index(t => t.MateriFileId);
            
            CreateTable(
                "dbo.EventPembicaras",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        EventId = c.String(nullable: false, maxLength: 128),
                        Nama = c.String(nullable: false),
                        Jabatan = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .Index(t => t.EventId);
            
            CreateTable(
                "dbo.EventPesertas",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        EventId = c.String(maxLength: 128),
                        KodeAK = c.String(nullable: false),
                        Nama = c.String(nullable: false),
                        Jabatan = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Telp = c.String(nullable: false),
                        Hadir = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Events", t => t.EventId)
                .Index(t => t.EventId);
            
            CreateTable(
                "dbo.HakAkses",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        NamaModul = c.String(nullable: false, maxLength: 50),
                        HasApprove = c.Boolean(nullable: false),
                        HasModify = c.Boolean(nullable: false),
                        HasView = c.Boolean(nullable: false),
                        HasViewAsKpei = c.Boolean(nullable: false),
                        HasAdmin = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.KeuanganLaporans",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        Tahun = c.String(nullable: false),
                        Jenis = c.String(nullable: false),
                        NeracaFileId = c.String(nullable: false, maxLength: 128),
                        LabaRugiFileId = c.String(maxLength: 128),
                        ArusKasFileId = c.String(maxLength: 128),
                        AsetLancar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UtangLancar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalAset = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalUtang = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Pendapatan = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LabaBersih = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Ekuitas = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CR = c.Single(nullable: false),
                        ROA = c.Single(nullable: false),
                        ROE = c.Single(nullable: false),
                        NetProfit = c.Single(nullable: false),
                        DAR = c.Single(nullable: false),
                        DER = c.Single(nullable: false),
                        NewRegId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.ArusKasFileId)
                .ForeignKey("dbo.Files", t => t.LabaRugiFileId)
                .ForeignKey("dbo.Files", t => t.NeracaFileId, cascadeDelete: true)
                .ForeignKey("dbo.DataRegistrasis", t => t.NewRegId)
                .Index(t => t.NeracaFileId)
                .Index(t => t.LabaRugiFileId)
                .Index(t => t.ArusKasFileId)
                .Index(t => t.NewRegId);
            
            CreateTable(
                "dbo.KeuanganRequests",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        DetailId = c.String(nullable: false, maxLength: 128),
                        StatusRequest = c.String(),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.KeuanganLaporans", t => t.DetailId, cascadeDelete: true)
                .Index(t => t.DetailId);
            
            CreateTable(
                "dbo.MclearsPesertas",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        Nama = c.String(nullable: false),
                        Jabatan = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        HP = c.String(),
                        AlertPC = c.Boolean(nullable: false),
                        AlertPS = c.Boolean(nullable: false),
                        AlertHTS = c.Boolean(nullable: false),
                        AlertHTR = c.Boolean(nullable: false),
                        AlertBACS = c.Boolean(nullable: false),
                        AlertACS = c.Boolean(nullable: false),
                        AlertTLP = c.Boolean(nullable: false),
                        AlertTLS = c.Boolean(nullable: false),
                        AlertTL5 = c.Boolean(nullable: false),
                        AlertMKBDA = c.Boolean(nullable: false),
                        AlertFCOLWA = c.Boolean(nullable: false),
                        AlertCLA = c.Boolean(nullable: false),
                        AlertOCA = c.Boolean(nullable: false),
                        AlertMCCA = c.Boolean(nullable: false),
                        AlertDCDA = c.Boolean(nullable: false),
                        AlertCPA = c.Boolean(nullable: false),
                        AlertL1 = c.Boolean(nullable: false),
                        AlertL3 = c.Boolean(nullable: false),
                        AlertL7 = c.Boolean(nullable: false),
                        AlertHPHFA = c.Boolean(nullable: false),
                        OnRequestPC = c.Boolean(nullable: false),
                        OnRequestPS = c.Boolean(nullable: false),
                        OnRequestHTS = c.Boolean(nullable: false),
                        OnRequestHTR = c.Boolean(nullable: false),
                        OnRequestPPR = c.Boolean(nullable: false),
                        OnRequestMKBDR = c.Boolean(nullable: false),
                        OnRequestOCR = c.Boolean(nullable: false),
                        OnRequestHaircut = c.Boolean(nullable: false),
                        OnRequestDCDR = c.Boolean(nullable: false),
                        OnRequestDCRR = c.Boolean(nullable: false),
                        OnRequestMCR = c.Boolean(nullable: false),
                        OnRequestCPR = c.Boolean(nullable: false),
                        OnRequestHPHFR = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MclearsRequests",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        DetailId = c.String(nullable: false, maxLength: 128),
                        StatusRequest = c.String(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MclearsPesertas", t => t.DetailId, cascadeDelete: true)
                .Index(t => t.DetailId);
            
            CreateTable(
                "dbo.PincodeRequests",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        JenisPincode = c.String(nullable: false),
                        Alasan = c.String(nullable: false),
                        AlasanDitolak = c.String(),
                        StatusRequest = c.String(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Surveis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Nama = c.String(nullable: false),
                        TglPelaksanaanUtc = c.DateTime(nullable: false),
                        JlhResponden = c.Int(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SurveiDivisis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SurveiId = c.String(nullable: false, maxLength: 128),
                        Kategori = c.String(nullable: false),
                        HasilFileId = c.String(maxLength: 128),
                        TindakLanjutFileId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.HasilFileId)
                .ForeignKey("dbo.Surveis", t => t.SurveiId, cascadeDelete: true)
                .ForeignKey("dbo.Files", t => t.TindakLanjutFileId)
                .Index(t => t.SurveiId)
                .Index(t => t.HasilFileId)
                .Index(t => t.TindakLanjutFileId);
            
            CreateTable(
                "dbo.SurveiPerusahaans",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SurveiId = c.String(nullable: false, maxLength: 128),
                        Kategori = c.String(nullable: false),
                        HasilFileId = c.String(maxLength: 128),
                        TindakLanjutFileId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.HasilFileId)
                .ForeignKey("dbo.Surveis", t => t.SurveiId, cascadeDelete: true)
                .ForeignKey("dbo.Files", t => t.TindakLanjutFileId)
                .Index(t => t.SurveiId)
                .Index(t => t.HasilFileId)
                .Index(t => t.TindakLanjutFileId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SurveiPerusahaans", "TindakLanjutFileId", "dbo.Files");
            DropForeignKey("dbo.SurveiPerusahaans", "SurveiId", "dbo.Surveis");
            DropForeignKey("dbo.SurveiPerusahaans", "HasilFileId", "dbo.Files");
            DropForeignKey("dbo.SurveiDivisis", "TindakLanjutFileId", "dbo.Files");
            DropForeignKey("dbo.SurveiDivisis", "SurveiId", "dbo.Surveis");
            DropForeignKey("dbo.SurveiDivisis", "HasilFileId", "dbo.Files");
            DropForeignKey("dbo.MclearsRequests", "DetailId", "dbo.MclearsPesertas");
            DropForeignKey("dbo.KeuanganRequests", "DetailId", "dbo.KeuanganLaporans");
            DropForeignKey("dbo.KeuanganLaporans", "NewRegId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.KeuanganLaporans", "NeracaFileId", "dbo.Files");
            DropForeignKey("dbo.KeuanganLaporans", "LabaRugiFileId", "dbo.Files");
            DropForeignKey("dbo.KeuanganLaporans", "ArusKasFileId", "dbo.Files");
            DropForeignKey("dbo.EventPesertas", "EventId", "dbo.Events");
            DropForeignKey("dbo.EventPembicaras", "EventId", "dbo.Events");
            DropForeignKey("dbo.Events", "MateriFileId", "dbo.Files");
            DropForeignKey("dbo.Events", "BrosurFileId", "dbo.Files");
            DropForeignKey("dbo.DataStatusKPEIs", "SuratKeteranganFileId", "dbo.Files");
            DropForeignKey("dbo.DataStatusBursas", "SuratKeteranganFileId", "dbo.Files");
            DropForeignKey("dbo.DataProfils", "SPAKFileId", "dbo.Files");
            DropForeignKey("dbo.DataProfils", "SPABFileId", "dbo.Files");
            DropForeignKey("dbo.DataProfils", "NPWPFileId", "dbo.Files");
            DropForeignKey("dbo.DataProfils", "LaporanKeuanganFileId", "dbo.Files");
            DropForeignKey("dbo.DataProfils", "KTPFileId", "dbo.Files");
            DropForeignKey("dbo.DataProfils", "IjinUsahaFileId", "dbo.Files");
            DropForeignKey("dbo.DataProfils", "AnggaranDasarFileId", "dbo.Files");
            DropForeignKey("dbo.DataProfils", "AktePerubahanFileId", "dbo.Files");
            DropForeignKey("dbo.DataProfils", "AktePendirianFileId", "dbo.Files");
            DropForeignKey("dbo.DataPMEs", "SuratKuasaSubRekFileId", "dbo.Files");
            DropForeignKey("dbo.DataPMEs", "SuratKuasaBaruFileId", "dbo.Files");
            DropForeignKey("dbo.DataPMEs", "KPEIAsLenderFileId", "dbo.Files");
            DropForeignKey("dbo.DataPMEs", "KPEIAsBorrowerFileId", "dbo.Files");
            DropForeignKey("dbo.DataPMEs", "AplikasiLenderFileId", "dbo.Files");
            DropForeignKey("dbo.DataPMEs", "AplikasiBorrowerFileId", "dbo.Files");
            DropForeignKey("dbo.DataPMEs", "AKNasabahFileId", "dbo.Files");
            DropForeignKey("dbo.DataPemegangSahams", "NewRegId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataPemegangSahams", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataPejabatBerwenangs", "TtdFileId", "dbo.Files");
            DropForeignKey("dbo.DataPejabatBerwenangs", "NewRegId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataPejabatBerwenangs", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataNamas", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataModalDisetors", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataModalDasars", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataKomisaris", "TtdFileId", "dbo.Files");
            DropForeignKey("dbo.DataKomisaris", "NewRegId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataKomisaris", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataInformasiLains", "SPAKFileId", "dbo.Files");
            DropForeignKey("dbo.DataInformasiLains", "SPABFileId", "dbo.Files");
            DropForeignKey("dbo.DataInformasiLains", "NPWPFileId", "dbo.Files");
            DropForeignKey("dbo.DataInformasiLains", "LaporanKeuanganFileId", "dbo.Files");
            DropForeignKey("dbo.DataInformasiLains", "KTPFileId", "dbo.Files");
            DropForeignKey("dbo.DataInformasiLains", "IjinUsahaFileId", "dbo.Files");
            DropForeignKey("dbo.DataInformasiLains", "AnggaranDasarFileId", "dbo.Files");
            DropForeignKey("dbo.DataInformasiLains", "AktePerubahanFileId", "dbo.Files");
            DropForeignKey("dbo.DataInformasiLains", "AktePendirianFileId", "dbo.Files");
            DropForeignKey("dbo.DataDireksis", "TtdFileId", "dbo.Files");
            DropForeignKey("dbo.DataDireksis", "NewRegId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataDireksis", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataContactPersons", "SuratKeteranganFileId", "dbo.Files");
            DropForeignKey("dbo.DataContactPersons", "NewRegId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataBankPembayarans", "SuratKeteranganFileId", "dbo.Files");
            DropForeignKey("dbo.DataAlamats", "SuratKeteranganFileId", "dbo.Files");
            DropForeignKey("dbo.DataAktes", "NewRegId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataAktes", "AkteFileId", "dbo.Files");
            DropForeignKey("dbo.Files", "NewRegId", "dbo.DataRegistrasis");
            DropForeignKey("dbo.DataRegistrasis", "SPAKFileId", "dbo.Files");
            DropForeignKey("dbo.DataRegistrasis", "SPABFileId", "dbo.Files");
            DropForeignKey("dbo.DataRegistrasis", "NPWPFileId", "dbo.Files");
            DropForeignKey("dbo.DataRegistrasis", "LaporanKeuanganFileId", "dbo.Files");
            DropForeignKey("dbo.DataRegistrasis", "KTPFileId", "dbo.Files");
            DropForeignKey("dbo.DataRegistrasis", "IjinUsahaFileId", "dbo.Files");
            DropForeignKey("dbo.DataRegistrasis", "AnggaranDasarFileId", "dbo.Files");
            DropForeignKey("dbo.DataRegistrasis", "AktePerubahanFileId", "dbo.Files");
            DropForeignKey("dbo.DataRegistrasis", "AktePendirianFileId", "dbo.Files");
            DropIndex("dbo.SurveiPerusahaans", new[] { "TindakLanjutFileId" });
            DropIndex("dbo.SurveiPerusahaans", new[] { "HasilFileId" });
            DropIndex("dbo.SurveiPerusahaans", new[] { "SurveiId" });
            DropIndex("dbo.SurveiDivisis", new[] { "TindakLanjutFileId" });
            DropIndex("dbo.SurveiDivisis", new[] { "HasilFileId" });
            DropIndex("dbo.SurveiDivisis", new[] { "SurveiId" });
            DropIndex("dbo.MclearsRequests", new[] { "DetailId" });
            DropIndex("dbo.KeuanganRequests", new[] { "DetailId" });
            DropIndex("dbo.KeuanganLaporans", new[] { "NewRegId" });
            DropIndex("dbo.KeuanganLaporans", new[] { "ArusKasFileId" });
            DropIndex("dbo.KeuanganLaporans", new[] { "LabaRugiFileId" });
            DropIndex("dbo.KeuanganLaporans", new[] { "NeracaFileId" });
            DropIndex("dbo.EventPesertas", new[] { "EventId" });
            DropIndex("dbo.EventPembicaras", new[] { "EventId" });
            DropIndex("dbo.Events", new[] { "MateriFileId" });
            DropIndex("dbo.Events", new[] { "BrosurFileId" });
            DropIndex("dbo.DataStatusKPEIs", new[] { "SuratKeteranganFileId" });
            DropIndex("dbo.DataStatusBursas", new[] { "SuratKeteranganFileId" });
            DropIndex("dbo.DataProfils", new[] { "KTPFileId" });
            DropIndex("dbo.DataProfils", new[] { "NPWPFileId" });
            DropIndex("dbo.DataProfils", new[] { "SPAKFileId" });
            DropIndex("dbo.DataProfils", new[] { "SPABFileId" });
            DropIndex("dbo.DataProfils", new[] { "IjinUsahaFileId" });
            DropIndex("dbo.DataProfils", new[] { "LaporanKeuanganFileId" });
            DropIndex("dbo.DataProfils", new[] { "AktePerubahanFileId" });
            DropIndex("dbo.DataProfils", new[] { "AktePendirianFileId" });
            DropIndex("dbo.DataProfils", new[] { "AnggaranDasarFileId" });
            DropIndex("dbo.DataPMEs", new[] { "SuratKuasaBaruFileId" });
            DropIndex("dbo.DataPMEs", new[] { "KPEIAsBorrowerFileId" });
            DropIndex("dbo.DataPMEs", new[] { "KPEIAsLenderFileId" });
            DropIndex("dbo.DataPMEs", new[] { "AplikasiBorrowerFileId" });
            DropIndex("dbo.DataPMEs", new[] { "AplikasiLenderFileId" });
            DropIndex("dbo.DataPMEs", new[] { "SuratKuasaSubRekFileId" });
            DropIndex("dbo.DataPMEs", new[] { "AKNasabahFileId" });
            DropIndex("dbo.DataPemegangSahams", new[] { "NewRegId" });
            DropIndex("dbo.DataPemegangSahams", new[] { "AkteId" });
            DropIndex("dbo.DataPejabatBerwenangs", new[] { "TtdFileId" });
            DropIndex("dbo.DataPejabatBerwenangs", new[] { "NewRegId" });
            DropIndex("dbo.DataPejabatBerwenangs", new[] { "AkteId" });
            DropIndex("dbo.DataNamas", new[] { "AkteId" });
            DropIndex("dbo.DataModalDisetors", new[] { "AkteId" });
            DropIndex("dbo.DataModalDasars", new[] { "AkteId" });
            DropIndex("dbo.DataKomisaris", new[] { "TtdFileId" });
            DropIndex("dbo.DataKomisaris", new[] { "NewRegId" });
            DropIndex("dbo.DataKomisaris", new[] { "AkteId" });
            DropIndex("dbo.DataInformasiLains", new[] { "KTPFileId" });
            DropIndex("dbo.DataInformasiLains", new[] { "NPWPFileId" });
            DropIndex("dbo.DataInformasiLains", new[] { "SPAKFileId" });
            DropIndex("dbo.DataInformasiLains", new[] { "SPABFileId" });
            DropIndex("dbo.DataInformasiLains", new[] { "IjinUsahaFileId" });
            DropIndex("dbo.DataInformasiLains", new[] { "LaporanKeuanganFileId" });
            DropIndex("dbo.DataInformasiLains", new[] { "AktePerubahanFileId" });
            DropIndex("dbo.DataInformasiLains", new[] { "AktePendirianFileId" });
            DropIndex("dbo.DataInformasiLains", new[] { "AnggaranDasarFileId" });
            DropIndex("dbo.DataDireksis", new[] { "TtdFileId" });
            DropIndex("dbo.DataDireksis", new[] { "NewRegId" });
            DropIndex("dbo.DataDireksis", new[] { "AkteId" });
            DropIndex("dbo.DataContactPersons", new[] { "NewRegId" });
            DropIndex("dbo.DataContactPersons", new[] { "SuratKeteranganFileId" });
            DropIndex("dbo.DataBankPembayarans", new[] { "SuratKeteranganFileId" });
            DropIndex("dbo.DataAlamats", new[] { "SuratKeteranganFileId" });
            DropIndex("dbo.DataRegistrasis", new[] { "KTPFileId" });
            DropIndex("dbo.DataRegistrasis", new[] { "NPWPFileId" });
            DropIndex("dbo.DataRegistrasis", new[] { "SPAKFileId" });
            DropIndex("dbo.DataRegistrasis", new[] { "SPABFileId" });
            DropIndex("dbo.DataRegistrasis", new[] { "IjinUsahaFileId" });
            DropIndex("dbo.DataRegistrasis", new[] { "LaporanKeuanganFileId" });
            DropIndex("dbo.DataRegistrasis", new[] { "AktePerubahanFileId" });
            DropIndex("dbo.DataRegistrasis", new[] { "AktePendirianFileId" });
            DropIndex("dbo.DataRegistrasis", new[] { "AnggaranDasarFileId" });
            DropIndex("dbo.Files", new[] { "NewRegId" });
            DropIndex("dbo.DataAktes", new[] { "NewRegId" });
            DropIndex("dbo.DataAktes", new[] { "AkteFileId" });
            DropTable("dbo.SurveiPerusahaans");
            DropTable("dbo.SurveiDivisis");
            DropTable("dbo.Surveis");
            DropTable("dbo.PincodeRequests");
            DropTable("dbo.MclearsRequests");
            DropTable("dbo.MclearsPesertas");
            DropTable("dbo.KeuanganRequests");
            DropTable("dbo.KeuanganLaporans");
            DropTable("dbo.HakAkses");
            DropTable("dbo.EventPesertas");
            DropTable("dbo.EventPembicaras");
            DropTable("dbo.Events");
            DropTable("dbo.DataStatusKPEIs");
            DropTable("dbo.DataStatusBursas");
            DropTable("dbo.DataRequests");
            DropTable("dbo.DataProfils");
            DropTable("dbo.DataPMEs");
            DropTable("dbo.DataPerjanjianKBOS");
            DropTable("dbo.DataPerjanjianEBUs");
            DropTable("dbo.DataPemegangSahams");
            DropTable("dbo.DataPejabatBerwenangs");
            DropTable("dbo.DataNamas");
            DropTable("dbo.DataModalDisetors");
            DropTable("dbo.DataModalDasars");
            DropTable("dbo.DataKomisaris");
            DropTable("dbo.DataJenisUsahas");
            DropTable("dbo.DataInformasiLains");
            DropTable("dbo.DataDireksis");
            DropTable("dbo.DataContactPersons");
            DropTable("dbo.DataBankPembayarans");
            DropTable("dbo.DataAlamats");
            DropTable("dbo.DataRegistrasis");
            DropTable("dbo.Files");
            DropTable("dbo.DataAktes");
        }
    }
}
