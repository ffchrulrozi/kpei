namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUserCompanyCodeColumnToUserActionLogsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserActionLogs", "UserAknpCode", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserActionLogs", "UserAknpCode");
        }
    }
}
