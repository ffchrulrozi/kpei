namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remigrateTipeMemberPartisipan : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataTipeMemberPartisipanRegistrasis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RegistrasiId = c.String(nullable: false, maxLength: 128),
                        Nama = c.String(nullable: false, maxLength: 128),
                        Value = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataRegistrasis", t => t.RegistrasiId, cascadeDelete: true)
                .Index(t => t.RegistrasiId);
            
            CreateTable(
                "dbo.DataTipeMemberPartisipanItems",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ParentDataId = c.String(nullable: false, maxLength: 128),
                        Nama = c.String(nullable: false, maxLength: 128),
                        Value = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataTipeMembers", t => t.ParentDataId, cascadeDelete: true)
                .Index(t => t.ParentDataId);
            
            DropColumn("dbo.DataRegistrasis", "TipeMemberPartisipan");
            DropColumn("dbo.DataTipeMembers", "TipeMemberPartisipan");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataTipeMembers", "TipeMemberPartisipan", c => c.String(maxLength: 128));
            AddColumn("dbo.DataRegistrasis", "TipeMemberPartisipan", c => c.String(maxLength: 128));
            DropForeignKey("dbo.DataTipeMemberPartisipanItems", "ParentDataId", "dbo.DataTipeMembers");
            DropForeignKey("dbo.DataTipeMemberPartisipanRegistrasis", "RegistrasiId", "dbo.DataRegistrasis");
            DropIndex("dbo.DataTipeMemberPartisipanItems", new[] { "ParentDataId" });
            DropIndex("dbo.DataTipeMemberPartisipanRegistrasis", new[] { "RegistrasiId" });
            DropTable("dbo.DataTipeMemberPartisipanItems");
            DropTable("dbo.DataTipeMemberPartisipanRegistrasis");
        }
    }
}
