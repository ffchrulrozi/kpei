namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class improveDataPejabatBerwenangDbModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataPejabatBerwenangItems",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ParentDataId = c.String(nullable: false, maxLength: 128),
                        TtdFileUrl = c.String(maxLength: 256),
                        Nama = c.String(nullable: false, maxLength: 128),
                        Jabatan = c.String(nullable: false, maxLength: 128),
                        Email = c.String(nullable: false, maxLength: 128),
                        Telp = c.String(nullable: false, maxLength: 128),
                        HP = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataPejabatBerwenangs", t => t.ParentDataId, cascadeDelete: true)
                .Index(t => t.ParentDataId);
            
            DropColumn("dbo.DataPejabatBerwenangs", "Nama");
            DropColumn("dbo.DataPejabatBerwenangs", "Jabatan");
            DropColumn("dbo.DataPejabatBerwenangs", "Email");
            DropColumn("dbo.DataPejabatBerwenangs", "Telp");
            DropColumn("dbo.DataPejabatBerwenangs", "HP");
            DropColumn("dbo.DataPejabatBerwenangs", "TtdFileUrl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataPejabatBerwenangs", "TtdFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataPejabatBerwenangs", "HP", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPejabatBerwenangs", "Telp", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPejabatBerwenangs", "Email", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPejabatBerwenangs", "Jabatan", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPejabatBerwenangs", "Nama", c => c.String(maxLength: 128));
            DropForeignKey("dbo.DataPejabatBerwenangItems", "ParentDataId", "dbo.DataPejabatBerwenangs");
            DropIndex("dbo.DataPejabatBerwenangItems", new[] { "ParentDataId" });
            DropTable("dbo.DataPejabatBerwenangItems");
        }
    }
}
