namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateDataStatusKPEI : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataStatusKPEIs", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataStatusKPEIs", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataStatusKPEIs", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataStatusKPEIs", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataStatusKPEIs", "Approver2UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataBankPembayarans", "BankPembayaran", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataStatusKPEIs", "StatusKPEI", c => c.String(maxLength: 128));
            DropColumn("dbo.DataStatusKPEIs", "TglAktifUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataStatusKPEIs", "TglAktifUtc", c => c.DateTime());
            AlterColumn("dbo.DataStatusKPEIs", "StatusKPEI", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataBankPembayarans", "BankPembayaran", c => c.String());
            DropColumn("dbo.DataStatusKPEIs", "Approver2UserId");
            DropColumn("dbo.DataStatusKPEIs", "Approver1UserId");
            DropColumn("dbo.DataStatusKPEIs", "RejectReason");
            DropColumn("dbo.DataStatusKPEIs", "ActiveUtc");
            DropColumn("dbo.DataStatusKPEIs", "Status");
        }
    }
}
