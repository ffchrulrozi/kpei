namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mclearsrelation201803261420 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MclearsRequestToPesertas",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RequestId = c.String(),
                        DetailId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MclearsRequestToPesertas");
        }
    }
}
