namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addApproverInPincode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PincodeRequests", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.PincodeRequests", "Approver2UserId", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PincodeRequests", "Approver2UserId");
            DropColumn("dbo.PincodeRequests", "Approver1UserId");
        }
    }
}
