namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class extendsLengthofKeteranganInStatusBursaAndStatusKpei : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataStatusBursas", "Keterangan", c => c.String(maxLength: 1024));
            AlterColumn("dbo.DataStatusKPEIs", "Keterangan", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataStatusKPEIs", "Keterangan", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataStatusBursas", "Keterangan", c => c.String(maxLength: 128));
        }
    }
}
