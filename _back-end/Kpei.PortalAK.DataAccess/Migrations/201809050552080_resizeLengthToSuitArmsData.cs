namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class resizeLengthToSuitArmsData : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataPerjanjianEBUs", "Keterangan", c => c.String(maxLength: 512));
            AlterColumn("dbo.DataPerjanjianKBOS", "Keterangan", c => c.String(maxLength: 512));
            AlterColumn("dbo.DataStatusBursas", "Alasan", c => c.String(maxLength: 512));
            AlterColumn("dbo.DataStatusBursas", "Keterangan", c => c.String(maxLength: 512));
            AlterColumn("dbo.DataStatusKPEIs", "Alasan", c => c.String(maxLength: 512));
            AlterColumn("dbo.DataStatusKPEIs", "Keterangan", c => c.String(maxLength: 512));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataStatusKPEIs", "Keterangan", c => c.String(maxLength: 1024));
            AlterColumn("dbo.DataStatusKPEIs", "Alasan", c => c.String(maxLength: 1024));
            AlterColumn("dbo.DataStatusBursas", "Keterangan", c => c.String(maxLength: 1024));
            AlterColumn("dbo.DataStatusBursas", "Alasan", c => c.String(maxLength: 1024));
            AlterColumn("dbo.DataPerjanjianKBOS", "Keterangan", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "Keterangan", c => c.String(maxLength: 256));
        }
    }
}
