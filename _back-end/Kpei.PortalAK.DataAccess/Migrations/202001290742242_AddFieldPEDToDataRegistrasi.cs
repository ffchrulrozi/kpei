namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldPEDToDataRegistrasi : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataRegistrasis", "PED", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataRegistrasis", "PED");
        }
    }
}
