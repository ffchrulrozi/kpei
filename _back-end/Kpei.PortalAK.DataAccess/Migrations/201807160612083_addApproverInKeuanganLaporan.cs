namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addApproverInKeuanganLaporan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.KeuanganLaporans", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.KeuanganLaporans", "Approver2UserId", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.KeuanganLaporans", "Approver2UserId");
            DropColumn("dbo.KeuanganLaporans", "Approver1UserId");
        }
    }
}
