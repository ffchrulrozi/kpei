namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateDataStatusBursa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataStatusBursas", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataStatusBursas", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataStatusBursas", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataStatusBursas", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataStatusBursas", "Approver2UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataStatusBursas", "StatusBursa", c => c.String(maxLength: 128));
            DropColumn("dbo.DataStatusBursas", "TglAktifUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataStatusBursas", "TglAktifUtc", c => c.DateTime());
            AlterColumn("dbo.DataStatusBursas", "StatusBursa", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.DataStatusBursas", "Approver2UserId");
            DropColumn("dbo.DataStatusBursas", "Approver1UserId");
            DropColumn("dbo.DataStatusBursas", "RejectReason");
            DropColumn("dbo.DataStatusBursas", "ActiveUtc");
            DropColumn("dbo.DataStatusBursas", "Status");
        }
    }
}
