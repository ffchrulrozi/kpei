namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeFileDbModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Files", "NewRegId", "dbo.DataRegistrasis");
            DropIndex("dbo.Files", new[] { "NewRegId" });
            DropTable("dbo.Files");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        NewRegId = c.String(maxLength: 128),
                        FilePath = c.String(nullable: false),
                        FileName = c.String(),
                        CreatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        TglAktifUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Files", "NewRegId");
            AddForeignKey("dbo.Files", "NewRegId", "dbo.DataRegistrasis", "Id");
        }
    }
}
