namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editLembarSahamToDecimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataPemegangSahams", "LembarSaham", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.DataPemegangSahamRegistrasis", "LembarSaham", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataPemegangSahamRegistrasis", "LembarSaham", c => c.Int(nullable: false));
            AlterColumn("dbo.DataPemegangSahams", "LembarSaham", c => c.Int(nullable: false));
        }
    }
}
