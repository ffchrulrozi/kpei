namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeDataTipeMemberAndHirarki : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.DataTipeMemberDanHirarkis");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DataTipeMemberDanHirarkis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        JenisKeanggotaan = c.String(nullable: false, maxLength: 128),
                        Parent = c.String(nullable: false, maxLength: 128),
                        SuratKeteranganFileUrl = c.String(maxLength: 256),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
