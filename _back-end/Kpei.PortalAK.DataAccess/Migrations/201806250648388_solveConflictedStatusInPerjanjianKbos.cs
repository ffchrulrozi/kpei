namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class solveConflictedStatusInPerjanjianKbos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataPerjanjianKBOS", "StatusKBOS", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataPerjanjianKBOS", "StatusKBOS");
        }
    }
}
