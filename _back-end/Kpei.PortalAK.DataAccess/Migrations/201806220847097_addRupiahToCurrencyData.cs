namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRupiahToCurrencyData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataModalDasars", "NominalModalRupiah", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DataModalDisetors", "NominalModalRupiah", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DataPemegangSahams", "NominalSahamRupiah", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.DataModalDasars", "NominalModal");
            DropColumn("dbo.DataModalDisetors", "NominalModal");
            DropColumn("dbo.DataPemegangSahams", "NominalSaham");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataPemegangSahams", "NominalSaham", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DataModalDisetors", "NominalModal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DataModalDasars", "NominalModal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.DataPemegangSahams", "NominalSahamRupiah");
            DropColumn("dbo.DataModalDisetors", "NominalModalRupiah");
            DropColumn("dbo.DataModalDasars", "NominalModalRupiah");
        }
    }
}
