namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove2ndAnd3rdReminder : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Deadlines", "Reminder2");
            DropColumn("dbo.Deadlines", "Reminder3");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deadlines", "Reminder3", c => c.Int(nullable: false));
            AddColumn("dbo.Deadlines", "Reminder2", c => c.Int(nullable: false));
        }
    }
}
