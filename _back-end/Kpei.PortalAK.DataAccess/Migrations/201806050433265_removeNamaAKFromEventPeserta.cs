namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeNamaAKFromEventPeserta : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.EventPesertas", "NamaAK");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EventPesertas", "NamaAK", c => c.String(maxLength: 128));
        }
    }
}
