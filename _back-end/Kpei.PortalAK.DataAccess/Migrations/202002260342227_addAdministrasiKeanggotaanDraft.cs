namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAdministrasiKeanggotaanDraft : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdministrasiKeanggotaans", "Draft", c => c.Boolean(nullable: false));
            AddColumn("dbo.AdministrasiKeanggotaans", "UserDraft", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AdministrasiKeanggotaans", "UserDraft");
            DropColumn("dbo.AdministrasiKeanggotaans", "Draft");
        }
    }
}
