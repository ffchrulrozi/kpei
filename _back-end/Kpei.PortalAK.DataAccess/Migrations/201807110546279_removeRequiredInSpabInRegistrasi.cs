namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeRequiredInSpabInRegistrasi : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataRegistrasis", "NoSPAB", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "TanggalSPABUtc", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataRegistrasis", "TanggalSPABUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataRegistrasis", "NoSPAB", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
