namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editKeuanganLog201804131034 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.KeuanganLogs", "RequestId", "dbo.KeuanganRequests");
            DropIndex("dbo.KeuanganLogs", new[] { "RequestId" });
            AddColumn("dbo.KeuanganLogs", "LaporanId", c => c.String(maxLength: 128));
            CreateIndex("dbo.KeuanganLogs", "LaporanId");
            AddForeignKey("dbo.KeuanganLogs", "LaporanId", "dbo.KeuanganLaporans", "Id");
            DropColumn("dbo.KeuanganLogs", "RequestId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.KeuanganLogs", "RequestId", c => c.String(maxLength: 128));
            DropForeignKey("dbo.KeuanganLogs", "LaporanId", "dbo.KeuanganLaporans");
            DropIndex("dbo.KeuanganLogs", new[] { "LaporanId" });
            DropColumn("dbo.KeuanganLogs", "LaporanId");
            CreateIndex("dbo.KeuanganLogs", "RequestId");
            AddForeignKey("dbo.KeuanganLogs", "RequestId", "dbo.KeuanganRequests", "Id");
        }
    }
}
