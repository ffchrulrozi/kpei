namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addIsTrashed201804121458 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventPesertas", "isTrashed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventPesertas", "isTrashed");
        }
    }
}
