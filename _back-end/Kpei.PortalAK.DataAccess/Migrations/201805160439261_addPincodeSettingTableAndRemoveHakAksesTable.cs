namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPincodeSettingTableAndRemoveHakAksesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PincodeSettings",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PincodeEclearsAdminTables",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        PincodeSettingId = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 128),
                        Email = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PincodeSettings", t => t.PincodeSettingId, cascadeDelete: true)
                .Index(t => t.PincodeSettingId);
            
            CreateTable(
                "dbo.PincodeTypeInfoTables",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        PincodeSettingId = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 128),
                        Url = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PincodeSettings", t => t.PincodeSettingId, cascadeDelete: true)
                .Index(t => t.PincodeSettingId);
            
            DropTable("dbo.HakAkses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.HakAkses",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        NamaModul = c.String(nullable: false, maxLength: 50),
                        HasApprove = c.Boolean(nullable: false),
                        HasModify = c.Boolean(nullable: false),
                        HasView = c.Boolean(nullable: false),
                        HasViewAsKpei = c.Boolean(nullable: false),
                        HasAdmin = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.PincodeTypeInfoTables", "PincodeSettingId", "dbo.PincodeSettings");
            DropForeignKey("dbo.PincodeEclearsAdminTables", "PincodeSettingId", "dbo.PincodeSettings");
            DropIndex("dbo.PincodeTypeInfoTables", new[] { "PincodeSettingId" });
            DropIndex("dbo.PincodeEclearsAdminTables", new[] { "PincodeSettingId" });
            DropTable("dbo.PincodeTypeInfoTables");
            DropTable("dbo.PincodeEclearsAdminTables");
            DropTable("dbo.PincodeSettings");
        }
    }
}
