namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFilePerjanjianToDataPerjanjian : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataPerjanjians", "PerjanjianTitipEfekUntukPinjamFileUrl", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataPerjanjians", "PerjanjianTitipEfekUntukPinjamFileUrl");
        }
    }
}
