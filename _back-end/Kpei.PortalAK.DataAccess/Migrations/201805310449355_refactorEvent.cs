namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class refactorEvent : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Events", "BrosurFileId", "dbo.Files");
            DropForeignKey("dbo.Events", "MateriFileId", "dbo.Files");
            DropForeignKey("dbo.EventPembicaras", "EventId", "dbo.Events");
            DropForeignKey("dbo.EventPesertas", "EventId", "dbo.Events");
            DropIndex("dbo.Events", new[] { "BrosurFileId" });
            DropIndex("dbo.Events", new[] { "MateriFileId" });
            DropIndex("dbo.EventPembicaras", new[] { "EventId" });
            DropIndex("dbo.EventPesertas", new[] { "EventId" });
            AddColumn("dbo.Events", "TglPelaksanaanStart", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "TglPelaksanaanEnd", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "BrosurFileUrl", c => c.String(maxLength: 128));
            AddColumn("dbo.Events", "MateriFileUrl", c => c.String(maxLength: 128));
            AlterColumn("dbo.Events", "Judul", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Events", "Highlight", c => c.String(maxLength: 256));
            AlterColumn("dbo.Events", "Tempat", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.Events", "JenisPelatihan", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Events", "StatusEvent", c => c.String(maxLength: 128));
            AlterColumn("dbo.Events", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Events", "UpdatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Events", "TglAktifUtc", c => c.DateTime());
            AlterColumn("dbo.EventPembicaras", "EventId", c => c.String(nullable: false));
            AlterColumn("dbo.EventPembicaras", "Nama", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.EventPembicaras", "Jabatan", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.EventPembicaras", "Email", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.EventPembicaras", "Telp", c => c.String(maxLength: 128));
            AlterColumn("dbo.EventPembicaras", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.EventPembicaras", "UpdatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.EventPesertas", "EventId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.EventPesertas", "KodeAK", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.EventPesertas", "NamaAK", c => c.String(maxLength: 128));
            AlterColumn("dbo.EventPesertas", "Nama", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.EventPesertas", "Jabatan", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.EventPesertas", "Email", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.EventPesertas", "Telp", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.EventPesertas", "CreatedUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.EventPesertas", "UpdatedUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.Events", "TglPelaksanaan");
            DropColumn("dbo.Events", "WaktuMulai");
            DropColumn("dbo.Events", "WaktuSelesai");
            DropColumn("dbo.Events", "BrosurFileId");
            DropColumn("dbo.Events", "MateriFileId");
            DropColumn("dbo.Events", "isTrashed");
            DropColumn("dbo.EventPembicaras", "TglAktifUtc");
            DropColumn("dbo.EventPesertas", "isTrashed");
            DropColumn("dbo.EventPesertas", "TglAktifUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EventPesertas", "TglAktifUtc", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.EventPesertas", "isTrashed", c => c.Boolean(nullable: false));
            AddColumn("dbo.EventPembicaras", "TglAktifUtc", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Events", "isTrashed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Events", "MateriFileId", c => c.String(maxLength: 128));
            AddColumn("dbo.Events", "BrosurFileId", c => c.String(maxLength: 128));
            AddColumn("dbo.Events", "WaktuSelesai", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "WaktuMulai", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "TglPelaksanaan", c => c.DateTime(nullable: false));
            AlterColumn("dbo.EventPesertas", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.EventPesertas", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.EventPesertas", "Telp", c => c.String(nullable: false));
            AlterColumn("dbo.EventPesertas", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.EventPesertas", "Jabatan", c => c.String(nullable: false));
            AlterColumn("dbo.EventPesertas", "Nama", c => c.String(nullable: false));
            AlterColumn("dbo.EventPesertas", "NamaAK", c => c.String());
            AlterColumn("dbo.EventPesertas", "KodeAK", c => c.String(nullable: false));
            AlterColumn("dbo.EventPesertas", "EventId", c => c.String(maxLength: 128));
            AlterColumn("dbo.EventPembicaras", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.EventPembicaras", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.EventPembicaras", "Telp", c => c.String());
            AlterColumn("dbo.EventPembicaras", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.EventPembicaras", "Jabatan", c => c.String(nullable: false));
            AlterColumn("dbo.EventPembicaras", "Nama", c => c.String(nullable: false));
            AlterColumn("dbo.EventPembicaras", "EventId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Events", "TglAktifUtc", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Events", "UpdatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Events", "CreatedUtc", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Events", "StatusEvent", c => c.String());
            AlterColumn("dbo.Events", "JenisPelatihan", c => c.String(nullable: false));
            AlterColumn("dbo.Events", "Tempat", c => c.String(nullable: false));
            AlterColumn("dbo.Events", "Highlight", c => c.String());
            AlterColumn("dbo.Events", "Judul", c => c.String(nullable: false));
            DropColumn("dbo.Events", "MateriFileUrl");
            DropColumn("dbo.Events", "BrosurFileUrl");
            DropColumn("dbo.Events", "TglPelaksanaanEnd");
            DropColumn("dbo.Events", "TglPelaksanaanStart");
            CreateIndex("dbo.EventPesertas", "EventId");
            CreateIndex("dbo.EventPembicaras", "EventId");
            CreateIndex("dbo.Events", "MateriFileId");
            CreateIndex("dbo.Events", "BrosurFileId");
            AddForeignKey("dbo.EventPesertas", "EventId", "dbo.Events", "Id");
            AddForeignKey("dbo.EventPembicaras", "EventId", "dbo.Events", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Events", "MateriFileId", "dbo.Files", "Id");
            AddForeignKey("dbo.Events", "BrosurFileId", "dbo.Files", "Id");
        }
    }
}
