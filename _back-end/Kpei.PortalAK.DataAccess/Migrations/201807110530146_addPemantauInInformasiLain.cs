namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPemantauInInformasiLain : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataInformasiLains", "Pemantau", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataInformasiLains", "Pemantau");
        }
    }
}
