namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addApproverLayananJasa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LayananJasas", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.LayananJasas", "Approver2UserId", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LayananJasas", "Approver2UserId");
            DropColumn("dbo.LayananJasas", "Approver1UserId");
        }
    }
}
