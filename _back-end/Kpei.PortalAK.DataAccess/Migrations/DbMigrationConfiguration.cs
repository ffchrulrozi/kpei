using System.Data.Entity.Migrations;
using Kpei.PortalAK.DataAccess.Database;

namespace Kpei.PortalAK.DataAccess.Migrations {

    public sealed class DbMigrationConfiguration : DbMigrationsConfiguration<AppDbContext> {
        public DbMigrationConfiguration() {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AppDbContext context) {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}