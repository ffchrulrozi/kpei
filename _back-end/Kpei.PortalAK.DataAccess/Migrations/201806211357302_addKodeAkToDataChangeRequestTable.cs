namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addKodeAkToDataChangeRequestTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataChangeRequests", "KodeAk", c => c.String(nullable: false, maxLength: 32));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataChangeRequests", "KodeAk");
        }
    }
}
