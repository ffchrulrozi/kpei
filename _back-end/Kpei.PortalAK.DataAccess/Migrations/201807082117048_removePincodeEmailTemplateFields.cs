namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removePincodeEmailTemplateFields : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PincodeSettings", "EmailTemplateForKpei");
            DropColumn("dbo.PincodeSettings", "EmailTemplateForAknp");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PincodeSettings", "EmailTemplateForAknp", c => c.String(maxLength: 2048));
            AddColumn("dbo.PincodeSettings", "EmailTemplateForKpei", c => c.String(maxLength: 2048));
        }
    }
}
