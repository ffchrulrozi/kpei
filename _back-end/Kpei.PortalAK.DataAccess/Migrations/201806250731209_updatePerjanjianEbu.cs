namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatePerjanjianEbu : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataInformasiLains", "TglSerahDokumenUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataInformasiLains", "TglSerahDanaMinCashUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataInformasiLains", "TglAktaPengikatanUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataInformasiLains", "TglRekomendasiUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataPerjanjianEBUs", "TglAktif", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataPerjanjianEBUs", "Keterangan", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataPerjanjianKBOS", "TanggalAktifUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataSPABs", "TanggalSPABUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataSPABs", "SPABFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataSPAKs", "TanggalSPAKUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataSPAKs", "SPAKFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataStatusBursas", "TanggalAktifUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusBursas", "TanggalSuspendAwalUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusBursas", "TanggalSuspendAkhirUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusBursas", "TanggalCabutUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "TanggalAwalStatusUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "TanggalAkhirStatusUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "TanggalAktifUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.DataInformasiLains", "NoSPAK");
            DropColumn("dbo.DataInformasiLains", "TglSPAKUtc");
            DropColumn("dbo.DataInformasiLains", "NoSPAB");
            DropColumn("dbo.DataInformasiLains", "TglSPABUtc");
            DropColumn("dbo.DataInformasiLains", "SPABFileUrl");
            DropColumn("dbo.DataInformasiLains", "SPAKFileUrl");
            DropColumn("dbo.DataPerjanjianKBOS", "TanggalAktif");
            DropColumn("dbo.DataSPABs", "TanggalSPAB");
            DropColumn("dbo.DataSPAKs", "TanggalSPAK");
            DropColumn("dbo.DataStatusBursas", "TanggalAktif");
            DropColumn("dbo.DataStatusBursas", "TanggalSuspendAwal");
            DropColumn("dbo.DataStatusBursas", "TanggalSuspendAkhir");
            DropColumn("dbo.DataStatusBursas", "TanggalCabut");
            DropColumn("dbo.DataStatusKPEIs", "TanggalAwalStatus");
            DropColumn("dbo.DataStatusKPEIs", "TanggalAkhirStatus");
            DropColumn("dbo.DataStatusKPEIs", "TanggalAktif");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataStatusKPEIs", "TanggalAktif", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "TanggalAkhirStatus", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "TanggalAwalStatus", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusBursas", "TanggalCabut", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusBursas", "TanggalSuspendAkhir", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusBursas", "TanggalSuspendAwal", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusBursas", "TanggalAktif", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataSPAKs", "TanggalSPAK", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataSPABs", "TanggalSPAB", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataPerjanjianKBOS", "TanggalAktif", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataInformasiLains", "SPAKFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataInformasiLains", "SPABFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataInformasiLains", "TglSPABUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataInformasiLains", "NoSPAB", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataInformasiLains", "TglSPAKUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataInformasiLains", "NoSPAK", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.DataStatusKPEIs", "TanggalAktifUtc");
            DropColumn("dbo.DataStatusKPEIs", "TanggalAkhirStatusUtc");
            DropColumn("dbo.DataStatusKPEIs", "TanggalAwalStatusUtc");
            DropColumn("dbo.DataStatusBursas", "TanggalCabutUtc");
            DropColumn("dbo.DataStatusBursas", "TanggalSuspendAkhirUtc");
            DropColumn("dbo.DataStatusBursas", "TanggalSuspendAwalUtc");
            DropColumn("dbo.DataStatusBursas", "TanggalAktifUtc");
            DropColumn("dbo.DataSPAKs", "SPAKFileUrl");
            DropColumn("dbo.DataSPAKs", "TanggalSPAKUtc");
            DropColumn("dbo.DataSPABs", "SPABFileUrl");
            DropColumn("dbo.DataSPABs", "TanggalSPABUtc");
            DropColumn("dbo.DataPerjanjianKBOS", "TanggalAktifUtc");
            DropColumn("dbo.DataPerjanjianEBUs", "Keterangan");
            DropColumn("dbo.DataPerjanjianEBUs", "TglAktif");
            DropColumn("dbo.DataInformasiLains", "TglRekomendasiUtc");
            DropColumn("dbo.DataInformasiLains", "TglAktaPengikatanUtc");
            DropColumn("dbo.DataInformasiLains", "TglSerahDanaMinCashUtc");
            DropColumn("dbo.DataInformasiLains", "TglSerahDokumenUtc");
        }
    }
}
