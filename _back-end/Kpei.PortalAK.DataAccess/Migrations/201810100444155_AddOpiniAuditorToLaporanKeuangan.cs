namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOpiniAuditorToLaporanKeuangan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.KeuanganLaporans", "OpiniAuditor", c => c.String(nullable: false, maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.KeuanganLaporans", "OpiniAuditor");
        }
    }
}
