namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixDateTimeToDateTimeNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataPerjanjianEBUs", "TglAktif", c => c.DateTime());
            AlterColumn("dbo.DataPerjanjianKBOS", "TanggalAktifUtc", c => c.DateTime());
            AlterColumn("dbo.DataPMEs", "LenderTglAwalUtc", c => c.DateTime());
            AlterColumn("dbo.DataPMEs", "LenderTglAkhirUtc", c => c.DateTime());
            AlterColumn("dbo.DataPMEs", "BorrowerTglAwalUtc", c => c.DateTime());
            AlterColumn("dbo.DataPMEs", "BorrowerTglAkhirUtc", c => c.DateTime());
            AlterColumn("dbo.DataStatusBursas", "TanggalAktifUtc", c => c.DateTime());
            AlterColumn("dbo.DataStatusBursas", "TanggalSuspendAwalUtc", c => c.DateTime());
            AlterColumn("dbo.DataStatusBursas", "TanggalSuspendAkhirUtc", c => c.DateTime());
            AlterColumn("dbo.DataStatusBursas", "TanggalCabutUtc", c => c.DateTime());
            AlterColumn("dbo.DataStatusKPEIs", "TanggalAkhirStatusUtc", c => c.DateTime());
            AlterColumn("dbo.DataStatusKPEIs", "TanggalAktifUtc", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataStatusKPEIs", "TanggalAktifUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataStatusKPEIs", "TanggalAkhirStatusUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataStatusBursas", "TanggalCabutUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataStatusBursas", "TanggalSuspendAkhirUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataStatusBursas", "TanggalSuspendAwalUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataStatusBursas", "TanggalAktifUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataPMEs", "BorrowerTglAkhirUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataPMEs", "BorrowerTglAwalUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataPMEs", "LenderTglAkhirUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataPMEs", "LenderTglAwalUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataPerjanjianKBOS", "TanggalAktifUtc", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DataPerjanjianEBUs", "TglAktif", c => c.DateTime(nullable: false));
        }
    }
}
