namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renameTanggalPerjanjianColumnInDataPerjanjianTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataPerjanjians", "TglPerjanjianUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.DataPerjanjians", "TglPerjanjian");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataPerjanjians", "TglPerjanjian", c => c.DateTime(nullable: false));
            DropColumn("dbo.DataPerjanjians", "TglPerjanjianUtc");
        }
    }
}
