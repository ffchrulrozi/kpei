namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RestructureModel : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataDireksis", "Email", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataDireksis", "Telp", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataDireksis", "HP", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataKomisaris", "Email", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataKomisaris", "Telp", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataKomisaris", "HP", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataDireksiRegistrasis", "Email", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataDireksiRegistrasis", "Telp", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataDireksiRegistrasis", "HP", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataKomisarisRegistrasis", "Email", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataKomisarisRegistrasis", "Telp", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataKomisarisRegistrasis", "HP", c => c.String(maxLength: 128));
            AlterColumn("dbo.Events", "Highlight", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Events", "Highlight", c => c.String(maxLength: 256));
            AlterColumn("dbo.DataKomisarisRegistrasis", "HP", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataKomisarisRegistrasis", "Telp", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataKomisarisRegistrasis", "Email", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataDireksiRegistrasis", "HP", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataDireksiRegistrasis", "Telp", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataDireksiRegistrasis", "Email", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataKomisaris", "HP", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataKomisaris", "Telp", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataKomisaris", "Email", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataDireksis", "HP", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataDireksis", "Telp", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataDireksis", "Email", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
