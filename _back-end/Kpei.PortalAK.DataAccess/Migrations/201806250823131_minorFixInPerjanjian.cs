namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class minorFixInPerjanjian : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataPerjanjianEBUs", "Keterangan", c => c.String());
            DropColumn("dbo.DataInformasiLains", "NoPerjanjian");
            DropColumn("dbo.DataInformasiLains", "TglPerjanjianUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataInformasiLains", "TglPerjanjianUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataInformasiLains", "NoPerjanjian", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataPerjanjianEBUs", "Keterangan", c => c.DateTime(nullable: false));
        }
    }
}
