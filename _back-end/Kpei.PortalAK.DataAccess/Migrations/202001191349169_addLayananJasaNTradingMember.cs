namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addLayananJasaNTradingMember : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LayananJasas",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        KategoriMember = c.String(nullable: false, maxLength: 128),
                        NoPerjanjianDenganKPEI = c.String(maxLength: 128),
                        TanggalPerjanjianDenganKPEIUtc = c.DateTime(),
                        PerjanjianDenganKPEIFileUrl = c.String(maxLength: 256),
                        NoPerjanjianPortabilitySponsor = c.String(maxLength: 128),
                        TanggalPerjanjianPortabilitySponsor = c.DateTime(),
                        PerjanjianPortabilitySponsorFileUrl = c.String(maxLength: 256),
                        NamaABSponsor = c.String(maxLength: 128),
                        NoIjinUsahaPED = c.String(maxLength: 128),
                        DokumenKhususIjinUsahaFileUrl = c.String(maxLength: 256),
                        TipeMemberPartisipan = c.String(),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LayananJasaTradingMembers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Nama = c.String(nullable: false, maxLength: 128),
                        NoPerjanjian = c.String(nullable: false, maxLength: 128),
                        TanggalPerjanjian = c.DateTime(nullable: false),
                        PerjanjianFileUrl = c.String(nullable: false, maxLength: 256),
                        LayananJasaId = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LayananJasas", t => t.LayananJasaId, cascadeDelete: true)
                .Index(t => t.LayananJasaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LayananJasaTradingMembers", "LayananJasaId", "dbo.LayananJasas");
            DropIndex("dbo.LayananJasaTradingMembers", new[] { "LayananJasaId" });
            DropTable("dbo.LayananJasaTradingMembers");
            DropTable("dbo.LayananJasas");
        }
    }
}
