namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeStatusPerusahaanFromInformasiLain : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.DataInformasiLains", "JenisPerusahaan");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataInformasiLains", "JenisPerusahaan", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
