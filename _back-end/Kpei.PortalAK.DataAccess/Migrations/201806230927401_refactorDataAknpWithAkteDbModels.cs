namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class refactorDataAknpWithAkteDbModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataDaftarDireksis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        AkteId = c.String(nullable: false, maxLength: 128),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataAktes", t => t.AkteId, cascadeDelete: true)
                .Index(t => t.AkteId);
            
            CreateTable(
                "dbo.DataDaftarKomisaris",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        AkteId = c.String(nullable: false, maxLength: 128),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataAktes", t => t.AkteId, cascadeDelete: true)
                .Index(t => t.AkteId);
            
            CreateTable(
                "dbo.DataDaftarPemegangSahams",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        AkteId = c.String(nullable: false, maxLength: 128),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataAktes", t => t.AkteId, cascadeDelete: true)
                .Index(t => t.AkteId);
            
            CreateTable(
                "dbo.DataModals",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ModalDisetorRupiah = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ModalDasarRupiah = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AkteId = c.String(nullable: false, maxLength: 128),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataAktes", t => t.AkteId, cascadeDelete: true)
                .Index(t => t.AkteId);
            
            AddColumn("dbo.DataAktes", "TglAkte", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataAktes", "Notaris", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataAktes", "SkKemenkumhamFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataAktes", "SkOjkFileUrl", c => c.String(maxLength: 256));
            AddColumn("dbo.DataAktes", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataAktes", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataAktes", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataAktes", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataAktes", "Approver2UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataDireksis", "DataDaftarId", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataKomisaris", "DataDaftarId", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataNamas", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataNamas", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataNamas", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataNamas", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataNamas", "Approver2UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPemegangSahams", "DataDaftarId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataDireksis", "Telp", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataDireksis", "HP", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataKomisaris", "Telp", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataKomisaris", "HP", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataNamas", "AkteId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.DataDireksis", "DataDaftarId");
            CreateIndex("dbo.DataKomisaris", "DataDaftarId");
            CreateIndex("dbo.DataPemegangSahams", "DataDaftarId");
            CreateIndex("dbo.DataNamas", "AkteId");
            AddForeignKey("dbo.DataDireksis", "DataDaftarId", "dbo.DataDaftarDireksis", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DataKomisaris", "DataDaftarId", "dbo.DataDaftarKomisaris", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DataPemegangSahams", "DataDaftarId", "dbo.DataDaftarPemegangSahams", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DataNamas", "AkteId", "dbo.DataAktes", "Id", cascadeDelete: true);
            DropColumn("dbo.DataAktes", "TglSejakUtc");
            DropColumn("dbo.DataAktes", "TglSampaiUtc");
            DropColumn("dbo.DataAktes", "NewRegId");
            DropColumn("dbo.DataAktes", "TglAktifUtc");
            DropColumn("dbo.DataDireksis", "KodeAK");
            DropColumn("dbo.DataDireksis", "AkteId");
            DropColumn("dbo.DataDireksis", "NewRegId");
            DropColumn("dbo.DataDireksis", "TglAktifUtc");
            DropColumn("dbo.DataKomisaris", "KodeAK");
            DropColumn("dbo.DataKomisaris", "AkteId");
            DropColumn("dbo.DataKomisaris", "NewRegId");
            DropColumn("dbo.DataKomisaris", "TglAktifUtc");
            DropColumn("dbo.DataNamas", "TglAktifUtc");
            DropColumn("dbo.DataPemegangSahams", "KodeAK");
            DropColumn("dbo.DataPemegangSahams", "AkteId");
            DropColumn("dbo.DataPemegangSahams", "NewRegId");
            DropColumn("dbo.DataPemegangSahams", "TglAktifUtc");
            DropTable("dbo.DataModalDasars");
            DropTable("dbo.DataModalDisetors");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DataModalDisetors",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        NominalModalRupiah = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AkteId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataModalDasars",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        NominalModalRupiah = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AkteId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                        TglAktifUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.DataPemegangSahams", "TglAktifUtc", c => c.DateTime());
            AddColumn("dbo.DataPemegangSahams", "NewRegId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPemegangSahams", "AkteId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPemegangSahams", "KodeAK", c => c.String(nullable: false, maxLength: 32));
            AddColumn("dbo.DataNamas", "TglAktifUtc", c => c.DateTime());
            AddColumn("dbo.DataKomisaris", "TglAktifUtc", c => c.DateTime());
            AddColumn("dbo.DataKomisaris", "NewRegId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataKomisaris", "AkteId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataKomisaris", "KodeAK", c => c.String(nullable: false, maxLength: 32));
            AddColumn("dbo.DataDireksis", "TglAktifUtc", c => c.DateTime());
            AddColumn("dbo.DataDireksis", "NewRegId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataDireksis", "AkteId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataDireksis", "KodeAK", c => c.String(nullable: false, maxLength: 32));
            AddColumn("dbo.DataAktes", "TglAktifUtc", c => c.DateTime());
            AddColumn("dbo.DataAktes", "NewRegId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataAktes", "TglSampaiUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataAktes", "TglSejakUtc", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.DataNamas", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataModals", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataPemegangSahams", "DataDaftarId", "dbo.DataDaftarPemegangSahams");
            DropForeignKey("dbo.DataDaftarPemegangSahams", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataKomisaris", "DataDaftarId", "dbo.DataDaftarKomisaris");
            DropForeignKey("dbo.DataDaftarKomisaris", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataDireksis", "DataDaftarId", "dbo.DataDaftarDireksis");
            DropForeignKey("dbo.DataDaftarDireksis", "AkteId", "dbo.DataAktes");
            DropIndex("dbo.DataNamas", new[] { "AkteId" });
            DropIndex("dbo.DataModals", new[] { "AkteId" });
            DropIndex("dbo.DataPemegangSahams", new[] { "DataDaftarId" });
            DropIndex("dbo.DataDaftarPemegangSahams", new[] { "AkteId" });
            DropIndex("dbo.DataKomisaris", new[] { "DataDaftarId" });
            DropIndex("dbo.DataDaftarKomisaris", new[] { "AkteId" });
            DropIndex("dbo.DataDireksis", new[] { "DataDaftarId" });
            DropIndex("dbo.DataDaftarDireksis", new[] { "AkteId" });
            AlterColumn("dbo.DataNamas", "AkteId", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataKomisaris", "HP", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.DataKomisaris", "Telp", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.DataDireksis", "HP", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.DataDireksis", "Telp", c => c.String(nullable: false, maxLength: 32));
            DropColumn("dbo.DataPemegangSahams", "DataDaftarId");
            DropColumn("dbo.DataNamas", "Approver2UserId");
            DropColumn("dbo.DataNamas", "Approver1UserId");
            DropColumn("dbo.DataNamas", "RejectReason");
            DropColumn("dbo.DataNamas", "ActiveUtc");
            DropColumn("dbo.DataNamas", "Status");
            DropColumn("dbo.DataKomisaris", "DataDaftarId");
            DropColumn("dbo.DataDireksis", "DataDaftarId");
            DropColumn("dbo.DataAktes", "Approver2UserId");
            DropColumn("dbo.DataAktes", "Approver1UserId");
            DropColumn("dbo.DataAktes", "RejectReason");
            DropColumn("dbo.DataAktes", "ActiveUtc");
            DropColumn("dbo.DataAktes", "Status");
            DropColumn("dbo.DataAktes", "SkOjkFileUrl");
            DropColumn("dbo.DataAktes", "SkKemenkumhamFileUrl");
            DropColumn("dbo.DataAktes", "Notaris");
            DropColumn("dbo.DataAktes", "TglAkte");
            DropTable("dbo.DataModals");
            DropTable("dbo.DataDaftarPemegangSahams");
            DropTable("dbo.DataDaftarKomisaris");
            DropTable("dbo.DataDaftarDireksis");
        }
    }
}
