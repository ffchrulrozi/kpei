namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeSomeMaxPejabatEtcFieldsFromGlobalSetting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GlobalSettings", "MaxCompanyPersonCount", c => c.Int(nullable: false, defaultValue: 10));
            DropColumn("dbo.GlobalSettings", "MaxPengurusPerusahaanCount");
            DropColumn("dbo.GlobalSettings", "MaxPemegangSahamCount");
            DropColumn("dbo.GlobalSettings", "MaxPejabatBerwenangCount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.GlobalSettings", "MaxPejabatBerwenangCount", c => c.Int(nullable: false, defaultValue: 10));
            AddColumn("dbo.GlobalSettings", "MaxPemegangSahamCount", c => c.Int(nullable: false, defaultValue: 10));
            AddColumn("dbo.GlobalSettings", "MaxPengurusPerusahaanCount", c => c.Int(nullable: false, defaultValue: 10));
            DropColumn("dbo.GlobalSettings", "MaxCompanyPersonCount");
        }
    }
}
