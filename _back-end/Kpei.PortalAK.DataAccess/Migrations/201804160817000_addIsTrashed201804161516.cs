namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addIsTrashed201804161516 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.KeuanganLaporans", "isTrashed", c => c.Boolean(nullable: false));
            AddColumn("dbo.KeuanganRequests", "isTrashed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.KeuanganRequests", "isTrashed");
            DropColumn("dbo.KeuanganLaporans", "isTrashed");
        }
    }
}
