namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renameLayananJasatoAdministrasiKeanggotaan : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.LayananJasas", newName: "AdministrasiKeanggotaans");
            DropForeignKey("dbo.LayananJasaTradingMembers", "LayananJasaId", "dbo.LayananJasas");
            DropIndex("dbo.LayananJasaTradingMembers", new[] { "LayananJasaId" });
            CreateTable(
                "dbo.AdministrasiKeanggotaanTradingMembers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Nama = c.String(maxLength: 128),
                        NoPerjanjian = c.String(maxLength: 128),
                        TanggalPerjanjian = c.DateTime(nullable: false),
                        PerjanjianFileUrl = c.String(maxLength: 256),
                        AdministrasiKeanggotaanId = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdministrasiKeanggotaans", t => t.AdministrasiKeanggotaanId, cascadeDelete: true)
                .Index(t => t.AdministrasiKeanggotaanId);
            
            DropTable("dbo.LayananJasaTradingMembers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.LayananJasaTradingMembers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Nama = c.String(maxLength: 128),
                        NoPerjanjian = c.String(maxLength: 128),
                        TanggalPerjanjian = c.DateTime(nullable: false),
                        PerjanjianFileUrl = c.String(maxLength: 256),
                        LayananJasaId = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.AdministrasiKeanggotaanTradingMembers", "AdministrasiKeanggotaanId", "dbo.AdministrasiKeanggotaans");
            DropIndex("dbo.AdministrasiKeanggotaanTradingMembers", new[] { "AdministrasiKeanggotaanId" });
            DropTable("dbo.AdministrasiKeanggotaanTradingMembers");
            CreateIndex("dbo.LayananJasaTradingMembers", "LayananJasaId");
            AddForeignKey("dbo.LayananJasaTradingMembers", "LayananJasaId", "dbo.LayananJasas", "Id", cascadeDelete: true);
            RenameTable(name: "dbo.AdministrasiKeanggotaans", newName: "LayananJasas");
        }
    }
}
