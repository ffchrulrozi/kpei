namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPincodeResetResultTableAndItsNavProps : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PincodeResetResults",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        NewUserName = c.String(nullable: false, maxLength: 256),
                        NewPassword = c.String(nullable: false, maxLength: 256),
                        PincodeRequestId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PincodeRequests", t => t.PincodeRequestId)
                .Index(t => t.PincodeRequestId);
            
            CreateTable(
                "dbo.PincodeResetEmails",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ResetResultId = c.String(maxLength: 128),
                        EmailAddress = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PincodeResetResults", t => t.ResetResultId)
                .Index(t => t.ResetResultId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PincodeResetResults", "PincodeRequestId", "dbo.PincodeRequests");
            DropForeignKey("dbo.PincodeResetEmails", "ResetResultId", "dbo.PincodeResetResults");
            DropIndex("dbo.PincodeResetEmails", new[] { "ResetResultId" });
            DropIndex("dbo.PincodeResetResults", new[] { "PincodeRequestId" });
            DropTable("dbo.PincodeResetEmails");
            DropTable("dbo.PincodeResetResults");
        }
    }
}
