namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSynchronizeTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SynchronizeTables",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RelatedTable = c.String(nullable: false, maxLength: 128),
                        SyncUtc = c.DateTime(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SynchronizeTables");
        }
    }
}
