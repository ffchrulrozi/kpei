namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class improveDataContextPersonDbModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataContactPersonItems",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ParentDataId = c.String(nullable: false, maxLength: 128),
                        Nama = c.String(nullable: false, maxLength: 128),
                        Jabatan = c.String(nullable: false, maxLength: 128),
                        Email = c.String(nullable: false, maxLength: 128),
                        Telp = c.String(nullable: false, maxLength: 128),
                        HP = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataContactPersons", t => t.ParentDataId, cascadeDelete: true)
                .Index(t => t.ParentDataId);
            
            DropColumn("dbo.DataContactPersons", "Nama");
            DropColumn("dbo.DataContactPersons", "Jabatan");
            DropColumn("dbo.DataContactPersons", "Email");
            DropColumn("dbo.DataContactPersons", "Telp");
            DropColumn("dbo.DataContactPersons", "HP");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataContactPersons", "HP", c => c.String(maxLength: 32));
            AddColumn("dbo.DataContactPersons", "Telp", c => c.String(maxLength: 128));
            AddColumn("dbo.DataContactPersons", "Email", c => c.String(maxLength: 128));
            AddColumn("dbo.DataContactPersons", "Jabatan", c => c.String(maxLength: 128));
            AddColumn("dbo.DataContactPersons", "Nama", c => c.String(maxLength: 128));
            DropForeignKey("dbo.DataContactPersonItems", "ParentDataId", "dbo.DataContactPersons");
            DropIndex("dbo.DataContactPersonItems", new[] { "ParentDataId" });
            DropTable("dbo.DataContactPersonItems");
        }
    }
}
