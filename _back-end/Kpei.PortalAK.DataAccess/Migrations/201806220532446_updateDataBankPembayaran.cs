namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateDataBankPembayaran : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataBankPembayarans", "Status", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataBankPembayarans", "ActiveUtc", c => c.DateTime());
            AddColumn("dbo.DataBankPembayarans", "RejectReason", c => c.String(maxLength: 1024));
            AddColumn("dbo.DataBankPembayarans", "Approver1UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DataBankPembayarans", "Approver2UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataBankPembayarans", "BankPembayaran", c => c.String());
            DropColumn("dbo.DataBankPembayarans", "TglAktifUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataBankPembayarans", "TglAktifUtc", c => c.DateTime());
            AlterColumn("dbo.DataBankPembayarans", "BankPembayaran", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.DataBankPembayarans", "Approver2UserId");
            DropColumn("dbo.DataBankPembayarans", "Approver1UserId");
            DropColumn("dbo.DataBankPembayarans", "RejectReason");
            DropColumn("dbo.DataBankPembayarans", "ActiveUtc");
            DropColumn("dbo.DataBankPembayarans", "Status");
        }
    }
}
