namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUserRightTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserRights",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        MemberUserId = c.String(nullable: false, maxLength: 128),
                        ModuleId = c.String(nullable: false, maxLength: 128),
                        KpeiApprover = c.Boolean(nullable: false),
                        KpeiModifier = c.Boolean(nullable: false),
                        KpeiViewer = c.Boolean(nullable: false),
                        KpeiAdmin = c.Boolean(nullable: false),
                        NonKpeiModifier = c.Boolean(nullable: false),
                        NonKpeiViewer = c.Boolean(nullable: false),
                        NonKpeiViewerAsKpei = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserRights");
        }
    }
}
