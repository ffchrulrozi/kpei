namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove2FileFromLaporanKeuangan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.KeuanganLaporans", "LaporanKeuanganFileUrl", c => c.String(nullable: false, maxLength: 256));
            DropColumn("dbo.KeuanganLaporans", "NeracaFileUrl");
            DropColumn("dbo.KeuanganLaporans", "LabaRugiFileUrl");
            DropColumn("dbo.KeuanganLaporans", "ArusKasFileUrl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.KeuanganLaporans", "ArusKasFileUrl", c => c.String(nullable: false, maxLength: 256));
            AddColumn("dbo.KeuanganLaporans", "LabaRugiFileUrl", c => c.String(nullable: false, maxLength: 256));
            AddColumn("dbo.KeuanganLaporans", "NeracaFileUrl", c => c.String(nullable: false, maxLength: 256));
            DropColumn("dbo.KeuanganLaporans", "LaporanKeuanganFileUrl");
        }
    }
}
