namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LayananBaruTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LayananBarus",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        TipeMemberAK = c.String(maxLength: 128),
                        TipeMemberPartisipan = c.String(maxLength: 256),
                        Field = c.String(maxLength: 256),
                        Value = c.String(),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LayananBarus");
        }
    }
}
