namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addGlobalSettingTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GlobalSettings",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        GuestAknpCode = c.String(nullable: false, maxLength: 10),
                        MaxPengurusPerusahaanCount = c.Int(nullable: false),
                        MaxPemegangSahamCount = c.Int(nullable: false),
                        MaxPejabatBerwenangCount = c.Int(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GlobalSettingKkeGroupEmails",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        GlobalSettingId = c.String(maxLength: 128),
                        EmailAddress = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GlobalSettings", t => t.GlobalSettingId)
                .Index(t => t.GlobalSettingId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GlobalSettingKkeGroupEmails", "GlobalSettingId", "dbo.GlobalSettings");
            DropIndex("dbo.GlobalSettingKkeGroupEmails", new[] { "GlobalSettingId" });
            DropTable("dbo.GlobalSettingKkeGroupEmails");
            DropTable("dbo.GlobalSettings");
        }
    }
}
