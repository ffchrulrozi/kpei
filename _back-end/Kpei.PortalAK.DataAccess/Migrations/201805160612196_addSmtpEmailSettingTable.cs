namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSmtpEmailSettingTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SmtpEmailSettings",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FromEmailAddress = c.String(maxLength: 128),
                        SmtpServerAddress = c.String(maxLength: 128),
                        SmtpServerPort = c.Int(),
                        SmtpUserName = c.String(maxLength: 128),
                        SmtpUserPassword = c.String(maxLength: 128),
                        SmtpEnableSsl = c.Boolean(),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SmtpEmailSettings");
        }
    }
}
