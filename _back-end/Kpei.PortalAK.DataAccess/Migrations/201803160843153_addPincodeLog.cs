namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPincodeLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PincodeRequestLogs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RequestId = c.String(maxLength: 128),
                        Author = c.String(),
                        Aktivitas = c.String(),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PincodeRequests", t => t.RequestId)
                .Index(t => t.RequestId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PincodeRequestLogs", "RequestId", "dbo.PincodeRequests");
            DropIndex("dbo.PincodeRequestLogs", new[] { "RequestId" });
            DropTable("dbo.PincodeRequestLogs");
        }
    }
}
