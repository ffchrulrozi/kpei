namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateStatusKpei : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataStatusKPEIs", "NoPengumuman", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataStatusKPEIs", "TanggalAktif", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "TanggalSuspendAwal", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "TanggalSuspendAkhir", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "TanggalCabut", c => c.DateTime(nullable: false));
            AddColumn("dbo.DataStatusKPEIs", "Alasan", c => c.String(maxLength: 128));
            AddColumn("dbo.DataStatusKPEIs", "Keterangan", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataStatusKPEIs", "Keterangan");
            DropColumn("dbo.DataStatusKPEIs", "Alasan");
            DropColumn("dbo.DataStatusKPEIs", "TanggalCabut");
            DropColumn("dbo.DataStatusKPEIs", "TanggalSuspendAkhir");
            DropColumn("dbo.DataStatusKPEIs", "TanggalSuspendAwal");
            DropColumn("dbo.DataStatusKPEIs", "TanggalAktif");
            DropColumn("dbo.DataStatusKPEIs", "NoPengumuman");
        }
    }
}
