namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class missingfieldmclears201803271356 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MclearsPesertas", "AlertDCRA", c => c.Boolean(nullable: false));
            AddColumn("dbo.MclearsPesertas", "AlertMCA", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MclearsPesertas", "AlertMCA");
            DropColumn("dbo.MclearsPesertas", "AlertDCRA");
        }
    }
}
