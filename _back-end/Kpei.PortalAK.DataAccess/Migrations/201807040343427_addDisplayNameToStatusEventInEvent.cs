namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDisplayNameToStatusEventInEvent : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Events", "StatusEvent", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Events", "StatusEvent", c => c.String(maxLength: 128));
        }
    }
}
