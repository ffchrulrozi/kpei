namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editGroupNameFromStatusBursa : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataStatusBursas", "StatusBursa", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataStatusKPEIs", "StatusKPEI", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataStatusKPEIs", "StatusKPEI", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataStatusBursas", "StatusBursa", c => c.String(maxLength: 128));
        }
    }
}
