namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDataTM : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataTMs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        NamaPerusahaan = c.String(nullable: false, maxLength: 128),
                        TipePerusahaan = c.String(nullable: false, maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DataTMs");
        }
    }
}
