namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateGlobalSettingTableAddMaxSubscribePesertaMclears : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GlobalSettings", "MaxSubscribePesertaMclears", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.GlobalSettings", "MaxSubscribePesertaMclears");
        }
    }
}
