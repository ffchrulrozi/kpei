namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeKeuanganLogAndRequestTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.KeuanganLogs", "LaporanId", "dbo.KeuanganLaporans");
            DropForeignKey("dbo.KeuanganRequests", "DetailId", "dbo.KeuanganLaporans");
            DropIndex("dbo.KeuanganLogs", new[] { "LaporanId" });
            DropIndex("dbo.KeuanganRequests", new[] { "DetailId" });
            DropTable("dbo.KeuanganLogs");
            DropTable("dbo.KeuanganRequests");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.KeuanganRequests",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KodeAK = c.String(nullable: false, maxLength: 32),
                        DetailId = c.String(nullable: false, maxLength: 128),
                        StatusRequest = c.String(),
                        isTrashed = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        TglAktifUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.KeuanganLogs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        LaporanId = c.String(maxLength: 128),
                        Author = c.String(),
                        Aktivitas = c.String(),
                        CreatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdatedUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.KeuanganRequests", "DetailId");
            CreateIndex("dbo.KeuanganLogs", "LaporanId");
            AddForeignKey("dbo.KeuanganRequests", "DetailId", "dbo.KeuanganLaporans", "Id", cascadeDelete: true);
            AddForeignKey("dbo.KeuanganLogs", "LaporanId", "dbo.KeuanganLaporans", "Id");
        }
    }
}
