namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeJenisPerubahanFromMclearsRequest : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.MclearsRequests", "JenisPerubahan");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MclearsRequests", "JenisPerubahan", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
