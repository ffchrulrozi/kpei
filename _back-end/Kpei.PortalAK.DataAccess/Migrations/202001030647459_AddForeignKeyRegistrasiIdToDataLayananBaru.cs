namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKeyRegistrasiIdToDataLayananBaru : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataLayananBarus", "RegistrasiId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.DataLayananBarus", "RegistrasiId");
            AddForeignKey("dbo.DataLayananBarus", "RegistrasiId", "dbo.DataRegistrasis", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DataLayananBarus", "RegistrasiId", "dbo.DataRegistrasis");
            DropIndex("dbo.DataLayananBarus", new[] { "RegistrasiId" });
            DropColumn("dbo.DataLayananBarus", "RegistrasiId");
        }
    }
}
