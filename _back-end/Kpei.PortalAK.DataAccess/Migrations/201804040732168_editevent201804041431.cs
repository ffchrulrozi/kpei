namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editevent201804041431 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "TglPelaksanaan", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "WaktuMulai", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "WaktuSelesai", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "TglDaftarBuka", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "TglDaftarTutup", c => c.DateTime(nullable: false));
            DropColumn("dbo.Events", "WaktuPelaksanaan");
            DropColumn("dbo.Events", "WaktuPenutupan");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Events", "WaktuPenutupan", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "WaktuPelaksanaan", c => c.DateTime(nullable: false));
            DropColumn("dbo.Events", "TglDaftarTutup");
            DropColumn("dbo.Events", "TglDaftarBuka");
            DropColumn("dbo.Events", "WaktuSelesai");
            DropColumn("dbo.Events", "WaktuMulai");
            DropColumn("dbo.Events", "TglPelaksanaan");
        }
    }
}
