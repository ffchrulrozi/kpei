namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editisTrashed201804121758 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Surveis", "isTrashed", c => c.Boolean(nullable: false));
            AddColumn("dbo.SurveiDivisis", "isTrashed", c => c.Boolean(nullable: false));
            AddColumn("dbo.SurveiPerusahaans", "isTrashed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SurveiPerusahaans", "isTrashed");
            DropColumn("dbo.SurveiDivisis", "isTrashed");
            DropColumn("dbo.Surveis", "isTrashed");
        }
    }
}
