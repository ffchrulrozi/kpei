namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addActiveUtcLayananJasa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LayananJasas", "ActiveUtc", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LayananJasas", "ActiveUtc");
        }
    }
}
