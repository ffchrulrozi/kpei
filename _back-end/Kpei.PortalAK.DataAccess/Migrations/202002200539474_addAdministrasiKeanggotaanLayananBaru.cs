namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAdministrasiKeanggotaanLayananBaru : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdministrasiKeanggotaanLayananBarus",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        TipeMemberAK = c.String(maxLength: 128),
                        TipeMemberPartisipan = c.String(maxLength: 256),
                        FieldName = c.String(maxLength: 256),
                        FieldType = c.String(maxLength: 256),
                        FieldValue = c.String(maxLength: 256),
                        AdministrasiKeanggotaanId = c.String(nullable: false, maxLength: 128),
                        LayananBaruId = c.String(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdministrasiKeanggotaans", t => t.AdministrasiKeanggotaanId, cascadeDelete: true)
                .Index(t => t.AdministrasiKeanggotaanId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdministrasiKeanggotaanLayananBarus", "AdministrasiKeanggotaanId", "dbo.AdministrasiKeanggotaans");
            DropIndex("dbo.AdministrasiKeanggotaanLayananBarus", new[] { "AdministrasiKeanggotaanId" });
            DropTable("dbo.AdministrasiKeanggotaanLayananBarus");
        }
    }
}
