namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editevent201804041417 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "WaktuPenutupan", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "StatusEvent", c => c.String());
            DropColumn("dbo.Events", "Penutupan");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Events", "Penutupan", c => c.Int(nullable: false));
            DropColumn("dbo.Events", "StatusEvent");
            DropColumn("dbo.Events", "WaktuPenutupan");
        }
    }
}
