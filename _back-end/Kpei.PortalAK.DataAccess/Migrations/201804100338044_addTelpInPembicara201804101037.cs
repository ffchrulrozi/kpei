namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTelpInPembicara201804101037 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventPembicaras", "Telp", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventPembicaras", "Telp");
        }
    }
}
