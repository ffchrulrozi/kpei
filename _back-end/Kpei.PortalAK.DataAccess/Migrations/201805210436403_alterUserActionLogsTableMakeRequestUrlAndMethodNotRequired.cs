namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class alterUserActionLogsTableMakeRequestUrlAndMethodNotRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserActionLogs", "RequestMethod", c => c.String(maxLength: 10));
            AlterColumn("dbo.UserActionLogs", "RequestUrl", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserActionLogs", "RequestUrl", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.UserActionLogs", "RequestMethod", c => c.String(nullable: false, maxLength: 10));
        }
    }
}
