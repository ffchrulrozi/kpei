namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeRequiredInPPEFromDbModels : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DataRegistrasis", "PPE", c => c.String(maxLength: 128));
            AlterColumn("dbo.DataJenisUsahas", "PPE", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataJenisUsahas", "PPE", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.DataRegistrasis", "PPE", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
