namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMissingJenisPerjanjianPme : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataPMEs", "NoLender", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPMEs", "JenisPerjanjianLender", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPMEs", "NoBorrower", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPMEs", "JenisPerjanjianBorrower", c => c.String(maxLength: 128));
            DropColumn("dbo.DataPMEs", "LenderPerjanjian");
            DropColumn("dbo.DataPMEs", "BorrowerPerjanjian");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DataPMEs", "BorrowerPerjanjian", c => c.String(maxLength: 128));
            AddColumn("dbo.DataPMEs", "LenderPerjanjian", c => c.String(maxLength: 128));
            DropColumn("dbo.DataPMEs", "JenisPerjanjianBorrower");
            DropColumn("dbo.DataPMEs", "NoBorrower");
            DropColumn("dbo.DataPMEs", "JenisPerjanjianLender");
            DropColumn("dbo.DataPMEs", "NoLender");
        }
    }
}
