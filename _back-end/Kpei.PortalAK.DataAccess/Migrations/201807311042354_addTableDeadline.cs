namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTableDeadline : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Deadlines",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        KeuanganAwalPeriode = c.DateTime(),
                        KeuanganAkhirPeriode = c.DateTime(),
                        Reminder1 = c.Int(nullable: false),
                        Reminder2 = c.Int(nullable: false),
                        Reminder3 = c.Int(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Deadlines");
        }
    }
}
