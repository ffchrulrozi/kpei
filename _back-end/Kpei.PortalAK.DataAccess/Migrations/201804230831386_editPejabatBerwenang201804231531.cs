namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editPejabatBerwenang201804231531 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataRegistrasis", "Notaris", c => c.String(nullable: false));
            AddColumn("dbo.DataPejabatBerwenangs", "Email", c => c.String(nullable: false));
            AddColumn("dbo.DataPejabatBerwenangs", "Telp", c => c.String(nullable: false));
            AddColumn("dbo.DataPejabatBerwenangs", "HP", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataPejabatBerwenangs", "HP");
            DropColumn("dbo.DataPejabatBerwenangs", "Telp");
            DropColumn("dbo.DataPejabatBerwenangs", "Email");
            DropColumn("dbo.DataRegistrasis", "Notaris");
        }
    }
}
