namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAdministrasiKeanggotaanIdToStatusKpei : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataStatusKPEIs", "AdministrasiKeanggotaanId", c => c.String(maxLength: 128));
            CreateIndex("dbo.DataStatusKPEIs", "AdministrasiKeanggotaanId");
            AddForeignKey("dbo.DataStatusKPEIs", "AdministrasiKeanggotaanId", "dbo.AdministrasiKeanggotaans", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DataStatusKPEIs", "AdministrasiKeanggotaanId", "dbo.AdministrasiKeanggotaans");
            DropIndex("dbo.DataStatusKPEIs", new[] { "AdministrasiKeanggotaanId" });
            DropColumn("dbo.DataStatusKPEIs", "AdministrasiKeanggotaanId");
        }
    }
}
