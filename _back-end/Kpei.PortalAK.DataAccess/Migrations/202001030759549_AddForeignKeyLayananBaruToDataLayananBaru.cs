namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKeyLayananBaruToDataLayananBaru : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataLayananBarus", "LayananBaruId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.DataLayananBarus", "LayananBaruId");
            AddForeignKey("dbo.DataLayananBarus", "LayananBaruId", "dbo.LayananBarus", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DataLayananBarus", "LayananBaruId", "dbo.LayananBarus");
            DropIndex("dbo.DataLayananBarus", new[] { "LayananBaruId" });
            DropColumn("dbo.DataLayananBarus", "LayananBaruId");
        }
    }
}
