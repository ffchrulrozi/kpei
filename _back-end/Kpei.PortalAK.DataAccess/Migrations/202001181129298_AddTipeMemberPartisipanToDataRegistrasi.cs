namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTipeMemberPartisipanToDataRegistrasi : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.DataTipeMemberPartisipanRegistrasis", "RegistrasiId", "dbo.DataRegistrasis");
            //DropIndex("dbo.DataTipeMemberPartisipanRegistrasis", new[] { "RegistrasiId" });
            AddColumn("dbo.DataRegistrasis", "TipeMemberPartisipan", c => c.String());
            //DropTable("dbo.DataTipeMemberPartisipanRegistrasis");
        }
        
        public override void Down()
        {
            //CreateTable(
            //    "dbo.DataTipeMemberPartisipanRegistrasis",
            //    c => new
            //        {
            //            Id = c.String(nullable: false, maxLength: 128),
            //            RegistrasiId = c.String(nullable: false, maxLength: 128),
            //            Nama = c.String(nullable: false, maxLength: 128),
            //            Value = c.Boolean(nullable: false),
            //            CreatedUtc = c.DateTime(nullable: false),
            //            UpdatedUtc = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.DataRegistrasis", "TipeMemberPartisipan");
            //CreateIndex("dbo.DataTipeMemberPartisipanRegistrasis", "RegistrasiId");
            //AddForeignKey("dbo.DataTipeMemberPartisipanRegistrasis", "RegistrasiId", "dbo.DataRegistrasis", "Id", cascadeDelete: true);
        }
    }
}
