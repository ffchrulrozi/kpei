namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class simplifyDataAkteAndRelations : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DataDaftarDireksis", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataDireksis", "DataDaftarId", "dbo.DataDaftarDireksis");
            DropForeignKey("dbo.DataDaftarKomisaris", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataKomisaris", "DataDaftarId", "dbo.DataDaftarKomisaris");
            DropForeignKey("dbo.DataDaftarPemegangSahams", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataPemegangSahams", "DataDaftarId", "dbo.DataDaftarPemegangSahams");
            DropForeignKey("dbo.DataModals", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataNamas", "AkteId", "dbo.DataAktes");
            DropIndex("dbo.DataDaftarDireksis", new[] { "AkteId" });
            DropIndex("dbo.DataDireksis", new[] { "DataDaftarId" });
            DropIndex("dbo.DataDaftarKomisaris", new[] { "AkteId" });
            DropIndex("dbo.DataKomisaris", new[] { "DataDaftarId" });
            DropIndex("dbo.DataDaftarPemegangSahams", new[] { "AkteId" });
            DropIndex("dbo.DataPemegangSahams", new[] { "DataDaftarId" });
            DropIndex("dbo.DataModals", new[] { "AkteId" });
            DropIndex("dbo.DataNamas", new[] { "AkteId" });
            AddColumn("dbo.DataAktes", "NamaPerusahaan", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataAktes", "ModalDisetorRupiah", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DataAktes", "ModalDasarRupiah", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DataDireksis", "AkteId", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataKomisaris", "AkteId", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataPemegangSahams", "AkteId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.DataDireksis", "AkteId");
            CreateIndex("dbo.DataKomisaris", "AkteId");
            CreateIndex("dbo.DataPemegangSahams", "AkteId");
            AddForeignKey("dbo.DataDireksis", "AkteId", "dbo.DataAktes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DataKomisaris", "AkteId", "dbo.DataAktes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DataPemegangSahams", "AkteId", "dbo.DataAktes", "Id", cascadeDelete: true);
            DropColumn("dbo.DataDireksis", "DataDaftarId");
            DropColumn("dbo.DataKomisaris", "DataDaftarId");
            DropColumn("dbo.DataPemegangSahams", "DataDaftarId");
            DropTable("dbo.DataDaftarDireksis");
            DropTable("dbo.DataDaftarKomisaris");
            DropTable("dbo.DataDaftarPemegangSahams");
            DropTable("dbo.DataModals");
            DropTable("dbo.DataNamas");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DataNamas",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Nama = c.String(nullable: false, maxLength: 128),
                        AkteId = c.String(nullable: false, maxLength: 128),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataModals",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ModalDisetorRupiah = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ModalDasarRupiah = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AkteId = c.String(nullable: false, maxLength: 128),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataDaftarPemegangSahams",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        AkteId = c.String(nullable: false, maxLength: 128),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataDaftarKomisaris",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        AkteId = c.String(nullable: false, maxLength: 128),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataDaftarDireksis",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        AkteId = c.String(nullable: false, maxLength: 128),
                        KodeAk = c.String(nullable: false, maxLength: 32),
                        Status = c.String(nullable: false, maxLength: 128),
                        ActiveUtc = c.DateTime(),
                        RejectReason = c.String(maxLength: 1024),
                        Approver1UserId = c.String(maxLength: 128),
                        Approver2UserId = c.String(maxLength: 128),
                        CreatedUtc = c.DateTime(nullable: false),
                        UpdatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.DataPemegangSahams", "DataDaftarId", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataKomisaris", "DataDaftarId", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.DataDireksis", "DataDaftarId", c => c.String(nullable: false, maxLength: 128));
            DropForeignKey("dbo.DataPemegangSahams", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataKomisaris", "AkteId", "dbo.DataAktes");
            DropForeignKey("dbo.DataDireksis", "AkteId", "dbo.DataAktes");
            DropIndex("dbo.DataPemegangSahams", new[] { "AkteId" });
            DropIndex("dbo.DataKomisaris", new[] { "AkteId" });
            DropIndex("dbo.DataDireksis", new[] { "AkteId" });
            DropColumn("dbo.DataPemegangSahams", "AkteId");
            DropColumn("dbo.DataKomisaris", "AkteId");
            DropColumn("dbo.DataDireksis", "AkteId");
            DropColumn("dbo.DataAktes", "ModalDasarRupiah");
            DropColumn("dbo.DataAktes", "ModalDisetorRupiah");
            DropColumn("dbo.DataAktes", "NamaPerusahaan");
            CreateIndex("dbo.DataNamas", "AkteId");
            CreateIndex("dbo.DataModals", "AkteId");
            CreateIndex("dbo.DataPemegangSahams", "DataDaftarId");
            CreateIndex("dbo.DataDaftarPemegangSahams", "AkteId");
            CreateIndex("dbo.DataKomisaris", "DataDaftarId");
            CreateIndex("dbo.DataDaftarKomisaris", "AkteId");
            CreateIndex("dbo.DataDireksis", "DataDaftarId");
            CreateIndex("dbo.DataDaftarDireksis", "AkteId");
            AddForeignKey("dbo.DataNamas", "AkteId", "dbo.DataAktes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DataModals", "AkteId", "dbo.DataAktes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DataPemegangSahams", "DataDaftarId", "dbo.DataDaftarPemegangSahams", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DataDaftarPemegangSahams", "AkteId", "dbo.DataAktes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DataKomisaris", "DataDaftarId", "dbo.DataDaftarKomisaris", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DataDaftarKomisaris", "AkteId", "dbo.DataAktes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DataDireksis", "DataDaftarId", "dbo.DataDaftarDireksis", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DataDaftarDireksis", "AkteId", "dbo.DataAktes", "Id", cascadeDelete: true);
        }
    }
}
