namespace Kpei.PortalAK.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeRequiredLayananJasa : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.LayananJasas", "KategoriMember", c => c.String(maxLength: 128));
            AlterColumn("dbo.LayananJasaTradingMembers", "Nama", c => c.String(maxLength: 128));
            AlterColumn("dbo.LayananJasaTradingMembers", "NoPerjanjian", c => c.String(maxLength: 128));
            AlterColumn("dbo.LayananJasaTradingMembers", "PerjanjianFileUrl", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.LayananJasaTradingMembers", "PerjanjianFileUrl", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.LayananJasaTradingMembers", "NoPerjanjian", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.LayananJasaTradingMembers", "Nama", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.LayananJasas", "KategoriMember", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
