﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataContactPersonItem : BaseAknpPerson {
        [Required]
        public string ParentDataId { get; set; }

        [ForeignKey(nameof(ParentDataId))]
        public virtual DataContactPerson ParentData { get; set; }
    }
}