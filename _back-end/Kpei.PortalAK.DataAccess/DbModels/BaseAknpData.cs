﻿using System;
using System.ComponentModel.DataAnnotations;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public abstract class BaseAknpData : BaseData {
        protected BaseAknpData() {
            Status = DataAknpRequestStatus.NEW_STATUS.Name;
        }

        [Required]
        [MaxLength(32)]
        public string KodeAk { get; set; }

        /// <summary>
        ///     Gets or sets the status.<br />
        ///     See <see cref="DataAknpRequestStatus" />.<see cref="DataAknpRequestStatus.Name" />
        /// </summary>
        /// <value>
        ///     The status.
        /// </value>
        [Required]
        [MaxLength(128)]
        public string Status { get; set; }

        public DateTime? ActiveUtc { get; set; }

        [MaxLength(1024)]
        public string RejectReason { get; set; }

        [MaxLength(128)]
        public string Approver1UserId { get; set; }

        [MaxLength(128)]
        public string Approver2UserId { get; set; }
        public bool Draft { get; set; }

        [MaxLength(128)]
        public string UserDraft { get; set; }
    }
}