﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class SynchronizeTable {
        public SynchronizeTable() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = DateTime.UtcNow;
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }
        
        [Required]
        [MaxLength(128)]
        public string RelatedTable { get; set; }

        [Required]
        [MaxLength(128)]
        public string Flow { get; set; }

        [Required]
        [MaxLength(128)]
        public string Action { get; set; }

        [Required]
        public DateTime SyncUtc { get; set; }

        [Required]
        public DateTime CreatedUtc { get; set; }

    }
}
