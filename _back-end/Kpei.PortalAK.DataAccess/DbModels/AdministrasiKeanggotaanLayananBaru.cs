﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels
{
    public class AdministrasiKeanggotaanLayananBaru
    {
        public AdministrasiKeanggotaanLayananBaru()
        {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }

        // DataTipeMember
        [Display(Name = "Tipe Member (AK)", GroupName = "Member", Order = 6)]
        [AknpDisplay("_tipeMemberAK", false, false)]
        [MaxLength(128)]
        public string TipeMemberAK { get; set; }

        [MaxLength(256)]
        public string TipeMemberPartisipan { get; set; }

        [MaxLength(256)]
        public string FieldName { get; set; }
        [MaxLength(256)]
        public string FieldType { get; set; }

        [MaxLength(256)]
        public string FieldValue { get; set; }

        [Required]
        public string AdministrasiKeanggotaanId { get; set; }

        [ForeignKey(nameof(AdministrasiKeanggotaanId))]
        public virtual AdministrasiKeanggotaan AdministrasiKeanggotaan { get; set; }

        [Required]
        public string LayananBaruId { get; set; }

        [Required]
        public DateTime CreatedUtc { get; set; }

        [Required]
        public DateTime UpdatedUtc { get; set; }
    }
}
