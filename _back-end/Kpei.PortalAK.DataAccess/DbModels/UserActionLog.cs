﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class UserActionLog {
        public UserActionLog() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }
        
        [Required]
        [MaxLength(128)]
        public string ModuleId { get; set; }

        [MaxLength(128)]
        public string UserId { get; set; }

        [MaxLength(128)]
        public string UserName { get; set; }

        [MaxLength(10)]
        public string UserAknpCode { get; set; }

        [Required]
        [MaxLength(512)]
        public string Message { get; set; }

        [MaxLength(2048)]
        public string DataJson { get; set; }

        [MaxLength(128)]
        public string RelatedEntityName { get; set; }

        [MaxLength(128)]
        public string RelatedEntityId { get; set; }

        [MaxLength(10)]
        public string RequestMethod { get; set; }
        
        [MaxLength(256)]
        public string RequestUrl { get; set; }

        [Required]
        public DateTime CreatedUtc { get; set; }

        [Required]
        public DateTime UpdatedUtc { get; set; }
    }
}