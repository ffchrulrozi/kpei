﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class Deadline {
        public Deadline() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }

        public DateTime? KeuanganAwalPeriodeUtc { get; set; }
        
        public DateTime? KeuanganAkhirPeriodeUtc { get; set; }

        public int Reminder1 { get; set; }

        [Required]
        public DateTime CreatedUtc { get; set; }

        [Required]
        public DateTime UpdatedUtc { get; set; }
    }
}
