﻿namespace Kpei.PortalAK.DataAccess.DbModels {
    public interface IAknpPemegangSaham {
        string Nama { get; set; }
        decimal LembarSaham { get; set; }
        decimal NominalSahamRupiah { get; set; }
        string WargaNegara { get; set; }
    }
}