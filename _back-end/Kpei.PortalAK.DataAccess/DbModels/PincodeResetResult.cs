﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class PincodeResetResult {

        private ICollection<PincodeResetEmail> _emailTargets;

        public PincodeResetResult() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }

        [Required]
        [MaxLength(256)]
        public string NewUserName { get; set; }

        [Required]
        [MaxLength(256)]
        public string NewPassword { get; set; }

        [ForeignKey(nameof(Request))]
        public string PincodeRequestId { get; set; }

        public virtual PincodeRequest Request { get; set; }

        [InverseProperty(nameof(PincodeResetEmail.ResetResult))]
        public virtual ICollection<PincodeResetEmail> EmailTargets {
            get { return _emailTargets ?? (_emailTargets = new List<PincodeResetEmail>()); }
            set { _emailTargets = value; }
        }

        [Required]
        public DateTime CreatedUtc { get; set; }

        [Required]
        public DateTime UpdatedUtc { get; set; }
    }

    public class PincodeResetEmail {
        public PincodeResetEmail() {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }

        [ForeignKey(nameof(ResetResult))]
        public string ResetResultId { get; set; }

        public virtual PincodeResetResult ResetResult { get; set; }

        [Required]
        [MaxLength(128)]
        public string EmailAddress { get; set; }
    }
}