﻿using System.ComponentModel.DataAnnotations;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataAlamat : BaseAknpDataWithSuratKeterangan {
        [Display(GroupName = "Alamat")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string Gedung { get; set; }

        [Display(GroupName = "Alamat")]
        [AknpDisplay(null, true, true)]
        [MaxLength(256)]
        public string Jalan { get; set; }

        [Display(GroupName = "Alamat")]
        [AknpDisplay(null, true, true)]
        [MaxLength(128)]
        public string Kota { get; set; }

        [Display(Name = "Kode Pos", GroupName = "Alamat")]
        [AknpDisplay(null, true, true)]
        [MaxLength(32)]
        public string KodePos { get; set; }

        [Display(GroupName = "Alamat")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string Telp { get; set; }

        [Display(GroupName = "Alamat")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string Fax { get; set; }

        [Display(GroupName = "Alamat")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string Website { get; set; }

        [Display(GroupName = "Alamat")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string Email { get; set; }
    }
}