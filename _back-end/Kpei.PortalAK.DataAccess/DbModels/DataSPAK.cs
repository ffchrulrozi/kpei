﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataSPAK : BaseAknpDataWithSuratKeterangan {
        [Display(Name = "No. SPAK", GroupName = "SPAK")]
        [AknpDisplay(null, true, true)]
        [MaxLength(128)]
        public string NoSPAK { get; set; }

        [Display(Name = "Tanggal SPAK", GroupName = "SPAK")]
        [AknpDisplay(null, true, false)]
        public DateTime? TanggalSPAKUtc { get; set; }
        
        [Display(Name = "File SPAK", GroupName = "Files")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string SPAKFileUrl { get; set; }
    }
}
