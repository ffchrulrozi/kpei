﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataContactPerson : BaseAknpDataWithSuratKeterangan {
        [Display(Name = "Daftar Contact Person", GroupName = "Contact Person", Order = 1)]
        [AknpDisplay(null, true, true)]
        public virtual ICollection<DataContactPersonItem> Data { get; set; }
    }
}