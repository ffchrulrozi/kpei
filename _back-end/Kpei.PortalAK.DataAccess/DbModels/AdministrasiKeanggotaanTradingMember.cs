﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels
{
    public class AdministrasiKeanggotaanTradingMember : BaseData
    {
        [MaxLength(128)]
        public string Nama { get; set; }

        [MaxLength(128)]
        public string NoPerjanjian { get; set; }

        public DateTime? TanggalPerjanjian { get; set; }

        [MaxLength(256)]
        public string PerjanjianFileUrl { get; set; }

        [Required]
        public string AdministrasiKeanggotaanId { get; set; }

        [ForeignKey(nameof(AdministrasiKeanggotaanId))]
        public virtual AdministrasiKeanggotaan AdministrasiKeanggotaan { get; set; }


    }
}
