﻿using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public interface IAknpPerson {
        string Nama { get; set; }
        string Jabatan { get; set; }
        string Email { get; set; }
        string Telp { get; set; }
        string HP { get; set; }
    }

    public abstract class BaseAknpPerson : BaseData, IAknpPerson {
        private string _nama;

        [MaxLength(128)]
        public string Nama {
            get { return _nama?.Trim(); }
            set { _nama = value; }
        }

        [MaxLength(128)]
        public string Jabatan { get; set; }

        [MaxLength(128)]
        public string Email { get; set; }

        [MaxLength(128)]
        public string Telp { get; set; }

        [MaxLength(128)]
        public string HP { get; set; }
    }
}