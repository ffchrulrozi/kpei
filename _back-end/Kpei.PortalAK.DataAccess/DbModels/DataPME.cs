﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataPME : BaseAknpDataWithSuratKeterangan {
        [Display(Name = "Jenis Keanggotaan", GroupName = "Data PME")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string JenisKeanggotaan { get; set; }

        [Display(Name = "Nomor Lender", GroupName = "Lender")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string NoLender { get; set; }

        [Display(Name = "Jenis Perjanjian Lender", GroupName = "Lender")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string JenisPerjanjianLender { get; set; }

        [Display(Name = "Tanggal Awal Lender", GroupName = "Lender")]
        [AknpDisplay(null, true, false)]
        public DateTime? LenderTglAwalUtc { get; set; }

        [Display(Name = "Tanggal Akhir Lender", GroupName = "Lender")]
        [AknpDisplay(null, true, false)]
        public DateTime? LenderTglAkhirUtc { get; set; }

        [Display(Name = "Status Lender", GroupName = "Lender")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string LenderStatus { get; set; }

        [Display(Name = "Nomor Borrower", GroupName = "Borrower")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string NoBorrower { get; set; }

        [Display(Name = "Jenis Perjanjian Borrower", GroupName = "Borrower")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string JenisPerjanjianBorrower { get; set; }

        [Display(Name = "Tanggal Awal Borrower", GroupName = "Borrower")]
        [AknpDisplay(null, true, false)]
        public DateTime? BorrowerTglAwalUtc { get; set; }

        [Display(Name = "Tanggal Akhir Borrower", GroupName = "Borrower")]
        [AknpDisplay(null, true, false)]
        public DateTime? BorrowerTglAkhirUtc { get; set; }

        [Display(Name = "Status Borrower", GroupName = "Borrower")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string BorrowerStatus { get; set; }

        [Display(Name = "File AK Nasabah", GroupName = "Data PME")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string AKNasabahFileUrl { get; set; }

        [Display(Name = "File Surat Kuasa Sub Rekening", GroupName = "Data PME")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string SuratKuasaSubRekFileUrl { get; set; }

        [Display(Name = "File Aplikasi Lender", GroupName = "Data PME")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string AplikasiLenderFileUrl { get; set; }

        [Display(Name = "File Aplikasi Borrower", GroupName = "Data PME")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string AplikasiBorrowerFileUrl { get; set; }

        [Display(Name = "File KPEI As Lender", GroupName = "Data PME")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string KPEIAsLenderFileUrl { get; set; }

        [Display(Name = "File KPEI As Borrower", GroupName = "Data PME")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string KPEIAsBorrowerFileUrl { get; set; }

        [Display(Name = "File Surat Kuasa Baru", GroupName = "Data PME")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string SuratKuasaBaruFileUrl { get; set; }
    }
}