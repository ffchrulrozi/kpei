﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataJenisUsaha : BaseAknpDataWithSuratKeterangan{
        [Display(GroupName = "Jenis Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string PPE { get; set; }

        [Display(GroupName = "Jenis Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string PEE { get; set; }

        [Display(GroupName = "Jenis Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string MI { get; set; }

        [Display(GroupName = "Jenis Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string BK { get; set; }

        [Display(GroupName = "Jenis Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string BU { get; set; }
    }
}