﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataPejabatBerwenangItem : BaseAknpDirectorPerson {
        [Required]
        public string ParentDataId { get; set; }

        [ForeignKey(nameof(ParentDataId))]
        public virtual DataPejabatBerwenang ParentData { get; set; }
    }
}