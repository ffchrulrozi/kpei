﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class MclearsPeserta {
        public MclearsPeserta() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }
        [Key]
        [MaxLength(128)]
        public string Id { get; set; }
        [Required]
        [MaxLength(32)]
        public string KodeAK { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Nama")]
        [MaxLength(128)]
        public string Nama { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Jabatan")]
        [MaxLength(128)]
        public string Jabatan { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "E-mail")]
        [MaxLength(128)]
        public string Email { get; set; }
        [MaxLength(128)]
        public string HP { get; set; }
        [Display(Name = "Alert - Pending Cash")]
        public Boolean? AlertPC { get; set; }
        [Display(Name = "Alert - Pending Saham")]
        public Boolean? AlertPS { get; set; }
        [Display(Name = "Alert - Hak Terima Saham di 004")]
        public Boolean? AlertHTS { get; set; }
        [Display(Name = "Alert - Hak Terima IDR di 004")]
        public Boolean? AlertHTR { get; set; }
        [Display(Name = "Alert - Bayar Alternate Cash Settlement")]
        public Boolean? AlertBACS { get; set; }
        [Display(Name = "Alert - Terima Alternate Cash Settlement")]
        public Boolean? AlertACS { get; set; }
        [Display(Name = "Alert - Trading Limit Pagi")]
        public Boolean? AlertTLP { get; set; }
        [Display(Name = "Alert - Trading Limit Siang")]
        public Boolean? AlertTLS { get; set; }
        [Display(Name = "Alert - Trading Limit Proyeksi Pukul 17.15")]
        public Boolean? AlertTL5 { get; set; }
        [Display(Name = "Alert - MKBD Alert")]
        public Boolean? AlertMKBDA { get; set; }
        [Display(Name = "Alert - Failed COLW Alert")]
        public Boolean? AlertFCOLWA { get; set; }
        [Display(Name = "Alert - Collateral Limit Alert")]
        public Boolean? AlertCLA { get; set; }
        [Display(Name = "Alert - Offline Collateral Alert")]
        public Boolean? AlertOCA { get; set; }
        [Display(Name = "Alert - Minimum Cash Collateral Alert")]
        public Boolean? AlertMCCA { get; set; }
        [Display(Name = "Alert - Derivative Cash Delivery Alert")]
        public Boolean? AlertDCDA { get; set; }
        [Display(Name = "Alert - Derivative Cash Receive Alert")]
        public Boolean? AlertDCRA { get; set; }
        [Display(Name = "Alert - Margin Call Alert")]
        public Boolean? AlertMCA { get; set; }
        [Display(Name = "Alert - Contract Position Alert")]
        public Boolean? AlertCPA { get; set; }
        [Display(Name = "Alert - < 1 Day Contract Due Alert")]
        public Boolean? AlertL1 { get; set; }
        [Display(Name = "Alert - < 3 Day Contract Due Alert")]
        public Boolean? AlertL3 { get; set; }
        [Display(Name = "Alert - < 7 Day Contract Due Alert")]
        public Boolean? AlertL7 { get; set; }
        [Display(Name = "Alert - HPH & HPF Alert")]
        public Boolean? AlertHPHFA { get; set; }
        [Display(Name = "Alert - Broadcast News From KPEI")]
        public Boolean? AlertBroadcast { get; set; }
        [Display(Name = "On Request - Pending Cash")]
        public Boolean? OnRequestPC { get; set; }
        [Display(Name = "On Request - Pending Saham")]
        public Boolean? OnRequestPS { get; set; }
        [Display(Name = "On Request - Hak Terima Saham di Rek 004")]
        public Boolean? OnRequestHTS { get; set; }
        [Display(Name = "On Request - Hak Terima IDR di Rek 004")]
        public Boolean? OnRequestHTR { get; set; }
        [Display(Name = "On Request - Purchase Power Request")]
        public Boolean? OnRequestPPR { get; set; }
        [Display(Name = "On Request - MKBD Request")]
        public Boolean? OnRequestMKBDR { get; set; }
        [Display(Name = "On Request - Offline Collateral Request")]
        public Boolean? OnRequestOCR { get; set; }
        [Display(Name = "On Request - Haircut")]
        public Boolean? OnRequestHaircut { get; set; }
        [Display(Name = "On Request - Derivative Cash Delivery Request")]
        public Boolean? OnRequestDCDR { get; set; }
        [Display(Name = "On Request - Derivative Cash Receive Request")]
        public Boolean? OnRequestDCRR { get; set; }
        [Display(Name = "On Request - Margin Call Request")]
        public Boolean? OnRequestMCR { get; set; }
        [Display(Name = "On Request - Contract Position Request")]
        public Boolean? OnRequestCPR { get; set; }
        [Display(Name = "On Request - HPH & HPF Request")]
        public Boolean? OnRequestHPHFR { get; set; }

        public string RequestId { get; set; }

        [Required]
        public DateTime CreatedUtc { get; set; }
        [Required]
        public DateTime UpdatedUtc { get; set; }

    }
}