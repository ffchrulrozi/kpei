﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataChangeRequest {
        public DataChangeRequest() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }

        [Key]
        [Required]
        [MaxLength(128)]
        public string Id { get; set; }

        [Required]
        public DateTime CreatedUtc { get; set; }

        [Required]
        public DateTime UpdatedUtc { get; set; }

        /// <summary>
        ///     Gets or sets the status.<br />
        ///     Will be copied from related <see cref="BaseAknpData" />.<br />
        ///     See <see cref="DataAknpRequestStatus" />.<see cref="DataAknpRequestStatus.Name" />
        /// </summary>
        /// <value>
        ///     The status.
        /// </value>
        [Required]
        [MaxLength(128)]
        public string Status { get; set; }

        [Required]
        [MaxLength(32)]
        public string KodeAk { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("ix_drv_slug_id", 1, IsUnique = true)]
        public string DataDriverSlug { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("ix_drv_slug_id", 2, IsUnique = true)]
        public string DataId { get; set; }
    }
}