﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataPejabatBerwenang : BaseAknpDataWithSuratKeterangan {
        [Display(Name = "Daftar Pejabat Berwenang", GroupName = "Pejabat Berwenang", Order = 1)]
        [AknpDisplay(null, true, true)]
        public virtual ICollection<DataPejabatBerwenangItem> Data { get; set; }
    }
}