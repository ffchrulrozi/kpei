﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public abstract class BaseAknpDataWithAkte : BaseData {
        [Required]
        public string AkteId { get; set; }

        [ForeignKey(nameof(AkteId))]
        public virtual DataAkte Akte { get; set; }
    }
}