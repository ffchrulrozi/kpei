﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class PincodeSetting {

        private ICollection<PincodeEclearsAdminTable> _pincodeEclearsAdmins;
        private ICollection<PincodeTypeInfoTable> _pincodeTypeInfos;

        public PincodeSetting() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }

        public virtual ICollection<PincodeEclearsAdminTable> PincodeEclearsAdmins {
            get { return _pincodeEclearsAdmins ?? (_pincodeEclearsAdmins = new List<PincodeEclearsAdminTable>()); }
            set { _pincodeEclearsAdmins = value; }
        }

        public virtual ICollection<PincodeTypeInfoTable> PincodeTypeInfos {
            get { return _pincodeTypeInfos ?? (_pincodeTypeInfos = new List<PincodeTypeInfoTable>()); }
            set { _pincodeTypeInfos = value; }
        }

        [Required]
        public DateTime CreatedUtc { get; set; }

        [Required]
        public DateTime UpdatedUtc { get; set; }
    }

    public class PincodeEclearsAdminTable {
        public PincodeEclearsAdminTable() {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }

        public virtual PincodeSetting ParentSetting { get; set; }

        [ForeignKey(nameof(ParentSetting))]
        [Required]
        public string PincodeSettingId { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        [MaxLength(128)]
        public string Email { get; set; }
    }

    public class PincodeTypeInfoTable {
        public PincodeTypeInfoTable() {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }

        public virtual PincodeSetting ParentSetting { get; set; }

        [ForeignKey(nameof(ParentSetting))]
        [Required]
        public string PincodeSettingId { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        [MaxLength(128)]
        public string Url { get; set; }
    }
}