﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public abstract class BaseAknpDirectorPersonWithRegistrasi : BaseData, IAknpDirectorPerson {
        [MaxLength(256)]
        public string TtdFileUrl { get; set; }

        [MaxLength(256)]
        public string KtpFileUrl { get; set; }

        private string _nama;

        [MaxLength(128)]
        public string Nama {
            get { return _nama?.Trim(); }
            set { _nama = value; }
        }

        [MaxLength(128)]
        public string Jabatan { get; set; }
        
        [MaxLength(128)]
        public string Email { get; set; }
        
        [MaxLength(128)]
        public string Telp { get; set; }

        [MaxLength(128)]
        public string HP { get; set; }

        [Required]
        public string RegistrasiId { get; set; }

        [ForeignKey(nameof(RegistrasiId))]
        public virtual DataRegistrasi Registrasi { get; set; }
    }
}