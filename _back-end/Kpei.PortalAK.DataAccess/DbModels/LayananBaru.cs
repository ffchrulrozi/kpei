﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels
{
    public class LayananBaru
    {
        public LayananBaru()
        {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }
        
        // DataTipeMember
        [Display(Name = "Tipe Member (AK)", GroupName = "Member", Order = 6)]
        [AknpDisplay("_tipeMemberAK", false, false)]
        [MaxLength(128)]
        public string TipeMemberAK { get; set; }

        [MaxLength(256)]
        public string TipeMemberPartisipan { get; set; }

        [MaxLength(256)]
        public string FieldName { get; set; }
        [MaxLength(256)]
        public string FieldType { get; set; }
        public bool FieldRequired { get; set; }

        [Required]
        public int Order { get; set; }

        [Required]
        public DateTime CreatedUtc { get; set; }

        [Required]
        public DateTime UpdatedUtc { get; set; }
    }
}
