﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class GlobalSetting {

        private string _guestAknpCode;

        private ICollection<GlobalSettingKkeGroupEmail> _kkeGroupEmails;

        private int _maxSubscribePesertaMclears;

        private int _maxCompanyPersonCount;

        public GlobalSetting() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }

        public virtual ICollection<GlobalSettingKkeGroupEmail> KkeGroupEmails {
            get { return _kkeGroupEmails ?? (_kkeGroupEmails = new List<GlobalSettingKkeGroupEmail>()); }
            set { _kkeGroupEmails = value; }
        }

        [Required]
        [MaxLength(10)]
        public string GuestAknpCode {
            get { return _guestAknpCode ?? (_guestAknpCode = "0000"); }
            set { _guestAknpCode = value; }
        }

        [Required]
        public int MaxCompanyPersonCount {
            get { return _maxCompanyPersonCount > 0 ? _maxCompanyPersonCount : 10; }
            set { _maxCompanyPersonCount = value; }
        }

        [Required]
        public int MaxSubscribePesertaMclears {
            get { return _maxSubscribePesertaMclears > 0 ? _maxSubscribePesertaMclears : 10; }
            set { _maxSubscribePesertaMclears = value; }
        }

        public bool EmailDebugMode { get; set; }

        public string EmailDebugAddress { get; set; }

        [Required]
        public DateTime CreatedUtc { get; set; }

        [Required]
        public DateTime UpdatedUtc { get; set; }
    }

    public class GlobalSettingKkeGroupEmail {
        public GlobalSettingKkeGroupEmail() {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }

        public virtual GlobalSetting ParentSetting { get; set; }

        [ForeignKey(nameof(ParentSetting))]
        public string GlobalSettingId { get; set; }

        [Required]
        [MaxLength(128)]
        public string EmailAddress { get; set; }
    }
}