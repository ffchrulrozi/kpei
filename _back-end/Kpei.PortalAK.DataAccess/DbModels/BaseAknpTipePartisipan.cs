﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public interface IAknpPartisipan {
        string Nama { get; set; }
        bool Value { get; set; }
    }

    public abstract class BaseAknpTipePartisipan : BaseData, IAknpPartisipan {
        private string _name;

        [MaxLength(128)]
        public string Nama {
            get { return _name?.Trim(); }
            set { _name = value; }
        }
        
        public bool Value { get; set; }
    }
}
