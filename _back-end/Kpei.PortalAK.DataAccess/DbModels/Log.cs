﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class Log {
        public DateTime timeUtc { get; set; }
        public string Author { get; set; }
        public string Aktivitas { get; set; }
        public string Url { get; set; }
    }
}
