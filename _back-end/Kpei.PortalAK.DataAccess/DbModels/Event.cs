﻿using Kpei.PortalAK.DataAccess.ModSosialisasi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Repos;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class Event : BaseData{

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Judul")]
        [MaxLength(128)]
        public string Judul { get; set; }

        [MaxLength(1024)]
        public string Highlight { get; set; }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Tempat")]
        [MaxLength(256)]
        public string Tempat { get; set; }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Tanggal Mulai Pelaksanaan")]
        public DateTime? TglPelaksanaanStartUtc { get; set; }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Tanggal Akhir Pelaksanaan")]
        public DateTime? TglPelaksanaanEndUtc { get; set; }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Tanggal Buka Pendaftaran")]
        public DateTime? TglDaftarBukaUtc { get; set; }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Tanggal Tutup Pendaftaran")]
        public DateTime? TglDaftarTutupUtc { get; set; }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Jenis Pelatihan")]
        [MaxLength(128)]
        public string JenisPelatihan { get; set; }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Kapasitas per AK")]
        public int KapasitasPerAK { get; set; }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Kapasitas Total")]
        public int KapasitasTotal { get; set; }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Status Acara")]
        [MaxLength(128)]
        public string StatusEvent { get; set; }

        public string StatusExpiration {
            get {
                if (StatusEvent == EventStatus.DRAFT_STATUS.Name) {
                    return EventExpirationStatus.DRAFT_STATUS.Label;
                }

                var utcNow = DateTime.UtcNow;

                if (utcNow < TglDaftarBukaUtc) {
                    return EventExpirationStatus.AWAIT_OPEN_STATUS.Label;
                } else if (utcNow < TglDaftarTutupUtc) {
                    return EventExpirationStatus.REGIS_OPEN_STATUS.Label;
                } else if (utcNow < TglPelaksanaanStartUtc) {
                    return EventExpirationStatus.REGIS_CLOSED_STATUS.Label;
                } else if (utcNow < TglPelaksanaanEndUtc) {
                    return EventExpirationStatus.EVENT_RUNNING_STATUS.Label;
                } else {
                    return EventExpirationStatus.DONE_STATUS.Label;
                }

            }
        }

        [Display(Name = "File Brosur")]
        [MaxLength(128)]
        public string BrosurFileUrl { get; set; }

        [MaxLength(128)]
        [Display(Name = "File Materi")]
        public string MateriFileUrl { get; set; }
    }
}