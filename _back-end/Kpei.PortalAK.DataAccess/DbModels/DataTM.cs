﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataTM : BaseData {

        [Required]
        [MaxLength(32)]
        public string KodePerusahaan { get; set; }

        [Required]
        [Display(Name = "Nama Perusahaan", GroupName = "Perusahaan")]
        [MaxLength(128)]
        public string NamaPerusahaan { get; set; }

        [Required]
        [Display(Name = "Tipe Perusahaan", GroupName = "Perusahaan")]
        [MaxLength(128)]
        public string TipePerusahaan { get; set; }
    }
}