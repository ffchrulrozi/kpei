﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class Survei : BaseData {

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Nama")]
        [MaxLength(256)]
        public string Nama { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Tanggal Pelaksanaan")]
        public DateTime? TglPelaksanaanUtc { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Jumlah Responden")]
        public int JlhResponden { get; set; }

        [Display(Name = "Nilai Survei")]
        public decimal Nilai { get; set; }
    }
}