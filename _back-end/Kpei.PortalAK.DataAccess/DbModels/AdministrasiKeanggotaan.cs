﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels
{
    public class AdministrasiKeanggotaan : BaseData
    {
        public virtual ICollection<AdministrasiKeanggotaanTradingMember> TradingMembers { get; set; }
        public virtual ICollection<AdministrasiKeanggotaanLayananBaru> LayananBaru { get; set; }

        public virtual ICollection<DataStatusKPEI> StatusKpei { get; set; }

        [Required]
        [MaxLength(32)]
        public string KodeAK { get; set; }

        [MaxLength(128)]
        public string KategoriMember { get; set; }

        [MaxLength(128)]
        public string NoPerjanjianDenganKPEI { get; set; }

        public DateTime? TanggalPerjanjianDenganKPEIUtc { get; set; }

        [MaxLength(256)]
        public string PerjanjianDenganKPEIFileUrl { get; set; }

        [MaxLength(128)]    
        public string NoPerjanjianPortabilitySponsor { get; set; }

        public DateTime? TanggalPerjanjianPortabilitySponsor { get; set; }

        [MaxLength(256)]
        public string PerjanjianPortabilitySponsorFileUrl { get; set; }

        [MaxLength(128)]
        public string NamaABSponsor { get; set; }

        [MaxLength(128)]
        public string NoIjinUsahaPED { get; set; }

        [MaxLength(256)]
        public string DokumenKhususIjinUsahaFileUrl { get; set; }

        public string TipeMemberPartisipan { get; set; }

        [Required]
        [MaxLength(128)]
        public string Status { get; set; }

        public DateTime? ActiveUtc { get; set; }

        [MaxLength(1024)]
        public string RejectReason { get; set; }

        [MaxLength(128)]
        public string Approver1UserId { get; set; }

        [MaxLength(128)]
        public string Approver2UserId { get; set; }
        public bool Draft { get; set; }

        [MaxLength(128)]
        public string UserDraft { get; set; }
    }
}
