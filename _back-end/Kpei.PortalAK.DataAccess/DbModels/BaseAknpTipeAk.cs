﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public interface IAknpAk {
        string ParentMember { get; set; }
        string ChildMember { get; set; }
    }
    public class BaseAknpTipeAk : BaseData, IAknpAk {
        private string _parent;
        private string _child;

        [MaxLength(128)]
        public string ParentMember {
            get { return _parent; }
            set { _parent = value; }
        }

        [MaxLength(128)]
        public string ChildMember {
            get { return _child; }
            set { _child = value; }
        }
    }
}
