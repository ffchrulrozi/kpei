﻿using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public interface IAknpDirectorPerson : IAknpPerson {
        string TtdFileUrl { get; set; }
        string KtpFileUrl { get; set; }
    }

    public abstract class BaseAknpDirectorPerson : BaseData, IAknpDirectorPerson {

        private string _nama;

        [MaxLength(256)]
        public string TtdFileUrl { get; set; }

        [MaxLength(256)]
        public string KtpFileUrl { get; set; }

        [MaxLength(128)]
        public string Nama {
            get { return _nama?.Trim(); }
            set { _nama = value; }
        }

        [MaxLength(128)]
        public string Jabatan { get; set; }

        [MaxLength(128)]
        public string Email { get; set; }

        [MaxLength(128)]
        public string Telp { get; set; }

        [MaxLength(128)]
        public string HP { get; set; }
    }
}