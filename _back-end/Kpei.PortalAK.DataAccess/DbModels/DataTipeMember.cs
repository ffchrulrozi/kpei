﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataTipeMember : BaseAknpData {
        [Display(Name = "Kategori", GroupName = "Tipe Member")]
        [AknpDisplay(null, true, true)]
        [MaxLength(128)]
        public string KategoriMember { get; set; }

        [Display(Name = "Tipe Member (AK)", GroupName = "Tipe Member")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string TipeMemberAK { get; set; }

        [Display(Name = "Tipe Member (Partisipan)", GroupName = "Tipe Member", Order = 1)]
        [AknpDisplay(null, true, true)]
        public virtual ICollection<DataTipeMemberPartisipanItem> Partisipan { get; set; }
        
    }
}
