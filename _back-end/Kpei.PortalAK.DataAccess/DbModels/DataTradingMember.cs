﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels
{
    public class DataTradingMember
    {
        public DataTradingMember()
        {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }

        [MaxLength(128)]
        public string TipeMemberAK { get; set; }

        [Required]
        public string RegistrasiId { get; set; }

        [ForeignKey(nameof(RegistrasiId))]
        public virtual DataRegistrasi Registrasi { get; set; }

        [Required]
        public DateTime CreatedUtc { get; set; }

        [Required]
        public DateTime UpdatedUtc { get; set; }
    }
}
