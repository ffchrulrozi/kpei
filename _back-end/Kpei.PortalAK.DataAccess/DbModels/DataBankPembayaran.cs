﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataBankPembayaran : BaseAknpDataWithSuratKeterangan {
        [Display(Name = "Bank Pembayaran", GroupName = "Bank Pembayaran")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string BankPembayaran { get; set; }
    }
}