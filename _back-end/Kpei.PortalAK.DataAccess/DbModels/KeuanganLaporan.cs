﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Kpei.PortalAK.DataAccess.ModKeuangan.Models;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class KeuanganLaporan : BaseData{

        [Required]
        [MaxLength(32)]
        public string KodeAK { get; set; }

        [Display(Name = "Tahun")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public int Tahun { get; set; }

        /// <summary>
        ///     Gets or sets the periode.<br />
        ///     See <see cref="PeriodeLaporanKeuangan" />
        /// </summary>
        /// <value>
        ///     The periode.
        /// </value>
        [Display(Name = "Periode")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [MaxLength(32)]
        public string Periode { get; set; }

        /// <summary>
        ///     Gets or sets the status request.<br />
        ///     See <see cref="LaporanKeuanganStatus" />
        /// </summary>
        /// <value>
        ///     The status request.
        /// </value>
        [Required]
        [MaxLength(32)]
        public string StatusRequest { get; set; }
        
        [DisplayName("File Laporan Keuangan")]
        [Required]
        [MaxLength(256)]
        public string LaporanKeuanganFileUrl { get; set; }

        [DisplayName("Opini Auditor")]
        [Required]
        [MaxLength(256)]
        public string OpiniAuditor { get; set; }
        
        [Display(Name = "Total Aset Lancar")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal AsetLancar { get; set; }

        [Display(Name = "Total Utang Lancar (Jangka Pendek)")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal UtangLancar { get; set; }

        [Display(Name = "Total Aset (Total Aktiva)")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal TotalAset { get; set; }

        [Display(Name = "Total Utang")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal TotalUtang { get; set; }

        [Display(Name = "Pendapatan Usaha")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal Pendapatan { get; set; }

        [Display(Name = "Laba Bersih setelah Pajak")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal LabaBersih { get; set; }

        [Display(Name = "Total Ekuitas")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        public decimal Ekuitas { get; set; }

        [DisplayName("Current Ratio")]
        public decimal CR => UtangLancar != 0 ? AsetLancar / UtangLancar : 0;

        [DisplayName("Return on Assets")]
        public decimal ROA => TotalAset != 0 ? LabaBersih / TotalAset : 0;

        [DisplayName("Return on Equity")]
        public decimal ROE => Ekuitas != 0 ? LabaBersih / Ekuitas : 0;

        [DisplayName("Net Profit Margin")]
        public decimal NetProfit => Pendapatan != 0 ? LabaBersih / Pendapatan : 0;

        [DisplayName("Debt to Assets Ratio")]
        public decimal DAR => TotalAset != 0 ? TotalUtang / TotalAset : 0;

        [DisplayName("Debt to Equity Ratio")]
        public decimal DER => Ekuitas != 0 ? TotalUtang / Ekuitas : 0;
        
        [MaxLength(128)]
        public string Approver1UserId { get; set; }

        [MaxLength(128)]
        public string Approver2UserId { get; set; }
        
        [MaxLength(1024)]
        public string RejectReason { get; set; }
    }
}