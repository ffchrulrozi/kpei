﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public interface IAknpAkItem {
        string ParentCode { get; set; }
        string ChildCode { get; set; }
    }

    class BaseAknpTipeAkItem : BaseData, IAknpAkItem {

        [MaxLength(128)]
        public string ParentCode { get; set; }

        [MaxLength(128)]
        public string ChildCode { get;set; }
    }
}
