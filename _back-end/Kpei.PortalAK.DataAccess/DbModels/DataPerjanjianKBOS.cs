﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataPerjanjianKBOS : BaseAknpData {
        [Display(Name = "Nomor SPM", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, true)]
        [MaxLength(128)]
        public string NoSPM { get; set; }

        [Display(Name = "Nomor Perjanjian", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, true)]
        [MaxLength(128)]
        public string NoPerjanjian { get; set; }

        [Display(Name = "Jenis Perjanjian", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string JenisPerjanjian { get; set; }

        [Display(Name = "Tanggal SPM", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, false)]
        public DateTime? TglSPMUtc { get; set; }

        [Display(Name = "Status", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string StatusKBOS { get; set; }

        [Display(Name = "Tanggal Aktif", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, false)]
        public DateTime? TanggalAktifUtc { get; set; }

        [Display(Name = "Keterangan", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, false)]
        [MaxLength(512)]
        public string Keterangan { get; set; }
        
        [Display(Name = "Liquidity Provider KB", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string LPKB { get; set; }

        [Display(Name = "Liquidity Provider OS", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string LPOS { get; set; }
    }
}