﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataPerjanjianEBU : BaseAknpData{
        [Display(Name = "Nomor SIM O", GroupName = "Sim O")]
        [AknpDisplay(null, true, true)]
        [MaxLength(128)]
        public string NoSimO { get; set; }

        [Display(Name = "Tanggal SIM O", GroupName = "Sim O")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglSimOUtc { get; set; }

        [Display(Name = "Nomor Perjanjian", GroupName = "Perjanjian")]
        [AknpDisplay(null, true, true)]
        [MaxLength(128)]
        public string NoPerjanjian { get; set; }

        [Display(Name = "Jenis Perjanjian", GroupName = "Perjanjian")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string JenisPerjanjian { get; set; }

        [Display(Name = "Tanggal Awal Perjanjian", GroupName = "Perjanjian")]
        [AknpDisplay(null, true, false)]
        public DateTime? TglSejakUtc { get; set; }

        [Display(Name = "Tanggal Akhir Perjanjian", GroupName = "Perjanjian")]
        [AknpDisplay(null, true, false)]
        public DateTime? TglSampaiUtc { get; set; }

        [Display(Name = "Status Perjanjian", GroupName = "Perjanjian")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string StatusPerjanjian { get; set; }

        [Display(Name = "Tanggal Aktif", GroupName = "Perjanjian")]
        [AknpDisplay(null, true, false)]
        public DateTime? TglAktif { get; set; }

        [Display(Name = "Keterangan", GroupName = "Perjanjian")]
        [AknpDisplay(null, true, false)]
        [MaxLength(512)]
        public string Keterangan { get; set; }
    }
}