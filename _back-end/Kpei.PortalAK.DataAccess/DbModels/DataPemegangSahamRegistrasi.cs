﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataPemegangSahamRegistrasi : BaseAknpDataWithRegistrasi, IAknpPemegangSaham {

        private string _nama;

        [MaxLength(128)]
        public string Nama {
            get { return _nama?.Trim(); }
            set { _nama = value; }
        }

        [Display(Name = "Jml. Lembar Saham")]
        public decimal LembarSaham { get; set; }
        
        public decimal NominalSahamRupiah { get; set; }

        [MaxLength(128)]
        public string WargaNegara { get; set; }
    }
}