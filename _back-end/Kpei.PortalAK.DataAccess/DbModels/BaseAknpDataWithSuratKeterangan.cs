﻿using System.ComponentModel.DataAnnotations;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public abstract class BaseAknpDataWithSuratKeterangan : BaseAknpData {
        // [Display(Name = "Surat Keterangan", Order = -100, GroupName = "Keterangan Perusahaan")]
        // [AknpDisplay(null, false, true)]
        [MaxLength(256)]
        public string SuratKeteranganFileUrl { get; set; }
    }
}