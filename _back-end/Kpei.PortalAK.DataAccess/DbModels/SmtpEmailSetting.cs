﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class SmtpEmailSetting {
        public SmtpEmailSetting() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }

        [Key]
        [MaxLength(128)]
        public string Id { get; set; }

        [MaxLength(128)]
        public string FromEmailAddress { get; set; }

        [MaxLength(128)]
        public string SmtpServerAddress { get; set; }

        public int? SmtpServerPort { get; set; }

        [MaxLength(128)]
        public string SmtpUserName { get; set; }

        [MaxLength(128)]
        public string SmtpUserPassword { get; set; }

        public bool? SmtpEnableSsl { get; set; }

        [Required]
        public DateTime CreatedUtc { get; set; }

        [Required]
        public DateTime UpdatedUtc { get; set; }
    }
}