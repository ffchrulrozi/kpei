﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class SurveiDivisi {
        public SurveiDivisi() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }
        [Key]
        [MaxLength(128)]
        public string Id { get; set; }
        
        [Required]
        [MaxLength(128)]
        public string SurveiId { get; set; }

        [Required]
        [MaxLength(128)]
        public string Kategori { get; set; }
        
        [DisplayName("File Hasil")]
        public string HasilFileUrl { get; set; }
        [DisplayName("File Tindak Lanjut")]
        public string TindakLanjutFileUrl { get; set; }
        
        [Required]
        public DateTime CreatedUtc { get; set; }
        [Required]
        public DateTime UpdatedUtc { get; set; }
    }
}