﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Kpei.PortalAK.DataAccess.ModPincode.Models;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class PincodeRequest : BaseData {

        private ICollection<PincodeResetResult> _resetResults;

        public PincodeRequest() {
            StatusRequest = PincodeRequestStatus.NEW_STATUS.Name;
        }

        [Required]
        [MaxLength(32)]
        public string KodeAK { get; set; }

        [Required]
        [DisplayName("Pincode")]
        public string JenisPincode { get; set; }

        [Display(Name = "Alasan reset")]
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [MaxLength(1024)]
        public string Alasan { get; set; }

        [Display(Name = "Alasan penolakan")]
        public string AlasanDitolak { get; set; }

        [Required]
        [MaxLength(32)]
        public string StatusRequest { get; set; }
        
        [Display(Name = "File Surat Permohonan")]
        [MaxLength(256)]
        public string SuratPermohonanFileUrl { get; set; }

        [InverseProperty(nameof(PincodeResetResult.Request))]
        public virtual ICollection<PincodeResetResult> ResetResults {
            get { return _resetResults ?? (_resetResults = new List<PincodeResetResult>()); }
            set { _resetResults = value; }
        }

        public DateTime? TglAktifUtc { get; set; }
    }
}