﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class EventPeserta {
        public EventPeserta() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }
        [Key]
        [MaxLength(128)]
        public string Id { get; set; }
        
        [Required]
        [MaxLength(128)]
        public string EventId { get; set; }
        
        [Required]
        [MaxLength(32)]
        public string KodeAK { get; set; }
        [Required]
        [MaxLength(128)]
        public string Nama { get; set; }
        [Required]
        [MaxLength(128)]
        public string Jabatan { get; set; }
        [Required]
        [MaxLength(128)]
        public string Email { get; set; }
        [Required]
        [MaxLength(128)]
        public string Telp { get; set; }
        public Boolean Hadir { get; set; }

        [Required]
        public DateTime CreatedUtc { get; set; }
        [Required]
        public DateTime UpdatedUtc { get; set; }
    }
}