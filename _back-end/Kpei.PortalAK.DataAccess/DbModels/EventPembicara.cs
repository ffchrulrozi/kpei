﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class EventPembicara {
        public EventPembicara() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }
        [Key]
        [MaxLength(128)]
        public string Id { get; set; }
        [Required]
        [MaxLength(128)]
        public string EventId { get; set; }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Nama")]
        [MaxLength(128)]
        public string Nama { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Jabatan")]
        [MaxLength(128)]
        public string Jabatan { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "E-mail")]
        [MaxLength(128)]
        public string Email { get; set; }
        [MaxLength(128)]
        public string Telp { get; set; }
        [Required]
        public DateTime CreatedUtc { get; set; }
        [Required]
        public DateTime UpdatedUtc { get; set; }
    }
}