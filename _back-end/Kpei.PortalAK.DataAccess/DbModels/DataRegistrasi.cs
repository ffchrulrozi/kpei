﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataRegistrasi : BaseAknpData {
        #region screen 1
        // DataPerjanjian
        [MaxLength(128)]
        public string NoPerjanjian { get; set; }

        // DataPerjanjian
        public DateTime? TglPerjanjianUtc { get; set; }

        //Data SPAK
        [MaxLength(128)]
        public string NoSPAK { get; set; }

        public DateTime? TanggalSPAKUtc { get; set; }

        //Data SPAB
        [MaxLength(128)]
        public string NoSPAB { get; set; }

        public DateTime? TanggalSPABUtc { get; set; }

        // DataTipeMember
        [Display(Name = "Kategori Member", GroupName = "Member", Order = 5)]
        [AknpDisplay("_kategoriMember", true, false)]
        [MaxLength(128)]
        public string KategoriMember { get; set; }

        // DataTipeMember
        [MaxLength(128)]
        public string TipeMemberAK { get; set; }

        // DataTipeMember
        [Display(Name = "Tipe Member Partisipan", GroupName = "Member", Order = 6)]
        [AknpDisplay("_tipeMemberPartisipan", true, false)]
        public string TipeMemberPartisipan { get; set; }

        //Data Layanan Baru
        [Display(Name = "", GroupName = "Member", Order = 8)]
        [AknpDisplay("_dataLayananBaru", true, false)]
        public virtual ICollection<DataLayananBaru> DataLayananBaru { get; set; }

        [Display(Name = "Trading Member", GroupName = "Member", Order = 8)]
        [AknpDisplay("_tradingMember", true, false)]
        public virtual ICollection<DataTradingMember> DataTradingMember { get; set; }

        [Display(Name = "Nama AB Sponsor", GroupName = "Member", Order = 8)]
        [AknpDisplay("_namaABSponsor", true, false)]
        [MaxLength(128)]
        public string NamaABSponsor { get; set; }

        // DataAkte
        [Display(Name = "Nama Perusahaan", GroupName = "Nama", Order = 8)]
        [AknpDisplay(null, true, true)]
        [MaxLength(128)]
        public string NamaPerusahaan { get; set; }

        // DataAlamat
        [Display(GroupName = "Alamat", Order = 9)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string Gedung { get; set; }

        // DataAlamat
        [Display(GroupName = "Alamat", Order = 10)]
        [AknpDisplay(null, false, true)]
        [MaxLength(256)]
        public string Jalan { get; set; }

        // DataAlamat
        [Display(GroupName = "Alamat", Order = 11)]
        [AknpDisplay(null, false, true)]
        [MaxLength(128)]
        public string Kota { get; set; }

        // DataAlamat
        [Display(Name = "Kode Pos", GroupName = "Alamat", Order = 12)]
        [AknpDisplay(null, false, true)]
        [MaxLength(32)]
        public string KodePos { get; set; }

        // DataAlamat
        [Display(GroupName = "Alamat", Order = 13)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string Telp { get; set; }

        // DataAlamat
        [Display(GroupName = "Alamat", Order = 14)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string Fax { get; set; }

        // DataAlamat
        [Display(GroupName = "Alamat", Order = 15)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        public string Website { get; set; }

        [Display(GroupName = "Alamat", Order = 15)]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string Email { get; set; }

        // DataInformasiLain
        [Display(Name = "Status Perusahaan", GroupName = "Perusahaan", Order = 16)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string StatusPerusahaan { get; set; }

        // DataInformasiLain
        [Display(GroupName = "Perusahaan", Order = 17)]
        [AknpDisplay(null, false, false)]
        //[Required]
        [MaxLength(128)]
        public string NPWP { get; set; }
        
        // DataAkte
        [Display(Name = "Modal Dasar (Rp)", GroupName = "Modal", Order = 18)]
        [AknpDisplay(null, false, false)]
        public decimal ModalDasarRupiah { get; set; }

        // DataAkte
        [Display(Name = "Modal Disetor (Rp)", GroupName = "Modal", Order = 19)]
        [AknpDisplay(null, false, false)]
        public decimal ModalDisetorRupiah { get; set; }
        
        // DataInformasiLain
        [Display(Name = "Anggota Bursa", GroupName = "Perusahaan", Order = 20)]
        [AknpDisplay(null, false, false)]
        //[Required]
        [MaxLength(128)]
        public string AnggotaBursa { get; set; }

        // DataJenisUsaha
        [Display(GroupName = "Ijin Usaha", Description = "No. izin. Kosongkan jika tidak termasuk dalam jenis ini",
            Order = 21)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string PPE { get; set; }

        // DataJenisUsaha
        [Display(GroupName = "Ijin Usaha", Description = "No. izin. Kosongkan jika tidak termasuk dalam jenis ini",
            Order = 22)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string PEE { get; set; }

        // DataJenisUsaha
        [Display(GroupName = "Ijin Usaha", Description = "No. izin. Kosongkan jika tidak termasuk dalam jenis ini",
            Order = 23)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string MI { get; set; }

        // DataJenisUsaha
        [Display(GroupName = "Ijin Usaha", Description = "No. izin. Kosongkan jika tidak termasuk dalam jenis ini",
            Order = 24)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string BK { get; set; }

        // DataJenisUsaha
        [Display(GroupName = "Ijin Usaha", Description = "No. izin. Kosongkan jika tidak termasuk dalam jenis ini",
            Order = 25)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string BU { get; set; }

        // DataJenisUsaha
        [Display(GroupName = "Ijin Usaha", Order = 26)]
        [AknpDisplay("_izinUsahaPED", false, false)]
        [MaxLength(128)]
        public string PED { get; set; }

        #endregion

        #region screen 2

        // DataAkte
        [Display(Name = "No. Akte", GroupName = "Akte", Order = 26)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string NoAkte { get; set; }

        // DataAkte
        [Display(Name = "Tgl. Akte", GroupName = "Akte", Order = 27)]
        [AknpDisplay(null, false, false)]
        public DateTime? TglAkte { get; set; }

        // DataAkte
        [Display(GroupName = "Akte", Order = 28)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string Notaris { get; set; }

        // DataAkte
        [Display(Name = "File Akte", GroupName = "Akte", Order = 29)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        public string AkteFileUrl { get; set; }

        // DataAkte
        [Display(Name = "Daftar Pemegang Saham", GroupName = "Pemegang Saham", Order = 30)]
        [AknpDisplay(null, false, false)]
        public virtual ICollection<DataPemegangSahamRegistrasi> DaftarPemegangSaham { get; set; }
        
        // DataAkte
        [Display(Name = "Daftar Komisaris", GroupName = "Komisaris", Order = 31)]
        [AknpDisplay(null, true, false)]
        public virtual ICollection<DataKomisarisRegistrasi> DaftarKomisaris { get; set; }

        // DataAkte
        [Display(Name = "Daftar Direksi", GroupName = "Direksi", Order = 32)]
        [AknpDisplay(null, true, false)]
        public virtual ICollection<DataDireksiRegistrasi> DaftarDireksi { get; set; }

        // DataPejabatBerwenang
        [Display(Name = "Daftar Pejabat Berwenang", GroupName = "Pejabat Berwenang", Order = 33)]
        [AknpDisplay(null, false, false)]
        public virtual ICollection<DataPejabatBerwenangRegistrasi> DaftarPejabatBerwenang { get; set; }

        // DataContactPerson
        [Display(Name = "Daftar Contact Person", GroupName = "Contact Person", Order = 34)]
        [AknpDisplay(null, false, false)]
        public virtual ICollection<DataContactPersonRegistrasi> DaftarContactPerson { get; set; }


        #endregion

        #region screen 3

        #region screen3
        //Dokumen Perjanjian
        [Display(Name = "File Perjanjian Anggota Kliring Umum dengan KPEI", GroupName = "Dokumen Perjanjian", Order = 35)]
        [AknpDisplay("_filePerjanjianAKU", false, false)]
        [MaxLength(256)]
        public string PerjanjianAkuDenganKPEIFileUrl { get; set; }

        [Display(Name = "File Perjanjian Anggota Kliring Individu dengan KPEI", GroupName = "Dokumen Perjanjian", Order = 36)]
        [AknpDisplay("_filePerjanjianAKI", false, false)]
        [MaxLength(256)]
        public string PerjanjianAkiDenganKPEIFileUrl { get; set; }

        [Display(Name = "File Perjanjian Perusahaan Efek Daerah dengan KPEI", GroupName = "Dokumen Perjanjian", Order = 37)]
        [AknpDisplay("_filePerjanjianPED", false, false)]
        [MaxLength(256)]
        public string PerjanjianPEDDenganKPEIFileUrl { get; set; }

        [Display(Name = "File Perjanjian Partisipan dengan KPEI", GroupName = "Dokumen Perjanjian", Order = 38)]
        [AknpDisplay("_filePerjanjianPartisipan", false, false)]
        [MaxLength(256)]
        public string PerjanjianPartisipanDenganKPEIFileUrl { get; set; }

        [Display(Name = "File Perjanjian Anggota Kliring Umum dengan Trading Member", GroupName = "Dokumen Perjanjian", Order = 39)]
        [AknpDisplay("_filePerjanjianAKU", false, false)]
        [MaxLength(256)]
        public string PerjanjianAkuDenganTradingMemberFileUrl { get; set; }

        [Display(Name = "File Perjanjian Perusahaan Efek Daerah dengan AB Sponsor", GroupName = "Dokumen Perjanjian", Order = 40)]
        [AknpDisplay("_filePerjanjianPED", false, false)]
        [MaxLength(256)]
        public string PerjanjianPEDDenganABSponsorFileUrl { get; set; }

        [Display(Name = "File Perjanjian Portability", GroupName = "Dokumen Perjanjian", Order = 41)]
        [AknpDisplay("_filePerjanjianPortability", false, false)]
        [MaxLength(256)]
        public string PerjanjianPortabilityFileUrl { get; set; }
        #endregion

        // DataInformasiLain
        [Display(Name = "File Anggaran Dasar Perusahaan", GroupName = "Dokumen Pendukung AK / Partisipan", Order = 42)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        public string AnggaranDasarFileUrl { get; set; }

        // DataInformasiLain
        [MaxLength(256)]
        public string AktePendirianFileUrl { get; set; }

        // DataInformasiLain
        [Display(Name = "File Akte Perubahan dan SK Kemenkumham", GroupName = "Dokumen Pendukung AK / Partisipan", Order = 43)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        public string AktePerubahanFileUrl { get; set; }

        // DataInformasiLain
        [Display(Name = "File Ijin Usaha dari OJK", GroupName = "Dokumen Pendukung AK / Partisipan", Order = 44)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        public string IjinUsahaFileUrl { get; set; }

        // Modul LaporanKeuangan
        [Display(Name = "File Laporan Keuangan", GroupName = "Dokumen Pendukung AK / Partisipan", Order = 45)]
        [AknpDisplay("_laporanKeuanganFileUrl", false, false)]
        [MaxLength(256)]
        public string LaporanKeuanganFileUrl { get; set; }

        // DataInformasiLain
        [Display(Name = "File NPWP", GroupName = "Dokumen Pendukung AK / Partisipan", Order = 46)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        public string NPWPFileUrl { get; set; }


        // DataSPAB
        [MaxLength(256)]
        public string SPABFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string FormulirDaftarAkFileUrl { get; set; }

        // DataSPAK
        [MaxLength(256)]
        public string SPAKFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string SPDKFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string SKPEFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string PerjanjianLayananKliringPenjaminanTransaksiFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string PemberitahuanPenggunaanBankPembayaranFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string PersetujuanPemberianWewenangFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string SuratKuasaPindahBukuDanaFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string SuratKuasaFileUrl { get; set; }
        
        // Data Perjanjian
        [MaxLength(256)]
        public string PerjanjianTitipEfekUntukPinjamFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string SuratKuasaPMEFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string AppPemberiPinjamanLenderPMEFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string AppPenerimaPinjamanBorrowerPMEFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string PerjanjianPMEAsBorrowerFileUrl { get; set; }

        // ???
        [MaxLength(256)]
        public string PerjanjianPMEAsLenderFileUrl { get; set; }

        // ???
        [Display(Name = "File Dokumen Pendukung Lainnya", GroupName = "Dokumen Pendukung AK / Partisipan", Order = 57)]
        [AknpDisplay(null, false, false)]
        public virtual ICollection<DataSupportDocumentRegistrasi> OtherSupportDocumentsFileUrls { get; set; }

        #endregion
    }
}