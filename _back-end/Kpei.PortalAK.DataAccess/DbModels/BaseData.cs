﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public abstract class BaseData {

        protected BaseData() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }

        [Key]
        [Required]
        [MaxLength(128)]
        public string Id { get; set; }

        [Required]
        public DateTime CreatedUtc { get; set; }

        [Required]
        public DateTime UpdatedUtc { get; set; }

    }
}