﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataRequest {
        public DataRequest() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }
        [Key]
        [MaxLength(128)]
        public string Id { get; set; }
        [Required]
        [MaxLength(32)]
        public string KodeAK { get; set; }

        [Required]
        [MaxLength(128)]
        public string DetailId { get; set; }
        [Required]
        [MaxLength(128)]
        public string DetailTableName { get; set; }
        [Required]
        [MaxLength(128)]
        public string StatusRequest { get; set; }
        [Required]
        public DateTime CreatedUtc { get; set; }
        [Required]
        public DateTime UpdatedUtc { get; set; }
        public DateTime? TglAktifUtc { get; set; }
    }
}