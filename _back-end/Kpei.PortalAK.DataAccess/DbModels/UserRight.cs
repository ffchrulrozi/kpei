﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class UserRight {
        public UserRight() {
            Id = Guid.NewGuid().ToString();
            CreatedUtc = UpdatedUtc = DateTime.UtcNow;
        }

        [Required]
        [MaxLength(128)]
        public string Id { get; set; }

        [Required]
        [MaxLength(128)]
        public string MemberUserId { get; set; }

        [Required]
        [MaxLength(128)]
        public string ModuleId { get; set; }

        [Required]
        public bool KpeiApprover { get; set; }

        [Required]
        public bool KpeiModifier { get; set; }

        [Required]
        public bool KpeiViewer { get; set; }

        [Required]
        public bool KpeiAdmin { get; set; }

        [Required]
        public bool NonKpeiModifier { get; set; }

        [Required]
        public bool NonKpeiViewer { get; set; }

        [Required]
        public bool NonKpeiViewerAsKpei { get; set; }

        public DateTime CreatedUtc { get; set; }
        public DateTime UpdatedUtc { get; set; }
    }
}