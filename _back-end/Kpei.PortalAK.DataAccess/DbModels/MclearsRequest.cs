﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class MclearsRequest : BaseData {

        [Required]
        [MaxLength(32)]
        public string KodeAK { get; set; }

        [Required]
        public string StatusRequest { get; set; }

    }
}