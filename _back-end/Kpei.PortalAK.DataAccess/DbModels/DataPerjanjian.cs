﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataPerjanjian : BaseAknpDataWithSuratKeterangan{
        [Display(Name = "No. Perjanjian", GroupName = "Perjanjian", Order = 1)]
        [AknpDisplay(null, true, true)]
        [MaxLength(128)]
        public string NoPerjanjian { get; set; }

        [Display(Name = "Jenis Perjanjian", GroupName = "Perjanjian", Order = 2)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string JenisPerjanjian { get; set; }

        [Display(Name = "Tanggal Perjanjian", GroupName = "Perjanjian", Order = 3)]
        [AknpDisplay(null, true, false)]
        public DateTime? TglPerjanjianUtc { get; set; }

        [Display(Name = "File Surat Perjanjian Penitipan Efek untuk Dipinjamkan", GroupName = "Perjanjian", Order = 4)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        public string PerjanjianTitipEfekUntukPinjamFileUrl { get; set; }
    }
}
