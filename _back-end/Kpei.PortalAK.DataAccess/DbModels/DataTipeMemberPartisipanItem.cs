﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataTipeMemberPartisipanItem : BaseAknpTipePartisipan {
        [Required]
        public string ParentDataId { get; set; }

        [ForeignKey(nameof(ParentDataId))]
        public virtual DataTipeMember ParentData { get; set; }
    }
}
