﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataPejabatBerwenangRegistrasi : BaseAknpDirectorPerson {
        [Required]
        public string RegistrasiId { get; set; }

        [ForeignKey(nameof(RegistrasiId))]
        public virtual DataRegistrasi Registrasi { get; set; }
    }
}