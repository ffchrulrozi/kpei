﻿using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.DbModels {
    public class DataSupportDocumentRegistrasi : BaseAknpDataWithRegistrasi {
        [MaxLength(256)]
        public string DocFileUrl { get; set; }

        [MaxLength(256)]
        public string NamaDoc { get; set; }
    }
}