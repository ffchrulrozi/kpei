﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Kpei.PortalAK.DataAccess.Validators {
    public class CollectionItemMustValidAttribute : ValidationAttribute {
        /// <inheritdoc />
        protected override ValidationResult IsValid(object value, ValidationContext valctx) {
            var success = ValidationResult.Success;

            if (value == null) return success;

            var val = (value as IEnumerable<object>)?.ToArray() ?? new object[0];

            foreach (var item in val) {
                if (item == null) {
                    return new ValidationResult($"Tidak boleh ada item yang null dalam {valctx.DisplayName}.");
                }

                var itemValctx = new ValidationContext(item, valctx.ServiceContainer, null);
                var itemValres = new List<ValidationResult>();
                
                if (Validator.TryValidateObject(item, itemValctx, itemValres, true)) continue;
                
                var firstItemValres = itemValres[0];
                return new ValidationResult(
                    $"Ada item yang tidak valid dalam {valctx.DisplayName} ({firstItemValres.ErrorMessage}).",
                    new[] {valctx.MemberName});
            }

            return success;
        }
    }
}