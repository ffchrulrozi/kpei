﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;

namespace Kpei.PortalAK.DataAccess.Validators {
    public class CollectionLengthAttribute : ValidationAttribute {
        public CollectionLengthAttribute() {
            MinLength = int.MinValue;
            MaxLength = int.MaxValue;
        }

        public int MinLength { get; set; }

        public int MaxLength { get; set; }

        /// <inheritdoc />
        protected override ValidationResult IsValid(object value, ValidationContext valctx) {
            var success = ValidationResult.Success;
            if (value == null) {
                return success;
            }

            var val = value as IEnumerable<object>;
            var count = val.Count();

            if (count < MinLength) {
                return new ValidationResult($"Dalam {valctx.DisplayName} harus ada minimal {MinLength} item.",
                    new[] {valctx.MemberName});
            }

            if (count > MaxLength) {
                return new ValidationResult($"Dalam {valctx.DisplayName} harus ada maksimal {MaxLength} item.",
                    new[] {valctx.MemberName});
            }

            return success;
        }
    }
}