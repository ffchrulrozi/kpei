﻿using Kpei.PortalAK.DataAccess.DbModels;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Models {
    public class DataAknpApproveResult {
        public BaseAknpData Data { get; set; }
        public DataAknpRequestStatus NewStatus { get; set; }
    }
}