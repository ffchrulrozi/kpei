﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Models {
    public class DataAknpJenisAkte {

        private DataAknpJenisAkte(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<DataAknpJenisAkte> All() {
            yield return NEW_AKTE;
            yield return OLD_AKTE;
        }

        public static readonly DataAknpJenisAkte NEW_AKTE = new DataAknpJenisAkte("NEW", "Akte Baru");
        public static readonly DataAknpJenisAkte OLD_AKTE = new DataAknpJenisAkte("OLD", "Akte Lama");
    }
}
