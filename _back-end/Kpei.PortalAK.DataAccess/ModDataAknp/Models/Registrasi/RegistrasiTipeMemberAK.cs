﻿using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi {
    public class RegistrasiTipeMemberAK {
        public static readonly RegistrasiTipeMemberAK MEMBER_AK_ICM = new RegistrasiTipeMemberAK("ICM", "ICM");
        public static readonly RegistrasiTipeMemberAK MEMBER_AK_GCM = new RegistrasiTipeMemberAK("GCM", "GCM");
        public static readonly RegistrasiTipeMemberAK MEMBER_AK_TM = new RegistrasiTipeMemberAK("TM", "TM");
        public static readonly RegistrasiTipeMemberAK MEMBER_AK_CLIENT = new RegistrasiTipeMemberAK("Client", "Client");

        private RegistrasiTipeMemberAK(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<RegistrasiTipeMemberAK> All() {
            yield return MEMBER_AK_ICM;
            yield return MEMBER_AK_GCM;
            yield return MEMBER_AK_TM;
            yield return MEMBER_AK_CLIENT;
        }
    }
}