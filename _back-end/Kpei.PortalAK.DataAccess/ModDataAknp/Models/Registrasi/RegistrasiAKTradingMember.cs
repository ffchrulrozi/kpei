﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi
{
    public class RegistrasiAKTradingMember
    {
        public RegistrasiAKTradingMember()
        {

        }

        public string Name { get; set; }
        public string Label { get; set; }
    }
}
