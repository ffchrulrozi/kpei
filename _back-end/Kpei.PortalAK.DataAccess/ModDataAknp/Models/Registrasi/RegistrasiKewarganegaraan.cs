﻿using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi {
    public class RegistrasiKewarganegaraan {
        public static readonly RegistrasiKewarganegaraan WNI = new RegistrasiKewarganegaraan("WNI", "WNI");
        public static readonly RegistrasiKewarganegaraan WNA = new RegistrasiKewarganegaraan("WNA", "WNA");

        private RegistrasiKewarganegaraan(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<RegistrasiKewarganegaraan> All() {
            yield return WNI;
            yield return WNA;
        }
    }
}