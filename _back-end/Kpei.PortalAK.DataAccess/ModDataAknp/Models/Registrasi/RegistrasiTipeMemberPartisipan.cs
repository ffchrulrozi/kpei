﻿using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi {
    public class RegistrasiTipeMemberPartisipan {
        public static readonly RegistrasiTipeMemberPartisipan MEMBER_PARTISIPAN_ETP =
            new RegistrasiTipeMemberPartisipan("ETP", "ETP");

        public static readonly RegistrasiTipeMemberPartisipan MEMBER_PARTISIPAN_AO =
            new RegistrasiTipeMemberPartisipan("AO", "AO");

        public static readonly RegistrasiTipeMemberPartisipan MEMBER_PARTISIPAN_ID =
            new RegistrasiTipeMemberPartisipan("ID", "ID");

        private RegistrasiTipeMemberPartisipan(string name, string label) {
            Name = name;
            Label = label;
        }

        public RegistrasiTipeMemberPartisipan()
        {

        }

        public string Name { get; set; }
        public string Label { get; set; }

        public static IEnumerable<RegistrasiTipeMemberPartisipan> All() {
            yield return MEMBER_PARTISIPAN_ETP;
            yield return MEMBER_PARTISIPAN_AO;
            yield return MEMBER_PARTISIPAN_ID;
        }
    }
}