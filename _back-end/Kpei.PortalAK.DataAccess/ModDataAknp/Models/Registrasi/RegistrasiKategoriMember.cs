﻿using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi {
    public class RegistrasiKategoriMember {
        public static readonly RegistrasiKategoriMember MEMBER_AK = new RegistrasiKategoriMember("AK", "AK");

        public static readonly RegistrasiKategoriMember MEMBER_AKU =
            new RegistrasiKategoriMember("AKU", "Anggota Kliring Umum");

        public static readonly RegistrasiKategoriMember MEMBER_AKI =
            new RegistrasiKategoriMember("AKI", "Anggota Kliring Individual");

        public static readonly RegistrasiKategoriMember MEMBER_PARTISIPAN =
            new RegistrasiKategoriMember("PARTISIPAN", "Partisipan");

        private RegistrasiKategoriMember(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<RegistrasiKategoriMember> All() {
            yield return MEMBER_AK;
            yield return MEMBER_PARTISIPAN;
        }

        public static IEnumerable<RegistrasiKategoriMember> NewAll()
        {
            yield return MEMBER_AKU;
            yield return MEMBER_AKI;
            yield return MEMBER_PARTISIPAN;
        }
    }
}