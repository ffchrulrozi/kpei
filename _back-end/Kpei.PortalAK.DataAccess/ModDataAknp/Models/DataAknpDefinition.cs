﻿namespace Kpei.PortalAK.DataAccess.ModDataAknp.Models {
    public class DataAknpDefinition {
        public DataAknpDefinition(string dataSlug, string dataTitle) {
            DataSlug = dataSlug;
            DataTitle = dataTitle;
        }

        public string DataSlug { get; }
        public string DataTitle { get; }
    }
}