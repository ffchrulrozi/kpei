﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using Kpei.PortalAK.DataAccess.ModDataAknp.Drivers;
using Kpei.PortalAK.DataAccess.ModDataAknp.Services;
using System.Collections.Generic;
using System;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Models {
    public class AknpProfileDisplay : BaseAknpData {

        // Data Perjanjian

        [Display(Name = "No. Perjanjian")]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        public string NoPerjanjian { get; set; }
        
        [Display(Name = "Tgl. Perjanjian")]
        [AknpDisplay(null, false, false, ShowOnlyForKpei = true)]
        public DateTime? TglPerjanjianUtc { get; set; }
        
        // Data SPAK

        [Display(Name = "No. SPAK", GroupName = "SPAK")]
        [AknpDisplay(null, true, true, ShowOnlyForKpei = true)]
        [MaxLength(128)]
        public string NoSPAK { get; set; }

        [Display(Name = "Tgl. SPAK", GroupName = "SPAK")]
        [AknpDisplay(null, false, false, ShowOnlyForKpei = true)]
        public DateTime? TglSPAK { get; set; }
        
        // Data Akte
        [Display(Name = "Nama Perusahaan")]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        public string NamaPerusahaan { get; set; }

        // Data Alamat

        [Display(GroupName = "Alamat")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string Gedung { get; set; }

        [Display(GroupName = "Alamat")]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(256)]
        public string Jalan { get; set; }

        [Display(GroupName = "Alamat")]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        public string Kota { get; set; }

        [Display(Name = "Kode Pos", GroupName = "Alamat")]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(32)]
        public string KodePos { get; set; }

        [Display(GroupName = "Alamat")]
        [AknpDisplay(null, true, false)]
        [Required]
        [MaxLength(128)]
        public string Telp { get; set; }

        [Display(GroupName = "Alamat")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string Fax { get; set; }

        // Keanggotaan Lain : (PME + KBOS + EBU)

        //Data PME

        [Display(Name = "Status Lender")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string LenderStatus { get; set; }

        [Display(Name = "Status Borrower")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string BorrowerStatus { get; set; }

        //Data Perjanjian KBOS

        [Display(Name = "Status KBOS")]
        [AknpDisplay(null, true, false)]
        [Required]
        [MaxLength(128)]
        public string StatusKBOS { get; set; }

        // Data Perjanjian EBU

        [Display(Name = "Status Perjanjian EBU")]
        [AknpDisplay(null, true, false)]
        [Required]
        [MaxLength(128)]
        public string StatusPerjanjianEBU    { get; set; }

        //Data Tipe Member 
        //
        // [Display(Name = "Tipe Member")]
        // [AknpDisplay(null, true, false, ShowOnlyForKpei = true)]
        // [Required]
        // [MaxLength(128)]
        // public string JenisKeanggotaan { get; set; }
        
        // Data Informasi Lain 

        [Display(Name = "Status Perusahaan")]
        [AknpDisplay(null, true, false)]
        [Required]
        [MaxLength(128)]
        public string StatusPerusahaan { get; set; }
        
        [Display(Name = "NPWP")]
        [AknpDisplay(null, true, false)]
        [Required]
        [MaxLength(128)]
        public string NPWP { get; set; }

        //Data Akte

        [Display(Name = "Modal Dasar (Rp)")]
        [AknpDisplay(null, true, false)]
        [Required]
        public decimal ModalDasarRupiah { get; set; }
        
        [Display(Name = "Modal Disetor (Rp)")]
        [AknpDisplay(null, false, false)]
        [Required]
        public decimal ModalDisetorRupiah { get; set; }

        //Data Informasi Lain

        [Display(Name = "Anggota Bursa")]
        [AknpDisplay(null, true, false, ShowOnlyForKpei = true)]
        [Required]
        [MaxLength(128)]
        public string AnggotaBursa { get; set; }

        //Data Status Bursa

        [Display(Name = "Status Bursa")]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        public string StatusBursa { get; set; }

        [Display(Name = "Status KPEI")]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        public string StatusKpei { get; set; }

        //Data Jenis Usaha

        [Display(GroupName = "Ijin Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string PPE { get; set; }

        [Display(GroupName = "Ijin Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string PEE { get; set; }

        [Display(GroupName = "Ijin Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string MI { get; set; }

        [Display(GroupName = "Ijin Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string BK { get; set; }

        [Display(GroupName = "Ijin Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string BU { get; set; }

        // Data Bank Pembayaran
        
        [Display(Name = "Bank Pembayaran")]
        [AknpDisplay(null, true, false)]
        [Required]
        [MaxLength(128)]
        public string BankPembayaran { get; set; }

        //Pemegang Saham
        [Display(Name = "Daftar Pemegang Saham", GroupName = "Pemegang Saham")]
        [AknpDisplay(null, true, false)]
        public List<DataPemegangSaham> DaftarPemegangSaham { get; set; }
        
        // Pengurus Perusahaan (Komisaris + Direksi)

        //Data Akte
        [Display(Name = "Daftar Komisaris", GroupName = "Komisaris")]
        [AknpDisplay(null, true, false)]
        public List<DataKomisaris> DaftarKomisaris { get; set; }

        [Display(Name = "Daftar Direksi", GroupName = "Direksi")]
        [AknpDisplay(null, true, false)]
        public List<DataDireksi> DaftarDireksi { get; set; }

        // Wakil Perusahaan ( Pejabat Berwenang + Perjanjian KBOS (AdminUser))

        // DataPejabatBerwenang
        [Display(Name = "Daftar Pejabat Berwenang", GroupName = "Pejabat Berwenang")]
        [AknpDisplay(null, true, true)]
        public List<DataPejabatBerwenangItem> PejabatBerwenang { get; set; }

        [Display(Name = "Daftar Contact Person", GroupName = "Contact Person Operasional")]
        [AknpDisplay(null, true, true)]
        public List<DataContactPersonItem> DaftarContactPerson { get; set; }

        public void Populate(string kodeAk, IDataAknpDriverStore store) {
            // get latest data from stores
            var allDrivers = store.LoadDrivers().ToArray();

            KodeAk = kodeAk;
            
            #region DataPerjanjian

            var perjDrv = allDrivers.First(x => x.DataSlug == DataPerjanjianAknpDriver.SLUG);
            var perjData = perjDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataPerjanjian>();

            NoPerjanjian = perjData?.NoPerjanjian;
            TglPerjanjianUtc = perjData?.TglPerjanjianUtc;

            #endregion

            #region Data Akte

            var akteDrv = allDrivers.First(x => x.DataSlug == DataAkteAknpDriver.SLUG);
            var akteData = akteDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataAkte>();

            NamaPerusahaan = akteData?.NamaPerusahaan;
            DaftarKomisaris = akteData?.DaftarKomisaris.ToList() ?? new List<DataKomisaris>();
            DaftarDireksi = akteData?.DaftarDireksi.ToList() ?? new List<DataDireksi>();
            DaftarPemegangSaham = akteData?.DaftarPemegangSaham.ToList() ?? new List<DataPemegangSaham>();

            if (akteData != null) {
                ModalDasarRupiah = akteData.ModalDasarRupiah;
                ModalDisetorRupiah = akteData.ModalDisetorRupiah;
            }
            
            #endregion

            #region Data Alamat

            var alamatDrv = allDrivers.First(x => x.DataSlug == DataAlamatAknpDriver.SLUG);
            var alamatData = alamatDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataAlamat>();

            Gedung = alamatData?.Gedung;
            Jalan = alamatData?.Jalan;
            Kota = alamatData?.Kota;
            KodePos = alamatData?.KodePos;
            Telp = alamatData?.Telp;
            Fax = alamatData?.Fax;

            #endregion

            #region Data PME

            var pmeDrv= allDrivers.First(x => x.DataSlug == DataPmeAknpDriver.SLUG);
            var pmeData = pmeDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataPME>();

            LenderStatus = pmeData?.LenderStatus;
            BorrowerStatus = pmeData?.BorrowerStatus;

            #endregion

            #region Data Perjanjian KBOS

            var kbosDrv = allDrivers.First(x => x.DataSlug == DataPerjanjianKbosAknpDriver.SLUG);
            var kbosData = kbosDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataPerjanjianKBOS>();

            StatusKBOS = kbosData?.StatusKBOS;

            #endregion

            #region Data Perjanjian EBU

            var ebuDrv = allDrivers.First(x => x.DataSlug == DataPerjanjianEbuAknpDriver.SLUG);
            var ebuData = ebuDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataPerjanjianEBU>();

            StatusPerjanjianEBU = ebuData?.StatusPerjanjian;

            #endregion

            #region Data Informasi Lain

            var infoDrv = allDrivers.First(x => x.DataSlug == DataInformasiLainAknpDriver.SLUG);
            var infoData = infoDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataInformasiLain>();

            StatusPerusahaan = infoData?.StatusPerusahaan;
            NPWP = infoData?.NPWP;
            AnggotaBursa = infoData?.AnggotaBursa;

            #endregion

            #region Data Status Bursa

            var bursaDrv = allDrivers.First(x => x.DataSlug == DataStatusBursaAknpDriver.SLUG);
            var bursaData = bursaDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataStatusBursa>();

            StatusBursa = bursaData?.StatusBursa;

            #endregion

            #region Data Status KPEI

            var kpeiDrv = allDrivers.First(x => x.DataSlug == DataStatusKpeiAknpDriver.SLUG);
            var kpeiData = kpeiDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataStatusKPEI>();

            StatusKpei = kpeiData?.StatusKPEI;

            #endregion

            #region Data Jenis Usaha

            var usahaDrv = allDrivers.First(x => x.DataSlug == DataJenisUsahaAknpDriver.SLUG);
            var usahaData = usahaDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataJenisUsaha>();

            PPE = usahaData?.PPE;
            PEE = usahaData?.PEE;
            MI = usahaData?.MI;
            BU = usahaData?.BU;
            BK = usahaData?.BK;

            #endregion

            #region Data Bank Pembayaran

            var bankDrv = allDrivers.First(x => x.DataSlug == DataBankPembayaranAknpDriver.SLUG);
            var bankData = bankDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataBankPembayaran>();

            BankPembayaran = bankData?.BankPembayaran;

            #endregion
            
            #region Data Pejabat Berwenang

            var pejabatDrv = allDrivers.First(x => x.DataSlug == DataPejabatBerwenangAknpDriver.SLUG);
            var pejabatData = pejabatDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataPejabatBerwenang>();

            PejabatBerwenang = pejabatData?.Data.ToList() ?? new List<DataPejabatBerwenangItem>();

            #endregion

            #region Data Contact Person / Admin user

            var contactDrv = allDrivers.First(x => x.DataSlug == DataContactPersonAknpDriver.SLUG);
            var contactData = contactDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataContactPerson>();

            DaftarContactPerson = contactData?.Data.ToList() ?? new List<DataContactPersonItem>();

            #endregion

            #region Data Tipe Member

            var memberDrv = allDrivers.First(x => x.DataSlug == DataTipeMemberAknpDriver.SLUG);
            var memberData = memberDrv.GetLatestActiveData(kodeAk).DirectCastTo<DataTipeMember>();

            // JenisKeanggotaan = memberData?.KategoriMember;

            #endregion

            #region Data SPAK

            var spakDriver = allDrivers.First(x => x.DataSlug == DataSpakAknpDriver.SLUG);
            var spakData = spakDriver.GetLatestActiveData(kodeAk).DirectCastTo<DataSPAK>();

            NoSPAK = spakData?.NoSPAK;
            TglSPAK = spakData?.TanggalSPAKUtc;

            #endregion
            
        }
    }
}
