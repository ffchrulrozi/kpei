﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.Tools;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.ModSynchronize.Models;
using Kpei.PortalAK.DataAccess.Services;
using System.Data.SqlClient;
using System.Data.Entity;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.ModDataAknp.Drivers;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Models {
    public abstract class BaseAknpDriver {
        protected readonly AppDbContext Db;
        protected readonly ArmsDbContext Arms;
        protected readonly Random Rand;
        protected readonly ISysLogger _logger;
        protected static readonly string portalName = "portal-ak";

        protected BaseAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) {
            Db = db;
            Arms = arms;
            _logger = logger;
            Rand = new Random();
            AlternateDataSlug = new List<string>();
        }

        public abstract string DataSlug { get; }
        public List<string> AlternateDataSlug { get; }
        public abstract string DataTitle { get; }
        public abstract bool RequiresAkteForChange { get; }
        public bool RequiresSuratForChange => !RequiresAkteForChange;
        public virtual bool IsRegistrationData => false;
        public abstract Type DataClassType { get; }
        public virtual bool HideFromMenu => false;

        /// <summary>
        ///     Gets the type of the form class.<br />
        ///     Must inherit from <see cref="BaseAknpForm" />
        /// </summary>
        /// <value>
        ///     The type of the form class.
        /// </value>
        public abstract Type FormClassType { get; }

        public virtual Type DttRequestFormType => typeof(BaseAknpDttRequestForm);

        public abstract bool EnabledOnlyForKpei { get; }

        public abstract BaseAknpData GetById(string dataId);

        protected abstract IQueryable<BaseAknpData> BaseQueryInternal();

        protected abstract IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req);

        protected abstract BaseAknpData CreateInternal(BaseAknpData aknpData);

        public virtual bool IsMatchDataSlug(string testDataSlug) {
            return DataSlug == testDataSlug || AlternateDataSlug.Contains(testDataSlug);
        }

        public virtual BaseAknpData GetLatestActiveData(string kodeAk) {
            var q = BaseQueryInternal();
            var utcNow = DateTime.UtcNow;
            q = q.Where($"{nameof(BaseAknpData.Status)} == @0", DataAknpRequestStatus.APPROVED_STATUS.Name)
                .Where($"{nameof(BaseAknpData.Draft)} == @0", false)
                .Where($"{nameof(BaseAknpData.KodeAk)} == @0", kodeAk)
                .Where($"{nameof(BaseAknpData.ActiveUtc)} != null")
                .Where($"{nameof(BaseAknpData.ActiveUtc)} <= @0", utcNow)
                .OrderBy($"{nameof(BaseAknpData.CreatedUtc)} desc, {nameof(BaseAknpData.ActiveUtc)} desc");
            var dat = q.FirstOrDefault();
            return dat;
        }
        public virtual BaseAknpData GetLatestNewOngoingData(string kodeAk) {
            var q = BaseQueryInternal();
            var utcNow = DateTime.UtcNow;
            q = q.Where(x => x.Status == DataAknpRequestStatus.ONGOING_STATUS.Name || x.Status == DataAknpRequestStatus.NEW_STATUS.Name)
                .Where($"{nameof(BaseAknpData.Draft)} == @0", false)
                .Where($"{nameof(BaseAknpData.KodeAk)} == @0", kodeAk)
                .OrderBy($"{nameof(BaseAknpData.CreatedUtc)} desc, {nameof(BaseAknpData.ActiveUtc)} desc");
            var dat = q.FirstOrDefault();
            return dat;
        }

        public virtual BaseAknpData GetLatestDraftData(string kodeAk, string userId) {
            var q = BaseQueryInternal();
            var utcNow = DateTime.UtcNow;
            q = q.Where($"{nameof(BaseAknpData.Draft)} == @0", true)
                .Where($"{nameof(BaseAknpData.KodeAk)} == @0", kodeAk)
                .Where($"{nameof(BaseAknpData.UserDraft)} == @0", userId);
            var dat = q.FirstOrDefault();
            return dat;
        }

        public virtual QueryPair<BaseAknpData> List(BaseAknpDttRequestForm req, IQueryable<BaseAknpData> baseQuery = null) {
            var allQ = baseQuery ?? BaseQueryInternal();
            var q = baseQuery ?? BaseQueryInternal();

            if (allQ.GetType().GetGenericTypeDefinition() == typeof(EnumerableQuery<>)) {
                allQ = XCast(allQ);
            }

            if (q.GetType().GetGenericTypeDefinition() == typeof(EnumerableQuery<>)) {
                q = XCast(q);
            }
            
            q = q.Where($"{nameof(BaseAknpData.Draft)} != @0", true);
            allQ = allQ.Where($"{nameof(BaseAknpData.Draft)} != @0", true);

            if (!string.IsNullOrWhiteSpace(req.KodeAk)) {
                q = q.Where($"{nameof(BaseAknpData.KodeAk)} == @0", req.KodeAk);
                allQ = allQ.Where($"{nameof(BaseAknpData.KodeAk)} == @0", req.KodeAk);
            }

            var kws = new string[0];
            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                kws = Regex.Split(req.search.value, @"\s+");
            }

            if (req.ActiveFromDateTime != null) {
                var fromUtc = req.ActiveFromDateTime.Value.Date.ToUniversalTime();
                q = q.Where($"{nameof(BaseAknpData.ActiveUtc)} >= @0", fromUtc);
            }

            if (req.ActiveToDateTime != null) {
                var toUtc = req.ActiveToDateTime.Value.Date.AddDays(1).ToUniversalTime();
                q = q.Where($"{nameof(BaseAknpData.ActiveUtc)} <= @0", toUtc);
            }

            if (req.FromDateTime != null) {
                var fromUtc = req.FromDateTime.Value.Date.ToUniversalTime();
                q = q.Where($"{nameof(BaseAknpData.CreatedUtc)} >= @0", fromUtc);
            }

            if (req.ToDateTime != null) {
                var toUtc = req.ToDateTime.Value.Date.AddDays(1).ToUniversalTime();
                q = q.Where($"{nameof(BaseAknpData.CreatedUtc)} <= @0", toUtc);
            }

            if (!string.IsNullOrWhiteSpace(req.Status)) {
                var stat = req.Status.ToLowerInvariant();
                q = q.Where(e => e.Status.ToLower() == stat);
            }

            var testQ = ApplySearchQueryInternal(q, kws);
            if (testQ != null) q = testQ;

            testQ = ApplyFilterQueryInternal(q, req);
            if (testQ != null) q = testQ;

            var orderColumn = req.firstOrderColumn?.name;
            var orderDesc = req.order.FirstOrDefault()?.IsDesc ?? false;

            if (!string.IsNullOrWhiteSpace(orderColumn)) {
                testQ = ApplySortQueryInternal(q, orderColumn, orderDesc);
                if (testQ != null) q = testQ;
            } else {
                q = q.OrderBy($"{nameof(BaseAknpData.CreatedUtc)} {(orderDesc ? "desc" : "asc")}");
            }

            return new QueryPair<BaseAknpData>(allQ, q);
        }

        protected virtual IQueryable<BaseAknpData> ApplySearchQueryInternal(IQueryable<BaseAknpData> currentQuery,
            string[] keywords) {

            var q = currentQuery;

            var kws = keywords?.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray() ?? new string[0];
            if (kws.Length == 0) return q;

            var stringProps = DataClassType.GetProperties()
                .Where(x => x.CanRead && x.CanWrite && x.PropertyType == typeof(string)).ToArray();
            if (stringProps.Length == 0) return q;

            var where = new StringBuilder();
            var pars = new List<object>();
            var parIdx = 0;

            where.Append("(");
            for (var i = 0; i < stringProps.Length; i++) {
                var prop = stringProps[i];
                if (i > 0) {
                    where.Append(" or ");
                }
                where.Append(" ( ");
                for (var j = 0; j < kws.Length; j++) {
                    var kw = kws[j].ToLowerInvariant();
                    if (j > 0) {
                        where.Append(" and ");
                    }

                    pars.Add(kw);
                    @where.Append(currentQuery.GetType().GetGenericTypeDefinition() == typeof(EnumerableQuery<>)
                        ? $" ({prop.Name} != null and {prop.Name}.ToLowerInvariant().Contains(@{parIdx})) "
                        : $" {prop.Name}.Contains(@{parIdx}) ");

                    parIdx++;
                }
                where.Append(" ) ");
            }
            where.Append(")");

            return q.Where(where.ToString(), pars.ToArray());
        }

        protected virtual IQueryable<BaseAknpData> ApplySortQueryInternal(IQueryable<BaseAknpData> currentQuery,
            string columnName, bool isDescending) {
            var colProp = DataClassType.GetProperty(columnName);
            if (colProp != null && colProp.CanRead && colProp.CanWrite) {
                return currentQuery.OrderBy($"{columnName} {(isDescending ? "desc" : "asc")}");
            } else {
                return currentQuery;
            }
        }

        public virtual BaseAknpForm InitForm(string kodeAk) {
            var fm = Activator.CreateInstance(FormClassType) as BaseAknpForm;
            var dat = GetLatestActiveData(kodeAk);
            if (dat != null) {
                MapFromTo(dat, fm);
            }

            return fm;
        }

        public virtual void RemoveDraft(string id)
        {
            var draft = GetById(id);
            Db.Entry(draft).State = EntityState.Deleted;
            Db.SaveChanges();
        }

        public virtual BaseAknpForm InitDraftForm(string kodeAk, string userId) {
            var fm = Activator.CreateInstance(FormClassType) as BaseAknpForm;
            var dat = GetLatestDraftData(kodeAk, userId);
            if (dat != null) {
                MapFromTo(dat, fm);
            }

            return fm;
        }

        public virtual BaseAknpForm InitForm(BaseAknpData aknpData) {
            var fm = Activator.CreateInstance(FormClassType) as BaseAknpForm;
            MapFromTo(aknpData, fm);
            return fm;
        }

        public virtual void MapFromTo(BaseAknpForm form, BaseAknpData data) {
            if (data == null || form == null) return;

            var fmProps = FormClassType.GetProperties()
                .Where(x => x.CanRead && x.CanWrite && (!typeof(IEnumerable).IsAssignableFrom(x.PropertyType) ||
                                                        typeof(string).IsAssignableFrom(x.PropertyType))).ToArray();
            var dtProps = DataClassType.GetProperties()
                .Where(x => x.CanRead && x.CanWrite && (!typeof(IEnumerable).IsAssignableFrom(x.PropertyType) ||
                                                        typeof(string).IsAssignableFrom(x.PropertyType))).ToArray();

            foreach (var fmp in fmProps) {
                foreach (var dtp in dtProps) {
                    if (fmp.Name != dtp.Name) continue;
                    if (!dtp.PropertyType.IsAssignableFrom(fmp.PropertyType)) continue;
                    if ((typeof(DateTime).IsAssignableFrom(fmp.PropertyType) ||
                         typeof(DateTime?).IsAssignableFrom(fmp.PropertyType)) && fmp.Name.EndsWith("Utc")) {
                        var dtv = fmp.GetValue(form) as DateTime?;
                        if (dtv != null) {
                            dtp.SetValue(data, dtv.Value.ConvertFromWibToUtc());
                        } else {
                            if (typeof(DateTime?).IsAssignableFrom(dtp.PropertyType)) {
                                dtp.SetValue(data, null);
                            } else {
                                dtp.SetValue(data, new DateTime(1970, 1, 1, 0, 0, 0));
                            }
                        }
                    } else {
                        dtp.SetValue(data, fmp.GetValue(form));
                    }
                }
            }
        }

        public virtual void MapFromTo(BaseAknpData data, BaseAknpForm form) {
            if (data == null || form == null) return;

            var fmProps = FormClassType.GetProperties()
                .Where(x => x.CanRead && x.CanWrite && (!typeof(IEnumerable).IsAssignableFrom(x.PropertyType) ||
                                                        typeof(string).IsAssignableFrom(x.PropertyType))).ToArray();
            var dtProps = DataClassType.GetProperties()
                .Where(x => x.CanRead && x.CanWrite && (!typeof(IEnumerable).IsAssignableFrom(x.PropertyType) ||
                                                        typeof(string).IsAssignableFrom(x.PropertyType))).ToArray();

            foreach (var dtp in dtProps) {
                foreach (var fmp in fmProps) {
                    if (dtp.Name != fmp.Name) continue;
                    if (!fmp.PropertyType.IsAssignableFrom(dtp.PropertyType)) continue;
                    if ((typeof(DateTime).IsAssignableFrom(dtp.PropertyType) ||
                        typeof(DateTime?).IsAssignableFrom(dtp.PropertyType)) && dtp.Name.EndsWith("Utc")) {
                        var dtv = dtp.GetValue(data) as DateTime?;
                        if (dtv != null) {
                            fmp.SetValue(form, dtv.Value.ConvertFromUtcToWib());
                        } else {
                            if (typeof(DateTime?).IsAssignableFrom(fmp.PropertyType)) {
                                fmp.SetValue(form, null);
                            } else {
                                fmp.SetValue(form, new DateTime(1970, 1, 1, 0, 0, 0));
                            }
                        }
                    } else {
                        fmp.SetValue(form, dtp.GetValue(data));
                    }
                }
            }
        }

        public virtual Tuple<BaseAknpData, DataChangeRequest> CreateChangeRequest(BaseAknpData aknpData) {
            var dat = aknpData;
            var cr = new DataChangeRequest();

            Create(dat);

            cr.KodeAk = dat.KodeAk;
            cr.Status = dat.Status;
            cr.DataDriverSlug = DataSlug;
            cr.DataId = dat.Id;
            cr.CreatedUtc = dat.CreatedUtc;
            cr.UpdatedUtc = dat.UpdatedUtc;

            Db.DataChangeRequests.Add(cr);
            Db.SaveChanges();

            return Tuple.Create(dat, cr);
        }

        public virtual Tuple<BaseAknpData, DataChangeRequest> CreateDraftChangeRequest(BaseAknpData aknpData) {
            var dat = aknpData;
            var cr = new DataChangeRequest();
            Update(dat);

            cr.KodeAk = dat.KodeAk;
            cr.Status = dat.Status;
            cr.DataDriverSlug = DataSlug;
            cr.DataId = dat.Id;
            cr.CreatedUtc = dat.CreatedUtc;
            cr.UpdatedUtc = dat.UpdatedUtc;

            Db.DataChangeRequests.Add(cr);
            Db.SaveChanges();

            return Tuple.Create(dat, cr);
        }

        public virtual BaseAknpData Create(BaseAknpData aknpData) {
            var dat = aknpData;
            dat.CreatedUtc = dat.UpdatedUtc = DateTime.UtcNow;
            dat = CreateInternal(dat);
            return dat;
        }

        public virtual BaseAknpData Update(BaseAknpData aknpData) {
            var dat = aknpData;
            dat.UpdatedUtc = DateTime.UtcNow;
            dat = UpdateInternal(dat);

            var cr = Db.DataChangeRequests.FirstOrDefault(x => (x.DataDriverSlug == DataSlug || AlternateDataSlug.Contains(x.DataDriverSlug)) && x.DataId == dat.Id);
            if (cr != null) {
                cr.KodeAk = dat.KodeAk;
                cr.Status = dat.Status;
                cr.DataDriverSlug = DataSlug;
                cr.CreatedUtc = dat.CreatedUtc;
                cr.UpdatedUtc = dat.UpdatedUtc;
            }

            Db.SaveChanges();

            return dat;
        }

        protected virtual BaseAknpData UpdateInternal(BaseAknpData aknpData) {
            var dt = aknpData.DirectCastTo<BaseAknpData>();
            if (dt.Approver1UserId != null && dt.Approver2UserId != null && aknpData.Draft != true)
            {
                dt.ActiveUtc = DateTime.UtcNow;
            }
            Db.SaveChanges();
            return aknpData;
        }

        public virtual DataAknpApproveResult Approve(string dataId, string userId) {
            var dat = GetById(dataId);
            if (dat.Status == DataAknpRequestStatus.NEW_STATUS.Name) {
                dat.UpdatedUtc = DateTime.UtcNow;
                dat.Status = DataAknpRequestStatus.ONGOING_STATUS.Name;
                dat.Approver1UserId = userId;
                Db.SaveChanges();

                var cr = Db.DataChangeRequests.FirstOrDefault(x => x.DataDriverSlug == DataSlug && x.DataId == dat.Id);
                if (cr != null) {
                    cr.UpdatedUtc = dat.UpdatedUtc;
                    cr.Status = dat.Status;
                    cr.KodeAk = dat.KodeAk;
                    Db.SaveChanges();
                }
            } else if (dat.Status == DataAknpRequestStatus.ONGOING_STATUS.Name && userId != dat.Approver1UserId) {
                dat.UpdatedUtc = DateTime.UtcNow;
                dat.Status = DataAknpRequestStatus.APPROVED_STATUS.Name;
                dat.Approver2UserId = userId;
                dat.ActiveUtc = DateTime.UtcNow;
                Db.SaveChanges();

                var cr = Db.DataChangeRequests.FirstOrDefault(x => x.DataDriverSlug == DataSlug && x.DataId == dat.Id);
                if (cr != null) {
                    cr.UpdatedUtc = dat.UpdatedUtc;
                    cr.Status = dat.Status;
                    cr.KodeAk = dat.KodeAk;
                    Db.SaveChanges();
                }
            }
            

            return new DataAknpApproveResult {
                Data = dat,
                NewStatus = DataAknpRequestStatus.All().First(x => x.Name == dat.Status)
            };
        }

        public virtual BaseAknpData Reject(string dataId, string rejectReason) {
            var dat = GetById(dataId);

            dat.UpdatedUtc = DateTime.UtcNow;
            dat.Status = DataAknpRequestStatus.REJECTED_STATUS.Name;
            dat.RejectReason = rejectReason;
            Db.SaveChanges();

            var cr = Db.DataChangeRequests.FirstOrDefault(x => x.DataDriverSlug == DataSlug && x.DataId == dat.Id);
            if (cr != null) {
                cr.UpdatedUtc = dat.UpdatedUtc;
                cr.Status = dat.Status;
                cr.KodeAk = dat.KodeAk;
                Db.SaveChanges();
            }

            return dat;
        }

        protected IQueryable<BaseAknpData> XCast(IQueryable<BaseAknpData> source) {
            var genericCastMethod = typeof(Queryable)
                .GetMethods(BindingFlags.Public | BindingFlags.Static)
                .First(x => x.Name == "Cast" && x.GetGenericArguments().Length == 1 &&
                            x.ReturnType.GetGenericTypeDefinition() == typeof(IQueryable<>));
            var castMethod = genericCastMethod.MakeGenericMethod(DataClassType);
            return castMethod.Invoke(null, new object[] { source }) as IQueryable<BaseAknpData>;
        }

        protected IQueryable<BaseAknpData> XCast(IEnumerable<BaseAknpData> source) {
            var genericCastMethod = typeof(Enumerable)
                .GetMethods(BindingFlags.Public | BindingFlags.Static)
                .First(x => x.Name == "Cast" && x.GetGenericArguments().Length == 1 &&
                            x.ReturnType.GetGenericTypeDefinition() == typeof(IQueryable<>));
            var castMethod = genericCastMethod.MakeGenericMethod(DataClassType);
            return castMethod.Invoke(null, new object[] { source }) as IQueryable<BaseAknpData>;
        }

        //testing, for science!

        #region Arms Synchronization

        public abstract IEnumerable<BaseAknpData> SyncToArms(int offset, int limit);

        public abstract IEnumerable<BaseAknpData> SyncToPortal();

        public virtual IEnumerable<BaseAknpData> GetRecordToSync(string table, DateTime utcNow, SyncFlow flow, SyncAction action, int offset, int limit) {
            var q = BaseQueryInternal();
            var latestSync = GetLatestSynchronous(table, flow, action);
            q = q.Where($"{nameof(BaseAknpData.Status)} == @0", DataAknpRequestStatus.APPROVED_STATUS.Name)
                .Where($"{nameof(BaseAknpData.ActiveUtc)} != null")
                .Where($"{nameof(BaseAknpData.ActiveUtc)} <= @0", utcNow)
                .OrderBy($"{nameof(BaseAknpData.CreatedUtc)} asc, {nameof(BaseAknpData.UpdatedUtc)} asc");
            if (latestSync != null) {
                q = q.Where($"{nameof(BaseAknpData.CreatedUtc)} > @0", latestSync.CreatedUtc);
            }
            q = q.Skip(offset).Take(limit).AsNoTracking();
            return q.ToArray();
        }

        protected string GetArmsAnggotaId(string kodeAk) {
            return Arms.Database.SqlQuery<Int64>($@"Select AnggotaId from dbo.Anggota where Kode = '{ kodeAk }' order by CreatedDate desc, LastModifiedDate desc").FirstOrDefault().ToString();
        }

        protected IEnumerable<ArmsAnggota> GetArmsAnggota() {
            return Arms.Database.SqlQuery<ArmsAnggota>("Select * from dbo.Anggota").ToList();
        }


        protected IEnumerable<ArmsNotaris> GetForeignNotaris() {
            return Arms.Database.SqlQuery<ArmsNotaris>("Select * from dbo.FndNotaris").ToList();
        }

        protected IEnumerable<ArmsBank> GetForeignBank() {
            return Arms.Database.SqlQuery<ArmsBank>("Select * from dbo.FndBankPembayaran").ToList();
        }

        protected IEnumerable<ArmsPemantau> GetForeignPemantau() {
            return Arms.Database.SqlQuery<ArmsPemantau>("Select * from dbo.FndPemantau").ToList();
        }

        protected string GetArmsKodeAk(string anggotaId) {
            return Arms.Database.SqlQuery<Int64>($@"Select Kode from dbo.Anggota where AnggotaId = '{ anggotaId }' order by CreatedDate desc, LastModifiedDate desc").FirstOrDefault().ToString();
        }


        protected SynchronizeTable GetLatestSynchronous(string table, SyncFlow flow, SyncAction action) {
            return Db.SynchronizeTable.Where(e => e.RelatedTable == table && e.Flow == flow.Name && e.Action == action.Name).OrderByDescending(e => e.CreatedUtc).FirstOrDefault();
        }

        protected SynchronizeTable WriteSynchronize(string table, SyncFlow flow, SyncAction action, DateTime utcNow) {
            var newRecord = new SynchronizeTable {
                RelatedTable = table,
                Flow = flow.Name,
                Action = action.Name,
                SyncUtc = utcNow
            };
            Db.SynchronizeTable.Add(newRecord);
            Db.SaveChanges();
            return newRecord;
        }

        protected string GetForeignKey(string dbTable, string target, string param) {
            var key = Arms.Database.SqlQuery<Int64>($@"
                Select { target } from { dbTable } 
                where { param } order by CreatedDate desc, LastModifiedDate desc").FirstOrDefault();
            return key > 0 ? key.ToString() : null;
        }

        protected string CreateForeignKey(string dbTable, string fields, string values, string target, string param, object[] sqlParam = null) {
            Int64 key = 0;
            if (sqlParam != null) {
                key = Arms.Database.SqlQuery<Int64>($@"
                Insert into { dbTable } ({ fields }) values({values})
                Select { target } from { dbTable } 
                where { param } order by CreatedDate desc, LastModifiedDate desc", sqlParam).FirstOrDefault();
            } else {
                key = Arms.Database.SqlQuery<Int64>($@"
                Insert into { dbTable } ({ fields }) values({values})
                Select { target } from { dbTable } 
                where { param } order by CreatedDate desc, LastModifiedDate desc").FirstOrDefault();
            }
            return key > 0 ? key.ToString() : null;
        }

        protected List<BaseAknpData> GetArmsRecordToSync(string table, string target, DateTime utcNow) {
            return Arms.Database.SqlQuery<BaseAknpData>($@"Select {target} from {table} where CreatedDate > '{utcNow}'").ToList();
        }
        #endregion
    }
}