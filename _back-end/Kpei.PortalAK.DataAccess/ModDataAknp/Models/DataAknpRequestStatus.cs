﻿using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Models {
    public class DataAknpRequestStatus {

        private DataAknpRequestStatus(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<DataAknpRequestStatus> All() {
            yield return NEW_STATUS;
            yield return ONGOING_STATUS;
            yield return APPROVED_STATUS;
            yield return REJECTED_STATUS;
        }

        public static readonly DataAknpRequestStatus NEW_STATUS = new DataAknpRequestStatus("NEW", "NEW");
        public static readonly DataAknpRequestStatus ONGOING_STATUS = new DataAknpRequestStatus("ONGOING", "ONGOING");
        public static readonly DataAknpRequestStatus APPROVED_STATUS = new DataAknpRequestStatus("APPROVED", "APPROVED");
        public static readonly DataAknpRequestStatus REJECTED_STATUS = new DataAknpRequestStatus("REJECTED", "REJECTED");
    }
}