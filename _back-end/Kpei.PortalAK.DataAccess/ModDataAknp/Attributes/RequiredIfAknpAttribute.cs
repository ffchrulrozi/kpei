﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Attributes {
    public class RequiredIfAknpAttribute : ValidationAttribute {
        /// <inheritdoc />
        protected override ValidationResult IsValid(object value, ValidationContext valctx) {
            var successResult = ValidationResult.Success;

            var usr = AppUser.CreateFromIdentity(HttpContext.Current.User.Identity);
            if (!usr.IsAknp && !usr.IsGuestAknp) return successResult;

            var errResult = new ValidationResult($"{valctx.DisplayName} harus diisi.", new[] {valctx.MemberName});
            if (value == null) return errResult;

            var valstr = value as string;
            if (valstr == null) return successResult;

            return string.IsNullOrWhiteSpace(valstr) ? errResult : successResult;

        }
    }
}