﻿using System;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Attributes {
    public class AknpDisplayAttribute : Attribute {
        public AknpDisplayAttribute(string templateName, bool showInDetailTable, bool showInHistoryTable) {
            TemplateName = templateName;
            ShowInDetailTable = showInDetailTable;
            ShowInHistoryTable = showInHistoryTable;
        }

        public string TemplateName { get; }
        public bool ShowInDetailTable { get; }
        public bool ShowInHistoryTable { get; }
        public bool ShowOnlyForKpei { get; set; }
    }
}