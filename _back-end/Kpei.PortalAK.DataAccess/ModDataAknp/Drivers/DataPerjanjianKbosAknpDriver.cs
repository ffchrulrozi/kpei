﻿using System;
using System.Collections.Generic;
using System.Linq;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.PerjanjianKbos;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSynchronize.Models;
using Kpei.PortalAK.DataAccess.Services;
using System.Data.SqlClient;
using System.Transactions;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataPerjanjianKbosAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-perjanjian-kbos";

        /// <inheritdoc />
        public DataPerjanjianKbosAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) { }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Keanggotaan KBOS";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => false;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataPerjanjianKBOS);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataPerjanjianKbosAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => true;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataPerjanjianKBOS.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataPerjanjianKBOS.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataPerjanjianKBOS>();
            Db.DataPerjanjianKBOS.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {
            throw new NotImplementedException();
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {

            #region Get Necessary Data

            var utcNow = DateTime.UtcNow;
            var syncData = GetLatestSynchronous(DataSlug, SyncFlow.ARMS_PORTAL_FLOW, SyncAction.INSERT_ACTION);
            var minDate = syncData != null ? $" and a1.LastModifiedDate > @SyncDate" : "";
            SqlParameter[] param;
            if (syncData != null) {
                param = new[] {
                    new SqlParameter("UtcNow", utcNow.AddHours(7)),
                    new SqlParameter("SyncDate", syncData.SyncUtc.AddHours(7))
                };
            } else {
                param = new[] {
                    new SqlParameter("UtcNow", utcNow.AddHours(7)),
                };
            }

            #endregion

            IEnumerable<DataPerjanjianKBOS> dats;

            #region query 

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {
                dats = Arms.Database.SqlQuery<DataPerjanjianKBOS>($@"
select 
a2.Kode as KodeAK, 
      SpmKbos as NoSPM,
      JenisPerjanjian,
      TglSpmKbos as TglSPMUtc,
      isnull(nullif(NoLpkb, ''), '-') as LPKB,
      isnull(nullif(NoLpos, ''), '-') as LPOS,
      Status as StatusKBOS,
      TglAktif as TanggalAktifUtc,
      Keterangan,
      a1.CreatedDate as CreatedUtc,
      a1.LastModifiedDate as UpdatedUtc
from dbo.HistoriAnggotaKbos as a1
inner join dbo.Anggota as a2 on a1.AnggotaId = a2.AnggotaId

                where a1.LastModifiedDate <= @UtcNow " + minDate, param).ToList();
                scope.Complete();
            }

            #endregion

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {
                foreach (var dat in dats) {
                    var data = dat as DataPerjanjianKBOS;

                    #region WIB to UTC conversion

                    data.CreatedUtc = data.CreatedUtc.AddHours(-7);
                    data.ActiveUtc = data.ActiveUtc?.AddHours(-7);
                    data.UpdatedUtc = data.UpdatedUtc.AddHours(-7);

                    data.TglSPMUtc = data.TglSPMUtc?.AddHours(-7);

                    #endregion


                    Db.DataPerjanjianKBOS.Add(data);
                }
                Db.SaveChanges();
                scope.Complete();
            }
            if (dats.Count() > 0) WriteSynchronize(DataSlug, SyncFlow.ARMS_PORTAL_FLOW, SyncAction.INSERT_ACTION, utcNow);
            return dats;
        }
    }
}