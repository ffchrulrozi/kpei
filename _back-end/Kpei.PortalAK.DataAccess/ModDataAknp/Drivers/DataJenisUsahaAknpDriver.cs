﻿using System;
using System.Collections.Generic;
using System.Linq;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.JenisUsaha;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSynchronize.Models;
using Kpei.PortalAK.DataAccess.Services;
using System.Data.SqlClient;
using System.Data;
using System.Transactions;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataJenisUsahaAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-ijin-usaha";

        public const string oldSLUG = "data-jenis-usaha";

        /// <inheritdoc />
        public DataJenisUsahaAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) {
            AlternateDataSlug.Add(oldSLUG);
        }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Ijin Usaha";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => false;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataJenisUsaha);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataJenisUsahaAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => false;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataJenisUsaha.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataJenisUsaha.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataJenisUsaha>();
            Db.DataJenisUsaha.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {

            #region Get Necessary Data

            var utcNow = DateTime.UtcNow;
            var dbData = GetRecordToSync(DataSlug, utcNow, SyncFlow.PORTAL_ARMS_FLOW, SyncAction.INSERT_ACTION, offset, limit);
            var dats = dbData.DirectCastTo<IEnumerable<BaseAknpData>>();
            var dbAnggota = GetArmsAnggota();

            #endregion

            #region Reset Last Version in ARMS

            var aks = dbData.Select(e => e.KodeAk).Distinct();
            foreach (var ak in aks) {
                var anggotaId = dbAnggota.Where(e => e.Kode == ak)?.OrderByDescending(e => e.CreatedDate)?.FirstOrDefault()?.AnggotaId;
                if (anggotaId != 0 && anggotaId != null) {
                    Arms.Database.ExecuteSqlCommand(@"DECLARE	@return_value int
                    EXEC    @return_value = [dbo].[uspResetLastVersionHistoriAnggotaJenisUsaha]
                            @AnggotaId = " + anggotaId);
                }
            }

            #endregion

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {
                foreach (var dat in dats.ToList()) {

                    #region Building Parameter 

                    var anggotaId = dbAnggota.Where(e => e.Kode == dat.KodeAk)?.OrderByDescending(e => e.CreatedDate)?.FirstOrDefault()?.AnggotaId;
                    var data = dat as DataJenisUsaha;
                    if (anggotaId == 0 || anggotaId == null) continue;
                    int lastVersion =
                        (DateTime.Compare(
                            data.CreatedUtc,
                            dats.Where(e => e.KodeAk == dat.KodeAk).Max(x => x.CreatedUtc)) == 0 &&
                        DateTime.Compare(
                            data.UpdatedUtc,
                            dats.Where(e => e.KodeAk == dat.KodeAk).Max(x => x.UpdatedUtc)) == 0 ?
                        1 : 0); 

                    var param = new[] {
                        new SqlParameter("CreatedUtc", SqlDbType.DateTime) {
                            Value = data.CreatedUtc.AddHours(7)
                        },
                        new SqlParameter("UpdatedUtc", SqlDbType.DateTime) {
                            Value = data.UpdatedUtc.AddHours(7)
                        }
                    };

                    #endregion

                    #region query 

                    Arms.Database.ExecuteSqlCommand($@"INSERT INTO dbo.HistoriAnggotaJenisUsaha
                    (AnggotaId, 
                    NoPpe, 
                    NoPee, 
                    NoMi, 
                    CreatedBy, 
                    LastModifiedBy,
                    CreatedDate, 
                    LastModifiedDate, 
                    LastVersion
                    )
                    values(
                    '{anggotaId}',
                    left('{data.PPE}', 25),
                    left('{data.PEE}', 25),
                    left('{data.MI}', 25),
                    '{portalName}',
                    '{portalName}',
                    @CreatedUtc,
                    @UpdatedUtc,
                    '{lastVersion}'
                    )", param);

                    #endregion

                }
                scope.Complete();
            }
            if (dats.Count() > 0) {
                WriteSynchronize(DataSlug, SyncFlow.PORTAL_ARMS_FLOW, SyncAction.INSERT_ACTION, utcNow);
            }
            return dats;
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {
            throw new NotImplementedException();
        }
    }
}
