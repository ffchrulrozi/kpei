﻿using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.Alamat;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSynchronize.Models;
using Kpei.PortalAK.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataAlamatAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-alamat";

        /// <inheritdoc />
        public DataAlamatAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) { }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Alamat";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => false;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataAlamat);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataAlamatAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => false;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataAlamat.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataAlamat.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataAlamat>();
            Db.DataAlamat.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {
            var utcNow = DateTime.UtcNow;
            var dbData = GetRecordToSync(DataSlug, utcNow, SyncFlow.PORTAL_ARMS_FLOW, SyncAction.INSERT_ACTION, offset, limit);

            #region Get Necessary Data

            var dats = dbData.ToList();
            var dbAnggota = GetArmsAnggota();

            #endregion

            #region Reset Latest Version

            var aks = dbData.Select(e => e.KodeAk).Distinct();
            foreach (var ak in aks) {
                var anggotaId = dbAnggota.Where(e => e.Kode == ak)?.OrderByDescending(e => e.CreatedDate)?.FirstOrDefault()?.AnggotaId;
                if (anggotaId != 0 && anggotaId != null) {
                    Arms.Database.ExecuteSqlCommand(@"DECLARE	@return_value int
                    EXEC    @return_value = [dbo].[uspResetLastVersionHistoriAnggotaAlamat]
                            @AnggotaId = " + anggotaId);
                }
            }

            #endregion

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {
                foreach(var dat in dats) {
                    #region Building Parameter

                    var data = dat as DataAlamat;
                    var anggotaId = dbAnggota.Where(e => e.Kode == dat.KodeAk)?.OrderByDescending(e => e.CreatedDate)?.FirstOrDefault()?.AnggotaId;
                    if (anggotaId == 0 || anggotaId == null) continue;
                    int lastVersion =
                        (DateTime.Compare(
                            data.CreatedUtc,
                            dats.Where(e => e.KodeAk == dat.KodeAk).Max(x => x.CreatedUtc)) == 0 &&
                        DateTime.Compare(
                            data.UpdatedUtc,
                            dats.Where(e => e.KodeAk == dat.KodeAk).Max(x => x.UpdatedUtc)) == 0 ?
                        1 : 0);

                    if (!string.IsNullOrWhiteSpace(anggotaId.ToString())) {

                        var param = new[] {
                            new SqlParameter("CreatedUtc", SqlDbType.DateTime) {
                                Value = data.CreatedUtc.AddHours(7)
                            },
                            new SqlParameter("UpdatedUtc", SqlDbType.DateTime) {
                                Value = data.UpdatedUtc.AddHours(7)
                            }
                        };

                        #endregion

                        #region query

                        Arms.Database.ExecuteSqlCommand($@"
INSERT INTO dbo.HistoriAnggotaAlamat(
                    AnggotaId, 
                    Gedung, 
                    Jalan, 
                    Kota, 
                    KodePos, 
                    Phone, 
                    Fax, 
                    Website, 
                    Utama, 
                    CreatedBy, 
                    LastModifiedBy,
                    CreatedDate, 
                    LastModifiedDate,
                    LastVersion 
                    )
                    values(
                    '{anggotaId}',
                    left('{data.Gedung}', 200),
                    left('{data.Jalan}', 200),
                    left('{data.Kota}', 50),
                    left('{data.KodePos}', 5),
                    left('{data.Telp}', 200),
                    left('{data.Fax}', 50),
                    left('{data.Website}', 50),
                    '1',
                    '{portalName}',
                    '{portalName}',
                    @CreatedUtc,
                    @UpdatedUtc,
                    '{lastVersion}'
                    )", param);

                        #endregion
                    }
                }
                scope.Complete();
            }
            if (dats.Count > 0) {
                WriteSynchronize(DataSlug, SyncFlow.PORTAL_ARMS_FLOW, SyncAction.INSERT_ACTION, utcNow);
            }
            return dats;
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {
            throw new NotImplementedException();
        }
    }
}