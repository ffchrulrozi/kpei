﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.Akte;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.Registrasi;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;
using System.Transactions;
using System.Data.SqlClient;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataRegistrasiAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-registrasi";

        /// <inheritdoc />
        public DataRegistrasiAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) { }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Registrasi Baru";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => true;

        /// <inheritdoc />
        public override bool IsRegistrationData => true;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataRegistrasi);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataRegistrasiAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => false;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataRegistrasi.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataRegistrasi.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dt = aknpData.DirectCastTo<DataRegistrasi>();
            Db.DataRegistrasi.Add(dt);
            Db.SaveChanges();
            if (dt.Approver1UserId != null && dt.Approver2UserId != null && aknpData.Draft != true)
            {
                Approve(dt.Id, aknpData.Approver1UserId);
            }
            return dt;
        }

        protected override BaseAknpData UpdateInternal(BaseAknpData aknpData) {
            var dt = aknpData.DirectCastTo<DataRegistrasi>();
            if (dt.Approver1UserId != null && dt.Approver2UserId != null && aknpData.Draft != true)
            {
                dt.ActiveUtc = DateTime.UtcNow;
                Db.SaveChanges();
                Approve(dt.Id, aknpData.Approver1UserId);
            }
            Db.SaveChanges();
            
            return dt;
        }

        /// <inheritdoc />
        public override void MapFromTo(BaseAknpForm form, BaseAknpData data) {
            base.MapFromTo(form, data);

            var fm = form.DirectCastTo<DataRegistrasiAknpForm>();
            var dt = data.DirectCastTo<DataRegistrasi>();

            Action assignListPemegangSaham = () => {
                dt.DaftarPemegangSaham = dt.DaftarPemegangSaham ?? new List<DataPemegangSahamRegistrasi>();

                var addedIds = new List<string>();

                foreach (var p in fm.DaftarPemegangSaham) {
                    if (string.IsNullOrWhiteSpace(p.Id)) {
                        p.Id = Guid.NewGuid().ToString();
                    }

                    var existing = dt.DaftarPemegangSaham.FirstOrDefault(x => x.Id == p.Id);
                    if (existing == null) {
                        addedIds.Add(p.Id);
                        dt.DaftarPemegangSaham.Add(new DataPemegangSahamRegistrasi {
                            RegistrasiId = dt.Id,
                            Id = p.Id,
                            LembarSaham = p.LembarSaham,
                            Nama = p.Nama,
                            NominalSahamRupiah = p.NominalSahamRupiah,
                            WargaNegara = p.WargaNegara
                        });
                    } else {
                        addedIds.Add(p.Id);
                        existing.UpdatedUtc = DateTime.UtcNow;
                        existing.LembarSaham = p.LembarSaham;
                        existing.Nama = p.Nama;
                        existing.NominalSahamRupiah = p.NominalSahamRupiah;
                        existing.WargaNegara = p.WargaNegara;
                    }
                }

                var rems = dt.DaftarPemegangSaham.Where(x => !addedIds.Contains(x.Id)).ToList();
                foreach (var rem in rems) {
                    dt.DaftarPemegangSaham.Remove(rem);
                    Db.Entry(rem).State = EntityState.Deleted;
                }
            };
            assignListPemegangSaham();

            Action assignListKomisaris = () => {
                dt.DaftarKomisaris = dt.DaftarKomisaris ?? new List<DataKomisarisRegistrasi>();

                var addedIds = new List<string>();

                foreach (var p in fm.DaftarKomisaris) {
                    if (string.IsNullOrWhiteSpace(p.Id)) {
                        p.Id = Guid.NewGuid().ToString();
                    }

                    var existing = dt.DaftarKomisaris.FirstOrDefault(x => x.Id == p.Id);
                    if (existing == null) {
                        addedIds.Add(p.Id);
                        dt.DaftarKomisaris.Add(new DataKomisarisRegistrasi {
                            RegistrasiId = dt.Id,
                            Id = p.Id,
                            Nama = p.Nama,
                            Email = p.Email,
                            HP = p.HP,
                            Jabatan = p.Jabatan,
                            Telp = p.Telp,
                            TtdFileUrl = p.TtdFileUrl,
                            KtpFileUrl = p.KtpFileUrl
                        });
                    } else {
                        addedIds.Add(p.Id);
                        existing.UpdatedUtc = DateTime.UtcNow;
                        existing.Nama = p.Nama;
                        existing.Email = p.Email;
                        existing.HP = p.HP;
                        existing.Jabatan = p.Jabatan;
                        existing.Telp = p.Telp;
                        existing.TtdFileUrl = p.TtdFileUrl;
                        existing.KtpFileUrl = p.KtpFileUrl;
                    }
                }

                var rems = dt.DaftarKomisaris.Where(x => !addedIds.Contains(x.Id)).ToList();
                foreach (var rem in rems) {
                    dt.DaftarKomisaris.Remove(rem);
                    Db.Entry(rem).State = EntityState.Deleted;
                }
            };
            assignListKomisaris();

            Action assignListDireksi = () => {
                dt.DaftarDireksi = dt.DaftarDireksi ?? new List<DataDireksiRegistrasi>();

                var addedIds = new List<string>();

                foreach (var p in fm.DaftarDireksi) {
                    if (string.IsNullOrWhiteSpace(p.Id)) {
                        p.Id = Guid.NewGuid().ToString();
                    }

                    var existing = dt.DaftarDireksi.FirstOrDefault(x => x.Id == p.Id);
                    if (existing == null) {
                        addedIds.Add(p.Id);
                        dt.DaftarDireksi.Add(new DataDireksiRegistrasi {
                            RegistrasiId = dt.Id,
                            Id = p.Id,
                            Nama = p.Nama,
                            Email = p.Email,
                            HP = p.HP,
                            Jabatan = p.Jabatan,
                            Telp = p.Telp,
                            TtdFileUrl = p.TtdFileUrl,
                            KtpFileUrl = p.KtpFileUrl
                        });
                    } else {
                        addedIds.Add(p.Id);
                        existing.UpdatedUtc = DateTime.UtcNow;
                        existing.Nama = p.Nama;
                        existing.Email = p.Email;
                        existing.HP = p.HP;
                        existing.Jabatan = p.Jabatan;
                        existing.Telp = p.Telp;
                        existing.TtdFileUrl = p.TtdFileUrl;
                        existing.KtpFileUrl = p.KtpFileUrl;
                    }
                }

                var rems = dt.DaftarDireksi.Where(x => !addedIds.Contains(x.Id)).ToList();
                foreach (var rem in rems) {
                    dt.DaftarDireksi.Remove(rem);
                    Db.Entry(rem).State = EntityState.Deleted;
                }
            };
            assignListDireksi();

            Action assignListPejabatBerwenang = () => {
                dt.DaftarPejabatBerwenang = dt.DaftarPejabatBerwenang ?? new List<DataPejabatBerwenangRegistrasi>();

                var addedIds = new List<string>();

                foreach (var p in fm.DaftarPejabatBerwenang) {
                    if (string.IsNullOrWhiteSpace(p.Id)) {
                        p.Id = Guid.NewGuid().ToString();
                    }

                    var existing = dt.DaftarPejabatBerwenang.FirstOrDefault(x => x.Id == p.Id);
                    if (existing == null) {
                        addedIds.Add(p.Id);
                        dt.DaftarPejabatBerwenang.Add(new DataPejabatBerwenangRegistrasi {
                            RegistrasiId = dt.Id,
                            Id = p.Id,
                            Nama = p.Nama,
                            Email = p.Email,
                            HP = p.HP,
                            Jabatan = p.Jabatan,
                            Telp = p.Telp,
                            TtdFileUrl = p.TtdFileUrl,
                            KtpFileUrl = p.KtpFileUrl
                        });
                    } else {
                        addedIds.Add(p.Id);
                        existing.UpdatedUtc = DateTime.UtcNow;
                        existing.Nama = p.Nama;
                        existing.Email = p.Email;
                        existing.HP = p.HP;
                        existing.Jabatan = p.Jabatan;
                        existing.Telp = p.Telp;
                        existing.TtdFileUrl = p.TtdFileUrl;
                        existing.KtpFileUrl = p.KtpFileUrl;
                    }
                }

                var rems = dt.DaftarPejabatBerwenang.Where(x => !addedIds.Contains(x.Id)).ToList();
                foreach (var rem in rems) {
                    dt.DaftarPejabatBerwenang.Remove(rem);
                    Db.Entry(rem).State = EntityState.Deleted;
                }
            };
            assignListPejabatBerwenang();

            Action assignListContactPerson = () => {
                dt.DaftarContactPerson = dt.DaftarContactPerson ?? new List<DataContactPersonRegistrasi>();

                var addedIds = new List<string>();

                foreach (var p in fm.DaftarContactPerson) {
                    if (string.IsNullOrWhiteSpace(p.Id)) {
                        p.Id = Guid.NewGuid().ToString();
                    }

                    var existing = dt.DaftarContactPerson.FirstOrDefault(x => x.Id == p.Id);
                    if (existing == null) {
                        addedIds.Add(p.Id);
                        dt.DaftarContactPerson.Add(new DataContactPersonRegistrasi {
                            RegistrasiId = dt.Id,
                            Id = p.Id,
                            Nama = p.Nama,
                            Email = p.Email,
                            HP = p.HP,
                            Jabatan = p.Jabatan,
                            Telp = p.Telp
                        });
                    } else {
                        addedIds.Add(p.Id);
                        existing.UpdatedUtc = DateTime.UtcNow;
                        existing.Nama = p.Nama;
                        existing.Email = p.Email;
                        existing.HP = p.HP;
                        existing.Jabatan = p.Jabatan;
                        existing.Telp = p.Telp;
                    }
                }

                var rems = dt.DaftarContactPerson.Where(x => !addedIds.Contains(x.Id)).ToList();
                foreach (var rem in rems) {
                    dt.DaftarContactPerson.Remove(rem);
                    Db.Entry(rem).State = EntityState.Deleted;
                }
            };
            assignListContactPerson();

            Action assignListSupDoc = () => {
                dt.OtherSupportDocumentsFileUrls = dt.OtherSupportDocumentsFileUrls ?? new List<DataSupportDocumentRegistrasi>();

                var addedIds = new List<string>();

                foreach (var p in fm.OtherSupportDocumentsFileUrls) {
                    if (string.IsNullOrWhiteSpace(p.Id)) {
                        p.Id = Guid.NewGuid().ToString();
                    }

                    var existing = dt.OtherSupportDocumentsFileUrls.FirstOrDefault(x => x.Id == p.Id);
                    if (existing == null) {
                        addedIds.Add(p.Id);
                        dt.OtherSupportDocumentsFileUrls.Add(new DataSupportDocumentRegistrasi {
                            RegistrasiId = dt.Id,
                            Id = p.Id,
                            NamaDoc = p.NamaDoc,
                            DocFileUrl = p.DocFileUrl
                        });
                    } else {
                        addedIds.Add(p.Id);
                        existing.UpdatedUtc = DateTime.UtcNow;
                        existing.NamaDoc = p.NamaDoc;
                        existing.DocFileUrl = p.DocFileUrl;
                    }
                }

                var rems = dt.OtherSupportDocumentsFileUrls.Where(x => !addedIds.Contains(x.Id)).ToList();
                foreach (var rem in rems) {
                    dt.OtherSupportDocumentsFileUrls.Remove(rem);
                    Db.Entry(rem).State = EntityState.Deleted;
                }
            };
            assignListSupDoc();

            Action assignListLayananBaru = () =>
            {
                dt.DataLayananBaru = dt.DataLayananBaru ?? new List<DataLayananBaru>();

                if (dt.DataLayananBaru.Count() > 0)
                {
                    Db.DataLayananBaru.RemoveRange(dt.DataLayananBaru);
                    Db.SaveChanges();
                }

                var addedIds = new List<string>();

                if (fm.KategoriMember == "AKU")
                {
                    foreach (var p in fm.FormLayananBaruAku)
                    {
                        if (string.IsNullOrWhiteSpace(p.Id))
                        {
                            p.Id = Guid.NewGuid().ToString();
                        }

                        var existing = dt.DataLayananBaru.FirstOrDefault(x => x.Id == p.Id);
                        if (existing == null)
                        {
                            addedIds.Add(p.Id);
                            dt.DataLayananBaru.Add(new DataLayananBaru
                            {
                                RegistrasiId = dt.Id,
                                Id = p.Id,
                                TipeMemberAK = p.TipeMemberAK,
                                TipeMemberPartisipan = p.TipeMemberPartisipan,
                                FieldName = p.FieldName,
                                FieldType = p.FieldType,
                                FieldValue = p.FieldValue,
                                LayananBaruId = p.LayananBaruId
                            });
                        }
                        else
                        {
                            addedIds.Add(p.Id);
                            existing.UpdatedUtc = DateTime.UtcNow;
                            existing.FieldValue = p.FieldValue;
                        }
                    }
                }
                else if (fm.KategoriMember == "AKI")
                {
                    foreach (var p in fm.FormLayananBaruAki)
                    {
                        if (string.IsNullOrWhiteSpace(p.Id))
                        {
                            p.Id = Guid.NewGuid().ToString();
                        }

                        var existing = dt.DataLayananBaru.FirstOrDefault(x => x.Id == p.Id);
                        if (existing == null)
                        {
                            addedIds.Add(p.Id);
                            dt.DataLayananBaru.Add(new DataLayananBaru
                            {
                                RegistrasiId = dt.Id,
                                Id = p.Id,
                                TipeMemberAK = p.TipeMemberAK,
                                TipeMemberPartisipan = p.TipeMemberPartisipan,
                                FieldName = p.FieldName,
                                FieldType = p.FieldType,
                                FieldValue = p.FieldValue,
                                LayananBaruId = p.LayananBaruId
                            });
                        }
                        else
                        {
                            addedIds.Add(p.Id);
                            existing.UpdatedUtc = DateTime.UtcNow;
                            existing.FieldValue = p.FieldValue;
                        }
                    }
                }
                else if (fm.KategoriMember == "PARTISIPAN")
                {
                    foreach (var p in fm.FormGroupPartisipan)
                    {
                        if (string.IsNullOrWhiteSpace(p.Id))
                        {
                            p.Id = Guid.NewGuid().ToString();
                        }

                        var existing = dt.DataLayananBaru.FirstOrDefault(x => x.Id == p.Id);
                        if (existing == null)
                        {
                            addedIds.Add(p.Id);
                            dt.DataLayananBaru.Add(new DataLayananBaru
                            {
                                RegistrasiId = dt.Id,
                                Id = p.Id,
                                TipeMemberAK = p.TipeMemberAK,
                                TipeMemberPartisipan = p.TipeMemberPartisipan,
                                FieldName = p.FieldName,
                                FieldType = p.FieldType,
                                FieldValue = p.FieldValue,
                                LayananBaruId = p.LayananBaruId
                            });
                        }
                        else
                        {
                            addedIds.Add(p.Id);
                            existing.UpdatedUtc = DateTime.UtcNow;
                            existing.FieldValue = p.FieldValue;
                        }
                    }
                }
            };
            assignListLayananBaru();

            Action assignTradingMember = () =>
            {
                dt.DataTradingMember = dt.DataTradingMember ?? new List<DataTradingMember>();

                if (dt.DataTradingMember.Count() > 0)
                {
                    Db.DataTradingMember.RemoveRange(dt.DataTradingMember);
                    Db.SaveChanges();
                }

                foreach (var tradingMember in fm.DataTradingMember)
                {
                    var existing = dt.DataTradingMember.FirstOrDefault(x => x.TipeMemberAK == tradingMember && x.RegistrasiId == dt.Id);

                    if (existing != null)
                    {
                        dt.DataTradingMember.Remove(existing);
                        Db.Entry(existing).State = EntityState.Deleted;
                    }

                    dt.DataTradingMember.Add(new DataTradingMember
                    {
                        RegistrasiId = dt.Id,
                        TipeMemberAK = tradingMember
                    });
                }
            };
            assignTradingMember();

            //if new ak, add it to arms
            if (data.Status == DataAknpRequestStatus.APPROVED_STATUS.Name) {
                var castData = data as DataRegistrasi;
                try {
                    using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                        IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                        Timeout = TimeSpan.FromMinutes(5)
                    })) {
                        MakeNewArmsAnggotaId(data.KodeAk, castData.NamaPerusahaan);
                        scope.Complete();
                    }
                } catch (Exception ex) {
                    _logger.LogError(new Exception("Error occured when registering new AK to ARMS"));
                    _logger.LogError(ex);
                }
            }
        }

        /// <inheritdoc />
        public override void MapFromTo(BaseAknpData data, BaseAknpForm form) {
            base.MapFromTo(data, form);

            var fm = form.DirectCastTo<DataRegistrasiAknpForm>();
            var dt = data.DirectCastTo<DataRegistrasi>();

            fm.DaftarPemegangSaham = dt.DaftarPemegangSaham?.Select(x => new DataPemegangSahamForm {
                Id = x.Id,
                LembarSaham = x.LembarSaham,
                NominalSahamRupiah = x.NominalSahamRupiah,
                Nama = x.Nama,
                WargaNegara = x.WargaNegara,
            }).ToList();

            fm.DaftarKomisaris = dt.DaftarKomisaris?.Select(x => new DataKomisarisPersonForm {
                Id = x.Id,
                Nama = x.Nama,
                KtpFileUrl = x.KtpFileUrl,
                TtdFileUrl = x.TtdFileUrl,
                Telp = x.Telp,
                Email = x.Email,
                Jabatan = x.Jabatan,
                HP = x.HP
            }).ToList();

            fm.DaftarDireksi = dt.DaftarDireksi?.Select(x => new DataDirectorPersonForm {
                Id = x.Id,
                Nama = x.Nama,
                KtpFileUrl = x.KtpFileUrl,
                TtdFileUrl = x.TtdFileUrl,
                Telp = x.Telp,
                Email = x.Email,
                Jabatan = x.Jabatan,
                HP = x.HP
            }).ToList();

            fm.FormGroupPartisipan = dt.DataLayananBaru?.Select(x => new DataLayananBaruForm
            {
                Id = x.Id,
                TipeMemberAK = x.TipeMemberAK,
                TipeMemberPartisipan = x.TipeMemberPartisipan,
                FieldName = x.FieldName,
                FieldType = x.FieldType,
                FieldValue = x.FieldValue,
                LayananBaruId = x.LayananBaruId
            })
                .Where(x => x.TipeMemberAK == "PARTISIPAN" && x.TipeMemberPartisipan == fm.TipeMemberPartisipan)
                .ToList();

            fm.FormLayananBaruAku = dt.DataLayananBaru?.Select(x => new DataLayananBaruForm
            {
                Id = x.Id,
                TipeMemberAK = x.TipeMemberAK,
                TipeMemberPartisipan = x.TipeMemberPartisipan,
                FieldName = x.FieldName,
                FieldType = x.FieldType,
                FieldValue = x.FieldValue,
                LayananBaruId = x.LayananBaruId
            }).Where(x => x.TipeMemberAK == "AKU").ToList();

            fm.FormLayananBaruAki = dt.DataLayananBaru?.Select(x => new DataLayananBaruForm
            {
                Id = x.Id,
                TipeMemberAK = x.TipeMemberAK,
                TipeMemberPartisipan = x.TipeMemberPartisipan,
                FieldName = x.FieldName,
                FieldType = x.FieldType,
                FieldValue = x.FieldValue,
                LayananBaruId = x.LayananBaruId
            }).Where(x => x.TipeMemberAK == "AKI").ToList();

            fm.DataTradingMember = dt.DataTradingMember?.Select(x => x.TipeMemberAK).ToList();

            fm.DaftarPejabatBerwenang = dt.DaftarPejabatBerwenang?.Select(x => new DataDirectorPersonForm {
                Id = x.Id,
                Nama = x.Nama,
                KtpFileUrl = x.KtpFileUrl,
                TtdFileUrl = x.TtdFileUrl,
                Telp = x.Telp,
                Email = x.Email,
                Jabatan = x.Jabatan,
                HP = x.HP
            }).ToList();

            fm.DaftarContactPerson = dt.DaftarContactPerson?.Select(x => new DataPersonForm {
                Id = x.Id,
                Nama = x.Nama,
                Telp = x.Telp,
                Email = x.Email,
                Jabatan = x.Jabatan,
                HP = x.HP
            }).ToList();

            fm.OtherSupportDocumentsFileUrls = dt.OtherSupportDocumentsFileUrls?.Select(x => new DataSupportDocumentForm {
                Id = x.Id,
                NamaDoc = x.NamaDoc,
                DocFileUrl = x.DocFileUrl
            }).ToList();
            
        }

        /// <inheritdoc />
        public override DataAknpApproveResult Approve(string dataId, string userId) {
            var result = base.Approve(dataId, userId);

            var data = result.Data.DirectCastTo<DataRegistrasi>();
            if (result.NewStatus == DataAknpRequestStatus.APPROVED_STATUS) {
                #region administrasi keanggotaan

                var administrasiKeanggotaan = new AdministrasiKeanggotaan
                {
                    KategoriMember = data.KategoriMember,
                    KodeAK = data.KodeAk,
                    PerjanjianPortabilitySponsorFileUrl = data.PerjanjianPortabilityFileUrl,
                    Approver1UserId = data.Approver1UserId,
                    Approver2UserId = data.Approver2UserId,
                    Status = data.Status,
                    ActiveUtc = data.ActiveUtc,
                    UpdatedUtc = data.UpdatedUtc,
                    CreatedUtc = data.CreatedUtc,
                    
                };
                administrasiKeanggotaan.LayananBaru = data.DataLayananBaru.Select(e =>
                    new AdministrasiKeanggotaanLayananBaru
                    {
                        LayananBaruId = e.LayananBaruId,
                        AdministrasiKeanggotaanId = administrasiKeanggotaan.Id,
                        FieldName = e.FieldName,
                        FieldType = e.FieldType,
                        FieldValue = e.FieldValue,
                        TipeMemberAK = e.TipeMemberAK,
                        TipeMemberPartisipan = e.TipeMemberPartisipan,
                        CreatedUtc = data.CreatedUtc,
                        UpdatedUtc = data.UpdatedUtc
                    }).ToList();
                if (data.KategoriMember == RegistrasiKategoriMember.MEMBER_AKU.Name)
                {
                    administrasiKeanggotaan.PerjanjianDenganKPEIFileUrl = data.PerjanjianAkuDenganKPEIFileUrl;
                    administrasiKeanggotaan.TradingMembers =
                        data.DataTradingMember.Select(x => new AdministrasiKeanggotaanTradingMember
                        {
                            Nama = x.TipeMemberAK,
                            CreatedUtc = data.CreatedUtc,
                            UpdatedUtc = data.UpdatedUtc,
                            AdministrasiKeanggotaanId = administrasiKeanggotaan.Id
                        }).ToList();
                }
                else if (data.KategoriMember == RegistrasiKategoriMember.MEMBER_AKI.Name)
                {
                    administrasiKeanggotaan.PerjanjianDenganKPEIFileUrl = data.PerjanjianAkiDenganKPEIFileUrl;
                }
                else if (data.KategoriMember == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name)
                {
                    administrasiKeanggotaan.PerjanjianDenganKPEIFileUrl = data.PerjanjianPartisipanDenganKPEIFileUrl;
                    administrasiKeanggotaan.TipeMemberPartisipan = data.TipeMemberPartisipan;
                    if (data.TipeMemberPartisipan == "PED")
                    {
                        administrasiKeanggotaan.DokumenKhususIjinUsahaFileUrl = data.IjinUsahaFileUrl;
                        administrasiKeanggotaan.NoIjinUsahaPED = data.PED;
                        administrasiKeanggotaan.PerjanjianDenganKPEIFileUrl = data.PerjanjianPEDDenganKPEIFileUrl;
                        administrasiKeanggotaan.NamaABSponsor = data.NamaABSponsor;
                        administrasiKeanggotaan.PerjanjianPortabilitySponsorFileUrl = data.PerjanjianPEDDenganABSponsorFileUrl;
                    }
                }

                Db.AdministrasiKeanggotaan.Add(administrasiKeanggotaan);
                

                #endregion

                #region DataInformasiLain

                var newInformasiLain = new DataInformasiLain {
                    RegistrasiBaruId = data.Id,
                    Status = data.Status,
                    KodeAk = data.KodeAk,
                    ActiveUtc = data.ActiveUtc,
                    Approver1UserId = data.Approver1UserId,
                    Approver2UserId = data.Approver2UserId,
                    StatusPerusahaan = data.StatusPerusahaan,
                    NPWP = data.NPWP,
                    AnggotaBursa = data.AnggotaBursa,
                    AnggaranDasarFileUrl = data.AnggaranDasarFileUrl,
                    AktePendirianFileUrl = data.AktePendirianFileUrl,
                    AktePerubahanFileUrl = data.AktePerubahanFileUrl,
                    IjinUsahaFileUrl = data.IjinUsahaFileUrl,
                    NPWPFileUrl = data.NPWPFileUrl,
                    LaporanKeuanganFileUrl = data.LaporanKeuanganFileUrl
                    // required but not provided in registration
                };

                Db.DataInformasiLain.Add(newInformasiLain);

                #endregion

                #region TipeMember

                //var newTipeMember = new DataTipeMember {
                //    Status = data.Status,
                //    KodeAk = data.KodeAk,
                //    ActiveUtc = data.ActiveUtc,
                //    Approver1UserId = data.Approver1UserId,
                //    Approver2UserId = data.Approver2UserId,
                //    KategoriMember = data.KategoriMember,
                //    TipeMemberAK = data.TipeMemberAK,
                //};
                //newTipeMember.Partisipan = data.TipeMemberPartisipan?.Select(x => new DataTipeMemberPartisipanItem {
                //    ParentDataId = newTipeMember.Id,
                //    Nama = x.Nama,
                //    Value = x.Value
                //}).ToList();

                //Db.DataTipeMember.Add(newTipeMember);

                #endregion

                #region DataAkte

                var newAkte = new DataAkte {
                    Status = data.Status,
                    KodeAk = data.KodeAk,
                    ActiveUtc = data.ActiveUtc,
                    Approver1UserId = data.Approver1UserId,
                    Approver2UserId = data.Approver2UserId,
                    NamaPerusahaan = data.NamaPerusahaan,
                    ModalDasarRupiah = data.ModalDasarRupiah,
                    ModalDisetorRupiah = data.ModalDisetorRupiah,
                    NoAkte = data.NoAkte,
                    TglAkte = data.TglAkte,
                    Notaris = data.Notaris,
                    AkteFileUrl = data.AkteFileUrl,
                    SkOjkFileUrl = data.IjinUsahaFileUrl,
                    SkKemenkumhamFileUrl = data.AktePerubahanFileUrl,
                    // required by db models but not provided by registration
                    Ringkasan = "-",
                    JenisAkte = "Baru",
                    TglNamaBerlaku = DateTime.Now
                };
                newAkte.DaftarPemegangSaham = data.DaftarPemegangSaham?.Select(x => new DataPemegangSaham {
                    AkteId = newAkte.Id,
                    LembarSaham = x.LembarSaham,
                    NominalSahamRupiah = x.NominalSahamRupiah,
                    Nama = x.Nama,
                    WargaNegara = x.WargaNegara
                }).ToList();
                newAkte.DaftarKomisaris = data.DaftarKomisaris?.Select(x => new DataKomisaris {
                    AkteId = newAkte.Id,
                    Nama = x.Nama,
                    Email = x.Email,
                    Jabatan = x.Jabatan,
                    HP = x.HP,
                    Telp = x.Telp,
                    KtpFileUrl = x.KtpFileUrl,
                    TtdFileUrl = x.TtdFileUrl
                }).ToList();
                newAkte.DaftarDireksi = data.DaftarDireksi?.Select(x => new DataDireksi {
                    AkteId = newAkte.Id,
                    Nama = x.Nama,
                    Email = x.Email,
                    Jabatan = x.Jabatan,
                    HP = x.HP,
                    Telp = x.Telp,
                    KtpFileUrl = x.KtpFileUrl,
                    TtdFileUrl = x.TtdFileUrl
                }).ToList();

                Db.DataAkte.Add(newAkte);

                #endregion

                #region DataPejabatBerwenang

                var newPejabatBerwenang = new DataPejabatBerwenang {
                    Status = data.Status,
                    KodeAk = data.KodeAk,
                    ActiveUtc = data.ActiveUtc,
                    Approver1UserId = data.Approver1UserId,
                    Approver2UserId = data.Approver2UserId
                };
                newPejabatBerwenang.Data = data.DaftarPejabatBerwenang?.Select(x => new DataPejabatBerwenangItem {
                    ParentDataId = newPejabatBerwenang.Id,
                    Nama = x.Nama,
                    Email = x.Email,
                    Jabatan = x.Jabatan,
                    HP = x.HP,
                    Telp = x.Telp,
                    KtpFileUrl = x.KtpFileUrl,
                    TtdFileUrl = x.TtdFileUrl
                }).ToList();

                Db.DataPejabatBerwenang.Add(newPejabatBerwenang);

                #endregion

                #region DataContactPerson

                var newContactPerson = new DataContactPerson {
                    Status = data.Status,
                    KodeAk = data.KodeAk,
                    ActiveUtc = data.ActiveUtc,
                    Approver1UserId = data.Approver1UserId,
                    Approver2UserId = data.Approver2UserId
                };
                newContactPerson.Data = data.DaftarContactPerson?.Select(x => new DataContactPersonItem {
                    ParentDataId = newContactPerson.Id,
                    Nama = x.Nama,
                    Email = x.Email,
                    Jabatan = x.Jabatan,
                    HP = x.HP,
                    Telp = x.Telp
                }).ToList();

                Db.DataContactPerson.Add(newContactPerson);

                #endregion

                #region DataAlamat

                var newAlamat = new DataAlamat {
                    Status = data.Status,
                    KodeAk = data.KodeAk,
                    ActiveUtc = data.ActiveUtc,
                    Approver1UserId = data.Approver1UserId,
                    Approver2UserId = data.Approver2UserId,
                    Fax = data.Fax,
                    Gedung = data.Gedung,
                    Jalan = data.Jalan,
                    KodePos = data.KodePos,
                    Kota = data.Kota,
                    Telp = data.Telp,
                    Website = data.Website,
                    Email = data.Email,
                };

                Db.DataAlamat.Add(newAlamat);

                #endregion

                #region DataJenisUsaha

                var newJenisUsaha = new DataJenisUsaha {
                    Status = data.Status,
                    KodeAk = data.KodeAk,
                    ActiveUtc = data.ActiveUtc,
                    Approver1UserId = data.Approver1UserId,
                    Approver2UserId = data.Approver2UserId,
                    BK = data.BK,
                    BU = data.BU,
                    MI = data.MI,
                    PEE = data.PEE,
                    PPE = data.PPE
                };

                Db.DataJenisUsaha.Add(newJenisUsaha);

                #endregion

                Db.SaveChanges();
                try {

                    using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                        IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                        Timeout = TimeSpan.FromMinutes(5)
                    })) {
                        MakeNewArmsAnggotaId(data.KodeAk, data.NamaPerusahaan);
                        scope.Complete();
                    }
                } catch (Exception ex) {
                    _logger.LogError(new Exception("Error occured when registering new AK to ARMS"));
                    _logger.LogError(ex);
                }
            }
            
            return result;
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {
            throw new NotImplementedException();
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {
            throw new NotImplementedException();
        }

        private string MakeNewArmsAnggotaId(string kodeAk, string nama) {
            var newLongCode = Arms.Database.SqlQuery<string>($@"
select Kode + right('00' + cast(cast(right(LongKode, 3) as int) + 1 as varchar), 3) 
from dbo.Anggota 
where isnumeric(right(LongKode, 3)) = 1 
and Kode = '{kodeAk}'").FirstOrDefault();

            if (string.IsNullOrWhiteSpace(newLongCode)) {
                newLongCode = kodeAk + "001";
            }
            var param = new[] {
                new SqlParameter("CreatedUtc", DateTime.UtcNow),
                new SqlParameter("UpdatedUtc", DateTime.UtcNow)
            };

            Arms.Database.ExecuteSqlCommand($@"
insert into dbo.Anggota(
Kode,
LongKode,
Nama,
CreatedBy,
LastModifiedBy,
CreatedDate,
LastModifiedDate)
values(
'{kodeAk}',
'{newLongCode}',
left('{nama}', 100),
'{portalName}',
'{portalName}',
@CreatedUtc,
@UpdatedUtc)", param);

            var key = Arms.Database.SqlQuery<Int64>($@"
select AnggotaId from dbo.Anggota where Kode = '{kodeAk}' and LongKode = '{newLongCode}'
").FirstOrDefault();
            return key > 0 ? key.ToString() : null;
        }
    }
}