﻿using System;
using System.Linq;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.PerjanjianEbu;
using Kpei.PortalAK.DataAccess.DbModels;
using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.ModSynchronize.Models;
using Kpei.PortalAK.DataAccess.Services;
using System.Data.SqlClient;
using System.Transactions;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataPerjanjianEbuAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-perjanjian-ebu";

        /// <inheritdoc />
        public DataPerjanjianEbuAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) { }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Keanggotaan EBU";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => false;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataPerjanjianEBU);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataPerjanjianEbuAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => true;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataPerjanjianEBU.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataPerjanjianEBU.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataPerjanjianEBU>();
            Db.DataPerjanjianEBU.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {
            return null;
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {

            #region Get Necessary Data

            var utcNow = DateTime.UtcNow;
            var syncData = GetLatestSynchronous(DataSlug, SyncFlow.ARMS_PORTAL_FLOW, SyncAction.INSERT_ACTION);
            var minDate = syncData != null ? $" and a1.LastModifiedDate > @SyncDate" : "";
            SqlParameter[] param;
            
            if (syncData != null) {
                param = new[] {
                    new SqlParameter("UtcNow", utcNow.AddHours(7)),
                    new SqlParameter("SyncDate", syncData.SyncUtc.AddHours(7))
                };
            } else {
                param = new[] {
                    new SqlParameter("UtcNow", utcNow.AddHours(7)),
                };
            }

            #endregion

            IEnumerable<DataPerjanjianEBU> dats;
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {

                #region query

                dats = Arms.Database.SqlQuery<DataPerjanjianEBU>($@"
select 
a2.Kode as KodeAK, 
isnull(nullif(a1.NoSimO, ''), '-') as NoSimO,
a1.TglSimO as TglSimOUtc,
isnull(nullif(a1.NoPerjanjian, ''), '-') as NoPerjanjian,
isnull(nullif(a1.JenisPerjanjian, ''), '-') as JenisPerjanjian,
a1.TglPerjanjianAwal as TglSejakUtc,
a1.TglPerjanjianAkhir as TglSampaiUtc,
isnull(nullif(a1.Status, ''), '-') as StatusPerjanjian,
a1.TglAktif as TglAktif,
a1.Keterangan as Keterangan,
a1.CreatedDate as CreatedUtc,
a1.LastModifiedDate as UpdatedUtc,
a1.CreatedDate as ActiveUtc,
'Approved' as 'Status'
from dbo.HistoriAnggotaEbu as a1
inner join dbo.Anggota as a2 on a1.AnggotaId = a2.AnggotaId

                where a1.LastModifiedDate <= @UtcNow " + minDate, param).ToList();
                scope.Complete();

                #endregion

            }
            

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {
                foreach (var dat in dats) {
                    var data = dat as DataPerjanjianEBU;

                    #region WIB to UTC conversion

                    data.CreatedUtc = data.CreatedUtc.AddHours(-7);
                    data.ActiveUtc = data.ActiveUtc?.AddHours(-7);
                    data.UpdatedUtc = data.UpdatedUtc.AddHours(-7);

                    data.TglSimOUtc = data.TglSimOUtc?.AddHours(-7);
                    data.TglSampaiUtc = data.TglSampaiUtc?.AddHours(-7);
                    data.TglSejakUtc = data.TglSejakUtc?.AddHours(-7);

                    #endregion

                    Db.DataPerjanjianEBU.Add(data);
                }
                Db.SaveChanges();
                scope.Complete();
            }

            if (dats.Count() > 0) {
                WriteSynchronize(DataSlug, SyncFlow.ARMS_PORTAL_FLOW, SyncAction.INSERT_ACTION, utcNow);
            }
            return dats;
        }
    }
}
