﻿using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.SPAK;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSynchronize.Models;
using Kpei.PortalAK.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {

    public class DataSpakAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-spak";

        /// <inheritdoc />
        public DataSpakAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) { }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "SPAK";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => false;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataSPAK);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataSpakAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => true;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataSPAK.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataSPAK.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataSPAK>();
            Db.DataSPAK.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {
            throw new NotImplementedException();
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {

            #region Get Necessary Data
            
            var utcNow = DateTime.UtcNow;
            var syncData = GetLatestSynchronous(DataSlug, SyncFlow.ARMS_PORTAL_FLOW, SyncAction.INSERT_ACTION);
            var minDate = syncData != null ? $" and a1.LastModifiedDate > @SyncDate" : "";
            SqlParameter[] param;
            if (syncData != null) {
                param = new[] {
                    new SqlParameter("UtcNow", utcNow.AddHours(7)),
                    new SqlParameter("SyncDate", syncData.SyncUtc.AddHours(7))
                };
            } else {
                param = new[] {
                    new SqlParameter("UtcNow", utcNow.AddHours(7)),
                };
            }
            IEnumerable<DataSPAK> dats;

            #endregion

            #region query 

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {
                dats = Arms.Database.SqlQuery<DataSPAK>($@"
select 
a2.Kode as KodeAK, 
isnull(nullif(a1.NoSpak, ''), '-') as NoSPAK,
a1.TglSpak as TanggalSPAKUtc,
a1.CreatedDate as CreatedUtc,
a1.LastModifiedDate as UpdatedUtc,
a1.CreatedDate as ActiveUtc,
'Approved' as 'Status'
from dbo.HistoriAnggotaSpak as a1
inner join dbo.Anggota as a2 on a1.AnggotaId = a2.AnggotaId

                where a1.LastModifiedDate <= @UtcNow " + minDate, param).ToList();
                scope.Complete();
            }

            #endregion 

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {
                foreach (var dat in dats) {
                    var data = dat as DataSPAK;

                    #region WIB to UTC conversion
                    
                    data.CreatedUtc = data.CreatedUtc.AddHours(-7);
                    data.ActiveUtc = data.ActiveUtc?.AddHours(-7);
                    data.UpdatedUtc = data.UpdatedUtc.AddHours(-7);

                    data.TanggalSPAKUtc = data.TanggalSPAKUtc?.AddHours(-7);

                    #endregion

                    Db.DataSPAK.Add(data);
                }
                Db.SaveChanges();
                scope.Complete();
            }

            if (dats.Count() > 0) WriteSynchronize(DataSlug, SyncFlow.ARMS_PORTAL_FLOW, SyncAction.INSERT_ACTION, utcNow);
            return dats;
        }
    }
}
