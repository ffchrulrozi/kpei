﻿using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.Akte;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSynchronize.Models;
using Kpei.PortalAK.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataAkteAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-akta";

        public const string oldSLUG = "data-akte";

        /// <inheritdoc />
        public DataAkteAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) {
            AlternateDataSlug.Add(oldSLUG);
        }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Akta";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => true;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataAkte);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataAkteAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => false;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataAkte.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataAkte.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataAkte>();
            Db.DataAkte.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        /// <inheritdoc />
        public override void MapFromTo(BaseAknpForm form, BaseAknpData data) {
            base.MapFromTo(form, data);

            var fm = form.DirectCastTo<DataAkteAknpForm>();
            var dt = data.DirectCastTo<DataAkte>();

            var addedIds = new List<string>();

            #region assign list pemegang saham

            if (dt.DaftarPemegangSaham == null) {
                dt.DaftarPemegangSaham = new List<DataPemegangSaham>();
            }

            foreach (var p in fm.ListPemegangSaham) {
                if (string.IsNullOrWhiteSpace(p.Id)) {
                    p.Id = Guid.NewGuid().ToString();
                }
                var existing = dt.DaftarPemegangSaham.FirstOrDefault(x => x.Id == p.Id);
                if (existing == null) {
                    addedIds.Add(p.Id);
                    dt.DaftarPemegangSaham.Add(new DataPemegangSaham {
                        AkteId = dt.Id,
                        Id = p.Id,
                        LembarSaham = p.LembarSaham,
                        Nama = p.Nama,
                        NominalSahamRupiah = p.NominalSahamRupiah,
                        WargaNegara = p.WargaNegara
                    });
                } else {
                    addedIds.Add(p.Id);
                    existing.UpdatedUtc = DateTime.UtcNow;
                    existing.LembarSaham = p.LembarSaham;
                    existing.Nama = p.Nama;
                    existing.NominalSahamRupiah = p.NominalSahamRupiah;
                    existing.WargaNegara = p.WargaNegara;
                }
            }
            var rems1 = dt.DaftarPemegangSaham.Where(x => !addedIds.Contains(x.Id)).ToList();
            foreach (var rem in rems1) {
                dt.DaftarPemegangSaham.Remove(rem);
                Db.Entry(rem).State = EntityState.Deleted;
            }

            addedIds.Clear();

            #endregion

            #region assign list komisaris

            if (dt.DaftarKomisaris == null) {
                dt.DaftarKomisaris = new List<DataKomisaris>();
            }

            foreach (var p in fm.ListKomisaris) {
                if (string.IsNullOrWhiteSpace(p.Id)) {
                    p.Id = Guid.NewGuid().ToString();
                }

                var existing = dt.DaftarKomisaris.FirstOrDefault(x => x.Id == p.Id);
                if (existing == null) {
                    addedIds.Add(p.Id);
                    dt.DaftarKomisaris.Add(new DataKomisaris {
                        AkteId = dt.Id,
                        Id = p.Id,
                        Nama = p.Nama,
                        Email = p.Email,
                        HP = p.HP,
                        Jabatan = p.Jabatan,
                        Telp = p.Telp,
                        TtdFileUrl = p.TtdFileUrl,
                        KtpFileUrl = p.KtpFileUrl
                    });
                } else {
                    addedIds.Add(p.Id);
                    existing.UpdatedUtc = DateTime.UtcNow;
                    existing.Nama = p.Nama;
                    existing.Email = p.Email;
                    existing.HP = p.HP;
                    existing.Jabatan = p.Jabatan;
                    existing.Telp = p.Telp;
                    existing.TtdFileUrl = p.TtdFileUrl;
                    existing.KtpFileUrl = p.KtpFileUrl;
                }
            }

            var rems2 = dt.DaftarKomisaris.Where(x => !addedIds.Contains(x.Id)).ToList();
            foreach (var rem in rems2) {
                dt.DaftarKomisaris.Remove(rem);
                Db.Entry(rem).State = EntityState.Deleted;
            }

            addedIds.Clear();

            #endregion

            #region assign list direksi

            if (dt.DaftarDireksi == null) {
                dt.DaftarDireksi = new List<DataDireksi>();
            }

            foreach (var p in fm.ListDireksi) {
                if (string.IsNullOrWhiteSpace(p.Id)) {
                    p.Id = Guid.NewGuid().ToString();
                }

                var existing = dt.DaftarDireksi.FirstOrDefault(x => x.Id == p.Id);
                if (existing == null) {
                    addedIds.Add(p.Id);
                    dt.DaftarDireksi.Add(new DataDireksi {
                        AkteId = dt.Id,
                        Id = p.Id,
                        Nama = p.Nama,
                        Email = p.Email,
                        HP = p.HP,
                        Jabatan = p.Jabatan,
                        Telp = p.Telp,
                        TtdFileUrl = p.TtdFileUrl,
                        KtpFileUrl = p.KtpFileUrl
                    });
                } else {
                    addedIds.Add(p.Id);
                    existing.UpdatedUtc = DateTime.UtcNow;
                    existing.Nama = p.Nama;
                    existing.Email = p.Email;
                    existing.HP = p.HP;
                    existing.Jabatan = p.Jabatan;
                    existing.Telp = p.Telp;
                    existing.TtdFileUrl = p.TtdFileUrl;
                    existing.KtpFileUrl = p.KtpFileUrl;
                }
            }

            var rems3 = dt.DaftarDireksi.Where(x => !addedIds.Contains(x.Id)).ToList();
            foreach (var rem in rems3) {
                dt.DaftarDireksi.Remove(rem);
                Db.Entry(rem).State = EntityState.Deleted;
            }

            addedIds.Clear();

            #endregion


        }

        /// <inheritdoc />
        public override void MapFromTo(BaseAknpData data, BaseAknpForm form) {
            base.MapFromTo(data, form);
            var fm = form.DirectCastTo<DataAkteAknpForm>();
            var dt = data.DirectCastTo<DataAkte>();
            if (dt.DaftarPemegangSaham == null) dt.DaftarPemegangSaham = new List<DataPemegangSaham>();

            fm.ListPemegangSaham = dt.DaftarPemegangSaham.Select(x => new DataPemegangSahamForm {
                Id = x.Id,
                Nama = x.Nama,
                LembarSaham = x.LembarSaham,
                NominalSahamRupiah = x.NominalSahamRupiah,
                WargaNegara = x.WargaNegara
            }).ToList();

            if (dt.DaftarKomisaris == null) dt.DaftarKomisaris = new List<DataKomisaris>();

            fm.ListKomisaris = dt.DaftarKomisaris.Select(x => new DataKomisarisPersonForm {
                Id = x.Id,
                Nama = x.Nama,
                TtdFileUrl = x.TtdFileUrl,
                KtpFileUrl = x.KtpFileUrl,
                Email = x.Email,
                Telp = x.Telp,
                Jabatan = x.Jabatan,
                HP = x.HP
            }).ToList();

            if (dt.DaftarDireksi == null) dt.DaftarDireksi = new List<DataDireksi>();

            fm.ListDireksi = dt.DaftarDireksi.Select(x => new DataDirectorPersonForm {
                Id = x.Id,
                Nama = x.Nama,
                TtdFileUrl = x.TtdFileUrl,
                KtpFileUrl = x.KtpFileUrl,
                Email = x.Email,
                Telp = x.Telp,
                Jabatan = x.Jabatan,
                HP = x.HP
            }).ToList();
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {
            var utcNow = DateTime.UtcNow;
            var syncData = GetLatestSynchronous(DataSlug, SyncFlow.ARMS_PORTAL_FLOW, SyncAction.INSERT_ACTION);
            
            #region Get Necessary Data to Sync

            //from portal
            var dbData = Db.Database.SqlQuery<PortalAkte>("Select * from dbo.Sync_AkteAnggota").ToList();
            var dbNamaData = Db.Database.SqlQuery<PortalAkte>("Select * from dbo.Sync_NamaAnggota").ToList();
            var dbDireksi = Db.Database.SqlQuery<PortalDireksi>("Select * from dbo.Sync_Akte_Direksi").ToList();
            var dbKomisaris = Db.Database.SqlQuery<PortalKomisaris>("Select * from dbo.Sync_Akte_Komisaris").ToList();
            var dbPemSaham = Db.Database.SqlQuery<PortalPemSaham>("Select * from dbo.Sync_Akte_PemegangSaham").ToList();
            var dats = dbData;
            
            //from arms
            var targetDataId = dats.Select(x => x.Id).ToList();
            var dbAnggota = GetArmsAnggota();
            var dbNotaris = GetForeignNotaris();

            #endregion

            foreach (var dat in dats.ToList()) {
                var anggotaId = dbAnggota.Where(e => e.Kode == dat.KodeAk)?.OrderByDescending(e => e.CreatedDate)?.FirstOrDefault()?.AnggotaId;

                #region define ARMS Flag
                long namaLatestKey = 0;
                long akteLatestKey = 0;
                long komisarisLatestKey = 0;
                long direksiLatestKey = 0;
                long pemsahamLatestKey = 0;
                #endregion

                var data = dat;
                if (anggotaId == 0 || anggotaId == null) continue;

                //Get Notaris Id from ARMS, if not exists, enter new Notaris
                #region Notaris

                var notarisKey = dbNotaris.Where(e => e.Nama == data.Notaris).OrderByDescending(e => e.CreatedDate).FirstOrDefault()?.NotarisId;
                if (notarisKey == 0 || notarisKey == null) {
                    var paramNotaris = new SqlParameter[] {
                        new SqlParameter("TglSttd", utcNow.AddHours(7)),
                        new SqlParameter("CreatedUtc", utcNow.AddHours(7)),
                        new SqlParameter("UpdatedUtc", utcNow.AddHours(7))
                    };
                    using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                        IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                        Timeout = TimeSpan.FromDays(7)
                    })) {
                        notarisKey = Arms.Database.SqlQuery<Int64>($@"INSERT INTO dbo.FndNotaris
(
Nama, 
TglSttd, 
CreatedBy,
LastModifiedBy,
CreatedDate,
LastModifiedDate
)
output NotarisId
values(
    left('{data.Notaris}', 100), 
    @TglSttd,
    '{portalName}',
    '{portalName}',
    @CreatedUtc,
    @UpdatedUtc
    )
", paramNotaris).FirstOrDefault();
                        scope.Complete();
                    }
                }

                #endregion
                
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromDays(7)
                })) {

                    #region Insert Akte

                    //populate Akte Parameter
                    int lastVersion = dat.LastVersion;
                    var paramTglAkte = new SqlParameter("TglAkte", SqlDbType.DateTime) {
                        Value = data.TglAkte ?? (Object)DBNull.Value,
                        IsNullable = true
                    };
                    var paramAkte = new SqlParameter[] {
                        new SqlParameter("TglAkte", SqlDbType.DateTime){
                            Value = data.TglAkte ?? (Object) DBNull.Value,
                            IsNullable = true
                        },
                        new SqlParameter("CreatedUtc", data.CreatedUtc.AddHours(7)),
                        new SqlParameter("UpdatedUtc", data.UpdatedUtc.AddHours(7)),
                        new SqlParameter("ModalDasar", data.ModalDasarRupiah),
                        new SqlParameter("ModalDisetor", data.ModalDisetorRupiah)
                    };

                    var insertAkteSql = $@"INSERT INTO dbo.AkteAnggota
                    (
                          AnggotaId,
                          NoAkte,
                          TglAkte,
                          NotarisId,
                          Ringkasan,
                          ModalDasar,
                          ModalSetor,
                          ChangeNamaFlag,
                          ChangeModalFlag,
                          ChangePemegangSahamFlag,
                          ChangeKomisarisFlag,
                          ChangeDireksiFlag,
                          CreatedBy,
                          LastModifiedBy,
                          CreatedDate,
                          LastModifiedDate
                    )
                    output inserted.AkteAnggotaId
                    values(
                        {anggotaId},
                        left('{data.NoAkte}', 20),
                        @TglAkte,
                        {notarisKey},
                        left('{data.Ringkasan}', 8000),
                        @ModalDasar,
                        @ModalDisetor,
                        '1',
                        '1',
                        '1',
                        '1',
                        '1',
                        '{portalName}',
                        '{portalName}',
                        @CreatedUtc,
                        @UpdatedUtc
                    )";
                    var akteKey = Arms.Database.SqlQuery<Int64>(insertAkteSql, paramAkte).FirstOrDefault();

                    #endregion Akte

                    if (data.LastVersion == 1) {
                        akteLatestKey = akteKey;
                    }

                    if (akteKey > 0) {
                        #region Nama Perusahaan
                        var namaData = dbNamaData.Where(e => e.KodeAk == data.KodeAk && e.NoAkte == data.NoAkte && e.TglAkte == data.TglAkte).FirstOrDefault();

                        if (namaData != null) {
                            var paramName = new SqlParameter[] {
                                new SqlParameter("AkteAnggotaId", akteKey),
                                new SqlParameter("CreatedUtc", namaData.CreatedUtc.AddHours(7)),
                                new SqlParameter("UpdatedUtc", namaData.UpdatedUtc.AddHours(7)),
                                new SqlParameter("TglBerlaku", namaData.TglNamaBerlaku.AddHours(7))
                            };

                            //Nama Perusahaan
                            Arms.Database.ExecuteSqlCommand($@"INSERT INTO dbo.HistoriAkteNama
                            (
                                  AkteAnggotaId,                          
                                  Nama,
                                  TglBerlaku,
                                  CreatedBy,
                                  LastModifiedBy,
                                  CreatedDate,
                                  LastModifiedDate,
                                  LastVersion
                            )
output inserted.HistoriAkteNamaId
                            values(
                                @AkteAnggotaId,
                                left('{namaData.NamaPerusahaan}', 100),
                                @TglBerlaku,
                                '{portalName}',
                                '{portalName}',
                                @CreatedUtc,
                                @UpdatedUtc,
                                '{namaData.LastVersion}')", paramName);

                            if (namaData.LastVersion == 1) {
                                namaLatestKey = akteKey;
                            }
                        }


                        #endregion

                        #region Data Komisaris

                        if (dbKomisaris != null) {
                            var curKomisaris = dbKomisaris.Where(e => e.AkteId == data.Id).ToList();
                            foreach (var komisaris in curKomisaris) {

                                var paramKomisaris = new SqlParameter[] {
                                new SqlParameter("AkteAnggotaId", akteKey),
                                new SqlParameter("CreatedUtc", SqlDbType.DateTime){
                                    Value = utcNow.AddHours(7)
                                },
                                new SqlParameter("UpdatedUtc", SqlDbType.DateTime) {
                                    Value = utcNow.AddHours(7)
                                }
                            };

                                Arms.Database.ExecuteSqlCommand($@"INSERT INTO dbo.HistoriAkteKomisaris
                            (
                                  AkteAnggotaId,    
                                  Seq,
                                  Nama,
                                  Jabatan,
                                  CreatedBy,
                                  LastModifiedBy,
                                  CreatedDate,
                                  LastModifiedDate,
                                  LastVersion
                            )
                            values(
                                @AkteAnggotaId,
                                '0',
                                left('{komisaris.Nama}', 100),
                                left('{komisaris.Jabatan}', 50),
                                '{portalName}',
                                '{portalName}',
                                @CreatedUtc,
                                @UpdatedUtc,
                                '{komisaris.LastVersion}')", paramKomisaris);

                                if (komisaris.LastVersion == 1) {
                                    komisarisLatestKey = akteKey;
                                }
                            }
                        }

                        #endregion
                        
                        #region Direksi
                        if (dbDireksi != null) {

                            var curDireksi = dbDireksi.Where(e => e.AkteId == data.Id).ToList();
                            foreach (var direksi in curDireksi) {

                                var paramDireksi = new SqlParameter[] {
                                new SqlParameter("AkteAnggotaId", akteKey),
                                new SqlParameter("CreatedUtc", SqlDbType.DateTime){
                                    Value = direksi.CreatedUtc.AddHours(7)
                                },
                                new SqlParameter("UpdatedUtc", SqlDbType.DateTime) {
                                    Value = direksi.UpdatedUtc.AddHours(7)
                                }
                            };

                                Arms.Database.ExecuteSqlCommand($@"INSERT INTO dbo.HistoriAkteDireksi
                            (
                                  AkteAnggotaId,    
                                  Seq,
                                  Nama,
                                  Jabatan,
                                  CreatedBy,
                                  LastModifiedBy,
                                  CreatedDate,
                                  LastModifiedDate,
                                  LastVersion
                            )
output inserted.HistoriAkteDireksiId
                            values(
                                @AkteAnggotaId,
                                '0',
                                left('{direksi.Nama}', 100),
                                left('{direksi.Jabatan}', 50),
                                '{portalName}',
                                '{portalName}',
                                @CreatedUtc,
                                @UpdatedUtc,
                                '{direksi.LastVersion}')", paramDireksi);

                                if (direksi.LastVersion == 1) {
                                    direksiLatestKey = akteKey;
                                }
                            }

                        }

                        #endregion

                        #region Pemegang Saham

                        if (dbPemSaham != null) {

                            var curPemSaham = dbPemSaham.Where(e => e.AkteId == data.Id).ToList();
                            foreach (var pemSaham in curPemSaham) {

                                var paramPemSaham = new[] {
                                new SqlParameter("AkteAnggotaId", akteKey),
                                new SqlParameter("CreatedUtc", SqlDbType.DateTime){
                                    Value = utcNow.AddHours(7)
                                },
                                new SqlParameter("UpdatedUtc", SqlDbType.DateTime) {
                                    Value = utcNow.AddHours(7)
                                },
                                new SqlParameter("LembarSaham", pemSaham.LembarSaham),
                                new SqlParameter("NominalSaham", pemSaham.NominalSahamRupiah),
                                new SqlParameter("Pct", pemSaham.LembarSaham / dbPemSaham.Sum(e => e.LembarSaham))
                            };

                                Arms.Database.ExecuteSqlCommand($@"INSERT INTO dbo.HistoriAktePemegangSaham
                            (
                                  AkteAnggotaId,    
                                  Nama,
                                  WargaNegara,
                                  JumlahLembarSaham,
                                  Nominal,
                                  Pct,
                                  CreatedBy,
                                  LastModifiedBy,
                                  CreatedDate,
                                  LastModifiedDate,
                                  LastVersion
                            )
                            values(
                                @AkteAnggotaId,
                                left('{pemSaham.Nama}', 100),
                                left('{pemSaham.WargaNegara}', 3),
                                @LembarSaham,
                                @NominalSaham,
                                @Pct,
                                '{portalName}',
                                '{portalName}',
                                @CreatedUtc,
                                @UpdatedUtc,
                                '{pemSaham.LastVersion}')", paramPemSaham);

                                if (pemSaham.LastVersion == 1) {
                                    pemsahamLatestKey = akteKey;
                                }
                            }
                        }
                        #endregion
                    }

                    #region Update ARMS Flag according to changes

                    Arms.Database.ExecuteSqlCommand($@"
    update dbo.AkteAnggota
set 
ChangeNamaFlag = {(namaLatestKey != 0 ? 1 : 0)},
ChangeModalFlag = 1,
ChangePemegangSahamFlag = {(pemsahamLatestKey != 0 ? 1 : 0)},
ChangeKomisarisFlag = {(komisarisLatestKey != 0 ? 1 : 0)},
ChangeDireksiFlag = {(direksiLatestKey != 0 ? 1 : 0)}
where AkteAnggotaId = {akteKey}
");
                    #endregion

                    scope.Complete();
                }
            }

            if (dats.Count > 0) {
                var dbList = new List<UpdateAnggota>();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromDays(7)
                })) {

                    #region Get All Anggota with latest version based on other tables 

                    dbList = Arms.Database.SqlQuery<UpdateAnggota>($@"
select
AnggotaId, 
Kode,
(select top(1) tnama.AkteAnggotaId
	from dbo.AkteAnggota as tnama
	inner join(
		select AkteAnggotaId from dbo.HistoriAkteNama as tnama2 
		where LastVersion = 1
		)as tnama2 on tnama.AkteAnggotaId = tnama2.AkteAnggotaId
		where tnama.AnggotaId = tbase.AnggotaId
		order by CreatedDate desc, LastModifiedDate desc
	) as Nama,

	(select top(1) tdireksi.AkteAnggotaId
	from dbo.AkteAnggota as tdireksi
	inner join(
		select AkteAnggotaId from dbo.HistoriAkteDireksi as tdireksi2 
		where LastVersion = 1
		)as tdireksi2 on tdireksi.AkteAnggotaId = tdireksi2.AkteAnggotaId
		where tdireksi.AnggotaId = tbase.AnggotaId
		order by CreatedDate desc, LastModifiedDate desc
	) as Direksi,

	(select top(1) tkomisaris.AkteAnggotaId
	from dbo.AkteAnggota as tkomisaris
	inner join(
		select AkteAnggotaId from dbo.HistoriAkteKomisaris as tkomisaris2 
		where LastVersion = 1
		)as tkomisaris2 on tkomisaris.AkteAnggotaId = tkomisaris2.AkteAnggotaId
		where tkomisaris.AnggotaId = tbase.AnggotaId
		order by CreatedDate desc, LastModifiedDate desc
	) as Komisaris,

	(select top(1) tpemsaham.AkteAnggotaId
	from dbo.AkteAnggota as tpemsaham
	inner join(
		select AkteAnggotaId from dbo.HistoriAktePemegangSaham as tpemsaham2 
		where LastVersion = 1
		)as tpemsaham2 on tpemsaham.AkteAnggotaId = tpemsaham2.AkteAnggotaId
		where tpemsaham.AnggotaId = tbase.AnggotaId
		order by CreatedDate desc, LastModifiedDate desc
	) as Pemsaham,

	(select top(1) modal.AkteAnggotaId
	from dbo.AkteAnggota as modal
	where modal.AnggotaId = tbase.AnggotaId
	order by CreatedDate desc, LastModifiedDate desc
	) as Modal
from dbo.Anggota as tbase
").ToList();
                    #endregion

                    scope.Complete();
                }

                #region Update Anggota with latest version
                foreach (var ak in dbData.Select(e => e.KodeAk).Distinct()) {
                    if (dbList.Select(e => e.Kode).Contains(ak)) {
                        var data = dbList.Where(e => e.Kode == ak)?.FirstOrDefault();
                        Arms.Database.ExecuteSqlCommand($@"
update dbo.Anggota
set
LastAkteNamaId = {data.Nama ?? 0},
LastAkteModalId = {data.Modal ?? 0},
LastAktePemegangSahamId = {data.Pemsaham ?? 0},
LastAkteKomisarisId = {data.Komisaris ?? 0},
LastAkteDireksiId = {data.Direksi ?? 0}
where Anggotaid = {data.AnggotaId}
");

                        #region nama

                        Arms.Database.ExecuteSqlCommand($@"
update dbo.HistoriAkteNama set LastVersion = 0 where AkteAnggotaId in (
	select AkteAnggotaId from dbo.AkteAnggota as akte
	inner join(
		select top(1) * from dbo.Anggota where Kode = '{ak}' order by CreatedDate desc, LastModifiedDate asc
	)as anggota on akte.AnggotaId = anggota.AnggotaId and LastAkteNamaId != akte.AkteAnggotaId
)");

                        #endregion

                        #region komisaris

                        Arms.Database.ExecuteSqlCommand($@"
update dbo.HistoriAkteKomisaris set LastVersion = 0 where AkteAnggotaId in (
	select AkteAnggotaId from dbo.AkteAnggota as akte
	inner join(
		select top(1) * from dbo.Anggota where Kode = '{ak}' order by CreatedDate desc, LastModifiedDate asc
	)as anggota on akte.AnggotaId = anggota.AnggotaId and LastAkteKomisarisId != akte.AkteAnggotaId
)");

                        #endregion

                        #region direksi

                        Arms.Database.ExecuteSqlCommand($@"
update dbo.HistoriAkteDireksi set LastVersion = 0 where AkteAnggotaId in (
	select AkteAnggotaId from dbo.AkteAnggota as akte
	inner join(
		select top(1) * from dbo.Anggota where Kode = '{ak}' order by CreatedDate desc, LastModifiedDate asc
	)as anggota on akte.AnggotaId = anggota.AnggotaId and LastAkteDireksiId != akte.AkteAnggotaId
)");

                        #endregion

                        #region pemegang saham

                        Arms.Database.ExecuteSqlCommand($@"
update dbo.HistoriAktePemegangSaham set LastVersion = 0 where AkteAnggotaId in (
	select AkteAnggotaId from dbo.AkteAnggota as akte
	inner join(
		select top(1) * from dbo.Anggota where Kode = '{ak}' order by CreatedDate desc, LastModifiedDate asc
	)as anggota on akte.AnggotaId = anggota.AnggotaId and LastAktePemegangSahamId != akte.AkteAnggotaId
)
");

                        #endregion
                    }
                }

                #endregion
            }

            #region Write to Synchronize Table if there's data to sync
            if (dats.Count > 0 || dbNamaData.Count > 0 || dbDireksi.Count > 0 || dbKomisaris.Count > 0 || dbPemSaham.Count > 0) {
                WriteSynchronize(DataSlug, SyncFlow.PORTAL_ARMS_FLOW, SyncAction.INSERT_ACTION, utcNow);
            }

            #endregion

            #region return
            if (dats.Count > 0)
                return dbData.DirectCastTo<IEnumerable<BaseAknpData>>();
            else if (dbNamaData.Count > 0)
                return dbNamaData.DirectCastTo<IEnumerable<BaseAknpData>>();
            else if (dbDireksi.Count > 0)
                return dbDireksi.DirectCastTo<IEnumerable<BaseAknpData>>();
            else if (dbKomisaris.Count > 0)
                return dbKomisaris.DirectCastTo<IEnumerable<BaseAknpData>>();
            else
                return dbPemSaham.DirectCastTo<IEnumerable<BaseAknpData>>();
            #endregion

        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {
            throw new NotImplementedException();
        }
    }
}