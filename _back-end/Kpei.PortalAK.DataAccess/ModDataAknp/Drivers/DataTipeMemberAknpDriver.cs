﻿using System;
using System.Linq;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.TipeMember;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using System.Collections.Generic;
using System.Data.Entity;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataTipeMemberAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-tipe-member";

        /// <inheritdoc />
        public DataTipeMemberAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) { }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Tipe Member";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => false;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataTipeMember);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataTipeMemberAknpForm);

        public override bool HideFromMenu => true;
        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => false;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataTipeMember.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataTipeMember.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataTipeMember>();
            Db.DataTipeMember.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        public override void MapFromTo(BaseAknpData data, BaseAknpForm form) {
            base.MapFromTo(data, form);
            var fm = form.DirectCastTo<DataTipeMemberAknpForm>();
            var dt = data.DirectCastTo<DataTipeMember>();

            if (dt.Partisipan == null) {
                dt.Partisipan = new List<DataTipeMemberPartisipanItem>();
            }

            fm.TipeMemberPartisipan = dt.Partisipan.Select(x => new DataPartisipanForm {
                Id = x.Id,
                Nama = x.Nama,
                Value = x.Value
            }).ToList();
        }
        public override void MapFromTo(BaseAknpForm form, BaseAknpData data) {
            base.MapFromTo(form, data);

            var fm = form.DirectCastTo<DataTipeMemberAknpForm>();
            var dt = data.DirectCastTo<DataTipeMember>();

            if (dt.Partisipan == null) {
                dt.Partisipan = new List<DataTipeMemberPartisipanItem>();
            }
            
            var addedIdsPartisipan = new List<string>();

            #region Multiple Member Partisipan

            if (dt.KategoriMember == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name) {
                foreach (var p in fm.TipeMemberPartisipan) {
                    if (string.IsNullOrWhiteSpace(p.Id)) {
                        p.Id = Guid.NewGuid().ToString();
                    }

                    var existing = dt.Partisipan.FirstOrDefault(x => x.Id == p.Id);
                    if (existing == null) {
                        addedIdsPartisipan.Add(p.Id);
                        dt.Partisipan.Add(new DataTipeMemberPartisipanItem {
                            ParentDataId = dt.Id,
                            Id = p.Id,
                            Nama = p.Nama,
                            Value = p.Value
                        });
                    } else {
                        addedIdsPartisipan.Add(p.Id);
                        existing.UpdatedUtc = DateTime.UtcNow;
                        existing.Nama = p.Nama;
                        existing.Value = p.Value;
                    }
                }
            }
            

            #endregion
            
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {
            throw new NotImplementedException();
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {
            throw new NotImplementedException();
        }
    }
}
