﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.StatusKpei;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSynchronize.Models;
using Kpei.PortalAK.DataAccess.Services;
using System.Data.SqlClient;
using System.Transactions;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataStatusKpeiAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-status-kpei";

        /// <inheritdoc />
        public DataStatusKpeiAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) { }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Status KPEI";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => false;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataStatusKPEI);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataStatusKpeiAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => true;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataStatusKPEI.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataStatusKPEI.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }
        public BaseAknpData GetLatestActiveData(string kodeAk, string administrasiKeanggotaanId) {
            var q = BaseQueryInternal();
            var utcNow = DateTime.UtcNow;
            q = q.Where($"{nameof(BaseAknpData.Status)} == @0", DataAknpRequestStatus.APPROVED_STATUS.Name)
                .Where($"{nameof(BaseAknpData.Draft)} == @0", false)
                .Where($"{nameof(BaseAknpData.KodeAk)} == @0", kodeAk)
                .Where($"{nameof(BaseAknpData.ActiveUtc)} != null")
                .Where($"{nameof(BaseAknpData.ActiveUtc)} <= @0", utcNow)
                .Where($"{nameof(DataStatusKPEI.AdministrasiKeanggotaanId)} == @0", administrasiKeanggotaanId)
                .OrderBy($"{nameof(BaseAknpData.CreatedUtc)} desc, {nameof(BaseAknpData.ActiveUtc)} desc");
            var dat = q.FirstOrDefault();
            return dat;
        }

        public AdministrasiKeanggotaan getLatestAdministrasiKeanggotaanActiveData(string kodeAk) {
            var q = Db.AdministrasiKeanggotaan.AsQueryable();
            var utcNow = DateTime.UtcNow;
            q = q.Where($"{nameof(AdministrasiKeanggotaan.Status)} == @0", DataAknpRequestStatus.APPROVED_STATUS.Name)
                .Where($"{nameof(AdministrasiKeanggotaan.KodeAK)} == @0", kodeAk)
                .Where($"{nameof(AdministrasiKeanggotaan.Draft)} == @0", false)
                .Where($"{nameof(AdministrasiKeanggotaan.UserDraft)} == null")
                .Where($"{nameof(AdministrasiKeanggotaan.ActiveUtc)} != null")
                .Where($"{nameof(AdministrasiKeanggotaan.ActiveUtc)} <= @0", utcNow);
            q = q.Where(c => c.KategoriMember.Contains(RegistrasiKategoriMember.MEMBER_AK.Name));

            q = q.OrderBy($"{nameof(AdministrasiKeanggotaan.CreatedUtc)} desc, {nameof(AdministrasiKeanggotaan.ActiveUtc)} desc");
            var dat = q.FirstOrDefault();
            
            return dat;
        }
        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataStatusKPEI>();
            var administrasiKeanggotaan = getLatestAdministrasiKeanggotaanActiveData(dat.KodeAk);
            dat.AdministrasiKeanggotaanId = administrasiKeanggotaan?.Id;
            Db.DataStatusKPEI.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {
            throw new NotImplementedException();
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {

            #region Get Necessary Data

            var utcNow = DateTime.UtcNow;
            var syncData = GetLatestSynchronous(DataSlug, SyncFlow.ARMS_PORTAL_FLOW, SyncAction.INSERT_ACTION);
            var minDate = syncData != null ? $" and a1.LastModifiedDate > @SyncDate" : "";
            SqlParameter[] param;
            if (syncData != null) {
                param = new[] {
                    new SqlParameter("UtcNow", utcNow),
                    new SqlParameter("SyncDate", syncData.SyncUtc)
                };
            } else {
                param = new[] {
                    new SqlParameter("UtcNow", utcNow),
                };
            }
            IEnumerable<DataStatusKPEI> dats;

            #endregion

            #region query

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {
                dats = Arms.Database.SqlQuery<DataStatusKPEI>($@"
select 
a2.Kode as KodeAK, 
isnull(nullif(a1.NoDokumen, ''), '-') as NoDokumen,
isnull(nullif(a3.Nama, ''), '-') as StatusKPEI,
isnull(nullif(a4.Nama, ''), '-') as SubStatusAnggotaKPEI,
a1.TglStatusAwal as TanggalAwalStatusUtc,
a1.TglStatusAkhir as TanggalAkhirStatusUtc,
a1.TglAktif as TanggalAktifUtc,
a1.Alasan as Alasan,
a1.Keterangan as Keterangan,
a1.CreatedDate as CreatedUtc,
a1.LastModifiedDate as UpdatedUtc,
a1.CreatedDate as ActiveUtc,
'Approved' as 'Status'
from dbo.HistoriAnggotaStatusKpei as a1
inner join dbo.Anggota as a2 on a1.AnggotaId = a2.AnggotaId
left join dbo.FndStatusAnggota as a3 on a3.StatusAnggotaId = a1.StatusAnggotaId
left join dbo.FndSubStatusAnggota as a4 on a4.SubStatusAnggotaId = a1.SubStatusAnggotaId

                where a1.LastModifiedDate <= @UtcNow " + minDate, param).ToList();
                scope.Complete();
            }

            #endregion

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {
                foreach (var dat in dats) {
                    var data = dat as DataStatusKPEI;

                    #region WIB to UTC conversion
                    
                    data.CreatedUtc = data.CreatedUtc.AddHours(-7);
                    data.ActiveUtc = data.ActiveUtc?.AddHours(-7);
                    data.UpdatedUtc = data.UpdatedUtc.AddHours(-7);

                    data.TanggalAkhirStatusUtc = data.TanggalAkhirStatusUtc?.AddHours(-7);
                    data.TanggalAktifUtc = data.TanggalAktifUtc?.AddHours(-7);
                    data.TanggalAwalStatusUtc = data.TanggalAwalStatusUtc?.AddHours(-7);

                    #endregion

                    Db.DataStatusKPEI.Add(data);
                }
                Db.SaveChanges();
                scope.Complete();
            }
            WriteSynchronize(DataSlug, SyncFlow.ARMS_PORTAL_FLOW, SyncAction.INSERT_ACTION, utcNow);
            return dats;
        }
    }
}
