﻿using System;
using System.Collections.Generic;
using System.Linq;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.InformasiLain;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSynchronize.Models;
using Kpei.PortalAK.DataAccess.Services;
using System.Data.SqlClient;
using System.Data;
using System.Transactions;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataInformasiLainAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-informasi-lain";

        /// <inheritdoc />
        public DataInformasiLainAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) { }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Informasi Lain";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => false;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataInformasiLain);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataInformasiLainAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => false;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataInformasiLain.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataInformasiLain.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataInformasiLain>();
            Db.DataInformasiLain.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {

            #region Get Necessary Data

            var utcNow = DateTime.UtcNow;
            var dbData = GetRecordToSync(DataSlug, utcNow, SyncFlow.PORTAL_ARMS_FLOW, SyncAction.INSERT_ACTION, offset, limit);
            var dats = dbData.ToList();
            var castDats = dats.Select(e => e.KodeAk).ToList();
            var tglMenjadiAk = Db.DataPerjanjian.Where(e => castDats.Contains(e.KodeAk)).ToList();
            var dbAnggota = GetArmsAnggota();
            var dbPemantau = GetForeignPemantau();

            #endregion
            
            #region Reset Last Version in ARMS

            var aks = dbData.Select(e => e.KodeAk).Distinct();
            foreach (var ak in aks) {
                var anggotaId = dbAnggota.Where(e => e.Kode == ak)?.OrderByDescending(e => e.CreatedDate)?.FirstOrDefault()?.AnggotaId;
                if (anggotaId != 0 && anggotaId != null) {
                    Arms.Database.ExecuteSqlCommand(@"DECLARE	@return_value int
                    EXEC    @return_value = [dbo].[uspResetLastVersionHistoriAnggotaInformasiLain]
                            @AnggotaId = " + anggotaId);
                }
            }

            #endregion

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {
                foreach (var dat in dats) {
                    var data = dat as DataInformasiLain;
                    var anggotaId = dbAnggota.Where(e => e.Kode == dat.KodeAk)?.OrderByDescending(e => e.CreatedDate)?.FirstOrDefault()?.AnggotaId;
                    if (anggotaId == 0 || anggotaId == null) continue;
                    
                    #region Get Notaris Foreign Key

                    var foreignKey = dbPemantau.Where(e => e.Kode == (data.Pemantau ?? "N/A"))?.FirstOrDefault()?.PemantauId;
                    if (foreignKey == 0 || foreignKey == null) {
                        var fkParam = new SqlParameter[] {
                            new SqlParameter("CreatedUtc", utcNow.AddHours(7)),
                            new SqlParameter("UpdatedUtc", utcNow.AddHours(7))
                        };

                        Arms.Database.ExecuteSqlCommand($@"Insert into dbo.FndPemantau (
                            Kode,
                            Extention,
                            CreatedBy, 
                            LastModifiedBy, 
                            CreatedDate, 
                            LastModifiedDate)
                        values(
                            '{data.Pemantau ?? "N/A"}', 
                            '{data.Pemantau ?? "N/A"}',
                            '{portalName}', 
                            '{portalName}', 
                            @CreatedUtc,
                            @UpdatedUtc
                        )", fkParam);

                        dbPemantau = GetForeignPemantau();
                        foreignKey = dbPemantau.Where(e => e.Kode == (data.Pemantau ?? "N/A"))?.FirstOrDefault()?.PemantauId;
                    }

                    #endregion

                    #region Building Parameter

                    int lastVersion =
                        (DateTime.Compare(
                            dat.CreatedUtc,
                            dats.Where(e => e.KodeAk == dat.KodeAk).Max(x => x.CreatedUtc)) == 0 &&
                        DateTime.Compare(
                            dat.UpdatedUtc,
                            dats.Where(e => e.KodeAk == dat.KodeAk).Max(x => x.UpdatedUtc)) == 0 ?
                        1 : 0); // 0 value if same

                    var sqlParam = new SqlParameter[] {new SqlParameter("CreatedUtc", SqlDbType.DateTime) {
                        Value = dat.CreatedUtc
                    },
                    new SqlParameter("UpdatedUtc", SqlDbType.DateTime) {
                        Value = dat.UpdatedUtc.AddHours(7)
                    },
                    new SqlParameter("TglSerahDokumen", SqlDbType.DateTime){
                        Value = data?.TglSerahDokumenUtc?.AddHours(7) ?? (Object) DBNull.Value,
                        IsNullable = true
                    },
                    new SqlParameter("TglSerahDanaMinCash", SqlDbType.DateTime){
                        Value = (object) data.TglSerahDanaMinCashUtc?.AddHours(7) ?? (Object) DBNull.Value,
                        IsNullable = true
                    },
                    new SqlParameter("TglAktePengikatan", SqlDbType.DateTime){
                        Value = (object) data.TglAktaPengikatanUtc?.AddHours(7) ?? (Object) DBNull.Value,
                        IsNullable = true
                    },
                    new SqlParameter("TglRekomendasi", SqlDbType.DateTime){
                        Value = (object) data.TglRekomendasiUtc?.AddHours(7) ?? (Object) DBNull.Value,
                        IsNullable = true
                    },
                    new SqlParameter("TglMenjadiAk", SqlDbType.DateTime){
                        Value = (object) tglMenjadiAk.Where(e => e.KodeAk == data.KodeAk)?.OrderByDescending(e => e.CreatedUtc).FirstOrDefault()?.TglPerjanjianUtc?.AddHours(7) ?? utcNow
                    }
                };

                    #endregion
                    
                    #region query 

                    Arms.Database.ExecuteSqlCommand($@"INSERT INTO dbo.HistoriAnggotaInformasiLain
                (
                        AnggotaId,
                        TglMenjadiAk,
                        PemantauId,
                        Npwp,
                        StatusPerusahaan,
                        TglSerahDokumen,
                        TglSerahDanaMinCash,
                        TglAktePengikatan,
                        TglRekomendasi,
                        CreatedBy,
                        LastModifiedBy,
                        CreatedDate,
                        LastModifiedDate,
                        LastVersion
                )
                values(
                    '{anggotaId}',
                    @TglMenjadiAk,
                    '{foreignKey}',
                    left('{data.NPWP}', 50),
                    left('{data.StatusPerusahaan}', 20),
                    @TglSerahDokumen,
                    @TglSerahDanaMinCash,
                    @TglAktePengikatan,
                    @TglRekomendasi,
                    '{portalName}',
                    '{portalName}',
                    @CreatedUtc,
                    @UpdatedUtc,
                    '{lastVersion}'
                )", sqlParam);

                    #endregion
                }
                scope.Complete();
            }
            if (dats.Count > 0) {
                WriteSynchronize(DataSlug, SyncFlow.PORTAL_ARMS_FLOW, SyncAction.INSERT_ACTION, utcNow);
            }
            return dats;
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {
            return null;
        }
    }
}
