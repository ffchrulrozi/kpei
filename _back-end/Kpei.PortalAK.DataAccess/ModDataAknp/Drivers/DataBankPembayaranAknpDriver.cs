﻿using System;
using System.Collections.Generic;
using System.Linq;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.BankPembayaran;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSynchronize.Models;
using Kpei.PortalAK.DataAccess.Services;
using System.Data.SqlClient;
using System.Data;
using System.Transactions;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataBankPembayaranAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-bank-pembayaran";

        /// <inheritdoc />
        public DataBankPembayaranAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) { }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Bank Pembayaran";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => false;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataBankPembayaran);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataBankPembayaranAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => false;

        public override bool HideFromMenu => true;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataBankPembayaran.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataBankPembayaran.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataBankPembayaran>();
            Db.DataBankPembayaran.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {
            var utcNow = DateTime.UtcNow;
            var dbData = GetRecordToSync(DataSlug, utcNow, SyncFlow.PORTAL_ARMS_FLOW, SyncAction.INSERT_ACTION, offset, limit);
            var dats = dbData.DirectCastTo<IEnumerable<BaseAknpData>>().ToList();

            #region Get Necessary Data

            var dbAnggota = GetArmsAnggota();
            var dbBank = GetForeignBank();

            #endregion

            #region Reset Last Version in ARMS

            var aks = dbData.Select(e => e.KodeAk).Distinct();
            foreach (var ak in aks) {
                var anggotaId = dbAnggota.Where(e => e.Kode == ak)?.OrderByDescending(e => e.CreatedDate)?.FirstOrDefault()?.AnggotaId;
                if (anggotaId != 0 && anggotaId != null) {
                    Arms.Database.ExecuteSqlCommand(@"DECLARE	@return_value int
                    EXEC    @return_value = [dbo].[uspResetLastVersionHistoriAnggotaBank]
                            @AnggotaId = " + anggotaId);
                }
            }

            #endregion

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {
                foreach (var dat in dats) {
                    #region Get Bank Pembayaran Foreign Key

                    var data = dat as DataBankPembayaran;
                    var anggotaId = dbAnggota.Where(e => e.Kode == dat.KodeAk)?.OrderByDescending(e => e.CreatedDate)?.FirstOrDefault()?.AnggotaId;
                    if (anggotaId == 0 || anggotaId == null) continue;
                    var foreignKey = dbBank.Where(e => e.Nama == data.BankPembayaran)?.OrderByDescending(e => e.CreatedDate)?.FirstOrDefault()?.BankPembayaranId;
                    if (foreignKey == 0 || foreignKey == null) {
                        var paramForeign = new SqlParameter[] {
                            new SqlParameter("CreatedUtc", utcNow.AddHours(7)),
                            new SqlParameter("UpdatedUtc", utcNow.AddHours(7))
                        };
                        

                        #region query 

                        Arms.Database.ExecuteSqlCommand($@"Insert into dbo.FndBankPembayaran(
                            Nama, 
                            PerjanjianSelesai, 
                            CreatedBy, 
                            LastModifiedBy, 
                            CreatedDate, 
                            LastModifiedDate
                            )values(
                            left('{ data.BankPembayaran }', 50), 
                           '1',
                           '{portalName}', 
                           '{portalName}', 
                            @CreatedUtc, 
                            @UpdatedUtc)"
                            , paramForeign);
                        dbBank = GetForeignBank();
                        foreignKey = dbBank.Where(e => e.Nama == data.BankPembayaran).OrderByDescending(e => e.CreatedDate).FirstOrDefault().BankPembayaranId;

                        #endregion
                    }

                    #endregion


                    #region Building Parameter

                    int lastVersion =
                        (DateTime.Compare(
                            data.CreatedUtc,
                            dats.Where(e => e.KodeAk == dat.KodeAk).Max(x => x.CreatedUtc)) == 0 &&
                        DateTime.Compare(
                            data.UpdatedUtc,
                            dats.Where(e => e.KodeAk == dat.KodeAk).Max(x => x.UpdatedUtc)) == 0 ?
                        1 : 0); // 0 value if same

                    var param = new[] {
                        new SqlParameter("CreatedUtc", SqlDbType.DateTime) {
                            Value = data.CreatedUtc.AddHours(7)
                        },
                        new SqlParameter("UpdatedUtc", SqlDbType.DateTime) {
                            Value = data.UpdatedUtc.AddHours(7)
                        }
                    };

                    #endregion

                    #region query 

                    Arms.Database.ExecuteSqlCommand($@"INSERT INTO dbo.HistoriAnggotaBank
                    (
                            AnggotaId,
                            BankPembayaranId,
                            CreatedBy,
                            LastModifiedBy,
                            CreatedDate,
                            LastModifiedDate,
                            LastVersion
                    )
                    values(
                        '{anggotaId}',
                        '{foreignKey}',
                        '{portalName}',
                        '{portalName}',
                        @CreatedUtc,
                        @UpdatedUtc,
                        '{lastVersion}'
                    )", param);

                    #endregion
                }
                scope.Complete();
            }
            
            if (dats.Count > 0) {
                WriteSynchronize(DataSlug, SyncFlow.PORTAL_ARMS_FLOW, SyncAction.INSERT_ACTION, utcNow);
            }
            return dats;
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {
            throw new NotImplementedException();
        }
    }
}
