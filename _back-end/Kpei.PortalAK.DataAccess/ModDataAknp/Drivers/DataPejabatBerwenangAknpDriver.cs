﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.PejabatBerwenang;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSynchronize.Models;
using Kpei.PortalAK.DataAccess.Services;
using System.Data.SqlClient;
using System.Data;
using System.Transactions;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataPejabatBerwenangAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-pejabat-berwenang";

        /// <inheritdoc />
        public DataPejabatBerwenangAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) { }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Pejabat Berwenang";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => false;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataPejabatBerwenang);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataPejabatBerwenangAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => false;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataPejabatBerwenang.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataPejabatBerwenang.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataPejabatBerwenang>();
            Db.DataPejabatBerwenang.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        /// <inheritdoc />
        public override void MapFromTo(BaseAknpForm form, BaseAknpData data) {
            base.MapFromTo(form, data);

            var fm = form.DirectCastTo<DataPejabatBerwenangAknpForm>();
            var dt = data.DirectCastTo<DataPejabatBerwenang>();

            if (dt.Data == null) {
                dt.Data = new List<DataPejabatBerwenangItem>();
            }

            var addedIds = new List<string>();

            foreach (var p in fm.ListPejabatBerwenang) {
                if (string.IsNullOrWhiteSpace(p.Id)) {
                    p.Id = Guid.NewGuid().ToString();
                }

                var existing = dt.Data.FirstOrDefault(x => x.Id == p.Id);
                if (existing == null) {
                    addedIds.Add(p.Id);
                    dt.Data.Add(new DataPejabatBerwenangItem {
                        ParentDataId = dt.Id,
                        Id = p.Id,
                        Nama = p.Nama,
                        Email = p.Email,
                        HP = p.HP,
                        Jabatan = p.Jabatan,
                        Telp = p.Telp,
                        TtdFileUrl = p.TtdFileUrl,
                        KtpFileUrl = p.KtpFileUrl
                    });
                } else {
                    addedIds.Add(p.Id);
                    existing.UpdatedUtc = DateTime.UtcNow;
                    existing.Nama = p.Nama;
                    existing.Email = p.Email;
                    existing.HP = p.HP;
                    existing.Jabatan = p.Jabatan;
                    existing.Telp = p.Telp;
                    existing.TtdFileUrl = p.TtdFileUrl;
                    existing.KtpFileUrl = p.KtpFileUrl;
                }
            }

            var rems = dt.Data.Where(x => !addedIds.Contains(x.Id)).ToList();
            foreach (var rem in rems) {
                dt.Data.Remove(rem);
                Db.Entry(rem).State = EntityState.Deleted;
            }
        }

        /// <inheritdoc />
        public override void MapFromTo(BaseAknpData data, BaseAknpForm form) {
            base.MapFromTo(data, form);

            var fm = form.DirectCastTo<DataPejabatBerwenangAknpForm>();
            var dt = data.DirectCastTo<DataPejabatBerwenang>();

            if (dt.Data == null) {
                dt.Data = new List<DataPejabatBerwenangItem>();
            }

            fm.ListPejabatBerwenang = dt.Data.Select(x => new DataDirectorPersonForm {
                Id = x.Id,
                Nama = x.Nama,
                TtdFileUrl = x.TtdFileUrl,
                KtpFileUrl = x.KtpFileUrl,
                Email = x.Email,
                Telp = x.Telp,
                Jabatan = x.Jabatan,
                HP = x.HP
            }).ToList();
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {

            #region Get Necessary Data

            var utcNow = DateTime.UtcNow;
            var dbData = GetRecordToSync(DataSlug, utcNow, SyncFlow.PORTAL_ARMS_FLOW, SyncAction.INSERT_ACTION, offset, limit);
            var dats = dbData.ToList();
            var targetDataId = dats.Select(x => x.Id).ToList();
            var dbItem = Db.DataPejabatBerwenangItem.Where(e => targetDataId.Contains(e.ParentDataId)).ToList();
            var dbAnggota = GetArmsAnggota();

            #endregion

            #region Reset Latest Version in ARMS

            var aks = dbData.Select(e => e.KodeAk).Distinct();
            foreach (var ak in aks) {
                var anggotaId = dbAnggota.Where(e => e.Kode == ak)?.OrderByDescending(e => e.CreatedDate)?.FirstOrDefault()?.AnggotaId;
                if (anggotaId != 0 && anggotaId != null) {
                    Arms.Database.ExecuteSqlCommand(@"DECLARE	@return_value int
                    EXEC    @return_value = [dbo].[uspResetLastVersionHistoriAnggotaPejabat]
                            @AnggotaId = " + anggotaId);
                }
            }

            #endregion

            foreach (var dat in dats.ToList()) {
                var anggotaId = dbAnggota.Where(e => e.Kode == dat.KodeAk)?.OrderByDescending(e => e.CreatedDate)?.FirstOrDefault()?.AnggotaId;
                var data = dat as DataPejabatBerwenang;
                if (anggotaId == 0 || anggotaId == null) continue;
                var curItem = dbItem.Where(e => e.ParentDataId == data.Id).ToList();

                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromDays(7)
                })) {
                    foreach (var item in curItem) {

                        #region Building Parameter

                        int lastVersion =
                        (DateTime.Compare(
                            data.CreatedUtc,
                            dats.Where(e => e.KodeAk == dat.KodeAk).Max(x => x.CreatedUtc)) == 0 &&
                        DateTime.Compare(
                            data.UpdatedUtc,
                            dats.Where(e => e.KodeAk == dat.KodeAk).Max(x => x.UpdatedUtc)) == 0 ?
                        1 : 0); // 0 value if same

                        var param = new[] {
                            new SqlParameter("CreatedUtc", SqlDbType.DateTime) {
                                Value = data.CreatedUtc.AddHours(7)
                            },
                            new SqlParameter("UpdatedUtc", SqlDbType.DateTime) {
                                Value = data.UpdatedUtc.AddHours(7)
                            }
                        };

                        #endregion

                        #region query

                        Arms.Database.ExecuteSqlCommand($@"INSERT INTO dbo.HistoriAnggotaPejabat
                        (
                              AnggotaId,
                              Seq,
                              Nama,
                              CreatedBy,
                              LastModifiedBy,
                              CreatedDate,
                              LastModifiedDate,
                              LastVersion
                        )
                        values(
                            '{anggotaId}',
                            '0',
                            left('{item.Nama}', 100),
                            '{portalName}',
                            '{portalName}',
                            @CreatedUtc,
                            @UpdatedUtc,
                            '{lastVersion}'
                        )", param);

                        #endregion
                    }
                    scope.Complete();
                }
            }
            if (dats.Count > 0) {
                WriteSynchronize(DataSlug, SyncFlow.PORTAL_ARMS_FLOW, SyncAction.INSERT_ACTION, utcNow);
            }
            return dats;
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {
            throw new NotImplementedException();
        }
    }

}