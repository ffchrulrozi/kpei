﻿using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.Perjanjian;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSynchronize.Models;
using Kpei.PortalAK.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataPerjanjianAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-perjanjian";

        /// <inheritdoc />
        public DataPerjanjianAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) { }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Perjanjian AK";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => false;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataPerjanjian);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataPerjanjianAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => true;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataPerjanjian.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataPerjanjian.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataPerjanjian>();
            Db.DataPerjanjian.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {
            throw new NotImplementedException();
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {

            #region Get Necessary Data

            var utcNow = DateTime.UtcNow;
            var syncData = GetLatestSynchronous(DataSlug, SyncFlow.ARMS_PORTAL_FLOW, SyncAction.INSERT_ACTION);
            var minDate = syncData != null ? $" and a1.LastModifiedDate > @SyncDate" : "";
            SqlParameter[] param;
            if (syncData != null) {
                param = new[] {
                    new SqlParameter("UtcNow", utcNow.AddHours(7)),
                    new SqlParameter("SyncDate", syncData.SyncUtc.AddHours(7))
                };
            } else {
                param = new[] {
                    new SqlParameter("UtcNow", utcNow.AddHours(7)),
                };
            }
            IEnumerable<DataPerjanjian> dats;

            #endregion

            #region query 

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {
                dats = Arms.Database.SqlQuery<DataPerjanjian>($@" 
select 
a2.Kode as KodeAK, 
isnull(nullif(a1.NoPerjanjian, ''), '-') as NoPerjanjian,
isnull(nullif(a1.JenisPerjanjian, ''), '-') as JenisPerjanjian,
a1.TglPerjanjian as TglPerjanjianUtc,
a1.CreatedDate as CreatedUtc,
a1.LastModifiedDate as UpdatedUtc,
a1.CreatedDate as ActiveUtc,
'Approved' as 'Status'
from dbo.HistoriAnggotaPerjanjian as a1
inner join dbo.Anggota as a2 on a1.AnggotaId = a2.AnggotaId


                where a1.CreatedDate <= @UtcNow " + minDate, param).ToList();

                scope.Complete();
            }

            #endregion

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.Serializable,
                Timeout = TimeSpan.FromDays(7)
            })) {

                foreach (var dat in dats) {
                    var data = dat as DataPerjanjian;

                    #region WIB to UTC conversion

                    data.CreatedUtc = data.CreatedUtc.AddHours(-7);
                    data.ActiveUtc = data.ActiveUtc?.AddHours(-7);
                    data.UpdatedUtc = data.UpdatedUtc.AddHours(-7);

                    data.TglPerjanjianUtc = data.TglPerjanjianUtc?.AddHours(-7);

                    #endregion

                    Db.DataPerjanjian.Add(data);
                }
                Db.SaveChanges();
                scope.Complete();
            }
        
            if(dats.Count() > 0) WriteSynchronize(DataSlug, SyncFlow.ARMS_PORTAL_FLOW, SyncAction.INSERT_ACTION, utcNow);
            return dats;
        }
    }
}
