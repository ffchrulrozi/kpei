﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.ContactPerson;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Drivers {
    public class DataContactPersonAknpDriver : BaseAknpDriver {
        public const string SLUG = "data-contact-person";

        /// <inheritdoc />
        public DataContactPersonAknpDriver(AppDbContext db, ArmsDbContext arms, ISysLogger logger) : base(db, arms, logger) { }

        /// <inheritdoc />
        public override string DataSlug => SLUG;

        /// <inheritdoc />
        public override string DataTitle => "Contact Person";

        /// <inheritdoc />
        public override bool RequiresAkteForChange => false;

        /// <inheritdoc />
        public override Type DataClassType => typeof(DataContactPerson);

        /// <inheritdoc />
        public override Type FormClassType => typeof(DataContactPersonAknpForm);

        /// <inheritdoc />
        public override bool EnabledOnlyForKpei => false;

        /// <inheritdoc />
        public override BaseAknpData GetById(string dataId) {
            return Db.DataContactPerson.Find(dataId);
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> BaseQueryInternal() {
            return Db.DataContactPerson.AsQueryable();
        }

        /// <inheritdoc />
        protected override IQueryable<BaseAknpData> ApplyFilterQueryInternal(IQueryable<BaseAknpData> currentQuery,
            BaseAknpDttRequestForm req) {
            return null;
        }

        /// <inheritdoc />
        protected override BaseAknpData CreateInternal(BaseAknpData aknpData) {
            var dat = aknpData.DirectCastTo<DataContactPerson>();
            Db.DataContactPerson.Add(dat);
            Db.SaveChanges();
            return dat;
        }

        /// <inheritdoc />
        public override void MapFromTo(BaseAknpForm form, BaseAknpData data) {
            base.MapFromTo(form, data);

            var fm = form.DirectCastTo<DataContactPersonAknpForm>();
            var dt = data.DirectCastTo<DataContactPerson>();

            if (dt.Data == null) {
                dt.Data = new List<DataContactPersonItem>();
            }

            var addedIds = new List<string>();

            foreach (var p in fm.ListContactPerson) {
                if (string.IsNullOrWhiteSpace(p.Id)) {
                    p.Id = Guid.NewGuid().ToString();
                }

                var existing = dt.Data.FirstOrDefault(x => x.Id == p.Id);
                if (existing == null) {
                    addedIds.Add(p.Id);
                    dt.Data.Add(new DataContactPersonItem {
                        ParentDataId = dt.Id,
                        Id = p.Id,
                        Nama = p.Nama,
                        Email = p.Email,
                        HP = p.HP,
                        Jabatan = p.Jabatan,
                        Telp = p.Telp
                    });
                } else {
                    addedIds.Add(p.Id);
                    existing.UpdatedUtc = DateTime.UtcNow;
                    existing.Nama = p.Nama;
                    existing.Email = p.Email;
                    existing.HP = p.HP;
                    existing.Jabatan = p.Jabatan;
                    existing.Telp = p.Telp;
                }
            }

            var rems = dt.Data.Where(x => !addedIds.Contains(x.Id)).ToList();
            foreach (var rem in rems) {
                dt.Data.Remove(rem);
                Db.Entry(rem).State = EntityState.Deleted;
            }
        }

        /// <inheritdoc />
        public override void MapFromTo(BaseAknpData data, BaseAknpForm form) {
            base.MapFromTo(data, form);
            var fm = form.DirectCastTo<DataContactPersonAknpForm>();
            var dt = data.DirectCastTo<DataContactPerson>();

            if (dt.Data == null) {
                dt.Data = new List<DataContactPersonItem>();
            }

            fm.ListContactPerson = dt.Data.Select(x => new DataPersonForm {
                Id = x.Id,
                Nama = x.Nama,
                Email = x.Email,
                Telp = x.Telp,
                Jabatan = x.Jabatan,
                HP = x.HP
            }).ToList();
        }

        public override IEnumerable<BaseAknpData> SyncToArms(int offset, int limit) {
            throw new NotImplementedException();
        }

        public override IEnumerable<BaseAknpData> SyncToPortal() {
            throw new NotImplementedException();
        }
    }
}