﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.Perjanjian {
    public class DataPerjanjianAknpForm : BaseAknpFormWithSuratKeterangan{
        [Display(Name = "No. Perjanjian", GroupName = "Perjanjian")]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        public string NoPerjanjian { get; set; }

        [Display(Name = "Jenis Perjanjian", GroupName = "Perjanjian")]
        [AknpDisplay("_jenisPerjanjian", false, false)]
        [Required]
        [MaxLength(128)]
        public string JenisPerjanjian { get; set; }

        [Display(Name = "Tanggal Perjanjian", GroupName = "Perjanjian")]
        [AknpDisplay(null, true, false)]
        [Required]
        public DateTime? TglPerjanjianUtc { get; set; }

        [Display(Name = "File Surat Perjanjian Penitipan Efek untuk Dipinjamkan", GroupName = "Perjanjian")]
        [AknpDisplay(null, false, false, ShowOnlyForKpei = true)]
        [MaxLength(256)]
        public string PerjanjianTitipEfekUntukPinjamFileUrl { get; set; }
    }
}
