﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.PerjanjianKbos {
    public class DataPerjanjianKbosAknpForm : BaseAknpFormWithSuratKeterangan {
        [Display(Name = "Nomor SPM", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        public string NoSPM { get; set; }

        [Display(Name = "Nomor Perjanjian", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        public string NoPerjanjian { get; set; }

        [Display(Name = "Jenis Perjanjian", GroupName = "Perjanjian KBOS")]
        [AknpDisplay("_jenisPerjanjianKbos", true, false)]
        [Required]
        [MaxLength(128)]
        public string JenisPerjanjian { get; set; }

        [Display(Name = "Tanggal SPM", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, false)]
        [Required]
        public DateTime? TglSPMUtc { get; set; }

        [Display(Name = "Status", GroupName = "Perjanjian KBOS")]
        [AknpDisplay("_statusKbos", true, false)]
        [Required]
        [MaxLength(128)]
        public string StatusKBOS { get; set; }

        [Display(Name = "Tanggal Aktif", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, false)]
        public DateTime? TanggalAktifUtc { get; set; }

        [Display(Name = "Keterangan", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, false)]
        [MaxLength(512)]
        public string Keterangan { get; set; }

        [Display(Name = "Liquidity Provider KB", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, false)]
        [Required]
        [MaxLength(128)]
        public string LPKB { get; set; }

        [Display(Name = "Liquidity Provider OS", GroupName = "Perjanjian KBOS")]
        [AknpDisplay(null, true, false)]
        [Required]
        [MaxLength(128)]
        public string LPOS { get; set; }
    }
}
