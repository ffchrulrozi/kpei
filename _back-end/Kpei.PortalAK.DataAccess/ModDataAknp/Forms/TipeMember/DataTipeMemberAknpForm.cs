﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.TipeMember {
    public class DataTipeMemberAknpForm : BaseAknpFormWithSuratKeterangan {
        [Display(Name = "Kategori", GroupName = "Member")]
        [AknpDisplay("_kategoriMember", true, true)]
        [Required]
        [MaxLength(128)]
        public string KategoriMember { get; set; }

        [Display(Name = "Tipe Member (AK)", GroupName = "Member")]
        [AknpDisplay("_tipeMemberAK", true, false)]
        [MaxLength(128)]
        public string TipeMemberAK { get; set; }

        private List<DataPartisipanForm> _tipeMemberPartisipan;

        [Display(Name = "Tipe Member (Partisipan)", GroupName = "Member", Order = 1)]
        [AknpDisplay(null, false, false)]
        [Required]
        [CollectionLength(MinLength = 1)]
        [CollectionItemMustValid]
        public List<DataPartisipanForm> TipeMemberPartisipan {
            get {
                _tipeMemberPartisipan = _tipeMemberPartisipan == null
                    ? _tipeMemberPartisipan = new List<DataPartisipanForm>()
                    : _tipeMemberPartisipan = _tipeMemberPartisipan
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.Nama)).ToList();
                return _tipeMemberPartisipan;
            }
            set { _tipeMemberPartisipan = value; }
        }
    }
}
