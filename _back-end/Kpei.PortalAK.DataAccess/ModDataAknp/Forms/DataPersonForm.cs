﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms {
    public class DataPersonForm {

        public DataPersonForm() {
            Id = Guid.NewGuid().ToString();
        }

        [MaxLength(128)]
        public string Id { get; set; }

        private string _nama;

        [Required]
        [MaxLength(128)]
        public string Nama {
            get { return _nama?.Trim(); }
            set { _nama = value; }
        }

        [Required]
        [MaxLength(128)]
        public string Jabatan { get; set; }

        [Required]
        [MaxLength(128)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MaxLength(128)]
        public string Telp { get; set; }

        [Required]
        [MaxLength(128)]
        public string HP { get; set; }
    }
}