﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Kpei.PortalAK.DataAccess.DbModels;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms {
    public class DataPartisipanForm {
        public DataPartisipanForm() {
            Id = Guid.NewGuid().ToString();
        }
        [MaxLength(128)]
        public string Id { get; set; }

        private string _nama;

        [Required]
        [MaxLength(128)]
        public string Nama {
            get { return _nama?.Trim(); }
            set { _nama = value; }
        }
        
        public bool Value { get; set; }

        public ICollection<DataLayananBaruForm> DataLayananBaruForm { get; set; }
    }
}
