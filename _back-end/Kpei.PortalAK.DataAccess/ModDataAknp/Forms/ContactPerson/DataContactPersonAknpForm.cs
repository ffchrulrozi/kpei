﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using Kpei.PortalAK.DataAccess.Validators;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.ContactPerson {
    public class DataContactPersonAknpForm : BaseAknpFormWithSuratKeterangan {
        private List<DataPersonForm> _listContactPerson;

        [Display(Name = "Daftar Contact Person", GroupName = "Contact Person", Order = 1)]
        [AknpDisplay(null, false, false)]
        [Required]
        [CollectionLength(MinLength = 1)]
        [CollectionItemMustValid]
        public List<DataPersonForm> ListContactPerson {
            get {
                _listContactPerson = _listContactPerson == null
                    ? _listContactPerson = new List<DataPersonForm>()
                    : _listContactPerson = _listContactPerson
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.Nama)).ToList();
                return _listContactPerson;
            }
            set { _listContactPerson = value; }
        }
    }
}