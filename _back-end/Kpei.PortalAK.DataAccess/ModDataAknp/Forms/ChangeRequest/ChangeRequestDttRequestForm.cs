﻿using System;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.ChangeRequest {
    public class ChangeRequestDttRequestForm : DttRequestForm {
        public string KodeAk { get; set; }
        public string DataSlug { get; set; }
        public string Status { get; set; }
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }
        public DateTime? ActiveFromDateTime { get; set; }

        public DateTime? ActiveToDateTime { get; set; }
    }
}