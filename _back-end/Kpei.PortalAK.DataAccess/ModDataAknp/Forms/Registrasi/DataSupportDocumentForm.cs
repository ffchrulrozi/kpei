﻿using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.Registrasi {
    public class DataSupportDocumentForm {
        [MaxLength(128)]
        public string Id { get; set; }

        [Required]
        [MaxLength(256)]
        public string DocFileUrl { get; set; }

        [Required]
        [MaxLength(256)]
        public string NamaDoc { get; set; }
    }
}