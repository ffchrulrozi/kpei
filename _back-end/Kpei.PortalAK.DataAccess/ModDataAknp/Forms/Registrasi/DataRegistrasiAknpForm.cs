﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.Akte;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.Validators;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.Registrasi {
    public class DataRegistrasiAknpForm : BaseAknpForm {

        #region screen 1

        // AK atau Partisipan
        [Display(Name = "Kategori Member", GroupName = "Member", Order = 8)]
        [AknpDisplay("_kategoriMember", true, false)]
        [Required]
        [MaxLength(128)]
        public string KategoriMember { get; set; }

        [Display(Name = "Tipe Member Partisipan", GroupName = "Member", Order = 10)]
        [AknpDisplay("_tipeMemberPartisipan", false, false)]
        [MaxLength(128)]
        [CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateTipeMemberPartisipan))]
        public string TipeMemberPartisipan { get; set; }

        public static ValidationResult ValidateTipeMemberPartisipan(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;
            if (form.KategoriMember == "PARTISIPAN" && string.IsNullOrWhiteSpace(form.TipeMemberPartisipan))
            {
                return new ValidationResult("Anda belum memilih Tipe Member Partisipan", new[] { valctx.MemberName });
            }
            return success;
        }

        private List<DataLayananBaruForm> _formLayananBaruAku;

        [Display(Name = "Form Member AKU", GroupName = "Member", Order = 11)]
        [AknpDisplay("_tipeMemberAKU", false, false)]
        [CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateTipeMemberAku))]
        public List<DataLayananBaruForm> FormLayananBaruAku
        {
            get
            {
                _formLayananBaruAku = _formLayananBaruAku == null
                    ? new List<DataLayananBaruForm>()
                    : _formLayananBaruAku = _formLayananBaruAku
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.FieldName)).ToList();
                return _formLayananBaruAku;
            }
            set { _formLayananBaruAku = value; }
        }

        public static ValidationResult ValidateTipeMemberAku(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;
            if (form.KategoriMember == RegistrasiKategoriMember.MEMBER_AKU.Name) {
                foreach(var d in form.FormLayananBaruAku)
                {
                    if (d.FieldRequired == true && string.IsNullOrWhiteSpace(d.FieldValue))
                    {
                        return new ValidationResult(d.FieldName+" wajib diisi");
                    }
                }
            }

            return success;
        }

        private List<DataLayananBaruForm> _formLayananBaruAki;

        [Display(Name = "Form Member AKI", GroupName = "Member", Order = 11)]
        [AknpDisplay("_tipeMemberAKI", false, false)]
        [CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateTipeMemberAki))]
        public List<DataLayananBaruForm> FormLayananBaruAki
        {
            get
            {
                _formLayananBaruAki = _formLayananBaruAki == null
                    ? new List<DataLayananBaruForm>()
                    : _formLayananBaruAki = _formLayananBaruAki
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.FieldName)).ToList();
                return _formLayananBaruAki;
            }
            set { _formLayananBaruAki = value; }
        }

        public static ValidationResult ValidateTipeMemberAki(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;
            if (form.KategoriMember == RegistrasiKategoriMember.MEMBER_AKI.Name) {
                foreach(var d in form.FormLayananBaruAki)
                {
                    if (d.FieldRequired == true && string.IsNullOrWhiteSpace(d.FieldValue))
                    {
                        return new ValidationResult(d.FieldName+" wajib diisi");
                    }
                }
            }
            return success;
        }

        private List<DataLayananBaruForm> _formGroupPartisipan;

        [Display(Name = "Form Member AKU", GroupName = "Member", Order = 11)]
        [AknpDisplay("_formMemberPartisipan", false, false)]
        [CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateGroupMemberPartisipan))]
        public List<DataLayananBaruForm> FormGroupPartisipan {
            get
            {
                _formGroupPartisipan = _formGroupPartisipan == null
                    ? new List<DataLayananBaruForm>()
                    : _formGroupPartisipan = _formGroupPartisipan
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.FieldName)).ToList();

                return _formGroupPartisipan;
            } 
            set
            {
                _formGroupPartisipan = value;
            } 
        }

        public static ValidationResult ValidateGroupMemberPartisipan(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;

            if (form.KategoriMember == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name) {
                foreach(var d in form.FormGroupPartisipan)
                {
                    if (d.FieldRequired == true && string.IsNullOrWhiteSpace(d.FieldValue))
                    {
                        return new ValidationResult(d.FieldName+" wajib diisi");
                    }
                }
            }
            return success;
        }

        private IEnumerable<string> _dataTradingMember;

        [Display(Name = "Trading Member", GroupName = "Member", Order = 13)]
        [AknpDisplay("_tradingMember", true, false)]
        //[CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateDataTradingMember))]
        public IEnumerable<string> DataTradingMember {
            get
            {
                _dataTradingMember = _dataTradingMember == null
                   ? new List<string>()
                   : _dataTradingMember = _dataTradingMember
                       .Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                return _dataTradingMember;
            } 
            set
            {
                _dataTradingMember = value;
            } 
        }

        public static ValidationResult ValidateDataTradingMember(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;

            if (form.KategoriMember == "AKU" && form.DataTradingMember.Count() == 0)
            {
                return new ValidationResult("Trading Member is required");
            }

            return success;
        }

        [Display(Name = "Nama AB Sponsor", GroupName = "Member", Order = 14)]
        [AknpDisplay("_namaABSponsor", false, false)]
        [MaxLength(128)]
        //[CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateNamaABSponsor))]
        public string NamaABSponsor { get; set; }

        public static ValidationResult ValidateNamaABSponsor(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;
            
            if (form.KategoriMember == "PARTISIPAN" && form.TipeMemberPartisipan == "PED" && string.IsNullOrWhiteSpace(form.NamaABSponsor))
            {
                return new ValidationResult("Nama AB Sponsor wajib diisi");
            }
            else
            {
                return success;
            }
        }

        // DataAkte
        [Display(Name = "Nama Perusahaan", GroupName = "Nama", Description = "Harap mengisi nama perusahaan tanpa pencantuman PT.", Order = 11)]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        [CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateNamaPerusahaan))]
        public string NamaPerusahaan { get; set; }

        public static ValidationResult ValidateNamaPerusahaan(object val, ValidationContext valctx) {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;
            var ptName = "";
            if (form.NamaPerusahaan?.Length > 2)
            {
                ptName = form.NamaPerusahaan?.Trim().Substring(0, 3) ?? "";
            }
            form.NamaPerusahaan = form.NamaPerusahaan?.Trim();
            if (ptName.Equals("PT ") || ptName.Equals("PT.")) {
                return new ValidationResult("Harap mengisi nama perusahaan tanpa pencantuman PT.");
            } else {
                return success;
            }
        }

        // DataAlamat
        [Display(GroupName = "Alamat", Order = 12)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string Gedung { get; set; }

        // DataAlamat
        [Display(GroupName = "Alamat", Order = 13)]
        [AknpDisplay(null, false, true)]
        [Required]
        [MaxLength(256)]
        public string Jalan { get; set; }

        // DataAlamat
        [Display(GroupName = "Alamat", Order = 14)]
        [AknpDisplay(null, false, true)]
        [Required]
        [MaxLength(128)]
        public string Kota { get; set; }

        // DataAlamat
        [Display(Name = "Kode Pos", GroupName = "Alamat", Order = 15)]
        [AknpDisplay("_kodePos", false, true)]
        [Required]
        [MaxLength(32)]
        public string KodePos { get; set; }

        // DataAlamat
        [Display(GroupName = "Alamat", Order = 16)]
        [AknpDisplay(null, false, false)]
        [Required]
        [MaxLength(128)]
        public string Telp { get; set; }

        // DataAlamat
        [Display(GroupName = "Alamat", Order = 17)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string Fax { get; set; }

        // DataAlamat
        [Display(GroupName = "Alamat", Order = 18)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        public string Website { get; set; }

        [Display(Name = "E-mail", GroupName = "Alamat", Order = 18)]
        [AknpDisplay(null, true, false)]
        [Required]
        [EmailAddress]
        [MaxLength(256)]
        public string Email { get; set; }

        // DataInformasiLain
        [Display(Name = "Status Perusahaan", GroupName = "Perusahaan", Order = 19)]
        [AknpDisplay("_statusPerusahaan", false, false)]
        [Required]
        [MaxLength(128)]
        public string StatusPerusahaan { get; set; }

        // DataInformasiLain
        [Display(GroupName = "Perusahaan", Order = 20)]
        [AknpDisplay(null, false, false)]
        //[Required]
        [MaxLength(128)]
        public string NPWP { get; set; }
        
        // DataAkte
        [Display(Name = "Modal Dasar (Rp)", GroupName = "Modal", Order = 21)]
        [AknpDisplay(null, false, false)]
        [Required]
        [CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateHigherModalDasar))]
        public decimal ModalDasarRupiah { get; set; }

        public static ValidationResult ValidateHigherModalDasar(object val, ValidationContext valctx) {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;
            var dasar = form.ModalDasarRupiah;
            var disetor = form.ModalDisetorRupiah;
            if (dasar < 0) {
                return new ValidationResult("Modal Dasar tidak dapat bernilai negatif");
            }
            if (disetor > 1000000000000000000000m) {
                return new ValidationResult("Maksimal Nilai Modal Disetor dibawah 1,000,000,000,000,000,000,000 (1000 trilliun)");
            }
            if (dasar < disetor) {
                return new ValidationResult("Modal Disetor tidak boleh lebih besar dari Modal Dasar.", new[] { valctx.MemberName });
            }
            return success;
        }

        // DataAkte
        [Display(Name = "Modal Disetor (Rp)", GroupName = "Modal", Order = 22)]
        [AknpDisplay(null, false, false)]
        [Required]
        [CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateTotalModal))]
        public decimal ModalDisetorRupiah { get; set; }

        public static ValidationResult ValidateTotalModal(object val, ValidationContext valctx) {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;
            var disetor = form.ModalDisetorRupiah;
            var inputTotal =
                form.DaftarPemegangSaham.Aggregate(0m, (currentTotal, x) => x.NominalSahamRupiah + currentTotal);
            if (disetor < 0) {
                return new ValidationResult("Modal Disetor tidak dapat bernilai negatif");
            }
            if (disetor > 1000000000000000000000m) {
                return new ValidationResult("Maksimal Nilai Modal Disetor dibawah 1,000,000,000,000,000,000,000 (1000 trilliun)");
            }
            if (disetor != inputTotal) {
                return new ValidationResult("Total nominal modal pemegang saham harus sama dengan modal disetor.",
                    new[] {valctx.MemberName});
            }

            return success;
        }
        
        // DataInformasiLain
        [Display(Name = "Anggota Bursa", GroupName = "Perusahaan", Order = 23)]
        [AknpDisplay("_anggotaBursa", false, false)]
        //[Required]
        [MaxLength(128)]
        public string AnggotaBursa { get; set; }

        // DataJenisUsaha
        [Display(GroupName = "Ijin Usaha",
            Order = 24)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        //[Required]
        public string PPE { get; set; }

        // DataJenisUsaha
        [Display(GroupName = "Ijin Usaha", Description = "No. izin. Kosongkan jika tidak termasuk dalam jenis ini",
            Order = 25)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string PEE { get; set; }

        // DataJenisUsaha
        [Display(GroupName = "Ijin Usaha", Description = "No. izin. Kosongkan jika tidak termasuk dalam jenis ini",
            Order = 26)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string MI { get; set; }

        // DataJenisUsaha
        [Display(GroupName = "Ijin Usaha", Description = "No. izin. Kosongkan jika tidak termasuk dalam jenis ini",
            Order = 27)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string BK { get; set; }

        // DataJenisUsaha
        [Display(GroupName = "Ijin Usaha", Description = "No. izin. Kosongkan jika tidak termasuk dalam jenis ini",
            Order = 28)]
        [AknpDisplay(null, false, false)]
        [MaxLength(128)]
        public string BU { get; set; }

        // DataJenis Usaha
        [Display(GroupName = "Ijin Usaha", Order = 29)]
        [AknpDisplay("_izinUsahaPED", false, false)]
        [CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateIzinUsahaPED))]
        [MaxLength(128)]
        public string PED { get; set; }

        public static ValidationResult ValidateIzinUsahaPED(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;

            if (form.KategoriMember == "PARTISIPAN" && form.TipeMemberPartisipan == "PED" && string.IsNullOrWhiteSpace(form.PED))
            {
                return new ValidationResult("No. Izin Usaha PED wajib diisi.");
            }
            else
            {
                return success;
            }
        }

        #endregion

        #region screen 2

        // DataAkte
        [Display(Name = "No. Akte", GroupName = "Akte", Order = 29)]
        [AknpDisplay(null, false, false)]
        [Required]
        [MaxLength(128)]
        public string NoAkte { get; set; }

        // DataAkte
        [Display(Name = "Tgl. Akte", GroupName = "Akte", Order = 30)]
        [AknpDisplay(null, false, false)]
        [Required]
        public DateTime? TglAkte { get; set; }

        // DataAkte
        [Display(GroupName = "Akte", Order = 31)]
        [AknpDisplay("_notaris", false, false)]
        [Required]
        [MaxLength(128)]
        public string Notaris { get; set; }

        // DataAkte
        [Display(Name = "File Akte", GroupName = "Akte", Order = 32)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        public string AkteFileUrl { get; set; }

        private List<DataPemegangSahamForm> _daftarPemegangSaham;

        // DataAkte
        [Display(Name = "Daftar Pemegang Saham", GroupName = "Pemegang Saham", Order = 33)]
        [AknpDisplay(null, false, false)]
        [CollectionLength(MinLength = 1)]
        [CollectionItemMustValid]
        public List<DataPemegangSahamForm> DaftarPemegangSaham {
            get {
                _daftarPemegangSaham = _daftarPemegangSaham == null
                    ? new List<DataPemegangSahamForm>()
                    : _daftarPemegangSaham = _daftarPemegangSaham
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.Nama)).ToList();
                return _daftarPemegangSaham;
            }
            set { _daftarPemegangSaham = value; }
        }

        private List<DataKomisarisPersonForm> _daftarKomisaris;

        // DataAkte
        [Display(Name = "Daftar Komisaris", GroupName = "Komisaris", Order = 34)]
        [AknpDisplay(null, true, false)]
        [CollectionLength(MinLength = 1)]
        [CollectionItemMustValid]
        public List<DataKomisarisPersonForm> DaftarKomisaris {
            get {
                _daftarKomisaris = _daftarKomisaris == null
                    ? new List<DataKomisarisPersonForm>()
                    : _daftarKomisaris = _daftarKomisaris.Where(x => x != null && !string.IsNullOrWhiteSpace(x.Nama))
                        .ToList();
                return _daftarKomisaris;
            }
            set { _daftarKomisaris = value; }
        }

        private List<DataDirectorPersonForm> _daftarDireksi;

        // DataAkte
        [Display(Name = "Daftar Direksi", GroupName = "Direksi", Order = 35)]
        [AknpDisplay(null, true, false)]
        [CollectionLength(MinLength = 1)]
        [CollectionItemMustValid]
        public List<DataDirectorPersonForm> DaftarDireksi {
            get {
                _daftarDireksi = _daftarDireksi == null
                    ? new List<DataDirectorPersonForm>()
                    : _daftarDireksi = _daftarDireksi.Where(x => x != null && !string.IsNullOrWhiteSpace(x.Nama))
                        .ToList();
                return _daftarDireksi;
            }
            set { _daftarDireksi = value; }
        }

        private List<DataDirectorPersonForm> _daftarPejabatBerwenang;

        // DataPejabatBerwenang
        [Display(Name = "Daftar Pejabat Berwenang", GroupName = "Pejabat Berwenang", Order = 36)]
        [AknpDisplay(null, false, false)]
        [CollectionLength(MinLength = 1)]
        [CollectionItemMustValid]
        public List<DataDirectorPersonForm> DaftarPejabatBerwenang {
            get {
                _daftarPejabatBerwenang = _daftarPejabatBerwenang == null
                    ? new List<DataDirectorPersonForm>()
                    : _daftarPejabatBerwenang = _daftarPejabatBerwenang
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.Nama)).ToList();
                return _daftarPejabatBerwenang;
            }
            set { _daftarPejabatBerwenang = value; }
        }

        private List<DataPersonForm> _daftarContactPerson;

        // DataContactPerson
        [Display(Name = "Daftar Contact Person", GroupName = "Contact Person", Order = 37)]
        [AknpDisplay(null, false, false)]
        [CollectionLength(MinLength = 1)]
        [CollectionItemMustValid]
        public List<DataPersonForm> DaftarContactPerson {
            get {
                _daftarContactPerson = _daftarContactPerson == null
                    ? new List<DataPersonForm>()
                    : _daftarContactPerson = _daftarContactPerson
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.Nama)).ToList();
                return _daftarContactPerson;
            }
            set { _daftarContactPerson = value; }
        }

        #endregion

        #region screen 3

        //Dokumen Perjanjian
        [Display(Name = "File Perjanjian Anggota Kliring Umum dengan KPEI", GroupName = "Dokumen Perjanjian", Order = 38)]
        [AknpDisplay("_filePerjanjianAKU", false, false)]
        [MaxLength(256)]
        public string PerjanjianAkuDenganKPEIFileUrl { get; set; }

        [Display(Name = "File Perjanjian Anggota Kliring Individu dengan KPEI", GroupName = "Dokumen Perjanjian", Order = 39)]
        [AknpDisplay("_filePerjanjianAKI", false, false)]
        [MaxLength(256)]
        public string PerjanjianAkiDenganKPEIFileUrl { get; set; }

        [Display(Name = "File Perjanjian Perusahaan Efek Daerah dengan KPEI", GroupName = "Dokumen Perjanjian", Order = 40)]
        [AknpDisplay("_filePerjanjianPED", false, false)]
        [MaxLength(256)]
        public string PerjanjianPEDDenganKPEIFileUrl { get; set; }

        [Display(Name = "File Perjanjian Partisipan dengan KPEI", GroupName = "Dokumen Perjanjian", Order = 41)]
        [AknpDisplay("_filePerjanjianPartisipan", false, false)]
        [MaxLength(256)]
        public string PerjanjianPartisipanDenganKPEIFileUrl { get; set; }

        [Display(Name = "File Perjanjian Anggota Kliring Umum dengan Trading Member", GroupName = "Dokumen Perjanjian", Order = 42)]
        [AknpDisplay("_filePerjanjianAKU", false, false)]
        [MaxLength(256)]
        [CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateFileTradingMember))]
        public string PerjanjianAkuDenganTradingMemberFileUrl { get; set; }

        [Display(Name = "File Perjanjian Perusahaan Efek Daerah dengan AB Sponsor", GroupName = "Dokumen Perjanjian", Order = 43)]
        [AknpDisplay("_filePerjanjianPED", false, false)]
        [MaxLength(256)]
        [CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateFileABSponsor))]
        public string PerjanjianPEDDenganABSponsorFileUrl { get; set; }

        [Display(Name = "File Perjanjian Portability", GroupName = "Dokumen Perjanjian", Order = 44)]
        [AknpDisplay("_filePerjanjianPortability", false, false)]
        [MaxLength(256)]
        public string PerjanjianPortabilityFileUrl { get; set; }

        public static ValidationResult ValidateFileABSponsor(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;

            if (form.KategoriMember == "PARTISIPAN" && form.TipeMemberPartisipan == "PED" && string.IsNullOrWhiteSpace(form.PerjanjianPEDDenganABSponsorFileUrl))
            {
                return new ValidationResult("Harap pilih File Perjanjian Perusahaan Efek Daerah dengan AB Sponsor");
            }
            else
            {
                return success;
            }
        }

        public static ValidationResult ValidateFileTradingMember(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;

            if (form.KategoriMember == "AKU" && String.IsNullOrWhiteSpace(form.PerjanjianAkuDenganTradingMemberFileUrl))
            {
                return new ValidationResult("Harap pilih File Perjanjian Anggota Kliring Umum dengan Trading Member");
            }
            else
            {
                return success;
            }
        }

        #endregion

        #region screen 4

        // DataInformasiLain
        [Display(Name = "File Anggaran Dasar Perusahaan", GroupName = "Dokumen Pendukung AK / Partisipan", Order = 45)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        [Required]
        public string AnggaranDasarFileUrl { get; set; }

        // DataInformasiLain
        [Display(Name = "File Akte Perubahan dan SK Kemenkumham", GroupName = "Dokumen Pendukung AK / Partisipan", Order = 46)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        [Required]
        public string AktePerubahanFileUrl { get; set; }

        // DataInformasiLain
        [Display(Name = "File Ijin Usaha dari OJK", GroupName = "Dokumen Pendukung AK / Partisipan", Order = 47)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        [Required]
        public string IjinUsahaFileUrl { get; set; }
        
        // Modul LaporanKeuangan
        [Display(Name = "File Laporan Keuangan", GroupName = "Dokumen Pendukung AK / Partisipan", Order = 48)]
        [AknpDisplay("_laporanKeuanganFileUrl", false, false)]
        [MaxLength(256)]
        [CustomValidation(typeof(DataRegistrasiAknpForm), nameof(ValidateFileLaporanKeuangan))]
        public string LaporanKeuanganFileUrl { get; set; }

        public static ValidationResult ValidateFileLaporanKeuangan(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataRegistrasiAknpForm;
            if (form == null) return success;

            if ((form.KategoriMember == "AKU" || 
                form.KategoriMember == "AKI" || 
                (form.KategoriMember == "PARTISIPAN" && form.TipeMemberPartisipan != "PED"))
                && String.IsNullOrWhiteSpace(form.LaporanKeuanganFileUrl))
            {
                return new ValidationResult("Harap pilih File Laporan Keungan");
            }
            else
            {
                return success;
            }
        }

        // DataInformasiLain
        [Display(Name = "File NPWP", GroupName = "Dokumen Pendukung AK / Partisipan", Order = 49)]
        [AknpDisplay(null, false, false)]
        [MaxLength(256)]
        [Required]
        public string NPWPFileUrl { get; set; }

        private List<DataSupportDocumentForm> _otherSupportDocumentsFileUrls;

        // ???
        [Display(Name = "File Dokumen Pendukung Lainnya", GroupName = "Dokumen Pendukung AK / Partisipan", Order = 58)]
        [AknpDisplay(null, false, false)]
        [CollectionItemMustValid]
        public List<DataSupportDocumentForm> OtherSupportDocumentsFileUrls {
            get {
                _otherSupportDocumentsFileUrls = _otherSupportDocumentsFileUrls == null
                    ? new List<DataSupportDocumentForm>()
                    : _otherSupportDocumentsFileUrls = _otherSupportDocumentsFileUrls
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.DocFileUrl)).ToList();
                return _otherSupportDocumentsFileUrls;
            }
            set { _otherSupportDocumentsFileUrls = value; }
        }

        #endregion

    }
}