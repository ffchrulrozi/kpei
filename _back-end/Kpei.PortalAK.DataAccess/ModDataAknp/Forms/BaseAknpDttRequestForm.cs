﻿using System;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms {
    public class BaseAknpDttRequestForm : DttRequestForm, IBindableClass {
        /// <inheritdoc />
        public string ClassTypeFullName => GetType().FullName;

        public string KodeAk { get; set; }

        public string Status { get; set; }

        public DateTime? ActiveFromDateTime { get; set; }

        public DateTime? ActiveToDateTime { get; set; }

        public DateTime? FromDateTime { get; set; }

        public DateTime? ToDateTime { get; set; }
    }
}