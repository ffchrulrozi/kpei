﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.BankPembayaran {
    public class DataBankPembayaranAknpForm : BaseAknpFormWithSuratKeterangan {
        [Display(Name = "Bank Pembayaran", GroupName = "Bank Pembayaran")]
        [AknpDisplay("_bankPembayaran", false, false)]
        [Required]
        [MaxLength(128)]
        public string BankPembayaran { get; set; }
    }
}
