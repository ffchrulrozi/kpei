﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using Kpei.PortalAK.DataAccess.Validators;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.PejabatBerwenang {
    public class DataPejabatBerwenangAknpForm : BaseAknpFormWithSuratKeterangan {
        private List<DataDirectorPersonForm> _listPejabatBerwenang;

        [Display(Name = "Daftar Pejabat Berwenang", GroupName = "Pejabat Berwenang", Order = 1)]
        [AknpDisplay(null, false, false)]
        [Required]
        [CollectionLength(MinLength = 1)]
        [CollectionItemMustValid]
        public List<DataDirectorPersonForm> ListPejabatBerwenang {
            get {
                _listPejabatBerwenang = _listPejabatBerwenang == null
                    ? _listPejabatBerwenang = new List<DataDirectorPersonForm>()
                    : _listPejabatBerwenang = _listPejabatBerwenang
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.Nama)).ToList();
                return _listPejabatBerwenang;
            }
            set { _listPejabatBerwenang = value; }
        }
    }
}