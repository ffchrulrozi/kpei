﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.JenisUsaha {
    public class DataJenisUsahaAknpForm : BaseAknpFormWithSuratKeterangan{
        [Display(GroupName = "Jenis Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string PPE { get; set; }

        [Display(GroupName = "Jenis Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string PEE { get; set; }

        [Display(GroupName = "Jenis Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string MI { get; set; }

        [Display(GroupName = "Jenis Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string BK { get; set; }

        [Display(GroupName = "Jenis Usaha")]
        [AknpDisplay(null, true, false)]
        [MaxLength(128)]
        public string BU { get; set; }
    }
}
