﻿using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.SPAB {
    public class DataSpabAknpForm : BaseAknpFormWithSuratKeterangan {
        [Display(Name = "No. SPAB", GroupName = "SPAB")]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        public string NoSPAB { get; set; }

        [Display(Name = "Tanggal SPAB", GroupName = "SPAB")]
        [AknpDisplay(null, true, false)]
        [Required]
        public DateTime? TanggalSPABUtc { get; set; }

        [Display(Name = "File SPAB", GroupName = "Files")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string SPABFileUrl { get; set; }

    }
}
