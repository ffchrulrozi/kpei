﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.StatusKpei {
    public class DataStatusKpeiAknpForm : BaseAknpFormWithSuratKeterangan {
        [Display(Name = "Kategori Member")]
        [Required]
        [AknpDisplay("_kategoriMember", true, false)]
        [MaxLength(128)]
        public string KategoriMember { get; set; }

        [Display(Name = "No. Pengumuman", GroupName = "Status KPEI")]
        [AknpDisplay(null, true, false)]
        [Required]
        [MaxLength(128)]
        public string NoDokumen { get; set; }

        [Display(Name = "Status KPEI", GroupName = "Status KPEI")]
        [AknpDisplay("_statusKpei", true, true)]
        [Required]
        [MaxLength(128)]
        public string StatusKPEI { get; set; }

        [Display(Name = "Sub Status Anggota di KPEI", GroupName = "Status KPEI")]
        [AknpDisplay("_subStatusKpei", true, false)]
        [MaxLength(128)]
        public string SubStatusAnggotaKPEI { get; set; }

        [Display(Name = "Tanggal Awal Status", GroupName = "Status KPEI")]
        [AknpDisplay(null, true, false)]
        [Required]
        public DateTime? TanggalAwalStatusUtc { get; set; }

        [Display(Name = "Tanggal Akhir Status", GroupName = "Status KPEI")]
        [AknpDisplay(null, true, false)]
        public DateTime? TanggalAkhirStatusUtc { get; set; }

        [Display(Name = "Tanggal Aktif", GroupName = "Status KPEI")]
        [AknpDisplay(null, true, false)]
        public DateTime? TanggalAktifUtc { get; set; }

        [Display(Name = "Alasan", GroupName = "Status KPEI")]
        [AknpDisplay(null, true, false)]
        [MaxLength(512)]
        public string Alasan { get; set; }

        [Display(Name = "Keterangan", GroupName = "Status KPEI")]
        [AknpDisplay(null, true, false)]
        [MaxLength(512)]
        public string Keterangan { get; set; }
    }
}
