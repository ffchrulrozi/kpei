﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.StatusBursa {
    public class DataStatusBursaAknpForm : BaseAknpFormWithSuratKeterangan {
        [Display(Name = "No. Pengumuman", GroupName = "Status Bursa")]
        [AknpDisplay(null, true, false)]
        [Required]
        [MaxLength(128)]
        public string NoPengumuman { get; set; }

        [Display(Name = "Status Bursa", GroupName = "Status Bursa")]
        [AknpDisplay("_statusBursa", true, true)]
        [Required]
        [MaxLength(128)]
        public string StatusBursa { get; set; }

        [Display(Name = "Tanggal Aktif", GroupName = "Status Bursa")]
        [AknpDisplay(null, true, true)]
        public DateTime? TanggalAktifUtc { get; set; }

        [Display(Name = "Tanggal Suspend Awal", GroupName = "Status Bursa")]
        [AknpDisplay(null, true, true)]
        public DateTime? TanggalSuspendAwalUtc { get; set; }

        [Display(Name = "Tanggal Suspend Akhir", GroupName = "Status Bursa")]
        [AknpDisplay(null, true, true)]
        public DateTime? TanggalSuspendAkhirUtc { get; set; }

        [Display(Name = "Tanggal Cabut", GroupName = "Status Bursa")]
        [AknpDisplay(null, true, true)]
        public DateTime? TanggalCabutUtc { get; set; }

        [Display(Name = "Alasan", GroupName = "Status Bursa")]
        [AknpDisplay(null, true, true)]
        [MaxLength(512)]
        public string Alasan { get; set; }

        [Display(Name = "Keterangan", GroupName = "Status Bursa")]
        [AknpDisplay(null, true, true)]
        [MaxLength(512)]
        public string Keterangan { get; set; }
    }
}
