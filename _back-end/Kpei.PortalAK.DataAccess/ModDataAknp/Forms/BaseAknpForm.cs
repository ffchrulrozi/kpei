﻿using System;
using System.ComponentModel.DataAnnotations;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms {
    public abstract class BaseAknpForm : IBindableClass {
        /// <inheritdoc />
        public string ClassTypeFullName => GetType().FullName;

        [MaxLength(32)]
        [RequiredIfKpei]
        public string KodeAk { get; set; }
        public string Id { get; set; }

        [MaxLength(128)]
        [RequiredIfKpei]
        public string Status { get; set; }

        public bool Draft { get; set; }
        public string UserDraft { get; set; }
        public DateTime? ActiveUtc { get; set; }
    }
}