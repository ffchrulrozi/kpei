﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms
{
    public class DataLayananBaruForm
    {
        public DataLayananBaruForm()
        {
            Id = Guid.NewGuid().ToString();
        }
        [MaxLength(128)]
        public string Id { get; set; }

        [MaxLength(128)]
        public string TipeMemberAK { get; set; }

        [MaxLength(256)]
        public string TipeMemberPartisipan { get; set; }

        [MaxLength(256)]
        public string FieldName { get; set; }
        [MaxLength(256)]
        public string FieldType { get; set; }

        [MaxLength(256)]
        public string FieldValue { get; set; }
        public bool FieldRequired { get; set; }

        [MaxLength(128)]
        public string LayananBaruId { get; set; }
    }
}
