﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms {
    public class DataMemberAkForm {
        public DataMemberAkForm() {
            Id = Guid.NewGuid().ToString();
        }

        [MaxLength(128)]
        public string Id { get; set; }

        [MaxLength(32)]
        private string _parent;
        [MaxLength(32)]
        private string _child;

        public string Parent { get { return _parent; } set { _parent = value; } }
        public string Child { get { return _child; } set { _child = value; } }
    }
}
