﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using Kpei.PortalAK.DataAccess.Validators;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.Akte {
    public class DataAkteAknpForm : BaseAknpForm {

        #region Akte Details

        [Display(Name = "No. Akte", GroupName = "Informasi", Order = 1)]
        [Required]
        [MaxLength(128)]
        public string NoAkte { get; set; }

        [Display(Name = "Jenis Akte", GroupName = "Informasi", Order = 2)]
        [AknpDisplay("_jenisAkte", true, true)]
        [Required]
        [MaxLength(128)]
        public string JenisAkte { get; set; }

        [Display(Name = "Tgl. Akte", GroupName = "Informasi", Order = 3)]
        [Required]
        public DateTime? TglAkte { get; set; }

        [Display(GroupName = "Informasi", Order = 4)]
        [AknpDisplay("_notaris", true, true)]
        [Required]
        [MaxLength(128)]
        public string Notaris { get; set; }

        [Display(GroupName = "Informasi", Order = 5)]
        [Required]
        [MaxLength(1024)]
        public string Ringkasan { get; set; }

        [Display(Name = "File Akte", GroupName = "Informasi", Order = 6)]
        [Required]
        [MaxLength(256)]
        public string AkteFileUrl { get; set; }

        [Display(Name = "SK dari Kemenkumham", GroupName = "Informasi", Order = 7)]
        [Required]
        [MaxLength(256)]
        public string SkKemenkumhamFileUrl { get; set; }

        [Display(Name = "SK dari OJK", GroupName = "Informasi", Order = 8)]
        [Required]
        [MaxLength(256)]
        public string SkOjkFileUrl { get; set; }

        #endregion

        #region Nama

        [Display(Name = "Nama Perusahaan", GroupName = "Nama", Description = "Harap mengisi nama perusahaan tanpa pencantuman PT.", Order = 11)]
        [AknpDisplay(null, true, true)]
        [Required]
        [MaxLength(128)]
        [CustomValidation(typeof(DataAkteAknpForm), nameof(ValidateNamaPerusahaan))]
        public string NamaPerusahaan { get; set; }

        public static ValidationResult ValidateNamaPerusahaan(object val, ValidationContext valctx) {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataAkteAknpForm;
            if (form == null) return success;
            var ptName = "";
            if (form.NamaPerusahaan?.Length > 2)
            {
                ptName = form.NamaPerusahaan?.Trim().Substring(0, 3) ?? "";
            }
            form.NamaPerusahaan = form.NamaPerusahaan?.Trim();
            if ((ptName.Equals("PT ") || ptName.Equals("PT."))) {
                return new ValidationResult("Harap mengisi nama perusahaan tanpa pencantuman PT.");
            } else {
                return success;
            }
        }

        [Display(Name = "Tgl. Berlaku Nama", GroupName = "Nama", Order = 10)]
        [Required]
        public DateTime? TglNamaBerlaku { get; set; }

        #endregion

        #region Modal
        
        [Display(Name = "Modal Disetor (Rp)", GroupName = "Modal", Order = 12)]
        [Required]
        [CustomValidation(typeof(DataAkteAknpForm), nameof(ValidateTotalModal))]
        public decimal ModalDisetorRupiah { get; set; }

        [Display(Name = "Modal Dasar (Rp)", GroupName = "Modal", Order = 11)]
        [Required]
        [CustomValidation(typeof(DataAkteAknpForm), nameof(ValidateHigherModalDasar))]
        public decimal ModalDasarRupiah { get; set; }

        public static ValidationResult ValidateHigherModalDasar(object val, ValidationContext valctx) {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataAkteAknpForm;
            if (form == null) return success;
            var dasar = form.ModalDasarRupiah;
            var disetor = form.ModalDisetorRupiah;
            if (dasar > 1000000000000000000000m) {
                return new ValidationResult("Maksimal Nilai Modal Dasar dibawah 1,000,000,000,000,000,000,000 (1000 trilliun)");
            }
            if (dasar < disetor) {
                return new ValidationResult("Modal Disetor tidak boleh lebih besar dari Modal Dasar.", new[] { valctx.MemberName });
            }
            return success;
        }

        public static ValidationResult ValidateTotalModal(object val, ValidationContext valctx) {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataAkteAknpForm;
            if (form == null) return success;
            var disetor = form.ModalDisetorRupiah;
            var inputTotal =
                form.ListPemegangSaham.Aggregate(0m, (currentTotal, x) => x.NominalSahamRupiah + currentTotal);

            if (disetor > 1000000000000000000000m) {
                return new ValidationResult("Maksimal Nilai Modal Disetor dibawah 1,000,000,000,000,000,000,000 (1000 trilliun)");
            }
            if (disetor != inputTotal) {
                return new ValidationResult("Total nominal modal pemegang saham harus sama dengan modal disetor.", new[] {valctx.MemberName});
            }

            return success;
        }

        #endregion

        #region Pemegang Saham

        private List<DataPemegangSahamForm> _listPemegangSaham;

        [Display(Name = "Daftar Pemegang Saham", GroupName = "Pemegang Saham", Order = 13)]
        [AknpDisplay(null, false, false)]
        [Required]
        [CollectionLength(MinLength = 1)]
        [CollectionItemMustValid]
        public List<DataPemegangSahamForm> ListPemegangSaham {
            get {
                _listPemegangSaham = _listPemegangSaham == null
                    ? new List<DataPemegangSahamForm>()
                    : _listPemegangSaham.Where(x => x != null && !string.IsNullOrWhiteSpace(x.Nama)).ToList();

                return _listPemegangSaham;
            }
            set { _listPemegangSaham = value; }
        }

        #endregion

        #region Komisaris

        private List<DataKomisarisPersonForm> _listKomisaris;

        [Display(Name = "Daftar Komisaris", GroupName = "Komisaris", Order = 14)]
        [AknpDisplay(null, false, false)]
        [Required]
        [CollectionLength(MinLength = 1)]
        [CollectionItemMustValid]
        public List<DataKomisarisPersonForm> ListKomisaris {
            get {
                _listKomisaris = _listKomisaris == null
                    ? new List<DataKomisarisPersonForm>()
                    : _listKomisaris.Where(x => x != null && !string.IsNullOrWhiteSpace(x.Nama)).ToList();

                return _listKomisaris;
            }
            set { _listKomisaris = value; }
        }

        #endregion

        #region Direksi

        private List<DataDirectorPersonForm> _listDireksi;

        [Display(Name = "Daftar Direksi", GroupName = "Direksi", Order = 15)]
        [AknpDisplay(null, false, false)]
        [Required]
        [CollectionLength(MinLength = 1)]
        [CollectionItemMustValid]
        public List<DataDirectorPersonForm> ListDireksi {
            get {
                _listDireksi = _listDireksi == null
                    ? new List<DataDirectorPersonForm>()
                    : _listDireksi.Where(x => x != null && !string.IsNullOrWhiteSpace(x.Nama)).ToList();

                return _listDireksi;
            }
            set { _listDireksi = value; }
        }

        #endregion

    }
}