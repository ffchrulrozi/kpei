﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.Akte {
    public class DataPemegangSahamForm {
        private string _nama;

        public DataPemegangSahamForm() {
            Id = Guid.NewGuid().ToString();
        }

        [MaxLength(128)]
        public string Id { get; set; }

        [Required]
        [MaxLength(128)]
        public string Nama {
            get { return _nama?.Trim(); }
            set { _nama = value; }
        }

        [Display(Name = "Jml. Lembar Saham")]
        [Required]
        [CustomValidation(typeof(DataPemegangSahamForm), nameof(ValidateDecimal))]
        public decimal LembarSaham { get; set; }

        public static ValidationResult ValidateDecimal(object val, ValidationContext valctx) {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as DataPemegangSahamForm;
            if (form == null) return success;
            var lbr = form.LembarSaham;
            if (lbr > 1000000000000000000000m ) {
                return new ValidationResult("Maksimal Nilai Lembar Saham dibawah 1,000,000,000,000,000,000,000 (1000 trilliun)");
            }
            return success;
        }



        [Display(Name = "Nominal Saham")]
        [Required]
        public decimal NominalSahamRupiah { get; set; }

        [Display(Name = "Kewarganegaraan")]
        [Required]
        [MaxLength(128)]
        public string WargaNegara { get; set; }
    }
}