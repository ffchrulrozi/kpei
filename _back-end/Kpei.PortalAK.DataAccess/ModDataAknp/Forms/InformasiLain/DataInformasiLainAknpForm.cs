﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.InformasiLain {
    public class DataInformasiLainAknpForm : BaseAknpFormWithSuratKeterangan {
        [Display(Name = "Status Perusahaan", GroupName = "Perusahaan")]
        [AknpDisplay("_statusPerusahaan", true, false)]
        [Required]
        [MaxLength(128)]
        public string StatusPerusahaan { get; set; }

        [Display(GroupName = "Perusahaan")]
        [AknpDisplay(null, true, false)]
        //[Required]
        [MaxLength(128)]
        public string NPWP { get; set; }

        [Display(Name = "Anggota Bursa", GroupName = "Perusahaan")]
        [AknpDisplay("_anggotaBursa", true, false)]
        //[Required]
        [MaxLength(128)]
        public string AnggotaBursa { get; set; }

        [Display(Name = "Kode Pemantau", GroupName = "Perusahaan")]
        [AknpDisplay("_pemantau", true, false)]
        [MaxLength(128)]
        public string Pemantau { get; set; }

        [Display(Name = "File Anggaran Dasar", GroupName = "Files")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string AnggaranDasarFileUrl { get; set; }

        [Display(Name = "File Akte Pendirian", GroupName = "Files")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string AktePendirianFileUrl { get; set; }

        [Display(Name = "File Akte Perubahan", GroupName = "Files")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string AktePerubahanFileUrl { get; set; }

        [Display(Name = "File Laporan Keuangan", GroupName = "Files")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string LaporanKeuanganFileUrl { get; set; }

        [Display(Name = "File Ijin Usaha dari OJK", GroupName = "Files")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string IjinUsahaFileUrl { get; set; }

        [Display(Name = "File NPWP", GroupName = "Files")]
        [AknpDisplay(null, true, false)]
        [MaxLength(256)]
        public string NPWPFileUrl { get; set; }

        public string RegistrasiBaruId { get; set; }

        // Untuk mengakses data-data yang ndak ada di mana-mana tapi hanya ada di registrasi baru
        [ForeignKey(nameof(RegistrasiBaruId))]
        public virtual DataRegistrasi RegistrasiBaru { get; set; }

        [Display(Name = "Tanggal Serah Dokumen", GroupName = "Informasi Tambahan")]
        [AknpDisplay(null, true, false)]
        public DateTime? TglSerahDokumenUtc { get; set; }

        [Display(Name = "Tanggal Serah Dana Minimum Cash", GroupName = "Informasi Tambahan")]
        [AknpDisplay(null, true, false)]
        public DateTime? TglSerahDanaMinCashUtc { get; set; }

        [Display(Name = "Tanggal Akta Pengikatan", GroupName = "Informasi Tambahan")]
        [AknpDisplay(null, true, false)]
        public DateTime? TglAktaPengikatanUtc { get; set; }

        [Display(Name = "Tanggal Rekomendasi", GroupName = "Informasi Tambahan")]
        [AknpDisplay(null, true, false)]
        public DateTime? TglRekomendasiUtc { get; set; }
    }
}
