﻿using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms.Alamat {
    public class DataAlamatAknpForm : BaseAknpFormWithSuratKeterangan {
        [Display(GroupName = "Alamat")]
        [MaxLength(128)]
        public string Gedung { get; set; }

        [Display(GroupName = "Alamat")]
        [Required]
        [MaxLength(256)]
        public string Jalan { get; set; }

        [Display(GroupName = "Alamat")]
        [Required]
        [MaxLength(128)]
        public string Kota { get; set; }
        
        [Display(Name = "Kode Pos", GroupName = "Alamat")]
        [Required]
        [MaxLength(32)]
        public string KodePos { get; set; }

        [Display(GroupName = "Alamat")]
        [Required]
        [MaxLength(128)]
        public string Telp { get; set; }

        [Display(GroupName = "Alamat")]
        [MaxLength(128)]
        public string Fax { get; set; }

        [Display(GroupName = "Alamat")]
        [MaxLength(256)]
        public string Website { get; set; }

        [Display(Name = "E-mail", GroupName = "Alamat")]
        [MaxLength(256)]
        [EmailAddress]
        [Required]
        public string Email { get; set; }
    }
}