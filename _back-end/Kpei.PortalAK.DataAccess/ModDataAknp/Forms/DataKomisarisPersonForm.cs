﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Forms {
    public class DataKomisarisPersonForm {

        private string _nama;

        public DataKomisarisPersonForm() {
            Id = Guid.NewGuid().ToString();
        }

        [MaxLength(128)]
        public string Id { get; set; }

        [Display(Name = "File Spesimen Tanda Tangan")]
        [MaxLength(256)]
        public string TtdFileUrl { get; set; }

        [Display(Name = "File KTP")]
        [MaxLength(256)]
        public string KtpFileUrl { get; set; }

        [Required]
        [MaxLength(128)]
        public string Nama {
            get { return _nama?.Trim(); }
            set { _nama = value; }
        }

        [Required]
        [MaxLength(128)]
        public string Jabatan { get; set; }

        [MaxLength(128)]
        [EmailAddress]
        public string Email { get; set; }

        [MaxLength(128)]
        public string Telp { get; set; }

        [MaxLength(128)]
        public string HP { get; set; }
    }
}
