﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.ChangeRequest;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Services {
    public interface IDataAknpDriverStore {
        IEnumerable<BaseAknpDriver> LoadDrivers();
        IEnumerable<DataAknpDefinition> LoadDefinitions();
        QueryPair<DataChangeRequest> ListChangeRequests(ChangeRequestDttRequestForm req);
    }
}