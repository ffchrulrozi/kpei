﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.ChangeRequest;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Drivers;

namespace Kpei.PortalAK.DataAccess.ModDataAknp.Services {
    public class DataAknpDriverStore : IDataAknpDriverStore {
        private readonly IEnumerable<BaseAknpDriver> _drivers;
        private readonly AppDbContext _db;

        public DataAknpDriverStore(IEnumerable<BaseAknpDriver> drivers, AppDbContext db) {
            _db = db;
            _drivers = drivers.OrderBy(x => x.DataTitle);
        }

        /// <inheritdoc />
        public IEnumerable<BaseAknpDriver> LoadDrivers() {
            return _drivers;
        }

        /// <inheritdoc />
        public IEnumerable<DataAknpDefinition> LoadDefinitions() {
            return _drivers.Select(x => new DataAknpDefinition(x.DataSlug, x.DataTitle));
        }

        /// <inheritdoc />
        public QueryPair<DataChangeRequest> ListChangeRequests(ChangeRequestDttRequestForm req) {
            var allQ = _db.DataChangeRequests.AsQueryable();
            var q = _db.DataChangeRequests.AsQueryable();
            q = q.Where(x => x.DataDriverSlug != "administrasi-keanggotaan");

                if (!string.IsNullOrWhiteSpace(req.KodeAk)) {
                q = q.Where(x => x.KodeAk == req.KodeAk);
                allQ = allQ.Where(x => x.KodeAk == req.KodeAk);
            }

            var kws = new string[0];
            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                kws = Regex.Split(req.search.value, @"\s+");
            }

            if (kws.Length > 0) {
                q = q.Where(x => kws.All(k => x.Status.Contains(k)) ||
                                 kws.All(k => x.DataDriverSlug.Contains(k)) ||
                                 kws.All(k => x.DataId.Contains(k)) ||
                                 kws.All(k => x.Id.Contains(k)));
            }

            if (!string.IsNullOrWhiteSpace(req.DataSlug)) {
                q = q.Where(x => x.DataDriverSlug == req.DataSlug);
            }

            if (!string.IsNullOrWhiteSpace(req.Status)) {
                q = q.Where(x => x.Status == req.Status);
            }

            


            if (req.FromDateTime != null) {
                var dtv = req.FromDateTime.Value.Date;
                q = q.Where(x => x.CreatedUtc >= dtv);
            }

            if (req.ToDateTime != null) {
                var dtv = req.ToDateTime.Value.Date.AddDays(1);
                q = q.Where(x => x.CreatedUtc <= dtv);
            }

            var col = req.firstOrderColumn?.name ?? nameof(DataChangeRequest.CreatedUtc);
            var dir = req.order.FirstOrDefault()?.IsDesc ?? false ? "desc" : "asc";
            q = q.OrderBy($"{col} {dir}");

            return new QueryPair<DataChangeRequest>(allQ, q);
        }
    }
}