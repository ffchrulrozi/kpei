﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSettings.Models {
    public class StatusKpei {

        private StatusKpei(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<StatusKpei> All() {
            yield return ACTIVE_STATUS;
            yield return RESTRICT_STATUS;
            yield return SUSPEND_STATUS;
            yield return CABUT_STATUS;
        }

        public static readonly StatusKpei ACTIVE_STATUS = new StatusKpei("Active", "Active");
        public static readonly StatusKpei RESTRICT_STATUS = new StatusKpei("Restricted", "Restricted");
        public static readonly StatusKpei SUSPEND_STATUS = new StatusKpei("Suspended", "Suspended");
        public static readonly StatusKpei CABUT_STATUS = new StatusKpei("Closed", "Closed");
    }
}
