﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSettings.Models {
    public class Kewarganegaraan {

        private Kewarganegaraan(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<Kewarganegaraan> All() {
            yield return NASIONAL_STATUS;
            yield return PATUNGAN_STATUS;
        }

        public static readonly Kewarganegaraan NASIONAL_STATUS = new Kewarganegaraan("WNI", "WNI");
        public static readonly Kewarganegaraan PATUNGAN_STATUS = new Kewarganegaraan("WNA", "WNA");
    }
}
