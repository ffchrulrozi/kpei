﻿using Kpei.PortalAK.DataAccess.DbModels;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModSettings.Models {
    public class EmailReplaceKey {
        public string key;
        public string detail;

        public EmailReplaceKey(string paramKey, string paramDetail) {
            key = paramKey;
            detail = paramDetail;
        }

        private static readonly string urlLink = "{Detail.Url}";
        private static readonly string metaUrlLink = "Link detail ke portal";

        private static readonly string aknpUsername = "{Aknp.Username}";
        private static readonly string metaAknpUsername = "Username AK&P";
        private static readonly string aknpCompany = "{Aknp.CompanyName}";
        private static readonly string metaAknpCompany = "Nama Perusahaan AK&P";

        private static readonly string pincodeUsername = "{Pincode.Username}";
        private static readonly string metaPincodeUsername = "Username baru PIN Code";
        private static readonly string pincodePassword = "{Pincode.Password}";
        private static readonly string metaPincodePassword = "Password baru PIN Code";
        private static readonly string pincodeUrl = "{Pincode.Url}";
        private static readonly string metaPincodeUrl = "Url Terkait PIN Code";

        private static readonly string rejectReason = "{Reject.Reason}";
        private static readonly string metaRejectReason = "Alasan Penolakkan";

        private static readonly string acaraJudul = "{Acara.Judul}";
        private static readonly string acaraTglPelaksanaan = "{Acara.TglPelaksanaan}";

        public static List<EmailReplaceKey> Help {
            get {
                return new List<EmailReplaceKey>{
                    new EmailReplaceKey(urlLink, metaUrlLink),
                    // new EmailReplaceKey(aknpUsername, metaAknpUsername),
                    new EmailReplaceKey(aknpCompany, metaAknpCompany),
                    new EmailReplaceKey(pincodeUsername, metaPincodeUsername),
                    new EmailReplaceKey(pincodePassword, metaPincodePassword),
                    new EmailReplaceKey(pincodeUrl, metaPincodeUrl),
                    new EmailReplaceKey(rejectReason, metaRejectReason)
                };
            }
        }

        public static string ReplaceLink(string template, string detailUrl) {
            var newTemplate = template;

            if (detailUrl != null) {
                detailUrl = "<a href=\"" + detailUrl + "\">Link to portal</a>";
                newTemplate = newTemplate.Replace(urlLink, detailUrl);
            }

            return newTemplate;
        }

        public static string ReplaceUser(string template, string user, string company) {
            var newTemplate = template;

            if (user != null) {
                newTemplate = newTemplate.Replace(aknpUsername, user);
            }
            if (company != null) {
                newTemplate = newTemplate.Replace(aknpCompany, company);
            }

            return newTemplate;
        }

        public static string ReplacePincode(string template, string username, string password, string url) {
            var newTemplate = template;
            
            if (username != null) {
                newTemplate = newTemplate.Replace(pincodeUsername, username);
            }
            if (password != null) {
                newTemplate = newTemplate.Replace(pincodePassword, password);
            }
            if (pincodeUrl != null) {
                var newUrl = "<a href=\"" + url + "\">" + url + "</a>";
                newTemplate = newTemplate.Replace(pincodeUrl, newUrl);
            }

            return newTemplate;
        }

        public static string ReplaceReason(string template, string reason) {
            var newTemplate = template;

            if (reason != null) {
                newTemplate = newTemplate.Replace(rejectReason, reason);
            }

            return newTemplate;
        }

        public static string ReplaceEvent(string template, string judul, DateTime? tgl) {
            var newTemplate = template;
            if (judul != null) {
                newTemplate = newTemplate.Replace(acaraJudul, judul);
            }
            if (tgl != null) {
                newTemplate = newTemplate.Replace(acaraTglPelaksanaan, tgl?.ToString("dd MMMM yyyy pukul HH:mm"));
            }
            return newTemplate;
        }
    }
}

