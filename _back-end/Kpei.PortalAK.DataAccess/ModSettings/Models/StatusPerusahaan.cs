﻿using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModSettings.Models {
    public class StatusPerusahaan {

        private StatusPerusahaan(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<StatusPerusahaan> All() {
            yield return NASIONAL_STATUS;
            yield return PATUNGAN_STATUS;
        }

        public static readonly StatusPerusahaan NASIONAL_STATUS = new StatusPerusahaan("NASIONAL", "NASIONAL");
        public static readonly StatusPerusahaan PATUNGAN_STATUS = new StatusPerusahaan("PATUNGAN", "PATUNGAN");
    }
}
