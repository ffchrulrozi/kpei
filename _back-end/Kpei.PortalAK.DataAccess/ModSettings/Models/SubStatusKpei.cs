﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSettings.Models {
    public class SubStatusKpei {
        private SubStatusKpei(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<SubStatusKpei> All() {
            yield return NO_TRADE_STATUS;
            yield return NO_WITHDRAW_STATUS;
            yield return NO_TRADE_NO_WITHDRAW_STATUS;
        }

        public static readonly SubStatusKpei NO_TRADE_STATUS = new SubStatusKpei("No Trading", "No Trading");
        public static readonly SubStatusKpei NO_WITHDRAW_STATUS = new SubStatusKpei("No Withdrawal", "No Withdrawal");
        public static readonly SubStatusKpei NO_TRADE_NO_WITHDRAW_STATUS = new SubStatusKpei("No Withdrawal No Trading", "No Withdrawal No Trading");
    }
}
