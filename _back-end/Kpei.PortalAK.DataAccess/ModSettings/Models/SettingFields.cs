﻿using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModSettings.Models {
    public class SettingFields {

        public static string PINCODE_JENIS = "JenisPincode";
        public static string PINCODE_URL = "URLTerkait";

        public static string SURVEI_PERUSAHAAN = "KategoriPerusahaan";
        public static string SURVEI_DIVISI = "KategoriDivisi";

        public static string EVENT_JENIS_PELATIHAN = "JenisPelatihan";
        public static string EVENT_REMINDER = "Event.Reminder";

        public static string KEUANGAN_PERIODE_AWAL = "Keuangan.Periode.Awal";
        public static string KEUANGAN_PERIODE_AKHIR = "Keuangan.Periode.Akhir";

        public static string KEUANGAN_REMINDER1 = "Keuangan.Periode.Reminder.1";
        public static string KEUANGAN_REMINDER2 = "Keuangan.Periode.Reminder.2";
        public static string KEUANGAN_REMINDER3 = "Keuangan.Periode.Reminder.3";



        #region DataAknp

        #region Akte

        public static string AKNP_SETTING_AKTE_NOTARIS = "Aknp.Setting.Akte.Notaris";

        #endregion

        #region bank pembayaran

        public static string AKNP_SETTING_BANK_PEMBAYARAN = "Aknp.Setting.BankPembayaran";

        #endregion

        #region perjanjian ebu
        
        public static string AKNP_SETTING_JENIS_PERJANJIAN_EBU = "Aknp.Setting.PerjanjianEbu.JenisPerjanjian";
        public static string AKNP_SETTING_STATUS_PERJANJIAN_EBU = "Aknp.Setting.PerjanjianEbu.StatusPerjanjian";

        #endregion

        #region perjanjian kbos

        public static string AKNP_SETTING_JENIS_PERJANJIAN_KBOS = "Aknp.Setting.PerjanjianKbos.JenisPerjanjian";
        public static string AKNP_SETTING_STATUS_PERJANJIAN_KBOS = "Aknp.Setting.PerjanjianKbos.StatusPerjanjian";

        #endregion

        #region perjanjian

        public static string AKNP_SETTING_JENIS_PERJANJIAN = "Aknp.Setting.Perjanjian.JenisPerjanjian";

        #endregion

        #region informasi lain

        public static string AKNP_SETTING_ANGGOTA_BURSA_INFORMASI_LAIN = "Aknp.Setting.InformasiLain.AnggotaBursa";
        public static string AKNP_SETTING_PEMANTAU_INFORMASI_LAIN = "Aknp.Setting.InformasiLain.Pemantau";

        #endregion

        #region pme

        public static string AKNP_SETTING_JENIS_KEANGGOTAAN_PME = "Aknp.Setting.Pme.JenisKeanggotaan";
        public static string AKNP_SETTING_JENIS_PERJANJIAN_LENDER_PME = "Aknp.Setting.Pme.JenisPerjanjianLender";
        public static string AKNP_SETTING_STATUS_LENDER_PME = "Aknp.Setting.Pme.StatusLender";
        public static string AKNP_SETTING_JENIS_PERJANJIAN_BORROWER_PME = "Aknp.Setting.Pme.JenisPerjanjianBorrower";
        public static string AKNP_SETTING_STATUS_BORROWER_PME = "Aknp.Setting.Pme.StatusBorrower";

        #endregion

        #region komisaris

        public static string AKNP_SETTING_JABATAN_KOMISARIS = "Aknp.Setting.Komisaris.Jabatan";

        #endregion

        #region direksi

        public static string AKNP_SETTING_JABATAN_DIREKSI = "Aknp.Setting.Direksi.Jabatan";

        #endregion

        #region contact person

        public static string AKNP_SETTING_JABATAN_CONTACT_PERSON = "Aknp.Setting.ContactPerson.Jabatan";

        #endregion

        #region pejabat berwenang

        public static string AKNP_SETTING_JABATAN_PEJABAT_BERWENANG = "Aknp.Setting.PejabatBerwenang.Jabatan";

        #endregion

        #region Tipe Member Partisipan

        public static string AKNP_SETTING_TIPE_MEMBER_PARTISIPAN = "Aknp.Setting.TipeMember.TipeMemberPartisipan";

        #endregion

        #endregion

        #region e-mail
        
        public static string EMAIL_DATA_NEW_KPEI = "MailTemplate.Data.New.Kpei";
        public static string EMAIL_DATA_NEW_AKNP = "MailTemplate.Data.New.Aknp";
        public static string EMAIL_DATA_NEW_GUEST = "MailTemplate.Data.New.Guest";
        public static string EMAIL_DATA_REJECT_KPEI = "MailTemplate.Data.Reject.Kpei";
        public static string EMAIL_DATA_REJECT_AKNP = "MailTemplate.Data.Reject.Aknp";
        public static string EMAIL_DATA_REJECT_GUEST = "MailTemplate.Data.Reject.Guest";
        public static string EMAIL_DATA_UPDATE_KPEI = "MailTemplate.Data.Update.Kpei";
        public static string EMAIL_DATA_UPDATE_AKNP = "MailTemplate.Data.Update.Aknp";
        public static string EMAIL_DATA_ACTION_KPEI = "MailTemplate.Data.Action.Kpei";
        public static string EMAIL_DATA_ACTION_AKNP = "MailTemplate.Data.Action.Aknp";

        
        public static string EMAIL_ADMINISTRASI_KEANGGOTAAN_NEW_KPEI = "MailTemplate.AdministrasiKeanggotaan.New.Kpei";
        public static string EMAIL_ADMINISTRASI_KEANGGOTAAN_NEW_AKNP = "MailTemplate.AdministrasiKeanggotaan.New.Aknp";
        public static string EMAIL_ADMINISTRASI_KEANGGOTAAN_NEW_GUEST = "MailTemplate.AdministrasiKeanggotaan.New.Guest";
        public static string EMAIL_ADMINISTRASI_KEANGGOTAAN_REJECT_KPEI = "MailTemplate.AdministrasiKeanggotaan.Reject.Kpei";
        public static string EMAIL_ADMINISTRASI_KEANGGOTAAN_REJECT_AKNP = "MailTemplate.AdministrasiKeanggotaan.Reject.Aknp";
        public static string EMAIL_ADMINISTRASI_KEANGGOTAAN_REJECT_GUEST = "MailTemplate.AdministrasiKeanggotaan.Reject.Guest";
        public static string EMAIL_ADMINISTRASI_KEANGGOTAAN_UPDATE_KPEI = "MailTemplate.AdministrasiKeanggotaan.Update.Kpei";
        public static string EMAIL_ADMINISTRASI_KEANGGOTAAN_UPDATE_AKNP = "MailTemplate.AdministrasiKeanggotaan.Update.Aknp";
        public static string EMAIL_ADMINISTRASI_KEANGGOTAAN_ACTION_KPEI = "MailTemplate.AdministrasiKeanggotaan.Action.Kpei";
        public static string EMAIL_ADMINISTRASI_KEANGGOTAAN_ACTION_AKNP = "MailTemplate.AdministrasiKeanggotaan.Action.Aknp";
        
        public static string EMAIL_PINCODE_NEW_KPEI = "MailTemplate.Pincode.New.Kpei";
        public static string EMAIL_PINCODE_NEW_AKNP = "MailTemplate.Pincode.New.Aknp";
        public static string EMAIL_PINCODE_ACTION_KPEI = "MailTemplate.Pincode.Action.Kpei";
        public static string EMAIL_PINCODE_ACTION_AKNP = "MailTemplate.Pincode.Action.Aknp";
        public static string EMAIL_PINCODE_REJECT_KPEI = "MailTemplate.Pincode.Reject.Kpei";
        public static string EMAIL_PINCODE_REJECT_AKNP = "MailTemplate.Pincode.Reject.Aknp";
        public static string EMAIL_PINCODE_SEND_AKNP = "MailTemplate.Pincode.SendCode.Aknp";
        public static string EMAIL_PINCODE_SEND_DIREKSI = "MailTemplate.Pincode.SendCode.Direksi";

        public static string EMAIL_MCLEARS_NEW_KPEI = "MailTemplate.Mclears.New.Kpei";
        public static string EMAIL_MCLEARS_NEW_AKNP = "MailTemplate.Mclears.New.Aknp";
        public static string EMAIL_MCLEARS_UPDATE_KPEI = "MailTemplate.Mclears.Update.Kpei";
        public static string EMAIL_MCLEARS_UPDATE_AKNP = "MailTemplate.Mclears.Update.Aknp";
        public static string EMAIL_MCLEARS_ACTION_KPEI = "MailTemplate.Mclears.Action.Kpei";
        public static string EMAIL_MCLEARS_ACTION_AKNP = "MailTemplate.Mclears.Action.Aknp";

        public static string EMAIL_EVENT_NEW_KPEI = "MailTemplate.Event.New.Kpei";
        public static string EMAIL_EVENT_UPDATE_KPEI = "MailTemplate.Event.Update.Kpei";
        public static string EMAIL_EVENT_UPDATE_AKNP = "MailTemplate.Event.Update.Aknp";
        public static string EMAIL_EVENT_REGISTER_KPEI = "MailTemplate.Event.Register.Kpei";
        public static string EMAIL_EVENT_REGISTER_AKNP = "MailTemplate.Event.Register.Aknp";
        public static string EMAIL_EVENT_UPDATEREG_KPEI = "MailTemplate.Event.UpdateReg.Kpei";
        public static string EMAIL_EVENT_UPDATEREG_AKNP = "MailTemplate.Event.UpdateReg.Aknp";
        public static string EMAIL_EVENT_REMIND_AKNP = "MailTemplate.Event.Remind.Aknp";

        public static string EMAIL_KEUANGAN_NEW_KPEI = "MailTemplate.Keuangan.New.Kpei";
        public static string EMAIL_KEUANGAN_NEW_AKNP = "MailTemplate.Keuangan.New.Aknp";
        public static string EMAIL_KEUANGAN_UPDATE_KPEI = "MailTemplate.Keuangan.Update.Kpei";
        public static string EMAIL_KEUANGAN_UPDATE_AKNP = "MailTemplate.Keuangan.Update.Aknp";
        public static string EMAIL_KEUANGAN_ACTION_KPEI = "MailTemplate.Keuangan.Action.Kpei";
        public static string EMAIL_KEUANGAN_ACTION_AKNP = "MailTemplate.Keuangan.Action.Aknp";
        public static string EMAIL_KEUANGAN_REJECT_KPEI = "MailTemplate.Keuangan.Reject.Kpei";
        public static string EMAIL_KEUANGAN_REJECT_AKNP = "MailTemplate.Keuangan.Reject.Aknp";
        public static string EMAIL_KEUANGAN_REMIND_AKNP = "MailTemplate.Keuangan.Remind.Aknp";

        public static string EMAIL_SURVEI_NEW_KPEI = "MailTemplate.Survei.New.Kpei";
        public static string EMAIL_SURVEI_UPDATE_KPEI = "MailTemplate.Survei.Update.Kpei";

        #endregion

        #region Email Header

        
        public static string HEADER_ADMINISTRASI_KEANGGOTAAN_NEW_KPEI = "MailHeader.AdministrasiKeanggotaan.New.Kpei";
        public static string HEADER_ADMINISTRASI_KEANGGOTAAN_NEW_AKNP = "MailHeader.AdministrasiKeanggotaan.New.Aknp";
        public static string HEADER_ADMINISTRASI_KEANGGOTAAN_NEW_GUEST = "MailHeader.AdministrasiKeanggotaan.New.Guest";
        public static string HEADER_ADMINISTRASI_KEANGGOTAAN_REJECT_KPEI = "MailHeader.AdministrasiKeanggotaan.Reject.Kpei";
        public static string HEADER_ADMINISTRASI_KEANGGOTAAN_REJECT_AKNP = "MailHeader.AdministrasiKeanggotaan.Reject.Aknp";
        public static string HEADER_ADMINISTRASI_KEANGGOTAAN_REJECT_GUEST = "MailHeader.AdministrasiKeanggotaan.Reject.Guest";
        public static string HEADER_ADMINISTRASI_KEANGGOTAAN_UPDATE_KPEI = "MailHeader.AdministrasiKeanggotaan.Update.Kpei";
        public static string HEADER_ADMINISTRASI_KEANGGOTAAN_UPDATE_AKNP = "MailHeader.AdministrasiKeanggotaan.Update.Aknp";
        public static string HEADER_ADMINISTRASI_KEANGGOTAAN_ACTION_KPEI = "MailHeader.AdministrasiKeanggotaan.Action.Kpei";
        public static string HEADER_ADMINISTRASI_KEANGGOTAAN_ACTION_AKNP = "MailHeader.AdministrasiKeanggotaan.Action.Aknp";

        public static string HEADER_DATA_NEW_KPEI = "MailHeader.Data.New.Kpei";
        public static string HEADER_DATA_NEW_AKNP = "MailHeader.Data.New.Aknp";
        public static string HEADER_DATA_NEW_GUEST = "MailHeader.Data.New.Guest";
        public static string HEADER_DATA_REJECT_KPEI = "MailHeader.Data.Reject.Kpei";
        public static string HEADER_DATA_REJECT_AKNP = "MailHeader.Data.Reject.Aknp";
        public static string HEADER_DATA_REJECT_GUEST = "MailHeader.Data.Reject.Guest";
        public static string HEADER_DATA_UPDATE_KPEI = "MailHeader.Data.Update.Kpei";
        public static string HEADER_DATA_UPDATE_AKNP = "MailHeader.Data.Update.Aknp";
        public static string HEADER_DATA_ACTION_KPEI = "MailHeader.Data.Action.Kpei";
        public static string HEADER_DATA_ACTION_AKNP = "MailHeader.Data.Action.Aknp";

        public static string HEADER_PINCODE_NEW_KPEI = "MailHeader.Pincode.New.Kpei";
        public static string HEADER_PINCODE_NEW_AKNP = "MailHeader.Pincode.New.Aknp";
        public static string HEADER_PINCODE_ACTION_KPEI = "MailHeader.Pincode.Action.Kpei";
        public static string HEADER_PINCODE_ACTION_AKNP = "MailHeader.Pincode.Action.Aknp";
        public static string HEADER_PINCODE_REJECT_KPEI = "MailHeader.Pincode.Reject.Kpei";
        public static string HEADER_PINCODE_REJECT_AKNP = "MailHeader.Pincode.Reject.Aknp";
        public static string HEADER_PINCODE_SEND_AKNP = "MailHeader.Pincode.SendCode.Aknp";
        public static string HEADER_PINCODE_SEND_DIREKSI = "MailHeader.Pincode.SendCode.Direksi";

        public static string HEADER_MCLEARS_NEW_KPEI = "MailHeader.Mclears.New.Kpei";
        public static string HEADER_MCLEARS_NEW_AKNP = "MailHeader.Mclears.New.Aknp";
        public static string HEADER_MCLEARS_UPDATE_KPEI = "MailHeader.Mclears.Update.Kpei";
        public static string HEADER_MCLEARS_UPDATE_AKNP = "MailHeader.Mclears.Update.Aknp";
        public static string HEADER_MCLEARS_ACTION_KPEI = "MailHeader.Mclears.Action.Kpei";
        public static string HEADER_MCLEARS_ACTION_AKNP = "MailHeader.Mclears.Action.Aknp";

        public static string HEADER_EVENT_NEW_KPEI = "MailHeader.Event.New.Kpei";
        public static string HEADER_EVENT_UPDATE_KPEI = "MailHeader.Event.Update.Kpei";
        public static string HEADER_EVENT_UPDATE_AKNP = "MailHeader.Event.Update.Aknp";
        public static string HEADER_EVENT_REGISTER_KPEI = "MailHeader.Event.Register.Kpei";
        public static string HEADER_EVENT_REGISTER_AKNP = "MailHeader.Event.Register.Aknp";
        public static string HEADER_EVENT_UPDATEREG_KPEI = "MailHeader.Event.UpdateReg.Kpei";
        public static string HEADER_EVENT_UPDATEREG_AKNP = "MailHeader.Event.UpdateReg.Aknp";
        public static string HEADER_EVENT_REMIND_AKNP = "MailHeader.Event.Remind.Aknp";

        public static string HEADER_KEUANGAN_NEW_KPEI = "MailHeader.Keuangan.New.Kpei";
        public static string HEADER_KEUANGAN_NEW_AKNP = "MailHeader.Keuangan.New.Aknp";
        public static string HEADER_KEUANGAN_UPDATE_KPEI = "MailHeader.Keuangan.Update.Kpei";
        public static string HEADER_KEUANGAN_UPDATE_AKNP = "MailHeader.Keuangan.Update.Aknp";
        public static string HEADER_KEUANGAN_ACTION_KPEI = "MailHeader.Keuangan.Action.Kpei";
        public static string HEADER_KEUANGAN_ACTION_AKNP = "MailHeader.Keuangan.Action.Aknp";
        public static string HEADER_KEUANGAN_REJECT_KPEI = "MailHeader.Keuangan.Reject.Kpei";
        public static string HEADER_KEUANGAN_REJECT_AKNP = "MailHeader.Keuangan.Reject.Aknp";
        public static string HEADER_KEUANGAN_REMIND_AKNP = "MailHeader.Keuangan.Remind.Aknp";

        public static string HEADER_SURVEI_NEW_KPEI = "MailHeader.Survei.New.Kpei";
        public static string HEADER_SURVEI_UPDATE_KPEI = "MailHeader.Survei.Update.Kpei";

        #endregion
    }
}