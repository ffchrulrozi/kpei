﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSettings.Models {
    public class StatusBursa {

        private StatusBursa(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<StatusBursa> All() {
            yield return ACTIVE_STATUS;
            yield return SUSPEND_STATUS;
            yield return CABUT_STATUS;
        }

        public static IEnumerable<StatusBursa> Aktif() {
            yield return ACTIVE_STATUS;
            yield return ACTIVE_STATUS_ID;
        }

        public static readonly StatusBursa ACTIVE_STATUS = new StatusBursa("ACTIVE", "ACTIVE");
        public static readonly StatusBursa ACTIVE_STATUS_ID = new StatusBursa("AKTIF", "AKTIF");
        public static readonly StatusBursa SUSPEND_STATUS = new StatusBursa("SUSPEND", "SUSPEND");
        public static readonly StatusBursa CABUT_STATUS = new StatusBursa("CABUT", "CABUT");
    }
}
