﻿using Kpei.PortalAK.DataAccess.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSettings.Models {
    public class EmailSetting {
        public EmailHeader Header { get; set; }

        private Setting _template;
        public Setting Template {
            get {
                return _template ?? new Setting();
            }
            set {
                _template = value;
            }
        }

        public EmailSetting() {
        }
        public EmailSetting(EmailHeader header, Setting template) {
            Header = header;
            Template = template;
        }
    }
}
