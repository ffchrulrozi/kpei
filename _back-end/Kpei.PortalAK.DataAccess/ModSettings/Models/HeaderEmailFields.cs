﻿using Kpei.PortalAK.DataAccess.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSettings.Models {
    public class EmailHeader {
        public string Key { get; set; }
        public string Header { get; set; }
        public string Section { get; set; }

        public EmailHeader(string key, string header, string section) {
            Key = key;
            Header = header;
            Section = section;
        }

        #region header

        private static readonly string moduleAknp = "[Data AK&P] ";
        private static readonly string modulePincode = "[PIN Code] ";
        private static readonly string moduleMclears = "[Layanan m-CLEARS] ";
        private static readonly string moduleAdministrasiKeanggotaan = "[Administrasi Keanggotaan] ";
        private static readonly string moduleSosialisasi = "[Sosialisasi dan Pelatihan] ";
        private static readonly string moduleKeuangan = "[Laporan Keuangan] ";
        private static readonly string moduleSurvei = "[Survei] ";

        private static readonly string newRegDataHeaderKpei = "Ada Registrasi baru!";
        private static readonly string newRegDataHeaderAknp = "Registrasi anda telah dimasukkan!";
        private static readonly string newDataHeaderKpei = "Request baru!";
        private static readonly string newDataHeaderAknp = "Request telah berhasil dimasukkan!";
        private static readonly string updateDataHeader = "Pembaharuan terhadap request!";
        private static readonly string actionDataHeader = "Tindak lanjut terhadap request telah berhasil diproses!";
        private static readonly string remindDataHeader = "Reminder";

        private static readonly string newRegistrationHeader = "Peserta baru berhasil dimasukkan!";
        private static readonly string updateRegistrationHeader = "Pembaharuan terhadap peserta!";

        private static readonly string newLaporanHeaderKpei = "Laporan baru!";
        private static readonly string newLaporanHeaderAknp = "Laporan telah berhasil dimasukkan!";
        private static readonly string updateLaporanHeader = "Pembaharuan terhadap laporan!";
        private static readonly string actionLaporanHeader = "Request telah disetujui!";
        private static readonly string rejectLaporanHeader = "Request ditolak!";
        private static readonly string remindLaporanHeader = "Reminder Laporan Keuangan";

        private static readonly string newEmailKpei = "Notifikasi Email Data Baru untuk KPEI";
        private static readonly string newEmailAknp = "Notifikasi Email Data Baru untuk AK&P";

        private static readonly string updateEmailKpei = "Notifikasi Email Perubahan untuk KPEI";
        private static readonly string updateEmailAknp = "Notifikasi Email Perubahan untuk AK&P";

        private static readonly string actionEmailKpei = "Notifikasi Email Approve untuk KPEI";
        private static readonly string actionEmailAknp = "Notifikasi Email Approve untuk AK&P";
        private static readonly string rejectEmailKpei = "Notifikasi Email Reject untuk KPEI";
        private static readonly string rejectEmailAknp = "Notifikasi Email Reject untuk AK&P";

        private static readonly string regEmailKpei = "Notifikasi Email Registrasi Peserta Baru untuk KPEI";
        private static readonly string regEmailAknp = "Notifikasi Email Registrasi Peserta Baru untuk AK&P";

        private static readonly string updateRegEmailKpei = "Notifikasi Email Update Peserta untuk KPEI";
        private static readonly string updateRegEmailAknp = "Notifikasi Email Update Peserta untuk AK&P";

        #endregion

        #region Aknp     

        private static readonly IEnumerable<EmailHeader> AknpHeaderList = new List<EmailHeader> {
            new EmailHeader(SettingFields.HEADER_DATA_NEW_KPEI, moduleAknp + newDataHeaderKpei, newEmailKpei),
            new EmailHeader(SettingFields.HEADER_DATA_NEW_AKNP, moduleAknp + newDataHeaderAknp, newEmailAknp),
            // new EmailHeader(SettingFields.HEADER_DATA_NEW_GUEST, moduleAknp + newDataHeaderAknp, "Notifikasi Email Registrasi untuk Guest"),
            // new EmailHeader(SettingFields.HEADER_DATA_REJECT_KPEI, moduleAknp + actionDataHeader, rejectEmailKpei),
            new EmailHeader(SettingFields.HEADER_DATA_REJECT_AKNP, moduleAknp + actionDataHeader, rejectEmailAknp),
            new EmailHeader(SettingFields.HEADER_DATA_REJECT_GUEST, moduleAknp + actionDataHeader, "Notifikasi Email Penolakan untuk Guest"),
            new EmailHeader(SettingFields.HEADER_DATA_UPDATE_KPEI, moduleAknp + updateDataHeader, updateEmailKpei),
            new EmailHeader(SettingFields.HEADER_DATA_UPDATE_AKNP, moduleAknp + updateDataHeader, updateEmailAknp),
            // new EmailHeader(SettingFields.HEADER_DATA_ACTION_KPEI, moduleAknp + actionDataHeader, actionEmailKpei),
            new EmailHeader(SettingFields.HEADER_DATA_ACTION_AKNP, moduleAknp + actionDataHeader, actionEmailAknp)
        };

        public static IEnumerable<EmailHeader> AknpHeader() {
            return AknpHeaderList;
        }

        #endregion

        #region AdministrasiKeanggotaan   

        private static readonly IEnumerable<EmailHeader> AdministrasiKeanggotaanHeaderList = new List<EmailHeader> {
            new EmailHeader(SettingFields.HEADER_ADMINISTRASI_KEANGGOTAAN_NEW_KPEI, moduleAdministrasiKeanggotaan + newDataHeaderKpei, newEmailKpei),
            new EmailHeader(SettingFields.HEADER_ADMINISTRASI_KEANGGOTAAN_NEW_AKNP, moduleAdministrasiKeanggotaan + newDataHeaderAknp, newEmailAknp),
            // new EmailHeader(SettingFields.HEADER_ADMINISTRASI_KEANGGOTAAN_REJECT_KPEI, moduleAdministrasiKeanggotaan + actionDataHeader, rejectEmailKpei),
            new EmailHeader(SettingFields.HEADER_ADMINISTRASI_KEANGGOTAAN_REJECT_AKNP, moduleAdministrasiKeanggotaan + actionDataHeader, rejectEmailAknp),
            new EmailHeader(SettingFields.HEADER_ADMINISTRASI_KEANGGOTAAN_UPDATE_KPEI, moduleAdministrasiKeanggotaan + updateDataHeader, updateEmailKpei),
            new EmailHeader(SettingFields.HEADER_ADMINISTRASI_KEANGGOTAAN_UPDATE_AKNP, moduleAdministrasiKeanggotaan + updateDataHeader, updateEmailAknp),
            // new EmailHeader(SettingFields.HEADER_ADMINISTRASI_KEANGGOTAAN_ACTION_KPEI, moduleAdministrasiKeanggotaan + actionDataHeader, actionEmailKpei),
            new EmailHeader(SettingFields.HEADER_ADMINISTRASI_KEANGGOTAAN_ACTION_AKNP, moduleAdministrasiKeanggotaan + actionDataHeader, actionEmailAknp)
        };

        public static IEnumerable<EmailHeader> AdministrasiKeanggotaanHeader() {
            return AdministrasiKeanggotaanHeaderList;
        }

        #endregion

        #region Pincode

        private static readonly IEnumerable<EmailHeader> PincodeHeaderList = new List<EmailHeader> {
            new EmailHeader(SettingFields.HEADER_PINCODE_NEW_KPEI, modulePincode + newDataHeaderKpei, newEmailKpei),
            new EmailHeader(SettingFields.HEADER_PINCODE_NEW_AKNP, modulePincode + newDataHeaderAknp, newEmailAknp),
            new EmailHeader(SettingFields.HEADER_PINCODE_ACTION_KPEI, modulePincode + actionDataHeader, actionEmailKpei),
            new EmailHeader(SettingFields.HEADER_PINCODE_ACTION_AKNP, modulePincode + actionDataHeader, actionEmailAknp),
            new EmailHeader(SettingFields.HEADER_PINCODE_REJECT_KPEI, modulePincode + actionDataHeader, rejectEmailKpei),
            new EmailHeader(SettingFields.HEADER_PINCODE_REJECT_AKNP, modulePincode + actionDataHeader, rejectEmailAknp),
            new EmailHeader(SettingFields.HEADER_PINCODE_SEND_AKNP, modulePincode + actionDataHeader, "Notifikasi Username dan Password terkirim untuk AK&P"),
            new EmailHeader(SettingFields.HEADER_PINCODE_SEND_DIREKSI, modulePincode + actionDataHeader, "Notifikasi Username dan Password terkirim untuk Direksi"),
        };

        public static IEnumerable<EmailHeader> PincodeHeader() {
            return PincodeHeaderList;
        }


        #endregion

        #region Laporan Keuangan

        private static readonly IEnumerable<EmailHeader> KeuanganHeaderList = new List<EmailHeader> {
                new EmailHeader(SettingFields.HEADER_KEUANGAN_NEW_KPEI, moduleKeuangan + newLaporanHeaderKpei, newEmailKpei),
                new EmailHeader(SettingFields.HEADER_KEUANGAN_NEW_AKNP, moduleKeuangan + newLaporanHeaderAknp, newEmailAknp),
                new EmailHeader(SettingFields.HEADER_KEUANGAN_UPDATE_KPEI, moduleKeuangan + updateLaporanHeader, updateEmailKpei),
                new EmailHeader(SettingFields.HEADER_KEUANGAN_UPDATE_AKNP, moduleKeuangan + updateLaporanHeader, updateEmailAknp),
                new EmailHeader(SettingFields.HEADER_KEUANGAN_ACTION_KPEI, moduleKeuangan + actionLaporanHeader, actionEmailKpei),
                new EmailHeader(SettingFields.HEADER_KEUANGAN_ACTION_AKNP, moduleKeuangan + actionLaporanHeader, actionEmailAknp),
                new EmailHeader(SettingFields.HEADER_KEUANGAN_REJECT_KPEI, moduleKeuangan + actionLaporanHeader, rejectEmailKpei),
                new EmailHeader(SettingFields.HEADER_KEUANGAN_REJECT_AKNP, moduleKeuangan + actionLaporanHeader, rejectEmailAknp),
                new EmailHeader(SettingFields.HEADER_KEUANGAN_REMIND_AKNP, moduleKeuangan + remindLaporanHeader, remindLaporanHeader),
        };

        public static IEnumerable<EmailHeader> KeuanganHeader() {
            return KeuanganHeaderList;
        }
        #endregion

        #region Survei

        private static readonly IEnumerable<EmailHeader> SurveiHeaderList = new List<EmailHeader> {
            new EmailHeader(SettingFields.HEADER_SURVEI_NEW_KPEI, moduleSurvei + newDataHeaderKpei, newEmailKpei),
            new EmailHeader(SettingFields.HEADER_SURVEI_UPDATE_KPEI, moduleSurvei + updateDataHeader, updateEmailKpei),
        };

        public static IEnumerable<EmailHeader> SurveiHeader() {
            return SurveiHeaderList;
        }

        #endregion

        #region m-CLEARS

        private static readonly IEnumerable<EmailHeader> MclearsHeaderList = new List<EmailHeader> {
            new EmailHeader(SettingFields.HEADER_MCLEARS_NEW_KPEI, moduleMclears + newDataHeaderKpei, newEmailKpei),
            new EmailHeader(SettingFields.HEADER_MCLEARS_NEW_AKNP, moduleMclears + newDataHeaderAknp, newEmailAknp),
            new EmailHeader(SettingFields.HEADER_MCLEARS_UPDATE_KPEI, moduleMclears + updateDataHeader, updateEmailKpei),
            new EmailHeader(SettingFields.HEADER_MCLEARS_UPDATE_AKNP, moduleMclears + updateDataHeader, updateEmailAknp),
            new EmailHeader(SettingFields.HEADER_MCLEARS_ACTION_KPEI, moduleMclears + actionDataHeader, actionEmailKpei),
            new EmailHeader(SettingFields.HEADER_MCLEARS_ACTION_AKNP, moduleMclears + actionDataHeader, actionEmailAknp),
        };

        public static IEnumerable<EmailHeader> MclearsHeader() {
            return MclearsHeaderList;
        }

        #endregion

        #region Sosialisasi dan Pelatihan

        private static readonly IEnumerable<EmailHeader> SospelHeaderList = new List<EmailHeader> {
            new EmailHeader(SettingFields.HEADER_EVENT_NEW_KPEI, moduleSosialisasi + newDataHeaderKpei, newEmailKpei),
            new EmailHeader(SettingFields.HEADER_EVENT_UPDATE_KPEI, moduleSosialisasi + updateDataHeader, updateEmailKpei),
            new EmailHeader(SettingFields.HEADER_EVENT_UPDATE_AKNP, moduleSosialisasi + updateDataHeader, updateEmailAknp),
            new EmailHeader(SettingFields.HEADER_EVENT_REGISTER_KPEI, moduleSosialisasi + newRegistrationHeader, regEmailKpei),
            new EmailHeader(SettingFields.HEADER_EVENT_REGISTER_AKNP, moduleSosialisasi + newRegistrationHeader, regEmailAknp),
            new EmailHeader(SettingFields.HEADER_EVENT_UPDATEREG_KPEI, moduleSosialisasi + updateRegistrationHeader, updateRegEmailKpei),
            new EmailHeader(SettingFields.HEADER_EVENT_UPDATEREG_AKNP, moduleSosialisasi + updateRegistrationHeader, updateRegEmailAknp),
            new EmailHeader(SettingFields.HEADER_EVENT_REMIND_AKNP, moduleSosialisasi + remindDataHeader, remindDataHeader),
        };

        public static IEnumerable<EmailHeader> SospelHeader() {
            return SospelHeaderList;
        }

        #endregion

        public static IEnumerable<EmailHeader> All() {
            var list = new List<EmailHeader>();
            list.AddRange(AknpHeaderList);
            list.AddRange(PincodeHeaderList);
            list.AddRange(MclearsHeaderList);
            list.AddRange(AdministrasiKeanggotaanHeaderList);
            list.AddRange(SospelHeaderList);
            list.AddRange(KeuanganHeaderList);
            list.AddRange(SurveiHeaderList);
            return list;
        }
    }
}

