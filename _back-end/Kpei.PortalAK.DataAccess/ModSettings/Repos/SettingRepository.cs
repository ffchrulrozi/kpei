﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;

namespace Kpei.PortalAK.DataAccess.ModSettings.Repos {
    public class SettingRepository : ISettingRepository {
        private readonly AppDbContext _context;
        private readonly IUserActionLogService _ulog;

        public SettingRepository(AppDbContext context, IUserActionLogService ulog) {
            _context = context;
            _ulog = ulog;
        }

        /// <inheritdoc />
        public GlobalSettingForm GlobalSetting(bool useCache = true) {

            var cacheKey = typeof(GlobalSettingForm).FullName;
            GlobalSettingForm stg;
            if (useCache) {
                stg = HttpContext.Current?.Items[cacheKey] as GlobalSettingForm;
                if (stg != null) return stg;
            }

            var dbstg = _context.GlobalSettings.FirstOrDefault();
            if (dbstg == null) {
                stg = new GlobalSettingForm {
                    GuestAknpCode = "0000",
                    MaxCompanyPersonCount = 10,
                    MaxSubscribePesertaMclears = 10,
                    EmailDebugMode = false
                };
            } else {
                stg = new GlobalSettingForm {
                    GuestAknpCode = dbstg.GuestAknpCode,
                    MaxCompanyPersonCount = dbstg.MaxCompanyPersonCount,
                    MaxSubscribePesertaMclears = dbstg.MaxSubscribePesertaMclears,
                    EmailDebugMode = dbstg.EmailDebugMode,
                    EmailDebugAddress = dbstg.EmailDebugAddress
                };
                stg.KkeGroupEmails.AddRange(dbstg.KkeGroupEmails.Select(x => x.EmailAddress)
                    .Where(x => !string.IsNullOrWhiteSpace(x)));
            }

            if (HttpContext.Current != null) {
                HttpContext.Current.Items[cacheKey] = stg;
            }
            return stg;

        }

        /// <inheritdoc />
        public void UpdateGlobalSetting(GlobalSettingForm form) {
            var dbstg = _context.GlobalSettings.FirstOrDefault();
            if (dbstg == null) {
                dbstg = new GlobalSetting();
                _context.GlobalSettings.Add(dbstg);
            }

            dbstg.GuestAknpCode = form.GuestAknpCode;
            dbstg.MaxCompanyPersonCount = form.MaxCompanyPersonCount;
            dbstg.MaxSubscribePesertaMclears = form.MaxSubscribePesertaMclears;
            dbstg.EmailDebugMode = form.EmailDebugMode;
            dbstg.EmailDebugAddress = form.EmailDebugAddress;

            foreach (var item in dbstg.KkeGroupEmails.ToList()) {
                _context.Entry(item).State = EntityState.Deleted;
                dbstg.KkeGroupEmails.Remove(item);
            }

            foreach (var em in form.KkeGroupEmails) {
                if (string.IsNullOrWhiteSpace(em)) continue;
                dbstg.KkeGroupEmails.Add(new GlobalSettingKkeGroupEmail {
                    GlobalSettingId = dbstg.Id,
                    EmailAddress = em
                });
            }

            _context.SaveChanges();
        }

        /// <inheritdoc />
        public PincodeSettingForm PincodeSetting(bool useCache = true) {
            var cacheKey = typeof(PincodeSettingForm).FullName;
            PincodeSettingForm stg;
            if (useCache) {
                stg = HttpContext.Current?.Items[cacheKey] as PincodeSettingForm;
                if (stg != null) return stg;
            }

            var dbstg = _context.PincodeSettings.FirstOrDefault();

            stg = new PincodeSettingForm();

            if (dbstg != null) {
                stg.EclearsAdmins = dbstg.PincodeEclearsAdmins.Select(x => new PincodeEclearsAdmin {
                    Email = x.Email,
                    Name = x.Name
                }).ToList();
                stg.PincodeTypeInfos = dbstg.PincodeTypeInfos.Select(x => new PincodeTypeInfo {
                    Name = x.Name,
                    Url = x.Url
                }).ToArray().OrderBy(x => x.Name).ToList();
            }

            if (HttpContext.Current != null) {
                HttpContext.Current.Items[cacheKey] = stg;
            }
            return stg;
        }

        /// <inheritdoc />
        public void UpdatePincodeSetting(PincodeSettingForm form) {
            var dbstg = _context.PincodeSettings.FirstOrDefault();
            if (dbstg == null) {
                dbstg = new PincodeSetting();
                _context.PincodeSettings.Add(dbstg);
            }

            foreach (var item in dbstg.PincodeEclearsAdmins.ToList()) {
                _context.Entry(item).State = EntityState.Deleted;
                dbstg.PincodeEclearsAdmins.Remove(item);
            }

            foreach (var item in dbstg.PincodeTypeInfos.ToList()) {
                _context.Entry(item).State = EntityState.Deleted;
                dbstg.PincodeTypeInfos.Remove(item);
            }

            foreach (var x in form.EclearsAdmins) {
                if (x == null || string.IsNullOrWhiteSpace(x.Name) || string.IsNullOrWhiteSpace(x.Email)) continue;
                dbstg.PincodeEclearsAdmins.Add(new PincodeEclearsAdminTable {
                    Name = x.Name,
                    Email = x.Email,
                    PincodeSettingId = dbstg.Id
                });
            }

            foreach (var x in form.PincodeTypeInfos) {
                if (x == null || string.IsNullOrWhiteSpace(x.Name) || string.IsNullOrWhiteSpace(x.Url)) continue;
                dbstg.PincodeTypeInfos.Add(new PincodeTypeInfoTable {
                    Name = x.Name,
                    Url = x.Url,
                    PincodeSettingId = dbstg.Id
                });
            }

            _context.SaveChanges();
        }

        /// <inheritdoc />
        public SmtpEmailSettingForm SmtpEmailSetting(bool useCache = true) {
            var cacheKey = typeof(SmtpEmailSettingForm).FullName;
            SmtpEmailSettingForm stg;
            if (useCache) {
                stg = HttpContext.Current?.Items[cacheKey] as SmtpEmailSettingForm;
                if (stg != null) return stg;
            }

            stg = new SmtpEmailSettingForm();
            var dbstg = _context.SmtpEmailSettings.FirstOrDefault();
            if (dbstg != null) {
                stg.FromEmailAddress = dbstg.FromEmailAddress;
                stg.SmtpServerAddress = dbstg.SmtpServerAddress;
                stg.SmtpServerPort = dbstg.SmtpServerPort ?? 25;
                stg.SmtpUserName = dbstg.SmtpUserName;
                stg.SmtpUserPassword = dbstg.SmtpUserPassword;
                stg.SmtpEnableSsl = dbstg.SmtpEnableSsl ?? false;
            }

            if (HttpContext.Current != null) {
                HttpContext.Current.Items[cacheKey] = stg;
            }
            return stg;
        }

        /// <inheritdoc />
        public void UpdateSmtpEmailSetting(SmtpEmailSettingForm form) {
            var dbstg = _context.SmtpEmailSettings.FirstOrDefault();
            if (dbstg == null) {
                dbstg = new SmtpEmailSetting();
                _context.SmtpEmailSettings.Add(dbstg);
            }

            dbstg.FromEmailAddress = form.FromEmailAddress;
            dbstg.SmtpServerAddress = form.SmtpServerAddress;
            dbstg.SmtpServerPort = form.SmtpServerPort;
            dbstg.SmtpUserName = form.SmtpUserName;
            dbstg.SmtpUserPassword = form.SmtpUserPassword;
            dbstg.SmtpEnableSsl = form.SmtpEnableSsl;

            _context.SaveChanges();
        }

        public DeadlineSettingForm DeadlineSetting(bool useCache = true) {
            var dbstg = _context.Deadline.FirstOrDefault();
            var form = new DeadlineSettingForm();
            if (dbstg != null) {
                form.Init(dbstg);
            }
            return form;
        }

        public void UpdateDeadlineSetting(DeadlineSettingForm form) {
            var dbstg = _context.Deadline.FirstOrDefault();
            if (dbstg == null) {
                dbstg = new Deadline();
                _context.Deadline.Add(dbstg);
            }
            dbstg.KeuanganAwalPeriodeUtc = form.KeuanganAwalPeriodeUtc;
            dbstg.KeuanganAkhirPeriodeUtc = form.KeuanganAkhirPeriodeUtc;
            dbstg.Reminder1 = form.Reminder1;

            _context.SaveChanges();
        }

        /// <inheritdoc />
        public SurveySettingForm SurveySetting(bool useCache = true) {
            return new SurveySettingForm {
                KategoriPerusahaan = _context.Setting.Where(e => e.Field == SettingFields.SURVEI_PERUSAHAAN).ToList(),
                KategoriDivisi = _context.Setting.Where(e => e.Field == SettingFields.SURVEI_DIVISI).ToList()
            };
            // todo make it real!
        }

        /// <inheritdoc />
        public void UpdateSurveySetting(SurveySettingForm form) {
            // todo make it real
        }

        public KeuanganSettingForm KeuanganSetting(bool useCache = true) {
            return new KeuanganSettingForm {
                TengahTahunUtc = _context.Setting.FirstOrDefault(e => e.Field == SettingFields.KEUANGAN_PERIODE_AWAL),
                AkhirTahunUtc = _context.Setting.FirstOrDefault(e => e.Field == SettingFields.KEUANGAN_PERIODE_AKHIR),
                FirstReminder = _context.Setting.FirstOrDefault(e => e.Field == SettingFields.KEUANGAN_REMINDER1)
            };
        }

        public AknpSettingForm AknpSetting(bool useCache = true) {
            var cacheKey = typeof(AknpSettingForm).FullName;
            AknpSettingForm stg;
            if (useCache) {
                stg = HttpContext.Current?.Items[cacheKey] as AknpSettingForm;
                if (stg != null) return stg;
            }
            var GuestAknpCode = GlobalSetting().GuestAknpCode;
            stg = new AknpSettingForm {
                ListNotaris = _context.Setting.Where(e => e.Field == SettingFields.AKNP_SETTING_AKTE_NOTARIS).OrderBy(e => e.CreatedUtc).ToList(),
                ListBankPembayaran = _context.Setting.Where(e => e.Field == SettingFields.AKNP_SETTING_BANK_PEMBAYARAN)
                    .OrderBy(e => e.CreatedUtc).ToList(),
                ListJenisPerjanjianEbu = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_JENIS_PERJANJIAN_EBU).OrderBy(e => e.CreatedUtc)
                    .ToList(),
                ListStatusPerjanjianEbu = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_STATUS_PERJANJIAN_EBU).OrderBy(e => e.CreatedUtc)
                    .ToList(),
                ListJenisPerjanjianKbos = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_JENIS_PERJANJIAN_KBOS).OrderBy(e => e.CreatedUtc)
                    .ToList(),
                ListStatusPerjanjianKbos = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_STATUS_PERJANJIAN_KBOS).OrderBy(e => e.CreatedUtc)
                    .ToList(),
                ListJenisPerjanjian =
                    _context.Setting.Where(e => e.Field == SettingFields.AKNP_SETTING_JENIS_PERJANJIAN)
                        .OrderBy(e => e.CreatedUtc).ToList(),
                ListStatusPerusahaanInformasiLain = StatusPerusahaan.All().ToList(),
                ListAnggotaBursaInformasiLain = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_ANGGOTA_BURSA_INFORMASI_LAIN)
                    .OrderBy(e => e.CreatedUtc).ToList(),
                ListPemantau = _context.Setting
                .Where(e => e.Field == SettingFields.AKNP_SETTING_PEMANTAU_INFORMASI_LAIN)
                .OrderBy(e => e.CreatedUtc).ToList(),

                ListJenisKeanggotaanPme = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_JENIS_KEANGGOTAAN_PME)
                    .OrderBy(e => e.CreatedUtc).ToList(),
                ListJenisPerjanjianLenderPme = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_JENIS_PERJANJIAN_LENDER_PME)
                    .OrderBy(e => e.CreatedUtc).ToList(),
                ListStatusLenderPme = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_STATUS_LENDER_PME)
                    .OrderBy(e => e.CreatedUtc).ToList(),
                ListJenisPerjanjianBorrowerPme = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_JENIS_PERJANJIAN_BORROWER_PME)
                    .OrderBy(e => e.CreatedUtc).ToList(),
                ListStatusBorrowerPme = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_STATUS_BORROWER_PME).OrderBy(e => e.CreatedUtc)
                    .ToList(),
                ListJabatanKomisaris = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_JABATAN_KOMISARIS)
                    .OrderBy(e => e.CreatedUtc).ToList(),
                ListJabatanDireksi = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_JABATAN_DIREKSI)
                    .OrderBy(e => e.CreatedUtc).ToList(),
                ListJabatanContactPerson = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_JABATAN_CONTACT_PERSON).OrderBy(e => e.CreatedUtc)
                    .ToList(),
                ListJabatanPejabatBerwenang = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_JABATAN_PEJABAT_BERWENANG).OrderBy(e => e.CreatedUtc)
                    .ToList(),
                ListTipeMemberPartisipan = _context.Setting
                    .Where(e => e.Field == SettingFields.AKNP_SETTING_TIPE_MEMBER_PARTISIPAN).OrderBy(e => e.CreatedUtc)
                    .ToList(),
                ListLayananBaruTipeMemberPartisipan = _context.LayananBaru
                    .Where(e => e.TipeMemberAK == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name).OrderBy(e => e.Order)
                    .ToList(),
                ListLayananBaruTipeMemberAku = _context.LayananBaru
                    .Where(e => e.TipeMemberAK == RegistrasiKategoriMember.MEMBER_AKU.Name).OrderBy(e => e.Order)
                    .ToList(),
                ListLayananBaruTipeMemberAki = _context.LayananBaru
                    .Where(e => e.TipeMemberAK == RegistrasiKategoriMember.MEMBER_AKI.Name).OrderBy(e => e.Order)
                    .ToList(),
                ListRegistrasiAKABSponsor = _context.AdministrasiKeanggotaan
                    .Where(e =>  e.Status == DataAknpRequestStatus.APPROVED_STATUS.Name && 
                        (e.KategoriMember == RegistrasiKategoriMember.MEMBER_AKU.Name || e.KategoriMember == RegistrasiKategoriMember.MEMBER_AKI.Name) && e.KodeAK != GuestAknpCode)
                    .GroupBy(e => e.KodeAK)
                    .Select(e => new RegistrasiAKABSponsor
                    { 
                        Name = e.Key,
                        Label = e.Key,
                    })
                    .ToList(),
                ListRegistrasiAKTradingMember = _context.DataTM
                    .OrderBy(e => e.KodePerusahaan)
                    .Select(e => new RegistrasiAKTradingMember
                    {
                        Name = e.KodePerusahaan,
                        Label = e.NamaPerusahaan,
                    })
                    .ToList(),
                ListAkRegistered = _context.DataRegistrasi.Where(x => x.Status == DataAknpRequestStatus.APPROVED_STATUS.Name)
                    .Where(x => x.Draft == false)
                    .GroupBy(x => x.KodeAk).ToList()
                    .Select(x => x.ElementAt(0)).ToList(),
                ListAdministrasiKeanggotaanAKI = _context.AdministrasiKeanggotaan
                    .Where(e => e.KategoriMember == RegistrasiKategoriMember.MEMBER_AKI.Name)
                    .GroupBy(e => e.KodeAK)
                    .Select(e => new RegistrasiAKTradingMember
                    {
                        Name = e.Key,
                        Label = e.Key,
                    }).ToList(),
                ListDataStatusKpei = _context.DataStatusKPEI.ToList(),
                ListStatusKpei = StatusKpei.All().ToList(),
                ListSubStatusKpei = SubStatusKpei.All().ToList(),
                ListStatusBursa = StatusBursa.All().ToList(),
                ListJenisAkte = DataAknpJenisAkte.All().ToList()
            };
            if (HttpContext.Current != null) {
                HttpContext.Current.Items[cacheKey] = stg;
            }
            return stg;
        }

        public Setting GetSettingByField(string field) {
            return _context
                .Setting
                .FirstOrDefault(e => e.Field == field);
        }

        public IEnumerable<Setting> GetAllSettingByField(string field) {
            return _context
                .Setting
                .Where(e => e.Field == field)
                .OrderBy(e => e.CreatedUtc)
                .ToList();
        }

        public EventSettingForm EventSetting(bool useCache = true) {
            return new EventSettingForm {
                ListJenisPelatihan = _context.Setting.Where(e => e.Field == SettingFields.EVENT_JENIS_PELATIHAN).OrderBy(e => e.CreatedUtc).ToList(),
                Reminder = _context.Setting.Where(e => e.Field == SettingFields.EVENT_REMINDER).FirstOrDefault()
            };
        }

        public Setting Get(string id) {
            return _context.Setting.FirstOrDefault(e => e.Id == id);
        }

        public Setting AddSetting(Setting setting) {
            var field = setting.Field;
            var value = setting.Value;
            _context.Setting.Add(setting);
            _context.SaveChanges();
            _ulog.LogUserActionAsync(HttpContext.Current, CoreModules.CORE_SYSTEM, $"Menambahkan nilai {value} di {field} Setting.", "Core");
            return setting;
        }

        public Setting UpdateSetting(Setting setting) {
            var field = setting.Field;
            var value = setting.Value;
            var updatedSetting = Get(setting.Id);
            if (updatedSetting.Field == setting.Field) {
                updatedSetting.Value = setting.Value;
                _context.SaveChanges();
            }
            _ulog.LogUserActionAsync(HttpContext.Current, CoreModules.CORE_SYSTEM, $"Mengubah nilai di {field} menjadi {value} pada Setting.", "Core");
            return updatedSetting;
        }

        public void DeleteSetting(string id, bool useCache = true) {
            var oldSetting = Get(id);
            if (oldSetting != null) {
                var field = oldSetting.Field;
                var value = oldSetting.Value;
                _context.Setting.Remove(oldSetting);
                _context.SaveChanges();

                _ulog.LogUserActionAsync(HttpContext.Current, CoreModules.CORE_SYSTEM, $"Menghapus nilai {value} dari {field} Setting.", "Core");
            }
        }

        public Setting AddEmail(Setting setting) {
            return _context.Setting.Add(setting);
        }
        

        public EmailSetting GetEmailById(string id) {
            var setting = _context.Setting.FirstOrDefault(e => e.Id == id);
            if (setting == null) {
                return null;
            }
            var headerField = setting?.Field?.Replace("MailTemplate", "MailHeader");
            var header = EmailHeader.All().FirstOrDefault(e => e.Key == headerField);
            var headerDb = GetSettingByField(headerField);
            if (headerDb != null) {
                header.Header = headerDb.Value;
            }
            var email = new EmailSetting {
                Template = setting,
                Header = header
            };
            return email;
        }

        public EmailSetting GetEmailByField(string field) {
            var templateField = (field).Replace("MailHeader", "MailTemplate");
            var setting = _context.Setting.FirstOrDefault(e => e.Field == templateField);
            var headerField = (setting?.Field ?? field).Replace("MailTemplate", "MailHeader");
            var header = EmailHeader.All().FirstOrDefault(e => e.Key == headerField);
            var headerDb = GetSettingByField(headerField);
            if (headerDb?.Value != null) {
                header.Header = headerDb.Value;
            }
            var email = new EmailSetting {
                Template = setting,
                Header = header
            };
            return email;
        }

        public IEnumerable<EmailSetting> GetAknpEmail() {
            return GetEmailSetting(EmailHeader.AknpHeader().ToList());
        }

        public IEnumerable<EmailSetting> GetPincodeEmail() {
            return GetEmailSetting(EmailHeader.PincodeHeader().ToList());
        }

        public IEnumerable<EmailSetting> GetMclearsEmail() {
            return GetEmailSetting(EmailHeader.MclearsHeader().ToList());
        }

        public IEnumerable<EmailSetting> GetAdministrasiKeanggotaanEmail() {
            return GetEmailSetting(EmailHeader.AdministrasiKeanggotaanHeader().ToList());
        }

        public IEnumerable<EmailSetting> GetSospelEmail() {
            return GetEmailSetting(EmailHeader.SospelHeader().ToList());
        }
        public IEnumerable<EmailSetting> GetKeuanganEmail() {
            return GetEmailSetting(EmailHeader.KeuanganHeader().ToList());
        }
        public IEnumerable<EmailSetting> GetSurveiEmail() {
            return GetEmailSetting(EmailHeader.SurveiHeader().ToList());
        }

        private IEnumerable<EmailSetting> GetEmailSetting(List<EmailHeader> headerList) {
            var emailList = new List<EmailSetting>();
            foreach (var header in headerList) {
                emailList.Add(GetEmailByField(header.Key) ?? new EmailSetting());
            }
            return emailList;
        }

        public Setting AddEmail(string field, string template) {
            var setting = new Setting {
                Field = field,
                Value = template
            };
            _context.Setting.Add(setting);
            _context.SaveChanges();
            return _context.Setting.Add(setting);
        }

        public Setting UpdateEmail(Setting setting) {
            var updatedSetting = Get(setting.Id);
            updatedSetting.Value = setting.Value;
            _context.SaveChanges();
            return updatedSetting;
        }

        #region Will be Refactored

        public IEnumerable<Setting> Setting => _context.Setting;

        public string SettingField { get; private set; }


        #endregion

    }
}