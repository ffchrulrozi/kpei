﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Models;

namespace Kpei.PortalAK.DataAccess.ModSettings.Repos {
    public interface ISettingRepository {

        GlobalSettingForm GlobalSetting(bool useCache = true);
        void UpdateGlobalSetting(GlobalSettingForm form);

        PincodeSettingForm PincodeSetting(bool useCache = true);
        void UpdatePincodeSetting(PincodeSettingForm form);

        SmtpEmailSettingForm SmtpEmailSetting(bool useCache = true);
        void UpdateSmtpEmailSetting(SmtpEmailSettingForm form);

        DeadlineSettingForm DeadlineSetting(bool useCache = true);
        void UpdateDeadlineSetting(DeadlineSettingForm form);

        IEnumerable<Setting> GetAllSettingByField(string field);

        SurveySettingForm SurveySetting(bool useCache = true);
        void UpdateSurveySetting(SurveySettingForm form);

        AknpSettingForm AknpSetting(bool useCache = true);

        EventSettingForm EventSetting(bool useCache = true);

        KeuanganSettingForm KeuanganSetting(bool useCache = true);

        Setting Get(string id);
        Setting GetSettingByField(string field);
        Setting AddSetting(Setting setting);
        Setting UpdateSetting(Setting setting);
        void DeleteSetting(string id, bool useCache = true);

        EmailSetting GetEmailByField(string field);
        EmailSetting GetEmailById(string id);
        IEnumerable<EmailSetting> GetAknpEmail();
        IEnumerable<EmailSetting> GetPincodeEmail();
        IEnumerable<EmailSetting> GetMclearsEmail();
        IEnumerable<EmailSetting> GetAdministrasiKeanggotaanEmail();
        IEnumerable<EmailSetting> GetSospelEmail();
        IEnumerable<EmailSetting> GetKeuanganEmail();
        IEnumerable<EmailSetting> GetSurveiEmail();

        Setting AddEmail(Setting setting);
        Setting AddEmail(string field, string template);

        #region Will be Refactored
        IEnumerable<Setting> Setting { get; }
        #endregion
    }
}