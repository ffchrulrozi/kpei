﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSettings.Forms {
    public class EmailSettingForm {
        public EmailHeader DefaultHeader { get; set; }
        public Setting Header { get; set; }
        public Setting Template { get; set; }
    }
}
