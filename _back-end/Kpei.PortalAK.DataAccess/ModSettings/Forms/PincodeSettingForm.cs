﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Kpei.PortalAK.DataAccess.ModSettings.Forms {
    public class PincodeSettingForm {
        private List<PincodeEclearsAdmin> _eclearsAdmins;
        private List<PincodeTypeInfo> _pincodeTypeInfos;

        [Display(Name = "Administrator e-CLEARS")]
        public List<PincodeEclearsAdmin> EclearsAdmins {
            get {
                if (_eclearsAdmins == null) {
                    _eclearsAdmins = new List<PincodeEclearsAdmin>();
                    return _eclearsAdmins;
                }

                _eclearsAdmins = _eclearsAdmins.Where(x =>
                    x != null && !string.IsNullOrWhiteSpace(x.Name) && !string.IsNullOrWhiteSpace(x.Email)).ToList();
                return _eclearsAdmins;
            }
            set { _eclearsAdmins = value; }
        }

        [Display(Name = "Jenis PIN Code")]
        public List<PincodeTypeInfo> PincodeTypeInfos {
            get {
                if (_pincodeTypeInfos == null) {
                    _pincodeTypeInfos = new List<PincodeTypeInfo>(new[] {
                        new PincodeTypeInfo {
                            Name = "e-CLEARS",
                            Url = "http://e-clears.pincode.kpei.example"
                        },
                        new PincodeTypeInfo {
                            Name = "e-BOCS",
                            Url = "http://e-bocs.pincode.kpei.example"
                        },
                        new PincodeTypeInfo {
                            Name = "SKD (Sistem Kliring Derivatif)",
                            Url = "http://skd.pincode.kpei.example"
                        },
                        new PincodeTypeInfo {
                            Name = "Member Interface",
                            Url = "http://member-interface.pincode.kpei.example"
                        },
                        new PincodeTypeInfo {
                            Name = "New m-CLEARS",
                            Url = "http://new-m-clears.pincode.kpei.example"
                        }
                    });
                    return _pincodeTypeInfos;
                }

                _pincodeTypeInfos = _pincodeTypeInfos.Where(x =>
                    x != null && !string.IsNullOrWhiteSpace(x.Name) && !string.IsNullOrWhiteSpace(x.Url)).ToList();
                return _pincodeTypeInfos;
            }
            set { _pincodeTypeInfos = value; }
        }
    }

    public class PincodeEclearsAdmin {
        private string _email;
        private string _name;
        public string Name {
            get { return _name?.Trim(); }
            set { _name = value; }
        }
        public string Email {
            get { return _email?.Trim(); }
            set { _email = value; }
        }
    }

    public class PincodeTypeInfo {
        private string _name;
        private string _url;
        public string Name {
            get { return _name?.Trim(); }
            set { _name = value; }
        }
        public string Url {
            get { return _url?.Trim(); }
            set { _url = value; }
        }
    }
}