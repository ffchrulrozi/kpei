﻿using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.ModSettings.Forms {
    public class SmtpEmailSettingForm {
        private string _fromEmailAddress;
        private string _smtpServerAddress;
        private int _smtpServerPort;
        private string _smtpUserName;

        [Display(Name = "Alamat Email Portal AK & P")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(128)]
        public string FromEmailAddress {
            get {
                if (string.IsNullOrWhiteSpace(_fromEmailAddress)) {
                    _fromEmailAddress = "portal-ak-no-reply@kpei.co.id";
                }

                _fromEmailAddress = _fromEmailAddress.Trim();
                return _fromEmailAddress;
            }
            set { _fromEmailAddress = value; }
        }

        [Display(Name = "Alamat Server SMTP")]
        [StringLength(128)]
        public string SmtpServerAddress {
            get {
                if (string.IsNullOrWhiteSpace(_smtpServerAddress)) {
                    _smtpServerAddress = "localhost";
                }

                _smtpServerAddress = _smtpServerAddress.Trim();
                return _smtpServerAddress;
            }
            set { _smtpServerAddress = value; }
        }

        [Display(Name = "Nomor Port SMTP Server")]
        [Range(1, 65535)]
        public int SmtpServerPort {
            get {
                if (_smtpServerPort < 1) return 25;
                return _smtpServerPort;
            }
            set { _smtpServerPort = value; }
        }

        [Display(Name = "Nama User SMTP")]
        [StringLength(128)]
        public string SmtpUserName {
            get { return _smtpUserName?.Trim(); }
            set { _smtpUserName = value; }
        }

        [Display(Name = "Password User SMTP")]
        [StringLength(128)]
        public string SmtpUserPassword { get; set; }

        [Display(Name = "Koneksi ke SMTP menggunakan SSL")]
        [Required(AllowEmptyStrings = false)]
        public bool SmtpEnableSsl { get; set; }
    }
}