﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSettings.Forms {
    public class AknpSettingForm {

        #region Bank Pembayaran

        private List<Setting> _listBankPembayaran;

        public List<Setting> ListBankPembayaran {
            get {
                return _listBankPembayaran ?? new List<Setting>();
            }
            set {
                _listBankPembayaran = value;
            }
        }

        #endregion

        #region Data Akte

        private List<DataAknpJenisAkte> _listJenisAkte;
        private List<Setting> _listNotaris;

        public List<DataAknpJenisAkte> ListJenisAkte {
            get {
                return _listJenisAkte ?? DataAknpJenisAkte.All().ToList();
            }
            set {
                _listJenisAkte = value;
            }
        }
        public List<Setting> ListNotaris {
            get {
                return _listNotaris ?? new List<Setting>(){
                };
            }
            set {
                _listNotaris = value;
            }
        }

        #endregion

        #region Informasi Lain

        private List<StatusPerusahaan> _listStatusPerusahaanInformasiLain;
        private List<Setting> _listAnggotaBursaInformasiLain;
        private List<Setting> _listPemantau;
        
        public List<StatusPerusahaan> ListStatusPerusahaanInformasiLain {
            get {
                return _listStatusPerusahaanInformasiLain ?? StatusPerusahaan.All().ToList();
            }
            set {
                _listStatusPerusahaanInformasiLain = value;
            }
        }
        public List<Setting> ListAnggotaBursaInformasiLain {
            get {
                return _listAnggotaBursaInformasiLain ?? new List<Setting>();
            }
            set {
                _listAnggotaBursaInformasiLain = value;
            }
        }
        public List<Setting> ListPemantau {
            get {
                return _listPemantau ?? new List<Setting>();
            }
            set {
                _listPemantau = value;
            }
        }


        #endregion

        #region Perjanjian EBU

        private List<Setting> _listJenisPerjanjianEbu;
        private List<Setting> _listStatusPerjanjianEbu;

        public List<Setting> ListJenisPerjanjianEbu {
            get {
                return _listJenisPerjanjianEbu ?? new List<Setting>();
            }
            set {
                _listJenisPerjanjianEbu = value;
            }
        }
        public List<Setting> ListStatusPerjanjianEbu {
            get {
                return _listStatusPerjanjianEbu ?? new List<Setting>();
            }
            set {
                _listStatusPerjanjianEbu = value;
            }
        }

        #endregion

        #region Status Bursa

        private List<StatusBursa> _listStatusBursa;

        public List<StatusBursa> ListStatusBursa {
            get {
                return _listStatusBursa ?? StatusBursa.All().ToList();
            }
            set {
                _listStatusBursa = value;
            }
        }

        #endregion

        #region Status KPEI

        private List<StatusKpei> _listStatusKpei;
        private List<SubStatusKpei> _listSubStatusKpei;

        public List<StatusKpei> ListStatusKpei {
            get {
                return _listStatusKpei ?? StatusKpei.All().ToList();
            }
            set {
                _listStatusKpei = value;
            }
        }

        public List<SubStatusKpei> ListSubStatusKpei {
            get {
                return _listSubStatusKpei ?? SubStatusKpei.All().ToList();
            }
            set {
                _listSubStatusKpei = value;
            }
        }

        #endregion

        #region AK Registered

        private List<DataRegistrasi> _listAkRegistered;

        public List<DataRegistrasi> ListAkRegistered {
            get {
                return _listAkRegistered ?? new List<DataRegistrasi>();
            }
            set {
                _listAkRegistered = value;
            }
        }

        #endregion

        #region PME

        private List<Setting> _listJenisKeanggotaanPme;
        private List<Setting> _listJenisPerjanjianLenderPme;
        private List<Setting> _listJenisPerjanjianBorrowerPme;
        private List<Setting> _listStatusLenderPme;
        private List<Setting> _listStatusBorrowerPme;

        public List<Setting> ListJenisKeanggotaanPme {
            get {
                return _listJenisKeanggotaanPme ?? new List<Setting>();
            }
            set {
                _listJenisKeanggotaanPme = value;
            }
        }

        public List<Setting> ListJenisPerjanjianLenderPme {
            get {
                return _listJenisPerjanjianLenderPme ?? new List<Setting>();
            }
            set {
                _listJenisPerjanjianLenderPme = value;
            }
        }
        public List<Setting> ListStatusLenderPme {
            get {
                return _listStatusLenderPme ?? new List<Setting>();
            }
            set {
                _listStatusLenderPme = value;
            }
        }
        public List<Setting> ListJenisPerjanjianBorrowerPme {
            get {
                return _listJenisPerjanjianBorrowerPme ?? new List<Setting>();
            }
            set {
                _listJenisPerjanjianBorrowerPme = value;
            }
        }
        public List<Setting> ListStatusBorrowerPme {
            get {
                return _listStatusBorrowerPme ?? new List<Setting>();
            }
            set {
                _listStatusBorrowerPme = value;
            }
        }

        #endregion

        #region Perjanjian KBOS

        private List<Setting> _listJenisPerjanjianKbos;
        private List<Setting> _listStatusPerjanjianKbos;

        public List<Setting> ListJenisPerjanjianKbos {
            get {
                return _listJenisPerjanjianKbos ?? new List<Setting>();
            }
            set {
                _listJenisPerjanjianKbos = value;
            }
        }
        public List<Setting> ListStatusPerjanjianKbos {
            get {
                return _listStatusPerjanjianKbos ?? new List<Setting>();
            }
            set {
                _listStatusPerjanjianKbos = value;
            }
        }

        #endregion

        #region Perjanjian
        
        private List<Setting> _listJenisPerjanjian;
        public List<Setting> ListJenisPerjanjian {
            get {
                return _listJenisPerjanjian ?? new List<Setting>();
            }
            set {
                _listJenisPerjanjian = value;
            }
        }

        #endregion

        #region Jabatan Komisaris

        private List<Setting> _listJabatanKomisaris;
        public List<Setting> ListJabatanKomisaris {
            get {
                return _listJabatanKomisaris ?? new List<Setting>();
            }
            set {
                _listJabatanKomisaris = value;
            }
        }

        #endregion

        #region Jabatan Direksi

        private List<Setting> _listJabatanDireksi;
        public List<Setting> ListJabatanDireksi {
            get {
                return _listJabatanDireksi ?? new List<Setting>();
            }
            set {
                _listJabatanDireksi = value;
            }
        }

        #endregion

        #region Jabatan Contact Person

        private List<Setting> _listJabatanContactPerson;
        public List<Setting> ListJabatanContactPerson {
            get {
                return _listJabatanContactPerson ?? new List<Setting>();
            }
            set {
                _listJabatanContactPerson = value;
            }
        }

        #endregion

        #region Jabatan Pejabat Berwenang

        private List<Setting> _listJabatanPejabatBerwenang;
        public List<Setting> ListJabatanPejabatBerwenang {
            get {
                return _listJabatanPejabatBerwenang ?? new List<Setting>();
            }
            set {
                _listJabatanPejabatBerwenang = value;
            }
        }

        #endregion

        #region Tipe Member

        private List<Setting> _listTipeMemberPartisipan;
        public List<Setting> ListTipeMemberPartisipan {
            get {
                return _listTipeMemberPartisipan ?? new List<Setting>();
            }
            set {
                _listTipeMemberPartisipan = value;
            }
        }

        private List<LayananBaru> _listLayananBaruTipeMemberPartisipan;
        public List<LayananBaru> ListLayananBaruTipeMemberPartisipan
        {
            get
            {
                return _listLayananBaruTipeMemberPartisipan ?? new List<LayananBaru>();
            }
            set
            {
                _listLayananBaruTipeMemberPartisipan = value;
            }
        }

        private List<LayananBaru> _listLayananBaruTipeMemberAku;
        public List<LayananBaru> ListLayananBaruTipeMemberAku
        {
            get
            {
                return _listLayananBaruTipeMemberAku ?? new List<LayananBaru>();
            }
            set
            {
                _listLayananBaruTipeMemberAku = value;
            }
        }

        private List<LayananBaru> _listLayananBaruTipeMemberAki;
        public List<LayananBaru> ListLayananBaruTipeMemberAki
        {
            get
            {
                return _listLayananBaruTipeMemberAki ?? new List<LayananBaru>();
            }
            set
            {
                _listLayananBaruTipeMemberAki = value;
            }
        }

        private List<RegistrasiAKTradingMember> _listRegistrasiAKTradingMember;
        public List<RegistrasiAKTradingMember> ListRegistrasiAKTradingMember
        {
            get
            {
                return _listRegistrasiAKTradingMember ?? new List<RegistrasiAKTradingMember>();
            }
            set
            {
                _listRegistrasiAKTradingMember = value;
            }
        }

        private List<RegistrasiAKTradingMember> _listAdministrasiKeanggotaanAKI;
        public List<RegistrasiAKTradingMember> ListAdministrasiKeanggotaanAKI
        {
            get
            {
                return _listAdministrasiKeanggotaanAKI ?? new List<RegistrasiAKTradingMember>();
            }
            set
            {
                _listAdministrasiKeanggotaanAKI = value;
            }
        }

        private List<DataStatusKPEI> _listDataStatusKpei;
        public List<DataStatusKPEI> ListDataStatusKpei
        {
            get
            {
                return _listDataStatusKpei ?? new List<DataStatusKPEI>();
            }
            set
            {
                _listDataStatusKpei = value;
            }
        }

        private List<RegistrasiAKABSponsor> _listRegistrasiAKABSponsor;
        public List<RegistrasiAKABSponsor> ListRegistrasiAKABSponsor
        {
            get
            {
                return _listRegistrasiAKABSponsor ?? new List<RegistrasiAKABSponsor>();
            }
            set
            {
                _listRegistrasiAKABSponsor = value;
            }
        }

        #endregion
    }
}