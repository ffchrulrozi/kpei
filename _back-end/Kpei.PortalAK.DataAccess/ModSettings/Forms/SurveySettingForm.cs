﻿using Kpei.PortalAK.DataAccess.DbModels;
using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModSettings.Forms {
    public class SurveySettingForm {

        private List<Setting> _kategoriDivisi;
        private List<Setting> _kategoriPerusahaan;

        public List<Setting> KategoriPerusahaan {
            get {
                return _kategoriPerusahaan ?? new List<Setting>();
            }
            set { _kategoriPerusahaan = value; }
        }
        public List<Setting> KategoriDivisi {
            get {
                return _kategoriDivisi ?? new List<Setting>();
            }
            set { _kategoriDivisi = value; }
        }
    }
}