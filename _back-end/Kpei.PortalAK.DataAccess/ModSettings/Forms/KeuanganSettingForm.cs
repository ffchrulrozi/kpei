﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSettings.Forms {
    public class KeuanganSettingForm {
        private Setting _firstReminder;
        private Setting _secondReminder;
        private Setting _lastReminder;

        private Setting _tengahTahunUtc;
        private Setting _akhirTahunUtc;

        public Setting TengahTahunUtc {
            get {
                return _tengahTahunUtc ?? new Setting() {
                    Field = SettingFields.KEUANGAN_PERIODE_AWAL
                };
            }
            set {
                _tengahTahunUtc = value;
            }
        }

        public Setting AkhirTahunUtc {
            get {
                return _akhirTahunUtc ?? new Setting() {
                    Field = SettingFields.KEUANGAN_PERIODE_AKHIR
                };
            }
            set {
                _akhirTahunUtc = value;
            }
        }

        public Setting FirstReminder {
            get {
                return _firstReminder ?? new Setting() {
                    Field = SettingFields.KEUANGAN_REMINDER1
                };
            }
            set {
                _firstReminder = value;
            }
        }
    }
}
