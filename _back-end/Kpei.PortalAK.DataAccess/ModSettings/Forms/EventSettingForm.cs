﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSettings.Forms {
    public class EventSettingForm {
        private List<Setting> _listJenisPelatihan;
        public List<Setting> ListJenisPelatihan {
            get {
                return _listJenisPelatihan ?? new List<Setting>();
            }
            set {
                _listJenisPelatihan = value;
            }
        }

        private Setting _reminder;
        public Setting Reminder {
            get {
                return _reminder ?? new Setting() {
                    Field = SettingFields.EVENT_REMINDER
                };
            }
            set {
                _reminder = value;
            }
        }
    }
}
