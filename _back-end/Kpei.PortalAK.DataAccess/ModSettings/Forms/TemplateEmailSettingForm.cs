﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSettings.Forms {
    public class TemplateEmailSettingForm {
        private List<EmailSetting> _listEmailSetting;

        public List<EmailSetting> ListEmailSetting {
            get {
                return _listEmailSetting ?? new List<EmailSetting>();
            }
            set {
                _listEmailSetting = value;
            }
        }
    }
}
