﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Kpei.PortalAK.DataAccess.ModSettings.Forms {
    public class GlobalSettingForm {
        private List<string> _kkeGroupEmails;

        [Display(Name = "Email Grup KKE")]
        public List<string> KkeGroupEmails {
            get {
                if (_kkeGroupEmails == null) {
                    _kkeGroupEmails = new List<string>();
                    return _kkeGroupEmails;
                }

                _kkeGroupEmails = _kkeGroupEmails.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                return _kkeGroupEmails;
            }
            set { _kkeGroupEmails = value; }
        }

        private string _guestAknpCode;

        [Display(Name = "Kode AK untuk Guest")]
        [Required]
        [StringLength(10)]
        public string GuestAknpCode {
            get { return _guestAknpCode?.Trim(); }
            set { _guestAknpCode = value; }
        }

        [Display(Name = "Max Orang Perusahaan")]
        [Required]
        [Range(1, 20)]
        public int MaxCompanyPersonCount { get; set; }

        [Display(Name = "Max Subscribe Peserta m-CLEARS")]
        [Required]
        [Range(1, 20)]
        public int MaxSubscribePesertaMclears { get; set; }
        
        public bool EmailDebugMode { get; set; }

        public string EmailDebugAddress { get; set; }
    }
}