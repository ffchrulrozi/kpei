﻿using Kpei.PortalAK.DataAccess.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSettings.Forms {
    public class DeadlineSettingForm {

        public string Id { get; set; }

        public DateTime? KeuanganAwalPeriodeUtc { get; set; }
        
        public DateTime? KeuanganAkhirPeriodeUtc { get; set; }

        public int Reminder1 { get; set; }

        public void Init(Deadline dat) {
            Id = dat.Id;
            KeuanganAwalPeriodeUtc = dat.KeuanganAwalPeriodeUtc;
            KeuanganAkhirPeriodeUtc = dat.KeuanganAkhirPeriodeUtc;
            Reminder1 = dat.Reminder1;
        }
    }
}
