﻿using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.DataAccess.ModPincode.Forms {
    public class NewPincodeForm {

        private string _alasan;
        private string _jenisPincode;

        [Display(Name = "Jenis PIN Code")]
        [Required(AllowEmptyStrings = false)]
        public string JenisPincode {
            get { return _jenisPincode?.Trim(); }
            set { _jenisPincode = value; }
        }

        [Display(Name = "Alasan Reset")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(1024)]
        public string Alasan {
            get { return _alasan?.Trim(); }
            set { _alasan = value; }
        }

        [Display(Name = "File Surat Permohonan")]
        [MaxLength(256)]
        [Required(AllowEmptyStrings = false)]
        public string SuratPermohonanFileUrl { get; set; }
    }
}