﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Kpei.PortalAK.DataAccess.ModPincode.Forms {
    public class SendPincodeForm {

        private List<string> _emailDireksi;
        private List<string> _emailEkstra;
        private string _newUserName;

        [Display(Name = "PIN Username Baru")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(256)]
        public string NewUserName {
            get { return _newUserName?.Trim(); }
            set { _newUserName = value; }
        }

        [Display(Name = "PIN Password Baru")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(256)]
        public string NewPassword { get; set; }

        [Display(Name = "Daftar Email Direksi")]
        public List<string> EmailDireksi {
            get {
                if (_emailDireksi == null) {
                    _emailDireksi = new List<string>();
                } else {
                    _emailDireksi = _emailDireksi.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                }
                return _emailDireksi;
            }
            set { _emailDireksi = value; }
        }

        [Display(Name = "Daftar Email Ekstra")]
        [CustomValidation(typeof(SendPincodeForm), nameof(ValidateEmailTargets))]
        public List<string> EmailEkstra {
            get {
                if (_emailEkstra == null) {
                    _emailEkstra = new List<string>();
                } else {
                    _emailEkstra = _emailEkstra.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                }
                return _emailEkstra;
            }
            set { _emailEkstra = value; }
        }

        public static ValidationResult ValidateEmailTargets(object value, ValidationContext valctx) {
            var ok = ValidationResult.Success;

            var fm = valctx.ObjectInstance as SendPincodeForm;
            if (fm == null) return ok;

            if (fm.EmailDireksi.Count + fm.EmailEkstra.Count == 0) {
                return new ValidationResult("Email tujuan pengiriman hasil reset harus ada.", new[] {valctx.MemberName});
            }

            return ok;
        }
    }
}