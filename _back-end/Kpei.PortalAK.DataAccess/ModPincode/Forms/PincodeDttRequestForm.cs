﻿using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using System;

namespace Kpei.PortalAK.DataAccess.ModPincode.Forms {
    public class PincodeDttRequestForm : DttRequestForm {
        public string KodeAk { get; set; }
        public string StatusRequest { get; set; }
        public string JenisPincode { get; set; }
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }
    }
}