﻿using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModPincode.Forms;

namespace Kpei.PortalAK.DataAccess.ModPincode.Repos {
    public interface IPincodeRepository {
        PincodeRequest GetPincodeRequest(string id);
        PaginatedResult<PincodeRequest> ListPincode(PincodeDttRequestForm req);
        PincodeRequest NewRequest(string kodeAk, NewPincodeForm form);
        PincodeRequest ApproveRequest(string id, string approverId);
        PincodeRequest RejectRequest(string id, string rejectReason);
        PincodeResetResult InputAndSendResetResult(string id, SendPincodeForm form);
    }
}