﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModPincode.Forms;
using Kpei.PortalAK.DataAccess.ModPincode.Models;

namespace Kpei.PortalAK.DataAccess.ModPincode.Repos {
    public class PincodeRepository : IPincodeRepository {
        private readonly AppDbContext _context;

        public PincodeRepository(AppDbContext context) {
            _context = context;
        }

        /// <inheritdoc />
        public PincodeRequest GetPincodeRequest(string id) {
            return _context.PincodeRequest.FirstOrDefault(x => x.Id == id);
        }

        /// <inheritdoc />
        public PaginatedResult<PincodeRequest> ListPincode(PincodeDttRequestForm req) {
            var result = new PaginatedResult<PincodeRequest>();

            var q = _context.PincodeRequest.AsQueryable();
            var filteredQ = _context.PincodeRequest.AsQueryable();
            
            if (!string.IsNullOrWhiteSpace(req.KodeAk)) {
                q = q.Where(x => x.KodeAK == req.KodeAk);
                filteredQ = filteredQ.Where(x => x.KodeAK == req.KodeAk);
            }

            if (!string.IsNullOrWhiteSpace(req.StatusRequest)) {
                q = q.Where(x => x.StatusRequest == req.StatusRequest);
                filteredQ = filteredQ.Where(x => x.StatusRequest == req.StatusRequest);
            }

            if (!string.IsNullOrWhiteSpace(req.JenisPincode)) {
                q = q.Where(x => x.JenisPincode == req.JenisPincode);
                filteredQ = filteredQ.Where(x => x.JenisPincode == req.JenisPincode);
            }

            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                var kws = Regex.Split(req.search.value, @"\s+");
                filteredQ = q.Where(x => kws.All(kw => x.StatusRequest.Contains(kw)) ||
                                         kws.All(kw => x.KodeAK.Contains(kw)) ||
                                         kws.All(kw => x.JenisPincode.Contains(kw))
                );
            }

            if (req.FromDateTime != null) {
                var fromUtc = req.FromDateTime.Value.Date.ToUniversalTime();
                q = q.Where(x => x.CreatedUtc >= fromUtc);
                filteredQ = filteredQ.Where(x => x.CreatedUtc >= fromUtc);
            } 
            if (req.ToDateTime != null) {
                var toUtc = req.ToDateTime.Value.Date.AddDays(1).ToUniversalTime();
                q = q.Where(x => x.CreatedUtc <= toUtc);
                filteredQ = filteredQ.Where(x => x.CreatedUtc <= toUtc);
            }

            var orderCol = req.firstOrderColumn?.name ?? nameof(PincodeRequest.CreatedUtc);
            var orderDesc = req.order.FirstOrDefault()?.IsDesc == true;

            if (orderCol == nameof(PincodeRequest.KodeAK)) {
                filteredQ = orderDesc ? filteredQ.OrderByDescending(x => x.KodeAK) : filteredQ.OrderBy(x => x.KodeAK);
            } else if (orderCol == nameof(PincodeRequest.StatusRequest)) {
                filteredQ = orderDesc
                    ? filteredQ.OrderByDescending(x => x.StatusRequest)
                    : filteredQ.OrderBy(x => x.StatusRequest);
            } else if (orderCol == nameof(PincodeRequest.JenisPincode)) {
                filteredQ = orderDesc
                    ? filteredQ.OrderByDescending(x => x.JenisPincode)
                    : filteredQ.OrderBy(x => x.JenisPincode);
            } else {
                filteredQ = orderDesc
                    ? filteredQ.OrderByDescending(x => x.CreatedUtc)
                    : filteredQ.OrderBy(x => x.CreatedUtc);
            }

            result.Initialize(q,filteredQ, req.start, req.length);

            return result;
        }

        /// <inheritdoc />
        public PincodeRequest NewRequest(string kodeAk, NewPincodeForm form) {
            var req = new PincodeRequest {
                KodeAK = kodeAk,
                JenisPincode = form.JenisPincode,
                Alasan = form.Alasan,
                SuratPermohonanFileUrl = form.SuratPermohonanFileUrl,
                StatusRequest = PincodeRequestStatus.NEW_STATUS.Name
            };
            _context.PincodeRequest.Add(req);
            _context.SaveChanges();
            return req;
        }

        /// <inheritdoc />
        public PincodeRequest ApproveRequest(string id, string approverId) {
            var req = GetPincodeRequest(id);
            if (req.JenisPincode.Contains("e-CLEARS")) {
                req.StatusRequest = PincodeRequestStatus.DONE_STATUS.Name;
            } else {
                req.StatusRequest = PincodeRequestStatus.APPROVED_STATUS.Name;
            }
            _context.SaveChanges();
            return req;
        }

        /// <inheritdoc />
        public PincodeRequest RejectRequest(string id, string rejectReason) {
            var req = GetPincodeRequest(id);
            req.AlasanDitolak = rejectReason;
            req.StatusRequest = PincodeRequestStatus.REJECTED_STATUS.Name;
            _context.SaveChanges();
            return req;
        }

        /// <inheritdoc />
        public PincodeResetResult InputAndSendResetResult(string id, SendPincodeForm form) {
            var req = GetPincodeRequest(id);
            req.StatusRequest = PincodeRequestStatus.DONE_STATUS.Name;

            var result = new PincodeResetResult {
                PincodeRequestId = req.Id,
                NewUserName = form.NewUserName,
                NewPassword = form.NewPassword
            };

            foreach (var em in form.EmailDireksi) {
                result.EmailTargets.Add(new PincodeResetEmail {
                    EmailAddress = em,
                    ResetResultId = result.Id
                });
            }

            foreach (var em in form.EmailEkstra) {
                result.EmailTargets.Add(new PincodeResetEmail {
                    EmailAddress = em,
                    ResetResultId = result.Id
                });
            }

            _context.PincodeResetResults.Add(result);
            _context.SaveChanges();
            return result;
        }
    }
}