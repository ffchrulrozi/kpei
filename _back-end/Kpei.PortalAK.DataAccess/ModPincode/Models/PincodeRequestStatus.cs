﻿using System.Collections.Generic;

namespace Kpei.PortalAK.DataAccess.ModPincode.Models {
    public class PincodeRequestStatus {

        private PincodeRequestStatus(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<PincodeRequestStatus> All() {
            yield return NEW_STATUS;
            yield return APPROVED_STATUS;
            yield return DONE_STATUS;
            yield return REJECTED_STATUS;
        }

        public static readonly PincodeRequestStatus NEW_STATUS = new PincodeRequestStatus("NEW", "NEW");
        public static readonly PincodeRequestStatus APPROVED_STATUS = new PincodeRequestStatus("APPROVED", "APPROVED");
        public static readonly PincodeRequestStatus DONE_STATUS = new PincodeRequestStatus("DONE", "DONE");
        public static readonly PincodeRequestStatus REJECTED_STATUS = new PincodeRequestStatus("REJECTED", "REJECTED");
    }
}