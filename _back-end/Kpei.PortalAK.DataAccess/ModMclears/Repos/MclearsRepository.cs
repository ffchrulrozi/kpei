﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModMclears.Forms;
using System.Linq;
using System.Text.RegularExpressions;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Models;
using Kpei.PortalAK.DataAccess.ModMclears.Models;

namespace Kpei.PortalAK.DataAccess.ModMclears.Repos {
    public class MclearsRepository : IMclearsRepository {
        private readonly AppDbContext _context;

        public MclearsRepository(AppDbContext context) {
            _context = context;
        }

        IEnumerable<MclearsPeserta> IMclearsRepository.MclearsPeserta => _context.MclearsPeserta;
        IEnumerable<MclearsRequest> IMclearsRepository.MclearsRequest => _context.MclearsRequest;


        public MclearsRequest Get(string id) => _context.MclearsRequest.FirstOrDefault(x => x.Id == id);

        public MclearsRequest Create(UpdateMclearsRequestForm form) {
            var newRequest = new MclearsRequest {
                KodeAK = form.KodeAK,
                StatusRequest = form.StatusRequest ?? MclearsStatusRequest.NEW_STATUS.Name
            };
            _context.MclearsRequest.Add(newRequest);
            _context.SaveChanges();
            return newRequest;
        }

        public MclearsRequest Update(UpdateMclearsRequestForm form) {
            var request = Get(form.Id);
            request.KodeAK = form.KodeAK;
            request.StatusRequest = form.StatusRequest;
            _context.SaveChanges();
            return request;
        }

        public MclearsRequest Approve(string id, string approverId) {
            var request = Get(id);
            if (request == null) {
                return null;
            }
            request.StatusRequest = MclearsStatusRequest.DONE_STATUS.Name;

            _context.SaveChanges();
            return request;
        }

        public MclearsPeserta GetPeserta(string id) => _context.MclearsPeserta.FirstOrDefault(x => x.Id == id);

        public List<MclearsPeserta> GetPesertaFromRequest(string id) => _context.MclearsPeserta.Where(e => e.RequestId == id).ToList();

        public MclearsPeserta CreatePeserta(UpdateMclearsPesertaForm form) {

            var newPeserta = new MclearsPeserta {
                KodeAK = form.KodeAK,
                RequestId = form.RequestId,
                Nama = form.Nama,
                Jabatan = form.Jabatan,
                Email = form.Email,
                HP = form.HP,
                AlertPC = form.AlertPC,
                AlertPS = form.AlertPS,
                AlertHTS = form.AlertHTS,
                AlertHTR = form.AlertHTR,
                AlertBACS = form.AlertBACS,
                AlertACS = form.AlertACS,
                AlertTLP = form.AlertTLP,
                AlertTLS = form.AlertTLS,
                AlertTL5 = form.AlertTL5,
                AlertMKBDA = form.AlertMKBDA,
                AlertFCOLWA = form.AlertFCOLWA,
                AlertCLA = form.AlertCLA,
                AlertOCA = form.AlertOCA,
                AlertMCCA = form.AlertMCCA,
                AlertDCDA = form.AlertDCDA,
                AlertDCRA = form.AlertDCRA,
                AlertMCA = form.AlertMCA,
                AlertCPA = form.AlertCPA,
                AlertL1 = form.AlertL1,
                AlertL3 = form.AlertL3,
                AlertL7 = form.AlertL7,
                AlertHPHFA = form.AlertHPHFA,
                AlertBroadcast = form.AlertBroadcast,
                OnRequestPC = form.OnRequestPC,
                OnRequestPS = form.OnRequestPS,
                OnRequestHTS = form.OnRequestHTS,
                OnRequestHTR = form.OnRequestHTR,
                OnRequestPPR = form.OnRequestPPR,
                OnRequestMKBDR = form.OnRequestMKBDR,
                OnRequestOCR = form.OnRequestOCR,
                OnRequestHaircut = form.OnRequestHaircut,
                OnRequestDCDR = form.OnRequestDCDR,
                OnRequestDCRR = form.OnRequestDCRR,
                OnRequestMCR = form.OnRequestMCR,
                OnRequestCPR = form.OnRequestCPR,
                OnRequestHPHFR = form.OnRequestHPHFR
            };
            _context.MclearsPeserta.Add(newPeserta);
            _context.SaveChanges();
            return newPeserta;
        }

        public MclearsPeserta UpdatePeserta(UpdateMclearsPesertaForm form) {
            var request = GetPeserta(form.Id);
            if (request == null && form != null) {
                CreatePeserta(form);
            } else if(request != null){
                request.KodeAK = form.KodeAK;
                request.RequestId = form.RequestId;
                request.Nama = form.Nama;
                request.Jabatan = form.Jabatan;
                request.Email = form.Email;
                request.HP = form.HP;
                request.AlertPC = form.AlertPC;
                request.AlertPS = form.AlertPS;
                request.AlertHTS = form.AlertHTS;
                request.AlertHTR = form.AlertHTR;
                request.AlertBACS = form.AlertBACS;
                request.AlertACS = form.AlertACS;
                request.AlertTLP = form.AlertTLP;
                request.AlertTLS = form.AlertTLS;
                request.AlertTL5 = form.AlertTL5;
                request.AlertMKBDA = form.AlertMKBDA;
                request.AlertFCOLWA = form.AlertFCOLWA;
                request.AlertCLA = form.AlertCLA;
                request.AlertOCA = form.AlertOCA;
                request.AlertMCCA = form.AlertMCCA;
                request.AlertDCDA = form.AlertDCDA;
                request.AlertDCRA = form.AlertDCRA;
                request.AlertMCA = form.AlertMCA;
                request.AlertCPA = form.AlertCPA;
                request.AlertL1 = form.AlertL1;
                request.AlertL3 = form.AlertL3;
                request.AlertL7 = form.AlertL7;
                request.AlertHPHFA = form.AlertHPHFA;
                request.AlertBroadcast = form.AlertBroadcast;
                request.OnRequestPC = form.OnRequestPC;
                request.OnRequestPS = form.OnRequestPS;
                request.OnRequestHTS = form.OnRequestHTS;
                request.OnRequestHTR = form.OnRequestHTR;
                request.OnRequestPPR = form.OnRequestPPR;
                request.OnRequestMKBDR = form.OnRequestMKBDR;
                request.OnRequestOCR = form.OnRequestOCR;
                request.OnRequestHaircut = form.OnRequestHaircut;
                request.OnRequestDCDR = form.OnRequestDCDR;
                request.OnRequestDCRR = form.OnRequestDCRR;
                request.OnRequestMCR = form.OnRequestMCR;
                request.OnRequestCPR = form.OnRequestCPR;
                request.OnRequestHPHFR = form.OnRequestHPHFR;
                _context.SaveChanges();
                return request;
            }

            return null;
        }

        public void DeletePeserta(string id) {
            var peserta = GetPeserta(id);
            if (peserta != null) {
                _context.MclearsPeserta.Remove(peserta);
                _context.SaveChanges();
            }
        }

        public PaginatedResult<MclearsRequest> List(MclearsDttRequestForm req) {
            var result = new PaginatedResult<MclearsRequest>();

            var allQ = _context.MclearsRequest.AsQueryable();
            var filQ = _context.MclearsRequest.AsQueryable();

            if (!string.IsNullOrWhiteSpace(req.KodeAk)) {
                filQ = filQ.Where(x => x.KodeAK == req.KodeAk);
            }

            if (!string.IsNullOrWhiteSpace(req.StatusRequest)) {
                filQ = filQ.Where(x => x.StatusRequest == req.StatusRequest);
            }

            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                var kws = Regex.Split(req.search.value, @"\s+");
                if (kws.Length > 0) {
                    filQ = filQ.Where(x =>
                            kws.All(kw => x.KodeAK.Contains(kw))
                            || kws.All(kw => x.StatusRequest.Contains(kw))
                        );
                }
            }

            if (req.firstOrderColumn?.name == nameof(MclearsRequest.KodeAK)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.KodeAK)
                    : filQ.OrderBy(x => x.KodeAK);
            } else if (req.firstOrderColumn?.name == nameof(MclearsRequest.StatusRequest)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.StatusRequest)
                    : filQ.OrderBy(x => x.StatusRequest);
            } else {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.CreatedUtc)
                    : filQ.OrderBy(x => x.CreatedUtc);
            }

            result.Initialize(allQ, filQ, req.start, req.length);

            return result;
        }

        public int NotNullLayananRequest(string id) {
            var listPeserta = GetPesertaFromRequest(id);
            var count = 0;
            foreach (var peserta in listPeserta) {
                count += NotNullLayananAlert(peserta);
                count += NotNullLayananOnRequest(peserta);
            }
            return count;
        }

        public int NotNullLayananAlert(MclearsPeserta peserta) {
            var count = 0;

            count += peserta.AlertPC != null ? 1 : 0;
            count += peserta.AlertPS != null ? 1 : 0;
            count += peserta.AlertHTS != null ? 1 : 0;
            count += peserta.AlertHTR != null ? 1 : 0;
            count += peserta.AlertBACS != null ? 1 : 0;
            count += peserta.AlertACS != null ? 1 : 0;
            count += peserta.AlertTLP != null ? 1 : 0;
            count += peserta.AlertTLS != null ? 1 : 0;
            count += peserta.AlertTL5 != null ? 1 : 0;
            count += peserta.AlertMKBDA != null ? 1 : 0;
            count += peserta.AlertFCOLWA != null ? 1 : 0;
            count += peserta.AlertCLA != null ? 1 : 0;
            count += peserta.AlertOCA != null ? 1 : 0;
            count += peserta.AlertMCCA != null ? 1 : 0;
            count += peserta.AlertDCDA != null ? 1 : 0;
            count += peserta.AlertDCRA != null ? 1 : 0;
            count += peserta.AlertMCA != null ? 1 : 0;
            count += peserta.AlertCPA != null ? 1 : 0;
            count += peserta.AlertL1 != null ? 1 : 0;
            count += peserta.AlertL3 != null ? 1 : 0;
            count += peserta.AlertL7 != null ? 1 : 0;
            count += peserta.AlertHPHFA != null ? 1 : 0;
            count += peserta.AlertBroadcast != null ? 1 : 0;

            return count;
        }

        public int NotNullLayananOnRequest(MclearsPeserta peserta) {
            var count = 0;
            count += peserta.OnRequestPC != null ? 1 : 0;
            count += peserta.OnRequestPS != null ? 1 : 0;
            count += peserta.OnRequestHTS != null ? 1 : 0;
            count += peserta.OnRequestHTR != null ? 1 : 0;
            count += peserta.OnRequestPPR != null ? 1 : 0;
            count += peserta.OnRequestMKBDR != null ? 1 : 0;
            count += peserta.OnRequestOCR != null ? 1 : 0;
            count += peserta.OnRequestHaircut != null ? 1 : 0;
            count += peserta.OnRequestDCDR != null ? 1 : 0;
            count += peserta.OnRequestDCRR != null ? 1 : 0;
            count += peserta.OnRequestMCR != null ? 1 : 0;
            count += peserta.OnRequestCPR != null ? 1 : 0;
            count += peserta.OnRequestHPHFR != null ? 1 : 0;
            return count;
        }
    }
}