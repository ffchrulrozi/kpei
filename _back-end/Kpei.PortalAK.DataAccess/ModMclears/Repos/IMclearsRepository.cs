﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.ModMclears.Forms;

namespace Kpei.PortalAK.DataAccess.ModMclears.Repos {
    public interface IMclearsRepository {
        IEnumerable<MclearsPeserta> MclearsPeserta { get; }
        IEnumerable<MclearsRequest> MclearsRequest { get; }

        MclearsRequest Get(string id);
        MclearsRequest Create(UpdateMclearsRequestForm form);
        MclearsRequest Update(UpdateMclearsRequestForm form);
        MclearsRequest Approve(string id, string approverId);

        MclearsPeserta GetPeserta(string id);
        List<MclearsPeserta> GetPesertaFromRequest(string id);
        MclearsPeserta CreatePeserta(UpdateMclearsPesertaForm form);
        MclearsPeserta UpdatePeserta(UpdateMclearsPesertaForm form);
        void DeletePeserta(string id);

        PaginatedResult<MclearsRequest> List(MclearsDttRequestForm req);

        int NotNullLayananRequest(string id);
        int NotNullLayananAlert(MclearsPeserta peserta);
        int NotNullLayananOnRequest(MclearsPeserta peserta);
    }
}