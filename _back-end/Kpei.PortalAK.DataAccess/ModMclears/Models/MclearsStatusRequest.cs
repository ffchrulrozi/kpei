﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModMclears.Models {
    public class MclearsStatusRequest {
        public static readonly MclearsStatusRequest NEW_STATUS = new MclearsStatusRequest("NEW", "NEW");
        
        public static readonly MclearsStatusRequest
            DONE_STATUS = new MclearsStatusRequest("DONE", "DONE");
        
        private MclearsStatusRequest(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<MclearsStatusRequest> All() {
            yield return NEW_STATUS;
            yield return DONE_STATUS;
        }
    }
}
