﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModMclears.Models {
    public class MclearsJenisPerubahan {
        public static readonly MclearsJenisPerubahan SUBSCRIBE = new MclearsJenisPerubahan("SUBSCRIBE", "SUBSCRIBE");

        public static readonly MclearsJenisPerubahan
            UNSUBSCRIBE = new MclearsJenisPerubahan("UNSUBSCRIBE", "UNSUBSCRIBE");

        
        private MclearsJenisPerubahan(string name, string label) {
            Name = name;
            Label = label;
        }

        public string Name { get; }
        public string Label { get; }

        public static IEnumerable<MclearsJenisPerubahan> All() {
            yield return SUBSCRIBE;
            yield return UNSUBSCRIBE;
        }
    }
}
