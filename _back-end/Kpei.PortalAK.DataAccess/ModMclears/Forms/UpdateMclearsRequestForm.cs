﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModMclears.Forms {
    public class UpdateMclearsRequestForm {
        
        [MaxLength(128)]
        public string Id { get; set; }

        [MaxLength(32)]
        public string KodeAK { get; set; }
        public string StatusRequest { get; set; }

        [Display(Name = "Data Peserta")]
        [CollectionItemMustValid]
        public List<UpdateMclearsPesertaForm> ListPeserta { get; set; }

        public void Init(MclearsRequest dat, List<MclearsPeserta> listDat) {
            Id = dat.Id;
            KodeAK = dat.KodeAK;
            StatusRequest = dat.StatusRequest;
            ListPeserta = new List<UpdateMclearsPesertaForm>();
            foreach (var data in listDat) {
                ListPeserta.Add(new UpdateMclearsPesertaForm(data));
            }
        }
    }
}
