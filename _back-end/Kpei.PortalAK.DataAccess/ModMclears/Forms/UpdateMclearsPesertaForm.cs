﻿using Kpei.PortalAK.DataAccess.DbModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModMclears.Forms {
    public class UpdateMclearsPesertaForm {
        public UpdateMclearsPesertaForm() {

        }

        [MaxLength(128)]
        public string Id { get; set; }
        [MaxLength(32)]
        public string KodeAK { get; set; }
        [Display(Name = "Nama")]
        [MaxLength(128)]
        [Required]
        public string Nama { get; set; }
        [Display(Name = "Jabatan")]
        [MaxLength(128)]
        [Required]
        public string Jabatan { get; set; }
        [Display(Name = "E-mail")]
        [MaxLength(128)]
        [Required]
        public string Email { get; set; }
        [MaxLength(128)]
        [Required]
        public string HP { get; set; }
        public Boolean? AlertPC { get; set; }
        public Boolean? AlertPS { get; set; }
        public Boolean? AlertHTS { get; set; }
        public Boolean? AlertHTR { get; set; }
        public Boolean? AlertBACS { get; set; }
        public Boolean? AlertACS { get; set; }
        public Boolean? AlertTLP { get; set; }
        public Boolean? AlertTLS { get; set; }
        public Boolean? AlertTL5 { get; set; }
        public Boolean? AlertMKBDA { get; set; }
        public Boolean? AlertFCOLWA { get; set; }
        public Boolean? AlertCLA { get; set; }
        public Boolean? AlertOCA { get; set; }
        public Boolean? AlertMCCA { get; set; }
        public Boolean? AlertDCDA { get; set; }
        public Boolean? AlertDCRA { get; set; }
        public Boolean? AlertMCA { get; set; }
        public Boolean? AlertCPA { get; set; }
        public Boolean? AlertL1 { get; set; }
        public Boolean? AlertL3 { get; set; }
        public Boolean? AlertL7 { get; set; }
        public Boolean? AlertHPHFA { get; set; }
        public Boolean? AlertBroadcast { get; set; }
        public Boolean? OnRequestPC { get; set; }
        public Boolean? OnRequestPS { get; set; }
        public Boolean? OnRequestHTS { get; set; }
        public Boolean? OnRequestHTR { get; set; }
        public Boolean? OnRequestPPR { get; set; }
        public Boolean? OnRequestMKBDR { get; set; }
        public Boolean? OnRequestOCR { get; set; }
        public Boolean? OnRequestHaircut { get; set; }
        public Boolean? OnRequestDCDR { get; set; }
        public Boolean? OnRequestDCRR { get; set; }
        public Boolean? OnRequestMCR { get; set; }
        public Boolean? OnRequestCPR { get; set; }
        public Boolean? OnRequestHPHFR { get; set; }

        public string RequestId { get; set; }

        public UpdateMclearsPesertaForm(MclearsPeserta dat) {
            Id = dat.Id;
            KodeAK = dat.KodeAK;
            Nama = dat.Nama;
            Jabatan = dat.Jabatan;
            Email = dat.Email;
            HP = dat.HP;
            AlertACS = dat.AlertACS;
            AlertBACS = dat.AlertBACS;
            AlertBroadcast = dat.AlertBroadcast;
            AlertCLA = dat.AlertCLA;
            AlertCPA = dat.AlertCPA;
            AlertDCDA = dat.AlertDCDA;
            AlertDCRA = dat.AlertDCRA;
            AlertFCOLWA = dat.AlertFCOLWA;
            AlertHPHFA = dat.AlertHPHFA;
            AlertHTR = dat.AlertHTR;
            AlertHTS = dat.AlertHTS;
            AlertL1 = dat.AlertL1;
            AlertL3 = dat.AlertL3;
            AlertL7 = dat.AlertL7;
            AlertMCA = dat.AlertMCA;
            AlertMCCA = dat.AlertMCCA;
            AlertMKBDA = dat.AlertMKBDA;
            AlertOCA = dat.AlertOCA;
            AlertPC = dat.AlertPC;
            AlertPS = dat.AlertPS;
            AlertTL5 = dat.AlertTL5;
            AlertTLP = dat.AlertTLP;
            AlertTLS = dat.AlertTLS;
            OnRequestCPR = dat.OnRequestCPR;
            OnRequestDCDR = dat.OnRequestDCDR;
            OnRequestHaircut = dat.OnRequestHaircut;
            OnRequestHPHFR = dat.OnRequestHPHFR;
            OnRequestHTR = dat.OnRequestHTR;
            OnRequestHTS = dat.OnRequestHTS;
            OnRequestMCR = dat.OnRequestMCR;
            OnRequestMKBDR = dat.OnRequestMKBDR;
            OnRequestOCR = dat.OnRequestOCR;
            OnRequestPC = dat.OnRequestPC;
            OnRequestPPR = dat.OnRequestPPR;
            OnRequestPS = dat.OnRequestPS;
        }
    }
}
