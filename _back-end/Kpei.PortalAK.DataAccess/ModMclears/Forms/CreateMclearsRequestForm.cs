﻿using Kpei.PortalAK.DataAccess.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModMclears.Forms {
    public class CreateMclearsRequestForm {
        [Display(Name = "Kode AK")]
        [MaxLength(32)]
        public string KodeAK { get; set; }
        [Display(Name = "Status")]
        public string StatusRequest { get; set; }
        
        [Display(Name = "Data Peserta")]
        [CollectionItemMustValid]
        public List<CreateMclearsPesertaForm> ListPeserta { get; set; }
    }
}
