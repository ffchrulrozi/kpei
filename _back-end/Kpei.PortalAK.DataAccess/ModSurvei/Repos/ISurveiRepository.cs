﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.ModSurvei.Forms;

namespace Kpei.PortalAK.DataAccess.ModSurvei.Repos {
    public interface ISurveiRepository {
        IEnumerable<Survei> Survei { get; }
        IEnumerable<SurveiPerusahaan> SurveiPerusahaan { get; }
        IEnumerable<SurveiDivisi> SurveiDivisi { get; }

        Survei Get(string id);
        Survei Create(CreateSurveiForm form);
        Survei Update(UpdateSurveiForm form);

        List<SurveiPerusahaan> GetPerusahaanFromSurvei(string id);
        SurveiPerusahaan GetPerusahaan(string id);
        SurveiPerusahaan CreatePerusahaan(CreateSurveiPerusahaanForm form);
        SurveiPerusahaan UpdatePerusahaan(UpdateSurveiPerusahaanForm form);

        List<SurveiDivisi> GetDivisiFromSurvei(string id);
        SurveiDivisi GetDivisi(string id);
        SurveiDivisi CreateDivisi(CreateSurveiDivisiForm form);
        SurveiDivisi UpdateDivisi(UpdateSurveiDivisiForm form);

        PaginatedResult<Survei> List(SurveiDttRequestForm req);
    }
}