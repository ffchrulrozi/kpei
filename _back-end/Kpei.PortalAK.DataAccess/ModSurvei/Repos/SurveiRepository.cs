﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSurvei.Forms;
using Kpei.PortalAK.DataAccess.Core;
using System.Linq;
using System.Text.RegularExpressions;

namespace Kpei.PortalAK.DataAccess.ModSurvei.Repos {
    public class SurveiRepository : ISurveiRepository {
        private readonly AppDbContext _context;

        public SurveiRepository(AppDbContext context) {
            _context = context;
        }

        public IEnumerable<Survei> Survei => _context.Survei;

        public IEnumerable<SurveiPerusahaan> SurveiPerusahaan => _context.SurveiPerusahaan;

        public IEnumerable<SurveiDivisi> SurveiDivisi => _context.SurveiDivisi;

        public Survei Create(CreateSurveiForm form) {
            var newSurvei = new Survei {
                Nama = form.Nama,
                TglPelaksanaanUtc = form.TglPelaksanaanUtc,
                JlhResponden = form.JlhResponden,
                Nilai = form.Nilai
            };
            _context.Survei.Add(newSurvei);
            _context.SaveChanges();
            return newSurvei;
        }

        public SurveiDivisi CreateDivisi(CreateSurveiDivisiForm form) {
            var newSurveiDivisi = new SurveiDivisi {
                SurveiId = form.SurveiId,
                Kategori = form.Kategori,
                HasilFileUrl = form.HasilFileUrl,
                TindakLanjutFileUrl = form.TindakLanjutFileUrl
            };
            _context.SurveiDivisi.Add(newSurveiDivisi);
            _context.SaveChanges();
            return newSurveiDivisi;
        }

        public SurveiPerusahaan CreatePerusahaan(CreateSurveiPerusahaanForm form) {
            var newSurveiPerusahaan = new SurveiPerusahaan {
                SurveiId = form.SurveiId,
                Kategori = form.Kategori,
                HasilFileUrl = form.HasilFileUrl,
                TindakLanjutFileUrl = form.TindakLanjutFileUrl
            };
            _context.SurveiPerusahaan.Add(newSurveiPerusahaan);
            _context.SaveChanges();
            return newSurveiPerusahaan;
        }

        public Survei Get(string id) {
            return _context.Survei.FirstOrDefault(x => x.Id == id);
        }

        public List<SurveiDivisi> GetDivisiFromSurvei(string id) {
            return _context.SurveiDivisi.Where(e => e.SurveiId == id).ToList();
        }

        public List<SurveiPerusahaan> GetPerusahaanFromSurvei(string id) {
            return _context.SurveiPerusahaan.Where(e => e.SurveiId == id).ToList();
        }

        public SurveiDivisi GetDivisi(string id) {
            return _context.SurveiDivisi.FirstOrDefault(x => x.Id == id);
        }

        public SurveiPerusahaan GetPerusahaan(string id) {
            return _context.SurveiPerusahaan.FirstOrDefault(x => x.Id == id);
        }


        public PaginatedResult<Survei> List(SurveiDttRequestForm req) {
            var result = new PaginatedResult<Survei>();

            var allQ = _context.Survei.AsQueryable();
            var filQ = _context.Survei.AsQueryable();

            if (req.StartDate != null && req.EndDate != null && req.StartDate < req.EndDate) {
                //todo Future Filtering by Date
            }

            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                var kws = Regex.Split(req.search.value, @"\s+");
                if (kws.Length > 0) {
                    filQ = filQ.Where(x =>
                            kws.All(kw => x.Nama.Contains(kw))
                        );
                }
            }

            if (req.firstOrderColumn?.name == nameof(DbModels.Survei.Nama)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.Nama)
                    : filQ.OrderBy(x => x.Nama);
            } else if (req.firstOrderColumn?.name == nameof(DbModels.Survei.Nilai)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.Nilai)
                    : filQ.OrderBy(x => x.Nilai);
            } else if (req.firstOrderColumn?.name == nameof(DbModels.Survei.JlhResponden)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.JlhResponden)
                    : filQ.OrderBy(x => x.JlhResponden);
            } else if (req.firstOrderColumn?.name == nameof(DbModels.Survei.TglPelaksanaanUtc)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.TglPelaksanaanUtc)
                    : filQ.OrderBy(x => x.TglPelaksanaanUtc);
            } else {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.CreatedUtc)
                    : filQ.OrderBy(x => x.CreatedUtc);
            }

            result.Initialize(allQ, filQ, req.start, req.length);
            return result;
        }

        public Survei Update(UpdateSurveiForm form) {
            var survei = Get(form.Id);
            survei.Nama = form.Nama;
            survei.TglPelaksanaanUtc = form.TglPelaksanaanUtc;
            survei.JlhResponden = form.JlhResponden;
            survei.Nilai = form.Nilai;
            _context.SaveChanges();
            return survei;
        }

        public SurveiPerusahaan UpdatePerusahaan(UpdateSurveiPerusahaanForm form) {
            bool emptyData = false;
            if (form.Kategori == null && form.HasilFileUrl == null && form.TindakLanjutFileUrl == null) {
                emptyData = true;
            }
            var surveiPerusahaan = GetPerusahaan(form.Id);
            if (surveiPerusahaan != null) {
                if (emptyData) {
                    DeletePerusahaan(form.Id);
                } else {
                    surveiPerusahaan.SurveiId = form.SurveiId;
                    surveiPerusahaan.Kategori = form.Kategori;
                    surveiPerusahaan.HasilFileUrl = form.HasilFileUrl;
                    surveiPerusahaan.TindakLanjutFileUrl = form.TindakLanjutFileUrl;
                }
            } else {
                if (!emptyData) {
                    CreatePerusahaan(new CreateSurveiPerusahaanForm {
                        SurveiId = form.SurveiId,
                        Kategori = form.Kategori,
                        HasilFileUrl = form.HasilFileUrl,
                        TindakLanjutFileUrl = form.TindakLanjutFileUrl
                    });
                }
            }

            _context.SaveChanges();
            return surveiPerusahaan;
        }

        public SurveiDivisi UpdateDivisi(UpdateSurveiDivisiForm form) {
            bool emptyData = false;
            if (form.Kategori == null && form.HasilFileUrl == null && form.TindakLanjutFileUrl == null) {
                emptyData = true;
            }
            var surveiDivisi = GetDivisi(form.Id);
            if (surveiDivisi != null) {
                if (emptyData) {
                    DeleteDivisi(form.Id);
                } else {
                    surveiDivisi.SurveiId = form.SurveiId;
                    surveiDivisi.Kategori = form.Kategori;
                    surveiDivisi.HasilFileUrl = form.HasilFileUrl;
                    surveiDivisi.TindakLanjutFileUrl = form.TindakLanjutFileUrl;
                }
            } else {
                if (!emptyData) {
                    CreateDivisi(new CreateSurveiDivisiForm {
                        SurveiId = form.SurveiId,
                        Kategori = form.Kategori,
                        HasilFileUrl = form.HasilFileUrl,
                        TindakLanjutFileUrl = form.TindakLanjutFileUrl
                    });
                }
            }

            _context.SaveChanges();
            return surveiDivisi;
        }

        private void DeletePerusahaan(string id) {
            _context.SurveiPerusahaan.Remove(GetPerusahaan(id));
        }

        private void DeleteDivisi(string id) {
            _context.SurveiDivisi.Remove(GetDivisi(id));
        }
    }
}