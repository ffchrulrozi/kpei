﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSurvei.Forms {
    public class CreateSurveiDivisiForm {
        [MaxLength(128)]
        public string SurveiId { get; set; }
        
        [MaxLength(128)]
        public string Kategori { get; set; }
        
        public string HasilFileUrl { get; set; }
        public string TindakLanjutFileUrl { get; set; }
    }
}
