﻿using Kpei.PortalAK.DataAccess.DbModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSurvei.Forms {
    public class UpdateSurveiForm {
        [MaxLength(128)]
        public string Id { get; set; }

        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Nama")]
        [MaxLength(256)]
        public string Nama { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Tanggal Pelaksanaan")]
        public DateTime? TglPelaksanaanUtc { get; set; }
        [Required(ErrorMessage = "{0} tidak boleh kosong")]
        [Display(Name = "Jumlah Responden")]
        public int JlhResponden { get; set; }

        [Display(Name = "Nilai Survei")]
        public decimal Nilai { get; set; }

        public List<UpdateSurveiDivisiForm> ListDivisi { get; set; }
        public List<UpdateSurveiPerusahaanForm> ListPerusahaan { get; set; }
        
        public void Init(Survei dat) {
            Id = dat.Id;
            Nama = dat.Nama;
            TglPelaksanaanUtc = dat.TglPelaksanaanUtc;
            JlhResponden = dat.JlhResponden;
            Nilai = dat.Nilai;
        }
        
    }
}
