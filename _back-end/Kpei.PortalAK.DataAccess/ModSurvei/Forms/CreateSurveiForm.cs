﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSurvei.Forms {
    public class CreateSurveiForm {
        [Required(ErrorMessage = "{0} harus diisi")]
        [Display(Name = "Nama")]
        [MaxLength(256)]
        public string Nama { get; set; }

        [Required(ErrorMessage = "{0} harus diisi")]
        [Display(Name = "Tanggal Pelaksanaan")]
        public DateTime? TglPelaksanaanUtc { get; set; }

        [Required(ErrorMessage = "{0} harus diisi")]
        [Display(Name = "Jumlah Responden")]
        public int JlhResponden { get; set; }

        [Display(Name = "Nilai Survei")]
        public decimal Nilai { get; set; }
        
        public List<CreateSurveiPerusahaanForm> ListPerusahaan { get; set; }

        public List<CreateSurveiDivisiForm> ListDivisi { get; set; }
    }
}
