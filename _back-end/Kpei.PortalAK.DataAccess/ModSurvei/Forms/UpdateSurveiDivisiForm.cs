﻿using Kpei.PortalAK.DataAccess.DbModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSurvei.Forms {
    public class UpdateSurveiDivisiForm {
        [MaxLength(128)]
        public string Id { get; set; }

        [Required]
        [MaxLength(128)]
        public string SurveiId { get; set; }
        
        [MaxLength(128)]
        public string Kategori { get; set; }

        [DisplayName("File Hasil")]
        public string HasilFileUrl { get; set; }
        [DisplayName("File Tindak Lanjut")]
        public string TindakLanjutFileUrl { get; set; }
        
        public UpdateSurveiDivisiForm() { }

        public UpdateSurveiDivisiForm(SurveiDivisi dat) {
            Id = dat.Id;
            Kategori = dat.Kategori;
            HasilFileUrl = dat.HasilFileUrl;
            TindakLanjutFileUrl = dat.TindakLanjutFileUrl;
        }

    }
}
