﻿using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModSurvei.Forms {
    public class SurveiDttRequestForm : DttRequestForm{
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
