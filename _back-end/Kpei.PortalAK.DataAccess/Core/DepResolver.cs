﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kpei.PortalAK.DataAccess.Core {
    public interface IDepResolver {
        object GetService(Type serviceType);
        IEnumerable<object> GetServices(Type serviceType);
    }

    public static class DepResolver {
        private static Func<IDepResolver> _getInstanceFunc;
        public static Func<IDepResolver> GetInstanceFunc {
            get { return _getInstanceFunc; }
            set {
                if (_getInstanceFunc != null) {
                    throw new InvalidOperationException("DepResolver's Get func can only be set once at startup!");
                }

                _getInstanceFunc = value;
            }
        }

        public static IDepResolver Instance => GetInstanceFunc();

        public static T GetService<T>(this IDepResolver dr) {
            return (T) dr.GetService(typeof(T));
        }

        public static IEnumerable<T> GetServices<T>(this IDepResolver dr) {
            return dr.GetServices(typeof(T)).Cast<T>();
        }
    }
}