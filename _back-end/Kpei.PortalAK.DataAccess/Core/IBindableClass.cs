﻿namespace Kpei.PortalAK.DataAccess.Core {
    public interface IBindableClass {
        string ClassTypeFullName { get; }
    }
}