﻿using System.Linq;

namespace Kpei.PortalAK.DataAccess.Core {
    public class QueryPair<T> {
        public QueryPair(IQueryable<T> all, IQueryable<T> filtered) {
            All = all;
            Filtered = filtered;
        }

        public IQueryable<T> All { get; }
        public IQueryable<T> Filtered { get; }
    }
}