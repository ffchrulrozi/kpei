﻿using System.Collections.Generic;
using System.Linq;

namespace Kpei.PortalAK.DataAccess.Core {
    public class PaginatedResult<T> {
        private List<T> _data;

        private int _limit;

        private int _offset;
        private long _recordsFiltered;
        private long _recordsTotal;

        public List<T> Data {
            get { return _data ?? (_data = new List<T>()); }
            set { _data = value; }
        }

        public long DataTotal => Data.Count;

        public long RecordsTotal {
            get {
                if (_recordsTotal < 0) {
                    _recordsTotal = 0;
                }

                return _recordsTotal;
            }
            set { _recordsTotal = value; }
        }
        public long RecordsFiltered {
            get {
                if (_recordsFiltered < 0) {
                    _recordsFiltered = 0;
                }

                if (_recordsFiltered > _recordsTotal) {
                    _recordsFiltered = _recordsTotal;
                }

                return _recordsFiltered;
            }
            set { _recordsFiltered = value; }
        }
        public int Offset {
            get {
                if (_offset < 0) {
                    _offset = 0;
                }

                return _offset;
            }
            set { _offset = value; }
        }
        public int Limit {
            get {
                if (_limit < 1) {
                    _limit = 1;
                }

                return _limit;
            }
            set { _limit = value; }
        }

        public void Initialize(IQueryable<T> query, int offset, int limit) {
            Offset = offset;
            Limit = limit;
            var pagedQuery = query.Skip(Offset).Take(Limit);
            Data = pagedQuery.ToList();
            RecordsTotal = query.LongCount();
            RecordsFiltered = RecordsTotal;
        }

        public void Initialize(IQueryable<T> allQuery, IQueryable<T> filteredQuery, int offset, int limit) {
            Offset = offset;
            Limit = limit;
            var pagedQuery = filteredQuery.Skip(Offset).Take(Limit);
            Data = pagedQuery.ToList();
            RecordsTotal = allQuery.LongCount();
            RecordsFiltered = filteredQuery.LongCount();
        }
    }
}