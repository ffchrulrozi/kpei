﻿using System;

namespace Kpei.PortalAK.DataAccess.Core {
    public static class CoreModules {

        public const string DATA_AK_PARTISIPAN = nameof(DATA_AK_PARTISIPAN);
        public const string REQUEST_PINCODE = nameof(REQUEST_PINCODE);
        public const string ADMINISTRASI_KEANGGOTAAN = nameof(ADMINISTRASI_KEANGGOTAAN);
        public const string LAYANAN_M_CLEARS = nameof(LAYANAN_M_CLEARS);
        public const string SOSIALISASI_PELATIHAN = nameof(SOSIALISASI_PELATIHAN);
        public const string LAPORAN_KEUANGAN = nameof(LAPORAN_KEUANGAN);
        public const string SURVEI_KEANGGOTAAN = nameof(SURVEI_KEANGGOTAAN);
        public const string CORE_SYSTEM = nameof(CORE_SYSTEM);

        public static Tuple<string, string>[] IdAndNameTuples => new[] {
            Tuple.Create(DATA_AK_PARTISIPAN, NameOf(DATA_AK_PARTISIPAN)),
            Tuple.Create(REQUEST_PINCODE, NameOf(REQUEST_PINCODE)),
            Tuple.Create(ADMINISTRASI_KEANGGOTAAN, NameOf(ADMINISTRASI_KEANGGOTAAN)),
            Tuple.Create(LAYANAN_M_CLEARS, NameOf(LAYANAN_M_CLEARS)),
            Tuple.Create(SOSIALISASI_PELATIHAN, NameOf(SOSIALISASI_PELATIHAN)),
            Tuple.Create(LAPORAN_KEUANGAN, NameOf(LAPORAN_KEUANGAN)),
            Tuple.Create(SURVEI_KEANGGOTAAN, NameOf(SURVEI_KEANGGOTAAN)),
            Tuple.Create(CORE_SYSTEM, NameOf(CORE_SYSTEM))
        };

        public static string NameOf(string moduleId) {
            switch (moduleId) {
                case nameof(DATA_AK_PARTISIPAN):
                    return "Data AK & Partisipan";
                case nameof(REQUEST_PINCODE):
                    return "Reset PIN Code";
                case nameof(ADMINISTRASI_KEANGGOTAAN):
                    return "Administrasi Keanggotaan KPEI";
                case nameof(LAYANAN_M_CLEARS):
                    return "Layanan m-CLEARS";
                case nameof(SOSIALISASI_PELATIHAN):
                    return "Pelatihan & Sosialisasi";
                case nameof(LAPORAN_KEUANGAN):
                    return "Laporan Keuangan";
                case nameof(SURVEI_KEANGGOTAAN):
                    return "Survei Kepuasan Pelanggan";
                case nameof(CORE_SYSTEM):
                    return "Sistem Inti";
                default:
                    return "";
            }
        }
    }
}