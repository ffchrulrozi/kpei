﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModNotification.Models {
    public class MclearsNotification : BaseNotification {
        public string _icon;
        public override string Icon {
            get { return "fa fa-phone"; }
            set { _icon = value; }
        }
    }
}
