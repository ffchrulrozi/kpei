﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModNotification.Models {
    public class PincodeNotification : BaseNotification {
        public string _icon;
        public override string Icon {
            get { return "fa fa-key"; }
            set { _icon = value; }
        }
    }
}
