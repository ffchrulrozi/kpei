﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModNotification.Models {
    public class AknpNotification : BaseNotification {
        public AknpNotification() { }
        public string _icon;
        public override string Icon {
            get { return _icon ?? "fa fa-history"; }
            set { _icon = value; }
        }
    }
}
