﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModNotification.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Kpei.PortalAK.DataAccess.ModNotification.Models {
    public class ImportBaseData {
        public string Id { get; set; }
        public string KodeAK { get; set; }
        public DateTime? CreatedUtc { get; set; }
        public DateTime? UpdatedUtc { get; set; }
        public string dataSlug { get; set; }
        public string Status { get; set; }
    }

    public abstract class BaseNotification {
        
        public string Author { get; set; }
        public string Link { get; set; }
        public DateTime? Time { get; set; }
        public string Status { get; set; }
        public abstract string Icon { get; set; }
        public bool Seen { get; set; }
        public string Color {
            get {
                if (Status == DataAknpRequestStatus.NEW_STATUS.Name) {
                    return "label-info";
                } else if (Status == DataAknpRequestStatus.ONGOING_STATUS.Name) {
                    return "label-success";
                } else if (Status == DataAknpRequestStatus.APPROVED_STATUS.Name) {
                    return "label-default";
                } else if (Status == "Edit") {
                    return "label-warning";
                } else if (Status == DataAknpRequestStatus.REJECTED_STATUS.Name) {
                    return "label-danger";
                } else {
                    return "label-danger";
                }
            }
        }
        public string Message {
            get {
                if (Status == DataAknpRequestStatus.NEW_STATUS.Name) {
                    return !string.IsNullOrWhiteSpace(Author) ? Author + " mengajukan request baru!" : "Data baru!";
                } else if (Status == DataAknpRequestStatus.ONGOING_STATUS.Name) {
                    return "Request sedang diproses!";
                } else if (Status == DataAknpRequestStatus.APPROVED_STATUS.Name) {
                    return "Request telah disetujui!";
                } else if (Status == "Edit") {
                    return !string.IsNullOrWhiteSpace(Author) ?  Author + " melakukan perubahan terhadap request!" : "Ada perubahan pada data!";
                } else if (Status == DataAknpRequestStatus.REJECTED_STATUS.Name) {
                    return !string.IsNullOrWhiteSpace(Author) ? Author + " menolak request ini!" : "Request ditolak!";
                } else {
                    return "Tidak ada pesan";
                }
            }
        }
    }


    public class NotificationObject {
        public List<BaseNotification> Notifications { get; set; }
    }

    public class NotificationCountObject {
        public int NotificationCount {
            get {
                return AkteCount + AlamatCount + BankCount + ContactCount + JenisUsahaCount + InformasiCount + PejabatCount + MemberCount + PincodeCount;
            }
        }

        public int AkteCount { get; set; }
        public int AlamatCount { get; set; }
        public int BankCount { get; set; }
        public int ContactCount { get; set; }
        public int JenisUsahaCount { get; set; }
        public int InformasiCount { get; set; }
        public int PejabatCount { get; set; }
        public int MemberCount { get; set; }

        public int PincodeCount { get; set; }
        public int MclearsCount { get; set; }
        public int EventCount { get; set; }
        public int KeuCount { get; set; }
        public int SurveyCount { get; set; }
    }

    public class NotificationCountJsonObject {
        public int NotificationCount { get; set; }
    }
}
