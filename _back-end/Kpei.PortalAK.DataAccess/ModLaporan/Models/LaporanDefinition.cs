﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Models {
    public class LaporanDefinition {
        public string DataSlug { get; }
        public string DataTitle { get; }
        
        public LaporanDefinition(string dataSlug, string dataTitle) {
            DataSlug = dataSlug;
            DataTitle = dataTitle;
        }
    }
}
