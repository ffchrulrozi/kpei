﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.ModLaporan.Models;
using Kpei.PortalAK.DataAccess.ModLaporan.Forms;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Services {
    public class LaporanDriverStore : ILaporanDriverStore {
        private readonly IEnumerable<BaseLaporanDriver> _drivers;
        public LaporanDriverStore(IEnumerable<BaseLaporanDriver> drivers) {
            _drivers = drivers.OrderBy(x => x.DataSlug);

        }

        public IEnumerable<BaseLaporanDriver> LoadDrivers() {
            return _drivers;
        }

        public IEnumerable<LaporanDefinition> LoadDefinition() {
            throw new NotImplementedException();
        }
    }
}
