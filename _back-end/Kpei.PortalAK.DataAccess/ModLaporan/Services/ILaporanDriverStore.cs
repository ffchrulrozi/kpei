﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.ModLaporan.Models;
using Kpei.PortalAK.DataAccess.ModLaporan.Forms;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Services {
    public interface ILaporanDriverStore {
        IEnumerable<BaseLaporanDriver> LoadDrivers();
        IEnumerable<LaporanDefinition> LoadDefinition();
    }
}

