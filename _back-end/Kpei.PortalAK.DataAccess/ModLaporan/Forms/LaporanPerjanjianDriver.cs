﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using System.Linq.Dynamic;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using System.Globalization;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.Services;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanPerjanjianDriver : BaseLaporanDriver {
        public LaporanPerjanjianDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "perjanjian";

        public override string DataTitle => "Laporan Perjanjian";

        public override bool FilterByStatusKpei => false;
        public override bool FilterByTipeMemberPartisipan => true;
        public override bool FilterByTipeMember => true;

        public override bool FilterByYear => true;

        public override Type DataClassType => typeof(LaporanPerjanjian);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            currentQuery = from x in currentQuery
                group x by new { x.KodeAk, x.AkActiveUtc } into y
                select y.OrderByDescending(t => t.AkActiveUtc).FirstOrDefault();

            if (!string.IsNullOrWhiteSpace(req.Perjanjian)) {
                var q = currentQuery.Cast<LaporanPerjanjian>();
                if (q != null) {
                    if (!string.IsNullOrWhiteSpace(req.ActiveYear)) {
                        DateTime thn;
                        if (DateTime.TryParseExact(req.ActiveYear, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out thn)) {
                            if (req.KBOS) {
                                q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianKbosActiveUtc)} != null", string.Empty);
                                q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianKbosActiveUtc)} < @0 and {nameof(LaporanPerjanjian.PerjanjianKbosActiveUtc)} > @1",
                                    thn.AddYears(1), thn.AddDays(-1));
                                q = from x in q
                                    group x by new { x.KodeAk, x.PerjanjianKbosActiveUtc, x.AkActiveUtc } into y
                                    select y.OrderByDescending(t => t.PerjanjianKbosActiveUtc).FirstOrDefault();
                            } else if (req.EBU) {
                                q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianEbuActiveUtc)} != null", string.Empty);
                                q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianEbuActiveUtc)} < @0 and {nameof(LaporanPerjanjian.PerjanjianEbuActiveUtc)} > @1",
                                    thn.AddYears(1), thn.AddDays(-1));
                                q = from x in q
                                    group x by new { x.KodeAk, x.PerjanjianEbuActiveUtc, x.AkActiveUtc } into y
                                    select y.OrderByDescending(t => t.PerjanjianEbuActiveUtc).FirstOrDefault();
                            } else if (req.PME) {
                                q = q.Where($"{nameof(LaporanPerjanjian.PmeActiveUtc)} != null", string.Empty);
                                q = q.Where($"{nameof(LaporanPerjanjian.PmeActiveUtc)} < @0 and {nameof(LaporanPerjanjian.PmeActiveUtc)} > @1",
                                    thn.AddYears(1), thn.AddDays(-1));
                                q = from x in q
                                    group x by new { x.KodeAk, x.PmeActiveUtc, x.AkActiveUtc } into y
                                    select y.OrderByDescending(t => t.PmeActiveUtc).FirstOrDefault();
                            } else if (req.AK) {
                                q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianAkActiveUtc)} != null", string.Empty);
                                q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianAkActiveUtc)} < @0 and {nameof(LaporanPerjanjian.PerjanjianAkActiveUtc)} > @1",
                                    thn.AddYears(1), thn.AddDays(-1));
                                q = from x in q
                                    group x by new { x.KodeAk, x.PerjanjianAkActiveUtc, x.AkActiveUtc } into y
                                    select y.OrderByDescending(t => t.PerjanjianAkActiveUtc).FirstOrDefault();
                            }
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(req.AkActiveYear)) {
                        DateTime thn2;
                        DateTime.TryParseExact(req.AkActiveYear, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out thn2);

                        var where = new StringBuilder();
                        var pars = new List<object>();

                        where.Append($@" {nameof(BaseLaporan.AkActiveUtc)} < @0 and {nameof(BaseLaporan.AkActiveUtc)} > @1 ");
                        pars.Add(thn2.AddYears(1));
                        pars.Add(thn2.AddDays(-1));
                        var parsCt = pars.Count();

                        //create where limit for each code

                        var aksCur = q
                            .Where(e => e.ActiveUtc < thn2.AddYears(1) && e.ActiveUtc > thn2.AddDays(-1))
                            .Select(e => e.KodeAk)
                            .Distinct()
                            .ToList();

                        var aks = q
                            .Where(e => e.AkActiveUtc < thn2.AddDays(-1))
                            .Select(e => e.KodeAk)
                            .Distinct()
                            .ToList();
                        if (req.KBOS) {
                            for (int i = 0; i < aks.Count(); i++) {

                                if (!aksCur.Contains(aks[i])) {
                                    where.Append(" or ");

                                    var lastRec = q
                                        .Where(e => e.AkActiveUtc < thn2 && e.KodeAk == aks[i])?
                                        .OrderByDescending(e => e.AkActiveUtc)
                                        .ThenByDescending(e => e.PerjanjianKbosActiveUtc)
                                        .FirstOrDefault()?
                                        .AkActiveUtc;

                                    var lastRec2 = q
                                        .Where(e => e.AkActiveUtc < thn2 && e.KodeAk == aks[i])?
                                        .OrderByDescending(e => e.AkActiveUtc)
                                        .ThenByDescending(e => e.PerjanjianKbosActiveUtc)
                                        .FirstOrDefault()?
                                        .ActiveUtc;

                                    if (lastRec != null || lastRec2 != null) {
                                        where.Append($" (" +
                                           (lastRec != null ? $"{nameof(BaseLaporan.AkActiveUtc)} = @{parsCt++} and " : "") +
                                            (lastRec2 != null ? $" {nameof(LaporanPerjanjian.PerjanjianKbosActiveUtc)} = @{parsCt++} and " : "") +
                                            $" {nameof(BaseLaporan.KodeAk)} = @{parsCt++} and " +
                                            $" {nameof(BaseLaporan.AkActiveUtc)} < @{parsCt++}) ");
                                        if (lastRec != null) pars.Add((DateTime)lastRec);
                                        if (lastRec2 != null) pars.Add((DateTime)lastRec2);
                                        pars.Add(aks[i]);
                                        pars.Add(thn2.AddYears(1));
                                    }
                                }
                            }
                        } else if (req.EBU) {
                            for (int i = 0; i < aks.Count(); i++) {

                                if (!aksCur.Contains(aks[i])) {
                                    where.Append(" or ");

                                    var lastRec = q
                                        .Where(e => e.AkActiveUtc < thn2 && e.KodeAk == aks[i])?
                                        .OrderByDescending(e => e.AkActiveUtc)
                                        .ThenByDescending(e => e.PerjanjianEbuActiveUtc)
                                        .FirstOrDefault()?
                                        .AkActiveUtc;

                                    var lastRec2 = q
                                        .Where(e => e.AkActiveUtc < thn2 && e.KodeAk == aks[i])?
                                        .OrderByDescending(e => e.AkActiveUtc)
                                        .ThenByDescending(e => e.PerjanjianEbuActiveUtc)
                                        .FirstOrDefault()?
                                        .ActiveUtc;

                                    if (lastRec != null || lastRec2 != null) {
                                        where.Append($" (" +
                                           (lastRec != null ? $"{nameof(BaseLaporan.AkActiveUtc)} = @{parsCt++} and " : "") +
                                            (lastRec2 != null ? $" {nameof(LaporanPerjanjian.PerjanjianEbuActiveUtc)} = @{parsCt++} and " : "") +
                                            $" {nameof(BaseLaporan.KodeAk)} = @{parsCt++} and " +
                                            $" {nameof(BaseLaporan.AkActiveUtc)} < @{parsCt++}) ");
                                        if (lastRec != null) pars.Add((DateTime)lastRec);
                                        if (lastRec2 != null) pars.Add((DateTime)lastRec2);
                                        pars.Add(aks[i]);
                                        pars.Add(thn2.AddYears(1));
                                    }
                                }
                            }
                        } else if (req.PME) {
                            for (int i = 0; i < aks.Count(); i++) {

                                if (!aksCur.Contains(aks[i])) {
                                    where.Append(" or ");

                                    var lastRec = q
                                        .Where(e => e.AkActiveUtc < thn2 && e.KodeAk == aks[i])?
                                        .OrderByDescending(e => e.AkActiveUtc)
                                        .ThenByDescending(e => e.PmeActiveUtc)
                                        .FirstOrDefault()?
                                        .AkActiveUtc;

                                    var lastRec2 = q
                                        .Where(e => e.AkActiveUtc < thn2 && e.KodeAk == aks[i])?
                                        .OrderByDescending(e => e.AkActiveUtc)
                                        .ThenByDescending(e => e.PmeActiveUtc)
                                        .FirstOrDefault()?
                                        .ActiveUtc;

                                    if (lastRec != null || lastRec2 != null) {
                                        where.Append($" (" +
                                           (lastRec != null ? $"{nameof(BaseLaporan.AkActiveUtc)} = @{parsCt++} and " : "") +
                                            (lastRec2 != null ? $" {nameof(LaporanPerjanjian.PmeActiveUtc)} = @{parsCt++} and " : "") +
                                            $" {nameof(BaseLaporan.KodeAk)} = @{parsCt++} and " +
                                            $" {nameof(BaseLaporan.AkActiveUtc)} < @{parsCt++}) ");
                                        if (lastRec != null) pars.Add((DateTime)lastRec);
                                        if (lastRec2 != null) pars.Add((DateTime)lastRec2);
                                        pars.Add(aks[i]);
                                        pars.Add(thn2.AddYears(1));
                                    }
                                }
                            }
                        } else if (req.AK) {
                            for (int i = 0; i < aks.Count(); i++) {

                                if (!aksCur.Contains(aks[i])) {
                                    where.Append(" or ");

                                    var lastRec = q
                                        .Where(e => e.AkActiveUtc < thn2 && e.KodeAk == aks[i])?
                                        .OrderByDescending(e => e.AkActiveUtc)
                                        .ThenByDescending(e => e.PerjanjianAkActiveUtc)
                                        .FirstOrDefault()?
                                        .AkActiveUtc;

                                    var lastRec2 = q
                                        .Where(e => e.AkActiveUtc < thn2 && e.KodeAk == aks[i])?
                                        .OrderByDescending(e => e.AkActiveUtc)
                                        .ThenByDescending(e => e.PerjanjianAkActiveUtc)
                                        .FirstOrDefault()?
                                        .ActiveUtc;

                                    if (lastRec != null || lastRec2 != null) {
                                        where.Append($" (" +
                                           (lastRec != null ? $"{nameof(BaseLaporan.AkActiveUtc)} = @{parsCt++} and " : "") +
                                            (lastRec2 != null ? $" {nameof(LaporanPerjanjian.PerjanjianAkActiveUtc)} = @{parsCt++} and " : "") +
                                            $" {nameof(BaseLaporan.KodeAk)} = @{parsCt++} and " +
                                            $" {nameof(BaseLaporan.AkActiveUtc)} < @{parsCt++}) ");
                                        if (lastRec != null) pars.Add((DateTime)lastRec);
                                        if (lastRec2 != null) pars.Add((DateTime)lastRec2);
                                        pars.Add(aks[i]);
                                        pars.Add(thn2.AddYears(1));
                                    }
                                }
                            }
                        }


                        if (pars.Count() > 0)
                            q = q.Where("(" + where.ToString() + ")", pars.ToArray());
                    }
                    if (req.FromDateTime != null) {
                        var fromUtc = req.FromDateTime;
                        if (req.KBOS) {
                            q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianKbosActiveUtc)} >= @0", fromUtc);
                        } else if (req.EBU) {
                            q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianEbuActiveUtc)} >= @0", fromUtc);
                        } else if (req.PME) {
                            q = q.Where($"{nameof(LaporanPerjanjian.PmeActiveUtc)} >= @0", fromUtc);
                        } else if (req.AK) {
                            q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianAkActiveUtc)} >= @0", fromUtc);
                        }
                    }
                    if (req.ToDateTime != null) {
                        var toUtc = req.ToDateTime;
                        if (req.KBOS) {
                            q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianKbosActiveUtc)} <= @0", toUtc);
                        } else if (req.EBU) {
                            q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianEbuActiveUtc)} <= @0", toUtc);
                        } else if (req.PME) {
                            q = q.Where($"{nameof(LaporanPerjanjian.PmeActiveUtc)} <= @0", toUtc);
                        } else if (req.AK) {
                            q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianAkActiveUtc)} <= @0", toUtc);
                        }
                    }
                    if (req.KBOS) {
                        q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianKbosActiveUtc)} != null", string.Empty);
                        q = from x in q
                            group x by new { x.KodeAk, x.PerjanjianKbosActiveUtc, x.AkActiveUtc } into y
                            select y.OrderByDescending(t => t.PerjanjianKbosActiveUtc).FirstOrDefault();
                    } else if (req.EBU) {
                        q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianEbuActiveUtc)} != null", string.Empty);
                        q = from x in q
                            group x by new { x.KodeAk, x.PerjanjianEbuActiveUtc, x.AkActiveUtc } into y
                            select y.OrderByDescending(t => t.PerjanjianEbuActiveUtc).FirstOrDefault();
                    } else if (req.PME) {
                        q = q.Where($"{nameof(LaporanPerjanjian.PmeActiveUtc)} != null", string.Empty);
                        q = from x in q
                            group x by new { x.KodeAk, x.PmeActiveUtc, x.AkActiveUtc } into y
                            select y.OrderByDescending(t => t.PmeActiveUtc).FirstOrDefault();
                    } else if (req.AK) {
                        q = q.Where($"{nameof(LaporanPerjanjian.PerjanjianAkActiveUtc)} != null", string.Empty);
                        q = from x in q
                            group x by new { x.KodeAk, x.PerjanjianAkActiveUtc, x.AkActiveUtc } into y
                            select y.OrderByDescending(t => t.PerjanjianAkActiveUtc).FirstOrDefault();
                    }
                    currentQuery = q;
                }   
            }

            currentQuery = currentQuery.GroupBy(x => x.KodeAk)
                .Select(x => x.OrderByDescending(o => o.AkActiveUtc).ElementAt(0));

            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            var sql = $@"
select * from V_AK_Perjanjian

";
            var data = Db.Database.SqlQuery<LaporanPerjanjian>(sql).ToArray();
            ToLocalTime(data);
            return data.AsQueryable();
        }


        public void ToLocalTime(IEnumerable<LaporanPerjanjian> dats) {

            foreach (var dat in dats) {
                BaseToLocalTime(dat);

                dat.TglPerjanjianAwalEbuUtc = dat.TglPerjanjianAwalEbuUtc?.AddHours(7);
                dat.TglPerjanjianAkhirEbuUtc = dat.TglPerjanjianAkhirEbuUtc?.AddHours(7);
                dat.TglPerjanjianKbosSpmUtc = dat.TglPerjanjianKbosSpmUtc?.AddHours(7);
                dat.TglPerjanjianAwalPmeLenderUtc = dat.TglPerjanjianAwalPmeLenderUtc?.AddHours(7);
                dat.TglPerjanjianAkhirPmeLenderUtc = dat.TglPerjanjianAkhirPmeLenderUtc?.AddHours(7);
                dat.TglPerjanjianAwalPmeBorrowerUtc = dat.TglPerjanjianAwalPmeBorrowerUtc?.AddHours(7);
                dat.TglPerjanjianAkhirPmeBorrowerUtc = dat.TglPerjanjianAkhirPmeBorrowerUtc?.AddHours(7);
                dat.TglPerjanjianAk = dat.TglPerjanjianAk?.AddHours(7);
                
                dat.PerjanjianAkActiveUtc = dat.PerjanjianAkActiveUtc?.AddHours(7);
                dat.PerjanjianEbuActiveUtc = dat.PerjanjianEbuActiveUtc?.AddHours(7);
                dat.PerjanjianKbosActiveUtc = dat.PerjanjianKbosActiveUtc?.AddHours(7);
                dat.PmeActiveUtc = dat.PmeActiveUtc?.AddHours(7);
            }
            
        }
    }
}
