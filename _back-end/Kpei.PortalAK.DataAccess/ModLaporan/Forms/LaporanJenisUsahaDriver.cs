﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.Core;
using System.Linq.Dynamic;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.Services;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanJenisUsahaDriver : BaseLaporanDriver {
        public LaporanJenisUsahaDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "ijin-usaha";

        public override string DataTitle => "Laporan Ijin Usaha";

        public override bool FilterByTipeMemberPartisipan => false;
        public override bool FilterByStatusKpei => false;
        public override bool FilterByTipeMember => false;
        public override bool FilterByYear => true;

        public override Type DataClassType => typeof(LaporanJenisUsaha);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            var q = currentQuery.Cast<LaporanJenisUsaha>();
            if (req.PPE) {
                q = q.Where($"{nameof(LaporanJenisUsaha.PPE)} != null && {nameof(LaporanJenisUsaha.PPE)} != @0", string.Empty);
            }
            if (req.PEE) {
                q = q.Where($"{nameof(LaporanJenisUsaha.PEE)} != null && {nameof(LaporanJenisUsaha.PEE)} != @0", string.Empty);
            }
            if (req.MI) {
                q = q.Where($"{nameof(LaporanJenisUsaha.MI)} != null && {nameof(LaporanJenisUsaha.MI)} != @0", string.Empty);
            }
            if (req.PED) {
                q = q.Where($"{nameof(LaporanJenisUsaha.PED)} != null && {nameof(LaporanJenisUsaha.PED)} != @0", string.Empty);
            }
            if (req.PPE || req.PEE || req.MI || req.PED) {
                currentQuery = q;
            }

            currentQuery = from x in currentQuery
                           group x by new { x.Id, x.KodeAk, x.ActiveUtc, x.AkActiveUtc } into y
                           select y.OrderByDescending(t => t.AkActiveUtc).FirstOrDefault();
            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            var companies = _companyService.AllKpeiCompanies().Select(x => x.Code).ToArray();
            var utcNow = DateTime.UtcNow;
            var queryAK = Db.AdministrasiKeanggotaan
                .Where(x => companies.Contains(x.KodeAK))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.UserDraft == null)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow);
            var data = Db.DataJenisUsaha
                .Where(x => companies.Contains(x.KodeAk))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .GroupBy(x => x.KodeAk)
                .SelectMany(b => 
                    b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
                .Select(a => new LaporanJenisUsaha
                {
                    KodeAk = a.KodeAk,
                    ActiveUtc = a.ActiveUtc,
                    CreatedUtc = a.CreatedUtc,
                    Status = a.Status,
                    MI = a.MI,
                    PEE = a.PEE,
                    PPE = a.PPE,
                    PED = queryAK
                        .Where(x => x.KodeAK == a.KodeAk)
                        .Where(x => x.KategoriMember == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name).FirstOrDefault(x => x.TipeMemberPartisipan == "PED").NoIjinUsahaPED
                }).ToArray();
            ToLocalTime(data);
            return data.AsQueryable();
        }

        public void ToLocalTime(IEnumerable<LaporanJenisUsaha> dats) {
            foreach (var dat in dats) {
                BaseToLocalTime(dat);
            }
        }
    }
}
