﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanPerubahanNamaDriver : BaseLaporanDriver {
        public LaporanPerubahanNamaDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "perubahan-nama";

        public override string DataTitle => "Laporan Perubahan Nama";

        public override bool FilterByStatusKpei => false;
        public override bool FilterByTipeMemberPartisipan => false;
        public override bool FilterByTipeMember => false;

        public override bool FilterByYear => true;

        public override Type DataClassType => typeof(LaporanPerubahanNama);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            var q = currentQuery.Cast<LaporanPerubahanNama>();
            if (q != null) {
                q = from x in q
                    group x by new { x.Id, x.KodeAk, x.NamaSebelumnya } into y
                    select y.OrderBy(t => t.TglNamaBerlakuSetelahnya).FirstOrDefault();
                currentQuery = q;
            }
            return currentQuery;
        }

        public string GetNamaSebelumnya(string kodeAk, DateTime? active)
        {
            var date = active.Value.ToUniversalTime();
            var data = Db.DataAkte
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.KodeAk == kodeAk)
                .Where(x => x.Draft == false)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.TglNamaBerlaku < date)
                .GroupBy(x => x.KodeAk)
                .SelectMany(b =>
                    b.OrderByDescending(o => o.TglNamaBerlaku).Take(1))
                .Select(x => x.NamaPerusahaan).FirstOrDefault();
            return data;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            // var data = Db.Database.SqlQuery<LaporanPerubahanNama>($@"
            //     select * from dbo.V_AK_Perubahan_Nama
            // ").ToArray();
            var companies = _companyService.AllKpeiCompanies().Select(x => x.Code).ToArray();
            var utcNow = DateTime.UtcNow;
            var data = Db.DataAkte
                .Where(x => companies.Contains(x.KodeAk))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .GroupBy(x => x.KodeAk)
                .SelectMany(b => 
                    b.OrderByDescending(o => o.TglNamaBerlaku).Take(1))
                .Select(a => new LaporanPerubahanNama
                {
                    KodeAk = a.KodeAk,
                    ActiveUtc = a.ActiveUtc,
                    CreatedUtc = a.CreatedUtc,
                    Status = a.Status,
                    TempActiveUtc = a.TglNamaBerlaku
                }).ToArray();
            ToLocalTime(data);
            return data.AsQueryable();
        }

        public void ToLocalTime(IEnumerable<LaporanPerubahanNama> dats) {
            foreach (var dat in dats) {
                BaseToLocalTime(dat);
                dat.TglNamaBerlakuSebelumnya = dat.TglNamaBerlakuSebelumnya.AddHours(7);
                dat.TglNamaBerlakuSetelahnya = dat.TglNamaBerlakuSetelahnya.AddHours(7);

            }
        }
    }
}
