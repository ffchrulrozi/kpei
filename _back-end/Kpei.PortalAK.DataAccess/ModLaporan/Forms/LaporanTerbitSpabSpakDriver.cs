﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;
using System.Globalization;
using System.Linq.Dynamic;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanTerbitSpabSpakDriver : BaseLaporanDriver {
        public LaporanTerbitSpabSpakDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "terbit-spab-spak";

        public override string DataTitle => "Laporan Terbit SPAB & SPAK";

        public override bool FilterByStatusKpei => false;
        public override bool FilterByTipeMemberPartisipan => false;
        public override bool FilterByTipeMember => false;
        public override bool FilterByYear => true;

        public override Type DataClassType => typeof(LaporanTerbitSpabSpak);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            var q = currentQuery.Cast<LaporanTerbitSpabSpak>();
            if (q != null) {
                q = from x in q
                    group x by new { x.Id, x.KodeAk, x.ActiveUtc, x.AkActiveUtc, x.TglSpabUtc, x.TglSpakUtc
                        //,x.SpabActiveUtc, x.SpakActiveUtc
                    } into y
                    select y.OrderByDescending(t => t.AkActiveUtc).FirstOrDefault();
            }
            return q;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            var sql = $@"
select * from V_AK_Terbit_SPAB_SPAK

";
            var data = Db.Database.SqlQuery<LaporanTerbitSpabSpak>(sql).ToArray();
            return data.AsQueryable();
        }

        public void ToLocalTime(IEnumerable<LaporanTerbitSpabSpak> dats) {
            foreach (var dat in dats) {
                BaseToLocalTime(dat);

                dat.TglSpabUtc = dat.TglSpabUtc?.AddHours(7);
                dat.TglSpakUtc = dat.TglSpakUtc?.AddHours(7);
            }
        }
    }
}
