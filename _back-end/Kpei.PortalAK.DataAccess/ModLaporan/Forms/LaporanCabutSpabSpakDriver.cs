﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanCabutSpabSpakDriver : BaseLaporanDriver {
        public LaporanCabutSpabSpakDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "cabut-spab-spak";

        public override string DataTitle => "Laporan Cabut SPAB & SPAK";

        public override bool FilterByStatusKpei => false;

        public override bool FilterByTipeMemberPartisipan => true;
        public override bool FilterByTipeMember => false;


        public override bool FilterByYear => true;

        public override Type DataClassType => typeof(LaporanCabutSpabSpak);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            currentQuery = from x in currentQuery
                           group x by new { x.Id, x.KodeAk, x.ActiveUtc, x.AkActiveUtc } into y
                           select y.OrderByDescending(t => t.AkActiveUtc).FirstOrDefault();
            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            var data = Db.Database.SqlQuery<LaporanCabutSpabSpak>("select * from V_AK_CabutSPAB_CabutSPAK").ToArray();
            ToLocalTime(data);
            return data.AsQueryable();
        }

        public void ToLocalTime(IEnumerable<LaporanCabutSpabSpak> dats) {
            foreach (var dat in dats) {
                base.BaseToLocalTime(dat);

                dat.TanggalCabutSpab = dat.TanggalCabutSpab?.AddHours(7);
                dat.TanggalCabutSpak = dat.TanggalCabutSpak?.AddHours(7);
                dat.SpabActiveUtc = dat.SpabActiveUtc?.AddHours(7);
                dat.SpakActiveUtc = dat.SpakActiveUtc?.AddHours(7);
            }
        }
    }
}
