﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanAlamatDriver : BaseLaporanDriver {
        public LaporanAlamatDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "alamat";

        public override string DataTitle => "Laporan Alamat";

        public override Type DataClassType => typeof(LaporanAlamat);

        public override bool FilterByTipeMemberPartisipan => true;
        public override bool FilterByStatusKpei => false;

        public override bool FilterByTipeMember => false;

        public override bool FilterByYear => true;

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            currentQuery = from x in currentQuery
                           group x by new { x.Id, x.KodeAk, x.ActiveUtc, x.AkActiveUtc } into y
                           select y.OrderByDescending(t => t.AkActiveUtc).FirstOrDefault();
            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal()
        {
            var companies = _companyService.AllKpeiCompanies().Select(x => x.Code).ToArray();
            var utcNow = DateTime.UtcNow;
            var data = Db.DataAlamat
                .Where(x => companies.Contains(x.KodeAk))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .GroupBy(x => x.KodeAk)
                .SelectMany(b =>
                    b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
                .Select(a => new LaporanAlamat
                {
                    KodeAk = a.KodeAk,
                    ActiveUtc = a.ActiveUtc,
                    CreatedUtc = a.CreatedUtc,
                    Status = a.Status,
                    Gedung = a.Gedung,
                    Jalan = a.Jalan,
                    KodePos = a.KodePos,
                    Kota = a.Kota,
                    Telp = a.Telp,
                    Fax = a.Fax,
                }).ToArray();

            ToLocalTime(data);
            return data.AsQueryable();
        }

        public void ToLocalTime(IEnumerable<LaporanAlamat> dats) {
            foreach (var dat in dats) {
                base.BaseToLocalTime(dat);
            }
        }

    }
}
