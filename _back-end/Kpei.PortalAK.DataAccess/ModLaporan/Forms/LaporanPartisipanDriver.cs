﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanPartisipanDriver : BaseLaporanDriver {
        public LaporanPartisipanDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "partisipan";

        public override string DataTitle => "Laporan Partisipan";

        public override bool FilterByStatusKpei => false;

        public override bool HideFromMenu => true;
        public override bool FilterByYear => true;
        public override bool FilterByTipeMemberPartisipan => true;
        public override bool FilterByTipeMember => false;

        public override Type DataClassType => typeof(LaporanPartisipan);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            if (currentQuery != null) {
                var q = currentQuery.Cast<LaporanPartisipan>();
                if (q != null && req.MemberPartisipan != null) {
                    q = q.Where(e => e.TipeMember.Split(',').Select(p => p.Trim()).Contains(req.MemberPartisipan));
                }
                currentQuery = q;
            }

            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            var data = Db.Database.SqlQuery<LaporanPartisipan>($@"
Select * from V_Partisipan_All
").ToList();
            return data.AsQueryable();
        }

    }
}
