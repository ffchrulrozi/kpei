﻿using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanStatusPerusahaanDriver : BaseLaporanDriver {
        public LaporanStatusPerusahaanDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "status-perusahaan";

        public override string DataTitle => "Laporan Status Perusahaan";
        public override bool FilterByTipeMemberPartisipan => false;
        public override bool FilterByTipeMember => false;
        public override bool FilterByStatusKpei => false;

        public override bool FilterByYear => true;

        public override Type DataClassType => typeof(LaporanStatusPerusahaan);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            if (!string.IsNullOrWhiteSpace(req.StatusPerusahaan)) {
                var q = currentQuery.Cast<LaporanStatusPerusahaan>();
                if (q != null) {
                    currentQuery = q.Where(e => e.StatusPerusahaan == req.StatusPerusahaan);
                }
            }
            currentQuery = from x in currentQuery
                           group x by new { x.Id, x.KodeAk, x.ActiveUtc, x.AkActiveUtc } into y
                select y.OrderByDescending(t => t.AkActiveUtc).FirstOrDefault();
            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            var companies = _companyService.AllKpeiCompanies().Select(x => x.Code).ToArray();
            var utcNow = DateTime.UtcNow;
            var data = Db.DataInformasiLain
                .Where(x => companies.Contains(x.KodeAk))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .GroupBy(x => x.KodeAk)
                .SelectMany(b => 
                    b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
                .Select(a => new LaporanStatusPerusahaan
                {
                    KodeAk = a.KodeAk,
                    ActiveUtc = a.ActiveUtc,
                    CreatedUtc = a.CreatedUtc,
                    Status = a.Status,
                    StatusPerusahaan = a.StatusPerusahaan
                }).ToArray();

            ToLocalTime(data);
            return data.AsQueryable();
        }


        public void ToLocalTime(IEnumerable<LaporanStatusPerusahaan> dats) {
            foreach (var dat in dats) {
                BaseToLocalTime(dat);
            }
        }
    }
}
