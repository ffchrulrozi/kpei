﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;
using System.Linq.Dynamic;
using System.Globalization;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanPemegangSahamDriver : BaseLaporanDriver {
        public LaporanPemegangSahamDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "pemegang-saham";

        public override string DataTitle => "Laporan Pemegang Saham";

        public override bool FilterByTipeMember => true;
        public override bool FilterByStatusKpei => false;
        public override bool FilterByTipeMemberPartisipan => true;

        public override bool FilterByYear => true;

        public override Type DataClassType => typeof(LaporanPemegangSaham);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req)
        {
            currentQuery = currentQuery.GroupBy(x => x.Id)
                .SelectMany(x => x.Take(1)).Select(x => x);
            return currentQuery;
        }
        private IQueryable<LaporanPemegangSaham> CustomFilterYear(IQueryable<LaporanPemegangSaham> q, BaseLaporanDttRequestForm req) {
            //if (!string.IsNullOrWhiteSpace(req.AkActiveYear) && q.All(e => e.ActiveUtc != null)) {
            //    DateTime thn;
            //    DateTime.TryParseExact(req.AkActiveYear, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out thn);

            //    var filteredYear = q.Where($"{nameof(BaseLaporan.ActiveUtc)} < @0 && {nameof(BaseLaporan.ActiveUtc)} > @1", thn.AddYears(1), thn.AddYears(-1));

            //    //populate old data (out of range) from AK who didn't make change in selected year.
            //    var AllAk = q.Select(e => e.KodeAk).Distinct().AsQueryable().ToList();
            //    var targetData = q.GroupBy(e => e.ActiveUtc).Where(e => e.Key < thn).OrderByDescending(t => t.Key).FirstOrDefault();
            //    if (targetData != null) {
            //        var targets = targetData.Select(e => new { e.KodeAk, e.ActiveUtc }).Distinct().ToArray();

            //        foreach (var target in targets) {
            //            filteredYear = filteredYear.Union(q.Where(e => e.KodeAk == target.KodeAk && e.ActiveUtc == target.ActiveUtc));
            //        }

            //        var peekFilteredYear = filteredYear.ToList();
            //        var joinedData = filteredYear;
            //        q = joinedData;
            //    }
                
            //}
            return q;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            var companies = _companyService.AllKpeiCompanies().Select(x => x.Code).ToArray();
            var utcNow = DateTime.UtcNow;

            var queryAK = Db.AdministrasiKeanggotaan
                .Where(x => companies.Contains(x.KodeAK))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.UserDraft == null)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow);
            var queryDA = Db.DataAkte
                .Where(x => companies.Contains(x.KodeAk))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .GroupBy(x => x.KodeAk)
                .SelectMany(b => 
                    b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
                .Select(x => x);
            var data = Db.DataPemegangSaham
                .Join(
                    queryDA,
                    dps => dps.AkteId,
                    da => da.Id,
                    (dps, da) => new {dps, da})
                .Join(
                    queryAK,
                    dps => dps.da.KodeAk,
                    ak => ak.KodeAK,
                    (dps, ak) => new {dps.dps, dps.da, ak})
                .Select(x => new LaporanPemegangSaham
                {
                    Id = x.dps.Id,
                    KategoriMember = x.ak.KategoriMember,
                    KodeAk = x.da.KodeAk,
                    NamaPemegangSaham = x.dps.Nama,
                    ActiveUtc = x.da.ActiveUtc,
                    LembarSaham = x.dps.LembarSaham,
                    TipePartisipan = x.ak.TipeMemberPartisipan,
                    TotalSaham = Db.DataPemegangSaham.Where(w => w.AkteId == x.da.Id).GroupBy(g => g.AkteId).Select(s => s.Sum(z => z.LembarSaham)).FirstOrDefault()
                }).ToArray();
            ToLocalTime(data);
            return data.AsQueryable();
        }

        public void ToLocalTime(IEnumerable<LaporanPemegangSaham> dats) {
            foreach (var dat in dats) {
                BaseToLocalTime(dat);
            }
        }
    }
}
