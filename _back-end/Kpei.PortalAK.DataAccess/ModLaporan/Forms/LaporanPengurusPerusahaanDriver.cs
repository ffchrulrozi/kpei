﻿using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanPengurusPerusahaanDriver : BaseLaporanDriver {
        public LaporanPengurusPerusahaanDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "pengurus-perusahaan";

        public override string DataTitle => "Laporan Pengurus Perusahaan";

        public override bool FilterByStatusKpei => false;

        public override bool FilterByTipeMember => true;
        public override bool FilterByTipeMemberPartisipan => true;
        public override bool FilterByYear => true;

        public override Type DataClassType => typeof(LaporanPengurusPerusahaan);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req)
        {
            currentQuery = currentQuery.GroupBy(x => x.KodeAk)
                .SelectMany(b => b.Take(1))
                .Select(x => x);
            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            var companies = _companyService.AllKpeiCompanies().Select(x => x.Code).ToArray();
            var utcNow = DateTime.UtcNow;
            var queryAkte = Db.DataAkte.Where(x => companies.Contains(x.KodeAk))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .GroupBy(x => x.KodeAk)
                .SelectMany(b =>
                    b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
                .Select(x => x);
            var queryDireksi = Db.DataDireksi;
            var queryKomisaris = Db.DataKomisaris;

            var data = Db.AdministrasiKeanggotaan
                .Where(x => companies.Contains(x.KodeAK))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.UserDraft == null)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .Join(
                    queryAkte,
                    ak => ak.KodeAK,
                    da => da.KodeAk,
                    (ak, da) => new {ak, da})
                .Select(x => new LaporanPengurusPerusahaan
                {
                    KategoriMember = x.ak.KategoriMember,
                    KodeAk = x.ak.KodeAK,
                    ListDireksi = queryDireksi.Where(w => w.AkteId == x.da.Id).Select(s => s.Nama + " (" + s.Jabatan + ")").ToList(),
                    ListKomisaris = queryKomisaris.Where(w => w.AkteId == x.da.Id).Select(s => s.Nama + " (" + s.Jabatan + ")").ToList(),
                }).ToArray();
            ToLocalTime(data);
            return data.AsQueryable();
        }

        public void ToLocalTime(IEnumerable<LaporanPengurusPerusahaan> dats) {
            foreach (var dat in dats) {
                BaseToLocalTime(dat);
            }
        }
    }
}
