﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.Services;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanAkPerTahunDriver : BaseLaporanDriver {
        public LaporanAkPerTahunDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "aknp";
        public override string DataTitle => "Laporan AK & P";

        public override bool FilterByStatusKpei => true;
        public override bool FilterByYear => true;
        public override bool FilterByTipeMember => true;
        public override bool FilterByTipeMemberPartisipan => true;
        
        public override Type DataClassType => typeof(LaporanAknp);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            var q = currentQuery.Cast<LaporanAknp>();
            if (q != null)
            {
                if (!string.IsNullOrWhiteSpace(req.ActiveYear)) {
                    DateTime thn;
                    if (DateTime.TryParseExact(req.ActiveYear, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out thn)) {
                        q = q.Where(x => x.AkActiveUtc != null && x.AkActiveUtc.Value.Year <= thn.Date.Year);
                    }
                }
                currentQuery = q;
            }

            currentQuery = currentQuery.GroupBy(x => x.KodeAk)
                .SelectMany(x => x.OrderByDescending(o => o.AkActiveUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
                .Select(x => x);

            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {

            var companies = _companyService.AllKpeiCompanies().Select(x => x.Code).ToArray();
            var utcNow = DateTime.UtcNow;
            // var data = Db.AdministrasiKeanggotaan
            //     .Where(x => companies.Contains(x.KodeAK))
            //     .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
            //     .Where(x => x.Draft == false)
            //     .Where(x => x.UserDraft == null)
            //     .Where(x => x.ActiveUtc != null)
            //     .Where(x => x.ActiveUtc <= utcNow)
            //     .GroupBy(x => x.KodeAK)
            //     .SelectMany(b =>
            //         b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
            //     .Select(x => x)
            //     .GroupJoin(
            //         Db.DataStatusKPEI
            //             .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
            //             .Where(x => x.Draft == false)
            //             .Where(x => x.ActiveUtc != null)
            //             .Where(x => x.ActiveUtc <= utcNow)
            //             .GroupBy(x => x.KodeAk)
            //             .SelectMany(b =>
            //                 b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
            //             .Select(x => x)
            //             .DefaultIfEmpty(),
            //         ak => ak.KodeAK,
            //         sk => sk.KodeAk,
            //         (ak, sk) => new {ak, sk}).SelectMany(
            //         res => res.sk.DefaultIfEmpty(),
            //         (res, sk) => new LaporanAknp
            //         {
            //             KategoriMember = res.ak.KategoriMember,
            //             KodeAk = res.ak.KodeAK,
            //             TipePartisipan = res.ak.TipeMemberPartisipan,
            //             NamaABSponsor = res.ak.NamaABSponsor,
            //             ActiveUtc = res.ak.ActiveUtc,
            //             CreatedUtc = res.ak.CreatedUtc,
            //             Status = res.ak.Status,
            //             StatusKpei = sk.StatusKPEI,
            //             AkActiveUtc = sk.TanggalAktifUtc
            //         }).ToArray();
            var queryAK = Db.AdministrasiKeanggotaan
                .Where(x => companies.Contains(x.KodeAK))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.UserDraft == null)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow);
            var queryStatusKpei = Db.DataStatusKPEI
                .Where(x => companies.Contains(x.KodeAk))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                // .Where(x => x.TanggalAktifUtc != null)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .OrderByDescending(x => x.TanggalAktifUtc);

            var left = queryAK
                .GroupJoin(
                    queryStatusKpei.DefaultIfEmpty(),
                    ak => ak.Id,
                    sk => sk.AdministrasiKeanggotaanId,
                    (ak, sk) => new {ak, sk})
                .SelectMany(
                    res => res.sk.DefaultIfEmpty(),
                    (res, sk) => new LaporanAknp
                    {
                        KategoriMember = res.ak.KategoriMember,
                        KodeAk = res.ak.KodeAK,
                        TipePartisipan = res.ak.TipeMemberPartisipan,
                        NamaABSponsor = res.ak.NamaABSponsor,
                        StatusKpei = sk.StatusKPEI,
                        ActiveUtc = sk.CreatedUtc,
                        AkActiveUtc = sk.TanggalAktifUtc,
                    });
            var data = left.ToArray();

            ToLocalTime(data);

            return data.AsQueryable();
        }

        public void ToLocalTime(IEnumerable<LaporanAknp> dats) {
            foreach (var dat in dats) {
                BaseToLocalTime(dat);
            }
        }

    }
}
