﻿using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class BaseLaporanDttRequestForm : DttRequestForm, IBindableClass {

        public string ClassTypeFullName => GetType().FullName;

        public string KodeAk { get; set; }
        public string AkActiveYear { get; set; }
        public string ActiveYear { get; set; }
        public string CreatedYear { get; set; }
        public string NamaPerusahaan { get; set; }

        public string StatusBursaKPEI { get; set; }
        public string StatusKpei { get; set; }
        public string StatusBursa { get; set; }
        public string TipeMember { get; set; }
        public string KategoriMember { get; set; }

        public string BankPembayaran { get; set; }
        
        public DateTime? ActiveFromDateTime { get; set; }
        public DateTime? ActiveToDateTime { get; set; }
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }

        public bool PPE { get; set; }
        public bool PEE { get; set; }
        public bool PED { get; set; }
        public bool MI { get; set; }
        private string _jenisUsaha;
        private BaseFilter filterJenisUsaha = new FilterJenisUsaha();
        private BaseFilter filterJenisMember = new FilterJenisMember();
        public string JenisUsaha {
            get {
                return _jenisUsaha ?? filterJenisUsaha.All().First().Name;
            }
            set {
                _jenisUsaha = value;
                if (!string.IsNullOrWhiteSpace(_jenisUsaha)) {
                    var filters = filterJenisUsaha.All().ToArray();
                    PPE = PEE = MI = false;
                    if (_jenisUsaha.Contains("PPE")) {
                        PPE = true;
                    }
                    if (_jenisUsaha.Contains("PEE")) {
                        PEE = true;
                    }
                    if (_jenisUsaha.Contains("MI")) {
                        MI = true;
                    }
                    if (_jenisUsaha.Contains("PED")) {
                        PED = true;
                    }
                }
            }
        }
        
        public bool KBOS { get; set; }
        public bool EBU { get; set; }
        public bool PME { get; set; }
        public bool AK { get; set; }
        private string _keanggotaanLain;
        private BaseFilter filterKeanggotaanLain = new FilterKeanggotaanLain();
        public string KeanggotaanLain {
            get {
                return _keanggotaanLain;
            }
            set {
                _keanggotaanLain = value;
                KBOS = EBU = PME = false;
                if (_keanggotaanLain != null) { 
                    if (_keanggotaanLain.Contains("KBOS")) {
                        KBOS = true;
                    }
                    if (_keanggotaanLain.Contains("EBU")) {
                        EBU = true;
                    }
                    if (_keanggotaanLain.Contains("PME")) {
                        PME = true;
                    }
                }
            }
        }

        private string _perjanjian;
        private BaseFilter filterPerjanjian = new FilterJenisPerjanjian();
        public string Perjanjian {
            get {
                return _perjanjian ?? filterPerjanjian.All().First().Name;
            }
            set {
                _perjanjian = value;
                KBOS = EBU = PME = AK = false;
                if (_perjanjian.Contains("KBOS")) {
                    KBOS = true;
                }
                if (_perjanjian.Contains("EBU")) {
                    EBU = true;
                }
                if (_perjanjian.Contains("PME")) {
                    PME = true;
                }
                if (_perjanjian.Contains("AK")) {
                    AK = true;
                }
            }
        }

        public string StatusPerusahaan { get; set; }

        private string _jenisMember;
        public string JenisMember {
            get {
                return _jenisMember ?? filterJenisMember.All().First().Name;
            }
            set {
                _jenisMember = value;
            }
        }

        public string MemberPartisipan { get; set; }
    }
}
