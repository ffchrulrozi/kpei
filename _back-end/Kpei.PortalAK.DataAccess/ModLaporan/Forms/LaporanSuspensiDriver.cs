﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanSuspensiDriver : BaseLaporanDriver {
        public LaporanSuspensiDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "laporan-suspensi";

        public override string DataTitle => "Laporan Suspensi Bursa & KPEI";

        public override bool FilterByTipeMemberPartisipan => false;
        public override bool FilterByStatusKpei => false;
        public override bool FilterByTipeMember => false;
        public override bool FilterByYear => true;

        public override Type DataClassType => typeof(LaporanSuspensi);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            var data = Db.Database.SqlQuery<LaporanSuspensi>($@"select * from V_AK_Suspensi").ToArray();
            ToLocalTime(data);
            return data.AsQueryable();
        }

        public void ToLocalTime(IEnumerable<LaporanSuspensi> dats) {
            foreach (var dat in dats) {
                BaseToLocalTime(dat);

                dat.TglSuspendBursa = dat.TglSuspendBursa?.AddHours(7);
                dat.TglAktifKembaliBursa = dat.TglAktifKembaliBursa?.AddHours(7);
                dat.TglSuspendKpei = dat.TglSuspendKpei?.AddHours(7);
                dat.TglAktifKembaliKpei = dat.TglAktifKembaliKpei?.AddHours(7);

            }
        }
    }
}
