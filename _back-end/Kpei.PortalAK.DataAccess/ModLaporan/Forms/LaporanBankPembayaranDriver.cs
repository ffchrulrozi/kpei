﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;
using System.Linq.Dynamic;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanBankPembayaranDriver : BaseLaporanDriver {
        public LaporanBankPembayaranDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "bank-pembayaran";

        public override string DataTitle => "Laporan Bank Pembayaran";

        public override bool FilterByStatusKpei => false;
        public override bool FilterByTipeMemberPartisipan => true;

        public override bool FilterByTipeMember => false;

        public override bool FilterByYear => true;
        public override bool HideFromMenu => true;

        public override Type DataClassType => typeof(LaporanBankPembayaran);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            if (!string.IsNullOrWhiteSpace(req.BankPembayaran)) {
                var q = currentQuery.Cast<LaporanBankPembayaran>();
                if (q != null) {
                    currentQuery = q.Where($"{nameof(LaporanBankPembayaran.BankPembayaran)} == @0", req.BankPembayaran);
                }
            }

            currentQuery = from x in currentQuery
                           group x by new { x.Id, x.KodeAk, x.ActiveUtc, x.AkActiveUtc} into y
                           select y.OrderByDescending(t => t.AkActiveUtc).FirstOrDefault();
            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal()
        {
            var companies = _companyService.AllKpeiCompanies().Select(x => x.Code).ToArray();
            var utcNow = DateTime.UtcNow;
            var data = Db.DataBankPembayaran
                .Where(x => companies.Contains(x.KodeAk))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .GroupBy(x => x.KodeAk)
                .SelectMany(b => 
                    b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
                .Select(b => new LaporanBankPembayaran
                {
                    BankPembayaran = b.BankPembayaran,
                    KodeAk = b.KodeAk
                }).ToArray();
            ToLocalTime(data);
            return data.AsQueryable();
        }

        public void ToLocalTime(IEnumerable<LaporanBankPembayaran> dats ) {
            foreach (var dat in dats) {
                base.BaseToLocalTime(dat);
            }
        }
    }
}
