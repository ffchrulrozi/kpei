﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.Services;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanTipeMemberDriver : BaseLaporanDriver {
        public LaporanTipeMemberDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "tipe-member";

        public override string DataTitle => "Laporan Tipe Member";

        public override bool FilterByStatusKpei => true;

        public override bool FilterByTipeMemberPartisipan => false;
        public override bool FilterByTipeMember => false;
        public override bool FilterByYear => true;
        public override Type DataClassType => typeof(LaporanTipeMember);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            if (currentQuery != null) {
                currentQuery = from x in currentQuery
                               group x by new { x.Id, x.KodeAk, x.ActiveUtc, x.AkActiveUtc } into y
                               select y.OrderByDescending(t => t.AkActiveUtc).FirstOrDefault();

                if (!string.IsNullOrEmpty(req.JenisMember)) {
                    var q = currentQuery.Cast<LaporanTipeMember>();
                    if (q != null) {
                        q = q.Where(e => e.TipeMember == req.JenisMember);
                    }
                    currentQuery = q;
                }
            }

            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            var sql = $@"
select * from V_AK_Tipe_Member
";
            var data = Db.Database.SqlQuery<LaporanTipeMember>(sql).ToArray();
            return data.AsQueryable();
        }
    }
}
