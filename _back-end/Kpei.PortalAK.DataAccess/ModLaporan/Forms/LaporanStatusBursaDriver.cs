﻿using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanStatusBursaDriver : BaseLaporanDriver {
        public LaporanStatusBursaDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "status-bursa-kpei";

        public override string DataTitle => "Laporan Status Bursa & KPEI";

        public override bool FilterByTipeMember => false;
        public override bool FilterByStatusKpei => true;
        public override bool FilterByTipeMemberPartisipan => false;

        public override bool FilterByYear => true;

        public override Type DataClassType => typeof(BaseLaporan);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            if (!string.IsNullOrWhiteSpace(req.StatusBursa)) {
                if (req.StatusBursa == StatusBursa.ACTIVE_STATUS.Name) {
                    currentQuery = currentQuery.Where(e => e.StatusBursa.Equals(StatusBursa.ACTIVE_STATUS_ID.Name, StringComparison.CurrentCultureIgnoreCase) || e.StatusBursa.Equals(StatusBursa.ACTIVE_STATUS.Name, StringComparison.CurrentCultureIgnoreCase));
                } else {
                    currentQuery = currentQuery.Where($"{nameof(BaseLaporan.StatusBursa)} == @0", req.StatusBursa);
                }
            }
            currentQuery = from x in currentQuery
                           group x by new { x.Id, x.KodeAk, x.ActiveUtc, x.AkActiveUtc } into y
                select y.OrderByDescending(t => t.AbActiveUtc).FirstOrDefault();
            currentQuery = currentQuery.Where(e => e.StatusKpei != StatusKpei.SUSPEND_STATUS.Name);
            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            var companies = _companyService.AllKpeiCompanies().Select(x => x.Code).ToArray();
            var utcNow = DateTime.UtcNow;
            var data = Db.DataStatusBursa
                .Where(x => companies.Contains(x.KodeAk))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .GroupBy(x => x.KodeAk)
                .SelectMany(b => 
                    b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
                .Select(a => new BaseLaporan
                {
                    KodeAk = a.KodeAk,
                    ActiveUtc = a.CreatedUtc,
                    CreatedUtc = a.CreatedUtc,
                    Status = a.Status,
                    StatusBursa = a.StatusBursa
                }).ToArray();
            ToLocalTime(data);
            return data.AsQueryable();
        }

        public void ToLocalTime(IEnumerable<BaseLaporan> dats) {
            foreach (var dat in dats) {
                BaseToLocalTime(dat);
            }
        }
    }
}
