﻿using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public abstract class BaseLaporanDriver {
        protected readonly AppDbContext Db;
        protected readonly ISysLogger _logger;
        protected readonly IKpeiCompanyService _companyService;

        public BaseLaporanDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) {
            Db = db;
            _logger = logger;
            _companyService = companyService;
        }

        public abstract string DataSlug { get; }
        public abstract string DataTitle { get; }

        public abstract bool FilterByStatusKpei { get; }
        public abstract bool FilterByTipeMember { get; }
        public abstract bool FilterByTipeMemberPartisipan { get; }
        public virtual bool HideFromMenu => false;
        public abstract bool FilterByYear { get; }

        public abstract Type DataClassType { get; }
        public virtual Type DttRequestFormType => typeof(BaseLaporanDttRequestForm);

        protected abstract IQueryable<BaseLaporan> BaseQueryInternal();
        protected abstract IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery,
            BaseLaporanDttRequestForm req);


        public virtual QueryPair<BaseLaporan> List(BaseLaporanDttRequestForm req, IQueryable<BaseLaporan> baseQuery = null) {
            var allQ = baseQuery ?? BaseQueryInternal();
            var q = allQ;

            if (allQ.GetType().GetGenericTypeDefinition() == typeof(EnumerableQuery<>)) {
                allQ = XCast(allQ);
            }

            if (q.GetType().GetGenericTypeDefinition() == typeof(EnumerableQuery<>)) {
                q = XCast(q);
            }

            if (!string.IsNullOrWhiteSpace(req.KodeAk)) {
                q = q.Where($"{nameof(BaseLaporan.KodeAk)} == @0", req.KodeAk);
                allQ = allQ.Where($"{nameof(BaseLaporan.KodeAk)} == @0", req.KodeAk);
            }

            if (!string.IsNullOrWhiteSpace(req.KategoriMember)) {
                q = q.Where($"{nameof(BaseLaporan.KategoriMember)} == @0", req.KategoriMember);
                allQ = allQ.Where($"{nameof(BaseLaporan.KategoriMember)} == @0", req.KategoriMember);
                if (!string.IsNullOrWhiteSpace(req.MemberPartisipan) && req.KategoriMember == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name) {

                    var administrasiKeanggotaan = Db.AdministrasiKeanggotaan.AsQueryable();
                    var utcNow = DateTime.UtcNow;
                    administrasiKeanggotaan = administrasiKeanggotaan.Where($"{nameof(AdministrasiKeanggotaan.Status)} == @0", DataAknpRequestStatus.APPROVED_STATUS.Name)
                        .Where($"{nameof(AdministrasiKeanggotaan.Draft)} == @0", false)
                        .Where($"{nameof(AdministrasiKeanggotaan.UserDraft)} == null")
                        .Where($"{nameof(AdministrasiKeanggotaan.ActiveUtc)} != null")
                        .Where($"{nameof(AdministrasiKeanggotaan.ActiveUtc)} <= @0", utcNow);
                    if (!string.IsNullOrWhiteSpace(req.KodeAk)) {
                        administrasiKeanggotaan = administrasiKeanggotaan.Where($"{nameof(AdministrasiKeanggotaan.KodeAK)} == @0", req.KodeAk);
                    }
                    administrasiKeanggotaan = administrasiKeanggotaan.Where(c => c.KategoriMember.Contains(req.KategoriMember));
                    administrasiKeanggotaan = administrasiKeanggotaan.Where($"{nameof(AdministrasiKeanggotaan.TipeMemberPartisipan)} == @0", req.MemberPartisipan);
                    var listAdministrasiKeanggotaan = administrasiKeanggotaan.ToList().Select(x => x.KodeAK).ToArray();
                    q = q.Where(x => listAdministrasiKeanggotaan.Contains(x.KodeAk));
                    allQ = allQ.Where(x => listAdministrasiKeanggotaan.Contains(x.KodeAk));
                }
            }

            var kws = new string[0];
            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                kws = Regex.Split(req.search.value, @"\s+");
            }

            if (!string.IsNullOrWhiteSpace(req.AkActiveYear) /*&& q.All(e => e.ActiveUtc != null)*/) {

                if (q != null) {
                    DateTime thn;
                    DateTime.TryParseExact(req.AkActiveYear, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out thn);

                    var where = new StringBuilder();
                    var pars = new List<object>();

                    where.Append($@" {nameof(BaseLaporan.AkActiveUtc)} < @0 and {nameof(BaseLaporan.AkActiveUtc)} > @1 ");
                    pars.Add(thn.AddYears(1));
                    pars.Add(thn.AddYears(-1));
                    var parsCt = pars.Count();

                    //create where limit for each code

                    var aksCur = q
                        .Where(e => e.ActiveUtc < thn && e.ActiveUtc > thn.AddYears(-1))
                        .Select(e => e.KodeAk)
                        .Distinct()
                        .ToList();

                    var aks = q
                        .Where(e => e.AkActiveUtc < thn.AddYears(-1))
                        .Select(e => e.KodeAk)
                        .Distinct()
                        .ToList();
                    
                    for (int i = 0; i < aks.Count(); i++) {

                        if (!aksCur.Contains(aks[i])) {
                            where.Append(" or ");

                            var lastRec = q
                                .Where(e => e.AkActiveUtc < thn && e.KodeAk == aks[i])?
                                .OrderByDescending(e => e.AkActiveUtc)
                                .ThenByDescending(e => e.ActiveUtc)
                                .FirstOrDefault()?
                                .AkActiveUtc;

                            var lastRec2 = q
                                .Where(e => e.AkActiveUtc < thn && e.KodeAk == aks[i])?
                                .OrderByDescending(e => e.AkActiveUtc)
                                .ThenByDescending(e => e.ActiveUtc)
                                .FirstOrDefault()?
                                .ActiveUtc;

                            if (lastRec != null || lastRec2 != null) {
                                where.Append($" (" +
                                   (lastRec != null ? $"{nameof(BaseLaporan.AkActiveUtc)} = @{parsCt++} and " : "") +
                                    (lastRec2 != null ? $" {nameof(BaseLaporan.ActiveUtc)} = @{parsCt++} and " : "") +
                                    $" {nameof(BaseLaporan.KodeAk)} = @{parsCt++} and " +
                                    $" {nameof(BaseLaporan.AkActiveUtc)} < @{parsCt++}) ");
                                if (lastRec != null) pars.Add((DateTime)lastRec);
                                if (lastRec2 != null) pars.Add((DateTime)lastRec2);
                                pars.Add(aks[i]);
                                pars.Add(thn.AddYears(1));
                            }
                        }
                    }

                    if (pars.Count() > 0)
                        q = q.Where("(" + where.ToString() + ")", pars.ToArray());
                }
            }

            if (!string.IsNullOrWhiteSpace(req.ActiveYear) && DataSlug != "perjanjian") {
                // DateTime thn;
                // if (DateTime.TryParseExact(req.ActiveYear, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out thn)) {
                //     q = q.Where($"{nameof(BaseLaporan.ActiveUtc)} > @0  and {nameof(BaseLaporan.ActiveUtc)} < @1", thn.AddDays(-1).Date, thn.AddYears(1).Date);
                // }


                //DateTime thn;

                //var where = new StringBuilder();
                //var pars = new List<object>();

                //where.Append($@" {nameof(BaseLaporan.ActiveUtc)} < @0 and {nameof(BaseLaporan.ActiveUtc)} > @1 ");
                //pars.Add(thn.AddYears(1));
                //pars.Add(thn.AddYears(-1));

                //create where limit for each code

                //var parsCt = pars.Count();
                //var aksCur = q.Where(e => e.ActiveUtc < thn.AddYears(1) && e.ActiveUtc > thn.AddYears(-1)).Select(e => e.KodeAk).Distinct().ToList();
                //var aks = q.Where(e => e.ActiveUtc < thn.AddYears(-1)).Select(e => e.KodeAk).Distinct().ToList();

                //if (string.IsNullOrWhiteSpace(req.AkActiveYear)) {
                //    for (int i = 0; i < aks.Count(); i++) {
                //        where.Append(" or ");
                //        var lastRec = q
                //            .Where(e => e.ActiveUtc < thn.AddYears(-1) && e.KodeAk == aks[i])?
                //            .OrderByDescending(e => e.ActiveUtc)
                //            .ThenByDescending(e => e.AkActiveUtc)
                //            .FirstOrDefault()?
                //            .AkActiveUtc;
                //        var lastRec2 = q
                //            .Where(e => e.ActiveUtc < thn && e.KodeAk == aks[i])?
                //            .OrderByDescending(e => e.ActiveUtc)
                //            .ThenByDescending(e => e.AkActiveUtc)
                //            .FirstOrDefault()?
                //            .ActiveUtc;

                //        if (lastRec != null || lastRec2 != null) {
                //            where
                //                .Append($" (" 
                //                + (lastRec != null ? $"{nameof(BaseLaporan.AkActiveUtc)} = @{parsCt++} and " : "") 
                //                + (lastRec2 != null ? $" {nameof(BaseLaporan.ActiveUtc)} = @{parsCt++} and " : "") 
                //                + $" {nameof(BaseLaporan.KodeAk)} = @{parsCt++} and " 
                //                + $" {nameof(BaseLaporan.ActiveUtc)} < @{parsCt++}) ");
                //            if (lastRec != null) pars.Add((DateTime)lastRec);
                //            if (lastRec2 != null) pars.Add((DateTime)lastRec2);
                //            pars.Add(aks[i]);
                //            pars.Add(thn.AddYears(1));
                //        }
                //    }
                //}

                //if (pars.Count() > 0)
                //    q = q.Where("(" + where.ToString() + ")", pars.ToArray());
            }

            if (!string.IsNullOrWhiteSpace(req.StatusKpei)) {
                q = q.Where($"{nameof(BaseLaporan.StatusKpei)} = @0", req.StatusKpei);
            }

            if (!string.IsNullOrWhiteSpace(req.CreatedYear)) {
                DateTime thn;
                DateTime.TryParseExact(req.CreatedYear, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out thn);
                q = q.Where($"{nameof(BaseLaporan.CreatedUtc)} < @0", thn.AddYears(1));
            }

            if (req.ActiveFromDateTime != null) {
                var fromUtc = req.ActiveFromDateTime;
                q = q.Where($"{nameof(BaseLaporan.AkActiveUtc)} >= @0", fromUtc);
            }

            if (req.ActiveToDateTime != null) {
                var toUtc = req.ActiveToDateTime;
                q = q.Where($"{nameof(BaseLaporan.AkActiveUtc)} <= @0", toUtc);
            }

            if (req.FromDateTime != null && DataSlug != "perjanjian") {
                var fromUtc = req.FromDateTime;
                q = q.Where($"{nameof(BaseLaporan.ActiveUtc)} >= @0", fromUtc);
            }

            if (req.ToDateTime != null && DataSlug != "perjanjian") {
                var toUtc = req.ToDateTime;
                q = q.Where($"{nameof(BaseLaporan.ActiveUtc)} <= @0", toUtc);
            }


            var testQ = ApplySearchQueryInternal(q, kws);
            if (testQ != null) q = testQ;

            var orderColumn = req.firstOrderColumn?.name;
            var orderDesc = req.order.FirstOrDefault()?.IsDesc ?? false;

            if (!string.IsNullOrWhiteSpace(orderColumn)) {
                testQ = ApplySortQueryInternal(q, orderColumn, orderDesc);
                if (testQ != null) q = testQ;
            } else {
                q = q.OrderBy($"{nameof(BaseLaporan.CreatedUtc)} {(orderDesc ? "desc" : "asc")}");
            }

            testQ = ApplyFilterQueryInternal(q, req);
            if (testQ != null) q = testQ;
            // allQ = allQ.GroupBy(x => x.KodeAk)
            //     .Select(x => x.OrderByDescending(o => o.ActiveUtc).ElementAt(0));
            //
            //
            // q = q.GroupBy(x => x.KodeAk)
            //     .Select(x => x.OrderByDescending(o => o.ActiveUtc).ElementAt(0));

            return new QueryPair<BaseLaporan>(allQ, q);
        }

        protected virtual IQueryable<BaseLaporan> ApplySearchQueryInternal(IQueryable<BaseLaporan> currentQuery,
            string[] keywords) {

            var q = currentQuery;

            var kws = keywords?.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray() ?? new string[0];
            if (kws.Length == 0) return q;

            var stringProps = DataClassType.GetProperties()
                .Where(x => x.CanRead && x.CanWrite && x.PropertyType == typeof(string)).ToArray();
            if (stringProps.Length == 0) return q;

            var where = new StringBuilder();
            var pars = new List<object>();
            var parIdx = 0;

            where.Append("(");
            for (var i = 0; i < stringProps.Length; i++) {
                var prop = stringProps[i];
                if (i > 0) {
                    where.Append(" or ");
                }
                where.Append(" ( ");
                for (var j = 0; j < kws.Length; j++) {
                    var kw = kws[j].ToLowerInvariant();
                    if (j > 0) {
                        where.Append(" and ");
                    }

                    pars.Add(kw);
                    @where.Append(currentQuery.GetType().GetGenericTypeDefinition() == typeof(EnumerableQuery<>)
                        ? $" ({prop.Name} != null and {prop.Name}.ToLowerInvariant().Contains(@{parIdx})) "
                        : $" {prop.Name}.Contains(@{parIdx}) ");

                    parIdx++;
                }
                where.Append(" ) ");
            }
            where.Append(")");

            return q.Where(where.ToString(), pars.ToArray());
        }

        protected IQueryable<BaseLaporan> FilterYear(IQueryable<BaseLaporan> q, BaseLaporanDttRequestForm req) {
            return q;
        }

        protected IQueryable<BaseLaporan> XCast(IQueryable<BaseLaporan> source) {
            var genericCastMethod = typeof(Queryable)
                .GetMethods(BindingFlags.Public | BindingFlags.Static)
                .First(x => x.Name == "Cast" && x.GetGenericArguments().Length == 1 &&
                            x.ReturnType.GetGenericTypeDefinition() == typeof(IQueryable<>));
            var castMethod = genericCastMethod.MakeGenericMethod(DataClassType);
            return castMethod.Invoke(null, new object[] { source }) as IQueryable<BaseLaporan>;
        }

        protected IQueryable<BaseLaporan> XCast(IEnumerable<BaseLaporan> source) {
            var genericCastMethod = typeof(Enumerable)
                .GetMethods(BindingFlags.Public | BindingFlags.Static)
                .First(x => x.Name == "Cast" && x.GetGenericArguments().Length == 1 &&
                            x.ReturnType.GetGenericTypeDefinition() == typeof(IQueryable<>));
            var castMethod = genericCastMethod.MakeGenericMethod(DataClassType);
            return castMethod.Invoke(null, new object[] { source }) as IQueryable<BaseLaporan>;
        }

        protected virtual IQueryable<BaseLaporan> ApplySortQueryInternal(IQueryable<BaseLaporan> currentQuery,
            string columnName, bool isDescending) {
            var colProp = DataClassType.GetProperty(columnName);
            if (colProp != null && colProp.CanRead && colProp.CanWrite) {
                return currentQuery.OrderBy($"{columnName} {(isDescending ? "desc" : "asc")}");
            } else {
                return currentQuery;
            }
        }

        protected IQueryable<BaseLaporan> DataAk() {
            /*get data
                with member type AK
                with Status KPEI all
            */
            var data = Db.Database.SqlQuery<BaseLaporan>($@"
select * from dbo.V_AK
").ToArray().AsQueryable();
            return data;
        }

        protected IQueryable<BaseLaporan> LatestDataAk(IQueryable<BaseLaporan> q = null, BaseLaporanDttRequestForm req = null) {
            var filter = q ?? DataAk();
            q = q ?? DataAk();
            if (req != null && req.AkActiveYear != null) {
                DateTime thn;
                DateTime.TryParseExact(req.CreatedYear, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out thn);
                filter = filter.Where($"{nameof(BaseLaporan.AkActiveUtc)} < @0", thn.AddYears(1));
            }
            var latest = filter.GroupBy(e => e.KodeAk).Select(e => new { KodeAk = e.Key, AkUpdatedUtc = e.Max(row => row.AkUpdatedUtc) }).ToArray();
            q = q.Join(latest, x => new { x.KodeAk, x.AkUpdatedUtc }, y => new { y.KodeAk, y.AkUpdatedUtc }, (x, y) => x);
            return q;
        }

        protected IQueryable<BaseLaporan> NotSuspendedData(IQueryable<BaseLaporan> q = null, BaseLaporanDttRequestForm req = null) {
            q = q ?? LatestDataAk();
            var filtered = q.Where(e =>
                !e.StatusBursa.ToLower().Contains(StatusBursa.CABUT_STATUS.Name.ToLower()) &&
                !e.StatusKpei.ToLower().Contains(StatusKpei.CABUT_STATUS.Name.ToLower()));
            if (req?.StatusKpei != null) {
                filtered = filtered.Where(e => e.StatusKpei == req.StatusKpei);
            }
            return filtered;
        }

        protected IQueryable<BaseLaporan> SuspendedData(IQueryable<BaseLaporan> q = null) {
            var filtered = q ?? LatestDataAk();
            return filtered.Where(e =>
                e.StatusBursa.ToLower().Contains(StatusBursa.CABUT_STATUS.Name.ToLower()) &&
                e.StatusKpei.ToLower().Contains(StatusKpei.CABUT_STATUS.Name.ToLower()));
        }

        protected IQueryable<BaseLaporan> DataPartisipan() {
            var data = Db.Database.SqlQuery<BaseLaporan>($@"
select 
a4.{nameof(DataTipeMember.KodeAk)},
a4.{nameof(DataTipeMember.KategoriMember)},
a3.{nameof(DataStatusKPEI.StatusKPEI)},
a6.{nameof(DataStatusBursa.StatusBursa)},
a4.{nameof(DataTipeMember.Status)},
a3.{nameof(DataStatusKPEI.ActiveUtc)} as {nameof(BaseLaporan.AkActiveUtc)},
a3.{nameof(DataStatusKPEI.UpdatedUtc)} as {nameof(BaseLaporan.AkUpdatedUtc)},
a3.{nameof(DataStatusKPEI.ActiveUtc)} as {nameof(BaseLaporan.ActiveUtc)},
a6.{nameof(DataStatusBursa.ActiveUtc)} as {nameof(BaseLaporan.AbActiveUtc)},
a6.{nameof(DataStatusBursa.UpdatedUtc)} as {nameof(BaseLaporan.AbUpdatedUtc)}
from dbo.{nameof(DataTipeMember)}s as a4
inner join (
	select
	{nameof(DataStatusKPEI.KodeAk)},
	{nameof(DataStatusKPEI.UpdatedUtc)},
	{nameof(DataStatusKPEI.StatusKPEI)},
    {nameof(DataStatusKPEI.ActiveUtc)}
	from dbo.{nameof(DataStatusKPEI)}s
	where {nameof(DataStatusKPEI.Status)} = '{DataAknpRequestStatus.APPROVED_STATUS.Name}'
) as a3 on a3.{nameof(DataStatusKPEI.KodeAk)} = a4.{nameof(DataTipeMember.KodeAk)}
inner join (
	select
	    {nameof(DataTipeMember.KodeAk)},
	    Max({nameof(DataTipeMember.UpdatedUtc)}) as {nameof(DataTipeMember.UpdatedUtc)}
	from dbo.{nameof(DataTipeMember)}s
	where {nameof(DataTipeMember.Status)} = '{DataAknpRequestStatus.APPROVED_STATUS.Name}'
	group by {nameof(DataTipeMember.KodeAk)}
) as a5 on a5.{nameof(DataTipeMember.UpdatedUtc)} = a4.{nameof(DataTipeMember.UpdatedUtc)}
inner join (
	select
	a2.{nameof(DataStatusBursa.KodeAk)},
	a2.{nameof(DataStatusBursa.UpdatedUtc)},
	a2.{nameof(DataStatusBursa.StatusBursa)},
    a2.{nameof(DataStatusBursa.ActiveUtc)}
	from dbo.{nameof(DataStatusBursa)}s as a2
	where {nameof(DataStatusBursa.Status)} = '{DataAknpRequestStatus.APPROVED_STATUS.Name}'
) as a6 on a6.{nameof(DataStatusBursa.KodeAk)} = a4.{nameof(DataTipeMember.KodeAk)}
where a4.{nameof(DataTipeMember.KategoriMember)} = '{RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name}'
").ToArray().AsQueryable();
            return data;
        }
        
        
        public void BaseToLocalTime(BaseLaporan dat) {
            if (dat.ActiveUtc != null) dat.ActiveUtc = dat.ActiveUtc?.AddHours(7);
            if (dat.AkActiveUtc != null) dat.AkActiveUtc = dat.AkActiveUtc?.AddHours(7);
            if (dat.AbActiveUtc != null) dat.AbActiveUtc = dat.AbActiveUtc?.AddHours(7);
            if (dat.AbUpdatedUtc != null) dat.AbUpdatedUtc = dat.AbUpdatedUtc?.AddHours(7);
            if (dat.AkCreatedUtc != null) dat.AkCreatedUtc = dat.AkCreatedUtc?.AddHours(7);
            if (dat.AkUpdatedUtc != null) dat.AkUpdatedUtc = dat.AkUpdatedUtc?.AddHours(7);
        }
    }
}
