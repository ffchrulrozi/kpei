﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using System.Linq.Dynamic;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Forms {
    public class LaporanKeanggotaanLainDriver : BaseLaporanDriver {
        public LaporanKeanggotaanLainDriver(AppDbContext db, ISysLogger logger, IKpeiCompanyService companyService) : base(db, logger, companyService) { }

        public override string DataSlug => "keanggotaan-lain";

        public override string DataTitle => "Laporan Keanggotaan Lain";

        public override bool FilterByTipeMember => false;

        public override bool FilterByTipeMemberPartisipan => false;
        public override bool FilterByStatusKpei => false;

        public override bool FilterByYear => true;

        public override Type DataClassType => typeof(LaporanKeanggotaanLain);

        protected override IQueryable<BaseLaporan> ApplyFilterQueryInternal(IQueryable<BaseLaporan> currentQuery, BaseLaporanDttRequestForm req) {
            currentQuery = from x in currentQuery
                           group x by new { x.Id, x.KodeAk, x.ActiveUtc, x.AkActiveUtc } into y
                           select y.OrderByDescending(t => t.AkActiveUtc).FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(req.KeanggotaanLain)) {
                var q = currentQuery.Cast<LaporanKeanggotaanLain>();
                if (q != null) {
                    if (req.KBOS) {
                        q = q.Where($"{nameof(LaporanKeanggotaanLain.StatusKBOS)} != @0", "-");
                        q = from x in q
                            group x by new { x.KodeAk, x.KbosActiveUtc } into y
                            select y.OrderByDescending(t => t.KbosActiveUtc).FirstOrDefault();
                    } else if (req.EBU) {
                        q = q.Where($"{nameof(LaporanKeanggotaanLain.StatusEBU)} != @0", "-");
                        q = from x in q
                            group x by new { x.KodeAk, x.EbuActiveUtc } into y
                            select y.OrderByDescending(t => t.EbuActiveUtc).FirstOrDefault();
                    } else if (req.PME) {
                        q = q.Where($"{nameof(LaporanKeanggotaanLain.StatusPMELender)} != @0 || {nameof(LaporanKeanggotaanLain.StatusPMEBorrower)} != @0", "-");
                        q = from x in q
                            group x by new { x.KodeAk, x.PmeActiveUtc } into y
                            select y.OrderByDescending(t => t.PmeActiveUtc).FirstOrDefault();
                    }
                    currentQuery = q;
                }
            }
            return currentQuery;
        }

        protected override IQueryable<BaseLaporan> BaseQueryInternal() {
            var companies = _companyService.AllKpeiCompanies().Select(x => x.Code).ToArray();
            var utcNow = DateTime.UtcNow;
            var queryEBU = Db.DataPerjanjianEBU
                .Where(x => companies.Contains(x.KodeAk))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.UserDraft == null)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .GroupBy(x => x.KodeAk)
                .SelectMany(b =>
                    b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
                .Select(x => x);
            var queryKBOS = Db.DataPerjanjianKBOS
                .Where(x => companies.Contains(x.KodeAk))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.UserDraft == null)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .GroupBy(x => x.KodeAk)
                .SelectMany(b =>
                    b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
                .Select(x => x);
            var queryPME = Db.DataPME
                .Where(x => companies.Contains(x.KodeAk))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.UserDraft == null)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .GroupBy(x => x.KodeAk)
                .SelectMany(b =>
                    b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
                .Select(x => x);

            var data = Db.AdministrasiKeanggotaan
                .Where(x => companies.Contains(x.KodeAK))
                .Where(x => x.Status.ToLower() == DataAknpRequestStatus.APPROVED_STATUS.Name.ToLower())
                .Where(x => x.Draft == false)
                .Where(x => x.UserDraft == null)
                .Where(x => x.ActiveUtc != null)
                .Where(x => x.ActiveUtc <= utcNow)
                .GroupBy(x => x.KodeAK)
                .SelectMany(b =>
                    b.OrderByDescending(o => o.CreatedUtc).ThenByDescending(o => o.ActiveUtc).Take(1))
                .Select(x => new LaporanKeanggotaanLain
                {
                    KodeAk = x.KodeAK,
                    StatusKBOS = queryKBOS.FirstOrDefault(w => w.KodeAk == x.KodeAK).StatusKBOS ?? "-",
                    StatusEBU = queryEBU.FirstOrDefault(w => w.KodeAk == x.KodeAK).StatusPerjanjian ?? "-",
                    StatusPMEBorrower = queryPME.FirstOrDefault(w => w.KodeAk == x.KodeAK).BorrowerStatus ?? "-",
                    StatusPMELender = queryPME.FirstOrDefault(w => w.KodeAk == x.KodeAK).LenderStatus ?? "-",
                }).ToArray();
            ToLocalTime(data);
            return data.AsQueryable();
        }

        public void ToLocalTime(IEnumerable<LaporanKeanggotaanLain> dats) {
            foreach (var dat in dats) {
                BaseToLocalTime(dat);

                dat.EbuActiveUtc = dat.EbuActiveUtc?.AddHours(7);
                dat.KbosActiveUtc = dat.KbosActiveUtc?.AddHours(7);
                dat.PmeActiveUtc = dat.PmeActiveUtc?.AddHours(7);
            }
        }
    }
}
