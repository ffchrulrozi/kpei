﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class FilterKeanggotaanLain : BaseFilter {
        public FilterKeanggotaanLain(string name, string label) : base(name, label) { }
        public FilterKeanggotaanLain() {}

        public override IEnumerable<BaseFilter> All() {
            yield return KBOS;
            yield return PME;
            yield return EBU;
        }

        public static FilterKeanggotaanLain KBOS = new FilterKeanggotaanLain("KBOS", "KBOS");
        public static FilterKeanggotaanLain PME = new FilterKeanggotaanLain("PME", "PME");
        public static FilterKeanggotaanLain EBU = new FilterKeanggotaanLain("EBU", "EBU");

    }
}

