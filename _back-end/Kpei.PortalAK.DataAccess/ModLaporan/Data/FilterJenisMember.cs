﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class FilterJenisMember : BaseFilter {
        public FilterJenisMember() {}
        public FilterJenisMember(string name, string label) : base(name, label){ }

        public static FilterJenisMember GCM = new FilterJenisMember("GCM", "GCM");
        public static FilterJenisMember ICM = new FilterJenisMember("ICM", "ICM");
        public static FilterJenisMember TM = new FilterJenisMember("TM", "TM");
        public static FilterJenisMember Client = new FilterJenisMember("Client", "Client");


        public override IEnumerable<BaseFilter> All() {
            yield return GCM;
            yield return ICM;
            yield return TM;
            yield return Client;
        }
    }
}
