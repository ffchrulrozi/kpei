﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanCabutSpabSpak : BaseLaporan {
        public LaporanCabutSpabSpak() { }
        public LaporanCabutSpabSpak(BaseLaporan dat, LaporanCabutSpabSpak data) {
            base.Init(dat, data);

            StatusSpab = data.StatusSpab;
            TanggalCabutSpab = data.TanggalCabutSpab;
            StatusSpak = data.StatusSpak;
            TanggalCabutSpak = data.TanggalCabutSpak;
            ActiveUtc = TanggalCabutSpab > TanggalCabutSpak ? TanggalCabutSpab : TanggalCabutSpak;
        }
        
        public string StatusSpab { get; set; }

        [Display(Name = "Tanggal Cabut SPAB")]
        [AknpDisplay(null, true, true)]
        public DateTime? TanggalCabutSpab { get; set; }

        public DateTime? SpabActiveUtc { get; set; }
        
        public string StatusSpak { get; set; }

        [Display(Name = "Tanggal Cabut SPAK")]
        [AknpDisplay(null, true, true)]
        public DateTime? TanggalCabutSpak { get; set; }

        public DateTime? SpakActiveUtc { get; set; }
    }
}
