﻿using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class BaseLaporan : IBindableClass {

        public string Id { get; set; }
        public string KodeAk { get; set; }
        public string NamaPerusahaan { get; set; }

        public string Status { get; set; }
        public string StatusBursa { get; set; }
        public string StatusKpei { get; set; }
        public string KategoriMember { get; set; }
        public string MemberPartisipan { get; set; }
        
        //AK User Information
        public DateTime? AkActiveUtc { get; set; }
        public DateTime? AbActiveUtc { get; set; }
        public DateTime? AbUpdatedUtc { get; set; }
        public DateTime? AkCreatedUtc { get; set; }
        public DateTime? AkUpdatedUtc { get; set; }
        
        //Request Information
        [Display(Name = "Tgl Aktif")]
        public DateTime? ActiveUtc { get; set; }
        [Display(Name = "Tgl Submit")]
        public DateTime CreatedUtc { get; set; }

        public string ClassTypeFullName => GetType().FullName;

        public void Init(BaseLaporan dat, BaseLaporan child = null) {
            StatusKpei = dat.StatusKpei;
            KategoriMember = dat.KategoriMember;
            AkActiveUtc = dat.AkActiveUtc;
            AkUpdatedUtc = dat.AkUpdatedUtc;

            if (child != null) {
                KodeAk = child.KodeAk;
                ActiveUtc = child.ActiveUtc;
                CreatedUtc = child.CreatedUtc;
                Status = child.Status;
            }
        }

        //to make activeutc for status bursa
        public void DataBursa() {
            ActiveUtc = AbActiveUtc;
        }
        public void DataKpei() {
            ActiveUtc = AkActiveUtc;
        }
        
    }
}
