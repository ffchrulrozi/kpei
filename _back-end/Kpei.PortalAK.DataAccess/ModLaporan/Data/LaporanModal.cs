﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanModal : BaseLaporan{

        public LaporanModal() {}

        [Display(Name = "Modal Dasar")]
        [AknpDisplay(null, true, true)]
        public decimal ModalDasarRupiah { get; set; }

        [Display(Name = "Modal Disetor")]
        [AknpDisplay(null, true, true)]
        public decimal ModalDisetorRupiah { get; set; }

        public LaporanModal(BaseLaporan dat, LaporanModal data) {
            base.Init(dat, data);
            
            ModalDasarRupiah = data.ModalDasarRupiah;
            ModalDisetorRupiah = data.ModalDisetorRupiah;
        }
    }
}
