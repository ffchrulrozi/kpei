﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanBankPembayaran : BaseLaporan {
        public LaporanBankPembayaran() { }

        public LaporanBankPembayaran(BaseLaporan data, LaporanBankPembayaran dat) {
            base.Init(data, dat);
            BankPembayaran = dat.BankPembayaran;
            CreatedUtc = dat.CreatedUtc;
        }

        [Display(Name = "Bank Pembayaran")]
        [AknpDisplay(null, true, true)]
        public string BankPembayaran { get; set; }
    }
}
