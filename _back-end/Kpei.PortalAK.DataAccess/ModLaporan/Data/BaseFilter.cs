﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public abstract class BaseFilter {
        public string Name;
        public string Label;

        public BaseFilter() {}
        public BaseFilter(string name, string label) {
            Name = name;
            Label = label;
        }

        public abstract IEnumerable<BaseFilter> All();
    }
}
