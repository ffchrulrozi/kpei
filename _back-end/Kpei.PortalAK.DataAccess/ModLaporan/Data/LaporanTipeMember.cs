﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanTipeMember : BaseLaporan {

        [Display(Name = "Tipe Member")]
        [AknpDisplay(null, true, true)]
        public string TipeMember { get; set; }
    }
}
