﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanTerbitSpabSpak : BaseLaporan {
        public LaporanTerbitSpabSpak() { }
        public LaporanTerbitSpabSpak(BaseLaporan dat, LaporanTerbitSpabSpak data) {
            base.Init(dat, data);

            TglSpabUtc = data.TglSpabUtc;
            TglSpakUtc = data.TglSpakUtc;
        }

        [Display(Name = "Tanggal Terbit SPAB")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglSpabUtc { get; set; }

        [Display(Name = "Tanggal Terbit SPAK")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglSpakUtc { get; set; }

    }
}
