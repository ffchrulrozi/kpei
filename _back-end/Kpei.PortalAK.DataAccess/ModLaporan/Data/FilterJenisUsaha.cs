﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class FilterJenisUsaha : BaseFilter {
        public FilterJenisUsaha(string name, string label) : base(name, label) { }

        public FilterJenisUsaha() {}

        public override IEnumerable<BaseFilter> All() {
            yield return PED;
            yield return PPE_ONLY;
            yield return PEE_ONLY;
            yield return MI_ONLY;
            yield return PPE_PEE_ONLY;
            yield return PPE_MI_ONLY;
            yield return PEE_MI_ONLY;
            yield return PPE_PEE_MI_ONLY;
        }

        public static FilterJenisUsaha PED = new FilterJenisUsaha("PED", "PED");
        public static FilterJenisUsaha PPE_ONLY = new FilterJenisUsaha("PPE_ONLY", "PPE");
        public static FilterJenisUsaha PEE_ONLY = new FilterJenisUsaha("PEE_ONLY", "PEE");
        public static FilterJenisUsaha MI_ONLY = new FilterJenisUsaha("MI_ONLY", "MI");
        public static FilterJenisUsaha PPE_PEE_ONLY = new FilterJenisUsaha("PPE_PEE_ONLY", "PPE-PEE");
        public static FilterJenisUsaha PPE_MI_ONLY = new FilterJenisUsaha("PPE_MI_ONLY", "PPE-MI");
        public static FilterJenisUsaha PEE_MI_ONLY = new FilterJenisUsaha("PEE_MI_ONLY", "PEE-MI");
        public static FilterJenisUsaha PPE_PEE_MI_ONLY = new FilterJenisUsaha("PPE_PEE_MI_ONLY", "PPE-PEE-MI");

    }
}
