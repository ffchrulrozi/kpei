﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class FilterJenisPerjanjian : BaseFilter {
        public FilterJenisPerjanjian() { }
        public FilterJenisPerjanjian(string name, string label) : base(name, label) { }

        public override IEnumerable<BaseFilter> All() {
            yield return PerjanjianAK;
            yield return PerjanjianEBU;
            yield return PerjanjianKBOS;
            yield return PerjanjianPME;
        }

        public static FilterJenisPerjanjian PerjanjianAK = new FilterJenisPerjanjian("PerjanjianAK", "Perjanjian AK");
        public static FilterJenisPerjanjian PerjanjianEBU = new FilterJenisPerjanjian("PerjanjianEBU", "Perjanjian EBU");
        public static FilterJenisPerjanjian PerjanjianKBOS = new FilterJenisPerjanjian("PerjanjianKBOS", "Perjanjian KBOS");
        public static FilterJenisPerjanjian PerjanjianPME = new FilterJenisPerjanjian("PerjanjianPME", "Perjanjian PME");

    }
}
