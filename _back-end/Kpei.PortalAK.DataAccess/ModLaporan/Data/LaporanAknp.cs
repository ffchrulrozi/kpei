﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.DbModels;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanAknp : BaseLaporan {
        [Display(Name = "Tanggal Terbit SPAK")]
        [AknpDisplay("_laporanAKNP", true, true)]
        public DateTime? TglSpakUtc { get; set; }

        [Display(Name = "Tipe Partisipan")]
        [AknpDisplay("_laporanAKNP", true, true)]
        public string TipePartisipan { get; set; }

        [Display(Name = "Nama Trading Member")]
        [AknpDisplay("_laporanAKNP", true, true)]
        public string TradingMember { get; set; }

        
        [Display(Name = "Nama PED")]
        [AknpDisplay("_laporanAKNP", true, true)]
        public string NamaPED { get; set; }

        [Display(Name = "Nama AB Sponsor")]
        [AknpDisplay("_laporanAKNP", true, true)]
        public string NamaABSponsor { get; set; }

        [Display(Name = "Nomor SPAK")]
        [AknpDisplay("_laporanAKNP", true, true)]
        public string NomorSpak { get; set; }

        [Display(Name = "Tahun Aktif")]
        [AknpDisplay("_laporanAKNP", true, true)]
        public DateTime? TahunAktif { get; set; }

        public LaporanAknp() { }
        public LaporanAknp(BaseLaporan dat, LaporanAknp data) {
            base.Init(dat, data);
        }
    }
}
