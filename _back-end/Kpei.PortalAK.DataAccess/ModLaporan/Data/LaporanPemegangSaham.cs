﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanPemegangSaham : BaseLaporan {

        [Display(Name = "Nama Pemegang Saham")]
        [AknpDisplay(null, true, true)]
        public string NamaPemegangSaham { get; set; }

        public string TipePartisipan { get; set; }

        [Display(Name = "Lembar Saham")]
        [AknpDisplay(null, true, true)]
        public decimal LembarSaham { get; set; }

        [Display(Name = "Total Saham")]
        [AknpDisplay(null, false, false)]
        public decimal TotalSaham { get; set; }

        [Display(Name = "Persentase Saham")]
        [AknpDisplay(null, true, true)]
        public string Persentase {
            get {
                if (TotalSaham == 0) {
                    return "undefined";
                }
                return ((LembarSaham / TotalSaham) * 100).ToString("0.000") + "%";
            }
        }

        public LaporanPemegangSaham() {}
        public LaporanPemegangSaham(BaseLaporan dat, LaporanPemegangSaham data) {
            base.Init(dat, data);
            NamaPemegangSaham = data.NamaPemegangSaham;
            LembarSaham = data.LembarSaham;
            TotalSaham = data.TotalSaham;
        }

    }
}
