﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanStatusPerusahaan : BaseLaporan {
        public LaporanStatusPerusahaan() { }
        public LaporanStatusPerusahaan(BaseLaporan dat, LaporanStatusPerusahaan data) {
            base.Init(dat, data);

            StatusPerusahaan = data.StatusPerusahaan;
        }

        [Display(Name = "Status Perusahaan")]
        [AknpDisplay(null, true, true)]
        public string StatusPerusahaan { get; set; }
    }
}
