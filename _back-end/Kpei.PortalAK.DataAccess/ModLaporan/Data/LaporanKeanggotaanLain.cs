﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanKeanggotaanLain : BaseLaporan {

        [Display(Name = "Tgl Aktif EBU")]
        // [AknpDisplay(null, true, true)]
        public DateTime? EbuActiveUtc { get; set; }
        
        [Display(Name = "Tgl Aktif KBOS")]
        // [AknpDisplay(null, true, true)]
        public DateTime? KbosActiveUtc { get; set; }

        [Display(Name = "Tgl Aktif PME")]
        // [AknpDisplay(null, true, true)]
        public DateTime? PmeActiveUtc { get; set; }

        [Display(Name = "Status PME (Lender)")]
        [AknpDisplay(null, true, true)]
        public string StatusPMELender { get; set; }

        [Display(Name = "Status PME (Borrower)")]
        [AknpDisplay(null, true, true)]
        public string StatusPMEBorrower { get; set; }
        
        [Display(Name = "Status EBU")]
        [AknpDisplay(null, true, true)]
        public string StatusEBU { get; set; }
        
        [Display(Name = "Status KBOS")]
        [AknpDisplay(null, true, true)]
        public string StatusKBOS { get; set; }

        public LaporanKeanggotaanLain() { }
        public LaporanKeanggotaanLain(BaseLaporan dat, LaporanKeanggotaanLain data) {
            base.Init(dat, data);
            
            StatusKBOS = data.StatusKBOS;
            KbosActiveUtc = data.KbosActiveUtc;
            PmeActiveUtc = data.PmeActiveUtc;
            StatusPMELender = data.StatusPMELender;
            StatusPMEBorrower = data.StatusPMEBorrower;
            EbuActiveUtc = data.EbuActiveUtc;
            StatusEBU = data.StatusEBU;
        }
    }
}
