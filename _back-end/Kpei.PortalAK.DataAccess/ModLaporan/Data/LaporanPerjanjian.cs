﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanPerjanjian : BaseLaporan {
        public LaporanPerjanjian() { }

        public string TipeMemberAK { get; set; }


        [Display(Name = "No Perjanjian EBU")]
        [AknpDisplay(null, true, true)]
        public string NoPerjanjianEbu { get; set; }

        [Display(Name = "No Perjanjian KBOS")]
        [AknpDisplay(null, true, true)]
        public string NoPerjanjianKbos { get; set; }

        [Display(Name = "No Perjanjian AK")]
        [AknpDisplay(null, true, true)]
        public string NoPerjanjianAk { get; set; }

        [Display(Name = "No Perjanjian PME Lender")]
        [AknpDisplay(null, true, true)]
        public string NoPerjanjianPmeLender { get; set; }
        [Display(Name = "No Perjanjian PME Borrower")]
        [AknpDisplay(null, true, true)]
        public string NoPerjanjianPmeBorrower { get; set; }

        [Display(Name = "Tgl Perjanjian Awal EBU")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglPerjanjianAwalEbuUtc { get; set; }
        [Display(Name = "Tgl Perjanjian Akhir EBU")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglPerjanjianAkhirEbuUtc { get; set; }

        [Display(Name = "Tgl Perjanjian KBOS")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglPerjanjianKbosSpmUtc { get; set; }

        [Display(Name = "Tgl Perjanjian Awal PME Lender")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglPerjanjianAwalPmeLenderUtc { get; set; }
        [Display(Name = "Tgl Perjanjian Awal PME Borrower")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglPerjanjianAwalPmeBorrowerUtc { get; set; }
        [Display(Name = "Tgl Perjanjian Akhir PME Lender")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglPerjanjianAkhirPmeLenderUtc { get; set; }
        [Display(Name = "Tgl Perjanjian Akhir PME Borrower")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglPerjanjianAkhirPmeBorrowerUtc { get; set; }

        [Display(Name = "Tgl Perjanjian AK")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglPerjanjianAk { get; set; }

        [Display(Name = "Tgl Perubahan Perjanjian EBU")]
        [AknpDisplay(null, true, true)]
        public DateTime? PerjanjianEbuActiveUtc { get; set; }
        [Display(Name = "Tgl Perubahan Perjanjian KBOS")]
        [AknpDisplay(null, true, true)]
        public DateTime? PerjanjianKbosActiveUtc { get; set; }
        [Display(Name = "Tgl Perubahan Perjanjian AK")]
        [AknpDisplay(null, true, true)]
        public DateTime? PerjanjianAkActiveUtc { get; set; }
        [Display(Name = "Tgl Perubahan Perjanjian PME")]
        [AknpDisplay(null, true, true)]
        public DateTime? PmeActiveUtc { get; set; }
        
        public LaporanPerjanjian(BaseLaporan dat, LaporanPerjanjian data) {
            base.Init(dat, data);

            NoPerjanjianEbu = data.NoPerjanjianEbu;
            NoPerjanjianKbos = data.NoPerjanjianKbos;
            NoPerjanjianPmeLender = data.NoPerjanjianPmeLender;
            NoPerjanjianPmeBorrower = data.NoPerjanjianPmeBorrower;
            NoPerjanjianAk = data.NoPerjanjianAk;

            TglPerjanjianAwalEbuUtc = data.TglPerjanjianAwalEbuUtc;
            TglPerjanjianAkhirEbuUtc = data.TglPerjanjianAkhirEbuUtc;
            TglPerjanjianKbosSpmUtc = data.TglPerjanjianKbosSpmUtc;
            TglPerjanjianAwalPmeLenderUtc = data.TglPerjanjianAkhirPmeLenderUtc;
            TglPerjanjianAkhirPmeLenderUtc = data.TglPerjanjianAkhirPmeLenderUtc;
            TglPerjanjianAwalPmeBorrowerUtc = data.TglPerjanjianAkhirPmeBorrowerUtc;
            TglPerjanjianAkhirPmeBorrowerUtc = data.TglPerjanjianAkhirPmeBorrowerUtc;
            TglPerjanjianAk = data.TglPerjanjianAk;

            PerjanjianAkActiveUtc = data.PerjanjianAkActiveUtc;
            PerjanjianEbuActiveUtc = data.PerjanjianEbuActiveUtc;
            PerjanjianKbosActiveUtc = data.PerjanjianKbosActiveUtc;
            PmeActiveUtc = data.PmeActiveUtc;
        }
    }
}
