﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanPerubahanNama : BaseLaporan {
        public LaporanPerubahanNama() { }

        public DateTime? TempActiveUtc { get; set; }

        [Display(Name = "Nama Sebelumnya")]
        [AknpDisplay("_laporanPerubahanNama", true, true)]
        public string NamaSebelumnya { get; set; }

        [Display(Name = "Tgl Berlaku Sebelumnya")]
        // [AknpDisplay(null, true, true)]
        public DateTime TglNamaBerlakuSebelumnya { get; set; }

        [Display(Name = "Nama Setelahnya")]
        // [AknpDisplay(null, true, true)]
        public string NamaSetelahnya { get; set; }

        [Display(Name = "Tgl Berlaku Setelahnya")]
        // [AknpDisplay(null, true, true)]
        public DateTime TglNamaBerlakuSetelahnya { get; set; }
    }
}
