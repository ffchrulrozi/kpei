﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanPengurusPerusahaan : BaseLaporan {
        public LaporanPengurusPerusahaan() {}
        public LaporanPengurusPerusahaan(BaseLaporan dat, LaporanPengurusPerusahaan data) {
            base.Init(dat, data);

            Direksi = data.Direksi;
            Komisaris = data.Komisaris;
        }

        [AknpDisplay("_laporanPengurusPerusahaan", true, true)]
        [MaxLength(1024)]
        public string Direksi { get; set; }
        public List<string> ListDireksi { get; set; }
        public List<string> ListKomisaris { get; set; }

        [AknpDisplay("_laporanPengurusPerusahaan", true, true)]
        [MaxLength(1024)]
        public string Komisaris { get; set; }
    }
}
