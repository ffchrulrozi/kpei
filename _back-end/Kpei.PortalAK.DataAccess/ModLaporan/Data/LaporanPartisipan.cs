﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanPartisipan : BaseLaporan {
        public LaporanPartisipan() { }

        [AknpDisplay(null, true, false)]
        public string Alamat { get; set; }
        
        [AknpDisplay(null, true, true)]
        public string Modal { get; set; }

        [Display(Name = "Tipe Member")]
        [AknpDisplay(null, true, true)]
        public string TipeMember { get; set; }

        [Display(Name = "Pemegang Saham")]
        [AknpDisplay(null, true, true)]
        [MaxLength(1024)]
        public string PemegangSaham { get; set; }

        [Display(Name = "Pengurus Perusahaan")]
        [AknpDisplay(null, true, true)]
        [MaxLength(1024)]
        public string PengurusPerusahaan { get; set; }

        [Display(Name = "Status Perusahaan")]
        [AknpDisplay(null, true, true)]
        public string StatusPerusahaan { get; set; }

        [Display(Name = "No Perjanjian Partisipan")]
        [AknpDisplay(null, true, true)]
        public string NoPerjanjian { get; set; }

        [Display(Name = "File Surat Persetujuan")]
        [AknpDisplay(null, true, true)]
        public string SuratPersetujuanFileUrl { get; set; }
    }
}
