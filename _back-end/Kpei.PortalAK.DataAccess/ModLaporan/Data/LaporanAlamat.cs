﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanAlamat : BaseLaporan{
        public LaporanAlamat() {}
        

        public LaporanAlamat(BaseLaporan data, LaporanAlamat dat) {
            Init(data, dat);
        }

        public string Gedung { get; set; }
        public string Jalan { get; set; }
        public string Kota { get; set; }
        public string KodePos { get; set; }
        
        [AknpDisplay(null, true, false)]
        public string Alamat {
            get {
                return $"{Gedung}, {Jalan}, {Kota} {KodePos}";
            }
        }
        
        [AknpDisplay(null, true, false)]
        public string Telp { get; set; }
        
        [AknpDisplay(null, true, false)]
        public string Fax { get; set; }
        
        public void Init(BaseLaporan data, LaporanAlamat dat) {
            base.Init(data, dat);

            Gedung = dat.Gedung;
            Jalan = dat.Jalan;
            Kota = dat.Kota;
            KodePos = dat.KodePos;
            Telp = dat.Telp;
            Fax = dat.Telp;
        }
    }
}
