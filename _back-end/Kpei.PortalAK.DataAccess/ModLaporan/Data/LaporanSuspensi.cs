﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanSuspensi : BaseLaporan {
        public LaporanSuspensi() { }
        public LaporanSuspensi(BaseLaporan dat, LaporanSuspensi data) {
            base.Init(dat, data);

            TglSuspendBursa = data.TglSuspendBursa;
            TglSuspendKpei = data.TglSuspendKpei;
            TglAktifKembaliBursa = data.TglAktifKembaliBursa;
            TglAktifKembaliKpei = data.TglAktifKembaliKpei;
        }

        [Display(Name = "Tanggal Suspend (Bursa)")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglSuspendBursa { get; set; }

        [Display(Name = "Tanggal Aktif Kembali (Bursa)")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglAktifKembaliBursa { get; set; }

        [Display(Name = "Tanggal Suspend (KPEI)")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglSuspendKpei { get; set; }

        [Display(Name = "Tanggal Aktif Kembali (KPEI)")]
        [AknpDisplay(null, true, true)]
        public DateTime? TglAktifKembaliKpei { get; set; }
    }
}
