﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModLaporan.Data {
    public class LaporanJenisUsaha : BaseLaporan {
        [Display(Name = "No. Ijin Usaha PPE")]
        [AknpDisplay(null, true, true)]
        public string PPE { get; set; }

        [Display(Name = "No. Ijin Usaha PEE")]
        [AknpDisplay(null, true, true)]
        public string PEE { get; set; }

        [Display(Name = "No. Ijin Usaha MI")]
        [AknpDisplay(null, true, true)]
        public string MI { get; set; }

        [Display(Name = "No. Ijin Usaha PED")]
        [AknpDisplay(null, true, true)]
        public string PED { get; set; }

        public LaporanJenisUsaha() {}
        public LaporanJenisUsaha(BaseLaporan dat, LaporanJenisUsaha data) {
            base.Init(dat, data);

            PPE = data.PPE;
            PEE = data.PEE;
            MI = data.MI;
        }
    }
}
