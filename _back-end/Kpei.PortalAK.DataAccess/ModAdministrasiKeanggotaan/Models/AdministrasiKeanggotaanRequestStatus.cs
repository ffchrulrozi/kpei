﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Models
{
    public class AdministrasiKeanggotaanRequestStatus
    {
        public string Name { get; }
        public string Label { get; }
        private AdministrasiKeanggotaanRequestStatus(string name, string label)
        {
            Name = name;
            Label = label;
        }

        public static IEnumerable<AdministrasiKeanggotaanRequestStatus> All() {
            yield return NEW_STATUS;
            yield return ONGOING_STATUS;
            yield return APPROVED_STATUS;
            yield return REJECTED_STATUS;
        }

        public static readonly AdministrasiKeanggotaanRequestStatus NEW_STATUS = new AdministrasiKeanggotaanRequestStatus("NEW", "NEW");
        public static readonly AdministrasiKeanggotaanRequestStatus ONGOING_STATUS = new AdministrasiKeanggotaanRequestStatus("ONGOING", "ONGOING");
        public static readonly AdministrasiKeanggotaanRequestStatus APPROVED_STATUS = new AdministrasiKeanggotaanRequestStatus("APPROVED", "APPROVED");
        public static readonly AdministrasiKeanggotaanRequestStatus REJECTED_STATUS = new AdministrasiKeanggotaanRequestStatus("REJECTED", "REJECTED");
    }
}
