﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Forms;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Attributes
{
    public class RequiredIfPEDAttribute : ValidationAttribute {
        /// <inheritdoc />
        protected override ValidationResult IsValid(object value, ValidationContext valctx) {
            var form = valctx.ObjectInstance as AdministrasiKeanggotaanAknpForm;

            var successResult = ValidationResult.Success;
            if (form.TipeMemberPartisipan != "PED")
            {
                return successResult;
            }

            var errResult = new ValidationResult($"{valctx.DisplayName} harus diisi.", new[] {valctx.MemberName});
            if (value == null) return errResult;

            var valstr = value as string;
            if (valstr == null) return successResult;

            return string.IsNullOrWhiteSpace(valstr) ? errResult : successResult;

        }
    }
}
