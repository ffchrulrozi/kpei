﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Attributes;

namespace Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Forms
{
    public class TradingMemberForm
    {
        public string Id { get; set; }

        [Display(Name = "Nama")]
        [MaxLength(128)]
        public string Nama { get; set; }

        [Display(Name = "No. Perjanjian dengan Trading Member")]
        public string NoPerjanjian { get; set; }

        [Display(Name = "Tanggal Perjanjian")]
        public DateTime? TanggalPerjanjian { get; set; }

        [Display(Name = "Perjanjian Anggota Kliring Umum (AKU) dengan Trading Member")]
        public string PerjanjianFileUrl { get; set; }

        public string AdministrasiKeanggotaanId { get; set; }
    }
}
