﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Attributes;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.Validators;

namespace Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Forms
{
    public class AdministrasiKeanggotaanAknpForm
    {
        public static string DRIVER_SLUG = "administrasi-keanggotaan";
        public string Id { get; set; }

        [Display(Name = "Kode AK&P")]
        [Required]
        [MaxLength(32)]
        public string KodeAK { get; set; }

        [Display(Name = "Kategori Member")]
        [Required]
        [MaxLength(128)]
        public string KategoriMember { get; set; }

        [Display(Name = "Data Trading Member")]
        [CustomValidation(typeof(AdministrasiKeanggotaanAknpForm), nameof(ValidateListTradingMember))]
        public List<TradingMemberForm> ListTradingMember { get; set; }

        public static ValidationResult ValidateListTradingMember(object value, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as AdministrasiKeanggotaanAknpForm;
            if (form.KategoriMember != RegistrasiKategoriMember.MEMBER_AKU.Name)
            {
                return success;
            }

            var val = (value as IEnumerable<object>)?.ToArray() ?? new object[0];

            foreach (TradingMemberForm item in val)
            {
                item.AdministrasiKeanggotaanId = item.AdministrasiKeanggotaanId ?? "notnull";
                item.Id = item.Id ?? "notnull";
                bool haveNullItem = item.GetType().GetProperties()
                    .Any(p => p.GetValue(item) == null);

                if (haveNullItem) {
                    return new ValidationResult($"Tidak boleh ada item yang kosong dalam {valctx.DisplayName}.");
                }

                var itemValctx = new ValidationContext(item, valctx.ServiceContainer, null);
                var itemValres = new List<ValidationResult>();
                
                if (Validator.TryValidateObject(item, itemValctx, itemValres, true)) continue;
                
                var firstItemValres = itemValres[0];
                return new ValidationResult(
                    $"Ada item yang tidak valid dalam {valctx.DisplayName} ({firstItemValres.ErrorMessage}).",
                    new[] {valctx.MemberName});
            }

            return success;
        }

        [Display(Name = "No. Perjanjian")]
        [RequiredIfAKUAKIPED]
        public string NoPerjanjianDenganKPEI { get; set; }

        [Display(Name = "Tanggal Perjanjian")]
        [RequiredIfAKUAKIPED]
        public DateTime? TanggalPerjanjianDenganKPEI { get; set; }

        [Display(Name = "Dokumen Perjanjian")]
        [RequiredIfAKUAKIPED]
        public string PerjanjianDenganKPEIFileUrl { get; set; }

        [Display(Name = "No. Perjanjian")]
        [RequiredIfAKUAKIPED]
        public string NoPerjanjianPortabilitySponsor { get; set; }

        [Display(Name = "Tanggal Perjanjian")]
        [RequiredIfAKUAKIPED]
        public DateTime? TanggalPerjanjianPortabilitySponsor { get; set; }

        [Display(Name = "Dokumen Perjanjian")]
        [RequiredIfAKUAKIPED]
        public string PerjanjianPortabilitySponsorFileUrl { get; set; }

        //partisipan
        [RequiredIfPED]
        [Display(Name = "File Ijin Usaha")]
        public string DokumenKhususIjinUsahaFileUrl { get; set; }

        [RequiredIfPartisipan]
        public string TipeMemberPartisipan { get; set; }

        [RequiredIfPED]
        [Display(Name = "Nomor Ijin Usaha Pengusaha Efek Daerah")]
        [MaxLength(128)]
        public string NoIjinUsahaPED { get; set; }

        [RequiredIfPED]
        [MaxLength(128)]
        [Display(Name = "Nama AB Sponsor")]
        public string NamaABSponsor { get; set; }

        
        [MaxLength(128)]
        public string Approver1UserId { get; set; }

        [MaxLength(128)]
        public string Approver2UserId { get; set; }

        
        [MaxLength(128)]
        public string Status { get; set; }

        private List<DataLayananBaruForm> _formLayananBaruAku;

        [Display(Name = "Form Member AKU", GroupName = "Member", Order = 11)]
        [CustomValidation(typeof(AdministrasiKeanggotaanAknpForm), nameof(ValidateTipeMemberAku))]
        public List<DataLayananBaruForm> FormLayananBaruAku
        {
            get
            {
                _formLayananBaruAku = _formLayananBaruAku == null
                    ? new List<DataLayananBaruForm>()
                    : _formLayananBaruAku = _formLayananBaruAku
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.FieldName)).ToList();
                return _formLayananBaruAku;
            }
            set { _formLayananBaruAku = value; }
        }

        public static ValidationResult ValidateTipeMemberAku(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as AdministrasiKeanggotaanAknpForm;
            if (form == null) return success;
            if (form.KategoriMember == RegistrasiKategoriMember.MEMBER_AKU.Name) {
                foreach(var d in form.FormLayananBaruAku)
                {
                    if (d.FieldRequired == true && string.IsNullOrWhiteSpace(d.FieldValue))
                    {
                        return new ValidationResult(d.FieldName+" wajib diisi");
                    }
                }
            }

            return success;
        }

        private List<DataLayananBaruForm> _formLayananBaruAki;

        [Display(Name = "Form Member AKI", GroupName = "Member", Order = 11)]
        [CustomValidation(typeof(AdministrasiKeanggotaanAknpForm), nameof(ValidateTipeMemberAki))]
        public List<DataLayananBaruForm> FormLayananBaruAki
        {
            get
            {
                _formLayananBaruAki = _formLayananBaruAki == null
                    ? new List<DataLayananBaruForm>()
                    : _formLayananBaruAki = _formLayananBaruAki
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.FieldName)).ToList();
                return _formLayananBaruAki;
            }
            set { _formLayananBaruAki = value; }
        }

        public static ValidationResult ValidateTipeMemberAki(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as AdministrasiKeanggotaanAknpForm;
            if (form == null) return success;
            if (form.KategoriMember == RegistrasiKategoriMember.MEMBER_AKI.Name) {
                foreach(var d in form.FormLayananBaruAki)
                {
                    if (d.FieldRequired == true && string.IsNullOrWhiteSpace(d.FieldValue))
                    {
                        return new ValidationResult(d.FieldName+" wajib diisi");
                    }
                }
            }
            return success;
        }

        private List<DataLayananBaruForm> _formGroupPartisipan;

        [Display(Name = "Form Member Partitipan", GroupName = "Member", Order = 11)]
        [CustomValidation(typeof(AdministrasiKeanggotaanAknpForm), nameof(ValidateGroupMemberPartisipan))]
        public List<DataLayananBaruForm> FormGroupPartisipan {
            get
            {
                _formGroupPartisipan = _formGroupPartisipan == null
                    ? new List<DataLayananBaruForm>()
                    : _formGroupPartisipan = _formGroupPartisipan
                        .Where(x => x != null && !string.IsNullOrWhiteSpace(x.FieldName)).ToList();

                return _formGroupPartisipan;
            } 
            set
            {
                _formGroupPartisipan = value;
            } 
        }

        public static ValidationResult ValidateGroupMemberPartisipan(object val, ValidationContext valctx)
        {
            var success = ValidationResult.Success;
            var form = valctx.ObjectInstance as AdministrasiKeanggotaanAknpForm;
            if (form == null) return success;

            if (form.KategoriMember == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name) {
                foreach(var d in form.FormGroupPartisipan)
                {
                    if (d.FieldRequired == true && string.IsNullOrWhiteSpace(d.FieldValue))
                    {
                        return new ValidationResult(d.FieldName+" wajib diisi");
                    }
                }
            }
            return success;
        }
        public bool Draft { get; set; }
        public string UserDraft { get; set; }

        public void Init(AdministrasiKeanggotaan data)
        {
            Id = data.Id;
            Draft = data.Draft;
            DokumenKhususIjinUsahaFileUrl = data.DokumenKhususIjinUsahaFileUrl;
            KategoriMember = data.KategoriMember;
            KodeAK = data.KodeAK;
            NamaABSponsor = data.NamaABSponsor;
            NoIjinUsahaPED = data.NoIjinUsahaPED;
            NoPerjanjianDenganKPEI = data.NoPerjanjianDenganKPEI;
            NoPerjanjianPortabilitySponsor = data.NoPerjanjianPortabilitySponsor;
            PerjanjianDenganKPEIFileUrl = data.PerjanjianDenganKPEIFileUrl;
            PerjanjianPortabilitySponsorFileUrl = data.PerjanjianPortabilitySponsorFileUrl;
            TanggalPerjanjianDenganKPEI = data.TanggalPerjanjianDenganKPEIUtc;
            TanggalPerjanjianPortabilitySponsor = data.TanggalPerjanjianPortabilitySponsor;
            TipeMemberPartisipan = data.TipeMemberPartisipan;
            if (data.TradingMembers != null)
            {
                ListTradingMember = data.TradingMembers.Select(x =>new TradingMemberForm
                {
                    Id = x.Id,
                    Nama = x.Nama,
                    AdministrasiKeanggotaanId = x.AdministrasiKeanggotaanId,
                    NoPerjanjian = x.NoPerjanjian,
                    PerjanjianFileUrl = x.PerjanjianFileUrl,
                    TanggalPerjanjian = x.TanggalPerjanjian
                }).ToList();   
            }

            if (data.LayananBaru != null)
            {
                if (data.KategoriMember == RegistrasiKategoriMember.MEMBER_AKU.Name)
                {
                    FormLayananBaruAku = data.LayananBaru.Select(x => this.InitLayananBaru(x)).ToList();
                }
                if (data.KategoriMember == RegistrasiKategoriMember.MEMBER_AKI.Name)
                {
                    FormLayananBaruAki = data.LayananBaru.Select(x => this.InitLayananBaru(x)).ToList();
                }
                if (data.KategoriMember == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name)
                {
                    FormGroupPartisipan = data.LayananBaru.Select(x => this.InitLayananBaru(x)).ToList();
                }    
            }
        }

        private DataLayananBaruForm InitLayananBaru(AdministrasiKeanggotaanLayananBaru data)
        {
            return new DataLayananBaruForm
            {
                FieldName = data.FieldName,
                FieldType = data.FieldType,
                FieldValue = data.FieldValue,
                Id = data.Id,
                LayananBaruId = data.LayananBaruId,
                TipeMemberAK = data.TipeMemberAK,
                TipeMemberPartisipan = data.TipeMemberPartisipan
            };
        }
    }
}
