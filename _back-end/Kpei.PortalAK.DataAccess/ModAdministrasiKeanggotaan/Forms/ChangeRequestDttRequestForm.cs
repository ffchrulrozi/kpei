﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;

namespace Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Forms
{
    public class ChangeRequestDttRequestForm : DttRequestForm
    {
        public string KodeAK { get; set; }
        public string DataSlug { get; set; }
        public string Status { get; set; }
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }
        public DateTime? ActiveFromDateTime { get; set; }
        public DateTime? ActiveToDateTime { get; set; }
    }
}
