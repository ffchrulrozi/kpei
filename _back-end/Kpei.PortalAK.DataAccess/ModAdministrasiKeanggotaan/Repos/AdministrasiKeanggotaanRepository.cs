﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.ModSettings.Models;

namespace Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Repos
{
    public class AdministrasiKeanggotaanRepository : IAdministrasiKeanggotaanRepository
    {
        private readonly AppDbContext _context;
        public AdministrasiKeanggotaanRepository(AppDbContext _context)
        {
            this._context = _context;
        }

        public IEnumerable<AdministrasiKeanggotaan> AdministrasiKeanggotaans => new List<AdministrasiKeanggotaan>();
        public AdministrasiKeanggotaan Get(string id) => _context.AdministrasiKeanggotaan.FirstOrDefault(x => x.Id == id);
        public DataRegistrasi GetLatestActiveAknpData(string kodeAk)
        {
            var q = _context.DataRegistrasi.AsQueryable();
            var utcNow = DateTime.UtcNow;
            q = q.Where($"{nameof(BaseAknpData.Status)} == @0", DataAknpRequestStatus.APPROVED_STATUS.Name)
                .Where($"{nameof(BaseAknpData.KodeAk)} == @0", kodeAk)
                .Where($"{nameof(BaseAknpData.Draft)} == @0", false)
                .Where($"{nameof(BaseAknpData.ActiveUtc)} != null")
                .Where($"{nameof(BaseAknpData.ActiveUtc)} <= @0", utcNow)
                .OrderBy($"{nameof(BaseAknpData.CreatedUtc)} desc, {nameof(BaseAknpData.ActiveUtc)} desc");
            var data = q.FirstOrDefault();
            return data;
        }

        public AdministrasiKeanggotaan GetNewOngoingData(string kodeAk, string kategoriMember, string tipeMemberPartisipan)
        {
            var query = _context.AdministrasiKeanggotaan
                .FirstOrDefault(x => x.KodeAK == kodeAk
                                     && (x.Status == DataAknpRequestStatus.NEW_STATUS.Name
                                        || x.Status == DataAknpRequestStatus.ONGOING_STATUS.Name)
                                     && x.KategoriMember == kategoriMember
                                     && x.Draft == false
                                     && x.UserDraft == null
                                     && x.TipeMemberPartisipan == tipeMemberPartisipan);
            //var data = query.FirstOrDefault();
            return query;
        }

        public Tuple<AdministrasiKeanggotaan, DataChangeRequest> CreateChangeRequest(AdministrasiKeanggotaanAknpForm data)
        {
            var cr = new DataChangeRequest();

            AdministrasiKeanggotaan newData = Create(data);
            cr.KodeAk = newData.KodeAK;
            cr.Status = newData.Status;
            cr.DataDriverSlug = AdministrasiKeanggotaanAknpForm.DRIVER_SLUG;
            cr.DataId = newData.Id;
            cr.CreatedUtc = newData.CreatedUtc;
            cr.UpdatedUtc = newData.UpdatedUtc;

            _context.DataChangeRequests.Add(cr);
            _context.SaveChanges();

            return Tuple.Create(newData, cr);
        }

        public Tuple<AdministrasiKeanggotaan, DataChangeRequest> CreateDraftChangeRequest(AdministrasiKeanggotaanAknpForm data)
        {
            var cr = new DataChangeRequest();
            var newData = Update(data);

            cr.KodeAk = newData.KodeAK;
            cr.Status = newData.Status;
            cr.DataDriverSlug = AdministrasiKeanggotaanAknpForm.DRIVER_SLUG;
            cr.DataId = newData.Id;
            cr.CreatedUtc = newData.CreatedUtc;
            cr.UpdatedUtc = newData.UpdatedUtc;

            _context.DataChangeRequests.Add(cr);
            _context.SaveChanges();

            return Tuple.Create(newData, cr);
        }

        public AdministrasiKeanggotaan Approve(string dataId, string userId)
        {
            AdministrasiKeanggotaan data = Get(dataId);

            if (data.Status == DataAknpRequestStatus.NEW_STATUS.Name)
            {
                data.UpdatedUtc = DateTime.UtcNow;
                data.Status = DataAknpRequestStatus.ONGOING_STATUS.Name;
                data.Approver1UserId = userId;
                _context.SaveChanges();
            }else if (data.Status == DataAknpRequestStatus.ONGOING_STATUS.Name
                      && userId != data.Approver1UserId)
            {
                data.UpdatedUtc = DateTime.UtcNow;
                data.ActiveUtc = DateTime.UtcNow;
                data.Status = DataAknpRequestStatus.APPROVED_STATUS.Name;
                data.Approver2UserId = userId;

                AdministrasiKeanggotaan existing =
                    GetLatestActiveData(data.KodeAK, data.KategoriMember, data.TipeMemberPartisipan);
                if (!string.IsNullOrWhiteSpace(existing.KodeAK))
                {
                    var statusKpei = GetStatusKpeiActiveData(existing.Id);
                    if (statusKpei != null && statusKpei.StatusKPEI != StatusKpei.CABUT_STATUS.Name)
                    {
                        var s = _context.DataStatusKPEI.FirstOrDefault(x => x.Id == statusKpei.Id);
                        s.AdministrasiKeanggotaanId = data.Id;
                    }
                    existing.Draft = true;
                }

                _context.SaveChanges();
            }

            var cr = _context.DataChangeRequests.FirstOrDefault(x => x.DataDriverSlug == AdministrasiKeanggotaanAknpForm.DRIVER_SLUG && x.DataId == data.Id);
            if (cr != null) {
                cr.UpdatedUtc = data.UpdatedUtc;
                cr.Status = data.Status;
                cr.KodeAk = data.KodeAK;
                _context.SaveChanges();
            }

            return data;
        }

        public AdministrasiKeanggotaan Reject(string id, string rejectReason)
        {
            AdministrasiKeanggotaan data = Get(id);
            data.UpdatedUtc = DateTime.UtcNow;
            data.Status = DataAknpRequestStatus.REJECTED_STATUS.Name;
            data.RejectReason = rejectReason;
            _context.SaveChanges();

            var cr = _context.DataChangeRequests.FirstOrDefault(x => x.DataDriverSlug == AdministrasiKeanggotaanAknpForm.DRIVER_SLUG && x.DataId == data.Id);
            if (cr != null) {
                cr.UpdatedUtc = data.UpdatedUtc;
                cr.Status = data.Status;
                cr.KodeAk = data.KodeAK;
                _context.SaveChanges();
            }
            return data;
        }

        public List<AdministrasiKeanggotaan> GetAllLatestActiveData(string kodeAk, string kategoriMember, string tipeMemberPartisipan) {
            var q = _context.AdministrasiKeanggotaan.AsQueryable();
            var utcNow = DateTime.UtcNow;
            q = q.Where($"{nameof(AdministrasiKeanggotaan.Status)} == @0", DataAknpRequestStatus.APPROVED_STATUS.Name)
                .Where($"{nameof(AdministrasiKeanggotaan.Draft)} == @0", false)
                .Where($"{nameof(AdministrasiKeanggotaan.UserDraft)} == null")
                .Where($"{nameof(AdministrasiKeanggotaan.ActiveUtc)} != null")
                .Where($"{nameof(AdministrasiKeanggotaan.ActiveUtc)} <= @0", utcNow);
            if (!string.IsNullOrWhiteSpace(kodeAk))
            {
                q = q.Where($"{nameof(AdministrasiKeanggotaan.KodeAK)} == @0", kodeAk);
            }

            if (!string.IsNullOrWhiteSpace(kategoriMember))
            {
                q = q.Where(c => c.KategoriMember.Contains(kategoriMember));
                if (kategoriMember == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name && !string.IsNullOrWhiteSpace(tipeMemberPartisipan))
                {
                    q = q.Where($"{nameof(AdministrasiKeanggotaan.TipeMemberPartisipan)} == @0", tipeMemberPartisipan);
                }
            }

            q = q.OrderBy($"{nameof(AdministrasiKeanggotaan.CreatedUtc)} desc, {nameof(AdministrasiKeanggotaan.ActiveUtc)} desc");
            var dat = q.ToList();
            
            return dat;
        }

        public List<AdministrasiKeanggotaan> GetAllLatestActiveNamaPED(string kodeAk) {
            var q = _context.AdministrasiKeanggotaan.AsQueryable();
            var utcNow = DateTime.UtcNow;
            q = q.Where($"{nameof(AdministrasiKeanggotaan.Status)} == @0", DataAknpRequestStatus.APPROVED_STATUS.Name)
                .Where($"{nameof(AdministrasiKeanggotaan.NamaABSponsor)} == @0", kodeAk)
                .Where($"{nameof(AdministrasiKeanggotaan.Draft)} == @0", false)
                .Where($"{nameof(AdministrasiKeanggotaan.UserDraft)} == null")
                .Where($"{nameof(AdministrasiKeanggotaan.ActiveUtc)} != null")
                .Where($"{nameof(AdministrasiKeanggotaan.ActiveUtc)} <= @0", utcNow);
            q = q.Where(c => c.KategoriMember.Contains(RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name));
            q = q.Where($"{nameof(AdministrasiKeanggotaan.TipeMemberPartisipan)} == @0", "PED");

            q = q.OrderBy($"{nameof(AdministrasiKeanggotaan.CreatedUtc)} desc, {nameof(AdministrasiKeanggotaan.ActiveUtc)} desc");
            var dat = q.ToList();
            
            return dat;
        }

        private List<DataStatusKPEI> GetLatestStatusKpeiActiveData(string administrasiKeanggotaanId) {
            var q = _context.DataStatusKPEI.AsQueryable();
            var utcNow = DateTime.UtcNow;
            q = q.Where($"{nameof(BaseAknpData.Status)} == @0", DataAknpRequestStatus.APPROVED_STATUS.Name)
                .Where($"{nameof(BaseAknpData.Draft)} == @0", false)
                .Where($"{nameof(BaseAknpData.ActiveUtc)} != null")
                .Where($"{nameof(BaseAknpData.ActiveUtc)} <= @0", utcNow)
                .Where($"{nameof(DataStatusKPEI.AdministrasiKeanggotaanId)} == @0", administrasiKeanggotaanId)
                .OrderBy($"{nameof(BaseAknpData.CreatedUtc)} desc, {nameof(BaseAknpData.ActiveUtc)} desc");
            var dat = q.ToList();
            return dat;
        }

        private DataStatusKPEI GetStatusKpeiActiveData(string administrasiKeanggotaanId) {
            var q = _context.DataStatusKPEI.AsQueryable();
            var utcNow = DateTime.UtcNow;
            q = q.Where($"{nameof(BaseAknpData.Status)} == @0", DataAknpRequestStatus.APPROVED_STATUS.Name)
                .Where($"{nameof(BaseAknpData.Draft)} == @0", false)
                .Where($"{nameof(BaseAknpData.ActiveUtc)} != null")
                .Where($"{nameof(BaseAknpData.ActiveUtc)} <= @0", utcNow)
                .Where($"{nameof(DataStatusKPEI.AdministrasiKeanggotaanId)} == @0", administrasiKeanggotaanId)
                .OrderBy($"{nameof(BaseAknpData.CreatedUtc)} desc, {nameof(BaseAknpData.ActiveUtc)} desc");
            var dat = q.FirstOrDefault();
            return dat;
        }

        public AdministrasiKeanggotaan GetLatestActiveData(string kodeAk, string kategoriMember, string tipeMemberPartisipan) {
            var q = _context.AdministrasiKeanggotaan.AsQueryable();
            var utcNow = DateTime.UtcNow;
            q = q.Where($"{nameof(AdministrasiKeanggotaan.Status)} == @0", DataAknpRequestStatus.APPROVED_STATUS.Name)
                .Where($"{nameof(AdministrasiKeanggotaan.KodeAK)} == @0", kodeAk)
                .Where($"{nameof(AdministrasiKeanggotaan.Draft)} == @0", false)
                .Where($"{nameof(AdministrasiKeanggotaan.UserDraft)} == null")
                .Where($"{nameof(AdministrasiKeanggotaan.ActiveUtc)} != null")
                .Where($"{nameof(AdministrasiKeanggotaan.ActiveUtc)} <= @0", utcNow);
            if (!string.IsNullOrWhiteSpace(kategoriMember))
            {
                q = q.Where(c => c.KategoriMember.Contains(kategoriMember));
                if (kategoriMember == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name && !string.IsNullOrWhiteSpace(tipeMemberPartisipan))
                {
                    q = q.Where($"{nameof(AdministrasiKeanggotaan.TipeMemberPartisipan)} == @0", tipeMemberPartisipan);
                }
            }

            q = q.OrderBy($"{nameof(AdministrasiKeanggotaan.CreatedUtc)} desc, {nameof(AdministrasiKeanggotaan.ActiveUtc)} desc");
            var dat = q.FirstOrDefault();
            if (dat != null && dat.KategoriMember != null)
            {
                dat.TradingMembers = _context.AdministrasiKeanggotaanTradingMember.Where(x => x.AdministrasiKeanggotaanId == dat.Id).ToList();
                dat.StatusKpei = GetLatestStatusKpeiActiveData(dat.Id);
                dat.LayananBaru =
                    _context.AdministrasiKeanggotaanLayananBaru.Where(x =>
                        x.AdministrasiKeanggotaanId == dat.Id).ToList();
            }
            return dat ?? new AdministrasiKeanggotaan();
        }

        public AdministrasiKeanggotaan GetLatestDraftData(string kodeAk, string userId) {
            var q = _context.AdministrasiKeanggotaan.AsQueryable();
            q = q.Where($"{nameof(AdministrasiKeanggotaan.KodeAK)} == @0", kodeAk)
                .Where($"{nameof(AdministrasiKeanggotaan.Draft)} == @0", true)
                .Where($"{nameof(AdministrasiKeanggotaan.UserDraft)} == @0", userId);

            var dat = q.FirstOrDefault();
            if (dat != null && dat.KategoriMember != null)
            {
                dat.TradingMembers = _context.AdministrasiKeanggotaanTradingMember.Where(x => x.AdministrasiKeanggotaanId == dat.Id).ToList();
                dat.LayananBaru =
                    _context.AdministrasiKeanggotaanLayananBaru.Where(x =>
                        x.AdministrasiKeanggotaanId == dat.Id).ToList();
            }
            return dat ?? new AdministrasiKeanggotaan();
        }
        public List<AdministrasiKeanggotaanTradingMember> GetTradingMemberFromAdministrasiKeanggotaan(string id) =>
            _context.AdministrasiKeanggotaanTradingMember.Where(x => x.AdministrasiKeanggotaanId == id).ToList();

        public List<AdministrasiKeanggotaanLayananBaru> GetLayananBaruFromAdministrasiKeanggotaan(string id) =>
            _context.AdministrasiKeanggotaanLayananBaru.Where(x => x.AdministrasiKeanggotaanId == id).ToList();

        public List<TradingMemberForm> GetTradingMemberFromAdministrasiKeanggotaanForm(string id)
        {
            List<AdministrasiKeanggotaanTradingMember> listTradingMembers =
                _context.AdministrasiKeanggotaanTradingMember.Where(x => x.AdministrasiKeanggotaanId == id).ToList();
            List<TradingMemberForm> listTradingMemberForm = new List<TradingMemberForm>();
            for (int i = 0; i < listTradingMembers?.Count; i++)
            {
                TradingMemberForm tradingMember = new TradingMemberForm
                {
                    Id = listTradingMembers[i].Id,
                    Nama = listTradingMembers[i].Nama,
                    AdministrasiKeanggotaanId = listTradingMembers[i].AdministrasiKeanggotaanId,
                    NoPerjanjian = listTradingMembers[i].NoPerjanjian,
                    PerjanjianFileUrl = listTradingMembers[i].PerjanjianFileUrl,
                    TanggalPerjanjian = listTradingMembers[i].TanggalPerjanjian
                };
                listTradingMemberForm.Add(tradingMember);
            }

            return listTradingMemberForm;
        }

        public AdministrasiKeanggotaan Create(AdministrasiKeanggotaanAknpForm form)
        {
            AdministrasiKeanggotaan newLayanan = new AdministrasiKeanggotaan
            {
                Status = DataAknpRequestStatus.NEW_STATUS.Name,
                KodeAK = form.KodeAK,
                KategoriMember = form.KategoriMember,
                TipeMemberPartisipan = form.TipeMemberPartisipan,
                DokumenKhususIjinUsahaFileUrl = form.DokumenKhususIjinUsahaFileUrl,
                NamaABSponsor = form.NamaABSponsor,
                NoIjinUsahaPED = form.NoIjinUsahaPED,
                NoPerjanjianDenganKPEI = form.NoPerjanjianDenganKPEI,
                NoPerjanjianPortabilitySponsor = form.NoPerjanjianPortabilitySponsor,
                PerjanjianDenganKPEIFileUrl = form.PerjanjianDenganKPEIFileUrl,
                PerjanjianPortabilitySponsorFileUrl = form.PerjanjianPortabilitySponsorFileUrl,
                TanggalPerjanjianDenganKPEIUtc = form.TanggalPerjanjianDenganKPEI,
                TanggalPerjanjianPortabilitySponsor = form.TanggalPerjanjianPortabilitySponsor,
                Draft = form.Draft,
                UserDraft = form.UserDraft
            };
            if (form.Approver1UserId != null && form.Approver2UserId != null)
            {
                newLayanan.Approver1UserId = form.Approver1UserId;
                newLayanan.Approver2UserId = form.Approver2UserId;
                newLayanan.Status = form.Status;
                newLayanan.ActiveUtc = DateTime.UtcNow;
                AdministrasiKeanggotaan existing =
                    GetLatestActiveData(form.KodeAK, form.KategoriMember, form.TipeMemberPartisipan);
                if (!string.IsNullOrWhiteSpace(existing.KodeAK) && form.Draft == false && string.IsNullOrWhiteSpace(form.UserDraft))
                {
                    var statusKpei = GetStatusKpeiActiveData(existing.Id);
                    if (statusKpei != null && statusKpei.StatusKPEI != StatusKpei.CABUT_STATUS.Name)
                    {
                        
                        var s = _context.DataStatusKPEI.FirstOrDefault(x => x.Id == statusKpei.Id);
                        s.AdministrasiKeanggotaanId = newLayanan.Id;
                    }
                    existing.Draft = true;
                }
            }
            _context.AdministrasiKeanggotaan.Add(newLayanan);
            _context.SaveChanges();
            return newLayanan;
        }

        public AdministrasiKeanggotaan Update(AdministrasiKeanggotaanAknpForm form)
        {
            AdministrasiKeanggotaan updateLayanan = Get(form.Id);
            updateLayanan.KodeAK = form.KodeAK;
            updateLayanan.KategoriMember = form.KategoriMember;
            updateLayanan.TipeMemberPartisipan = form.TipeMemberPartisipan;
            updateLayanan.DokumenKhususIjinUsahaFileUrl = form.DokumenKhususIjinUsahaFileUrl;
            updateLayanan.NamaABSponsor = form.NamaABSponsor;
            updateLayanan.NoIjinUsahaPED = form.NoIjinUsahaPED;
            updateLayanan.NoPerjanjianDenganKPEI = form.NoPerjanjianDenganKPEI;
            updateLayanan.NoPerjanjianPortabilitySponsor = form.NoPerjanjianPortabilitySponsor;
            updateLayanan.PerjanjianDenganKPEIFileUrl = form.PerjanjianDenganKPEIFileUrl;
            updateLayanan.PerjanjianPortabilitySponsorFileUrl = form.PerjanjianPortabilitySponsorFileUrl;
            updateLayanan.TanggalPerjanjianDenganKPEIUtc = form.TanggalPerjanjianDenganKPEI;
            updateLayanan.TanggalPerjanjianPortabilitySponsor = form.TanggalPerjanjianPortabilitySponsor;
            updateLayanan.UpdatedUtc = DateTime.UtcNow;
            updateLayanan.Draft = form.Draft;
            updateLayanan.UserDraft = form.UserDraft;

            if (updateLayanan.Approver1UserId != null && updateLayanan.Approver2UserId != null)
            {
                AdministrasiKeanggotaan existing =
                    GetLatestActiveData(updateLayanan.KodeAK, updateLayanan.KategoriMember, updateLayanan.TipeMemberPartisipan);
                if (!string.IsNullOrWhiteSpace(existing.KodeAK) && updateLayanan.Draft == false && string.IsNullOrWhiteSpace(updateLayanan.UserDraft))
                {
                    var statusKpei = GetStatusKpeiActiveData(existing.Id);
                    if (statusKpei != null && statusKpei.StatusKPEI != StatusKpei.CABUT_STATUS.Name)
                    {
                        var s = _context.DataStatusKPEI.FirstOrDefault(x => x.Id == statusKpei.Id);
                        s.AdministrasiKeanggotaanId = updateLayanan.Id;
                    }
                    existing.Draft = true;
                }
                updateLayanan.ActiveUtc = DateTime.UtcNow;
            }

            _context.SaveChanges();
            return updateLayanan;
        }

        public AdministrasiKeanggotaanTradingMember CreateTradingMember(TradingMemberForm form)
        {
            AdministrasiKeanggotaanTradingMember newTradingMember = new AdministrasiKeanggotaanTradingMember
            {
                AdministrasiKeanggotaanId = form.AdministrasiKeanggotaanId,
                Nama = form.Nama,
                NoPerjanjian = form.NoPerjanjian,
                PerjanjianFileUrl = form.PerjanjianFileUrl,
                TanggalPerjanjian = form.TanggalPerjanjian,
            };
            _context.AdministrasiKeanggotaanTradingMember.Add(newTradingMember);
            _context.SaveChanges();
            return newTradingMember;
        }

        public void RemoveDraft(string id)
        {
            AdministrasiKeanggotaan draft = Get(id);
            _context.Entry(draft).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public AdministrasiKeanggotaanLayananBaru CreateUpdateLayananBaru(AdministrasiKeanggotaanAknpForm form, string administrasiKeanggotaanId)
        {
            List<DataLayananBaruForm> listLayananForm = form.FormLayananBaruAku;
            if (form.KategoriMember == RegistrasiKategoriMember.MEMBER_AKI.Name)
            {
                listLayananForm = form.FormLayananBaruAki;
            }
            if (form.KategoriMember == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name)
            {
                listLayananForm = form.FormGroupPartisipan;
            }

            _context.AdministrasiKeanggotaanLayananBaru.RemoveRange(
                _context.AdministrasiKeanggotaanLayananBaru
                    .Where(x => x.AdministrasiKeanggotaanId == administrasiKeanggotaanId));

            foreach (var layananBaru in listLayananForm)
            {
                AdministrasiKeanggotaanLayananBaru layanan = new AdministrasiKeanggotaanLayananBaru{
                    AdministrasiKeanggotaanId = administrasiKeanggotaanId,
                    TipeMemberPartisipan = layananBaru.TipeMemberPartisipan,
                    TipeMemberAK = layananBaru.TipeMemberAK,
                    FieldName = layananBaru.FieldName,
                    FieldType = layananBaru.FieldType,
                    FieldValue = layananBaru.FieldValue,
                    LayananBaruId = layananBaru.LayananBaruId,
                };

                _context.AdministrasiKeanggotaanLayananBaru.Add(layanan);
                _context.SaveChanges();
            }
            return new AdministrasiKeanggotaanLayananBaru();
        }

        public AdministrasiKeanggotaanTradingMember UpdateTradingMember(TradingMemberForm form)
        {
            AdministrasiKeanggotaanTradingMember tradingMember = _context.AdministrasiKeanggotaanTradingMember
                .FirstOrDefault(x => x.Id == form.Id);

            tradingMember.Nama = form.Nama;
            tradingMember.PerjanjianFileUrl = form.PerjanjianFileUrl;
            tradingMember.NoPerjanjian = form.NoPerjanjian;
            tradingMember.TanggalPerjanjian = (DateTime) form.TanggalPerjanjian;
            _context.SaveChanges();
            return tradingMember;
        }

        public PaginatedResult<DataChangeRequest> ListRequest(ChangeRequestDttRequestForm req)
        {
            var result = new PaginatedResult<DataChangeRequest>();

            var allQ = _context.DataChangeRequests.AsQueryable();
            var filQ = _context.DataChangeRequests.AsQueryable();
            filQ = filQ.Where(x => x.DataDriverSlug == req.DataSlug);

            if (!string.IsNullOrWhiteSpace(req.KodeAK)) {
                filQ = filQ.Where(x => x.KodeAk == req.KodeAK);
            }

            if (!string.IsNullOrWhiteSpace(req.Status)) {
                filQ = filQ.Where(x => x.Status == req.Status);
            }
            
            if (req.FromDateTime != null) {
                var fromUtc = req.FromDateTime.Value.Date.ToUniversalTime();
                filQ = filQ.Where(x => x.CreatedUtc >= fromUtc);
            }

            if (req.ToDateTime != null) {
                var toUtc = req.ToDateTime.Value.Date.AddDays(1).ToUniversalTime();
                filQ = filQ.Where(x => x.CreatedUtc <= toUtc);
            }

            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                var kws = Regex.Split(req.search.value, @"\s+");
                if (kws.Length > 0) {
                    filQ = filQ.Where(x =>
                        kws.All(kw => x.KodeAk.Contains(kw))
                        || kws.All(kw => x.Status.Contains(kw))
                    );
                }
            }

            if (req.firstOrderColumn?.name == nameof(DataChangeRequest.KodeAk)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.KodeAk)
                    : filQ.OrderBy(x => x.KodeAk);
            } else if (req.firstOrderColumn?.name == nameof(DataChangeRequest.Status)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.Status)
                    : filQ.OrderBy(x => x.Status);
            } else {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.CreatedUtc)
                    : filQ.OrderBy(x => x.CreatedUtc);
            }

            result.Initialize(allQ, filQ, req.start, req.length);

            return result;
        }

        public PaginatedResult<AdministrasiKeanggotaan> ListHistory(ChangeRequestDttRequestForm req)
        {
            var result = new PaginatedResult<AdministrasiKeanggotaan>();

            var allQ = _context.AdministrasiKeanggotaan.AsQueryable();
            var filQ = _context.AdministrasiKeanggotaan.AsQueryable();
            filQ = filQ.Where(x => x.UserDraft == null);

            if (!string.IsNullOrWhiteSpace(req.KodeAK)) {
                filQ = filQ.Where(x => x.KodeAK == req.KodeAK);
            }

            if (!string.IsNullOrWhiteSpace(req.Status)) {
                filQ = filQ.Where(x => x.Status == req.Status);
            }
            
            if (req.FromDateTime != null) {
                var fromUtc = req.FromDateTime.Value.Date.ToUniversalTime();
                filQ = filQ.Where(x => x.CreatedUtc >= fromUtc);
            }

            if (req.ToDateTime != null) {
                var toUtc = req.ToDateTime.Value.Date.AddDays(1).ToUniversalTime();
                filQ = filQ.Where(x => x.CreatedUtc <= toUtc);
            }

            if (req.ActiveFromDateTime != null) {
                var fromUtc = req.ActiveFromDateTime.Value.Date.ToUniversalTime();
                filQ = filQ.Where(x => x.ActiveUtc >= fromUtc);
            }

            if (req.ActiveToDateTime != null) {
                var toUtc = req.ActiveToDateTime.Value.Date.AddDays(1).ToUniversalTime();
                filQ = filQ.Where(x => x.ActiveUtc <= toUtc);
            }

            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                var kws = Regex.Split(req.search.value, @"\s+");
                if (kws.Length > 0) {
                    filQ = filQ.Where(x =>
                        kws.All(kw => x.KodeAK.Contains(kw))
                        || kws.All(kw => x.Status.Contains(kw))
                    );
                }
            }

            if (req.firstOrderColumn?.name == nameof(AdministrasiKeanggotaan.KodeAK)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.KodeAK)
                    : filQ.OrderBy(x => x.KodeAK);
            } else if (req.firstOrderColumn?.name == nameof(AdministrasiKeanggotaan.Status)) {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.Status)
                    : filQ.OrderBy(x => x.Status);
            } else {
                filQ = req.order.FirstOrDefault()?.IsDesc == true
                    ? filQ.OrderByDescending(x => x.CreatedUtc)
                    : filQ.OrderBy(x => x.CreatedUtc);
            }

            result.Initialize(allQ, filQ, req.start, req.length);

            return result;
        }

        public PaginatedResult<AdministrasiKeanggotaan> ListActive(ChangeRequestDttRequestForm req)
        {
            var result = new PaginatedResult<AdministrasiKeanggotaan>();

            var allQ = _context.AdministrasiKeanggotaan.AsQueryable();
            var filQ = _context.AdministrasiKeanggotaan.AsQueryable();
            filQ = filQ.Where(x => x.Draft == false);
            filQ = filQ.Where(x => x.UserDraft == null);
            filQ = filQ.Where($"{nameof(AdministrasiKeanggotaan.Status)} == @0",
                    DataAknpRequestStatus.APPROVED_STATUS.Name)
                .Where($"{nameof(AdministrasiKeanggotaan.ActiveUtc)} != null")
                .Where($"{nameof(BaseAknpData.ActiveUtc)} <= @0", DateTime.UtcNow);

            if (!string.IsNullOrWhiteSpace(req.KodeAK)) {
                filQ = filQ.Where(x => x.KodeAK == req.KodeAK);
            }

            if (!string.IsNullOrWhiteSpace(req.search.value)) {
                var kws = Regex.Split(req.search.value, @"\s+");
                if (kws.Length > 0) {
                    filQ = filQ.Where(x =>
                        kws.All(kw => x.KodeAK.Contains(kw))
                        || kws.All(kw => x.Status.Contains(kw))
                    );
                }
            }

            filQ = req.order.FirstOrDefault()?.IsDesc == true
                ? filQ.OrderByDescending(x => x.KodeAK)
                : filQ.OrderBy(x => x.KodeAK);

            result.Initialize(allQ, filQ, req.start, req.length);

            return result;
        }
    }
}
