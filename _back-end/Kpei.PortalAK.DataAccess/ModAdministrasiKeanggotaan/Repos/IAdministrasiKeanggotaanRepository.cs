﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;

namespace Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Repos
{
    public interface IAdministrasiKeanggotaanRepository
    {
        void RemoveDraft(string id);
        IEnumerable<AdministrasiKeanggotaan> AdministrasiKeanggotaans { get; }

        AdministrasiKeanggotaan Get(string id);

        DataRegistrasi GetLatestActiveAknpData(string kodeAk);

        List<AdministrasiKeanggotaan> GetAllLatestActiveData(string kodeAk, string kategoriMember, string tipeMemberPartisipan);
        List<AdministrasiKeanggotaan> GetAllLatestActiveNamaPED(string kodeAk);
        AdministrasiKeanggotaan GetLatestActiveData(string kodeAk, string kategoriMember, string tipeMemberPartisipan);
        AdministrasiKeanggotaan GetLatestDraftData(string kodeAk, string userId);
        AdministrasiKeanggotaan GetNewOngoingData(string kodeAk, string kategoriMember, string tipeMemberPartisipan);

        Tuple<AdministrasiKeanggotaan, DataChangeRequest> CreateChangeRequest(AdministrasiKeanggotaanAknpForm data);
        Tuple<AdministrasiKeanggotaan, DataChangeRequest> CreateDraftChangeRequest(AdministrasiKeanggotaanAknpForm data);

        AdministrasiKeanggotaan Approve(string dataId, string userId);
        AdministrasiKeanggotaan Reject(string id, string rejectReason);

        List<AdministrasiKeanggotaanTradingMember> GetTradingMemberFromAdministrasiKeanggotaan(string id);
        List<AdministrasiKeanggotaanLayananBaru> GetLayananBaruFromAdministrasiKeanggotaan(string id);
        List<TradingMemberForm> GetTradingMemberFromAdministrasiKeanggotaanForm(string id);

        AdministrasiKeanggotaan Create(AdministrasiKeanggotaanAknpForm form);
        AdministrasiKeanggotaan Update(AdministrasiKeanggotaanAknpForm form);

        AdministrasiKeanggotaanTradingMember CreateTradingMember(TradingMemberForm form);
        AdministrasiKeanggotaanLayananBaru CreateUpdateLayananBaru(AdministrasiKeanggotaanAknpForm form, string administrasiKeanggotaanId);
        AdministrasiKeanggotaanTradingMember UpdateTradingMember(TradingMemberForm form);
        PaginatedResult<DataChangeRequest> ListRequest(ChangeRequestDttRequestForm req);
        PaginatedResult<AdministrasiKeanggotaan> ListHistory(ChangeRequestDttRequestForm req);
        PaginatedResult<AdministrasiKeanggotaan> ListActive(ChangeRequestDttRequestForm req);

    }
}
