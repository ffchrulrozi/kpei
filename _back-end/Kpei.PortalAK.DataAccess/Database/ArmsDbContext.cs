﻿using System.Data.Entity;

namespace Kpei.PortalAK.DataAccess.Database {
    public class ArmsDbContext : DbContext {
        public ArmsDbContext() : base("ArmsDbConnection") {
            this.Database.CommandTimeout = 120;
        }
    }
}