﻿using System.Data.Entity;

namespace Kpei.PortalAK.DataAccess.Database {
    public class MemberDbContext : DbContext {
        public MemberDbContext() : base("MemberDbConnection") { }
    }
}