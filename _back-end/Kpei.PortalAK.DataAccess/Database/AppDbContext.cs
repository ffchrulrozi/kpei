﻿using System.Data.Entity;
using Kpei.PortalAK.DataAccess.DbModels;

namespace Kpei.PortalAK.DataAccess.Database {
    public class AppDbContext : DbContext {
        public AppDbContext() : base("MasterDbConnection") { }
        public AppDbContext(string nameOrConnectionString) : base(nameOrConnectionString) { }

        #region Setting
        public DbSet<Setting> Setting { get; set; }
        public DbSet<UserRight> UserRights { get; set; }
        public DbSet<GlobalSetting> GlobalSettings { get; set; }
        public DbSet<SmtpEmailSetting> SmtpEmailSettings { get; set; }
        public DbSet<PincodeSetting> PincodeSettings { get; set; }
        public DbSet<Deadline> Deadline { get; set; }
        public DbSet<LayananBaru> LayananBaru { get; set; }
        #endregion

        #region Request PIN Code

        public DbSet<PincodeRequest> PincodeRequest { get; set; }
        public DbSet<PincodeResetResult> PincodeResetResults { get; set; }

        #endregion

        #region Global

        public DbSet<UserActionLog> UserActionLogs { get; set; }

        #endregion

        #region Layanan m-Clears

        public DbSet<MclearsPeserta> MclearsPeserta { get; set; }
        public DbSet<MclearsRequest> MclearsRequest { get; set; }

        #endregion

        #region administrasi keanggotaan
        public DbSet<AdministrasiKeanggotaan> AdministrasiKeanggotaan { get; set; }
        public DbSet<AdministrasiKeanggotaanLayananBaru> AdministrasiKeanggotaanLayananBaru { get; set; }
        public DbSet<AdministrasiKeanggotaanTradingMember> AdministrasiKeanggotaanTradingMember { get; set; }

        #endregion

        #region Sosialisasi & Pelatihan

        public DbSet<Event> Event { get; set; }
        public DbSet<EventPembicara> EventPembicara { get; set; }
        public DbSet<EventPeserta> EventPeserta { get; set; }

        #endregion

        #region Survei Kepuasan

        public DbSet<Survei> Survei { get; set; }
        public DbSet<SurveiDivisi> SurveiDivisi { get; set; }
        public DbSet<SurveiPerusahaan> SurveiPerusahaan { get; set; }

        #endregion

        #region Laporan Keuangan

        public DbSet<KeuanganLaporan> KeuanganLaporan { get; set; }

        #endregion

        #region AK & P

        public DbSet<DataChangeRequest> DataChangeRequests { get; set; }

        #region Akte Related Data

        public DbSet<DataAkte> DataAkte { get; set; }
        public DbSet<DataPemegangSaham> DataPemegangSaham { get; set; }
        public DbSet<DataKomisaris> DataKomisaris { get; set; }
        public DbSet<DataDireksi> DataDireksi { get; set; }

        #endregion

        #region Registrasi Related Data

        public DbSet<DataRegistrasi> DataRegistrasi { get; set; }
        public DbSet<DataSupportDocumentRegistrasi> DataSupportDocumentRegistrasi { get; set; }
        public DbSet<DataContactPersonRegistrasi> DataContactPersonRegistrasi { get; set; }
        public DbSet<DataDireksiRegistrasi> DataDireksiRegistrasi { get; set; }
        public DbSet<DataKomisarisRegistrasi> DataKomisarisRegistrasi { get; set; }
        public DbSet<DataPejabatBerwenangRegistrasi> DataPejabatBerwenangRegistrasi { get; set; }
        public DbSet<DataPemegangSahamRegistrasi> DataPemegangSahamRegistrasi { get; set; }

        #endregion

        public DbSet<DataTM> DataTM { get; set; }
        public DbSet<DataAlamat> DataAlamat { get; set; }
        public DbSet<DataBankPembayaran> DataBankPembayaran { get; set; }
        public DbSet<DataContactPerson> DataContactPerson { get; set; }
        public DbSet<DataContactPersonItem> DataContactPersonItem { get; set; }
        public DbSet<DataInformasiLain> DataInformasiLain { get; set; }
        public DbSet<DataJenisUsaha> DataJenisUsaha { get; set; }
        public DbSet<DataPejabatBerwenang> DataPejabatBerwenang { get; set; }
        public DbSet<DataPejabatBerwenangItem> DataPejabatBerwenangItem { get; set; }
        public DbSet<DataPerjanjianEBU> DataPerjanjianEBU { get; set; }
        public DbSet<DataPerjanjianKBOS> DataPerjanjianKBOS { get; set; }
        public DbSet<DataPME> DataPME { get; set; }
        public DbSet<DataRequest> DataRequest { get; set; }
        public DbSet<DataStatusBursa> DataStatusBursa { get; set; }
        public DbSet<DataStatusKPEI> DataStatusKPEI { get; set; }

        public DbSet<DataTipeMember> DataTipeMember { get; set; }
        public DbSet<DataTipeMemberPartisipanItem> DataTipeMemberPartisipanItem { get; set; }
        public DbSet<DataLayananBaru> DataLayananBaru { get; set; }
        public DbSet<DataTradingMember> DataTradingMember { get; set; }
        public DbSet<DataSPAK> DataSPAK { get; set; }
        public DbSet<DataSPAB> DataSPAB { get; set; }
        public DbSet<DataPerjanjian> DataPerjanjian { get; set; }
        #endregion

        #region Notification

        public DbSet<Notifications> Notification { get; set; }

        #endregion
        
        public DbSet<SynchronizeTable> SynchronizeTable { get; set; }

    }
}