﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class UpdateRequestChangeViewModel {
        public string Id { get; set; }
        
        public BaseAknpDriver Driver { get; set; }
        public AppUser User { get; set; }
        public BaseAknpForm Form { get; set; }

        private KpeiCompany[] _companies;

        public GlobalSettingForm GlobalSetting { get; set; }

        public KpeiCompany[] Companies {
            get { return _companies ?? new KpeiCompany[0]; }
            set { _companies = value; }
        }
        private DataAknpRequestStatus[] _statuses;
        public DataAknpRequestStatus[] Statuses {
            get { return _statuses ?? new DataAknpRequestStatus[0]; }
            set { _statuses = value; }
        }
    }
}