﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class FormRenderViewModel {
        public BaseAknpDriver Driver { get; set; }

        public GlobalSettingForm GlobalSetting { get; set; }

        public KpeiCompany[] Companies { get; set; }
        public AppUser User { get; set; }
        public BaseAknpForm Form { get; set; }
    }
}