﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class DetailKpeiViewModel {
        private KpeiCompany[] _companies;

        public BaseAknpDriver Driver { get; set; }
        public AppUser User { get; set; }
        public string KodeAk { get; set; }
        public string ViewMode { get; set; }

        public KpeiCompany[] Companies {
            get { return _companies ?? new KpeiCompany[0]; }
            set { _companies = value; }
        }
    }
}