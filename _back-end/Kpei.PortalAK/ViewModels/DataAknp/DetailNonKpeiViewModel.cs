﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class DetailNonKpeiViewModel {
        public BaseAknpDriver Driver { get; set; }
        public AppUser User { get; set; }
        public string KodeAk { get; set; }
    }

}