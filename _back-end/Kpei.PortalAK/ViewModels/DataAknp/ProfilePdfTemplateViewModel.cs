﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class ProfilePdfTemplateViewModel {
        private KpeiCompany[] _companies;
        public AppUser User { get; set; }
        public AknpProfileDisplay ProfileData { get; set; }
        public string KodeAk { get; set; }
        public KpeiCompany[] Companies {
            get { return _companies ?? new KpeiCompany[0]; }
            set { _companies = value; }
        }
    }
}