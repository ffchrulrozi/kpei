﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.Features.DataAknpViewSystem;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class FieldViewModel {
        public object PropertyValue { get; set; }
        public BaseAknpData Data { get; set; }
        public DataAknpViewDefinition ViewDefinition { get; set; }
        public BaseAknpDriver Driver { get; set; }
        public AppUser User { get; set; }
    }
}