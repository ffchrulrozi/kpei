﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class RequestChangeLogViewModel {
        public BaseAknpDriver Driver { get; set; }
        public BaseAknpData Data { get; set; }
        public AppUser User { get; set; }
    }
}