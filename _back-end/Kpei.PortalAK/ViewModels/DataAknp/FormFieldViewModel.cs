﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.Features.DataAknpViewSystem;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class FormFieldViewModel {
        public object PropertyValue { get; set; }
        public BaseAknpForm Form { get; set; }
        public DataAknpViewDefinition ViewDefinition { get; set; }
        public BaseAknpDriver Driver { get; set; }
        public AppUser User { get; set; }
        public string KodeAk { get; set; }
    }
}