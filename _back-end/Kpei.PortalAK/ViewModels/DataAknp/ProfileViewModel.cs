﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Services;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class ProfileViewModel {
        public AppUser User { get; set; }
        public IDataAknpDriverStore DriverStore { get; set; }
        public string KodeAk { get; set; }
        private KpeiCompany[] _companies;
        public KpeiCompany[] Companies {
            get { return _companies ?? new KpeiCompany[0]; }
            set { _companies = value; }
        }
    }
}