﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class ListGlobalRequestViewModel {
        private KpeiCompany[] _companies;
        private DataAknpRequestStatus[] _requestStatuses;
        public AppUser User { get; set; }
        public DataAknpRequestStatus[] RequestStatuses {
            get { return _requestStatuses ?? new DataAknpRequestStatus[0]; }
            set { _requestStatuses = value; }
        }
        public KpeiCompany[] Companies {
            get { return _companies ?? new KpeiCompany[0]; }
            set { _companies = value; }
        }
        private DataAknpDefinition[] _dataDefinitions;
        public DataAknpDefinition[] DataDefinitions {
            get { return _dataDefinitions ?? new DataAknpDefinition[0]; }
            set { _dataDefinitions = value; }
        }
    }
}