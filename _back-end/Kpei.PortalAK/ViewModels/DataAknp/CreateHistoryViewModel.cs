﻿using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class CreateHistoryViewModel {
        private KpeiCompany[] _companies;
        public BaseAknpDriver Driver { get; set; }

        public GlobalSettingForm GlobalSetting { get; set; }

        public AppUser User { get; set; }
        public BaseAknpForm Form { get; set; }
        public string KodeAk { get; set; }

        public KpeiCompany[] Companies {
            get { return _companies ?? new KpeiCompany[0]; }
            set { _companies = value; }
        }
    }
}