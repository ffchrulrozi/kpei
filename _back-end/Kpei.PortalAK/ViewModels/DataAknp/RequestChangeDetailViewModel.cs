﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class RequestChangeDetailViewModel {
        public BaseAknpDriver Driver { get; set; }
        public AppUser User { get; set; }
        public BaseAknpData Data { get; set; }
    }
}