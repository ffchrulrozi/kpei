﻿using System.ComponentModel.DataAnnotations;

namespace Kpei.PortalAK.ViewModels.DataAknp {
    public class RejectRequestChangeViewModel {
        private string _rejectReason;

        [Display(Name = "Alasan Penolakan")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} harus diisi.")]
        [StringLength(1024)]
        public string RejectReason {
            get { return _rejectReason?.Trim(); }
            set { _rejectReason = value; }
        }
    }
}