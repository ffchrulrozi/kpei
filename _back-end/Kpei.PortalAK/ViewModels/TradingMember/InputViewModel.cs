﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Models;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModTradingMember.Forms;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.TradingMember
{
    public class InputViewModel
    {
        public AppUser User { get; set; }

        [Required]
        public string TipePerusahaan { get; set; }

        private IEnumerable<string> _dataTradingMember;
        public IEnumerable<string> DataTradingMemberAKI {
            get
            {
                _dataTradingMember = _dataTradingMember == null
                    ? new List<string>()
                    : _dataTradingMember = _dataTradingMember
                        .Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                return _dataTradingMember;
            } 
            set
            {
                _dataTradingMember = value;
            } 
        }

        public List<TradingMemberForm> DataTradingMember { get; set; }
    }
}