﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Models;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.TradingMember
{
    public class ListViewModel
    {
        private IEnumerable<KpeiCompany> _companies;

        public AppUser User { get; set; }
        public GlobalSettingForm GlobalSetting { get; set; }

        public IEnumerable<KpeiCompany> Companies {
            get { return _companies ?? new KpeiCompany[0]; }
            set { _companies = value; }
        }
    }
}