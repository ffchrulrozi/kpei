﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels {
    public class MclearsRequestViewModel {
        public IEnumerable<MclearsRequest> requests { get; set; }
        public AppUser AppUser { get; set; }
    }
}