﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;

namespace Kpei.PortalAK.ViewModels {
    public class SettingSurveiViewModel {
        public IEnumerable<Setting> KategoriPerusahaan { get; set; }
        public IEnumerable<Setting> KategoriDivisi { get; set; }
    }
}