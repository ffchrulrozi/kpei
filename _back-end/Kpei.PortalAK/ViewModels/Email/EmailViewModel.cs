﻿using Kpei.PortalAK.DataAccess.ModSettings.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Email {
    public class EmailViewModel {
        private List<EmailSetting> _listEmailSetting;

        public List<EmailSetting> ListEmailSetting {
            get {
                return _listEmailSetting ?? new List<EmailSetting>();
            }
            set {
                _listEmailSetting = value;
            }
        }
    }
}