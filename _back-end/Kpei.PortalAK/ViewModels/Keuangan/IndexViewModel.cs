﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.ModKeuangan.Models;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.Keuangan {
    public class IndexViewModel {

        private IEnumerable<KpeiCompany> _companies;
        private IEnumerable<PeriodeLaporanKeuangan> _periods;
        private IEnumerable<LaporanKeuanganStatus> _statuses;

        public IEnumerable<LaporanKeuanganStatus> Statuses {
            get { return _statuses ?? new LaporanKeuanganStatus[0]; }
            set { _statuses = value; }
        }
        public IEnumerable<KpeiCompany> Companies {
            get { return _companies ?? new KpeiCompany[0]; }
            set { _companies = value; }
        }
        public IEnumerable<PeriodeLaporanKeuangan> Periods {
            get { return _periods ?? new PeriodeLaporanKeuangan[0]; }
            set { _periods = value; }
        }

        public AppUser AppUser { get; set; }
    }
}