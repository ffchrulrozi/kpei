﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Keuangan {
    public class RejectViewModel {
        private string _rejectReason;

        [Display(Name = "Alasan Penolakan")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} harus diisi.")]
        [StringLength(1024)]
        public string RejectReason {
            get { return _rejectReason?.Trim(); }
            set { _rejectReason = value; }
        }
    }
}