﻿using Kpei.PortalAK.DataAccess.DbModels;

namespace Kpei.PortalAK.ViewModels.Keuangan {
    public class LogViewModel {
        public KeuanganLaporan Laporan { get; set; }
    }
}