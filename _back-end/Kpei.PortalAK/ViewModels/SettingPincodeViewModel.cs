﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;

namespace Kpei.PortalAK.ViewModels {
    public class SettingPincodeViewModel {
        public IEnumerable<Setting> JenisPincode { get; set; }
        public IEnumerable<Setting> URLTerkait { get; set; }
    }
}