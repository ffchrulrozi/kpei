﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels {
    public class SurveiViewModel {
        public Kpei.PortalAK.DataAccess.DbModels.Survei survei { get; set; }
        public List<SurveiPerusahaan> surveiPerusahaan { get; set; }
        public List<SurveiDivisi> surveiDivisi { get; set; }
        public List<Setting> listKategoriPerusahaan { get; set; }
        public List<Setting> listKategoriDivisi { get; set; }
        public AppUser AppUser { get; set; }
        public string helperId { get; set; }
    }
}