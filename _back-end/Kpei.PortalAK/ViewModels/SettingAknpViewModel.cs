﻿using Kpei.PortalAK.DataAccess.ModSettings.Forms;

namespace Kpei.PortalAK.ViewModels {
    public class SettingAknpViewModel {
        public AknpSettingForm Form { get; set; }
    }
}