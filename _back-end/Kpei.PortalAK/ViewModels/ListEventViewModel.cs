﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels {
    public class ListEventViewModel {
        public List<Event> listEvent;
        public List<EventPeserta> listPeserta;
        public AppUser AppUser;
    }
}