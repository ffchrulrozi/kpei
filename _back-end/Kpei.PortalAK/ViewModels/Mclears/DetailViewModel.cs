﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.UserIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Mclears {
    public class DetailViewModel {
        public MclearsRequest Request { get; set; }
        public MclearsPeserta Peserta { get; set; }
        public AppUser AppUser { get; set; }
        public int TotalItem { get; set; }
        public int Index { get; set; }
        public int TotalAlert { get; set; }
        public int TotalOnRequest { get; set; }
    }
}