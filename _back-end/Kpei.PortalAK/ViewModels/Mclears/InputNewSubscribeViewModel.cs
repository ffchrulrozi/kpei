﻿using Kpei.PortalAK.DataAccess.ModMclears.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Mclears {
    public class InputNewSubscribeViewModel {
        public UpdateMclearsRequestForm Request { get; set; }
    }
}