﻿using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModMclears.Models;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Mclears {
    public class IndexViewModel {
        private IEnumerable<KpeiCompany> _companies;
        private IEnumerable<MclearsJenisPerubahan> _perubahan;

        public IEnumerable<KpeiCompany> Companies {
            get { return _companies ?? new KpeiCompany[0]; }
            set { _companies = value; }
        }
        public IEnumerable<MclearsJenisPerubahan> Perubahan {
            get { return _perubahan ?? new MclearsJenisPerubahan[0]; }
            set { _perubahan = value; }
        }
        public IEnumerable<MclearsStatusRequest> Statuses => MclearsStatusRequest.All().ToList();

        public AppUser AppUser { get; set; }

    }
}