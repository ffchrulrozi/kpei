﻿using Kpei.PortalAK.DataAccess.DbModels;
using System.Collections.Generic;

namespace Kpei.PortalAK.ViewModels.Mclears {
    public class LogViewModel {
        public MclearsRequest MclearsRequest { get; set; }
        public List<MclearsPeserta> MclearsPeserta { get; set; }
    }
}