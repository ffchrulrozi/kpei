﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;

namespace Kpei.PortalAK.ViewModels {
    public class ListDataBaruViewModel {
        public IEnumerable<DataRequest> ListRequest { get; set; }
        public IEnumerable<DataRegistrasi> ListRegistrasi { get; set; }
    }
}