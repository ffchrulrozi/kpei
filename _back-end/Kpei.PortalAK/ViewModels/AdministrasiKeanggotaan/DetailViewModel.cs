﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.AdministrasiKeanggotaan
{
    public class DetailViewModel
    {
        public AppUser User { get; set; }

        public DataAccess.DbModels.AdministrasiKeanggotaan AdministrasiKeanggotaan { get; set; }

        public List<AdministrasiKeanggotaanTradingMember> TradingMember { get; set; }
        public List<AdministrasiKeanggotaanLayananBaru> LayananBaru { get; set; }
    }
}