﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.AdministrasiKeanggotaan
{
    public class InputViewModel
    {
        public AppUser User { get; set; }

        public GlobalSettingForm GlobalSetting { get; set; }

        public AdministrasiKeanggotaanAknpForm FormAknp { get; set; }
    }
}