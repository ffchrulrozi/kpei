﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Models;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.AdministrasiKeanggotaan
{
    public class ListRequestViewModel
    {
        private IEnumerable<KpeiCompany> _companies;

        public AppUser User { get; set; }
        public GlobalSettingForm GlobalSetting { get; set; }

        public IEnumerable<KpeiCompany> Companies {
            get { return _companies ?? new KpeiCompany[0]; }
            set { _companies = value; }
        }

        public IEnumerable<AdministrasiKeanggotaanRequestStatus> Statuses = AdministrasiKeanggotaanRequestStatus.All().ToList();
    }
}