﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.FileManager {
    public class UploadViewModel {
        private List<HttpPostedFileBase> _files;
        private string _folderPath;
        private List<string> _validFileExtensions;
        
        public List<HttpPostedFileBase> Files {
            get { return _files ?? (_files = new List<HttpPostedFileBase>()); }
            set { _files = value?.Where(x => x != null)?.ToList(); }
        }
        
        public List<string> ValidFileExtensions {
            get { return _validFileExtensions ?? (_validFileExtensions = new List<string>()); }
            set { _validFileExtensions = value?.Where(x => !string.IsNullOrWhiteSpace(x)).ToList(); }
        }

        private List<string> _validMimeTypes;
        public List<string> ValidMimeTypes {
            get { return _validMimeTypes ?? (_validMimeTypes = new List<string>()); }
            set { _validMimeTypes = value?.Where(x => !string.IsNullOrWhiteSpace(x)).ToList(); }
        }

        public int? MaxBytes { get; set; }
        
        [Required]
        [StringLength(512)]
        public string FolderPath {
            get { return _folderPath; }
            set { _folderPath = value?.Trim('/', '\\', ' ', '\t', '\r', '\n'); }
        }
    }
}