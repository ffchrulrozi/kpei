﻿using System;

namespace Kpei.PortalAK.ViewModels.UserRightSetting {
    public class IndexViewModel {
        public bool IsKpei { get; set; }
        public Tuple<string, string>[] Modules { get; set; }
    }
}