﻿using Kpei.PortalAK.DataAccess.DbModels;

namespace Kpei.PortalAK.ViewModels {
    public class SettingEmailViewModel {
        public Setting PincodeNewRequestAK { get; set; }
        public Setting PincodeNewRequestStaf { get; set; }
        public Setting PincodeSubmitAK { get; set; }
    }
}