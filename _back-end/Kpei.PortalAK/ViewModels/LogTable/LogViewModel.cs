﻿using Kpei.PortalAK.DataAccess.ModLog.Models;
using Kpei.PortalAK.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.LogTable {
    public class LogViewModel {
        private IEnumerable<KpeiCompany> _companies;
        public IEnumerable<KpeiCompany> Companies {
            get { return _companies ?? new KpeiCompany[0]; }
            set { _companies = value; }
        }


        public IEnumerable<RelatedEntityName> RelatedEntity => RelatedEntityName.All();
    }
}