﻿using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModSurvei.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Survey {
    public class EditViewModel {
        public UpdateSurveiForm Form { get; set; }
        public SurveySettingForm Setting { get; set; }
    }
}