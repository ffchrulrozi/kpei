﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.UserIdentity;
using System.Collections.Generic;

namespace Kpei.PortalAK.ViewModels.Survey {
    public class DetilViewModel {
        public Survei Survei { get; set; }
        public List<SurveiPerusahaan> ListPerusahaan { get; set; }
        public List<SurveiDivisi> ListDivisi { get; set; }
        public AppUser AppUser { get; set; }
    }
}