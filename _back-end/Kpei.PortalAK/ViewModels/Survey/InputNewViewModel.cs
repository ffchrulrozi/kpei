﻿using Kpei.PortalAK.DataAccess.ModSurvei.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Survey {
    public class InputNewViewModel {
        public CreateSurveiForm Form { get; set; }
        public SurveySettingForm Setting { get; set; }
    }
}