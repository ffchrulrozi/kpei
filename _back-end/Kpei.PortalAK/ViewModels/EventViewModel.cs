﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels {
    public class EventViewModel {
        public Event Event { get; set; }
        public List<EventPeserta> listPeserta { get; set; }
        public List<EventPembicara> listPembicara { get; set; }
        public List<Setting> opsiJenisPelatihan { get; set; }
        public string oldId { get; set; }
        public string helperPelaksanaan { get; set; }
        public string helperDaftarBuka { get; set; }
        public string helperDaftarTutup { get; set; }
        public string helperKodeAK { get; set; }
        public string helperNamaAK { get; set; }
        public bool helperIsKpei { get; set; }
        public bool helperEdit { get; set; }
        public AppUser AppUser { get; set; }
    }
}