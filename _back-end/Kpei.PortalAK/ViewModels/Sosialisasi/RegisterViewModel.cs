﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Forms;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Sosialisasi {
    public class RegisterViewModel {
        public Event Event { get; set; }
        public AppUser AppUser { get; set; }
        public List<CreateEventPesertaForm> ListPeserta { get; set; }
        public List<KpeiCompany> AllCompanies { get; set; }
    }
}