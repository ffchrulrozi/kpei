﻿using Kpei.PortalAK.DataAccess.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Sosialisasi {
    public class CalendarViewModel {
        public List<Event> Event { get; set; }
    }
}