﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.UserIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Sosialisasi {
    public class DetailViewModel {
        public Event Event { get; set; }
        public List<EventPembicara> ListPembicara { get; set; }
        public AppUser AppUser { get; set; }
        public List<EventPeserta> ListPeserta { get; set; }
    }
}