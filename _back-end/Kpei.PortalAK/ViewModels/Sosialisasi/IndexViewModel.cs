﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Models;
using Kpei.PortalAK.DataAccess.UserIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Sosialisasi {
    public class IndexViewModel {
        public AppUser AppUser { get; set; }
        public List<string> JenisPelatihan { get; set; }
        public List<EventExpirationStatus> EventExpiration => EventExpirationStatus.All().ToList();
    }
}