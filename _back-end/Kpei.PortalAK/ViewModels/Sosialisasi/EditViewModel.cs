﻿using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Forms;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Models;
using System.Collections.Generic;
using System.Linq;

namespace Kpei.PortalAK.ViewModels.Sosialisasi {
    public class EditViewModel {
        public UpdateEventForm Form { get; set; }
        public EventSettingForm Setting { get; set; }
        public List<EventStatus> Status => EventStatus.All().ToList();
    }
}