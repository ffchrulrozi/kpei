﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Forms;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Sosialisasi {
    public class InputNewViewModel {
        public CreateEventForm Form { get; set; }
        public EventSettingForm Setting { get; set; }
        public List<EventStatus> Status => EventStatus.All().ToList();
    }
}