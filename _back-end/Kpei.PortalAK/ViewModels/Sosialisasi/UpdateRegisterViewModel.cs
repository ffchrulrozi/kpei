﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Forms;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Sosialisasi {
    public class UpdateRegisterViewModel {
        public List<UpdateEventPesertaForm> ListPeserta { get; set; }
        public Event Event { get; set; }
        public AppUser AppUser { get; set; }
        public IEnumerable<KpeiCompany> AllCompanies { get; set; }
        public bool isKpei { get; set; }

        public void InitList(List<EventPeserta> listOldPeserta) {
            ListPeserta = new List<UpdateEventPesertaForm>();
            for (int i = 0; i < listOldPeserta.Count; i++) {
                ListPeserta.Add(new UpdateEventPesertaForm {
                    Id = listOldPeserta[i].Id,
                    EventId = listOldPeserta[i].EventId,
                    KodeAK = listOldPeserta[i].KodeAK,
                    Nama = listOldPeserta[i].Nama,
                    Jabatan = listOldPeserta[i].Jabatan,
                    Email = listOldPeserta[i].Email,
                    Telp = listOldPeserta[i].Telp,
                    Hadir = listOldPeserta[i].Hadir,
                });
            }
        }
    }
}