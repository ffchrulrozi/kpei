﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;

namespace Kpei.PortalAK.ViewModels {
    public class SettingEventViewModel {
        public IEnumerable<Setting> listJenisPelatihan;
        public Setting Reminder;
    }
}