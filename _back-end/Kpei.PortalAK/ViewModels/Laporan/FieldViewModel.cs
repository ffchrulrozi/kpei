﻿using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.ModLaporan.Forms;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.Features.DataAknpViewSystem;

namespace Kpei.PortalAK.ViewModels.Laporan {
    public class FieldViewModel {
        public object PropertyValue { get; set; }
        public BaseLaporan Data { get; set; }
        public DataAknpViewDefinition ViewDefinition { get; set; }
        public BaseLaporanDriver Driver { get; set; }
        public AppUser User { get; set; }
    }
}