﻿using Kpei.PortalAK.DataAccess.ModLaporan.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Laporan {
    public class ListDataViewModel {
        private KpeiCompany[] _companies;

        public KpeiCompany[] Companies {
            get {return _companies ?? new KpeiCompany[0];}
            set {_companies = value;}
        }
        
        public StatusKpei[] statusKpei {
            get { return StatusKpei.All().ToArray(); }
        }

        public BaseLaporanDriver Driver { get; set; }
        public AppUser User { get; set; }
    }
}