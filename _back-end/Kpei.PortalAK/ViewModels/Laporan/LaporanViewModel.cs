﻿using Kpei.PortalAK.DataAccess.ModLaporan.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.ViewModels.Laporan {
    public class LaporanViewModel {
        public BaseLaporanDriver Driver { get; set; }
        public AppUser AppUser { get; set; }
        public IEnumerable<KpeiCompany> Companies { get; set; }
        public IEnumerable<StatusKpei> statusKpei => StatusKpei.All().ToList();
    }
}