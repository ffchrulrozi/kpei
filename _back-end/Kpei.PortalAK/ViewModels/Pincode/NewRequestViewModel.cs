﻿using Kpei.PortalAK.DataAccess.ModPincode.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;

namespace Kpei.PortalAK.ViewModels.Pincode {
    public class NewRequestViewModel {
        public NewPincodeForm Form { get; set; }
        public PincodeTypeInfo[] PincodeTypeInfos { get; set; }
    }
}