﻿using Kpei.PortalAK.DataAccess.DbModels;

namespace Kpei.PortalAK.ViewModels.Pincode {
    public class LogViewModel {
        public PincodeRequest Pincode { get; set; }
    }
}