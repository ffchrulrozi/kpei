﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModPincode.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;

namespace Kpei.PortalAK.ViewModels.Pincode {
    public class SendNewPinViewModel {
        public PincodeTypeInfo[] TypeInfos { get; set; }
        public PincodeRequest Pincode { get; set; }
        public SendPincodeForm Form { get; set; }
    }
}