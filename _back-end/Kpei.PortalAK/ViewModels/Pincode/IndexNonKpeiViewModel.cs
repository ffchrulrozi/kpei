﻿using System.Collections.Generic;
using System.Linq;
using Kpei.PortalAK.DataAccess.ModPincode.Models;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.Pincode {
    public class IndexNonKpeiViewModel {
        public AppUser AppUser { get; set; }
        public List<PincodeRequestStatus> AllStatus => PincodeRequestStatus.All().ToList();
    }
}