﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.Pincode {
    public class DetailViewModel {
        public PincodeTypeInfo[] TypeInfos { get; set; }
        public PincodeRequest Pincode { get; set; }
        public AppUser User { get; set; }
    }
}