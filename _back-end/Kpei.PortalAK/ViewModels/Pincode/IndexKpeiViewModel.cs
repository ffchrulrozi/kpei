﻿using System.Collections.Generic;
using System.Linq;
using Kpei.PortalAK.DataAccess.ModPincode.Models;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels.Pincode {
    public class IndexKpeiViewModel {
        public List<PincodeRequestStatus> AllStatus => PincodeRequestStatus.All().ToList();
        public List<KpeiCompany> AllCompanies { get; set; }
        public AppUser AppUser { get; set; }
        public List<string> ListJenisPincode { get; set; }
    }
}