﻿using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.ViewModels {
    public class MclearsPesertaViewModel {
        public List<MclearsPeserta> Peserta { get; set; }
        public string statusRequest { get; set; }
        public string RequestId { get; set; }
        public string helperKodeAK { get; set; }
        public AppUser AppUser { get; set; }
    }
}