﻿using System;
using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSurvei.Repos;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.Features;
using System.Net;
using Kpei.PortalAK.DataAccess.ModSurvei.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using System.Transactions;
using Kpei.PortalAK.ViewModels.Survey;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using System.Linq;
using System.Collections.Generic;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;

namespace Kpei.PortalAK.Controllers {
    [Authorize]
    [RoutePrefix("survei")]
    public class SurveiController : Controller {
        private readonly IKpeiCompanyService _compsvc;
        private readonly ISurveiRepository _repo;
        private readonly IUserActionLogService _ulog;
        private readonly IUserService _usersvc;
        private readonly ISettingRepository _stgrepo;
        private AppUser AppUser => User.Identity.GetAppUser();

        public SurveiController(IKpeiCompanyService compsvc, ISurveiRepository repo, IUserActionLogService ulog, IUserService usersvc, ISettingRepository stgrepo) {
            _compsvc = compsvc;
            _repo = repo;
            _ulog = ulog;
            _usersvc = usersvc;
            _stgrepo = stgrepo;
        }

        [Route("")]
        [HttpGet]
        public ActionResult Index() {
            if (AppUser.IsKpei || AppUser.NonKpeiViewerAsKpei(CoreModules.SURVEI_KEANGGOTAAN)) {
                return IndexKpei();
            }
            if (AppUser.IsAknp) {
                return IndexNonKpei();
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        private ActionResult IndexKpei() {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SURVEI_KEANGGOTAAN) && !AppUser.NonKpeiViewerAsKpei(CoreModules.SURVEI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var mdl = new IndexViewModel {
                AppUser = AppUser
            };
            return View("IndexKpei", mdl);
        }

        private ActionResult IndexNonKpei() {
            if (!AppUser.NonKpeiViewerRightFor(CoreModules.SURVEI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var mdl = new IndexViewModel {
                AppUser = AppUser
            };
            return View("IndexNonKpei", mdl);
        }

        [Route("list-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListKpei(SurveiDttRequestForm req) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SURVEI_KEANGGOTAAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.SURVEI_KEANGGOTAAN) && 
                !AppUser.NonKpeiViewerAsKpei(CoreModules.SURVEI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var resp = new DttResponseForm {
                draw = req.draw
            };

            var result = _repo.List(req);
            resp.recordsTotal = result.RecordsTotal;
            resp.recordsFiltered = result.RecordsFiltered;
            resp.data = result.Data.Select(x => new {
                x.Nama, 
                TglPelaksanaan = x.TglPelaksanaanUtc?.ConvertUtcToWib().ToString("dd MMM yyyy"),
                x.JlhResponden,
                x.Nilai,
                DetailUrl = Url.Action("Detail", new { id = x.Id })
            }).Cast<object>().ToList();

            return Json(resp);
        }

        [Route("buat-baru")]
        [HttpGet]
        public ActionResult InputNew() {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SURVEI_KEANGGOTAAN) && 
                !AppUser.NonKpeiModifierRightFor(CoreModules.SURVEI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var mdl = new InputNewViewModel {
                Form = new CreateSurveiForm(),
                Setting = _stgrepo.SurveySetting()
            };
            return View(mdl);
        }

        [Route("buat-baru")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InputNew(CreateSurveiForm form) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SURVEI_KEANGGOTAAN) && !AppUser.NonKpeiModifierRightFor(CoreModules.SURVEI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            form = form ?? new CreateSurveiForm();
            //convert wib->utc
            form.TglPelaksanaanUtc?.ConvertWibToUtc();
            decimal maxSurveiValue = (decimal) 5.0;
            decimal minSurveiValue = (decimal)0.0;

            bool validSurveiValue = false;
            //further validation
            if (form.Nilai <= maxSurveiValue && form.Nilai >= minSurveiValue) {
                validSurveiValue = true;
            }

            if (ModelState.IsValid && validSurveiValue) {
                Survei newSurvei;
                SurveiPerusahaan newPerusahaan;
                SurveiDivisi newDivisi;

                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    newSurvei = _repo.Create(form);
                    for (int i = 0; i < form.ListPerusahaan.Count; i++) {
                        if (form.ListPerusahaan[i].Kategori == null && form.ListPerusahaan[i].HasilFileUrl == null && form.ListPerusahaan[i].TindakLanjutFileUrl == null) {
                            continue;
                        }
                        form.ListPerusahaan[i].SurveiId = newSurvei.Id;
                        newPerusahaan = _repo.CreatePerusahaan(form.ListPerusahaan[i]);
                    }
                    for (int i = 0; i < form.ListDivisi.Count; i++) {
                        if (form.ListDivisi[i].Kategori == null && form.ListDivisi[i].HasilFileUrl == null && form.ListDivisi[i].TindakLanjutFileUrl == null) {
                            continue;
                        }
                        form.ListDivisi[i].SurveiId = newSurvei.Id;
                        newDivisi = _repo.CreateDivisi(form.ListDivisi[i]);
                    }
                    scope.Complete();
                };

                _ulog.LogUserActionAsync(HttpContext, CoreModules.SURVEI_KEANGGOTAAN, "Menginput Survei Baru", "SurveiKeanggotaan", newSurvei.Id);

                _usersvc.SendNotifNewSurvei(Url.Action("Detail", "Survei", new { id = newSurvei.Id }, Request.Url.Scheme));

                return RedirectToAction("Detail", new { id = newSurvei.Id });
            } else {
                if (!(validSurveiValue)) {
                    ModelState.AddModelError("", "Nilai survei hanya boleh antara 0.0 sampai 5.0");
                }
            }
            return View("InputNew", new InputNewViewModel { Form = form, Setting = _stgrepo.SurveySetting() });
        }

        [Route("detil/{id}")]
        [HttpGet]
        public ActionResult Detail(string id) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SURVEI_KEANGGOTAAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.SURVEI_KEANGGOTAAN) && 
                !AppUser.NonKpeiViewerAsKpei(CoreModules.SURVEI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var survei = _repo.Get(id);
            if (survei == null) {
                return HttpNotFound();
            }
            var listPerusahaan = _repo.GetPerusahaanFromSurvei(id);
            var listDivisi = _repo.GetDivisiFromSurvei(id);

            var mdl = new DetilViewModel {
                Survei = survei,
                ListPerusahaan = listPerusahaan,
                ListDivisi = listDivisi,
                AppUser = AppUser
            };

            return View(mdl);
        }

        [Route("ubah/{id}")]
        [HttpGet]
        public ActionResult Edit(string id) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SURVEI_KEANGGOTAAN) && !AppUser.NonKpeiModifierRightFor(CoreModules.SURVEI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var survei = _repo.Get(id);
            if (survei == null) {
                return HttpNotFound();
            }
            var form = new UpdateSurveiForm();
            form.Init(survei);
            
            form.ListPerusahaan = new List<UpdateSurveiPerusahaanForm>();
            var listPerusahaan = _repo.GetPerusahaanFromSurvei(id);
            if (listPerusahaan != null) {
                //data
                for (int i = 0; i < listPerusahaan.Count; i++) {
                    form.ListPerusahaan.Add(new UpdateSurveiPerusahaanForm(listPerusahaan[i]));
                }
            }
            if (listPerusahaan.Count < 5) {
                //filler data
                for (int i = listPerusahaan.Count; i < 5; i++) {
                    form.ListPerusahaan.Add(new UpdateSurveiPerusahaanForm());
                }
            }

            form.ListDivisi = new List<UpdateSurveiDivisiForm>();
            var listDivisi = _repo.GetDivisiFromSurvei(id);
            if (listDivisi != null) {
                //data
                for (int i = 0; i < listDivisi.Count; i++) {
                    form.ListDivisi.Add(new UpdateSurveiDivisiForm(listDivisi[i]));
                }
            }
            if (listPerusahaan.Count < 5) {
                //filler data
                for (int i = listDivisi.Count; i < 5; i++) {
                    form.ListDivisi.Add(new UpdateSurveiDivisiForm());
                }
            }
            
            var mdl = new EditViewModel {
                Form = form,
                Setting = _stgrepo.SurveySetting()
            };

            return View(mdl);
        }

        [Route("ubah/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, UpdateSurveiForm form) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SURVEI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var survei = _repo.Get(id);
            if (survei == null || !AppUser.IsKpei) {
                return HttpNotFound();
            }

            if (form == null) {
                form = new UpdateSurveiForm();
                form.Init(survei);
            }

            //convert wib->utc
            form.TglPelaksanaanUtc?.ConvertWibToUtc();
            decimal maxSurveiValue = (decimal)5.0;
            decimal minSurveiValue = (decimal)0.0;

            //further validation
            bool validSurveiValue = false;

            if (form.Nilai <= maxSurveiValue && form.Nilai >= minSurveiValue) {
                validSurveiValue = true;
            }

            if (ModelState.IsValid && validSurveiValue) {
                Survei editedSurvei;
                List<SurveiPerusahaan> listEditedPerusahaan = new List<SurveiPerusahaan>();
                List<SurveiDivisi> listEditedDivisi = new List<SurveiDivisi>();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    editedSurvei = _repo.Update(form);
                    for (int i = 0; i < form.ListPerusahaan?.Count; i++) {
                        listEditedPerusahaan.Add(_repo.UpdatePerusahaan(form.ListPerusahaan[i]));
                    }
                    for (int i = 0; i < form.ListDivisi?.Count; i++) {
                        listEditedDivisi.Add(_repo.UpdateDivisi(form.ListDivisi[i]));
                    }
                    scope.Complete();
                }

                _ulog.LogUserActionAsync(HttpContext, CoreModules.SURVEI_KEANGGOTAAN, "Mengedit Survei", "SurveiKeanggotaan", editedSurvei.Id);

                _usersvc.SendNotifUpdateSurvei(Url.Action("Detail", "Survei", new { id = editedSurvei.Id }, Request.Url.Scheme));

                return RedirectToAction("Detail", new { id = id });
            } else {
                if (!(validSurveiValue)) {
                    ModelState.AddModelError("", "Nilai survei hanya boleh antara 0.0 sampai 5.0");
                }
            }
            return View(new EditViewModel { Form = form, Setting = new SurveySettingForm() });
        }
    }
}
