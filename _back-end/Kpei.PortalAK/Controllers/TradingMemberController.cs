﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Models;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.ModTradingMember.Forms;
using Kpei.PortalAK.DataAccess.ModTradingMember.Repos;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.ViewModels.TradingMember;

namespace Kpei.PortalAK.Controllers
{
    [RoutePrefix("data/trading-member")]
    [Authorize]

    public class TradingMemberController : Controller
    {
        private readonly IKpeiCompanyService _compsvc;
        private readonly ISettingRepository _settingRepository;
        private readonly ITradingMemberRepository _tradingMemberRepository;
        private readonly IUserActionLogService _userLogService;
        private readonly IUserService _usersvc;

        private AppUser AppUser => User.Identity.GetAppUser();
        private GlobalSettingForm GlobalSetting => _settingRepository.GlobalSetting();

        public TradingMemberController(IKpeiCompanyService compsvc,
            ISettingRepository settingRepository,
            ITradingMemberRepository tradingMemberRepository,
            IUserService usersvc,
            IUserActionLogService userLogService)
        {
            _compsvc = compsvc;
            _usersvc = usersvc;
            _tradingMemberRepository = tradingMemberRepository;
            _settingRepository = settingRepository;
            _userLogService = userLogService;
        }

        [HttpGet]
        [Route("")]
        public ActionResult Index()
        {
            if (!AppUser.KpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.ADMINISTRASI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            ListViewModel model = new ListViewModel
            {
                User = AppUser,
            };

            return View("List", model);
        }

        [Route("create")]
        [HttpGet]
        public ActionResult Create()
        {
            return View("InputForm");
        }

        [Route("delete/{id}")]
        [HttpGet]
        public ActionResult Delete(string id)
        {
            TempData["DeleteSuccess"] = _tradingMemberRepository.Delete(id);
            
            return RedirectToAction("Index");
        }

        [Route("save")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(InputViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (viewModel.TipePerusahaan == TradingMemberTipePerusahaan.AK.Name)
                {
                    foreach (var data in viewModel.DataTradingMemberAKI)
                    {
                        _tradingMemberRepository.Create(new TradingMemberForm
                        {
                            KodePerusahaan = data,
                            NamaPerusahaan = _compsvc.AllKpeiCompanies().FirstOrDefault(x => x.Code == data).Name,
                            TipePerusahaan = viewModel.TipePerusahaan,
                        });    
                    }
                }
                else
                {
                    foreach (var data in viewModel.DataTradingMember)
                    {
                        _tradingMemberRepository.Create(new TradingMemberForm
                        {
                            KodePerusahaan = data.KodePerusahaan,
                            NamaPerusahaan = data.NamaPerusahaan,
                            TipePerusahaan = viewModel.TipePerusahaan,
                        }); 
                    }
                }
                TempData["CreateSuccess"] = true;
                return RedirectToAction("Index");
            }

            return View("InputForm", viewModel);
        }


        [Route("list-dtt")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListDtt(ChangeRequestDttRequestForm req)
        {
            var resp = new DttResponseForm {
                draw = req.draw
            };
            var result = _tradingMemberRepository.ListDtt(req);
            resp.recordsFiltered = result.RecordsFiltered;
            resp.recordsTotal = result.RecordsTotal;
            resp.data = result.Data.Select(x => new {
                x.Id,
                x.KodePerusahaan,
                x.NamaPerusahaan,
                x.TipePerusahaan,
            }).Cast<object>().ToList();
            return Json(resp);
        }
    }
}