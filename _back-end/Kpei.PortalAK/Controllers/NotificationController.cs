﻿using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Drivers;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModKeuangan.Models;
using Kpei.PortalAK.DataAccess.ModMclears.Models;
using Kpei.PortalAK.DataAccess.ModNotification.Models;
using Kpei.PortalAK.DataAccess.ModPincode.Models;
using Kpei.PortalAK.DataAccess.UserIdentity;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Kpei.PortalAK.Controllers {
    [RoutePrefix("notif")]
    public class NotificationController : Controller {
        private readonly AppDbContext _db;
        private readonly IUserService _usrSvc;
        private string akteSlug, alamatSlug, bankSlug, contactSlug, usahaSlug, informasiSlug, pejabatSlug, memberSlug, regSlug;
        private string pincodeKey, mclearsKey, eventKey, keuKey, surveyKey;

        public NotificationController(AppDbContext db, IUserService usrSvc) {
            _db = db;
            akteSlug = DataAkteAknpDriver.SLUG;
            alamatSlug = DataAlamatAknpDriver.SLUG;
            bankSlug = DataBankPembayaranAknpDriver.SLUG;
            contactSlug = DataContactPersonAknpDriver.SLUG;
            usahaSlug = DataJenisUsahaAknpDriver.SLUG;
            informasiSlug = DataInformasiLainAknpDriver.SLUG;
            pejabatSlug = DataPejabatBerwenangAknpDriver.SLUG;
            memberSlug = DataTipeMemberAknpDriver.SLUG;
            regSlug = DataRegistrasiAknpDriver.SLUG;

            pincodeKey = "ResetPincode";
            mclearsKey = "Mclears";
            eventKey = "Event";
            keuKey = "LaporKeu";
            surveyKey = "Survei";
            _usrSvc = usrSvc;
        }

        [HttpGet]
        [Route("")]
        public ActionResult Index(string userId) {
            if (!string.IsNullOrWhiteSpace(userId)) {
                // var oldLog = _db.Notification.Where(e => e.UserId == userId).OrderByDescending(e => e.CreatedUtc).FirstOrDefault();
                var count = _db.Database.SqlQuery<int>($"exec dbo.sp_PullNotification @userId = '{userId}'").First();
                return Json(new NotificationCountJsonObject { NotificationCount = count }, JsonRequestBehavior.AllowGet);
            }
            return Json("Unauthorized user", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("drawer")]
        public ActionResult Drawer(string userId) {
            if (!string.IsNullOrWhiteSpace(userId)) {
                var oldLog = _db.Notification.Where(e => e.UserId == userId).OrderByDescending(e => e.UpdatedUtc).FirstOrDefault();

                #region getData

                var obj = new NotificationObject {
                    Notifications = new List<BaseNotification>()
                };

                var dataFromDb = _db.Database.SqlQuery<ImportBaseData>($"exec dbo.sp_PullNotificationDrawer @userId = '{userId}'").ToList();
                
                #endregion

                #region generateNotification
                
                obj.Notifications.AddRange(GenerateNotification(dataFromDb, oldLog));

                #endregion
                if (obj.Notifications.Where(e => e.Seen == false).Count() > 0) {
                    CreateNotificationLog(userId);
                }

                return Json(obj.Notifications.OrderByDescending(e => e.Time), JsonRequestBehavior.AllowGet);
            }

            return Json("Unauthorized user", JsonRequestBehavior.AllowGet);
        }

        private void CreateNotificationLog(string userId) {
            if (!string.IsNullOrWhiteSpace(userId)) {
                var log = new Notifications {
                    UserId = userId
                };
                _db.Notification.Add(log);
                _db.SaveChanges();
            }
        }

        private List<BaseNotification> GenerateNotification(IEnumerable<ImportBaseData> list, Notifications oldLog, bool newStatus = true) {
            var obj = new List<BaseNotification>();
            list = list.OrderByDescending(e => e.UpdatedUtc).ToList();
            var latestLog = oldLog;
            foreach (var data in list) {
                if (data.dataSlug == akteSlug ||
                data.dataSlug == alamatSlug ||
                data.dataSlug == bankSlug ||
                data.dataSlug == contactSlug ||
                data.dataSlug == usahaSlug ||
                data.dataSlug == informasiSlug ||
                data.dataSlug == pejabatSlug ||
                data.dataSlug == memberSlug ||
                data.dataSlug == regSlug
                ) {
                    if (data.Status == DataAknpRequestStatus.NEW_STATUS.Name && data.CreatedUtc != data.UpdatedUtc) {
                        data.Status = "Edit";
                    }
                    if (data.dataSlug != regSlug) {
                        obj.Add(new AknpNotification {
                            Author = data.Status == DataAknpRequestStatus.ONGOING_STATUS.Name ? "" : data.KodeAK,
                            Link = Url.Action("RequestChangeDetail", "DataAknp", new { dataSlug = data.dataSlug, id = data.Id }),
                            Time = data.UpdatedUtc?.AddHours(7),
                            Status = data.Status,
                            Seen = latestLog != null ? data.UpdatedUtc > latestLog?.CreatedUtc ? false : true : false
                        });
                    } else {
                        obj.Add(new AknpNotification {
                            Author = data.Status == DataAknpRequestStatus.ONGOING_STATUS.Name ? "" : data.KodeAK,
                            Link = Url.Action("RequestChangeDetail", "DataAknp", new { dataSlug = data.dataSlug, id = data.Id }),
                            Time = data.UpdatedUtc?.AddHours(7),
                            Status = data.Status,
                            Icon = "fa fa-star",
                            Seen = latestLog != null ? data.UpdatedUtc > latestLog?.CreatedUtc ? false : true : false
                        });
                    }
                } else if (data.dataSlug == pincodeKey) {
                    if (data.Status == PincodeRequestStatus.NEW_STATUS.Name && data.CreatedUtc != data.UpdatedUtc) {
                        data.Status = "Edit";
                    } else if (data.Status == PincodeRequestStatus.NEW_STATUS.Name) {
                        data.Status = DataAknpRequestStatus.NEW_STATUS.Name;
                    }
                    obj.Add(new PincodeNotification {
                        Author = data.KodeAK,
                        Link = Url.Action("Detail", data.dataSlug, new { id = data.Id }),
                        Time = data.UpdatedUtc?.AddHours(7),
                        Status = data.Status,
                        Seen = latestLog != null ? data.UpdatedUtc > latestLog?.CreatedUtc ? false : true : false
                    });
                } else if (data.dataSlug == mclearsKey) {
                    if (data.Status == MclearsStatusRequest.NEW_STATUS.Name && data.CreatedUtc != data.UpdatedUtc) {
                        data.Status = "Edit";
                    } else if (data.Status == MclearsStatusRequest.NEW_STATUS.Name) {
                        data.Status = DataAknpRequestStatus.NEW_STATUS.Name;
                    } else if (data.Status == MclearsStatusRequest.DONE_STATUS.Name) {
                        data.Status = DataAknpRequestStatus.APPROVED_STATUS.Name;
                    }
                    obj.Add(new MclearsNotification {
                        Author = data.KodeAK,
                        Link = Url.Action("Detail", data.dataSlug, new { id = data.Id }),
                        Time = data.UpdatedUtc?.AddHours(7),
                        Status = data.Status,
                        Seen = latestLog != null ? data.UpdatedUtc > latestLog?.CreatedUtc ? false : true : false
                    });
                } else if (data.dataSlug == keuKey) {
                    if (data.Status == LaporanKeuanganStatus.NEW_STATUS.Name && data.CreatedUtc != data.UpdatedUtc) {
                        data.Status = "Edit";
                    } else if (data.Status == LaporanKeuanganStatus.NEW_STATUS.Name) {
                        data.Status = DataAknpRequestStatus.NEW_STATUS.Name;
                    } else if (data.Status == LaporanKeuanganStatus.ONGOING_STATUS.Name) {
                        data.Status = DataAknpRequestStatus.ONGOING_STATUS.Name;
                    } else if (data.Status == LaporanKeuanganStatus.APPROVED_STATUS.Name) {
                        data.Status = DataAknpRequestStatus.APPROVED_STATUS.Name;
                    } else if (data.Status == LaporanKeuanganStatus.REJECTED_STATUS.Name) {
                        data.Status = DataAknpRequestStatus.REJECTED_STATUS.Name;
                    }
                    obj.Add(new KeuanganNotification {
                        Author = data.Status == DataAknpRequestStatus.ONGOING_STATUS.Name ? "" : data.KodeAK,
                        Link = Url.Action("Detail", data.dataSlug, new { id = data.Id }),
                        Time = data.UpdatedUtc?.AddHours(7),
                        Status = data.Status,
                        Seen = latestLog != null ? data.UpdatedUtc > latestLog?.CreatedUtc ? false : true : false
                    });
                }

            }

            return obj;
        }
    }
}