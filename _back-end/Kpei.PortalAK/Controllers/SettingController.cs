﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.ViewModels;
using System.Transactions;
using System;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;

namespace Kpei.PortalAK.Controllers {
    [Authorize]
    public class SettingController : Controller {
        private List<Setting> _listJenisPelatihan;
        private List<Setting> _listKategoriDivisi;
        private List<Setting> _listKategoriPerusahaan;

        private readonly AppDbContext _db;
        private readonly ISettingRepository _repository;
        private readonly IUserActionLogService _ulog;

        private AppUser AppUser => AppUser.CreateFromIdentity(User.Identity);

        public SettingController(ISettingRepository repo, AppDbContext db, IUserActionLogService ulog) {
            _repository = repo;
            _db = db;
            _ulog = ulog;
        }

        #region !!! These belong to the setting repository, not in the controller !!!

        private void PopulateSurveiSetting() {
            _listKategoriPerusahaan = _repository.GetAllSettingByField(SettingFields.SURVEI_PERUSAHAAN).OrderBy(e => e.CreatedUtc).ToList();
            _listKategoriDivisi = _repository.GetAllSettingByField(SettingFields.SURVEI_DIVISI).OrderBy(e => e.CreatedUtc).ToList();
        }

        private void PopulateEventSetting() {
            _listJenisPelatihan = _repository.GetAllSettingByField(SettingFields.EVENT_JENIS_PELATIHAN).OrderBy(e => e.CreatedUtc).ToList();
        }

        private void AddSetting(Setting setting, string field) {
            if (!string.IsNullOrWhiteSpace(setting.Value?.Trim())) {
                setting.Field = field;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    _repository.AddSetting(setting);
                    scope.Complete();
                }
            }

            // magic to scroll last modified table into view
            TempData["__LastFormAction"] = Request.Form["__LastFormAction"] ?? "";
        }

        private void TrashSetting(Setting setting) {
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = IsolationLevel.Serializable,
                Timeout = TimeSpan.FromMinutes(3)
            })) {
                _repository.DeleteSetting(setting.Id);
                scope.Complete();
            }

            // magic to scroll last modified table into view
            TempData["__LastFormAction"] = Request.Form["__LastFormAction"] ?? "";
        }

        #endregion

        #region Survei Kepuasan Pelanggan

        [Route("settings/survei")]
        public ActionResult Survei() {
            if (!AppUser.KpeiAdminRightFor(CoreModules.SURVEI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (_listKategoriPerusahaan == null && _listKategoriDivisi == null) {
                PopulateSurveiSetting();
            }

            var viewModel = new SettingSurveiViewModel {
                KategoriPerusahaan = _listKategoriPerusahaan,
                KategoriDivisi = _listKategoriDivisi
            };
            return View(viewModel);
        }

        [Route("settings/survei/add-kategori-perusahaan")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddKategoriPerusahaan(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.SURVEI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.SURVEI_PERUSAHAAN);
            return RedirectToAction("Survei");
        }

        [Route("settings/survei/add-kategori-divisi")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddKategoriDivisi(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.SURVEI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.SURVEI_DIVISI);
            return RedirectToAction("Survei");
        }

        [Route("settings/survei/trash")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TrashKategori(Setting change) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.SURVEI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            TrashSetting(change);
            return RedirectToAction("Survei");
        }

        #endregion

        #region Laporan Keuangan

        [Route("settings/keuangan")]
        [HttpGet]
        public ActionResult Keuangan() {
            if (!AppUser.KpeiAdminRightFor(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var viewModel = new SettingKeuanganViewModel {
                Setting = _repository.DeadlineSetting()
            };
            return View(viewModel);
        }

        [Route("settings/keuangan")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Keuangan(DeadlineSettingForm form) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (ModelState.IsValid) {
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(1)
                })) {
                    _repository.UpdateDeadlineSetting(form);
                    scope.Complete();
                }
            }

            return RedirectToAction("Keuangan");
        }

        #endregion

        #region Event / Sosialisasi dan Pelatihan

        [Route("settings/sospel")]
        [HttpGet]
        public ActionResult Event() {
            if (!AppUser.KpeiAdminRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var stg = _repository.EventSetting();
            if (_listJenisPelatihan == null) {
                _listJenisPelatihan = stg.ListJenisPelatihan;
            }

            var viewModel = new SettingEventViewModel {
                listJenisPelatihan = _listJenisPelatihan,
                Reminder = stg.Reminder
            };
            return View(viewModel);
        }

        [Route("settings/sospel")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Event(Setting reminder) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (reminder != null) {
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    if (_repository.Get(reminder.Id) == null) {
                        var newStg = _repository.AddSetting(reminder);
                    } else {
                        var updatedStg = _repository.UpdateSetting(reminder);
                    }
                    scope.Complete();
                }
            }
            return RedirectToAction("Event");
        }

        [Route("settings/sospel/add-jenis-pelatihan")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddJenisPelatihan(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.EVENT_JENIS_PELATIHAN);
            return RedirectToAction("Event");
        }

        [Route("settings/sospel/trash")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TrashJenisPelatihan(Setting change) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            TrashSetting(change);
            return RedirectToAction("Event");
        }

        #endregion

        #region Data AK & P

        [Route("settings/aknp")]
        public ActionResult Aknp() {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var viewModel = new SettingAknpViewModel {
                Form = _repository.AknpSetting()
            };
            return View(viewModel);
        }

        #region akte

        [Route("settings/aknp/add-notaris")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddNotaris(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_AKTE_NOTARIS);
            return RedirectToAction("Aknp");
        }

        #endregion

        #region bank-pembayaran

        [Route("settings/aknp/add-bank-pembayaran")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddBankPembayaran(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_BANK_PEMBAYARAN);
            return RedirectToAction("Aknp");
        }

        #endregion

        #region perjanjian-ebu

        [Route("settings/aknp/add-jenis-perjanjian-ebu")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddJenisPerjanjianEbu(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_JENIS_PERJANJIAN_EBU);
            return RedirectToAction("Aknp");
        }

        [Route("settings/aknp/add-status-perjanjian-ebu")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddStatusPerjanjianEbu(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_STATUS_PERJANJIAN_EBU);
            return RedirectToAction("Aknp");
        }

        #endregion

        #region perjanjian-kbos

        [Route("settings/aknp/add-jenis-perjanjian-kbos")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddJenisPerjanjianKbos(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_JENIS_PERJANJIAN_KBOS);
            return RedirectToAction("Aknp");
        }

        [Route("settings/aknp/add-status-perjanjian-kbos")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddStatusPerjanjianKbos(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_STATUS_PERJANJIAN_KBOS);
            return RedirectToAction("Aknp");
        }

        #endregion

        #region Perjanjian

        [Route("settings/aknp/add-jenis-perjanjian")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddJenisPerjanjian(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_JENIS_PERJANJIAN);
            return RedirectToAction("Aknp");
        }

        #endregion

        #region Informasi lain

        [Route("settings/aknp/add-pemantau")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddPemantau(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_PEMANTAU_INFORMASI_LAIN);
            return RedirectToAction("Aknp");
        }

        [Route("settings/aknp/add-anggota-bursa")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddAnggotaBursa(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_ANGGOTA_BURSA_INFORMASI_LAIN);
            return RedirectToAction("Aknp");
        }

        #endregion

        #region pme

        [Route("settings/aknp/add-jenis-keanggotaan-pme")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddJenisKeanggotaanPme(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_JENIS_KEANGGOTAAN_PME);
            return RedirectToAction("Aknp");
        }

        [Route("settings/aknp/add-jenis-perjanjian-lender-pme")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddJenisPerjanjianLenderPme(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_JENIS_PERJANJIAN_LENDER_PME);
            return RedirectToAction("Aknp");
        }

        [Route("settings/aknp/add-status-lender-pme")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddStatusLenderPme(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_STATUS_LENDER_PME);
            return RedirectToAction("Aknp");
        }

        [Route("settings/aknp/add-jenis-perjanjian-borrower-pme")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddJenisPerjanjianBorrowerPme(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_JENIS_PERJANJIAN_BORROWER_PME);
            return RedirectToAction("Aknp");
        }

        [Route("settings/aknp/add-status-borrower-pme")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddStatusBorrowerPme(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_STATUS_BORROWER_PME);
            return RedirectToAction("Aknp");
        }

        #endregion

        #region jabatan komisaris

        [Route("settings/aknp/add-jabatan-komisaris")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddJabatanKomisaris(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_JABATAN_KOMISARIS);
            return RedirectToAction("Aknp");
        }

        #endregion

        #region jabatan direksi

        [Route("settings/aknp/add-jabatan-direksi")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddJabatanDireksi(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_JABATAN_DIREKSI);
            return RedirectToAction("Aknp");
        }

        #endregion

        #region jabatan contact person

        [Route("settings/aknp/add-jabatan-contact-person")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddJabatanContactPerson(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_JABATAN_CONTACT_PERSON);
            return RedirectToAction("Aknp");
        }

        #endregion

        #region jabatan pejabat berwenang

        [Route("settings/aknp/add-jabatan-pejabat-berwenang")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddJabatanPejabatBerwenang(Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_JABATAN_PEJABAT_BERWENANG);
            return RedirectToAction("Aknp");
        }

        #endregion

        #region Tipe Member

        [Route("settings/aknp/add-tipe-member-partisipan")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddTipeMemberPartisipan (Setting setting) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AddSetting(setting, SettingFields.AKNP_SETTING_TIPE_MEMBER_PARTISIPAN);
            return RedirectToAction("Aknp");
        }

        #endregion

        [Route("settings/aknp/trash")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TrashAknpSetting(Setting change) {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var field = change.Field;
            var value = change.Value;
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = IsolationLevel.Serializable,
                Timeout = TimeSpan.FromMinutes(3)
            })) {
                TrashSetting(change);
                scope.Complete();
            }
            _ulog.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.CORE_SYSTEM, $"Menghapus nilai {value} dari {field}.", "Core");
            return RedirectToAction("Aknp");
        }

        #endregion



    }
}