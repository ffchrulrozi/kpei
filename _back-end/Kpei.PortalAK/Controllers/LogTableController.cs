﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using System.Net;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using Kpei.PortalAK.Features;
using Kpei.PortalAK.ViewModels.LogTable;

namespace Kpei.PortalAK.Controllers
{
    [Authorize]
    [RoutePrefix("log")]
    public class LogTableController : Controller
    {
        private readonly IKpeiCompanyService _compsvc;
        private readonly IUserActionLogService _ulog;

        public LogTableController(IUserActionLogService ulog, IKpeiCompanyService compsvc) {
            _ulog = ulog;
            _compsvc = compsvc;
        }

        private AppUser AppUser => User.Identity.GetAppUser();

        // GET: LogTable
        [Route("table")]
        [HttpGet]
        public ActionResult Index()
        {
            if (!AppUser.IsKpei) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var mdl = new LogViewModel {
                Companies = _compsvc.AllKpeiCompanies()
            };
            return View(mdl);
        }

        [Route("list-log")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogDtt(UserActionLogDttRequestForm req) {
            if (!AppUser.IsKpei) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var pag = _ulog.ListLogsAsync(req).Result;
            var resp = new DttResponseForm {
                draw = req.draw,
                recordsTotal = pag.RecordsTotal,
                recordsFiltered = pag.RecordsFiltered,
                data = pag.Data.Select(x => new {
                    WaktuPerubahan = x.CreatedUtc.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss"),
                    DibuatOleh = $"{x.UserName} / {x.UserAknpCode}",
                    Aktivitas = x.Message
                }).Cast<object>().ToList()
            };

            return Json(resp);
        }
    }
}