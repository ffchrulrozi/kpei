﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace Kpei.PortalAK.Controllers {
    [RoutePrefix("test-sandbox")]
    [Authorize]
    public class TestSandboxController : Controller {
        [Route("")]
        [HttpGet]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Index() {
            return View();
        }

        [Route("custom-fields")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CustomFields(SandboxCustomFieldsForm form) {
            if (ModelState.IsValid) {
                return Json(new {
                    Input = form
                });
            }

            return Json(new {
                Input = form,
                Errors = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray()
            });
        }
    }

    public class SandboxCustomFieldsForm {
        public string SampleFileUrl { get; set; }
        public DateTime? SampleDateTime { get; set; }
        public DateTime? SampleDateOnly { get; set; }
        public DateTime? SampleTimeOnly { get; set; }
    }
}