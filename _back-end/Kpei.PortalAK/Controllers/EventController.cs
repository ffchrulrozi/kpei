﻿using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Forms;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Models;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Repos;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.Features;
using Kpei.PortalAK.ViewModels.Sosialisasi;
using Simplexcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web.Mvc;

namespace Kpei.PortalAK.Controllers {
    [Authorize]
    [RoutePrefix("sosialisasi")]
    public class EventController : Controller {
        private readonly IKpeiCompanyService _compsvc;
        private readonly IEventRepository _repo;
        private readonly IUserActionLogService _ulog;
        private readonly IUserService _usersvc;
        private readonly ISettingRepository _stgrepo;
        private AppUser AppUser => User.Identity.GetAppUser();

        public EventController(IKpeiCompanyService compsvc, IEventRepository repo, IUserActionLogService ulog, IUserService usersvc, ISettingRepository stgrepo) {
            _compsvc = compsvc;
            _repo = repo;
            _ulog = ulog;
            _usersvc = usersvc;
            _stgrepo = stgrepo;
        }

        [Route("")]
        [HttpGet]
        public ActionResult Index() {
            if (AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return IndexKpei();
            }
            if (AppUser.IsAknp) {
                return IndexNonKpei();
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        private ActionResult IndexKpei() {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.SOSIALISASI_PELATIHAN)) { 
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var mdl = new IndexViewModel {
                AppUser = AppUser
            };
            return View("IndexKpei", mdl);
        }

        private ActionResult IndexNonKpei() {
            if (!AppUser.NonKpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var mdl = new IndexViewModel {
                AppUser = AppUser
            };
            return View("IndexNonKpei", mdl);
        }

        [Route("list-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListKpei(EventDttRequestForm req) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiViewerAsKpei(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var resp = new DttResponseForm {
                draw = req.draw
            };

            var result = _repo.List(req);
            var listPeserta = _repo.GetAllPeserta();
            resp.recordsFiltered = result.RecordsFiltered;
            resp.recordsTotal = result.RecordsTotal;
            resp.data = result.Data.Select(x => new {
                x.Judul,
                TglPelaksanaan = x.TglPelaksanaanStartUtc?.ConvertUtcToWib().ToString("dd MMM yyyy"),
                x.JenisPelatihan,
                x.KapasitasTotal,
                StatusExpiration = listPeserta.Where(e => e.EventId == x.Id).Count() >= x.KapasitasTotal ? x.StatusExpiration + " (kuota sudah penuh)" : x.StatusExpiration,
                DetailUrl = Url.Action("Detail", new { id = x.Id })
            }).Cast<object>().ToList();

            return Json(resp);
        }

        [Route("list-non-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListNonKpei(EventDttRequestForm req) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) &&
                !AppUser.NonKpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            req.Status = EventStatus.PUBLISHED_STATUS.Name;

            var resp = new DttResponseForm {
                draw = req.draw
            };

            var result = _repo.List(req);
            var listPeserta = _repo.GetAllPeserta();
            resp.recordsFiltered = result.RecordsFiltered;
            resp.recordsTotal = result.RecordsTotal;
            resp.data = result.Data.Select(x => new {
                x.Judul,
                TglPelaksanaan = x.TglPelaksanaanStartUtc?.ConvertUtcToWib().ToString("dd MMM yyyy"),
                x.JenisPelatihan,
                x.KapasitasTotal,
                StatusExpiration = listPeserta.Where(e => e.KodeAK == AppUser.KpeiCompanyCode && e.EventId == x.Id).Count() >= x.KapasitasPerAK ? x.StatusExpiration + " (kuota anda sudah penuh)" : listPeserta.Where(e => e.EventId == x.Id).Count() >= x.KapasitasTotal ? x.StatusExpiration + " (kuota sudah penuh)" : x.StatusExpiration,
                DetailUrl = Url.Action("Detail", new { id = x.Id })
            }).Cast<object>().ToList();

            return Json(resp);
        }

        [Route("buat-baru")]
        [HttpGet]
        public ActionResult InputNew() {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var mdl = new InputNewViewModel {
                Form = new CreateEventForm(),
                Setting = _stgrepo.EventSetting()
            };

            return View(mdl);
        }

        [Route("buat-baru")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InputNew(CreateEventForm form) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN) && !AppUser.NonKpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            form = form ?? new CreateEventForm();

            //convert wib->utc
            form.TglDaftarBukaUtc = form.TglDaftarBukaUtc?.ConvertWibToUtc();
            form.TglDaftarTutupUtc = form.TglDaftarTutupUtc?.ConvertWibToUtc();
            form.TglPelaksanaanStartUtc = form.TglPelaksanaanStartUtc?.ConvertWibToUtc();
            form.TglPelaksanaanEndUtc = form.TglPelaksanaanEndUtc?.ConvertWibToUtc();

            if (ModelState.IsValid) {

                Event newEvent;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    newEvent = _repo.Create(form);
                    for (int i = 0; i < form.ListPembicara?.Count; i++) {
                        if (form.ListPembicara[i].Nama == null && form.ListPembicara[i].Jabatan == null && form.ListPembicara[i].Email == null) {
                            continue;
                        }
                        form.ListPembicara[i].EventId = newEvent.Id;
                        _repo.CreatePembicara(form.ListPembicara[i]);
                    }
                    scope.Complete();
                }
                _ulog.LogUserActionAsync(HttpContext, CoreModules.SOSIALISASI_PELATIHAN, "Menginput Acara Baru", "SosialisasiPelatihan", newEvent.Id);

                _usersvc.SendNotifNewSospel(Url.Action("Detail", "Event", new { id = newEvent.Id }, Request.Url.Scheme));

                return RedirectToAction("Detail", new { id = newEvent.Id });
            }
            var mdl = new InputNewViewModel {
                Form = form,
                Setting = _stgrepo.EventSetting()
            };
            return View("InputNew", mdl);
        }

        [Route("detil/{id}")]
        [HttpGet]
        public ActionResult Detail(string id) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var sospel = _repo.Get(id);
            if (sospel == null) {
                return HttpNotFound();
            }
            var listPembicara = _repo.GetPembicaraFromEvent(id);
            var listPeserta = _repo.GetPesertaFromEvent(id).ToList();
            var mdl = new DetailViewModel {
                Event = sospel,
                ListPembicara = listPembicara,
                AppUser = AppUser,
                ListPeserta = listPeserta
            };
            return View(mdl);
        }

        [Route("ubah/{id}")]
        [HttpGet]
        public ActionResult Edit(string id) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var sospel = _repo.Get(id);
            if (sospel == null) {
                return HttpNotFound();
            }

            var form = new UpdateEventForm();
            form.Init(sospel);

            var listPembicara = _repo.GetPembicaraFromEvent(id);
            form.ListPembicara = new List<UpdateEventPembicaraForm>();

            if (listPembicara?.Count > 0) {
                for (int i = 0; i < listPembicara.Count; i++) {
                    form.ListPembicara.Add(new UpdateEventPembicaraForm(listPembicara[i]));
                }
            }

            if (listPembicara?.Count < 5 || listPembicara == null) {
                var minLength = listPembicara.Count > 0 ? listPembicara.Count : 1;
                for (int i = minLength; i <= 5; i++) {
                    form.ListPembicara.Add(new UpdateEventPembicaraForm());
                }
            }

            var mdl = new EditViewModel {
                Form = form,
                Setting = _stgrepo.EventSetting()
            };

            return View(mdl);
        }

        [Route("ubah/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, UpdateEventForm form) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var sospel = _repo.Get(id);
            if (sospel == null && !AppUser.IsKpei) {
                return HttpNotFound();
            }
            if (form == null) {
                form = new UpdateEventForm();
                form.Init(sospel);
            }

            //convert wib->utc
            form.TglDaftarBukaUtc = form.TglDaftarBukaUtc?.ConvertWibToUtc();
            form.TglDaftarTutupUtc = form.TglDaftarTutupUtc?.ConvertWibToUtc();
            form.TglPelaksanaanStartUtc = form.TglPelaksanaanStartUtc?.ConvertWibToUtc();
            form.TglPelaksanaanEndUtc = form.TglPelaksanaanEndUtc?.ConvertWibToUtc();

            if (ModelState.IsValid) {

                Event editedEvent;
                List<EventPembicara> listEditedPembicara = new List<EventPembicara>();
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    editedEvent = _repo.Update(form);
                    for (int i = 0; i < form.ListPembicara.Count; i++) {
                        listEditedPembicara.Add(_repo.UpdatePembicara(form.ListPembicara[i]));
                    }
                    scope.Complete();
                }

                _ulog.LogUserActionAsync(HttpContext, CoreModules.SOSIALISASI_PELATIHAN, "Meng-update acara baru", "SosialisasiPelatihan", editedEvent.Id);

                var peserta = _repo.GetPesertaFromEvent(form.Id).Select(x => x.KodeAK).Distinct().ToList();
                if (peserta?.Count > 0) {
                    _usersvc.SendNotifUpdateSospel(peserta, Url.Action("Detail", "Event", new { id = form.Id}, Request.Url.Scheme));
                }

                return RedirectToAction("Detail", new { id = id });
            }

            var mdl = new EditViewModel {
                Form = form,
                Setting = _stgrepo.EventSetting()
            };
            return View(mdl);
        }

        [Route("hapus/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var dataEvent = _repo.Get(id);
            if (dataEvent == null) {
                return HttpNotFound();
            }

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = IsolationLevel.Serializable,
                Timeout = TimeSpan.FromMinutes(3)
            })) {
                _repo.Delete(id);
                scope.Complete();
            }

            return RedirectToAction("Index");
        }

        [Route("daftar-kehadiran/{id}")]
        [HttpGet]
        public ActionResult DaftarKehadiran(string id) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var dataEvent = _repo.Get(id);
            if (dataEvent == null) {
                return HttpNotFound();
            }

            List<EventPeserta> listPeserta = _repo.GetPesertaFromEvent(id).ToList();
            var listPembicara = _repo.GetPembicaraFromEvent(id);

            var mdl = new DaftarHadirViewModel {
                Event = dataEvent,
                AppUser = AppUser
            };
            return View("DetailDaftarKehadiran", mdl);
        }

        [Route("list-hadir-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListHadirKpei(PesertaDttRequestForm req, string id) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiViewerAsKpei(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            req.EventId = id;

            var resp = new DttResponseForm {
                draw = req.draw
            };

            var result = _repo.ListPeserta(req);
            resp.recordsFiltered = result.RecordsFiltered;
            resp.recordsTotal = result.RecordsTotal;
            resp.data = result.Data.Select(x => new KehadiranPeserta(x, _compsvc.AllKpeiCompanies().FirstOrDefault(e => e.Code == x.KodeAK).Name)).Cast<object>().ToList();

            return Json(resp);
        }

        [Route("list-hadir-non-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListHadirNonKpei(PesertaDttRequestForm req, string id) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) && !AppUser.NonKpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            req.EventId = id;
            req.KodeAK = AppUser.KpeiCompanyCode;

            var resp = new DttResponseForm {
                draw = req.draw
            };

            var result = _repo.ListPeserta(req);
            resp.recordsFiltered = result.RecordsFiltered;
            resp.recordsTotal = result.RecordsTotal;
            resp.data = result.Data.Select(x => new KehadiranPeserta(x)).Cast<object>().ToList();

            return Json(resp);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("toggle")]
        public ActionResult Toggle(PesertaHadirToggleForm mdl) {

            if (AppUser.IsKpei && 
                AppUser.KpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                var result = _repo.ChangeHadir(mdl.PesertaId, mdl.Hadir);
                return Json(new {
                    Attendance = result
                });
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        [Route("registrasi/{id}")]
        [HttpGet]
        public ActionResult Register(string id) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            
            var dataEvent = _repo.Get(id);
            if (dataEvent == null) {
                return HttpNotFound();
            }

            var utcNow = DateTime.UtcNow;

            if (!(utcNow >= dataEvent.TglDaftarBukaUtc && utcNow <= dataEvent.TglDaftarTutupUtc)
                || dataEvent.StatusEvent != EventStatus.PUBLISHED_STATUS.Name) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            
            if (AppUser.IsAknp) {
                return RegisterNonKpei(dataEvent);
            }
            if (AppUser.IsKpei) {
                return RegisterKpei(dataEvent);
            }

            return HttpNotFound();
        }

        private ActionResult RegisterNonKpei(Event dataEvent) {
            List<CreateEventPesertaForm> listPeserta = new List<CreateEventPesertaForm>();
            var regPeserta = _repo.GetPesertaFromEvent(dataEvent.Id).Where(e => e.KodeAK == AppUser.KpeiCompanyCode);
            for (int i = 0; i < (dataEvent.KapasitasPerAK - regPeserta.Count()); i++) {
                listPeserta.Add(new CreateEventPesertaForm());
            }

            var mdl = new RegisterViewModel {
                Event = dataEvent,
                AppUser = AppUser,
                ListPeserta = listPeserta
            };

            return View("RegisterNonKpei", mdl);
        }

        private ActionResult RegisterKpei(Event dataEvent) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            List<CreateEventPesertaForm> listPeserta = new List<CreateEventPesertaForm>();
            for (int i = 0; i < 5; i++) {
                listPeserta.Add(new CreateEventPesertaForm());
            }

            var mdl = new RegisterViewModel {
                Event = dataEvent,
                AppUser = AppUser,
                ListPeserta = listPeserta,
                AllCompanies = _compsvc.AllKpeiCompanies().ToList()
            };

            return View("RegisterKpei", mdl);
        }

        [Route("registrasi/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(string id, List<CreateEventPesertaForm> listPeserta) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var dataEvent = _repo.Get(id);
            if (dataEvent == null) {
                return HttpNotFound();
            }

            var utcNow = DateTime.UtcNow;
            if (!(utcNow >= dataEvent.TglDaftarBukaUtc && utcNow <= dataEvent.TglDaftarTutupUtc)
                || dataEvent.StatusEvent != EventStatus.PUBLISHED_STATUS.Name) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (listPeserta == null || listPeserta.Count <= 0) {
                return HttpNotFound();
            }

            var capacityExceeded = false;

            foreach (var kodeAk in listPeserta.Select(e => e.KodeAK).Distinct()) {
                var pesertaValid = listPeserta
                    .Where(
                        e => e.KodeAK == kodeAk &&
                        !string.IsNullOrWhiteSpace(e.Nama) &&
                        !string.IsNullOrWhiteSpace(e.Jabatan) &&
                        !string.IsNullOrWhiteSpace(e.Email) &&
                        !string.IsNullOrWhiteSpace(e.Telp)).ToList();
                var registeredAllPeserta = _repo.GetPesertaFromEvent(id);

                if (AppUser.IsKpei) {
                    if (pesertaValid.Count + registeredAllPeserta.ToList().Count > dataEvent.KapasitasTotal) {
                        capacityExceeded = true;
                    }
                } else { 
                    if ((pesertaValid.Count + (registeredAllPeserta.Where(e => e.KodeAK == kodeAk).ToList()).Count) > dataEvent.KapasitasPerAK || pesertaValid.Count > dataEvent.KapasitasTotal - registeredAllPeserta.ToList().Count) {
                        capacityExceeded = true;
                    }
                }
            }


            if (ModelState.IsValid) {
                //validasi kapasitas

                if (capacityExceeded) {
                    ModelState.AddModelError("EventCapacityExceeded", "Maaf, kapasitas acara sudah melebihi batas");
                } else {
                    List<EventPeserta> newPeserta = new List<EventPeserta>();

                    using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                        IsolationLevel = IsolationLevel.Serializable,
                        Timeout = TimeSpan.FromMinutes(3)
                    })) {

                        foreach (var peserta in listPeserta) {
                            if (!string.IsNullOrWhiteSpace(peserta.Nama) &&
                                !string.IsNullOrWhiteSpace(peserta.Jabatan) &&
                                !string.IsNullOrWhiteSpace(peserta.Email) &&
                                !string.IsNullOrWhiteSpace(peserta.Telp)) {
                                newPeserta.Add(_repo.CreatePeserta(peserta));
                            }
                        }
                        scope.Complete();
                    }

                    _ulog.LogUserActionAsync(HttpContext, CoreModules.SOSIALISASI_PELATIHAN, "Menambahkan peserta ke acara", "SosialisasiPelatihan", id);

                    foreach (var kodeAk in listPeserta.Select(e => e.KodeAK).Distinct()) {
                        _usersvc.SendNotifNewRegistrationSospel(kodeAk, Url.Action("Detail", "Event", new { id = id }, Request.Url.Scheme));
                    }

                    return RedirectToAction("Detail", new { id = id });
                }
            }

            var mdl = new RegisterViewModel {
                Event = dataEvent,
                ListPeserta = listPeserta,
                AppUser = AppUser,
                AllCompanies = _compsvc.AllKpeiCompanies().ToList()
            };

            if (AppUser.IsKpei) {
                return View("RegisterKpei", mdl);
            }
            if (AppUser.IsAknp) {
                return View("RegisterNonKpei", mdl);
            }
            return HttpNotFound();
        }

        [Route("edit-registrasi/{id}")]
        [HttpGet]
        public ActionResult UpdateRegister(string id) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var dataEvent = _repo.Get(id);

            if (dataEvent == null) {
                return HttpNotFound();
            }
            var utcNow = DateTime.UtcNow;
            if (!(utcNow >= dataEvent.TglDaftarBukaUtc && utcNow <= dataEvent.TglDaftarTutupUtc)
                || dataEvent.StatusEvent != EventStatus.PUBLISHED_STATUS.Name) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var listPeserta = _repo.GetPesertaFromEvent(id);
            IEnumerable<KpeiCompany> allComp = null;
            if (AppUser.IsAknp) {
                listPeserta = listPeserta.Where(e => e.KodeAK == AppUser.KpeiCompanyCode).ToList();
            } else if (AppUser.IsKpei) {
                allComp = _compsvc.AllKpeiCompanies().ToList();
            }
            
            var mdl = new UpdateRegisterViewModel {
                Event = dataEvent,
                AppUser = AppUser,
                isKpei = AppUser.IsKpei,
                AllCompanies = allComp
            };
            if (listPeserta != null) {
                mdl.InitList(listPeserta.OrderBy(e => e.KodeAK).ToList());
            }
            return View(mdl);
        }

        [Route("edit-registrasi/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateRegister(string id, List<UpdateEventPesertaForm> listPeserta) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiModifierRightFor(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var dataEvent = _repo.Get(id);
            if (dataEvent == null) {
                return HttpNotFound();
            }
            var utcNow = DateTime.UtcNow;
            if (!(utcNow >= dataEvent.TglDaftarBukaUtc && utcNow <= dataEvent.TglDaftarTutupUtc)
                || dataEvent.StatusEvent != EventStatus.PUBLISHED_STATUS.Name) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (listPeserta == null || listPeserta.Count <= 0) {
                return HttpNotFound();
            }

            if (ModelState.IsValid) {

                List<EventPeserta> updatedPeserta = new List<EventPeserta>();

                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {

                    for (int i = 0; i < listPeserta.Count; i++) {
                        updatedPeserta.Add(_repo.UpdatePeserta(listPeserta[i]));
                    }
                    scope.Complete();
                }

                _ulog.LogUserActionAsync(HttpContext, CoreModules.SOSIALISASI_PELATIHAN, "Mengubah peserta pada acara", "SosialisasiPelatihan", id);

                foreach (var kodeAk in listPeserta.Select(e => e.KodeAK).Distinct()) {
                    _usersvc.SendNotifUpdateRegistrationSospel(kodeAk, Url.Action("Detail", "Event", new { id = id }, Request.Url.Scheme));
                }

                return RedirectToAction("Detail", new { id = id });
            }
            var mdl = new UpdateRegisterViewModel {
                Event = dataEvent,
                ListPeserta = listPeserta,
                AppUser = AppUser
            };

            return View(mdl);
        }

        [Route("grafik/{id}")]
        [HttpGet]
        public ActionResult GrafikKehadiran(string id) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var dataEvent = _repo.Get(id);
            List<EventPeserta> listPeserta = _repo.GetPesertaFromEvent(id).ToList();
            if (dataEvent == null) {
                return HttpNotFound();
            }
            if (listPeserta == null) {
                listPeserta = new List<EventPeserta>();
            }

            var mdl = new DetailViewModel {
                Event = dataEvent,
                ListPeserta = listPeserta,
                AppUser = AppUser,
            };

            return View("DetailGrafikKehadiran", mdl);
        }

        [Route("kehadiran")]
        [HttpGet]
        public ActionResult Kehadiran() {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var mdl = new IndexViewModel {
                AppUser = AppUser,
                JenisPelatihan = _stgrepo.EventSetting().ListJenisPelatihan.Select(x => x.Value).ToList()
            };
            if (AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) ||
                AppUser.NonKpeiViewerAsKpei(CoreModules.SOSIALISASI_PELATIHAN)) {
                return View("ListKehadiran", mdl);
            }

            if (AppUser.IsAknp) {
                return View("ListKehadiranNonKpei", mdl);
            }
            return HttpNotFound();
        }

        [Route("list-kehadiran-non-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListKehadiranNonKpei(EventDttRequestForm req) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var resp = new DttResponseForm {
                draw = req.draw
            };
            req.KodeAK = AppUser.KpeiCompanyCode;

            var result = _repo.List(req);
            var utcNow = DateTime.UtcNow;
            resp.recordsFiltered = result.RecordsFiltered;
            resp.recordsTotal = result.RecordsTotal;
            resp.data = result.Data.Select(x => new {
                x.Judul,
                TglPelaksanaan = x.TglPelaksanaanStartUtc?.ConvertUtcToWib().ToString("dd MMM yyyy"),
                x.JenisPelatihan,
                x.KapasitasTotal,
                PesertaTerdaftar = _repo.GetPesertaFromEvent(x.Id).Count(),
                x.StatusExpiration,
                DetailUrl = Url.Action("Detail", new { id = x.Id })
            }).Cast<object>().ToList();

            return Json(resp);
        }
        [Route("list-kehadiran-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListKehadiranKpei(EventDttRequestForm req) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var resp = new DttResponseForm {
                draw = req.draw
            };
            req.Status = EventStatus.PUBLISHED_STATUS.Name;

            var result = _repo.List(req);
            resp.recordsFiltered = result.RecordsFiltered;
            resp.recordsTotal = result.RecordsTotal;
            resp.data = result.Data.Select(x => new {
                x.Judul,
                TglPelaksanaan = x.TglPelaksanaanStartUtc?.ConvertUtcToWib().ToString("dd MMM yyyy"),
                x.JenisPelatihan,
                x.KapasitasTotal,
                PesertaTerdaftar = _repo.GetPesertaFromEvent(x.Id).Count(),
                x.StatusExpiration,
                DetailUrl = Url.Action("Detail", new { id = x.Id })
            }).Cast<object>().ToList();

            return Json(resp);
        }

        [Route("calendar")]
        [HttpGet]
        public ActionResult Calendar() {
            if (!AppUser.KpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.SOSIALISASI_PELATIHAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.SOSIALISASI_PELATIHAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var daftarEvent = _repo.Event.ToList();
            if (daftarEvent == null) {
                daftarEvent = new List<Event>();
            } else if (AppUser.IsAknp) {
                daftarEvent = daftarEvent.Where(e => e.StatusEvent == EventStatus.PUBLISHED_STATUS.Name).ToList();
            }
            var mdl = new CalendarViewModel {
                Event = daftarEvent
            };
            return View(mdl);
        }

        [Route("export-to-excel/{id}")]
        public void ExportExcel(string id) {
            if (AppUser.IsKpei) {
                var curEvent = _repo.Get(id);
                if (curEvent == null) {
                    this.HttpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;
                } else {
                    var listPeserta = _repo.GetPesertaFromEvent(id).ToList();
                    var worksheet = new Worksheet("Daftar Kehadiran");

                    worksheet.Cells[0, 0] = curEvent.Judul;
                    worksheet.Cells[2, 0] = "Kode AK";
                    worksheet.Cells[2, 1] = "Nama Perusahaan";
                    worksheet.Cells[2, 2] = "Nama Peserta";
                    worksheet.Cells[2, 3] = "Jabatan";
                    worksheet.Cells[2, 4] = "Tanda tangan";

                    worksheet.Cells[0, 0].Bold = true;
                    worksheet.Cells[2, 0].Bold = true;
                    worksheet.Cells[2, 1].Bold = true;
                    worksheet.Cells[2, 2].Bold = true;
                    worksheet.Cells[2, 3].Bold = true;
                    worksheet.Cells[2, 4].Bold = true;

                    worksheet.ColumnWidths[0] = 8;
                    worksheet.ColumnWidths[1] = 24;
                    worksheet.ColumnWidths[2] = 24;
                    worksheet.ColumnWidths[3] = 14;
                    worksheet.ColumnWidths[4] = 13;

                    worksheet.Cells[0, 0].FontSize = 14;
                    worksheet.Cells[2, 0].FontSize = 12;
                    worksheet.Cells[2, 1].FontSize = 12;
                    worksheet.Cells[2, 2].FontSize = 12;
                    worksheet.Cells[2, 3].FontSize = 12;
                    worksheet.Cells[2, 4].FontSize = 12;

                    worksheet.Cells[2, 0].Border = CellBorder.All;
                    worksheet.Cells[2, 1].Border = CellBorder.All;
                    worksheet.Cells[2, 2].Border = CellBorder.All;
                    worksheet.Cells[2, 3].Border = CellBorder.All;
                    worksheet.Cells[2, 4].Border = CellBorder.All;

                    for (int row = 3; row < listPeserta.Count + 3; row++) {
                        worksheet.Cells[row, 0] = listPeserta[row - 3].KodeAK;
                        worksheet.Cells[row, 1] = _compsvc.AllKpeiCompanies().FirstOrDefault(e => e.Code == listPeserta[row - 3].KodeAK).Name;
                        worksheet.Cells[row, 2] = listPeserta[row - 3].Nama;
                        worksheet.Cells[row, 3] = listPeserta[row - 3].Jabatan;

                        worksheet.Cells[row, 0].Border = CellBorder.All;
                        worksheet.Cells[row, 1].Border = CellBorder.All;
                        worksheet.Cells[row, 2].Border = CellBorder.All;
                        worksheet.Cells[row, 3].Border = CellBorder.All;
                        worksheet.Cells[row, 4].Border = CellBorder.All;

                        worksheet.Cells[row, 0].FontSize = 12;
                        worksheet.Cells[row, 1].FontSize = 12;
                        worksheet.Cells[row, 2].FontSize = 12;
                        worksheet.Cells[row, 3].FontSize = 12;
                        worksheet.Cells[row, 4].FontSize = 40;
                    }

                    var workbook = new Workbook();
                    workbook.Add(worksheet);

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + curEvent.Judul + ".xlsx");
                    workbook.Save(Response.OutputStream, true);
                }
            }
        }
    }
}