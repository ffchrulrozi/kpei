﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;
using JrzAsp.Lib.EntityFrameworkUtilities;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.Migrations;
using Kpei.PortalAK.DataAccess.ModDataAknp.Drivers;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.ModDataAknp.Services;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.Services;

namespace Kpei.PortalAK.Controllers {
    [RoutePrefix("db-migrator")]
    public class DbMigratorController : Controller {
        private readonly object _dbMigrationLock = new object();
        private readonly ISysLogger _logger;
        protected readonly AppDbContext Db;
        private readonly ISettingRepository _settingRepository;
        private readonly IDataAknpDriverStore _dataAknpDriverStore;
        private readonly IKpeiCompanyService _companyService;
        private GlobalSettingForm GlobalSetting => _settingRepository.GlobalSetting();

        public DbMigratorController(
            ISysLogger logger,
            AppDbContext db,
            IKpeiCompanyService companyService,
            IDataAknpDriverStore dataAknpDriverStore,
            ISettingRepository settingRepository) {
            _logger = logger;
            _dataAknpDriverStore = dataAknpDriverStore;
            _companyService = companyService;
            _settingRepository = settingRepository;
            Db = db;
        }

        [HttpGet]
        [Route("migrate-to-target")]
        public ActionResult MigrateToTarget(string password, string target = null) {
            try {
                if (password != Global.DbMigratorPassword) {
                    throw new InvalidOperationException("Wrong password");
                }

                lock (_dbMigrationLock) {
                    var migrator = new DatabaseMigrator<AppDbContext, DbMigrationConfiguration>();
                    migrator.MigrateToLatestVersion(true, target);
                }

                return Json(new {Status = "Success"}, JsonRequestBehavior.AllowGet);
            } catch (Exception ex) {
                _logger.LogError(ex);
                return Json(new {
                    Status = "Fail",
                    ErrorMessage = ex.Message,
                    ErrorClass = ex.GetType()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("ak-to-aki")]
        public ActionResult AkToAki(string password)
        {
            try {
                if (password != Global.DbMigratorPassword) {
                    throw new InvalidOperationException("Wrong password");
                }

                lock (_dbMigrationLock)
                {
                    var companies = _companyService.AllKpeiCompanies().Where(x => x.Code != GlobalSetting.GuestAknpCode).
                        Where(x => !x.Code.Any(char.IsDigit)).Where(x => x.Code.Length == 2).ToArray();
                    foreach (var company in companies)
                    {
                        var administrasiKeanggotaan = new AdministrasiKeanggotaan{};
                        administrasiKeanggotaan.KodeAK = company.Code;
                        administrasiKeanggotaan.KategoriMember = RegistrasiKategoriMember.MEMBER_AKI.Name;
                        administrasiKeanggotaan.Status = DataAknpRequestStatus.APPROVED_STATUS.Name;
                        administrasiKeanggotaan.ActiveUtc = administrasiKeanggotaan.CreatedUtc =
                            administrasiKeanggotaan.UpdatedUtc = DateTime.UtcNow;
                        administrasiKeanggotaan.Approver1UserId = administrasiKeanggotaan.Approver2UserId = "migrator";

                        var kategoriMember = RegistrasiKategoriMember.MEMBER_AK.Name;
                        var tipeMember = Db.DataTipeMember.FirstOrDefault(x => x.KodeAk == company.Code);
                        if (tipeMember != null && tipeMember?.KategoriMember != RegistrasiKategoriMember.MEMBER_AK.Name)
                        {
                            administrasiKeanggotaan.KategoriMember = tipeMember.KategoriMember;
                            var partisipan = Db.DataTipeMemberPartisipanItem
                                .FirstOrDefault(x => x.ParentDataId == tipeMember.Id);
                            if (partisipan != null && tipeMember.KategoriMember == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name)
                            {
                                kategoriMember = RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name;
                                administrasiKeanggotaan.TipeMemberPartisipan = partisipan.Nama;
                            }
                        }

                        var oldData = Db.AdministrasiKeanggotaan.FirstOrDefault(x =>
                            x.KategoriMember.Contains(kategoriMember)
                            && x.TipeMemberPartisipan == administrasiKeanggotaan.TipeMemberPartisipan
                            && x.KodeAK == administrasiKeanggotaan.KodeAK);
                        if (oldData != null)
                        {
                            continue;
                        }

                        var driverAkte = _dataAknpDriverStore.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(DataAkteAknpDriver.SLUG));
                        var dataAkte = (DataAkte) driverAkte.GetLatestActiveData(administrasiKeanggotaan.KodeAK);
                        administrasiKeanggotaan.DokumenKhususIjinUsahaFileUrl = dataAkte?.SkOjkFileUrl;
                        Db.AdministrasiKeanggotaan.Add(administrasiKeanggotaan);
                        var driverStatusKpei = _dataAknpDriverStore.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(DataStatusKpeiAknpDriver.SLUG));
                        var dataStatusKpei =
                            (DataStatusKPEI) driverStatusKpei.GetLatestActiveData(administrasiKeanggotaan.KodeAK);
                        if (dataStatusKpei != null && administrasiKeanggotaan.KategoriMember != RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name)
                        {
                            dataStatusKpei.AdministrasiKeanggotaanId = administrasiKeanggotaan.Id;
                        }
                    }
                    Db.SaveChanges();
                    // var dataAk = Db.DataRegistrasi.Where(x => x.KategoriMember == RegistrasiKategoriMember.MEMBER_AK.Name).ToList();
                    // dataAk.ForEach(ak => { ak.KategoriMember = RegistrasiKategoriMember.MEMBER_AKI.Name; });
                    // Db.SaveChanges();
                }


                return Json(new {Status = "Success"}, JsonRequestBehavior.AllowGet);
            } catch (Exception ex) {
                _logger.LogError(ex);
                return Json(new {
                    Status = "Fail",
                    ErrorMessage = ex.Message,
                    ErrorClass = ex.GetType()
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}