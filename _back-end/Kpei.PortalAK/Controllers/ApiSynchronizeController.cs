﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Services;
using Kpei.PortalAK.DataAccess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace Kpei.PortalAK.Controllers
{
    [RoutePrefix("sync")]
    public class ApiSynchronizeController : Controller
    {
        private readonly IDataAknpDriverStore _store;
        private readonly IKpeiCompanyService _compSvc;
        private readonly ISysLogger _logger;
        private readonly JsonResult success;

        public ApiSynchronizeController(IDataAknpDriverStore store, IKpeiCompanyService compSvc, ISysLogger logger) {
            _store = store;
            _compSvc = compSvc;
            _logger = logger;
            success = Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
        }

        [Route("to/arms/{dataSlug}")]
        public ActionResult BeginSynchronizeToArms(string dataSlug, string password, int offset = 0, int limit = Int32.MaxValue)
        {
            if (!Properties.Settings.Default.LockSynchronizeLink) {
                try {
                    if (password != Global.DbMigratorPassword) {
                        throw new InvalidOperationException("Wrong password");
                    }

                    var drv = _store.LoadDrivers().FirstOrDefault(x => x.DataSlug == dataSlug);
                    if (drv == null) return HttpNotFound();
                    if (drv.IsRegistrationData) return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                    IEnumerable<BaseAknpData> dats = null;
                    //scope
                    dats = drv.SyncToArms(offset, limit);

                    if (dats != null && dats.Count() > 0) {
                        return success;
                    } else {
                        return Json(new { Status = "Semua data saat ini telah disinkronisasi", Message = "Jika anda yakin ini adalah sebuah error, harap menghubungi pihak administrasi server" }, JsonRequestBehavior.AllowGet);
                    }

                } catch (Exception ex) {
                    _logger.LogError(new Exception(dataSlug + ": Error occured when synchronize process to ARMS"));
                    _logger.LogError(ex);
                    return Json(new {
                        Status = "Fail",
                        ErrorMessage = ex.Message,
                        ErrorClass = ex.GetType()
                    }, JsonRequestBehavior.AllowGet);
                }
            } else {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
        }

        [Route("from/arms/{dataSlug}")]
        public ActionResult BeginSynchronizeFromArms(string dataSlug, string password) {
            if (!Properties.Settings.Default.LockSynchronizeLink) {
                try {
                    if (password != Global.DbMigratorPassword) {
                        throw new InvalidOperationException("Wrong password");
                    }

                    var drv = _store.LoadDrivers().FirstOrDefault(x => x.DataSlug == dataSlug);
                    if (drv == null) return HttpNotFound();
                    if (drv.IsRegistrationData) return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                    IEnumerable<BaseAknpData> dats = null;
                    dats = drv.SyncToPortal();

                    if (dats != null && dats.Count() > 0) {
                        return success;
                    } else {
                        return Json(new { Status = "Semua data saat ini telah disinkronisasi", Message = "Jika anda yakin ini adalah sebuah error, harap menghubungi pihak administrasi server" }, JsonRequestBehavior.AllowGet);
                    }
                } catch (Exception ex) {
                    _logger.LogError(new Exception(dataSlug + ": Error occured when synchronize process to Portal"));
                    _logger.LogError(ex);
                    return Json(new {
                        Status = "Fail",
                        ErrorMessage = ex.Message,
                        ErrorClass = ex.GetType()
                    }, JsonRequestBehavior.AllowGet);
                }
            } else {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
        }
    }
}