﻿using Kpei.PortalAK.DataAccess.ModKeuangan.Models;
using Kpei.PortalAK.DataAccess.ModKeuangan.Repos;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Models;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Repos;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kpei.PortalAK.Controllers {
    [RoutePrefix("reminder")]
    public class ReminderController : Controller {
        private readonly IEventRepository _eventRepo;
        private readonly ISettingRepository _stgRepo;
        private readonly IKeuanganRepository _keuanganRepo;
        private readonly IKpeiCompanyService _compSvc;
        private readonly IUserService _userSvc;
        private readonly ISysLogger _logger;

        public ReminderController(IEventRepository eventRepo, IKeuanganRepository keuanganRepo, IKpeiCompanyService compSvc, ISettingRepository stgRepo, IUserService userSvc, ISysLogger logger) {
            _eventRepo = eventRepo;
            _keuanganRepo = keuanganRepo;
            _compSvc = compSvc;
            _userSvc = userSvc;
            _stgRepo = stgRepo;
            _logger = logger;
        }

        [Route("sosialisasi-pelatihan")]
        public ActionResult RemindEvent(string password) {
            try {
                if (password != Global.DbMigratorPassword) {
                    throw new InvalidOperationException("Wrong password");
                }

                var allEvent = _eventRepo.Event.ToList();
                var utcNow = DateTime.UtcNow;
                var stg = _stgRepo.GetSettingByField(SettingFields.EVENT_REMINDER);
                int minThreshold = Convert.ToInt32(stg?.Value ?? "1");
                var email = _stgRepo.GetEmailByField(SettingFields.EMAIL_EVENT_REMIND_AKNP);
                var kpeiCode = "ZZ";

                var targetEvent = allEvent.Where(e =>
                    utcNow.AddDays(minThreshold) >= e.TglPelaksanaanStartUtc &&
                    utcNow < e.TglPelaksanaanEndUtc &&
                    e.StatusEvent == EventStatus.PUBLISHED_STATUS.Name).ToArray();

                var peserta = _eventRepo.GetAllPeserta();

                var allComp = _compSvc.AllKpeiCompanies();
                var targetEventLog = "TargetEvent = ";
                var targetCodeLog = "TargetCode = ";

                foreach (var target in targetEvent) {
                    targetEventLog += "{ Pelaksanaan: " + target.TglPelaksanaanStartUtc + "}";
                    var targetPeserta = peserta.Where(e => e.EventId == target.Id && e.KodeAK != kpeiCode).Select(e => e.KodeAK).Distinct().ToList();
                    if (email.Template.Value != null) {
                        email.Template.Value = EmailReplaceKey.ReplaceEvent(email.Template.Value, target.Judul, target.TglPelaksanaanStartUtc);
                    }

                    if (targetPeserta.Count > 0) {
                        var targetUser = allComp.Where(e => targetPeserta.Contains(e.Code)).ToArray();

                        foreach (var user in targetUser) {
                            targetCodeLog += "{ " + user.Code + " : " + user.Name + " }";
                        }
                        SendReminder(targetUser, email.Header.Header, email.Template.Value);
                    }
                    return Json(new { Status = "Success", targetEventLog, targetCodeLog }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { status = "No target to remind" }, JsonRequestBehavior.AllowGet );
            } catch (Exception ex) {
                _logger.LogError(ex);
                return Json(new {
                    Status = "Fail",
                    ErrorMessage = ex.Message,
                    ErrorClass = ex.GetType()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [Route("laporan-keuangan")]
        public ActionResult RemindLaporan(string password) {
            try {
                if (password != Global.DbMigratorPassword) {
                    throw new InvalidOperationException("Wrong password");
                }

                var allLaporan = _keuanganRepo.Laporan.ToList();
                var utcNow = DateTime.UtcNow;
                var guestCode = _stgRepo.GlobalSetting().GuestAknpCode;
                var stg = _stgRepo.DeadlineSetting();
                var email = _stgRepo.GetEmailByField(SettingFields.EMAIL_KEUANGAN_REMIND_AKNP);
                var kpeiCode = "ZZ";

                var currPeriode = utcNow.Month > stg.KeuanganAwalPeriodeUtc?.Month ?
                PeriodeLaporanKeuangan.AKHIR_TAHUN.Name :
                PeriodeLaporanKeuangan.TENGAH_TAHUN.Name;

                if (CheckReminderDate(currPeriode, utcNow, stg)) {
                    //accepted / on-going laporan
                    var currLaporan = allLaporan.Where(
                        e => e.Periode == currPeriode &&
                        e.Tahun == utcNow.Year &&
                        e.StatusRequest != LaporanKeuanganStatus.REJECTED_STATUS.Name &&
                        e.KodeAK != kpeiCode
                        ).Select(e => e.KodeAK);

                    var targetLogin = _compSvc.LoadKpeiLogin().Select(e => e.Code);
                    var targetUser = _compSvc.AllKpeiCompanies().Where(e => !currLaporan.Contains(e.Code) && targetLogin.Contains(e.Code) && e.Code != guestCode && e.Code != kpeiCode).ToList();

                    SendReminder(targetUser, email.Header.Header, email.Template.Value);

                    string targetCodeLog = "";

                    foreach (var user in targetUser) {
                        targetCodeLog += "{ " + user.Code + " : " + user.Name + " }";
                    }
                    
                    return Json(new { Status = "Success", targetCodeLog }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { Status = "No need to remind" }, JsonRequestBehavior.AllowGet);

            } catch (Exception ex) {
                _logger.LogError(ex);
                return Json(new {
                    Status = "Fail",
                    ErrorMessage = ex.Message,
                    ErrorClass = ex.GetType()
                }, JsonRequestBehavior.AllowGet);
            }

        }

        private void SendReminder(IEnumerable<KpeiCompany> targets, string header, string body) {
            var codes = targets.Select(e => e.Code).ToList();
            _userSvc.SendEmailToManyAknp(codes, header, body);
        }

        private bool CheckReminderDate(string currPeriode, DateTime utcNow, DeadlineSettingForm stg) {
            bool reminder1 =
                (currPeriode == PeriodeLaporanKeuangan.TENGAH_TAHUN.Name &&
                    utcNow.AddDays(stg.Reminder1).Month >= stg.KeuanganAwalPeriodeUtc?.Month &&
                    utcNow.AddDays(stg.Reminder1).Day >= stg.KeuanganAwalPeriodeUtc?.Day) ||
                (currPeriode == PeriodeLaporanKeuangan.AKHIR_TAHUN.Name &&
                    utcNow.AddDays(stg.Reminder1).Month >= stg.KeuanganAkhirPeriodeUtc?.Month &&
                    utcNow.AddDays(stg.Reminder1).Day >= stg.KeuanganAkhirPeriodeUtc?.Day);

            return reminder1;
        }
    }
}