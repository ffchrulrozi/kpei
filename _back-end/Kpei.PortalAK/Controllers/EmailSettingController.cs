﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Models;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.ViewModels.Email;
using System;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;

namespace Kpei.PortalAK.Controllers {
    [Authorize]
    [RoutePrefix("template-email")]
    public class EmailSettingController : Controller {
        private readonly ISettingRepository _setting;
        private readonly IUserActionLogService _ulog;
        
        public EmailSettingController(ISettingRepository setting, IUserActionLogService ulog) {
            _setting = setting;
            _ulog = ulog;
        }

        private AppUser AppUser => AppUser.CreateFromIdentity(User.Identity);

        // GET: EmailSetting
        //public ActionResult Index()
        //{
        //    return View();
        //}

        [HttpGet]
        [Route("data-aknp")]
        public ActionResult Aknp() {
            var mdl = new EmailViewModel {
                ListEmailSetting = _setting.GetAknpEmail().ToList()
            };
            TempData["title"] = "AK & P";
            TempData["tag"] = "email-setting-aknp";
            return View("Index", mdl);
        }

        [HttpGet]
        [Route("reset-pincode")]
        public ActionResult Pincode() {
            var mdl = new EmailViewModel {
                ListEmailSetting = _setting.GetPincodeEmail().ToList()
            };
            TempData["title"] = "Reset Pincode";
            TempData["tag"] = "email-setting-pincode";
            return View("Index", mdl);
        }

        [HttpGet]
        [Route("layanan-mclears")]
        public ActionResult Mclears() {
            var mdl = new EmailViewModel {
                ListEmailSetting = _setting.GetMclearsEmail().ToList()
            };
            TempData["title"] = "Layanan m-CLEARS";
            TempData["tag"] = "email-setting-mclears";
            return View("Index", mdl);
        }

        [HttpGet]
        [Route("administrasi-keanggotaan")]
        public ActionResult AdministrasiKeanggotaan() {
            var mdl = new EmailViewModel {
                ListEmailSetting = _setting.GetAdministrasiKeanggotaanEmail().ToList()
            };
            TempData["title"] = "Administrasi Keanggotaan";
            TempData["tag"] = "email-setting-administrasi-keanggotaan";
            return View("Index", mdl);
        }

        [HttpGet]
        [Route("sosialisasi-pelatihan")]
        public ActionResult Event() {
            var mdl = new EmailViewModel {
                ListEmailSetting = _setting.GetSospelEmail().ToList()
            };
            TempData["title"] = "Sosialisasi & Pelatihan";
            TempData["tag"] = "email-setting-event";
            return View("Index", mdl);
        }

        [HttpGet]
        [Route("laporan-keuangan")]
        public ActionResult Keuangan() {
            var mdl = new EmailViewModel {
                ListEmailSetting = _setting.GetKeuanganEmail().ToList()
            };
            TempData["title"] = "Laporan Keuangan";
            TempData["tag"] = "email-setting-keuangan";
            return View("Index", mdl);
        }

        [HttpGet]
        [Route("survei")]
        public ActionResult Survei() {
            var mdl = new EmailViewModel {
                ListEmailSetting = _setting.GetSurveiEmail().ToList()
            };
            TempData["title"] = "Survei";
            TempData["tag"] = "email-setting-survei";
            return View("Index", mdl);
        }

        [Route("edit-template")]
        [HttpGet]
        public ActionResult Edit(string field) {
            var templateHeader = field.Replace("MailHeader", "MailTemplate");
            var email = _setting.GetSettingByField(templateHeader) ?? new Setting {
                Field = templateHeader
            };
            var header = _setting.GetSettingByField(field) ?? new Setting {
                Field = field
            };
            var title = EmailHeader.All().FirstOrDefault(e => e.Key == field);
            var mdl = new UpdateViewModel {
                Email = new EmailSettingForm {
                    DefaultHeader = title,
                    Header = header,
                    Template = email
                }
            };

            if (field.Contains("Data")) {
                TempData["tag"] = "email-setting-aknp";
            } else if (field.Contains("Pincode")) {
                TempData["tag"] = "email-setting-pincode";
            } else if (field.Contains("Mclears")) {
                TempData["tag"] = "email-setting-mclears";
            } else if (field.Contains("AdministrasiKeanggotaan")) {
                TempData["tag"] = "email-setting-administrasi-keanggotaan";
            } else if (field.Contains("Event")) {
                TempData["title"] = "Sosialisasi & Pelatihan";
                TempData["tag"] = "email-setting-event";
            } else if (field.Contains("Keuangan")) {
                TempData["tag"] = "email-setting-keuangan";
            } else if (field.Contains("Survei")) {
                TempData["tag"] = "email-setting-survei";
            }

            return View(mdl);
        }

        [Route("edit-template")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(string field, EmailSettingForm email) {
            //template
            var oldData = _setting.GetSettingByField(email.Template.Field);
            if (oldData != null) {
                EditSetting(email.Template);
            } else {
                AddSetting(email.Template);
            }

            //header
            var oldHeader = _setting.GetSettingByField(email.Header.Field);
            if (oldHeader != null) {
                EditSetting(email.Header);
            } else {
                AddSetting(email.Header);
            }

            var mod = field;
            string action = null;
            if (mod.Contains("Data")) {
                action = "Aknp";
            } else if (mod.Contains("Pincode")) {
                action = "Pincode";
            } else if (mod.Contains("Mclears")) {
                action = "Mclears";
            } else if (mod.Contains("Event")) {
                action = "Event";
            } else if (mod.Contains("AdministrasiKeanggotaan")) {
                action = "AdministrasiKeanggotaan";
            } else if (mod.Contains("Keuangan")) {
                action = "Keuangan";
            } else if (mod.Contains("Survei")) {
                action = "Survei";
            }
            if (action == null) {
                return HttpNotFound();
            }
            return RedirectToAction(action);
        }

        private void AddSetting(Setting setting) {
            if (!string.IsNullOrWhiteSpace(setting.Value?.Trim())) {
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    _setting.AddSetting(setting);
                    scope.Complete();
                }
            }
        }

        private void EditSetting(Setting setting) {
            if (!string.IsNullOrWhiteSpace(setting.Value?.Trim())) {
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    _setting.UpdateSetting(setting);
                    scope.Complete();
                }
            }
        }
    }
}