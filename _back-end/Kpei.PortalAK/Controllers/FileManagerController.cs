﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.ViewModels.FileManager;

namespace Kpei.PortalAK.Controllers {
    [RoutePrefix("file-manager")]
    [Authorize]
    public class FileManagerController : Controller {
        private readonly ISysLogger _logger;

        public FileManagerController(ISysLogger logger) {
            _logger = logger;
        }

        [Route("upload")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Upload(UploadViewModel mdl) {
            var errors = new List<string>();
            var successFiles = new List<string>();
            var failedFiles = new List<string>();
            if (ModelState.IsValid) {
                foreach (var fi in mdl.Files) {
                    try {
                        var fileValid = ValidateFileSize(mdl, fi, errors);
                        
                        if (fileValid) {
                            fileValid = ValidateFileExtension(mdl, fi, errors);
                        }

                        if (fileValid) {
                            fileValid = ValidateFileMimeType(mdl, fi, errors);
                        }

                        if (fileValid) {
                            string path = $"~/Uploads/{mdl.FolderPath}";
                            if (!String.IsNullOrEmpty(WebConfigurationManager.AppSettings["PathUploadFile"])) {
                                path = WebConfigurationManager.AppSettings["PathUploadFile"].ToString();
                            }
                            var folder = $"{path}/{Guid.NewGuid():N}";
                            var saveFile = $"{folder}/{fi.FileName}";
                            
                            
                            if (!String.IsNullOrEmpty(WebConfigurationManager.AppSettings["PathUploadFile"]))
                            {
                                var realFolder = folder;
                                if (!Directory.Exists(realFolder))
                                {
                                    Directory.CreateDirectory(realFolder);
                                }
                                var realSaveFile = saveFile;
                                fi.SaveAs(realSaveFile);
                            }
                            else {
                                var realFolder = Server.MapPath(folder);
                                if (!Directory.Exists(realFolder))
                                {
                                    Directory.CreateDirectory(realFolder);
                                }
                                var realSaveFile = Server.MapPath(saveFile);
                                fi.SaveAs(realSaveFile);
                            }
                            
                            successFiles.Add(saveFile);
                        } else {
                            failedFiles.Add(fi.FileName);
                        }
                    } catch (Exception ex) {
                        _logger.LogError(ex);
                        errors.Add(ex.Message);
                        failedFiles.Add(fi.FileName);
                    }
                }
            } else {
                errors.AddRange(ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
            }

            return Json(new {
                SuccessFiles = successFiles.ToArray(),
                FailedFiles = failedFiles.ToArray(),
                Errors = errors.ToArray()
            });
        }

        private static string FriendlySize(int sizeInBytes) {
            var size = (double) sizeInBytes;
            var sufs = new[] {"KB", "MB", "GB", "TB"};
            var currentSuf = "B";
            foreach (var suf in sufs) {
                var nextSize = size / 1024;
                if (nextSize >= 1) {
                    size = nextSize;
                    currentSuf = suf;
                } else {
                    break;
                }
            }

            return $"{size:F2} {currentSuf}";
        }

        private static bool ValidateFileSize(UploadViewModel mdl, HttpPostedFileBase fi, List<string> errors) {
            if (mdl.MaxBytes.HasValue) {
                if (fi.ContentLength > mdl.MaxBytes.Value) {
                    errors.Add($"{fi.FileName} ukurannya tidak boleh lebih dari {FriendlySize(mdl.MaxBytes.Value)}");
                    return false;
                }
            }

            return true;
        }

        private static bool ValidateFileMimeType(UploadViewModel mdl, HttpPostedFileBase fi, List<string> errors) {
            if (mdl.ValidMimeTypes.Count > 0) {
                if (mdl.ValidMimeTypes.All(x =>
                    !String.Equals(x, fi.ContentType, StringComparison.InvariantCultureIgnoreCase))) {
                    errors.Add($"{fi.FileName} tidak diperbolehkan untuk diupload (MIME)");
                    return false;
                }
            }

            return true;
        }

        private static bool ValidateFileExtension(UploadViewModel mdl, HttpPostedFileBase fi, List<string> errors) {
            if (mdl.ValidFileExtensions.Count > 0) {
                var fileExt = "";
                var lastDotIdx = fi.FileName.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase);
                if (lastDotIdx >= 0 && lastDotIdx != fi.FileName.Length - 1) {
                    fileExt = fi.FileName.Substring(lastDotIdx + 1).ToLowerInvariant();
                }

                if (mdl.ValidFileExtensions.All(x => x.ToLowerInvariant() != fileExt)) {
                    errors.Add($"{fi.FileName} tidak diperbolehkan untuk diupload (EXT)");
                    return false;
                }
            }

            return true;
        }
    }
}