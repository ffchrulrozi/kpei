﻿using System;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using Kpei.PortalAK.DataAccess.ModPincode.Forms;
using Kpei.PortalAK.DataAccess.ModPincode.Repos;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.Features;
using Kpei.PortalAK.ViewModels.Pincode;
using Kpei.PortalAK.DataAccess.ModDataAknp.Drivers;
using Kpei.PortalAK.DataAccess.ModPincode.Models;

namespace Kpei.PortalAK.Controllers {
    [RoutePrefix("reset-pincode")]
    [Authorize]
    public class ResetPincodeController : Controller {
        private readonly IKpeiCompanyService _compsvc;
        private readonly IPincodeRepository _repo;
        private readonly ISettingRepository _stgrepo;
        private readonly IUserActionLogService _ulog;
        private readonly IUserService _usersvc;
        private readonly IEmailService _emailsvc;
        private readonly DataAkteAknpDriver _akteDrv;

        public ResetPincodeController(IPincodeRepository repo, IKpeiCompanyService compsvc, ISettingRepository stgrepo,
            IUserActionLogService ulog, IEmailService emailsvc, IUserService usersvc, DataAkteAknpDriver akteDrv) {
            _repo = repo;
            _compsvc = compsvc;
            _stgrepo = stgrepo;
            _ulog = ulog;
            _emailsvc = emailsvc;
            _usersvc = usersvc;
            _akteDrv = akteDrv;
        }

        private AppUser AppUser => User.Identity.GetAppUser();

        [HttpGet]
        [Route("")]
        public ActionResult Index() {
            if (AppUser.KpeiViewerRightFor(CoreModules.REQUEST_PINCODE) || AppUser.NonKpeiViewerAsKpei(CoreModules.REQUEST_PINCODE)) {
                return IndexKpei();
            }

            if (AppUser.IsAknp) {
                return IndexNonKpei();
            }

            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        private ActionResult IndexKpei() {
            if (!AppUser.KpeiViewerRightFor(CoreModules.REQUEST_PINCODE) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.REQUEST_PINCODE)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var mdl = new IndexKpeiViewModel {
                AllCompanies = _compsvc.AllKpeiCompanies().ToList(),
                ListJenisPincode = _stgrepo.PincodeSetting().PincodeTypeInfos.Select(x => x.Name).ToList(),
                AppUser = AppUser,
            };
            return View("IndexKpei", mdl);
        }

        private ActionResult IndexNonKpei() {
            if (!AppUser.NonKpeiViewerRightFor(CoreModules.REQUEST_PINCODE)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var mdl = new IndexNonKpeiViewModel() {
                AppUser = AppUser
            };
            return View("IndexNonKpei", mdl);
        }

        [Route("list-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListKpei(PincodeDttRequestForm req) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.REQUEST_PINCODE) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.REQUEST_PINCODE)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var data = _repo.ListPincode(req);

            var res = new DttResponseForm {
                draw = req.draw,
                recordsTotal = data.RecordsTotal,
                recordsFiltered = data.RecordsFiltered,
                data = data.Data.Select(x => MakeDttListItem(x)).Cast<object>().ToList()
            };
            return Json(res);
        }

        [Route("list-non-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListNonKpei(PincodeDttRequestForm req) {
            if (!AppUser.NonKpeiViewerRightFor(CoreModules.REQUEST_PINCODE)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            req.KodeAk = AppUser.KpeiCompanyCode ?? "0";
            var data = _repo.ListPincode(req);

            var res = new DttResponseForm {
                draw = req.draw,
                recordsTotal = data.RecordsTotal,
                recordsFiltered = data.RecordsFiltered,
                data = data.Data.Select(x => MakeDttListItem(x)).Cast<object>().ToList()
            };
            return Json(res);
        }

        private dynamic MakeDttListItem(PincodeRequest x) {
            return new {
                x.KodeAK,
                NamaPerusahaan = _compsvc.AllKpeiCompanies().FirstOrDefault(e => e.Code == x.KodeAK).Name,
                x.JenisPincode,
                Status = x.StatusRequest,
                WaktuPengajuan = x.CreatedUtc.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss"),
                x.Id,
                DetailUrl = Url.Action("Detail", new { id = x.Id })
            };
        }

        [Route("detil/{id}")]
        [HttpGet]
        public ActionResult Detail(string id) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.REQUEST_PINCODE) &&
                !AppUser.NonKpeiViewerRightFor(CoreModules.REQUEST_PINCODE) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.REQUEST_PINCODE)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var mdl = new DetailViewModel {
                TypeInfos = _stgrepo.PincodeSetting().PincodeTypeInfos.ToArray(),
                User = AppUser,
                Pincode = _repo.GetPincodeRequest(id),
            };

            if (mdl.Pincode == null) return HttpNotFound();

            if (!AppUser.KpeiViewerRightFor(CoreModules.REQUEST_PINCODE) && !AppUser.NonKpeiViewerAsKpei(CoreModules.REQUEST_PINCODE) && mdl.Pincode.KodeAK != AppUser.KpeiCompanyCode) return HttpNotFound();

            return View(mdl);
        }

        [Route("request-baru")]
        [HttpGet]
        public ActionResult NewRequest() {
            if (!AppUser.NonKpeiModifierRightFor(CoreModules.REQUEST_PINCODE)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var mdl = new NewRequestViewModel {
                Form = new NewPincodeForm(),
                PincodeTypeInfos =
                    _stgrepo.PincodeSetting().PincodeTypeInfos.ToArray()
            };

            return View(mdl);
        }

        [Route("request-baru")]
        [HttpPost]
        public ActionResult NewRequest(NewPincodeForm form) {
            if (!AppUser.NonKpeiModifierRightFor(CoreModules.REQUEST_PINCODE)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (ModelState.IsValid) {
                var kodeAk = AppUser.KpeiCompanyCode;
                PincodeRequest newReq;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    newReq = _repo.NewRequest(kodeAk, form);
                    scope.Complete();
                }

                _ulog.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.REQUEST_PINCODE,
                    $"Request reset PIN code baru dibuat.",
                    "ResetPincodeRequest", newReq.Id);

                _usersvc.SendNotifNewPincode(newReq.KodeAK, Url.Action("Detail", "ResetPincode", new { id = newReq.Id }, Request.Url.Scheme));

                return RedirectToAction("Index");
            }

            var mdl = new NewRequestViewModel {
                Form = form ?? new NewPincodeForm(),
                PincodeTypeInfos =
                    _stgrepo.PincodeSetting().PincodeTypeInfos.ToArray()
            };

            return View(mdl);
        }

        [Route("approve/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Approve(string id) {
            if (!AppUser.KpeiApproverRightFor(CoreModules.REQUEST_PINCODE)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var req = _repo.GetPincodeRequest(id);
            if (req == null) return HttpNotFound();

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = IsolationLevel.Serializable,
                Timeout = TimeSpan.FromMinutes(3)
            })) {
                req = _repo.ApproveRequest(id, AppUser.Id);
                scope.Complete();
            }

            _ulog.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.REQUEST_PINCODE,
                $"Request reset PIN code di-approve.",
                "ResetPincodeRequest", req.Id);

            _usersvc.SendNotifActionPincode(req.KodeAK, Url.Action("Detail", "ResetPincode", new { id = req.Id }, Request.Url.Scheme));

            return RedirectToAction("Detail", new { id });
        }

        [Route("reject/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Reject(string id, RejectViewModel mdl) {
            if (!AppUser.KpeiApproverRightFor(CoreModules.REQUEST_PINCODE)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var req = _repo.GetPincodeRequest(id);
            if (req == null) return HttpNotFound();

            if (ModelState.IsValid) {
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    req = _repo.RejectRequest(id, mdl.RejectReason);
                    scope.Complete();
                }
            } else {
                TempData["ValidationErrors"] =
                    ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            _ulog.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.REQUEST_PINCODE,
                $"Request reset PIN code di-reject.",
                "ResetPincodeRequest", req.Id);

            _usersvc.SendNotifRejectPincode(req.KodeAK, Url.Action("Detail", "ResetPincode", new { id = req.Id }, Request.Url.Scheme), req.AlasanDitolak);

            return RedirectToAction("Detail", new { id });
        }

        [Route("hasil-reset/{id}")]
        [HttpGet]
        public ActionResult SendNewPin(string id) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.REQUEST_PINCODE)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var req = _repo.GetPincodeRequest(id);
            if (req == null) return HttpNotFound();

            var mdl = new SendNewPinViewModel {
                TypeInfos = _stgrepo.PincodeSetting().PincodeTypeInfos.ToArray(),
                Pincode = req,
                Form = new SendPincodeForm()
            };

            var latestAkte = _akteDrv.GetLatestActiveData(req.KodeAK) as DataAkte;
            if (latestAkte != null) {
                mdl.Form.EmailDireksi = latestAkte.DaftarDireksi.Select(e => e.Email).ToList();
            }

            return View(mdl);
        }

        [Route("hasil-reset/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendNewPin(string id, SendPincodeForm form) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.REQUEST_PINCODE)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var req = _repo.GetPincodeRequest(id);
            if (req == null) return HttpNotFound();

            if (ModelState.IsValid) {
                PincodeResetResult result;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    result = _repo.InputAndSendResetResult(id, form);
                    scope.Complete();
                }

                _ulog.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.REQUEST_PINCODE,
                    $"Request reset PIN code selesai. Hasil reset juga telah dikirim.",
                    "ResetPincodeRequest", req.Id, new {
                        ResetResultId = result.Id
                    });
                
                var urlTerkait = _stgrepo.PincodeSetting().PincodeTypeInfos.ToArray().FirstOrDefault(x => x.Name == req.JenisPincode)?.Url;

                _usersvc.SendNewPincodeEmail(form, req.KodeAK, Url.Action("Detail", "ResetPincode", new { id = req.Id }, Request.Url.Scheme), result.NewUserName, result.NewPassword, urlTerkait);

                return RedirectToAction("Detail", new { id });
            }

            var mdl = new SendNewPinViewModel {
                TypeInfos = _stgrepo.PincodeSetting().PincodeTypeInfos.ToArray(),
                Pincode = req,
                Form = form ?? new SendPincodeForm()
            };

            return View(mdl);
        }

        [Route("log/{id}")]
        [HttpGet]
        public ActionResult Log(string id) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.REQUEST_PINCODE) &&
                !AppUser.NonKpeiViewerRightFor(CoreModules.REQUEST_PINCODE) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.REQUEST_PINCODE)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var pinreq = _repo.GetPincodeRequest(id);
            if (pinreq == null || !AppUser.KpeiViewerRightFor(CoreModules.REQUEST_PINCODE) && !AppUser.NonKpeiViewerAsKpei(CoreModules.REQUEST_PINCODE) && pinreq.KodeAK != AppUser.KpeiCompanyCode) {
                return HttpNotFound();
            }

            var mdl = new LogViewModel {
                Pincode = pinreq
            };

            return View(mdl);
        }

        [Route("list-log/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListLog(string id, UserActionLogDttRequestForm req) {
            req.UserId = null;
            req.UserName = null;
            req.KodeAk = null;
            req.RelatedEntityName = "ResetPincodeRequest";
            req.RelatedEntityId = id;

            var pinreq = _repo.GetPincodeRequest(id);
            if (pinreq == null || !AppUser.KpeiViewerRightFor(CoreModules.REQUEST_PINCODE) && !AppUser.NonKpeiViewerAsKpei(CoreModules.REQUEST_PINCODE) && pinreq.KodeAK != AppUser.KpeiCompanyCode) {
                return HttpNotFound();
            }

            var pag = _ulog.ListLogsAsync(req).Result;
            var resp = new DttResponseForm {
                draw = req.draw,
                recordsTotal = pag.RecordsTotal,
                recordsFiltered = pag.RecordsFiltered,
                data = pag.Data.Select(x => new {
                    WaktuPerubahan = x.CreatedUtc.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss"),
                    DibuatOleh = $"{x.UserName} / {x.UserAknpCode}",
                    Aktivitas = x.Message
                }).Cast<object>().ToList()
            };

            return Json(resp);
        }
    }
}