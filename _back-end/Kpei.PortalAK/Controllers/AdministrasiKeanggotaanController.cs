﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using Kpei.PortalAK.DataAccess.ModDataAknp.Drivers;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.ModLayananBaru.Repos;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Forms;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Repos;
using Kpei.PortalAK.DataAccess.ModDataAknp.Services;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.Features;
using Kpei.PortalAK.ViewModels.DataAknp;
using Kpei.PortalAK.ViewModels.AdministrasiKeanggotaan;
using ListRequestViewModel = Kpei.PortalAK.ViewModels.AdministrasiKeanggotaan.ListRequestViewModel;

namespace Kpei.PortalAK.Controllers
{
    [RoutePrefix("administrasi-keanggotaan")]
    [Authorize]

    public class AdministrasiKeanggotaanController : Controller
    {
        private readonly IKpeiCompanyService _compsvc;
        private readonly IDataAknpDriverStore _dataAknpDriver;
        private readonly ISettingRepository _settingRepository;
        private readonly IUserActionLogService _userLogService;
        private readonly ILayananBaruRepository _layananBaruRepository;
        private readonly IAdministrasiKeanggotaanRepository _administrasiKeanggotaanRepository;
        private readonly IUserService _usersvc;

        private static readonly string LOG_ENTITY_NAME = "AdministrasiKeanggotaan";
        public static readonly string DRIVER_SLUG = AdministrasiKeanggotaanAknpForm.DRIVER_SLUG;
        private AppUser AppUser => User.Identity.GetAppUser();
        private GlobalSettingForm GlobalSetting => _settingRepository.GlobalSetting();

        public AdministrasiKeanggotaanController(IKpeiCompanyService compsvc,
            ILayananBaruRepository layananBaruRepository,
            IAdministrasiKeanggotaanRepository administrasiKeanggotaanRepository,
            ISettingRepository settingRepository,
            IUserService usersvc,
            IDataAknpDriverStore dataAknpDriver,
            IUserActionLogService userLogService)
        {
            _compsvc = compsvc;
            _usersvc = usersvc;
            _dataAknpDriver = dataAknpDriver;
            _layananBaruRepository = layananBaruRepository;
            _administrasiKeanggotaanRepository = administrasiKeanggotaanRepository;
            _settingRepository = settingRepository;
            _userLogService = userLogService;
        }

        [HttpGet]
        [Route("")]
        public ActionResult Index()
        {
            if (!AppUser.KpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.ADMINISTRASI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var companies = _compsvc.AllKpeiCompanies();
            companies = companies.Where(x => x.Code != GlobalSetting.GuestAknpCode).ToArray();


            ListRequestViewModel model = new ListRequestViewModel
            {
                Companies = companies,
                User = AppUser,
                GlobalSetting = GlobalSetting
            };

            return View("ListActiveData", model);
        }

        [Route("list-history")]
        [HttpGet]
        public ActionResult ListHistory()
        {
            if (!AppUser.KpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.ADMINISTRASI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var companies = _compsvc.AllKpeiCompanies();
            companies = companies.Where(x => x.Code != GlobalSetting.GuestAknpCode).ToArray();

            ListRequestViewModel model = new ListRequestViewModel
            {
                Companies = companies,
                User = AppUser,
                GlobalSetting = GlobalSetting
            };

            return View(model);
        }

        [Route("list-request")]
        [HttpGet]
        public ActionResult ListRequest()
        {
            if (!AppUser.KpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.ADMINISTRASI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            ListRequestViewModel model = new ListRequestViewModel
            {
                Companies = _compsvc.AllKpeiCompanies(),
                User = AppUser,
                GlobalSetting = GlobalSetting
            };

            return View(model);
        }

        [Route("daftar-aktif-dtt")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListActiveDtt(ChangeRequestDttRequestForm req)
        {
            if (!AppUser.KpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.ADMINISTRASI_KEANGGOTAAN)) {
                req.KodeAK = AppUser.KpeiCompanyCode;
            }

            var resp = new DttResponseForm {
                draw = req.draw
            };

            var statusKpeiDriver = (DataStatusKpeiAknpDriver)_dataAknpDriver.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(DataStatusKpeiAknpDriver.SLUG));
            var companies = _compsvc.AllKpeiCompanies();
            var allLatestStatusKpei = companies.Select(x => statusKpeiDriver.GetLatestActiveData(x.Code)).Where(x => x != null)
                .ToArray().AsQueryable();

            Func<string, bool> isAK = (k) =>
            {
                return k == RegistrasiKategoriMember.MEMBER_AKU.Name ||
                       k == RegistrasiKategoriMember.MEMBER_AKI.Name;
            };

            Func<string, string, DataStatusKPEI> getStatusKpei = (kodeAK, administrasiKeanggotaanId) =>
            {
                return (DataStatusKPEI) statusKpeiDriver.GetLatestActiveData(kodeAK, administrasiKeanggotaanId);
            };
            var result = _administrasiKeanggotaanRepository.ListActive(req);
            resp.recordsFiltered = result.RecordsFiltered;
            resp.recordsTotal = result.RecordsTotal;
            resp.data = result.Data.Select(x => new {
                x.Id,
                x.KodeAK,
                NamaPerusahaan = _compsvc.AllKpeiCompanies().FirstOrDefault(e => e.Code == x.KodeAK).Name,
                WaktuAktif = x.ActiveUtc?.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss"),
                KategoriMember = x.KategoriMember ?? "-",
                TipeMemberPartisipan = x.TipeMemberPartisipan ?? "-",
                StatusKPEI = isAK(x.KategoriMember) ? getStatusKpei(x.KodeAK, x.Id)?.StatusKPEI ?? "-" : "-",
            }).Cast<object>().ToList();
            return Json(resp);
        }

        [Route("daftar-history-dtt")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListHistoryDtt(ChangeRequestDttRequestForm req)
        {
            if (!AppUser.KpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.ADMINISTRASI_KEANGGOTAAN)) {
                req.KodeAK = AppUser.KpeiCompanyCode;
            }

            var resp = new DttResponseForm {
                draw = req.draw
            };
            var result = _administrasiKeanggotaanRepository.ListHistory(req);
            resp.recordsFiltered = result.RecordsFiltered;
            resp.recordsTotal = result.RecordsTotal;
            resp.data = result.Data.Select(x => new {
                x.Id,
                x.KodeAK,
                NamaPerusahaan = _compsvc.AllKpeiCompanies().FirstOrDefault(e => e.Code == x.KodeAK).Name,
                WaktuPengajuan = x.CreatedUtc.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss"),
                JenisData = "Pendaftaran Baru",
                x.Status,
            }).Cast<object>().ToList();
            return Json(resp);
        }

        [Route("daftar-request-dtt")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListRequestDtt(ChangeRequestDttRequestForm req)
        {
            if (!AppUser.KpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.ADMINISTRASI_KEANGGOTAAN)) {
                req.KodeAK = AppUser.KpeiCompanyCode;
            }
            req.DataSlug = DRIVER_SLUG;

            var resp = new DttResponseForm {
                draw = req.draw
            };
            var result = _administrasiKeanggotaanRepository.ListRequest(req);
            resp.recordsFiltered = result.RecordsFiltered;
            resp.recordsTotal = result.RecordsTotal;
            resp.data = result.Data.Select(x => new {
                x.Id,
                x.KodeAk,
                NamaPerusahaan = _compsvc.AllKpeiCompanies().FirstOrDefault(e => e.Code == x.KodeAk).Name,
                WaktuPengajuan = x.CreatedUtc.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss"),
                JenisData = "Pendaftaran Baru",
                x.Status,
                x.DataId,
            }).Cast<object>().ToList();
            return Json(resp);
        }

        [Route("log-request/{id}")]
        [HttpGet]
        public ActionResult LogRequest(string id)
        {
            if (!AppUser.KpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.ADMINISTRASI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            LogRequestViewModel model = new LogRequestViewModel
            {
               AdministrasiKeanggotaan = _administrasiKeanggotaanRepository.Get(id) ,
               User = AppUser
            };

            return View(model);
        }

        [Route("log-request-dtt/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogRequestDtt(string dataSlug, string id, UserActionLogDttRequestForm req) {
            req.UserId = null;
            req.UserName = null;
            req.KodeAk = null;
            req.RelatedEntityName = LOG_ENTITY_NAME;
            req.RelatedEntityId = id;

            var data = _administrasiKeanggotaanRepository.Get(id);
            if (data == null || data.KodeAK != AppUser.KpeiCompanyCode &&
                !AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) { 
                return HttpNotFound();
            }

            var pag = _userLogService.ListLogsAsync(req).Result;
            var resp = new DttResponseForm {
                draw = req.draw,
                recordsTotal = pag.RecordsTotal,
                recordsFiltered = pag.RecordsFiltered,
                data = pag.Data.Select(x => new {
                    WaktuPerubahan = x.CreatedUtc.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss"),
                    DibuatOleh = $"{x.UserName} / {x.UserAknpCode}",
                    Aktivitas = x.Message
                }).Cast<object>().ToList()
            };

            return Json(resp);
        }

        [Route("detail/{id}")]
        [HttpGet]
        public ActionResult Detail(string id)
        {
            DetailViewModel viewModel = new DetailViewModel
            {
                AdministrasiKeanggotaan = _administrasiKeanggotaanRepository.Get(id),
                TradingMember = _administrasiKeanggotaanRepository.GetTradingMemberFromAdministrasiKeanggotaan(id),
                LayananBaru = _administrasiKeanggotaanRepository.GetLayananBaruFromAdministrasiKeanggotaan(id),
                User = AppUser,
            };

            if (viewModel.AdministrasiKeanggotaan == null) return HttpNotFound();


            return View("Detail", viewModel);
        }

        [Route("detail-request/{id}")]
        [HttpGet]
        public ActionResult DetailRequest(string id)
        {
            DetailViewModel viewModel = new DetailViewModel
            {
                AdministrasiKeanggotaan = _administrasiKeanggotaanRepository.Get(id),
                TradingMember = _administrasiKeanggotaanRepository.GetTradingMemberFromAdministrasiKeanggotaan(id),
                User = AppUser,
            };
            
            return View("Detail", viewModel);
        }

        [Route("approve-request/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveRequestChange(string id)
        {
            AdministrasiKeanggotaan data = _administrasiKeanggotaanRepository.Get(id);
            if (data == null) return HttpNotFound();
            
            if (data.Approver1UserId == AppUser.Id || data.Approver2UserId == AppUser.Id) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (data.KodeAK == GlobalSetting.GuestAknpCode)
            {
                TempData["DataStillGuest"] = true;
                return RedirectToAction("Detail", new {id});
            }

            AdministrasiKeanggotaan status;
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = IsolationLevel.Serializable,
                Timeout = TimeSpan.FromMinutes(3)
            })) {
                status = _administrasiKeanggotaanRepository.Approve(id, AppUser.Id);
                scope.Complete();
            }
            string statText;
            if (status.Status == DataAknpRequestStatus.ONGOING_STATUS.Name) {
                statText = "diterima dan sedang melalui proses approval";
            } else if (status.Status == DataAknpRequestStatus.APPROVED_STATUS.Name) {
                _usersvc.SendNotifApproveAdministrasiKeanggotaan(data.KodeAK, Url.Action("Detail", "AdministrasiKeanggotaan", new { id = data.Id }, Request.Url.Scheme));
                statText = "di-approve";
            } else {
                statText = $"di-{status.Status.ToLowerInvariant()}";
            }

            _userLogService.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.ADMINISTRASI_KEANGGOTAAN,
                $"Request data Administrasi Keanggotaan telah {statText}.",
                LOG_ENTITY_NAME, status.Id);
            TempData["ApproveSuccess"] = true;
            return RedirectToAction("Detail", new {id});
        }

        [Route("reject-request/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RejectRequestChange(string id, RejectRequestChangeViewModel mdl)
        {
            if (!AppUser.KpeiApproverRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            AdministrasiKeanggotaan data = _administrasiKeanggotaanRepository.Get(id);
            if (data == null) return HttpNotFound();
            ModelState.Clear();
            if (TryValidateModel(mdl)) {
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    _administrasiKeanggotaanRepository.Reject(data.Id, mdl.RejectReason);
                    scope.Complete();
                }
                _userLogService.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.ADMINISTRASI_KEANGGOTAAN,
                    $"Request data Administrasi Keanggotaan telah di-reject.",
                    LOG_ENTITY_NAME, data.Id);
                _usersvc.SendNotifRejectAdministrasiKeanggotaan(data.KodeAK, Url.Action("Detail", "AdministrasiKeanggotaan", new { id = data.Id }, Request.Url.Scheme), data.RejectReason);

                TempData["RejectSuccess"] = true;
            } else {
                TempData["ValidationErrors"] =
                    ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }
            return RedirectToAction("Detail", new {id});
        }

        [Route("create")]
        [HttpGet]
        public ActionResult Create(string kodeAK)
        {
            AdministrasiKeanggotaan draftData =
                _administrasiKeanggotaanRepository.GetLatestDraftData(kodeAK, AppUser.Id);
            if (draftData.Draft == true)
            {
                return RedirectToAction("Update", new {id = draftData.Id});
            }

            AdministrasiKeanggotaanAknpForm FormAknp = new AdministrasiKeanggotaanAknpForm();

            FormAknp.KodeAK = kodeAK;

            InputViewModel viewModel = new InputViewModel
            {
                FormAknp = FormAknp,
                GlobalSetting = GlobalSetting,
                User = AppUser
            };

            return View("InputForm", viewModel);
        }

        [Route("remove-draft/{id}")]
        [HttpGet]
        public ActionResult RemoveDraft(string id)
        {
            _administrasiKeanggotaanRepository.RemoveDraft(id);
            return RedirectToAction("Index");
        }

        [Route("request-data")]
        [HttpGet]
        public ActionResult RequestData(string id = null)
        {
            InputViewModel viewModel = new InputViewModel
            {
                FormAknp = new AdministrasiKeanggotaanAknpForm(),
                GlobalSetting = GlobalSetting,
                User = AppUser
            };
            viewModel.FormAknp.KodeAK = AppUser.KpeiCompanyCode;

            if (id != null)
            {
                viewModel.FormAknp.Init(_administrasiKeanggotaanRepository.Get(id));
                viewModel.FormAknp.Id = null;
                AdministrasiKeanggotaan draftData =
                    _administrasiKeanggotaanRepository.GetLatestDraftData(viewModel.FormAknp.KodeAK, AppUser.Id);
                if (draftData.Draft == true)
                {
                    return RedirectToAction("Update", new {id = draftData.Id});
                }
            }
            else
            {
                AdministrasiKeanggotaan draftData =
                    _administrasiKeanggotaanRepository.GetLatestDraftData(AppUser.KpeiCompanyCode, AppUser.Id);
                if (draftData.Draft == true)
                {
                    return RedirectToAction("Update", new {id = draftData.Id});
                }
            }

            return View("RequestForm", viewModel);
        }

        [Route("request-data")]
        [HttpPost]
        public ActionResult RequestData(InputViewModel viewModel)
        {
            AdministrasiKeanggotaan data = _administrasiKeanggotaanRepository.GetNewOngoingData(AppUser.KpeiCompanyCode, 
                viewModel.FormAknp.KategoriMember, viewModel.FormAknp.TipeMemberPartisipan);
            if (data != null)
            {
                TempData["haveNotyetApproved"] = true;
                return RedirectToAction("Detail", new { id = data.Id });
            }
            return Save(viewModel);
        }

        [Route("update/{id}")]
        [HttpGet]
        public ActionResult Update(string id)
        {
            AdministrasiKeanggotaan data = _administrasiKeanggotaanRepository.Get(id);
            if (data == null) return HttpNotFound();
            if (data.Status == DataAknpRequestStatus.APPROVED_STATUS.Name && data.Draft == false) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            List<AdministrasiKeanggotaanTradingMember> tradingMember = _administrasiKeanggotaanRepository.GetTradingMemberFromAdministrasiKeanggotaan(data.Id);

            if (!(AppUser.NonKpeiModifierRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) &&
                  data.Status == DataAknpRequestStatus.NEW_STATUS.Name ||
                  AppUser.KpeiModifierRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN) &&
                  data.Status != DataAknpRequestStatus.REJECTED_STATUS.Name)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            AdministrasiKeanggotaanAknpForm form = new AdministrasiKeanggotaanAknpForm();
            form.Init(data);

            if (form.KategoriMember == RegistrasiKategoriMember.MEMBER_AKU.Name)
            {
                form.ListTradingMember = _administrasiKeanggotaanRepository.GetTradingMemberFromAdministrasiKeanggotaanForm(data.Id);
            }

            if (data.Draft && !string.IsNullOrWhiteSpace(data.UserDraft))
            {
                TempData["DataStillDraft"] = true;
            }

            if (!AppUser.KpeiModifierRightFor(CoreModules.ADMINISTRASI_KEANGGOTAAN)) {
                form.KodeAK = AppUser.KpeiCompanyCode;
            }
            InputViewModel viewModel = new InputViewModel
            {
                GlobalSetting = GlobalSetting,
                FormAknp = form,
                User = AppUser
            };
            return View("InputForm", viewModel);
        }

        [Route("save")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(InputViewModel viewModel)
        {
            viewModel.User = AppUser;
            viewModel.GlobalSetting = GlobalSetting;
            bool request = AppUser.IsAknp;
            if (ModelState.IsValid || viewModel.FormAknp.Draft == true)
            {
                AdministrasiKeanggotaan newLayanan = new AdministrasiKeanggotaan();

                bool hasValidData = false;
                string logMessage;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(5)
                }))
                {
                    if (viewModel.FormAknp.Draft == true)
                    {
                        viewModel.FormAknp.UserDraft = AppUser.Id;
                    }
                    logMessage = "Data Administrasi Keanggotaan baru dibuat.";
                    if (viewModel.FormAknp.Id != null)
                    {
                        AdministrasiKeanggotaan existing =
                            _administrasiKeanggotaanRepository.Get(viewModel.FormAknp.Id);
                        if ((existing.Draft == true && viewModel.FormAknp.Draft == false) && request)
                        {
                            var result =
                                _administrasiKeanggotaanRepository.CreateDraftChangeRequest(viewModel.FormAknp);
                            newLayanan = result.Item1;
                        }
                        else
                        {
                            logMessage = "Perubahan data Administrasi Keanggotaan telah dilakukan.";
                            newLayanan = _administrasiKeanggotaanRepository.Update(viewModel.FormAknp);
                        }
                    }
                    else
                    {
                        if (AppUser.IsKpei)
                        {
                            viewModel.FormAknp.Approver1UserId = AppUser.Id;
                            viewModel.FormAknp.Approver2UserId = AppUser.Id;
                            viewModel.FormAknp.Status = DataAknpRequestStatus.APPROVED_STATUS.Name;
                        }

                        if (viewModel.FormAknp.Draft == true || AppUser.IsKpei)
                        {
                            newLayanan = _administrasiKeanggotaanRepository.Create(viewModel.FormAknp);
                        }
                        else
                        {
                            viewModel.FormAknp.KodeAK = AppUser.KpeiCompanyCode;
                            var result = _administrasiKeanggotaanRepository.CreateChangeRequest(viewModel.FormAknp);
                            newLayanan = result.Item1;
                        }
                    }

                    _administrasiKeanggotaanRepository.CreateUpdateLayananBaru(viewModel.FormAknp, newLayanan.Id);
                    if (viewModel.FormAknp.KategoriMember == RegistrasiKategoriMember.MEMBER_AKU.Name)
                    {
                        if (viewModel.FormAknp.ListTradingMember?.Count > 0)
                        {
                            hasValidData = true;
                            for (int i = 0; i < viewModel.FormAknp.ListTradingMember.Count; i++)
                            {
                                if (viewModel.FormAknp.Id == null)
                                {
                                    viewModel.FormAknp.ListTradingMember[i].AdministrasiKeanggotaanId = newLayanan.Id;
                                    _administrasiKeanggotaanRepository.CreateTradingMember(viewModel.FormAknp.ListTradingMember[i]);
                                    continue;
                                }
                                _administrasiKeanggotaanRepository.UpdateTradingMember(viewModel.FormAknp.ListTradingMember[i]);   
                            }    
                        }
                    }else
                    {
                        hasValidData = true;
                    }

                    if (!hasValidData)
                    {
                        ModelState.AddModelError("notValid", "Data yang anda masukkan tidak valid. Mohon dicek kembali");
                        return View("InputForm", viewModel);
                    }
                    scope.Complete();
                };
                if (viewModel.FormAknp.Draft == false)
                {
                    _userLogService.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.ADMINISTRASI_KEANGGOTAAN,
                        logMessage,
                        LOG_ENTITY_NAME, newLayanan.Id);
                    if (viewModel.FormAknp.Id == null)
                    {
                        if (AppUser.IsKpei)
                        {
                            _usersvc.SendNotifApproveAdministrasiKeanggotaan(newLayanan.KodeAK, Url.Action("Detail", "AdministrasiKeanggotaan", new { id = newLayanan.Id }, Request.Url.Scheme));
                        }
                        else
                        {
                            _usersvc.SendNotifNewAdministrasiKeanggotaan(newLayanan.KodeAK, Url.Action("Detail", "AdministrasiKeanggotaan", new { id = newLayanan.Id }, Request.Url.Scheme));
                        }
                    }
                    else
                    {
                        _usersvc.SendNotifUpdateAdministrasiKeanggotaan(newLayanan.KodeAK, Url.Action("Detail", "AdministrasiKeanggotaan", new { id = newLayanan.Id }, Request.Url.Scheme));
                    }
                }
                return RedirectToAction("Index");
            }

            return View("InputForm", viewModel);
        }
    }
}