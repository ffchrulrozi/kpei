﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web.Mvc;
using JrzAsp.Lib.DirectViewRenderer;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms;
using Kpei.PortalAK.DataAccess.ModDataAknp.Forms.ChangeRequest;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Repos;
using Kpei.PortalAK.DataAccess.ModDataAknp.Services;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.Features;
using Kpei.PortalAK.Features.DataAknpViewSystem;
using Kpei.PortalAK.Features.Pdf;
using Kpei.PortalAK.ViewModels.DataAknp;
using Kpei.PortalAK.DataAccess.ModDataAknp.Drivers;
using System.Web.Routing;

namespace Kpei.PortalAK.Controllers {
    [Authorize]
    [RoutePrefix("aknp")]
    public class DataAknpController : Controller {
        private readonly IKpeiCompanyService _compSvc;
        private readonly IDataAknpDriverStore _dataAknpDrvs;
        private readonly IDataAknpRepository _dataAknpRepo;
        private readonly ISettingRepository _stg;
        private readonly IUserActionLogService _ulog;
        private readonly IPdfConverter _pdfConvert;
        private readonly IUserService _usersvc;

        public DataAknpController(IDataAknpRepository repo, IDataAknpDriverStore dataAknpDrvs,
            IKpeiCompanyService compSvc, IUserActionLogService ulog, ISettingRepository stg, IPdfConverter pdfConvert, IUserService usersvc) {
            _dataAknpRepo = repo;
            _dataAknpDrvs = dataAknpDrvs;
            _compSvc = compSvc;
            _ulog = ulog;
            _stg = stg;
            _usersvc = usersvc;
            _pdfConvert = pdfConvert;
        }

        private AppUser AppUser => User.Identity.GetAppUser();
        private GlobalSettingForm GlobalSetting => _stg.GlobalSetting();

        [Route("profil/{kodeAk?}")]
        [HttpGet]
        public ActionResult Profile(string kodeAk = null) {
            if (!AppUser.NonKpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (AppUser.NonKpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) && 
                !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN) && 
                kodeAk != AppUser.KpeiCompanyCode) {
                kodeAk = AppUser.KpeiCompanyCode;
            }
            var guestCode = _stg.GlobalSetting().GuestAknpCode;

            return View(new ProfileViewModel {
                User = AppUser,
                DriverStore = _dataAknpDrvs,
                KodeAk = kodeAk,
                Companies = _compSvc.AllKpeiCompanies().Where(e => e.Code != guestCode).ToArray(),
            });
        }

        [Route("profil-pdf/{kodeAk}")]
        [HttpGet]
        public ActionResult ProfilePdf(string kodeAk) {
            if (!AppUser.NonKpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (!AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN) && 
                AppUser.NonKpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) && 
                kodeAk != AppUser.KpeiCompanyCode) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var comps = _compSvc.AllKpeiCompanies();
            var comp = comps.FirstOrDefault(x => x.Code == kodeAk);
            if (comp == null) {
                return HttpNotFound();
            }

            var profile = new AknpProfileDisplay();
            profile.Populate(kodeAk, _dataAknpDrvs);

            var mdl = new ProfilePdfTemplateViewModel {
                KodeAk = kodeAk,
                User = AppUser,
                Companies = comps,
                ProfileData = profile
            };

            var html = this.RenderViewToString("~/Views/DataAknp/_ProfilePdfTemplate.cshtml", mdl, true);
            var pdf = _pdfConvert.ConvertFromHtmlToPdf(html, new[] {"~/assets/custom/css/profile-pdf-template.css"});

            return File(pdf, "application/pdf", $"Ringkasan Profil - {comp.Code} - {comp.Name}.{DateTime.Now:yyyyMMdd.HHmmss}.pdf");
        }

        [Route("{dataSlug}/remove-draft/{id}")]
        [HttpGet]
        public ActionResult RemoveDraft(string dataSlug, string id = null) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();
            drv.RemoveDraft(id);
            if (AppUser.IsGuestAknp)
            {
                return RedirectToAction("RequestChange", new {dataSlug});
            }
            return RedirectToAction("Detail", new {dataSlug});
        }

        [Route("{dataSlug}")]
        [HttpGet]
        public ActionResult Detail(string dataSlug, string kodeAk = null, string viewMode = null) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            if (drv.IsRegistrationData) {
                return RedirectToAction("ListRequest", new {dataSlug});
            }

            if (drv.EnabledOnlyForKpei && 
                !AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) && 
                !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var companies = _compSvc.AllKpeiCompanies();

            companies = companies.Where(x => x.Code != GlobalSetting.GuestAknpCode).ToArray();

            if (string.IsNullOrWhiteSpace(viewMode)) {
                viewMode = "table";
            }
            
            if (AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) ||
                AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
                return View("DetailKpei", new DetailKpeiViewModel {
                    KodeAk = kodeAk,
                    ViewMode = viewMode,
                    User = AppUser,
                    Companies = companies,
                    Driver = drv
                });
            }

            if (AppUser.NonKpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                if (kodeAk != null) {
                    return RedirectToAction("Detail", new {dataSlug = drv.DataSlug, kodeAk = ""});
                }

                return View("DetailNonKpei", new DetailNonKpeiViewModel {
                    KodeAk = AppUser.KpeiCompanyCode,
                    User = AppUser,
                    Driver = drv
                });
            }

            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        [Route("{dataSlug}/detail-dtt")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DetailDtt(string dataSlug, BaseAknpDttRequestForm req) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            //if (!AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) && !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
            //    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            //}

            var comps = _compSvc.AllKpeiCompanies();
            var allLatest = comps.Select(x => drv.GetLatestActiveData(x.Code)).Where(x => x != null)
                .ToArray().AsQueryable();
            var data = drv.List(req, allLatest);
            var pag = new PaginatedResult<BaseAknpData>();
            pag.Initialize(data.All, data.Filtered, req.start, req.length);

            var res = new DttResponseForm {
                draw = req.draw,
                recordsTotal = pag.RecordsTotal,
                recordsFiltered = pag.RecordsFiltered,
                data = pag.Data.Select(x => MakeDetailDttItem(drv, x)).Cast<object>().ToList()
            };
            return Json(res);
        }

        [Route("{dataSlug}/request")]
        [HttpGet]
        public ActionResult RequestChange(string dataSlug) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            if (drv.IsRegistrationData && !AppUser.IsGuestAknp ||
                !drv.IsRegistrationData && !AppUser.NonKpeiModifierRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var form = drv.InitDraftForm(AppUser.KpeiCompanyCode, AppUser.Id);

            var mdl = new RequestChangeViewModel {
                User = AppUser,
                Driver = drv,
                GlobalSetting = GlobalSetting,
                Companies = _compSvc.AllKpeiCompanies(),
                Form = form
            };

            if (drv.DataSlug != DataRegistrasiAknpDriver.SLUG && string.IsNullOrWhiteSpace(form.Id))
            {
                mdl.Form = drv.InitForm(AppUser.KpeiCompanyCode);
                mdl.Form.Id = null;
            }

            if (mdl.Form.Draft && !string.IsNullOrWhiteSpace(mdl.Form.UserDraft))
            {
                TempData["DataStillDraft"] = true;
            }

            return View(mdl);
        }

        [Route("{dataSlug}/request")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RequestChange(string dataSlug, BaseAknpForm form)
        {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            if (drv.IsRegistrationData && !AppUser.IsGuestAknp ||
                !drv.IsRegistrationData && !AppUser.NonKpeiModifierRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            form.Status = DataAknpRequestStatus.NEW_STATUS.Name;
            form.KodeAk = AppUser.KpeiCompanyCode;

            ModelState.Clear();
            if (TryValidateModel(form) || form.Draft == true) {
                BaseAknpData dat;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(5)
                })) {
                    var formType = form.GetType();
                    var formCollProps = formType.GetProperties().Where(e => typeof(IEnumerable<object>).IsAssignableFrom(e.PropertyType));
                    foreach (var colProp in formCollProps) {
                        var colVal = colProp.GetValue(form) as IEnumerable<object>;
                        foreach (var item in colVal) {
                            var itemType = item.GetType();
                            var idProp = itemType.GetProperty("Id");
                            if (idProp != null) {
                                idProp.SetValue(item, Guid.NewGuid().ToString());
                            }
                        }
                    }
                    form.UserDraft = null;
                    if (form.Draft == true)
                    {
                        form.UserDraft = AppUser.Id;
                    }

                    bool newData = false;
                    dat = drv.GetById(form.Id) as BaseAknpData;
                    if (form.Id == null)
                    {
                        newData = true;
                        form.Id = Guid.NewGuid().ToString();
                        dat = Activator.CreateInstance(drv.DataClassType) as BaseAknpData;
                    }

                    Tuple<BaseAknpData, DataChangeRequest> result;
                    drv.MapFromTo(form, dat);
                    if (newData)
                    {
                        if (form.Draft == true)
                        {
                            dat = drv.Create(dat);
                        }
                        else
                        {
                            result = drv.CreateChangeRequest(dat);
                            dat = result.Item1;
                        }
                    }
                    else
                    {
                        if (dat.Draft == true)
                        {
                            dat = drv.Update(dat);
                        }
                        else
                        {
                            dat.UserDraft = null;
                            result = drv.CreateDraftChangeRequest(dat);
                            dat = result.Item1;
                        }
                    }
                    scope.Complete();

                }

                if (dat.Draft != true)
                {
                    _ulog.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.DATA_AK_PARTISIPAN,
                        $"Request data {drv.DataTitle} baru dibuat.",
                        CreateLogEntityName(drv), dat.Id);
                
                    var registrasi = dat as DataRegistrasi;
                    if (AppUser.IsGuestAknp && drv.DataSlug == DataRegistrasiAknpDriver.SLUG && registrasi.Email != null) {
                        _usersvc.SendNotifNewRegistrasi(registrasi.Email, Url.Action("RequestChangeDetail", "DataAknp", new { dataSlug = dataSlug, id = dat.Id }, Request.Url.Scheme));
                    } else {
                        _usersvc.SendNotifNewAknp(form.KodeAk, Url.Action("RequestChangeDetail", "DataAknp", new { dataSlug = dataSlug, id = dat.Id }, Request.Url.Scheme));
                    }
                }


                if (drv.IsRegistrationData) {
                    if (dat.Draft != true)
                    {
                        TempData["RegisterSuccess"] = true;
                    }
                    return RedirectToAction("RequestChange", new {dataSlug = drv.DataSlug});
                }

                return RedirectToAction("ListRequest", new {dataSlug = drv.DataSlug});
            }

            var mdl = new RequestChangeViewModel {
                User = AppUser,
                GlobalSetting = GlobalSetting,
                Driver = drv,
                Form = form
            };
            return View(mdl);
        }

        [Route("{dataSlug}/daftar-request")]
        [HttpGet]
        public ActionResult ListRequest(string dataSlug) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            if (!AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.NonKpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var companies = _compSvc.AllKpeiCompanies();

            companies = companies.Where(x => x.Code != GlobalSetting.GuestAknpCode).ToArray();

            var mdl = new ListRequestViewModel {
                User = AppUser,
                Driver = drv,
                Companies = companies,
                RequestStatuses = DataAknpRequestStatus.All().ToArray()
            };

            return View(mdl);
        }

        [Route("{dataSlug}/daftar-request-dtt")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListRequestDtt(string dataSlug, ChangeRequestDttRequestForm req) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            //if (!AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
            //    !AppUser.NonKpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
            //    !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
            //    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            //}

            req.DataSlug = drv.DataSlug;

            if (!AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
                req.KodeAk = AppUser.KpeiCompanyCode;
            }

            var data = _dataAknpDrvs.ListChangeRequests(req);
            var pag = new PaginatedResult<DataChangeRequest>();
            pag.Initialize(data.All, data.Filtered, req.start, req.length);

            var res = new DttResponseForm {
                draw = req.draw,
                recordsTotal = pag.RecordsTotal,
                recordsFiltered = pag.RecordsFiltered,
                data = pag.Data.Select(x => new {
                    NamaPerusahaan = _compSvc.AllKpeiCompanies().FirstOrDefault(e => e.Code == x.KodeAk).Name,
                    x.DataId,
                    WaktuPengajuan = x.CreatedUtc.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss"),
                    JenisData = _dataAknpDrvs.LoadDefinitions().FirstOrDefault(y => y.DataSlug == x.DataDriverSlug)
                                    ?.DataTitle ?? x.DataDriverSlug,
                    x.DataDriverSlug,
                    x.Status,
                    x.KodeAk
                }).Cast<object>().ToList()
            };
            return Json(res);
        }

        [Route("daftar-request", Name = "AllRequest")]
        [HttpGet]
        public ActionResult ListGlobalRequest() {
            if (!AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var mdl = new ListGlobalRequestViewModel {
                User = AppUser,
                Companies = _compSvc.AllKpeiCompanies(),
                RequestStatuses = DataAknpRequestStatus.All().ToArray(),
                DataDefinitions = _dataAknpDrvs.LoadDefinitions().ToArray()
            };

            return View(mdl);
        }

        [Route("daftar-request-dtt")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListGlobalRequestDtt(ChangeRequestDttRequestForm req) {
            //if (!AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
            //    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            //}

            var data = _dataAknpDrvs.ListChangeRequests(req);
            var pag = new PaginatedResult<DataChangeRequest>();
            pag.Initialize(data.All, data.Filtered, req.start, req.length);

            var res = new DttResponseForm {
                draw = req.draw,
                recordsTotal = pag.RecordsTotal,
                recordsFiltered = pag.RecordsFiltered,
                data = pag.Data.Select(x => new {
                    NamaPerusahaan = _compSvc.AllKpeiCompanies().FirstOrDefault(e => e.Code == x.KodeAk).Name,
                    x.DataId,
                    WaktuPengajuan = x.CreatedUtc.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss"),
                    JenisData = _dataAknpDrvs.LoadDefinitions().FirstOrDefault(y => y.DataSlug == x.DataDriverSlug)
                                    ?.DataTitle ?? x.DataDriverSlug,
                    x.DataDriverSlug,
                    x.Status,
                    x.KodeAk
                }).Cast<object>().ToList()
            };
            return Json(res);
        }

        [Route("{dataSlug}/detil-request/{id}")]
        [HttpGet]
        public ActionResult RequestChangeDetail(string dataSlug, string id) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            if (!AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.NonKpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) { 
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var data = drv.GetById(id);
            if (data == null) return HttpNotFound();

            var mdl = new RequestChangeDetailViewModel {
                Driver = drv,
                User = AppUser,
                Data = data
            };

            return View(mdl);
        }

        [Route("{dataSlug}/log-request/{id}")]
        [HttpGet]
        public ActionResult RequestChangeLog(string dataSlug, string id) {

            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            if (!AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.NonKpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var data = drv.GetById(id);
            if (data == null || data.KodeAk != AppUser.KpeiCompanyCode &&
                !AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) && !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
                return HttpNotFound();
            }

            var mdl = new RequestChangeLogViewModel {
                Data = data,
                Driver = drv,
                User = AppUser
            };

            return View(mdl);
        }

        [Route("{dataSlug}/log-request-dtt/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RequestChangeLogDtt(string dataSlug, string id, UserActionLogDttRequestForm req) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            //if (!AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
            //    !AppUser.NonKpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
            //    !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
            //    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            //}

            req.UserId = null;
            req.UserName = null;
            req.KodeAk = null;
            req.RelatedEntityName = CreateLogEntityName(drv);
            req.RelatedEntityId = id;

            var data = drv.GetById(id);
            if (data == null || data.KodeAk != AppUser.KpeiCompanyCode &&
                !AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) { 
                return HttpNotFound();
            }

            var pag = _ulog.ListLogsAsync(req).Result;
            var resp = new DttResponseForm {
                draw = req.draw,
                recordsTotal = pag.RecordsTotal,
                recordsFiltered = pag.RecordsFiltered,
                data = pag.Data.Select(x => new {
                    WaktuPerubahan = x.CreatedUtc.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss"),
                    DibuatOleh = $"{x.UserName} / {x.UserAknpCode}",
                    Aktivitas = x.Message
                }).Cast<object>().ToList()
            };

            return Json(resp);
        }

        private string CreateLogEntityName(BaseAknpDriver drv) {
            return $"DataAknp({drv.DataSlug})";
        }

        [Route("{dataSlug}/ubah-request/{id}")]
        [HttpGet]
        public ActionResult UpdateRequestChange(string dataSlug, string id) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            var data = drv.GetById(id);
            if (data == null) return HttpNotFound();

            if (drv.IsRegistrationData && data.Status == DataAknpRequestStatus.APPROVED_STATUS.Name && data.Draft != true) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            
            if (!(AppUser.NonKpeiModifierRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                  data.Status == DataAknpRequestStatus.NEW_STATUS.Name ||
                  AppUser.KpeiModifierRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                  data.Status != DataAknpRequestStatus.REJECTED_STATUS.Name)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var form = drv.InitForm(data);

            if (!AppUser.KpeiModifierRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                form.KodeAk = AppUser.KpeiCompanyCode;
                form.Status = data.Status;
                form.ActiveUtc = data.ActiveUtc;
            }

            var companies = _compSvc.AllKpeiCompanies();

            if (form.KodeAk != GlobalSetting.GuestAknpCode)
            {
                companies = companies.Where(x => x.Code != GlobalSetting.GuestAknpCode).ToArray();
            }

            if (data.Draft && !string.IsNullOrWhiteSpace(data.UserDraft))
            {
                TempData["DataStillDraft"] = true;
            }

            var mdl = new UpdateRequestChangeViewModel {
                Id = id,
                User = AppUser,
                Driver = drv,
                GlobalSetting = GlobalSetting,
                Form = form,
                Companies = companies,
                Statuses = DataAknpRequestStatus.All().ToArray()
            };
            return View(mdl);
        }

        [Route("{dataSlug}/ubah-request/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateRequestChange(string dataSlug, string id, BaseAknpForm form) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            var data = drv.GetById(id);
            if (data == null) return HttpNotFound();

            if (!(AppUser.NonKpeiModifierRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                  data.Status == DataAknpRequestStatus.NEW_STATUS.Name ||
                  AppUser.KpeiModifierRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
                  data.Status != DataAknpRequestStatus.REJECTED_STATUS.Name)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            // var ongoingData = drv.GetLatestNewOngoingData(form.KodeAk);
            // if (drv.DataSlug == DataRegistrasiAknpDriver.SLUG && ongoingData != null && form.KodeAk != GlobalSetting.GuestAknpCode)
            // {
            //     TempData["HaveNotyetApproved"] = true;
            //     return RedirectToAction("RequestChangeDetail", new {dataSlug = drv.DataSlug, ongoingData.Id});
            // }

            form = form ?? drv.InitForm(data);

            if (!AppUser.KpeiModifierRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                form.KodeAk = data.KodeAk;
                form.Status = data.Status;
                form.ActiveUtc = data.ActiveUtc;
            }

            ModelState.Clear();
            bool draft = false;
            if (TryValidateModel(form) || form.Draft == true) {
                BaseAknpData dat;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(5)
                }))
                {
                    dat = drv.GetById(id);
                    draft = dat.Draft;
                    drv.MapFromTo(form, dat);
                    if (form.Draft != true)
                    {
                        dat.UserDraft = null;
                        dat.Draft = false;
                    }
                    dat = drv.Update(dat);
                    scope.Complete();
                }

                if (dat.Draft != true && draft == true)
                {
                    _ulog.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.DATA_AK_PARTISIPAN,
                        $"Data {drv.DataTitle} baru dibuat secara manual.",
                        CreateLogEntityName(drv), dat.Id);

                    _usersvc.SendNotifNewAknp(dat.KodeAk, Url.Action("RequestChangeDetail", "DataAknp", new { dataSlug = dataSlug, id = dat.Id }, Request.Url.Scheme));
                }

                if (form.Draft == true)
                {
                    return RedirectToAction("ListRequest", new {dataSlug = drv.DataSlug});
                }

                if (dat.Draft != true && draft != true)
                {
                    
                    _ulog.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.DATA_AK_PARTISIPAN,
                        $"Perubahan data {drv.DataTitle} telah dilakukan.",
                        CreateLogEntityName(drv), dat.Id);

                    _usersvc.SendNotifUpdateAknp(form.KodeAk, Url.Action("RequestChangeDetail", "DataAknp", new { dataSlug = dataSlug, id = dat.Id }, Request.Url.Scheme));
                }

                return RedirectToAction("RequestChangeDetail", new {dataSlug = drv.DataSlug, id});
            }

            form.Draft = true;

            var mdl = new UpdateRequestChangeViewModel {
                Id = id,
                User = AppUser,
                GlobalSetting = GlobalSetting,
                Driver = drv,
                Form = form,
                Companies = _compSvc.AllKpeiCompanies(),
                Statuses = DataAknpRequestStatus.All().ToArray()
            };
            return View(mdl);
        }

        [Route("{dataSlug}/approve-request/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveRequestChange(string dataSlug, string id) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            var data = drv.GetById(id);
            if (data == null) return HttpNotFound();
            
            if (data.Approver1UserId == AppUser.Id || data.Approver2UserId == AppUser.Id) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (data.KodeAk == GlobalSetting.GuestAknpCode) {
                TempData["DataStillGuest"] = true;
                return RedirectToAction("RequestChangeDetail", new {dataSlug = drv.DataSlug, id});
            }

            DataAknpApproveResult stat;
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = IsolationLevel.Serializable,
                Timeout = TimeSpan.FromMinutes(3)
            })) {
                stat = drv.Approve(id, AppUser.Id);
                scope.Complete();
            }

            string statText;
            if (stat.NewStatus == DataAknpRequestStatus.ONGOING_STATUS) {
                statText = "diterima dan sedang melalui proses approval";
            } else if (stat.NewStatus == DataAknpRequestStatus.APPROVED_STATUS) {
                statText = "di-approve";
            } else {
                statText = $"di-{stat.NewStatus.Label.ToLowerInvariant()}";
            }

            _ulog.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.DATA_AK_PARTISIPAN,
                $"Request data {drv.DataTitle} telah {statText}.",
                CreateLogEntityName(drv), stat.Data.Id);

            if (stat.NewStatus == DataAknpRequestStatus.APPROVED_STATUS) {
                _usersvc.SendNotifApproveAknp(data.KodeAk, Url.Action("RequestChangeDetail", "DataAknp", new { dataSlug = dataSlug, id = stat.Data.Id }, Request.Url.Scheme));
            }

            TempData["ApproveSuccess"] = true;

            return RedirectToAction("RequestChangeDetail", new {dataSlug = drv.DataSlug, id});
        }

        [Route("{dataSlug}/reject-request/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RejectRequestChange(string dataSlug, string id, RejectRequestChangeViewModel mdl) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            if (!AppUser.KpeiApproverRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var data = drv.GetById(id);
            if (data == null) return HttpNotFound();

            ModelState.Clear();
            if (TryValidateModel(mdl)) {
                BaseAknpData dat;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    dat = drv.Reject(data.Id, mdl.RejectReason);
                    scope.Complete();
                }

                _ulog.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.DATA_AK_PARTISIPAN,
                    $"Request data {drv.DataTitle} telah di-reject.",
                    CreateLogEntityName(drv), dat.Id);


                var guestCode = _stg.GlobalSetting().GuestAknpCode;
                if (dat.KodeAk == guestCode) {
                    var form = dat as DataRegistrasi;
                    _usersvc.SendNotifRejectGuestRegistrasi(form.Email, Url.Action("RequestChangeDetail", "DataAknp", new { dataSlug = dataSlug, id = dat.Id }, Request.Url.Scheme), dat.RejectReason);
                } else {
                    _usersvc.SendNotifRejectAknp(dat.KodeAk, Url.Action("RequestChangeDetail", "DataAknp", new { dataSlug = dataSlug, id = dat.Id }, Request.Url.Scheme), dat.RejectReason);
                }


                TempData["RejectSuccess"] = true;
            } else {
                TempData["ValidationErrors"] =
                    ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToArray();
            }

            return RedirectToAction("RequestChangeDetail", new {dataSlug = drv.DataSlug, id});
        }

        [Route("{dataSlug}/daftar-riwayat")]
        [HttpGet]
        public ActionResult ListHistory(string dataSlug) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            if (!AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) && 
                !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var companies = _compSvc.AllKpeiCompanies();

            companies = companies.Where(x => x.Code != GlobalSetting.GuestAknpCode).ToArray();

            var mdl = new ListHistoryViewModel {
                User = AppUser,
                Driver = drv,
                Companies = companies,
                RequestStatuses = DataAknpRequestStatus.All().ToArray()
            };
            return View(mdl);
        }

        [Route("{dataSlug}/daftar-riwayat-dtt")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListHistoryDtt(string dataSlug, BaseAknpDttRequestForm req) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            //if (!AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) &&
            //    !AppUser.NonKpeiViewerAsKpei(CoreModules.DATA_AK_PARTISIPAN)) {
            //    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            //}

            var data = drv.List(req);
            var pag = new PaginatedResult<BaseAknpData>();
            pag.Initialize(data.All, data.Filtered, req.start, req.length);

            var res = new DttResponseForm {
                draw = req.draw,
                recordsTotal = pag.RecordsTotal,
                recordsFiltered = pag.RecordsFiltered,
                data = pag.Data.Select(x => MakeHistoryDttItem(drv, x)).Cast<object>().ToList()
            };
            return Json(res);
        }

        [Route("{dataSlug}/input-baru")]
        [HttpGet]
        public ActionResult CreateHistory(string dataSlug, string kodeAk) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            if (!AppUser.KpeiModifierRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            string initAk = dataSlug == DataRegistrasiAknpDriver.SLUG ? "null" : kodeAk;
            var form = drv.InitForm(initAk);
            var draftForm = drv.InitDraftForm(kodeAk, AppUser.Id);
            
            form.KodeAk = kodeAk;
            form.Status = DataAknpRequestStatus.APPROVED_STATUS.Name;
            form.ActiveUtc = DateTime.UtcNow;

            var companies = _compSvc.AllKpeiCompanies();

            companies = companies.Where(x => x.Code != GlobalSetting.GuestAknpCode).ToArray();

            if (draftForm.Draft == true)
            {
                return RedirectToAction("UpdateRequestChange", new {dataSlug = dataSlug, id = draftForm.Id});
            }
            var mdl = new CreateHistoryViewModel {
                User = AppUser,
                Driver = drv,
                Form = form,
                GlobalSetting = GlobalSetting,
                Companies = companies
            };
            return View(mdl);
        }

        [Route("{dataSlug}/input-baru")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateHistory(string dataSlug, string kodeAk, BaseAknpForm form) {
            var drv = _dataAknpDrvs.LoadDrivers().FirstOrDefault(x => x.IsMatchDataSlug(dataSlug));
            if (drv == null) return HttpNotFound();

            if (!AppUser.KpeiModifierRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var ongoingData = drv.GetLatestNewOngoingData(form.KodeAk);
            if (drv.DataSlug == DataRegistrasiAknpDriver.SLUG && ongoingData != null)
            {
                TempData["HaveNotyetApproved"] = true;
                return RedirectToAction("RequestChangeDetail", new {dataSlug = drv.DataSlug, ongoingData.Id});
            }

            if (form == null) {
                form = drv.InitForm(kodeAk);
                form.KodeAk = kodeAk;
                form.Status = DataAknpRequestStatus.APPROVED_STATUS.Name;
                form.ActiveUtc = DateTime.UtcNow;
            }

            ModelState.Clear();
            if (TryValidateModel(form) || form.Draft == true) {
                BaseAknpData dat;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(5)
                })) {
                    var formType = form.GetType();
                    var formCollProps = formType.GetProperties().Where(e => typeof(IEnumerable<object>).IsAssignableFrom(e.PropertyType));
                    foreach(var colProp in formCollProps) {
                        var colVal = colProp.GetValue(form) as IEnumerable<object>;
                        foreach (var item in colVal) {
                            var itemType = item.GetType();
                            var idProp = itemType.GetProperty("Id");
                            if (idProp != null) {
                                idProp.SetValue(item, Guid.NewGuid().ToString());
                            }
                        }
                    }

                    form.Id = Guid.NewGuid().ToString();
                    form.Status = DataAknpRequestStatus.APPROVED_STATUS.Name;

                    if (form.Draft == true)
                    {
                        form.UserDraft = AppUser.Id;
                    }

                    dat = Activator.CreateInstance(drv.DataClassType) as BaseAknpData;
                    drv.MapFromTo(form, dat);
                    
                    dat.Approver1UserId = AppUser.Id;
                    dat.Approver2UserId = AppUser.Id;

                    var result = drv.Create(dat);
                    scope.Complete();

                    dat = result;
                }

                if (dat.Draft != true)
                {
                    _ulog.LogUserActionAsync(System.Web.HttpContext.Current, CoreModules.DATA_AK_PARTISIPAN,
                        $"Data {drv.DataTitle} baru dibuat secara manual.",
                        CreateLogEntityName(drv), dat.Id);

                    _usersvc.SendNotifNewAknp(dat.KodeAk, Url.Action("RequestChangeDetail", "DataAknp", new { dataSlug = dataSlug, id = dat.Id }, Request.Url.Scheme));
                }

                return RedirectToAction("ListHistory", new {dataSlug = drv.DataSlug});
            }

            var companies = _compSvc.AllKpeiCompanies();

            companies = companies.Where(x => x.Code != GlobalSetting.GuestAknpCode).ToArray();

            var mdl = new CreateHistoryViewModel {
                User = AppUser,
                Driver = drv,
                GlobalSetting = GlobalSetting,
                Form = form,
                Companies = companies
            };
            return View(mdl);
        }

        private Dictionary<string, object> MakeHistoryDttItem(BaseAknpDriver driver, BaseAknpData data) {
            var result = new Dictionary<string, object>();

            foreach (var prop in driver.DataClassType.GetProperties().Where(x => x.CanRead)) {
                var attrs = prop.GetCustomAttributes(true);

                var aknpDispAttr = attrs.FirstOrDefault(x => x is AknpDisplayAttribute) as AknpDisplayAttribute;
                if (aknpDispAttr == null || !aknpDispAttr.ShowInHistoryTable) {
                    continue;
                }

                var dispAttr = attrs.FirstOrDefault(x => x is DisplayAttribute) as DisplayAttribute;
                var vm = new FieldViewModel {
                    User = AppUser,
                    Driver = driver,
                    Data = data,
                    PropertyValue = prop.GetValue(data),
                    ViewDefinition = new DataAknpViewDefinition(prop.Name) {
                        Order = dispAttr?.GetOrder() ?? 0,
                        Label = dispAttr?.Name ?? prop.Name,
                        HideFromAknp = false,
                        Section = dispAttr?.GetGroupName() ?? ""
                    }
                };
                if (string.IsNullOrWhiteSpace(aknpDispAttr.TemplateName)) {
                    result[prop.Name] =
                        this.RenderViewToString($"~/Views/DataAknp/_AknpColumnViews/_global.cshtml", vm, true)?.Trim();
                } else {
                    result[prop.Name] =
                        this.RenderViewToString($"~/Views/DataAknp/_AknpColumnViews/{aknpDispAttr.TemplateName}.cshtml",
                            vm, true)?.Trim();
                }
            }

            result[nameof(BaseAknpData.Status)] = data.Status;
            result[nameof(BaseAknpData.ActiveUtc)] = data.ActiveUtc?.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss");
            result[nameof(BaseAknpData.CreatedUtc)] = data.CreatedUtc.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss");
            result[nameof(BaseAknpData.KodeAk)] = data.KodeAk;
            result[nameof(BaseAknpData.Id)] = data.Id;
            
            result["NamaPerusahaanAK"] = _compSvc.AllKpeiCompanies()
                .FirstOrDefault(e => e.Code == data.KodeAk)?.Name ?? "";
            return result;
        }

        private Dictionary<string, object> MakeDetailDttItem(BaseAknpDriver driver, BaseAknpData data) {
            var result = new Dictionary<string, object>();

            foreach (var prop in driver.DataClassType.GetProperties().Where(x => x.CanRead)) {
                var attrs = prop.GetCustomAttributes(true);

                var aknpDispAttr = attrs.FirstOrDefault(x => x is AknpDisplayAttribute) as AknpDisplayAttribute;
                if (aknpDispAttr == null || !aknpDispAttr.ShowInDetailTable) {
                    continue;
                }

                var dispAttr = attrs.FirstOrDefault(x => x is DisplayAttribute) as DisplayAttribute;
                var vm = new FieldViewModel {
                    User = AppUser,
                    Driver = driver,
                    Data = data,
                    PropertyValue = prop.GetValue(data),
                    ViewDefinition = new DataAknpViewDefinition(prop.Name) {
                        Order = dispAttr?.GetOrder() ?? 0,
                        Label = dispAttr?.Name ?? prop.Name,
                        HideFromAknp = false,
                        Section = dispAttr?.GetGroupName() ?? ""
                    }
                };
                if (string.IsNullOrWhiteSpace(aknpDispAttr.TemplateName)) {
                    result[prop.Name] =
                        this.RenderViewToString($"~/Views/DataAknp/_AknpColumnViews/_global.cshtml", vm, true)?.Trim();
                } else {
                    result[prop.Name] =
                        this.RenderViewToString($"~/Views/DataAknp/_AknpColumnViews/{aknpDispAttr.TemplateName}.cshtml",
                            vm, true)?.Trim();
                }
            }
            result[nameof(BaseAknpData.KodeAk)] = data.KodeAk;
            result[nameof(BaseAknpData.Id)] = data.Id;
            result["NamaPerusahaanAK"] = _compSvc.AllKpeiCompanies()
                .FirstOrDefault(e => e.Code == data.KodeAk)?.Name ?? "";
            return result;
        }
    }
}