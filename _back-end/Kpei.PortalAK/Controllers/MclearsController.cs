﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModMclears.Repos;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.ViewModels;
using Kpei.PortalAK.DataAccess.Services;
using System.Net;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.ViewModels.Mclears;
using Kpei.PortalAK.DataAccess.ModMclears.Models;
using Kpei.PortalAK.DataAccess.ModMclears.Forms;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using Kpei.PortalAK.Features;
using System.Transactions;
using System;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;

namespace Kpei.PortalAK.Controllers {
    [Authorize]
    [RoutePrefix("layanan-m-clears")]
    public class MclearsController : Controller {
        private readonly IKpeiCompanyService _compsvc;
        private readonly IMclearsRepository _repo;
        private readonly IUserActionLogService _ulog;
        private readonly ISettingRepository _stgrepo;
        private readonly IUserService _usersvc;
        private AppUser AppUser => User.Identity.GetAppUser();

        public MclearsController(IKpeiCompanyService compsvc, IMclearsRepository repo, IUserActionLogService ulog, ISettingRepository stgrepo, IUserService usersvc) {
            _compsvc = compsvc;
            _repo = repo;
            _ulog = ulog;
            _stgrepo = stgrepo;
            _usersvc = usersvc;
        }

        [Route("")]
        [HttpGet]
        public ActionResult Index() {
            if (!AppUser.KpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.LAYANAN_M_CLEARS)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (AppUser.KpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS) || AppUser.NonKpeiViewerAsKpei(CoreModules.LAYANAN_M_CLEARS)) {
                return IndexKpei();
            }
            if (AppUser.IsAknp) {
                return IndexNonKpei();
            }
            return HttpNotFound();
        }

        private ActionResult IndexKpei() {
            if (!AppUser.KpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.LAYANAN_M_CLEARS)) { 
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var mdl = new IndexViewModel {
                Companies = _compsvc.AllKpeiCompanies(),
                Perubahan = MclearsJenisPerubahan.All(),
                AppUser = AppUser
            };
            return View("IndexKpei", mdl);
        }

        private ActionResult IndexNonKpei() {
            if (!AppUser.NonKpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var mdl = new IndexViewModel {
                Companies = _compsvc.AllKpeiCompanies(),
                Perubahan = MclearsJenisPerubahan.All(),
                AppUser = AppUser
            };
            return View("IndexNonKpei", mdl);
        }

        [Route("list-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListKpei(MclearsDttRequestForm req) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS) && 
                !AppUser.NonKpeiViewerAsKpei(CoreModules.LAYANAN_M_CLEARS)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var resp = new DttResponseForm {
                draw = req.draw
            };
            var result = _repo.List(req);
            resp.recordsFiltered = result.RecordsFiltered;
            resp.recordsTotal = result.RecordsTotal;
            resp.data = result.Data.Select(x => new {
                x.KodeAK,
                x.StatusRequest,
                NamaPerusahaan = _compsvc.AllKpeiCompanies().FirstOrDefault(e => e.Code == x.KodeAK).Name,
                JlhPeserta = _repo.GetPesertaFromRequest(x.Id).Count(),
                JlhRequestLayanan = _repo.NotNullLayananRequest(x.Id),
                CreatedUtc = x.CreatedUtc.ConvertUtcToWib().ToString("dd MMM yyyy"),
                DetailUrl = Url.Action("Detail", new { id = x.Id, index = 0 })
            }).Cast<object>().ToList();

            return Json(resp);
        }

        [Route("list-non-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListNonKpei(MclearsDttRequestForm req) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS) && !AppUser.NonKpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            req.KodeAk = AppUser.KpeiCompanyCode;

            var resp = new DttResponseForm {
                draw = req.draw
            };
            var result = _repo.List(req);
            resp.recordsFiltered = result.RecordsFiltered;
            resp.recordsTotal = result.RecordsTotal;
            resp.data = result.Data.Select(x => new {
                x.StatusRequest,
                CreatedUtc = x.CreatedUtc.ConvertUtcToWib().ToString("dd MMM yyyy"),
                DetailUrl = Url.Action("Detail", new { id = x.Id })
            }).Cast<object>().ToList();

            return Json(resp);
        }

        [Route("request-layanan")]
        [HttpGet]
        public ActionResult Subscribe() {
            if (!AppUser.KpeiModifierRightFor(CoreModules.LAYANAN_M_CLEARS) && !AppUser.NonKpeiModifierRightFor(CoreModules.LAYANAN_M_CLEARS)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var mdl = new InputNewSubscribeViewModel {
                Request = new UpdateMclearsRequestForm(),
            };
            var maxPeserta = _stgrepo.GlobalSetting().MaxSubscribePesertaMclears;
            mdl.Request.ListPeserta = new List<UpdateMclearsPesertaForm>();
            mdl.Request.ListPeserta.Add(new UpdateMclearsPesertaForm());
            return View("InputNewSubscribe", mdl);
        }

        [Route("request-layanan")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Subscribe(UpdateMclearsRequestForm form) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.LAYANAN_M_CLEARS) && !AppUser.NonKpeiModifierRightFor(CoreModules.LAYANAN_M_CLEARS)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            ModelState.Clear();

            if (form.ListPeserta?.Count > 0) {
                for(int i = 0; i < form.ListPeserta.Count; i++) {
                    if(AppUser.IsAknp && form.ListPeserta[i].KodeAK != AppUser.KpeiCompanyCode) {
                        form.ListPeserta[i].KodeAK = AppUser.KpeiCompanyCode;
                    }
                    if (form.ListPeserta[i].Nama == null &&
                        form.ListPeserta[i].Jabatan == null &&
                        form.ListPeserta[i].Email == null &&
                        form.ListPeserta[i].HP == null && !HasSelectedLayanan(form.ListPeserta[i])) {
                        form.ListPeserta.Remove(form.ListPeserta[i]);
                        i--;
                    }
                }
            }


            TryValidateModel(form);
            
            if (ModelState.IsValid && ModelState.IsValidField("ListPeserta")) {
                form.KodeAK = AppUser.KpeiCompanyCode;
                MclearsRequest newRequest = new MclearsRequest();
                List<MclearsPeserta> listPeserta = new List<MclearsPeserta>();

                var hasValidData = false;

                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    for (int i = 0; i < form.ListPeserta.Count; i++) {
                        if (form.ListPeserta[i].Nama == null || 
                            form.ListPeserta[i].Jabatan == null || 
                            form.ListPeserta[i].Email == null ||
                            form.ListPeserta[i].HP == null) {
                            continue;
                        }
                        if (!hasValidData) {
                            //create request if at least one data is valid.
                            hasValidData = true;
                            newRequest = _repo.Create(form);
                        }
                        form.ListPeserta[i].RequestId = newRequest.Id;
                        listPeserta.Add(_repo.CreatePeserta(form.ListPeserta[i]));
                    }
                    scope.Complete();
                };

                if (!hasValidData) {
                    ModelState.AddModelError("notValid", "Data yang anda masukkan tidak valid. Mohon dicek kembali");
                    var model = new InputNewSubscribeViewModel {
                        Request = form
                    };
                    return View("InputNewSubscribe", model);
                }

                _ulog.LogUserActionAsync(HttpContext, CoreModules.LAYANAN_M_CLEARS, "Mengajukan subscribe baru", "LayananMclears", newRequest.Id);
                _usersvc.SendNotifNewMclears(newRequest.KodeAK, Url.Action("Detail", "Mclears", new { id = newRequest.Id }, Request.Url.Scheme));

                return RedirectToAction("Detail", new { id = newRequest.Id, index = 0 });
            }
            var mdl = new InputNewSubscribeViewModel {
                Request = form
            };
            return View("InputNewSubscribe", mdl);
        }
        
        [Route("detil/{id}/{index?}")]
        [HttpGet]
        public ActionResult Detail(string id, int index = 0) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS) && 
                !AppUser.NonKpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.LAYANAN_M_CLEARS)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var request = _repo.Get(id);
            var list = _repo.GetPesertaFromRequest(id).OrderBy(e => e.CreatedUtc).ToList();
            if (request == null || list == null || list.Count == 0) {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var totalItem = list.Count;
            var peserta = list[index];
            if (request == null || peserta == null) {
                return HttpNotFound();
            }
            var mdl = new DetailViewModel {
                Request = request,
                Peserta = peserta,
                AppUser = AppUser,
                Index = index,
                TotalItem = totalItem,
                TotalAlert = _repo.NotNullLayananAlert(peserta),
                TotalOnRequest = _repo.NotNullLayananOnRequest(peserta)
            };

            return View(mdl);
        }

        [Route("edit/{id}")]
        [HttpGet]
        public ActionResult Edit(string id) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.LAYANAN_M_CLEARS) && !AppUser.NonKpeiModifierRightFor(CoreModules.LAYANAN_M_CLEARS)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var request = _repo.Get(id);
            var peserta = _repo.GetPesertaFromRequest(id);
            if (request == null || peserta == null) {
                return HttpNotFound();
            }
            var mdl = new UpdateViewModel();
            mdl.Request = new UpdateMclearsRequestForm();
            mdl.Request.Init(request, peserta);
            return View(mdl);
        }

        [Route("edit/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, UpdateMclearsRequestForm form) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.LAYANAN_M_CLEARS) && !AppUser.NonKpeiModifierRightFor(CoreModules.LAYANAN_M_CLEARS)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var request = _repo.Get(id);
            if (request == null) {
                return HttpNotFound();
            }

            var peserta = _repo.GetPesertaFromRequest(id);
            if (peserta == null) {
                return HttpNotFound();
            }
            var oldId = peserta.Select(e => e.Id).ToArray();

            if (form == null) {
                form = new UpdateMclearsRequestForm();
                form.Init(request, peserta);
            }

            for (int i = 0; i < form.ListPeserta.Count; i++) {
                form.ListPeserta[i].KodeAK = form.KodeAK;
            }

            form.StatusRequest = MclearsStatusRequest.NEW_STATUS.Name;

            if (ModelState.IsValid) {
                MclearsRequest newRequest = new MclearsRequest();
                List<MclearsPeserta> listPeserta = new List<MclearsPeserta>();
                var hasValidData = false;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    for (int i = 0; i < form.ListPeserta.Count; i++) {
                        for (int j = 0; j < oldId.Count(); j++) {
                            if (Array.IndexOf(form.ListPeserta.Select(x => x.Id).ToArray(), oldId[j]) < 0) {
                                _repo.DeletePeserta(oldId[j]);
                            }
                        }
                        if (!hasValidData) {
                            hasValidData = true;
                            newRequest = _repo.Update(form);
                        }
                        form.ListPeserta[i].RequestId = form.Id;
                        listPeserta.Add(_repo.UpdatePeserta(form.ListPeserta[i]));
                    }
                    scope.Complete();
                };

                if (!hasValidData) {
                    ModelState.AddModelError("notValid", "Data yang anda masukkan tidak valid. Mohon dicek kembali");
                    var model = new UpdateViewModel {
                        Request = form
                    };
                    return View(model);
                }

                _ulog.LogUserActionAsync(HttpContext, CoreModules.LAYANAN_M_CLEARS, "Melakukan perubahan pada request", "LayananMclears", form.Id);
                _usersvc.SendNotifUpdateMclears(form.KodeAK, Url.Action("Detail", "Mclears", new { id = newRequest.Id }, Request.Url.Scheme));

                return RedirectToAction("Detail", new { id = form.Id, index = 0 });
            }
            var mdl = new UpdateViewModel {
                Request = form
            };
            return View(mdl);
        }

        public bool HasSelectedLayanan(UpdateMclearsPesertaForm dat) {
            if (dat.AlertACS == null &&
                dat.AlertBACS == null &&
                dat.AlertBroadcast == null &&
                dat.AlertCLA == null &&
                dat.AlertCPA == null &&
                dat.AlertDCDA == null &&
                dat.AlertDCRA == null &&
                dat.AlertFCOLWA == null &&
                dat.AlertHPHFA == null &&
                dat.AlertHTR == null &&
                dat.AlertHTS == null &&
                dat.AlertL1 == null &&
                dat.AlertL3 == null &&
                dat.AlertL7 == null &&
                dat.AlertMCA == null &&
                dat.AlertMCCA == null &&
                dat.AlertMKBDA == null &&
                dat.AlertOCA == null &&
                dat.AlertPC == null &&
                dat.AlertPS == null &&
                dat.AlertTL5 == null &&
                dat.AlertTLP == null &&
                dat.AlertTLS == null &&
                dat.OnRequestCPR == null &&
                dat.OnRequestDCDR == null &&
                dat.OnRequestDCRR == null &&
                dat.OnRequestHaircut == null &&
                dat.OnRequestHPHFR == null &&
                dat.OnRequestHTR == null &&
                dat.OnRequestHTS == null &&
                dat.OnRequestMCR == null &&
                dat.OnRequestMKBDR == null &&
                dat.OnRequestOCR == null &&
                dat.OnRequestPC == null &&
                dat.OnRequestPPR == null &&
                dat.OnRequestPS == null) {
                return false;
            }
            return true;
        }



        [Route("done/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Done(string id) {
            if (!AppUser.IsKpei || !AppUser.KpeiApproverRightFor(CoreModules.LAYANAN_M_CLEARS)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var req = _repo.Approve(id, AppUser.Id);
            if (req == null) {
                return HttpNotFound();
            }
            _ulog.LogUserActionAsync(HttpContext, CoreModules.LAYANAN_M_CLEARS, "Menyelesaikan request layanan", "LayananMclears", id);
            
            _usersvc.SendNotifActionMclears(_repo.Get(id).KodeAK, Url.Action("Detail", "Mclears", new { id = id }, Request.Url.Scheme));

            return RedirectToAction("Detail", new { id = id, index = 0 });
        }

        [Route("log/{id}")]
        [HttpGet]
        public ActionResult Log(string id) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS) &&
                !AppUser.NonKpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.LAYANAN_M_CLEARS)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var req = _repo.Get(id);
            if (req == null || 
                !AppUser.KpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS) && 
                !AppUser.NonKpeiViewerAsKpei(CoreModules.LAYANAN_M_CLEARS) &&
                req.KodeAK != AppUser.KpeiCompanyCode) {
                return HttpNotFound();
            }

            var mdl = new ViewModels.Mclears.LogViewModel {
                MclearsRequest = req,
                MclearsPeserta = _repo.GetPesertaFromRequest(id)
            };

            return View(mdl);
        }

        [Route("list-log/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListLog(string id, UserActionLogDttRequestForm req) {
            req.UserId = null;
            req.UserName = null;
            req.KodeAk = null;
            req.RelatedEntityName = "LayananMclears";
            req.RelatedEntityId = id;

            var dbData = _repo.Get(id);
            if (dbData == null || 
                !AppUser.KpeiViewerRightFor(CoreModules.LAYANAN_M_CLEARS) && 
                !AppUser.NonKpeiViewerAsKpei(CoreModules.LAYANAN_M_CLEARS) &&
                dbData.KodeAK != AppUser.KpeiCompanyCode) {
                return HttpNotFound();
            }

            var pag = _ulog.ListLogsAsync(req).Result;
            var resp = new DttResponseForm {
                draw = req.draw,
                recordsTotal = pag.RecordsTotal,
                recordsFiltered = pag.RecordsFiltered,
                data = pag.Data.Select(x => new {
                    WaktuPerubahan = x.CreatedUtc.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss"),
                    DibuatOleh = $"{x.UserName} / {x.UserAknpCode}",
                    Aktivitas = x.Message
                }).Cast<object>().ToList()
            };

            return Json(resp);
        }
    }
}
