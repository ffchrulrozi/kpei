﻿using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.ModDataAknp.Drivers;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.Controllers {
    [Authorize]
    public class HomeController : Controller {

        private AppUser AppUser => User.Identity.GetAppUser();

        [HttpGet]
        public ActionResult Index() {
            if (AppUser.NonKpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN) ||
                AppUser.KpeiViewerRightFor(CoreModules.DATA_AK_PARTISIPAN)) {
                return RedirectToAction("Profile", "DataAknp");
            }

            if (AppUser.IsGuestAknp) {
                return RedirectToAction("RequestChange", "DataAknp", new {dataSlug = DataRegistrasiAknpDriver.SLUG});
            }

            return View();
        }
    }
}