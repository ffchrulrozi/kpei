﻿using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.DataAccess.ModLayananBaru.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.ModLayananBaru.Repos;
using System.Transactions;

namespace Kpei.PortalAK.Controllers
{
    [RoutePrefix("settings/layanan-baru")]
    [Authorize]
    public class LayananBaruController : Controller
    {
        private readonly AppDbContext _db;
        private readonly ILayananBaruRepository _repository;
        private readonly IUserActionLogService _ulog;

        private AppUser AppUser => AppUser.CreateFromIdentity(User.Identity);

        public LayananBaruController(ILayananBaruRepository repo, AppDbContext db, IUserActionLogService ulog)
        {
            _repository = repo;
            _db = db;
            _ulog = ulog;
        }

        [HttpGet]
        [Route("")]
        public ActionResult Index(string tipe)
        {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            return View();
        }

        [HttpPost]
        [Route("")]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LayananBaruForm form)
            {

            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (ModelState.IsValid)
            {
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(1)
                }))
                {
                    _repository.UpdateLayananBaru(form);
                    scope.Complete();
                }
            }
            return RedirectToAction("Index", "LayananBaru", new { tipe = form.TipeMemberAK });
        }

        [HttpGet]
        [Route("get-layanan-baru-by-tipe")]
        public ActionResult GetLayananBaruByTipe(string tipe)
        {
            if (!AppUser.KpeiAdminRightFor(CoreModules.DATA_AK_PARTISIPAN))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (tipe == "AKU" || tipe == "AKI")
            {
                return Json(_repository.GetLayananBaruByTipeMember(tipe), JsonRequestBehavior.AllowGet);
            }

            return Json(_repository.GetLayananBaruByTipeMemberPartisipan(tipe), JsonRequestBehavior.AllowGet);
        }
    }
}