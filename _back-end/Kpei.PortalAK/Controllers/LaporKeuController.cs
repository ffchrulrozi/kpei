﻿using System;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using Kpei.PortalAK.DataAccess.ModKeuangan.Forms;
using Kpei.PortalAK.DataAccess.ModKeuangan.Models;
using Kpei.PortalAK.DataAccess.ModKeuangan.Repos;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.Features;
using Kpei.PortalAK.ViewModels.Keuangan;

namespace Kpei.PortalAK.Controllers {
    [Authorize]
    [RoutePrefix("laporan-keuangan")]
    public class LaporKeuController : Controller {
        private readonly IKpeiCompanyService _compsvc;
        private readonly IKeuanganRepository _repo;
        private readonly IUserActionLogService _ulog;
        private readonly IUserService _usersvc;

        public LaporKeuController(IKeuanganRepository repo, IKpeiCompanyService compsvc, IUserActionLogService ulog, IUserService usersvc) {
            _repo = repo;
            _compsvc = compsvc;
            _ulog = ulog;
            _usersvc = usersvc;
        }

        private AppUser AppUser => User.Identity.GetAppUser();

        [Route("")]
        [HttpGet]
        public ActionResult Index() {
            if (AppUser.IsKpei || AppUser.NonKpeiViewerAsKpei(CoreModules.LAPORAN_KEUANGAN)) {
                return IndexKpei();
            }

            if (AppUser.IsAknp) {
                return IndexNonKpei();
            }

            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        private ActionResult IndexKpei() {
            if (!AppUser.KpeiViewerRightFor(CoreModules.LAPORAN_KEUANGAN) && !AppUser.NonKpeiViewerAsKpei(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var mdl = new IndexViewModel {
                Companies = _compsvc.AllKpeiCompanies(),
                Statuses = LaporanKeuanganStatus.All(),
                Periods = PeriodeLaporanKeuangan.All(),
                AppUser = AppUser
            };
            return View("IndexKpei", mdl);
        }

        private ActionResult IndexNonKpei() {
            if (!AppUser.NonKpeiViewerRightFor(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var mdl = new IndexViewModel {
                Companies = _compsvc.AllKpeiCompanies(),
                Statuses = LaporanKeuanganStatus.All(),
                Periods = PeriodeLaporanKeuangan.All(),
                AppUser = AppUser
            };
            return View("IndexNonKpei", mdl);
        }

        [Route("list-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListKpei(LaporanKeuanganDttRequestForm req) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.LAPORAN_KEUANGAN) && !AppUser.NonKpeiViewerAsKpei(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var resp = new DttResponseForm {
                draw = req.draw
            };

            var result = _repo.List(req);
            resp.recordsTotal = result.RecordsTotal;
            resp.recordsFiltered = result.RecordsFiltered;
                try
                {
                    resp.data = result.Data.Select(x => new {
                        x.KodeAK,
                        NamaPerusahaan = _compsvc.AllKpeiCompanies().FirstOrDefault(e => e.Code == x.KodeAK) != null ? _compsvc.AllKpeiCompanies().FirstOrDefault(e => e.Code == x.KodeAK).Name : "",
                        Periode = PeriodeLaporanKeuangan.All().FirstOrDefault(p => p.Name == x.Periode)?.Label ?? x.Periode,
                        x.Tahun,
                        TanggalSubmit = x.CreatedUtc.ConvertUtcToWib().ToString("dd MMM yyyy"),
                        x.Id,
                        CR = (x.CR * 100).ToString("F2") + "%",
                        ROA = (x.ROA * 100).ToString("F2") + "%",
                        ROE = (x.ROE * 100).ToString("F2") + "%",
                        NetProfit = (x.NetProfit * 100).ToString("F2") + "%",
                        DAR = (x.DAR * 100).ToString("F2") + "%",
                        DER = (x.DER * 100).ToString("F2") + "%",
                        Status = x.StatusRequest,
                        DetailUrl = Url.Action("Detail", new { id = x.Id })
                    }).Cast<object>().ToList();
                }
                catch (Exception excp) {
                int x = 10;
                }
                
            return Json(resp);
        }

        [Route("list-non-kpei")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListNonKpei(LaporanKeuanganDttRequestForm req) {
            if (!AppUser.NonKpeiViewerRightFor(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var resp = new DttResponseForm {
                draw = req.draw
            };
            
            req.KodeAk = AppUser.KpeiCompanyCode;

            var result = _repo.List(req);
            resp.recordsTotal = result.RecordsTotal;
            resp.recordsFiltered = result.RecordsFiltered;
            resp.data = result.Data.Select(x => new {
                x.KodeAK,
                Periode = PeriodeLaporanKeuangan.All().FirstOrDefault(p => p.Name == x.Periode)?.Label ?? x.Periode,
                x.Tahun,
                TanggalSubmit = x.CreatedUtc.ConvertUtcToWib().ToString("dd MMM yyyy"),
                x.Id,
                Status = x.StatusRequest,
                DetailUrl = Url.Action("Detail", new {id = x.Id})
            }).Cast<object>().ToList();

            return Json(resp);
        }

        [Route("detil/{id}")]
        [HttpGet]
        public ActionResult Detail(string id) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.LAPORAN_KEUANGAN) &&
                !AppUser.NonKpeiViewerRightFor(CoreModules.LAPORAN_KEUANGAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

           var lap = _repo.Get(id);
            if (lap == null || !AppUser.KpeiViewerRightFor(CoreModules.LAPORAN_KEUANGAN) && lap.KodeAK != AppUser.KpeiCompanyCode) {
                return HttpNotFound();
            }

            var mdl = new DetilViewModel {
                Companies = _compsvc.AllKpeiCompanies(),
                Statuses = LaporanKeuanganStatus.All(),
                Periods = PeriodeLaporanKeuangan.All(),
                TheUser = AppUser,
                Laporan = lap,
            };
            return View(mdl);
        }

        [Route("input-new")]
        [HttpGet]
        public ActionResult InputNew() {
            if (!AppUser.KpeiModifierRightFor(CoreModules.LAPORAN_KEUANGAN) &&
                !AppUser.NonKpeiModifierRightFor(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var mdl = new InputNewViewModel {
                Companies = _compsvc.AllKpeiCompanies(),
                Statuses = LaporanKeuanganStatus.All(),
                Periods = PeriodeLaporanKeuangan.All(),
                Form = new CreateLaporanKeuanganForm(),
                TheUser = AppUser
            };

            return View(mdl);
        }

        [Route("input-new")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InputNew(CreateLaporanKeuanganForm form) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.LAPORAN_KEUANGAN) &&
                !AppUser.NonKpeiModifierRightFor(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            form = form ?? new CreateLaporanKeuanganForm();
            if (!AppUser.IsKpei) {
                form.KodeAK = AppUser.KpeiCompanyCode;
            }

            if (ModelState.IsValid) {
                KeuanganLaporan newLap;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(3)
                })) {
                    newLap = _repo.Create(form);
                    scope.Complete();
                }

                _ulog.LogUserActionAsync(HttpContext, CoreModules.LAPORAN_KEUANGAN, "Menginput laporan keuangan baru",
                    "LaporanKeuangan", newLap.Id);

                _usersvc.SendNotifNewLapKeu(newLap.KodeAK, Url.Action("Detail", "LaporKeu", new { id = newLap.Id }, Request.Url.Scheme));

                return RedirectToAction("Index");
            }

            var mdl = new InputNewViewModel {
                Companies = _compsvc.AllKpeiCompanies(),
                Statuses = LaporanKeuanganStatus.All(),
                Periods = PeriodeLaporanKeuangan.All(),
                Form = form,
                TheUser = AppUser
            };

            return View(mdl);
        }


        [Route("edit/{id}")]
        [HttpGet]
        public ActionResult Edit(string id) {
            if (!AppUser.KpeiModifierRightFor(CoreModules.LAPORAN_KEUANGAN) &&
                !AppUser.NonKpeiModifierRightFor(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var lap = _repo.Get(id);
            if (lap == null || !AppUser.IsKpei && lap.KodeAK != AppUser.KpeiCompanyCode) {
                return HttpNotFound();
            }

            var f = new UpdateLaporanKeuanganForm();
            f.Init(lap);

            var mdl = new EditViewModel {
                Companies = _compsvc.AllKpeiCompanies(),
                Statuses = LaporanKeuanganStatus.All(),
                Periods = PeriodeLaporanKeuangan.All(),
                Form = f,
                TheUser = AppUser
            };

            return View(mdl);
        }

        [Route("edit/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, UpdateLaporanKeuanganForm form) {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
            try
            {
                if (!AppUser.KpeiModifierRightFor(CoreModules.LAPORAN_KEUANGAN) &&
    !AppUser.NonKpeiModifierRightFor(CoreModules.LAPORAN_KEUANGAN))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                }

                var lap = _repo.Get(id);
                if (lap == null || !AppUser.IsKpei && lap.KodeAK != AppUser.KpeiCompanyCode)
                {
                    return HttpNotFound();
                }

                if (form == null)
                {
                    form = new UpdateLaporanKeuanganForm();
                    form.Init(lap);
                }
                else if (!AppUser.IsKpei)
                {
                    form.KodeAK = AppUser.KpeiCompanyCode;
                }

                if (ModelState.IsValid)
                {
                    KeuanganLaporan editedLap;
                    using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions
                    {
                        IsolationLevel = IsolationLevel.Serializable,
                        Timeout = TimeSpan.FromMinutes(3)
                    }))
                    {
                        editedLap = _repo.Update(form);
                        scope.Complete();
                    }

                    _ulog.LogUserActionAsync(HttpContext, CoreModules.LAPORAN_KEUANGAN, "Mengedit laporan keuangan",
                        "LaporanKeuangan", editedLap.Id);

                    _usersvc.SendNotifUpdateLapKeu(editedLap.KodeAK, Url.Action("Detail", "LaporKeu", new { id = editedLap.Id }, Request.Url.Scheme));

                    return RedirectToAction("Detail", new { id = editedLap.Id });
                }

                var mdl = new EditViewModel
                {
                    Companies = _compsvc.AllKpeiCompanies(),
                    Statuses = LaporanKeuanganStatus.All(),
                    Periods = PeriodeLaporanKeuangan.All(),
                    Form = form,
                    TheUser = AppUser
                };

                return View(mdl);
            }
            catch (Exception excp) {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
        }

        [Route("approve/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Approve(string id) {
            if (!AppUser.KpeiApproverRightFor(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var lap = _repo.Get(id);
            if (lap == null || !AppUser.IsKpei && lap.KodeAK != AppUser.KpeiCompanyCode) {
                return HttpNotFound();
            }

            if (lap.StatusRequest == LaporanKeuanganStatus.APPROVED_STATUS.Name ||
                lap.StatusRequest == LaporanKeuanganStatus.REJECTED_STATUS.Name) {
                return RedirectToAction("Detail", new {id = lap.Id});
            }

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = IsolationLevel.Serializable,
                Timeout = TimeSpan.FromMinutes(3)
            })) {
                lap = _repo.Approve(id, AppUser.Id);
                scope.Complete();
            }

            _ulog.LogUserActionAsync(HttpContext, CoreModules.LAPORAN_KEUANGAN, "Meng-approve laporan keuangan",
                "LaporanKeuangan", lap.Id);

            if (lap.StatusRequest == LaporanKeuanganStatus.APPROVED_STATUS.Name) {
                _usersvc.SendNotifActionLapKeu(lap.KodeAK, Url.Action("Detail", "LaporKeu", new { id = lap.Id }, Request.Url.Scheme));
            }

            return RedirectToAction("Detail", new {id = lap.Id});
        }

        [Route("reject/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Reject(string id, RejectViewModel mdl) {
            if (!AppUser.KpeiApproverRightFor(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var lap = _repo.Get(id);
            if (lap == null || !AppUser.IsKpei && lap.KodeAK != AppUser.KpeiCompanyCode) {
                return HttpNotFound();
            }

            if ((lap.StatusRequest != LaporanKeuanganStatus.NEW_STATUS.Name || lap.StatusRequest != LaporanKeuanganStatus.ONGOING_STATUS.Name) && mdl.RejectReason == null) {
                return RedirectToAction("Detail", new { id = lap.Id });
            }

            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = IsolationLevel.Serializable,
                Timeout = TimeSpan.FromMinutes(3)
            })) {
                lap = _repo.Reject(id, mdl.RejectReason);
                scope.Complete();
            }

            _ulog.LogUserActionAsync(HttpContext, CoreModules.LAPORAN_KEUANGAN, "Me-reject laporan keuangan",
                "LaporanKeuangan", lap.Id);

            _usersvc.SendNotifRejectLapKeu(lap.KodeAK, Url.Action("Detail", "LaporKeu", new { id = lap.Id }, Request.Url.Scheme), lap.RejectReason);

            return RedirectToAction("Detail", new {id = lap.Id});
        }

        [Route("log/{id}")]
        [HttpGet]
        public ActionResult Log(string id) {
            if (!AppUser.KpeiViewerRightFor(CoreModules.LAPORAN_KEUANGAN) &&
                !AppUser.NonKpeiViewerRightFor(CoreModules.LAPORAN_KEUANGAN) &&
                !AppUser.NonKpeiViewerAsKpei(CoreModules.LAPORAN_KEUANGAN)) {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var lap = _repo.Get(id);
            if (lap == null || 
                !(AppUser.KpeiViewerRightFor(CoreModules.LAPORAN_KEUANGAN) || AppUser.NonKpeiViewerAsKpei(CoreModules.LAPORAN_KEUANGAN)) && 
                lap.KodeAK != AppUser.KpeiCompanyCode) {
                return HttpNotFound();
            }

            var mdl = new LogViewModel {
                Laporan = lap
            };

            return View(mdl);
        }

        [Route("list-log/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListLog(string id, UserActionLogDttRequestForm req) {
            req.UserId = null;
            req.UserName = null;
            req.KodeAk = null;
            req.RelatedEntityName = "LaporanKeuangan";
            req.RelatedEntityId = id;

            var lap = _repo.Get(id);
            if (lap == null || 
                !(AppUser.KpeiViewerRightFor(CoreModules.LAPORAN_KEUANGAN) || AppUser.NonKpeiViewerAsKpei(CoreModules.LAPORAN_KEUANGAN)) && 
                lap.KodeAK != AppUser.KpeiCompanyCode) {
                return HttpNotFound();
            }

            var pag = _ulog.ListLogsAsync(req).Result;
            var resp = new DttResponseForm {
                draw = req.draw,
                recordsTotal = pag.RecordsTotal,
                recordsFiltered = pag.RecordsFiltered,
                data = pag.Data.Select(x => new {
                    WaktuPerubahan = x.CreatedUtc.ConvertUtcToWib().ToString("dd/MM/yyyy HH:mm:ss"),
                    DibuatOleh = $"{x.UserName} / {x.UserAknpCode}",
                    Aktivitas = x.Message
                }).Cast<object>().ToList()
            };

            return Json(resp);
        }
    }
}