﻿using JrzAsp.Lib.DirectViewRenderer;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using Kpei.PortalAK.DataAccess.ModDataAknp.Attributes;
using Kpei.PortalAK.DataAccess.ModLaporan.Data;
using Kpei.PortalAK.DataAccess.ModLaporan.Forms;
using Kpei.PortalAK.DataAccess.ModLaporan.Services;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.Features;
using Kpei.PortalAK.Features.DataAknpViewSystem;
using Kpei.PortalAK.ViewModels.Laporan;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

//for debug
using System.Diagnostics;
using System.Reflection;
using Kpei.PortalAK.DataAccess.ModDataAknp.Repos;
using Kpei.PortalAK.DataAccess.Database;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;

namespace Kpei.PortalAK.Controllers
{
    [Authorize]
    [RoutePrefix("laporan")]
    public class LaporanController : Controller
    {
        private readonly ILaporanDriverStore _laporanDrv;
        private readonly IKpeiCompanyService _compSvc;
        private readonly AppDbContext _db;
        
        private static readonly CultureInfo indoCulture = new CultureInfo("id-ID");

        public LaporanController(ILaporanDriverStore laporanDrv, IKpeiCompanyService compSvc, AppDbContext db) {
            _laporanDrv = laporanDrv;
            _compSvc = compSvc;
            _db = db;
        }

        private AppUser AppUser => User.Identity.GetAppUser();
        
        // GET: Laporan
        [Route("{dataSlug}")]
        [HttpGet]
        public ActionResult Laporan(string dataSlug = null)
        {
            if (!AppUser.IsKpei) return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            var drv = _laporanDrv.LoadDrivers().FirstOrDefault(x => x.DataSlug == dataSlug);
            if (drv == null) return HttpNotFound();

            if (dataSlug == "partisipan") {
                //only get participant companies
                var participant = 
                    _db.DataTipeMember
                    .Where(
                        e => e.KategoriMember == RegistrasiKategoriMember.MEMBER_PARTISIPAN.Name 
                        && e.Status == DataAknpRequestStatus.APPROVED_STATUS.Name)
                    .Select(e => e.KodeAk).ToList();
                ViewBag.Comps = _compSvc.AllKpeiCompanies().Where(e => participant.Contains(e.Code)).ToList();
            }

            return View("Laporan", new LaporanViewModel {
                Driver = drv,
            });
        }

        [Route("{dataSlug}/dtt")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DetailDtt(string dataSlug, BaseLaporanDttRequestForm req) {
            var swThis = Stopwatch.StartNew();
            var sw = Stopwatch.StartNew();

            if (req.StatusBursaKPEI != null && req.StatusBursaKPEI.Equals("status-kpei"))
            {
                dataSlug = "status-kpei";
            }
            
            var drv = _laporanDrv.LoadDrivers().FirstOrDefault(x => x.DataSlug == dataSlug);
            Debug.WriteLine(MethodInfo.GetCurrentMethod() + ": LoadDrivers (" + sw.Elapsed.ToString() + ") @" + swThis.Elapsed.ToString());
            sw.Reset();
            if (drv == null) return HttpNotFound();
            Debug.WriteLine("DetailDtt: Condition Driver (" + sw.Elapsed.ToString() + ") @" + swThis.Elapsed.ToString());
            sw.Reset();
            var data = drv.List(req);
            Debug.WriteLine("List elapsed " + sw.Elapsed.ToString() + " @" + swThis.Elapsed.ToString());

            var pag = new PaginatedResult<BaseLaporan>();
            sw.Reset();
            pag.Initialize(data.All, data.Filtered, req.start, req.length);
            Debug.WriteLine("Initialize elapsed " + sw.Elapsed.ToString() + " @" + swThis.Elapsed.ToString());

            var res = new DttResponseForm {
                draw = req.draw,
                recordsTotal = pag.RecordsTotal,
                recordsFiltered = pag.RecordsFiltered,
                data = pag.Data.Select(x => MakeHistoryDttItem(drv, x)).Cast<object>().ToList()
            };

            Debug.WriteLine("DetailDtt finished @" + swThis.Elapsed.ToString());
            return Json(res);
        }

        private Dictionary<string, object> MakeHistoryDttItem(BaseLaporanDriver driver, BaseLaporan data) {
            var swThis = Stopwatch.StartNew();
            var sw = Stopwatch.StartNew();
            var result = new Dictionary<string, object>();
            sw.Reset();
            var props = driver.DataClassType.GetProperties().Where(x => x.CanRead);
            sw.Reset();
            var comps = _compSvc.AllKpeiCompanies();

            sw.Reset();
            foreach (var prop in props) {
                var attrs = prop.GetCustomAttributes(true);

                var aknpDispAttr = attrs.FirstOrDefault(x => x is AknpDisplayAttribute) as AknpDisplayAttribute;
                if (aknpDispAttr == null || !aknpDispAttr.ShowInDetailTable) {
                    continue;
                }

                var dispAttr = attrs.FirstOrDefault(x => x is DisplayAttribute) as DisplayAttribute;
                var vm = new FieldViewModel {
                    //User = AppUser,
                    Driver = driver,
                    Data = data,
                    PropertyValue = prop.GetValue(data),
                    ViewDefinition = new DataAknpViewDefinition(prop.Name) {
                        Order = dispAttr?.GetOrder() ?? 0,
                        Label = dispAttr?.Name ?? prop.Name,
                        HideFromAknp = false,
                        Section = dispAttr?.GetGroupName() ?? ""
                    }
                };
                if (string.IsNullOrWhiteSpace(aknpDispAttr.TemplateName)) {
                    result[prop.Name] =
                        this.RenderViewToString($"~/Views/Laporan/_ColumnViews/_global.cshtml", vm, true)?.Trim();
                } else {
                    result[prop.Name] =
                        this.RenderViewToString($"~/Views/Laporan/_ColumnViews/{aknpDispAttr.TemplateName}.cshtml",
                            vm, true)?.Trim();
                }
            }
            Debug.WriteLine("Loop elapsed " + sw.Elapsed.ToString() + " @" + swThis.Elapsed.ToString());
            result[nameof(BaseLaporan.ActiveUtc)] = data.ActiveUtc?.ToString("dd MMMM yyyy HH:mm:ss", indoCulture);
            result[nameof(BaseLaporan.AkActiveUtc)] = string.Format("<b>[{0}]</b> <i>{1}</i>", data.StatusKpei ?? "No Data", data.ActiveUtc?.ToString("dd MMMM yyyy HH:mm:ss", indoCulture));
            result[nameof(BaseLaporan.CreatedUtc)] = data.CreatedUtc.ToString("dd MMMM yyyy HH:mm:ss", indoCulture);
            // result[nameof(BaseLaporan.StatusKpei)] = data.StatusKpei;
            result[nameof(BaseLaporan.StatusBursa) + "KPEI"] = data.StatusBursa ?? data.StatusKpei;
            result[nameof(BaseLaporan.KodeAk)] = data.KodeAk;
            result[nameof(BaseLaporan.Id)] = data.Id;
            sw.Reset();
            result["NamaPerusahaan"] = comps.FirstOrDefault(e => e.Code == data.KodeAk)?.Name ?? "<i>No Data</i>";
            // result["NamaPerusahaan"] = comps.Where(e => e.HistoryDate < data.ActiveUtc).OrderByDescending(e => e.HistoryDate).FirstOrDefault(e => e.Code == data.KodeAk)?.Name ?? comps.FirstOrDefault(e => e.Code == data.KodeAk)?.MemberName ?? "<i>No Data</i>";
            Debug.WriteLine("Getting Nama Perusahaan elapsed " + sw.Elapsed.ToString() + " @" + swThis.Elapsed.ToString());
            Debug.WriteLine("finished MakeHistoryDttItem @" + swThis.Elapsed.ToString());
            return result;
        }
    }
}