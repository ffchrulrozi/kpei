﻿using System;
using System.Net;
using System.Transactions;
using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.ModSettings.Forms;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.UserIdentity;

namespace Kpei.PortalAK.Controllers {
    [RoutePrefix("settings/pincode")]
    [Authorize]
    public class PincodeSettingController : Controller {
        private readonly ISettingRepository _stgrepo;

        public PincodeSettingController(ISettingRepository stgrepo) {
            _stgrepo = stgrepo;
        }

        /// <inheritdoc />
        protected override void OnAuthorization(AuthorizationContext filterContext) {
            var user = AppUser.CreateFromIdentity(User.Identity);
            var right = user?.GetRight();
            if (right?.ForModule[CoreModules.REQUEST_PINCODE].KpeiAdmin != true) {
                filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                return;
            }

            base.OnAuthorization(filterContext);
        }

        [HttpGet]
        [Route("")]
        public ActionResult Index() {
            var mdl = _stgrepo.PincodeSetting(false);
            return View(mdl);
        }

        [Route("")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(PincodeSettingForm mdl) {
            if (ModelState.IsValid) {
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable,
                    Timeout = TimeSpan.FromMinutes(1)
                })) {
                    _stgrepo.UpdatePincodeSetting(mdl);
                    scope.Complete();
                }
            }

            return View(mdl);
        }
    }
}