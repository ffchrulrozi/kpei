﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Forms.JqueryDataTable;
using Kpei.PortalAK.DataAccess.ModUserRight.Forms;
using Kpei.PortalAK.DataAccess.ModUserRight.Models;
using Kpei.PortalAK.DataAccess.ModUserRight.Repos;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.ViewModels.UserRightSetting;

namespace Kpei.PortalAK.Controllers {
    [RoutePrefix("settings/user-right")]
    [Authorize]
    public class UserRightSettingController : Controller {
        private readonly ISysLogger _errlog;
        private readonly IUserRightRepository _repo;

        public UserRightSettingController(IUserRightRepository repo, ISysLogger errlog) {
            _repo = repo;
            _errlog = errlog;
        }

        /// <inheritdoc />
        protected override void OnAuthorization(AuthorizationContext filterContext) {
            var user = AppUser.CreateFromIdentity(User.Identity);
            var right = user?.GetRight();
            if (right?.ForModule[CoreModules.CORE_SYSTEM].KpeiAdmin != true) {
                filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                return;
            }

            base.OnAuthorization(filterContext);
        }

        [HttpGet]
        [Route("")]
        public ActionResult Index(bool isKpei = true) {
            var mdl = new IndexViewModel {
                IsKpei = isKpei,
                Modules = CoreModules.IdAndNameTuples
            };
            return View(mdl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("list")]
        public ActionResult List(UserRightDttRequestForm req) {
            var resp = new DttResponseForm();
            try {
                var result = _repo.GetMemberRights(req);

                resp.draw = req.draw;
                resp.recordsTotal = result.RecordsTotal;
                resp.recordsFiltered = result.RecordsFiltered;
                resp.data = result.Data.Select(x => new MemberUserInfoPerModule(x, req.ModuleId))
                    .Cast<object>().ToList();

                return Json(resp);
            } catch (Exception ex) {
                _errlog.LogError(ex);
                return Json(new DttResponseErrorForm(resp) {
                    error = $"{ex.Message} ({ex.GetType().Name})"
                });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("toggle")]
        public ActionResult Toggle(UserRightToggleForm mdl) {
            var result = _repo.ChangeRight(mdl.UserId, mdl.ModuleId, mdl.RightName, mdl.Check);
            return Json(new {
                HasRight = result
            });
        }
    }
}