﻿using System;
using System.Configuration;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Elmah;
using Kpei.PortalAK.Features.ModelBinding;
using Ninject;
using Ninject.Web.Common.WebHost;

namespace Kpei.PortalAK {
    public class Global : NinjectHttpApplication {
        public static string DbMigratorPassword = ConfigurationManager.AppSettings["PortalAK:DbMigratorPassword"];

        protected override void OnApplicationStarted() {
            base.OnApplicationStarted();

            AntiForgeryConfig.CookieName = "__RequestVerificationTokenPortalAK";

            ModelBinderProviders.BinderProviders.Add(new BindableAbstractClassModelBinderProvider());

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected override IKernel CreateKernel() {
            var kernel = new StandardKernel();
            kernel.Load(typeof(Global).Assembly);
            return kernel;
        }

        public static void LogError(Exception ex) {
            ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
}