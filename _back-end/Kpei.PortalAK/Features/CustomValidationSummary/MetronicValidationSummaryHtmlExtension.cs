﻿using System.Web.Mvc;
using JrzAsp.Lib.RazorTools;

namespace Kpei.PortalAK.Features.CustomValidationSummary {
    public static class MetronicValidationSummaryHtmlExtension {
        public static MvcHtmlString MetronicValidationSummary(this HtmlHelper html) {
            return html.PartialFor(html.ViewData.Model, "~/Views/Shared/_ValidationSummary.cshtml", x => x);
        }
    }
}