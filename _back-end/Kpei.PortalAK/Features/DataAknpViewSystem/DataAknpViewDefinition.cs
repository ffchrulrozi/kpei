﻿using Kpei.PortalAK.Features.FormFieldRazor;

namespace Kpei.PortalAK.Features.DataAknpViewSystem {
    public class DataAknpViewDefinition {
        private readonly bool _ctorUseTemplate;

        public DataAknpViewDefinition(string propName, string templateName) {
            PropertyName = propName;
            _ctorUseTemplate = true;
            TemplateName = templateName;
            // use template
        }

        public DataAknpViewDefinition(string propName, FormFieldSetting setting) {
            PropertyName = propName;
            FormFieldSetting = setting;
            // use form field
        }

        public DataAknpViewDefinition(string propName) {
            PropertyName = propName;
            // use default
        }

        public string PropertyName { get; }
        public string TemplateName { get; }
        public FormFieldSetting FormFieldSetting { get; }

        public string Label { get; set; }
        public int Order { get; set; }
        public string Section { get; set; }
        public bool HideFromAknp { get; set; }

        public bool UseTemplate => _ctorUseTemplate && !string.IsNullOrWhiteSpace(TemplateName);
        public bool UseFormField => !UseTemplate && FormFieldSetting != null;
        public bool UseDefault => !UseTemplate && !UseFormField;
    }
}