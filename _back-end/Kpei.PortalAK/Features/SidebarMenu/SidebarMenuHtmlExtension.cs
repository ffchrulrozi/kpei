﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Kpei.PortalAK.Features.SidebarMenu {
    public static class SidebarMenuHtmlExtension {
        public static List<string> SelectedMenuTags(this HtmlHelper html) {
            const string itemKey = "SelectedMenuTagsForCurrentRequest";
            var tags = HttpContext.Current.Items[itemKey] as List<string>;
            if (tags == null) {
                tags = new List<string>();
                HttpContext.Current.Items[itemKey] = tags;
            }

            return tags;
        }

        public static bool SelectedMenuHasAnyTags(this HtmlHelper html, params string[] tags) {
            if (tags == null) {
                tags = new string[0];
            }

            foreach (var tag in tags) {
                if (html.SelectedMenuTags().Contains(tag)) {
                    return true;
                }
            }

            return false;
        }

        public static bool SelectedMenuHasAllTags(this HtmlHelper html, params string[] tags) {
            if (tags == null) {
                tags = new string[0];
            }

            var hasAll = true;

            foreach (var tag in tags) {
                hasAll = html.SelectedMenuTags().Contains(tag) && hasAll;
                if (!hasAll) break;
            }

            return hasAll;
        }
    }
}