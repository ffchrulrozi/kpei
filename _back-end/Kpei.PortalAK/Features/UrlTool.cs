﻿using System;
using System.Web;

namespace Kpei.PortalAK.Features {
    public static class UrlTool {

        public static string RootWebsiteUrl =>
            HttpContext.Current.Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);

        public static string BaseAppUrlAbsolute {
            get {
                var req = HttpContext.Current.Request;
                var absPath = (RootWebsiteUrl + req.ApplicationPath).Trim('/') + "/";
                return absPath;
            }
        }

        public static string BaseAppUrl {
            get {
                var relPath = BaseAppUrlAbsolute.Substring(RootWebsiteUrl.Length).Trim('/') + "/";
                return relPath;
            }
        }
    }
}