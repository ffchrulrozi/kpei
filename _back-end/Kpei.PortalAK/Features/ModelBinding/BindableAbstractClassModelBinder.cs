﻿using System.Web.Mvc;
using JrzAsp.Lib.TypeUtilities;
using Kpei.PortalAK.DataAccess.Core;

namespace Kpei.PortalAK.Features.ModelBinding {
    public class BindableAbstractClassModelBinder : DefaultModelBinder {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {
            var value = bindingContext.ValueProvider.GetValue(
                (bindingContext.FallbackToEmptyPrefix ? "" : bindingContext.ModelName + ".") +
                nameof(IBindableClass.ClassTypeFullName)
            );

            if (value != null) {
                var clsFn = value.AttemptedValue;
                var type = TypesCache.GetTypeByFullName(clsFn);
                if (type != null) {
                    bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(null, type);
                    return base.BindModel(controllerContext, bindingContext);
                }
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}