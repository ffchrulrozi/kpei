﻿using System;
using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.Core;

namespace Kpei.PortalAK.Features.ModelBinding {
    public class BindableAbstractClassModelBinderProvider : IModelBinderProvider {
        public IModelBinder GetBinder(Type modelType) {
            var bac = typeof(IBindableClass);
            if (bac.IsAssignableFrom(modelType) && modelType != bac) {
                return new BindableAbstractClassModelBinder();
            }

            return null;
        }
    }
}