﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.Features.FormFieldRazor.Settings
{
    public class ArrTipeMemberAkiFieldSetting : FormFieldSetting
    {
        public override string TemplateId => "arr-tipe-member-aki";

        public int MaxItems { get; set; }

        public string UploadFolder { get; set; }
    }
}