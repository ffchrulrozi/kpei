﻿namespace Kpei.PortalAK.Features.FormFieldRazor.Settings {
    public class ArrDirectorPersonFieldSetting : FormFieldSetting {
        public ArrDirectorPersonFieldSetting() {
            MaxItems = 15;
        }

        /// <inheritdoc />
        public override string TemplateId => "arr-director-person";

        public int MaxItems { get; set; }

        public string UploadFolder { get; set; }
    }
}