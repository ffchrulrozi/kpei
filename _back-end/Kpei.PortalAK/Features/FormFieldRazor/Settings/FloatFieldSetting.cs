﻿namespace Kpei.PortalAK.Features.FormFieldRazor.Settings {
    public class FloatFieldSetting : FormFieldSetting {
        /// <inheritdoc />
        public override string TemplateId => "float";
    }
}