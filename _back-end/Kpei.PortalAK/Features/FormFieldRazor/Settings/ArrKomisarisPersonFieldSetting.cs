﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.Features.FormFieldRazor.Settings {
    public class ArrKomisarisPersonFieldSetting : FormFieldSetting {
        public ArrKomisarisPersonFieldSetting() {
            MaxItems = 15;
        }

        /// <inheritdoc />
        public override string TemplateId => "arr-komisaris-person";

        public int MaxItems { get; set; }

        public string UploadFolder { get; set; }
    }
}