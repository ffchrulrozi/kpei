﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.Features.FormFieldRazor.Settings
{
    public class ArrTipeMemberAkuFieldSetting : FormFieldSetting
    {
        public override string TemplateId => "arr-tipe-member-aku";

        public int MaxItems { get; set; }

        public string UploadFolder { get; set; }
    }
}