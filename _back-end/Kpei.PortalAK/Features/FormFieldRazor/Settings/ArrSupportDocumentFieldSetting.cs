﻿namespace Kpei.PortalAK.Features.FormFieldRazor.Settings {
    public class ArrSupportDocumentFieldSetting : FormFieldSetting {
        public ArrSupportDocumentFieldSetting() {
            MaxItems = 15;
        }

        /// <inheritdoc />
        public override string TemplateId => "arr-sup-doc";

        public int MaxItems { get; set; }

        public string UploadFolder { get; set; }
    }
}