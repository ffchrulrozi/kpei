﻿namespace Kpei.PortalAK.Features.FormFieldRazor.Settings {
    public class IntFieldSetting : FormFieldSetting {
        /// <inheritdoc />
        public override string TemplateId => "int";

        public int MaxLength { get; set; } = -1;
    }
}