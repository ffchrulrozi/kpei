﻿namespace Kpei.PortalAK.Features.FormFieldRazor.Settings {
    public class ArrMclearsSubscriberFieldSetting : FormFieldSetting {
        public ArrMclearsSubscriberFieldSetting() {
            MaxItems = 15;
        }

        /// <inheritdoc />
        public override string TemplateId => "arr-mclears-subscriber";

        public int MaxItems { get; set; }
    }
}