﻿namespace Kpei.PortalAK.Features.FormFieldRazor.Settings {
    public class DateTimeFieldSetting : FormFieldSetting {
        public DateTimeFieldSetting() {
            Style = DateTimeFieldStyle.Complete;
        }

        /// <inheritdoc />
        public override string TemplateId => "datetime";

        public DateTimeFieldStyle Style { get; set; }

        public bool UseVerticalFormFieldFormat { get; set; }
    }

    public enum DateTimeFieldStyle {
        Complete = 0,
        DateOnly = 1,
        TimeOnly = 2
    }
}