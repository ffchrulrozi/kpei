﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;

namespace Kpei.PortalAK.Features.FormFieldRazor.Settings
{
    public class ArrAdministrasiKeanggotaanTradingMemberFieldSetting : FormFieldSetting
    {
        public ArrAdministrasiKeanggotaanTradingMemberFieldSetting()
        {
            MaxItems = 20;
        }

        public override string TemplateId => "arr-administrasikeanggotaan-trading-member";

        public int MaxItems { get; set; }

        public string UploadFolder { get; set; }

        public RegistrasiAKTradingMember[] ListNamaTM { get; set; }
    }
}