﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.Features.FormFieldRazor.Settings
{
    public class Select2FieldSetting : FormFieldSetting
    {
        public Select2FieldSetting()
        {
            EmptyOptionLabel = "";
        }
        /// <inheritdoc />
        public override string TemplateId => "select2";
        public IEnumerable<object> Options { get; set; }
        public Func<object, string> OptionLabelGetter { get; set; }
        public Func<object, string> OptionValueGetter { get; set; }
        public bool AddEmptyOption { get; set; }
        public string EmptyOptionLabel { get; set; }
    }
}