﻿namespace Kpei.PortalAK.Features.FormFieldRazor.Settings {
    public class ArrPemegangSahamFieldSetting : FormFieldSetting {
        public ArrPemegangSahamFieldSetting() {
            MaxItems = 15;
        }

        /// <inheritdoc />
        public override string TemplateId => "arr-pemegang-saham";

        public int MaxItems { get; set; }
    }
}