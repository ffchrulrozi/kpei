﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kpei.PortalAK.Features.FormFieldRazor.Settings {
    public class ArrPartisipanFieldSetting : FormFieldSetting {

        public override string TemplateId => "arr-partisipan";

        public int MaxItems { get; set; }

        public string UploadFolder { get; set; }
    }
}