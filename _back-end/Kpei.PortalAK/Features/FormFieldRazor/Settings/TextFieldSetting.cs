﻿namespace Kpei.PortalAK.Features.FormFieldRazor.Settings {
    public class TextFieldSetting : FormFieldSetting {
        public TextFieldSetting() {
            Style = TextFieldStyle.Simple;
        }

        /// <inheritdoc />
        public override string TemplateId => "text";
        public int MaxLength { get; set; } = -1;

        public TextFieldStyle Style { get; set; }
    }

    public enum TextFieldStyle {
        Simple = 0,
        Textarea,
        RichText,
        Password
    }
}