﻿namespace Kpei.PortalAK.Features.FormFieldRazor.Settings {
    public class ArrPersonFieldSetting : FormFieldSetting {
        public ArrPersonFieldSetting() {
            MaxItems = 15;
        }

        /// <inheritdoc />
        public override string TemplateId => "arr-person";

        public int MaxItems { get; set; }
    }
}