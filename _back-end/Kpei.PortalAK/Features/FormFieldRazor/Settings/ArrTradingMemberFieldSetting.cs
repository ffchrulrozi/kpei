﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models.Registrasi;

namespace Kpei.PortalAK.Features.FormFieldRazor.Settings
{
    public class ArrTradingMemberFieldSetting : FormFieldSetting
    {
        public ArrTradingMemberFieldSetting()
        {
            MaxItems = 20;
        }

        public override string TemplateId => "arr-trading-member";

        public int MaxItems { get; set; }

        public string UploadFolder { get; set; }

    }
}