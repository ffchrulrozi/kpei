﻿using System;
using System.Collections.Generic;

namespace Kpei.PortalAK.Features.FormFieldRazor.Settings {
    public class SelectFieldSetting : FormFieldSetting {
        public SelectFieldSetting() {
            EmptyOptionLabel = "-kosong-";
        }
        /// <inheritdoc />
        public override string TemplateId => "select";
        public IEnumerable<object> Options { get; set; }
        public Func<object, string> OptionLabelGetter { get; set; }
        public Func<object, string> OptionValueGetter { get; set; }
        public bool AddEmptyOption { get; set; }
        public string EmptyOptionLabel { get; set; }
    }
}