﻿namespace Kpei.PortalAK.Features.FormFieldRazor {
    public abstract class FormFieldSetting {
        public abstract string TemplateId { get; }
        public string InputName { get; set; }
        public string Label { get; set; }
        public string Placeholder { get; set; }
        public string HelpText { get; set; }
        public string CssClass { get; set; }
        public string HtmlId { get; set; }
    }
}