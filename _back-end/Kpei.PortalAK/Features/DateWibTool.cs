﻿using System;
using System.Linq;

namespace Kpei.PortalAK.Features {
    public static class DateWibTool {
        public static readonly TimeZoneInfo WIB_TIME_ZONE = TimeZoneInfo.GetSystemTimeZones()
            .First(x => x.Id == "SE Asia Standard Time");

        public static DateTime ConvertUtcToWib(this DateTime dt) {
            var utc = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            return TimeZoneInfo.ConvertTimeFromUtc(utc, WIB_TIME_ZONE);
        }

        public static DateTime ConvertWibToUtc(this DateTime dt) {
            var wib = DateTime.SpecifyKind(dt, DateTimeKind.Unspecified);
            var conv = TimeZoneInfo.ConvertTimeToUtc(wib, WIB_TIME_ZONE);
            var utc = DateTime.SpecifyKind(conv, DateTimeKind.Utc);
            return utc;
        }
    }
}