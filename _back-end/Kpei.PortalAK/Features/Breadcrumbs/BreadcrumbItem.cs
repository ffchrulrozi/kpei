﻿namespace Kpei.PortalAK.Features.Breadcrumbs {
    public class BreadcrumbItem {
        public string Label { get; set; }
        public string Href { get; set; }
    }
}