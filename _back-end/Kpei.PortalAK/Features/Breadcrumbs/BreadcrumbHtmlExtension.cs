﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Kpei.PortalAK.Features.Breadcrumbs {
    public static class BreadcrumbHtmlExtension {
        public static List<BreadcrumbItem> GetBreadcrumbs(this HtmlHelper html) {
            const string breadcrumbsItemKey = "BreadcrumbsForCurrentRequest";
            var reqBcms = HttpContext.Current.Items[breadcrumbsItemKey] as List<BreadcrumbItem>;
            if (reqBcms == null) {
                reqBcms = new List<BreadcrumbItem>();
                HttpContext.Current.Items[breadcrumbsItemKey] = reqBcms;
            }

            return reqBcms;
        }
    }
}