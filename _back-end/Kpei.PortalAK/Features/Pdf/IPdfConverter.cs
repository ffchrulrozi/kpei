﻿using System.Collections.Generic;
using System.IO;

namespace Kpei.PortalAK.Features.Pdf {
    public interface IPdfConverter {
        byte[] ConvertFromHtmlToPdf(string htmlString, IEnumerable<string> cssRelativePaths,
            float pageWidthMillimeter = 210, float pageHeightMillimeter = 297,
            float marginLeftMillimeter = 20, float marginTopMillimeter = 20,
            float marginRightMillimeter = 20, float marginBottomMillimeter = 20);
    }
}