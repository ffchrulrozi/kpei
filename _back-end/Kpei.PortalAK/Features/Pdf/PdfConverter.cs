﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Hosting;
using HtmlAgilityPack;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

namespace Kpei.PortalAK.Features.Pdf {
    public class PdfConverter : IPdfConverter {
        /// <inheritdoc />
        public byte[] ConvertFromHtmlToPdf(string htmlString, IEnumerable<string> cssRelativePaths,
            float pageWidthMillimeter = 210, float pageHeightMillimeter = 297,
            float marginLeftMillimeter = 20, float marginTopMillimeter = 20,
            float marginRightMillimeter = 20, float marginBottomMillimeter = 20) {

            byte[] result = null;

            Func<float, float> mmToPt = Utilities.MillimetersToPoints;
            
            using (var pdfStream = new MemoryStream())
            using (var doc = new Document())
            using (var writer = PdfWriter.GetInstance(doc, pdfStream))
            using (var xhtmlStream = new MemoryStream())
            using (var cssStream = new MemoryStream()) {

                var cssRelPaths = cssRelativePaths ?? new string[0];
                foreach (var cssRp in cssRelPaths) {
                    var cssAbp = HostingEnvironment.MapPath(cssRp);
                    if (cssAbp == null) continue;
                    var cssData = File.ReadAllBytes(cssAbp);
                    cssStream.Write(cssData, 0, cssData.Length);
                }

                cssStream.Seek(0, SeekOrigin.Begin);

                doc.Open();
                doc.SetPageSize(new Rectangle(mmToPt(pageWidthMillimeter), mmToPt(pageHeightMillimeter)));
                doc.SetMargins(mmToPt(marginLeftMillimeter), mmToPt(marginRightMillimeter),
                    mmToPt(marginTopMillimeter), mmToPt(marginBottomMillimeter));

                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(htmlString);
                htmlDoc.OptionOutputAsXml = true;
                htmlDoc.Save(xhtmlStream, Encoding.UTF8);

                xhtmlStream.Seek(0, SeekOrigin.Begin);

                XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, xhtmlStream, cssStream);

                doc.Close();

                result = pdfStream.ToArray();
            }

            return result ?? new byte[0];
        }
    }
}