﻿using System.Web.Http;
using Elmah.Contrib.WebApi;

namespace Kpei.PortalAK {
    public static class WebApiConfig {
        public static void Register(HttpConfiguration config) {
            // Web API configuration and services
            config.Filters.Add(new ElmahHandleErrorApiAttribute());

            // Web API routes
            config.MapHttpAttributeRoutes();
        }
    }
}