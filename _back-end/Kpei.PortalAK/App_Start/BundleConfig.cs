﻿using System.Web.Optimization;

namespace Kpei.PortalAK {
    public static class BundleConfig {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles) {
            // Lodash
            bundles.Add(new ScriptBundle("~/bundles/vendors/lodash").Include(
                "~/assets/custom/vendors/lodash/lodash.js"));

            // VueJS
            bundles.Add(new ScriptBundle("~/bundles/vendors/vue").Include(
                "~/assets/custom/vendors/vuejs/vue.js"));

            // Clockpicker
            bundles.Add(new StyleBundle("~/bundles/vendors/clockpicker-css").Include(
                "~/assets/custom/vendors/clockpicker/bootstrap-clockpicker.css"));
            bundles.Add(new ScriptBundle("~/bundles/vendors/clockpicker-js").Include(
                "~/assets/custom/vendors/clockpicker/bootstrap-clockpicker.js"));
        }
    }
}