﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Kpei.PortalAK {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}/{index}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional, index = UrlParameter.Optional}
            );
        }
    }
}