﻿using System.Security.Claims;
using System.Web.Helpers;
using Kpei.Member.SsoClient;
using PortalAK;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace PortalAK {
    public class Startup {
        public void Configuration(IAppBuilder app) {
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;

            app.UseKpeiSsoCookieAuthentication(new SsoAuthConfig {
                AuthenticationType = "PortalAkDanPartisipanWebAppCookie",
                OnBuildingUserClaims = (claims) => {
                    // noop, only debug helper
                }
            });
        }
    }
}