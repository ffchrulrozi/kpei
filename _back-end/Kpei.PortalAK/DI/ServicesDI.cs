﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Kpei.PortalAK.DataAccess.Core;
using Kpei.PortalAK.DataAccess.Services;
using Kpei.PortalAK.DataAccess.UserIdentity;
using Kpei.PortalAK.Features.Pdf;
using Ninject.Modules;
using Ninject.Web.Common;

namespace Kpei.PortalAK.DI {
    public class ServicesDI : NinjectModule {
        /// <inheritdoc />
        public override void Load() {
            DepResolver.GetInstanceFunc = () => new PortalAKDepResolver();
            
            Kernel.Bind<IKpeiCompanyService>().To<KpeiCompanyService>().InRequestScope();
            Kernel.Bind<IUserService>().To<UserService>().InRequestScope();
            Kernel.Bind<IEmailService>().To<EmailService>().InRequestScope();
            Kernel.Bind<IUserActionLogService>().To<UserActionLogService>().InRequestScope();
            Kernel.Bind<IPdfConverter>().To<PdfConverter>().InRequestScope();
        }
    }

    public class PortalAKDepResolver : IDepResolver {

        /// <inheritdoc />
        public object GetService(Type serviceType) {
            return DependencyResolver.Current.GetService(serviceType);
        }

        /// <inheritdoc />
        public IEnumerable<object> GetServices(Type serviceType) {
            return DependencyResolver.Current.GetServices(serviceType);
        }
    }
}