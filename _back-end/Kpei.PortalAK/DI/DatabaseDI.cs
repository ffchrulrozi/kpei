﻿using Kpei.PortalAK.DataAccess.Database;
using Ninject.Modules;
using Ninject.Web.Common;

namespace Kpei.PortalAK.DI {
    public class DatabaseDI : NinjectModule {
        public override void Load() {
            Kernel.Bind<AppDbContext>().ToSelf().InRequestScope();
            Kernel.Bind<MemberDbContext>().ToSelf().InRequestScope();
        }
    }
}