﻿using Kpei.PortalAK.DataAccess.Services;
using Ninject.Modules;
using Ninject.Web.Common;

namespace Kpei.PortalAK.DI {
    public class SysLoggerDI : NinjectModule {
        /// <inheritdoc />
        public override void Load() {
            Kernel.Bind<ISysLogger>().To<DefaultSysLogger>().InRequestScope();
            DefaultSysLogger.LogErrorFunc = Global.LogError;
        }
    }
}