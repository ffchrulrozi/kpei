﻿using Kpei.PortalAK.DataAccess.DbModels;
using Kpei.PortalAK.DataAccess.ModDataAknp.Drivers;
using Kpei.PortalAK.DataAccess.ModDataAknp.Models;
using Kpei.PortalAK.DataAccess.ModDataAknp.Repos;
using Kpei.PortalAK.DataAccess.ModDataAknp.Services;
using Kpei.PortalAK.DataAccess.ModKeuangan.Repos;
using Kpei.PortalAK.DataAccess.ModLaporan.Forms;
using Kpei.PortalAK.DataAccess.ModLaporan.Services;
using Kpei.PortalAK.DataAccess.ModLayananBaru.Repos;
using Kpei.PortalAK.DataAccess.ModAdministrasiKeanggotaan.Repos;
using Kpei.PortalAK.DataAccess.ModMclears.Repos;
using Kpei.PortalAK.DataAccess.ModPincode.Repos;
using Kpei.PortalAK.DataAccess.ModSettings.Repos;
using Kpei.PortalAK.DataAccess.ModSosialisasi.Repos;
using Kpei.PortalAK.DataAccess.ModSurvei.Repos;
using Kpei.PortalAK.DataAccess.ModTradingMember.Repos;
using Kpei.PortalAK.DataAccess.ModUserRight.Repos;
using Kpei.PortalAK.Features.DataAknpViewSystem;
using Ninject.Modules;
using Ninject.Web.Common;

namespace Kpei.PortalAK.DI {
    public class ReposDI : NinjectModule {
        /// <inheritdoc />
        public override void Load() {
            UserRight();
            Settings();
            DataAknp();
            Sosialisasi();
            Keuangan();
            Mclears();
            Pincode();
            Survei();
            LaporanDrivers();
            LayananBaru();
            AdministrasiKeanggotaan();
            TradingMember();
        }
        

        private void UserRight() {
            Kernel.Bind<IUserRightRepository>().To<UserRightRepository>().InRequestScope();
        }
        
        private void Settings() {
            Kernel.Bind<ISettingRepository>().To<SettingRepository>().InRequestScope();
        }

        private void LayananBaru()
        {
            Kernel.Bind<ILayananBaruRepository>().To<LayananBaruRepository>().InRequestScope();
        }

        private void Sosialisasi() {
            Kernel.Bind<IEventRepository>().To<EventRepository>().InRequestScope();
        }

        private void Keuangan() {
            Kernel.Bind<IKeuanganRepository>().To<KeuanganRepository>().InRequestScope();
        }

        private void Mclears() {
            Kernel.Bind<IMclearsRepository>().To<MclearsRepository>().InRequestScope();
        }

        private void AdministrasiKeanggotaan()
        {
            Kernel.Bind<IAdministrasiKeanggotaanRepository>().To<AdministrasiKeanggotaanRepository>().InRequestScope();
        }
        private void TradingMember()
        {
            Kernel.Bind<ITradingMemberRepository>().To<TradingMemberRepository>().InRequestScope();
        }

        private void Pincode() {
            Kernel.Bind<IPincodeRepository>().To<PincodeRepository>().InRequestScope();
        }

        private void Survei() {
            Kernel.Bind<ISurveiRepository>().To<SurveiRepository>().InRequestScope();
        }

        private void DataAknp() {
            Kernel.Bind<IDataAknpRepository>().To<DataAknpRepository>().InRequestScope();
            DataAknpDrivers();
        }

        private void DataAknpDrivers() {

            #region core

            Kernel.Bind<IDataAknpDriverStore>().To<DataAknpDriverStore>().InRequestScope();

            #endregion

            #region registrasi baru

            Kernel.Bind<BaseAknpDriver>().To<DataRegistrasiAknpDriver>().InRequestScope();

            #endregion

            #region akte

            Kernel.Bind<BaseAknpDriver>().To<DataAkteAknpDriver>().InRequestScope();

            #endregion

            #region alamat

            Kernel.Bind<BaseAknpDriver>().To<DataAlamatAknpDriver>().InRequestScope();

            #endregion

            #region bank-pembayaran

            Kernel.Bind<BaseAknpDriver>().To<DataBankPembayaranAknpDriver>().InRequestScope();

            #endregion

            #region status-kpei

            Kernel.Bind<BaseAknpDriver>().To<DataStatusKpeiAknpDriver>().InRequestScope();
            
            #endregion

            #region status-bursa

            Kernel.Bind<BaseAknpDriver>().To<DataStatusBursaAknpDriver>().InRequestScope();

            #endregion

            #region perjanjian-ebu
            
            Kernel.Bind<BaseAknpDriver>().To<DataPerjanjianEbuAknpDriver>().InRequestScope();

            #endregion

            #region perjanjian-kbos
            
            Kernel.Bind<BaseAknpDriver>().To<DataPerjanjianKbosAknpDriver>().InRequestScope();

            #endregion

            #region informasi-lain

            Kernel.Bind<BaseAknpDriver>().To<DataInformasiLainAknpDriver>().InRequestScope();

            #endregion

            #region pme

            Kernel.Bind<BaseAknpDriver>().To<DataPmeAknpDriver>().InRequestScope();

            #endregion

            #region contact-person

            Kernel.Bind<BaseAknpDriver>().To<DataContactPersonAknpDriver>().InRequestScope();

            #endregion

            #region pejabat-berwenang

            Kernel.Bind<BaseAknpDriver>().To<DataPejabatBerwenangAknpDriver>().InRequestScope();

            #endregion

            #region SPAK

            Kernel.Bind<BaseAknpDriver>().To<DataSpakAknpDriver>().InRequestScope();

            #endregion

            #region SPAB

            Kernel.Bind<BaseAknpDriver>().To<DataSpabAknpDriver>().InRequestScope();

            #endregion

            #region Perjanjian

            Kernel.Bind<BaseAknpDriver>().To<DataPerjanjianAknpDriver>().InRequestScope();

            #endregion

            #region Jenis Usaha

            Kernel.Bind<BaseAknpDriver>().To<DataJenisUsahaAknpDriver>().InRequestScope();

            #endregion

            #region Tipe Member

            Kernel.Bind<BaseAknpDriver>().To<DataTipeMemberAknpDriver>().InRequestScope();

            #endregion
        }

        private void LaporanDrivers() {
            Kernel.Bind<BaseLaporanDttRequestForm>().To<LaporanJenisUsahaDttRequestForm>().InRequestScope();

            Kernel.Bind<ILaporanDriverStore>().To<LaporanDriverStore>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanAlamatDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanAkPerTahunDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanBankPembayaranDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanJenisUsahaDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanKeanggotaanLainDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanModalDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanPemegangSahamDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanPengurusPerusahaanDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanStatusPerusahaanDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanStatusBursaDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanStatusKpeiDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanPerjanjianDriver>().InRequestScope();
            // Kernel.Bind<BaseLaporanDriver>().To<LaporanTerbitSpabSpakDriver>().InRequestScope();
            // Kernel.Bind<BaseLaporanDriver>().To<LaporanCabutSpabSpakDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanPerubahanNamaDriver>().InRequestScope();
            // Kernel.Bind<BaseLaporanDriver>().To<LaporanSuspensiDriver>().InRequestScope();
            // Kernel.Bind<BaseLaporanDriver>().To<LaporanTipeMemberDriver>().InRequestScope();
            Kernel.Bind<BaseLaporanDriver>().To<LaporanPartisipanDriver>().InRequestScope();
        }
    }
}