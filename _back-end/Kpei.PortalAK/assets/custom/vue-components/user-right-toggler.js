﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function() {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;
    var eventName = 'toggleClicked';
    var comp = {
        name: 'user-right-toggler',
        template: '#user-right-toggler-tpl',
        props: [
            'htmlId',
            'cssClass',
            'checked',
            'isLoading'
        ],
        data: function() {
            return {};
        },
        methods: {
            clickToggle: function(check) {
                var self = this;
                self.$emit(eventName, check);
            }
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/user-right-toggler.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});