/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function() {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;
    var eventName = 'toggleClicked';
    var comp = {
        name: 'attendance-toggler',
        template: '#attendance-toggler-tpl',
        props: [
            'htmlId',
            'cssClass',
            'checked',
            'isLoading'
        ],
        data: function() {
            return {};
        },
        methods: {
            clickToggle: function(check) {
                var self = this;
                self.$emit(eventName, check);
            }
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/attendance-toggler.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});