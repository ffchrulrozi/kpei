﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function () {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;
    var devLog = true;

    var comp = {
        name: 'arr-member-ak-item',
        template: '#arr-member-ak-item-tpl',
        props: [
            'htmlId',
            'cssClass',
            'inputName',
            'dataItems',
            'dataOptions',
            'memberType'
        ],
        data: function () {
            return {
                items: [],
                timer: '',
                type: this.type
            };
        },
        methods: {
            addItem: function () {
                var self = this;
                if (this.type = 'GCM') {
                    self.items.push({
                        Id: '',
                        Parent: {
                            Name: '',
                            Code: ''
                        },
                        Child: {
                            Name: '',
                            Code: ''
                        }
                    });
                } else {
                    self.items.push({
                        Id: '',
                        Parent: {
                            Name: '',
                            Code: ''
                        },
                        Child: {
                            Name: '',
                            Code: ''
                        }
                    });
                }
                if (devLog) {
                    console.log('addItem triggered');
                    console.log(self.items);
                    console.log(self.type);
                }
            },
            remItemAt: function (idx) {
                var self = this;
                self.items = _.filter(self.items,
                    function (val, pos) {
                        return pos !== idx;
                    });
                if (self.items.length === 0) {
                    self.addItem();
                }
                if (devLog) {
                    console.log('remItemAt triggered');
                    console.log(self.options);
                }
            },
            reloadMemberType: function () {
                var indicator = $('select[name="TipeMemberAK"]').children("option:selected").val();
                if (indicator != this.type) {
                    this.$set(this, 'type', indicator);
                    console.log('ReloadMemberType: Indicator: ' + indicator);
                    console.log('ReloadMemberType: type: ' + this.type);
                    if (this.type == null || this.type == '') {
                        this.$set(this, items, '');
                    }
                }
            }
        },
        created: function () {
            var self = this;
            self.items = self.dataItems;
            if (self.items.length === 0) {
                self.addItem();
            }
            self.options = self.dataOptions;
            if (devLog) {
                console.log('created triggered');
                console.log(self.options)
            }
            self.timer = setInterval(this.reloadMemberType, 1000)
            self.type = self.memberType;
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/arr-member-ak-item.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});