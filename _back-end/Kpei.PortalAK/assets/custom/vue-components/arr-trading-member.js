﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function () {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;

    var comp = {
        name: 'arr-trading-member',
        template: '#arr-trading-member-tpl',
        props: [
            'htmlId',
            'cssClass',
            'inputName',
            'maxItems',
            'dataItems',
        ],
        data: function () {
            return {
                items: [],
            };
        },
        components: {
            vuejsDatepicker
        },
        methods: {
            addItem: function () {
                var self = this;
                if (self.items.length < self.maxItems) {
                    self.items.push({
                        Id: '',
                        KodePerusahaan: '',
                        NamaPerusahaan: '',
                    });
                }
            },
            remItemAt: function (idx) {
                var self = this;
                _.remove(self.items,
                    function (val, pos) {
                        Vue.set(self.items, pos, val);
                        return pos === idx;
                    });
                if (self.items.length === 0) {
                    self.addItem();
                }
            },
        },
        created: function () {
            var self = this;
            self.items = self.dataItems;
            if (self.items.length === 0) {

                self.addItem();
            }
            
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/arr-trading-member.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});