﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function() {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;

    var comp = {
        name: 'arr-pemegang-saham',
        template: '#arr-pemegang-saham-tpl',
        props: [
            'htmlId',
            'cssClass',
            'caption',
            'inputName',
            'dataItems',
            'maxItems'
        ],
        data: function() {
            return {
                items: []
            };
        },
        methods: {
            addItem: function() {
                var self = this;
                if (self.items.length < self.maxItems) {
                    self.items.push({
                        Id: '',
                        Nama: '',
                        WargaNegara: '',
                        LembarSaham: 0,
                        NominalSahamRupiah: 0,
                        __rupiahMaskStatus: false
                    });
                }
            },
            remItemAt: function(idx) {
                var self = this;
                self.items = _.filter(self.items,
                    function(val, pos) {
                        return pos !== idx;
                    });
                if (self.items.length === 0) {
                    self.addItem();
                }
            },
            persentaseSaham: function(item) {
                var self = this;
                var pct = 0;
                if (!item.LembarSaham) {
                    pct = 0.0;
                }
                if (self.totalLembarSaham > 0) {
                    pct = item.LembarSaham * 1.0 / self.totalLembarSaham * 100.0;
                } else {
                    pct = 0.0;
                }
                return pct.toFixed(2);
            },
            formatMoney: function(num) {
                return PortalAK.FormatMoney(num, 0);
            }
        },
        computed: {
            totalLembarSaham: function() {
                var self = this;
                var total = 0;
                _.each(self.items,
                    function(o) {
                        total += ((o.LembarSaham || 0) * 1);
                    });
                return total;
            },
            totalNominalSaham: function() {
                var self = this;
                var total = 0;
                _.each(self.items,
                    function(o) {
                        total += ((o.NominalSahamRupiah || 0) * 1);
                    });
                return total;
            }
        },
        created: function() {
            var self = this;
            self.items = self.dataItems;
            _.each(self.items, function(o) {
                o.__rupiahMaskStatus = false;
            });
            if (self.items.length === 0) {
                self.addItem();
            }
        },
        mounted: function() {
            const self = this;
            window.setInterval(function() {
                for (var i = 0; i < self.items.length; i++) {
                    var item = self.items[i];
                    if (!item.__rupiahMaskStatus) {
                        item.__rupiahMaskStatus = true;
                        var el = self.$refs['rupiah_' + i];
                        PortalAK.SetupInputMask(el);

                        var elOnChange = function () {
                            var num = this.name.match(/\d+/)[0];
                            self.items[num].NominalSahamRupiah = ($(this).val() || 0) * 1;
                        };
                        $(el).on('change', elOnChange);
                        $(el).on('keyup', elOnChange);
                    }

                }
            }, 500);
        },
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/arr-pemegang-saham.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});