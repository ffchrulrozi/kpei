﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function() {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;

    var comp = {
        name: 'string-list',
        template: '#string-list-tpl',
        props: [
            'htmlId',
            'cssClass',
            'inputName',
            'stringItems'
        ],
        data: function() {
            return {
                items: []
            };
        },
        methods: {
            addItem: function() {
                var self = this;
                self.items.push('');
            },
            remItemAt: function(idx) {
                var self = this;
                self.items = _.filter(self.items,
                    function(val, pos) {
                        return pos !== idx;
                    });
            }
        },
        created: function() {
            var self = this;
            self.items = self.stringItems;
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/string-list.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});