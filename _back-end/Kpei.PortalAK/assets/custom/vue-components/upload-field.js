﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
/* global Dropzone */
$(function() {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;

    var comp = {
        name: 'upload-field',
        template: '#upload-field-tpl',
        props: [
            'htmlId',
            'cssClass',
            'inputName',
            'placeholder',
            'value',
            'uploadUrl',
            'maxBytes',
            'validExts',
            'validMimes',
            'folderPath',
            'csrfToken'
        ],
        data: function() {
            return {
                val: '',
                maxFileSize: 0,
                randomId: 123,
                dropzone: null,
                successFiles: [],
                errors: []
            };
        },
        watch: {
            val: function(newVal, oldVal) {
                var self = this;
                self.$emit('input', newVal);
            },
            value: function(newVal, oldVal) {
                var self = this;
                if (self.val !== newVal) {
                    self.val = newVal;
                }
            }
        },
        computed: {
            modalId: function() {
                var self = this;
                return 'upload-modal-' + self.randomId;
            },
            maxFileSizeFriendly: function() {
                var self = this;
                var sz = self.maxFileSize;
                var suf = 'B';
                var sufs = ['KB', 'MB', 'GB', 'TB'];
                for (var i = 0; i < sufs.length; i++) {
                    var next = sz * 1.0 / 1024;
                    if (next >= 1) {
                        sz = next;
                        suf = sufs[i];
                    } else {
                        break;
                    }
                }
                return sz.toFixed(2) + ' ' + suf;
            },
            acceptedExts: function() {
                var self = this;
                var txt = '';
                var exts = self.validExts || [];
                if (exts.length === 0) {
                    return '<em>semua ekstensi file</em>';
                }
                for (var i = 0; i < exts.length; i++) {
                    if (i > 0) {
                        txt = txt + ', ';
                    }
                    txt = txt + exts[i];
                }
                return txt;
            },
            acceptedMimes: function() {
                var self = this;
                var txt = '';
                var mimes = self.validMimes || [];
                if (mimes.length === 0) {
                    return '';
                }
                for (var i = 0; i < mimes.length; i++) {
                    if (i > 0) {
                        txt = txt + ', ';
                    }
                    txt = txt + mimes[i];
                }
                return txt;
            }
        },
        methods: {
            resetValToInit: function() {
                var self = this;
                self.val = self.value;
            },
            clearVal: function() {
                var self = this;
                self.val = '';
            },
            uploadFiles: function() {
                var self = this;
                self.successFiles = [];
                self.errors = [];
                Vue.nextTick(function() {
                    self.dropzone.processQueue();
                });
            }
        },
        created: function() {
            var self = this;
            self.randomId = Math.floor(Math.random() * 10000000);
            self.resetValToInit();
            self.maxFileSize = self.maxBytes || 1;
            if (self.maxFileSize < 1) {
                self.maxFileSize = 1;
            }
        },
        mounted: function() {
            var self = this;
            var mimes = self.validMimes || [];
            var exts = self.validExts || [];
            var pars = {
                __RequestVerificationToken: self.csrfToken,
                FolderPath: self.folderPath,
                MaxBytes: self.maxFileSize
            };
            for (var extIdx = 0; extIdx < exts.length; extIdx++) {
                pars['ValidFileExtensions[' + extIdx + ']'] = exts[extIdx];
            }
            for (var mimeIdx = 0; mimeIdx < mimes.length; mimeIdx++) {
                pars['ValidMimeTypes[' + mimeIdx + ']'] = exts[mimeIdx];
            }
            self.dropzone = new Dropzone(self.$refs.uploadDropzone,
                {
                    dictDefaultMessage: '',
                    dictFileTooBig: 'Ukuran file melebihi maksimum',
                    url: baseUrl + 'file-manager/upload',
                    paramName: 'Files',
                    params: pars,
                    maxFiles: 1,
                    maxFilesize: self.maxFileSize / 1024.0 / 1024.0, // MB
                    addRemoveLinks: true,
                    autoProcessQueue: false,
                    parallelUploads: 1,
                    uploadMultiple: false,
                    init: function() {
                        var dz = this;
                        dz.on('addedfile',
                            function(file) {
                                if (dz.files.length > 1) {
                                    dz.removeFile(file);
                                }
                                self.successFiles = [];
                                self.errors = [];
                            });
                        dz.on('removefile', function(file) {
                                self.successFiles = [];
                                self.errors = [];
                            }),
                        dz.on('success',
                            function(file, resp) {
                                if (resp.Errors.length === 0) {
                                    self.successFiles.push(file.name + ' berhasil diupload');
                                    self.val = resp.SuccessFiles[0];
                                    dz.removeFile(file);
                                    $('#' + self.modalId).modal('hide');
                                } else {
                                    file.status = Dropzone.QUEUED;
                                    file.upload.bytesSent = 0;
                                    file.upload.progress = 0;
                                    window.setTimeout(function() {
                                        $(file.previewElement).removeClass('dz-processing dz-success dz-complete');
                                        $(file.previewElement).find('.dz-progress .dz-upload').width(0);
                                    }, 1000);
                                    _.forEach(resp.Errors, function(er) {
                                        self.errors.push(er);
                                    });
                                }
                            });
                        dz.on('error',
                            function(file, errMsg, xhr) {
                                if (xhr) {
                                    self.errors.push('Error ketika mengupload file ' + file.name);
                                } else {
                                    self.errors = [];
                                    self.errors.push(errMsg);
                                    dz.removeFile(file);
                                }
                            });
                    }
                });
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/upload-field.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});