﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function () {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;

    var comp = {
        name: 'arr-mclears-service',
        template: '#arr-mclears-service-tpl',
        props: [
            'htmlId',
            'cssClass',
            'inputName',
            'dataItems',
            'dataServices'
        ],
        data: function () {
            return {
                items: [],
                services: []
            };
        },
        created: function () {
            var self = this;
            self.items = self.dataItems;
            self.services = self.dataServices;
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/arr-mclears-service.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});