﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function() {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;

    var comp = {
        name: 'arr-partisipan',
        template: '#arr-partisipan-tpl',
        props: [
            'htmlId',
            'cssClass',
            'inputName',
            'dataItems',
            'dataGroups',
            'uploadFolder',
            'uploadFieldConfig'
        ],
        data: function() {
            return {
                groups: [],
                items: [],
                options: [],
                fields: []
            };
        },
        methods: {
            isChecked: function (index, event) {
                if (this.items[index].Nama == this.groups[index].Key && event.target.checked) {
                    this.groups[index].checked = true;

                    if (this.items[index].Nama == "PED") {
                        $('.nama-ab-sponsor-selector-container').show();
                        $('.laporan-keuangan-ak-partisipan-file-upload').hide();

                        $('.perjanjian-ped-file-upload').show();
                        $('.perjanjian-portability-file-upload').show();
                    } else {
                        $('.laporan-keuangan-ak-partisipan-file-upload').show();
                        $('.perjanjian-partisipan-file-upload').show();
                    }
                } else {
                    this.groups[index].checked = false;

                    if (this.items[index].Nama == "PED") {
                        $('.nama-ab-sponsor-selector-container').hide();
                        $('.laporan-keuangan-ak-partisipan-file-upload').show();

                        $('.perjanjian-ped-file-upload').hide();
                        $('.perjanjian-portability-file-upload').show();
                    } else {
                        $('.laporan-keuangan-ak-partisipan-file-upload').show();
                        $('.perjanjian-partisipan-file-upload').hide();
                    }
                }
            },
            addGroup: function () {
                var self = this;
                if (self.items.length < self.maxItems) {
                    self.groups.push({
                        Id: '',
                        FieldName: '',
                        FieldValue: '',
                        Checked: ''
                    });
                }
            },
            addItem: function() {
                var self = this;
                if (self.items.length < self.maxItems) {
                    self.items.push({
                        Id: '',
                        NamaDoc: '',
                        DocFileUrl : ''
                    });
                }
            },
            remItemAt: function(idx) {
                var self = this;
                self.items = _.filter(self.items,
                    function(val, pos) {
                        return pos !== idx;
                    });
                if (self.items.length === 0) {
                    self.addItem();
                }
            },
            remGroupAt: function (idx) {
                var self = this;
                self.groups = _.filter(self.groups,
                    function (val, pos) {
                        return pos !== idx;
                    });
                if (self.groups.length === 0) {
                    self.addGroup();
                }
            }
        },
        created: function() {
            var self = this;
            self.items = self.dataItems;
            self.groups = self.dataGroups;

            if (self.items.length === 0) {
                self.addItem();
            }

            if (self.groups.length === 0) {
                self.addGroup();
            } else {
                for (var groupIdx in self.groups) {
                    if (self.items[groupIdx].Nama == self.groups[groupIdx].Key && self.items[groupIdx].Value == true) {
                        self.groups[groupIdx].checked = true;

                        if (this.items[groupIdx].Nama == "PED") {
                            $('.nama-ab-sponsor-selector-container').show();
                            $('.laporan-keuangan-ak-partisipan-file-upload').hide();

                            $('.perjanjian-ped-file-upload').show();
                            $('.perjanjian-portability-file-upload').show();
                        } else {
                            $('.laporan-keuangan-ak-partisipan-file-upload').show();
                            $('.perjanjian-partisipan-file-upload').show();
                        }
                    } else {
                        self.groups[groupIdx].checked = false;

                        if (this.items[groupIdx].Nama == "PED") {
                            $('.nama-ab-sponsor-selector-container').hide();
                            $('.laporan-keuangan-ak-partisipan-file-upload').show();

                            $('.perjanjian-ped-file-upload').hide();
                            $('.perjanjian-portability-file-upload').show();
                        } else {
                            $('.laporan-keuangan-ak-partisipan-file-upload').show();
                            $('.perjanjian-partisipan-file-upload').hide();
                        }
                    }
                }
            }
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/arr-partisipan.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});