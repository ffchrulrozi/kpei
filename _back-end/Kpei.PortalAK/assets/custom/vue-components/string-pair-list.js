﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function() {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;

    var comp = {
        name: 'string-pair-list',
        template: '#string-pair-list-tpl',
        props: [
            'htmlId',
            'cssClass',
            'inputName',
            'objectItems',
            'leftInputName',
            'leftLabel',
            'rightInputName',
            'rightLabel'
        ],
        data: function() {
            return {
                items: []
            };
        },
        methods: {
            addItem: function() {
                var self = this;
                var obj = {};
                obj[self.leftInputName] = '';
                obj[self.rightInputName] = '';
                self.items.push(obj);
            },
            remItemAt: function(idx) {
                var self = this;
                self.items = _.filter(self.items,
                    function(val, pos) {
                        return pos !== idx;
                    });
            }
        },
        created: function() {
            var self = this;
            self.items = self.objectItems;
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/string-pair-list.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});