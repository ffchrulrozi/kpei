﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function () {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;

    var comp = {
        name: 'notification-count-vue',
        template: '#notification-count-tpl',
        props: [
            'notifUrl',
            'globalUrl',
            'userId'
        ],
        data: function () {
            return {
                url: '',
                urlGlobal: '',
                count: '',
                checked: false,
                trigger: true,
                items: [],
                interval: '',
                idLog: 'Notification Count: ',
                devMode: true,
                user: '',
                waitStatus: false
            };
        },
        methods: {
            get: function (resp) {
                var self = this;
                console.log(self.user);
                this.$http.get(self.url, { params: { userId: self.user } }).then((response) => {
                    self.checked = true;
                    if (self.count != response.body.NotificationCount) {
                        self.count = response.body.NotificationCount;
                    }
                    self.waitStatus = false;
                });
            },
            refresh: function () {
                var self = this;
                self.interval = setInterval(() => {
                    console.log(self.waitStatus == true ? "Waiting" : "Idle");
                    if (self.waitStatus == false) {
                        self.waitStatus = true;
                        self.get();
                    }
                }, 3000);
            },
            reset: function () {
                var self = this;
                if (self.trigger == true) {
                    self.trigger = false;
                    this.count = 0;
                } else {
                    self.trigger = true;
                }
            }
        },
        created: function () {
            var self = this;
            self.count = 0;
            self.url = self.notifUrl;
            self.urlGlobal = self.globalUrl;
            self.user = self.userId;
            self.waitStatus = false;
        },
        mounted: function () {
            this.refresh();
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/notification-count.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});