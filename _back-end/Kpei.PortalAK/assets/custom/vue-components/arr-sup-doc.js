﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function() {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;

    var comp = {
        name: 'arr-sup-doc',
        template: '#arr-sup-doc-tpl',
        props: [
            'htmlId',
            'cssClass',
            'inputName',
            'maxItems',
            'dataItems',
            'uploadFolder',
            'uploadFieldConfig'
        ],
        data: function() {
            return {
                items: []
            };
        },
        methods: {
            addItem: function() {
                var self = this;
                if (self.items.length < self.maxItems) {
                    self.items.push({
                        Id: '',
                        NamaDoc: '',
                        DocFileUrl : ''
                    });
                }
            },
            remItemAt: function(idx) {
                var self = this;
                self.items = _.filter(self.items,
                    function(val, pos) {
                        return pos !== idx;
                    });
                if (self.items.length === 0) {
                    self.addItem();
                }
            }
        },
        created: function() {
            var self = this;
            self.items = self.dataItems;
            if (self.items.length === 0) {
                self.addItem();
            }
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/arr-sup-doc.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});