﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function () {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;

    var comp = {
        name: 'arr-mclears-subscriber',
        template: '#arr-mclears-subscriber-tpl',
        props: [
            'htmlId',
            'cssClass',
            'inputName',
            'maxItems',
            'dataItems'
        ],
        data: function () {
            return {
                items: []
            };
        },
        methods: {
            addItem: function () {
                var self = this;
                if (self.items.length < self.maxItems) {
                    self.items.push({
                        Id: '',
                        Nama: '',
                        Jabatan: '',
                        Email: '',
                        Telp: '',
                        HP: '',

                        AlertACS: null,
                        AlertBACS: null,
                        AlertBroadct: null,
                        AlertCLA: null,
                        AlertCPA: null,
                        AlertDCDA: null,
                        AlertFCOLWA: null,
                        AlertHPHFA: null,
                        AlertHTR: null,
                        AlertHTS: null,
                        AlertL1: null,
                        AlertL3: null,
                        AlertL7: null,
                        AlertMCA: null,
                        AlertMCCA: null,
                        AlertMKBDA: null,
                        AlertOCA: null,
                        AlertPC: null,
                        AlertPS: null,
                        AlertTL5: null,
                        AlertTLP: null,
                        AlertTLS: null,

                        OnRequestCPR: null,
                        OnRequestDCDR: null,
                        OnRequestDCRR: null,
                        OnRequestHaircut: null,
                        OnRequestHPHFR: null,
                        OnRequestHTR: null,
                        OnRequestHTS: null,
                        OnRequestMCR: null,
                        OnRequestMKBDR: null,
                        OnRequestOCR: null,
                        OnRequestPC: null,
                        OnRequestPPR: null,
                        OnRequestPS: null
                    });
                }
            },
            remItemAt: function (idx) {
                var self = this;
                _.remove(self.items,
                    function (val, pos) {
                        Vue.set(self.items, pos, val);
                        return pos === idx;
                    });
                if (self.items.length === 0) {
                    self.addItem();
                }
            }
        },
        created: function () {
            var self = this;
            self.items = self.dataItems;
            if (self.items.length === 0) {
                self.addItem();
            }
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/arr-mclears-subscriber.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});