﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
$(function () {
    var baseUrl = PortalAK.BaseAppUrlAbsolute;

    var comp = {
        name: 'arr-layananjasa-trading-member',
        template: '#arr-layananjasa-trading-member-tpl',
        props: [
            'htmlId',
            'cssClass',
            'inputName',
            'maxItems',
            'dataItems',
            'listNamaTm',
            'uploadFolder',
            'uploadFieldConfig',
            'vuejsDatepicker'
        ],
        data: function () {
            return {
                items: [],
                selectedNamaTm: [],
            };
        },
        components: {
            vuejsDatepicker
        },
        methods: {
            onNamaTMSelected: function(i) {
                this.selectedNamaTm[i] = event.target.value
            },
            addItem: function () {
                var self = this;
                if (self.items.length < self.maxItems) {
                    self.items.push({
                        Id: '',
                        Nama: '',
                        NoPerjanjian: '',
                        TanggalPerjanjian: '',
                        PerjanjianFileUrl: ''
                    });
                }
            },
            remItemAt: function (idx) {
                var self = this;
                _.remove(self.items,
                    function (val, pos) {
                        Vue.set(self.items, pos, val);
                        return pos === idx;
                    });
                if (self.items.length === 0) {
                    self.addItem();
                }
            },
        },
        created: function () {
            var self = this;
            self.items = self.dataItems;
            for (var i = 0; i < self.items.length; i++) {
                self.selectedNamaTm[i] = self.items[i].Nama
            }

            if (self.items.length === 0) {

                self.addItem();
            }
            
        }
    };

    var mainTpl = baseUrl + 'assets/custom/vue-components/arr-layananjasa-trading-member.html';
    PortalAK.AddVueComponent(comp, mainTpl);
});