﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
(function () {
    PortalAK.InitDtt = function (tableHtmlSelector, url, columns, orderStg, preXhrHandler) {
        var csrfToken = PortalAK.AntiForgeryToken;

        columns = columns || [];
        orderStg = orderStg ||
            [
                [1, 'asc']
            ];

        var cols = [
            {
                name: null,
                data: null,
                title: 'No.',
                searchable: false,
                orderable: false,
                width: '30px',
                render: function (data, type, row, meta) {
                    return (meta.row + meta.settings.oAjaxData.start + 1) + '.';
                }
            }
        ];
        _.forEach(columns, function (col) {
            cols.push(col);
        });

        preXhrHandler = preXhrHandler || function (e, stg, dat) { };

        var noPrintCol = ["Action", "File"];

        var colNum = [];
        for (var i = 0; i < cols.length; i++) {
            if (noPrintCol.filter(x => x === cols[i].title || cols[i].title.match(x)) > -1) {
                colNum.push(i);
            }
        }

        customExport = [
            {
                extend: 'print',
                className: 'btn dark btn-outline',
                exportOptions: {
                    columns: colNum
                },
                customize: function (win) {
                    $(win.document.body)
                        .css('font-size', '10pt');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            },
            {
                extend: 'pdf',
                className: 'btn red btn-outline',
                exportOptions: {
                    columns: colNum
                },
                customize: function (doc) {
                    doc.defaultStyle.fontSize = 8;
                    doc.styles.tableHeader.fontSize = 8;
                }
            },
            {
                extend: 'excel',
                className: 'btn green-jungle btn-outline ',
                exportOptions: {
                    columns: colNum
                }
            },
            {
                extend: 'csv',
                className: 'btn purple btn-outline ',
                exportOptions: {
                    columns: colNum
                }
            }
        ];

        $(tableHtmlSelector).on('preXhr.dt',
            function (e, stg, dat) {
                dat.__RequestVerificationToken = csrfToken;
                preXhrHandler(e, stg, dat);
            });
        
        var dttCfg = {
            serverSide: true,
            processing: true,
            ajax: {
                url: url,
                method: 'POST',
                headers: {
                    accept: 'application/json'
                }
            },
            columns: cols,
            order: orderStg,
            lengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "All"]],
            buttons: customExport || [
                { extend: 'print', className: 'btn dark btn-outline' },
                { extend: 'pdf', className: 'btn red btn-outline' },
                { extend: 'excel', className: 'btn green-jungle btn-outline ' },
                { extend: 'csv', className: 'btn purple btn-outline ' }
            ]
        };

        var getExportButtonIcon = function (buttonName) {
            if (buttonName === 'print') {
                return '<i class="fa fa-print"></i>';
            } else if (buttonName === 'pdf') {
                return '<i class="fa fa-file-pdf-o"></i>';
            } else if (buttonName === 'excel') {
                return '<i class="fa fa-file-excel-o"></i>';
            } else if (buttonName === 'csv') {
                return '<i class="fa fa-file-text-o"></i>';
            }
            return '';
        }

        var masterTable = $(tableHtmlSelector).DataTable(dttCfg);

        var dttToolsPanel = $('<div class="text-center" style="margin: 10px;"></div>');
        _.each(dttCfg.buttons,
            function (btn, idx) {
                var btnEl = $('<button class="' + btn.className + '">'+ btn.extend + '</button>');
                btnEl.on('click',
                    function () {
                        masterTable.button(idx).trigger();
                    });
                dttToolsPanel.append(btnEl);
            });

        dttToolsPanel.insertBefore(tableHtmlSelector);

        return masterTable;
    }
})();