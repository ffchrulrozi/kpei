﻿/* global PortalAK */
/* global window */
/* global _ */
/* global Vue */
/* global $ */
/* global bootbox */
/* global Inputmask */

(function() {
    PortalAK.AntiForgeryToken = $('#portal-ak-anti-forgery-hidden input').val();

    PortalAK.AddVueComponent = function(componentObject, mainTemplate, extraTemplates) {
        extraTemplates = extraTemplates || [];
        _.forEach(extraTemplates,
            function(tpl) {
                PortalAK.VueComponentPromises.push(
                    new Promise(
                        function(resolve, reject) {
                            $.get(_.trim(tpl, ' \t\r\n\\/'))
                                .done(function(html) {
                                    $('body').append(html);
                                    resolve(1);
                                }).fail(function(err) {
                                    reject(err);
                                });
                        }
                    )
                );
            });

        PortalAK.VueComponentPromises.push(
            new Promise(
                function(resolve, reject) {
                    $.get(_.trim(mainTemplate, ' \t\r\n\\/'))
                        .done(function(html) {
                            $('body').append(html);
                            Vue.component(componentObject.name, componentObject);
                            resolve(1);
                        }).fail(function(err) {
                            reject(err);
                        });
                }
            )
        );
    };

    PortalAK.LoadVue = function(vueDefinition, storeInPortalAK) {
        return new Promise(
            function(resolve) {
                Promise.all(PortalAK.VueComponentPromises).then(function() {
                    var vue = new Vue(vueDefinition);
                    if (storeInPortalAK) {
                        PortalAK.Vues.push(vue);
                    }
                    resolve(vue);
                });
            });
    };

    PortalAK.ConfirmAction = function(message, onYes) {
        message = message || 'Anda yakin?';
        onYes = onYes || function() {};
        bootbox.confirm(message,
            function(result) {
                if (result) {
                    onYes();
                }
            });
    }

    PortalAK.AlertMessage = function(message) {
        bootbox.alert(message);
    }

    PortalAK.AttachConfirmAlertToFormButtons = function() {
        $('button.form-submit-button').on('click',
            function () {
                console.log($btn.closest('form'));
                var $btn = $(this);
                bootbox.confirm('Anda yakin?',
                    function(result) {
                        if (result) {
                            $('input#draftStatus').val("false")
                            $btn.closest('form').submit();
                        }
                    });
            });
        $('button.form-draft-button').on('click',
            function() {
                var $btn = $(this);
                bootbox.confirm('Simpan sebagai draf, Anda yakin?',
                    function(result) {
                        if (result) {
                            $('input#draftStatus').val('true')
                            $btn.closest('form').submit();
                        }
                    });
            });

        $('button.form-reset-button').on('click',
            function() {
                bootbox.confirm('Anda yakin?',
                    function(result) {
                        if (result) {
                            window.location.replace(window.location.href);
                        }
                    });
            });
        $('button.delete-button').on('click',
            function() {
                var href = $(this).data('href')
                bootbox.confirm('Anda yakin?',
                    function(result) {
                        if (result) {
                            window.location.replace(href);
                        }
                    });
            });
    };

    PortalAK.DttLinkButtonHtml = function(href, label, btnType, iconCss) {
        btnType = btnType || 'default';
        return '<a href="' +
            href +
            '" class="btn btn-' +
            btnType +
            ' btn-xs"><i class="' +
            iconCss +
            '"></i> ' +
            label +
            '</a>';
    }
    
    PortalAK.InitDateTimePicker = function(elem, style) {
        var inp = style === 'time'
            ? $(elem).find('input[type=hidden].clock-input')
            : $(elem).find('input[type=hidden].datetime-input');
        var inpDisp = style === 'time'
            ? $(elem).find('input[type=text].clock-display')
            : $(elem).find('input[type=text].datetime-display');
        var tog = style === 'time' ? $(elem).find('.clock-toggle') : $(elem).find('.datetime-toggle');
        var res = style === 'time' ? $(elem).find('.clock-reset') : $(elem).find('.datetime-reset');
        if (style === 'time') {
            $(inpDisp).clockpicker({
                donetext: 'Pilih',
                autoclose: true,
                afterDone: function() {
                    var items = _.split($(inpDisp).val(), ':');
                    var hh = items[0] * 1;
                    var mm = items[1] * 1;
                    var locmom = moment([
                        2000, 5, 15, hh, mm, 0
                    ]);
                    $(inp).val(locmom.toISOString());
                }
            });
            $(tog).click(function(ev) {
                ev.stopPropagation();
                $(inpDisp).clockpicker('show');
            });
        } else {
            var dtopts = {
                isRTL: App.isRTL(),
                format: 'd M yyyy, hh:ii',
                autoclose: true,
                todayBtn: true,
                fontAwesome: true,
                pickerPosition: (App.isRTL() ? 'bottom-left' : 'bottom-right'),
                minuteStep: 5,
                language: 'id'
            };

            if (style === 'date') {
                dtopts.minView = 2;
                dtopts.format = 'd M yyyy';
            }
            
            $(inpDisp).datetimepicker(dtopts).on('changeDate', function(ev) {
                if (ev.date) {
                    $(inp).val(ev.date.toISOString().replace('Z', ''));
                } else {
                    $(inp).val('');
                }
                $(inp).trigger('change');
            });

            $(tog).click(function(ev) {
                ev.stopPropagation();
                $(inpDisp).datetimepicker('show');
            });
        }

        $(res).click(function(ev) {
            ev.stopPropagation();
            $(inpDisp).val('');
            $(inp).val('');
            $(inp).trigger('change');
        });
    }

    Inputmask.extendAliases({
        rupiah: {
            prefix: 'Rp ',
            groupSeparator: '.',
            radixPoint: ',',
            alias: 'numeric',
            placeholder: '0',
            autoGroup: true,
            groupSize: 3,
            digits: 2,
            showMaskOnHover: false,
            clearMaskOnLostFocus: false,
            removeMaskOnSubmit: true,
            allowMinus: true
        }
    });
    
    PortalAK.SetupInputMaskMoney = function(elem) {
        $(elem).inputmask('rupiah',
            {
                rightAlign: false,
                greedy: false,
                autoUnmask: true,
                placeholder: '0',
                digits: '0',
            });
    }

    PortalAK.SetupInputMask = function (elem) {
        $(elem).inputmask('rupiah',
            {
                rightAlign: false,
                greedy: false,
                autoUnmask: true,
                placeholder: '0',
                digits: '0',
                prefix: ''
            }
        )
    }

    //https://stackoverflow.com/a/149099
    PortalAK.FormatMoney = function(num, c, d, t) {
        var n = num,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? ',' : d,
            t = t == undefined ? '.' : t,
            s = n < 0 ? '-' : '',
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s +
            (j ? i.substr(0, j) + t : '') +
            i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) +
            (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
    };

})();

$(function() {
    PortalAK.AttachConfirmAlertToFormButtons();
});